<?php

class NitroSpinner {

    function getSpinnedText($myText) {

        $url = 'http://thebestspinner.com/api.php';
        $testmethod = 'identifySynonyms';
        # Build the data array for authenticating.
        $data = array();
        $data['action'] = 'authenticate';
        $data['format'] = 'php'; # You can also specify 'xml' as the format.
        # The user credentials should change for each UAW user with a TBS account.

        $data['username'] = 'darren@internetzonei.com';
        $data['password'] = '501ae653d2895';

        # Authenticate and get back the session id.
        # You only need to authenticate once per session.
        # A session is good for 24 hours.
        $output = unserialize($this->curl_post($url, $data, $info));

        if ($output['success'] == 'true') {

            //----- Skip Characters --- . , ! ? : ; - -- digits -- 's chars
            $skipChar = array('~[\d]{2}+~', '~\'\w?+~', '~[\.,\'"!\?;:\/\-\~\`]~');
            $skipProtectedWords = array('#pds#', '#pdr#');
            # Success.
            $session = $output['session'];

            # Build the data array for the example.
            $data = array();
            $data['session'] = $session;
            $data['format'] = 'php'; # You can also specify 'xml' as the format.
            $text = 'Please give some text';

            $spinText = $this->processDcl($myText, 'encode');

            $spinText = count($myText) ? $myText : $text;
            $spinText = urldecode(trim($spinText));
            $originalReq = $spinText;
            $spinText = str_replace("&nbsp;", " ", $spinText);
            $spinText = str_replace(array("’", "&rsquo;"), "'", $spinText);
            $spinText = preg_replace("/(<\/?)(\w+)([^>]*>)/e", "'\\1'.strtolower('\\2').'\\3 '", $spinText);
            $spinText = preg_replace("~\s\s+~", " ", str_replace('~', '', $spinText));
            $spinText = trim($spinText);
            $spinText = stripslashes($spinText);
            $spinText = trim($spinText);
            $spinText = preg_replace("~\s\s+~", " ", str_replace(array('~', '\\'), '', $spinText));
            //slit into words and get,set the new word synonyms
            $plainText = strip_tags($spinText);
            $plainText = str_replace("/", " / ", $plainText);
            $plainText = trim($plainText);

            // START check for new words
            $newSynonyms = $plainText;
            $hasNewSynms = true;
            $wordToReplace = array();
            $preDefinedWords = array();
            $protectedWords = (isset($_POST['pText']) && !empty($_POST['pText'])) ? (array) explode(",", rtrim(trim($_POST['pText']), ",")) : array();

            if ($hasNewSynms) {
                //-- send api request
                $data['action'] = $testmethod;
                $data['maxsyns'] = '50'; # The number of synonyms per term.
                $data['quality'] = '1';
                $data['text'] = $newSynonyms;
                // call the api 
                $output = $this->curl_post($url, $data, $info);
                $output = unserialize($output);
                $synText = str_replace("\r", "<br>", $output['output']);
                $synText = preg_replace("~\s\s+~", ' ', $synText);
                //echo  'syntext = '. $synText.' <br /> ---------------<br /> ';
                $fetchSyn = preg_split('~[\{\}]~', trim($synText));
                $fetchSyn = array_filter($fetchSyn, create_function('$v', 'return trim($v) ? trim($v) : false;'));
                $fetchSynUnq = array_unique($fetchSyn);
                //pr($synonymsRes);
                //pr($fetchSynUnq);
                //exit;
                foreach ($fetchSynUnq as $w) {
                    $w = preg_replace($skipChar, "", $w);
                    $w = strtolower(trim($w));

                    if ($w == '')
                        continue;
                    //echo strtolower($w) .'<br>-----<br>';
                    $wd = $synmKey = '';
                    //--- Insert new synonyms and words without synonyms to DB
                    if (preg_match("/\|/", $w)) {
                        $wd = explode("|", $w);
                        $synmKey = trim(mysql_escape_string(array_shift($wd)));
                        if ($synmKey == '' || strlen($synmKey) == 1)
                            continue;
                        if (isset($synonymsRes[$synmKey]))
                            continue;
                        //                         echo $synmKey.'<br>-----<br>';
                        $synonymsRes[$synmKey] = $synmKey . '|' . mysql_escape_string(implode('|', $wd));
                        $wordToReplace[$synmKey] = '{' . $synonymsRes[$synmKey] . '}';
                    } else {
                        $wd = explode(' ', $w);
                        foreach ($wd as $v) {
                            $v = trim($v);
                            if ($v == '' || ( strlen($v) == 1 ) || in_array($v, $skipProtectedWords))
                                continue;
                            if (preg_match("/[\d]{2}+/", $v))
                                continue;
                            $v = mysql_escape_string($v);
                            if (isset($synonymsRes[$v]))
                                continue;
                            //                            echo $v.'<br>-----+<br>';
                            $synonymsRes[$v] = mysql_escape_string($v);
                            $wordToReplace[$v] = $synonymsRes[$v];
                        }
                    }
                }
            }

            $returnText = '';
            preg_match_all("/(<.+?>)?([^<>]*)/is", $originalReq, $matches, PREG_SET_ORDER);
            //pr($matches);

            if (count($wordToReplace) > 0) {
                $randReplace = count($wordToReplace) > 50 ? array_rand($wordToReplace, 50) : array_keys($wordToReplace);      //---- Pick only 50 words to replace with original text
                //echo 'Random replaced Words = ';pr($randReplace);

                $rtxt = array();
                foreach ($matches as $k => $vv) {
                    $rtxt[] = $vv[1];           //--- add non text characters
                    $ttxt = '';
                    if (strlen($vv[2]) > 1) {
                        $wrds = explode(' ', $vv[2]);
                        $phrase = '';
                        foreach ((array) $wrds as $i => $w) {
                            $phrase .= ' ' . $w . ' ';
                            foreach ($randReplace as $pp) {
                                $phrase = trim($phrase);
                                if (preg_match('~\b' . $pp . '\b~is', $phrase)) {
                                    $ttxt .= (preg_match("/\|/", $wordToReplace[$pp])) ? substr_replace($phrase, $wordToReplace[$pp], stripos($phrase, $pp), strlen($pp)) . ' ' : $phrase . ' ';
                                    $phrase = '';
                                    break;
                                }
                            }
                            if ((count($wrds) == ($i + 1)) && (!empty($phrase))) {
                                $ttxt .= $phrase . ' ';
                                $phrase = '';
                            }
                        }

                        //--- Reassign predefined Synonyms
                        foreach ($preDefinedWords as $p => $pd) {
                            if (empty($pd))
                                continue;
                            if (preg_match('~#' . $p . '#~', $ttxt)) {
                                foreach ($preDefinedWords[$p] as $i => $pdw) {
                                    if (stripos($ttxt, '#' . $p . '#')) {
                                        $ttxt = substr_replace($ttxt, $pdw, stripos($ttxt, '#' . $p . '#'), 5);
                                        unset($preDefinedWords[$p][$i]);
                                    }
                                }
                            }
                        }
                    }
                    $rtxt[] = $ttxt;
                }
                $spinText = $this->processDcl($myText, 'decode');
                $myText = implode('', (array) $rtxt);
            }
        }
        return $myText;
    }

    function processDcl($myText, $code) {
        if (count($myText)) {
            if ($code == 'encode') {
                $myText = str_replace('[dcl=', 'L##dcl=', $myText);
                $myText = str_replace(']', 'R##', $myText);
            } elseif ($code == 'decode') {
                $myText = str_replace('L##dcl=', '[dcl=', $myText);
                $myText = str_replace('R##', ']', $myText);
            }
        }
        return $myText;
    }

    /* =======spinner functions start======= */

    function curl_post($url, $data, &$info) {
        $header = array();
        $header[] = "Keep-Alive: 300";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->curl_postData($data));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $html = trim(curl_exec($ch));
        curl_close($ch);

        return $html;
    }

    function curl_postData($data) {
        $fdata = "";
        foreach ($data as $key => $val) {
            $fdata .= "$key=" . urlencode($val) . "&";
        }
        return $fdata;
    }

    function exists($word = '', $type = 'e') {
        $sql = mysql_query("select * from synonyms where word =  '" . trim($word) . "' LIMIT 0,1");
        if ($type == 'e') {
            return mysql_num_rows($sql);
        } else {
            $d = mysql_fetch_array($sql);
            return $d;
        }
    }

    function searchConc($str) {
        if (empty($str))
            return $str;

        $sep = preg_split('~[\{\}]~', trim($str));
    }

    /* =======additional functions start======= */

    function spinContent($txt) {
        $pattern = '#\{([^{}]*)\}#msi';
        $test = preg_match_all($pattern, $txt, $out);
        if (!$test)
            return $txt;
        $atrouver = array();
        $aremplacer = array();
        foreach ($out[0] as $id => $match) {
            $choisir = explode("|", $out[1][$id]);
            $atrouver[] = trim($match);
            $aremplacer[] = trim($choisir[rand(0, count($choisir) - 1)]);
        }
        $reponse = str_replace($atrouver, $aremplacer, $txt);
        return spinContent($reponse);
    }

}

?>