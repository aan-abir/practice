<?php

class LinkemperorVendor {

    function __construct($api_key) {
        $this->api_key = "76d4c635242a15890a4022ca695ef9f7c17fc195";
        $this->base_path = 'https://app.linkemperor.com/';
    }

    function linkemperor_bundle($id) {
        if (!is_numeric($id))
            die("You called linkemperor_bundle() with an invalid ID parameter.");
        return $this->linkemperor_exec("linkbuilding_bundles/" . $id . ".xml");
    }

    /**
     * Marks a linkbuilding request as fulfilled
     * @param integer ID of the request
     * @param text List of URLs built (\n-delimited)
     */
    function linkemperor_fulfill($id, $links) {
        $id = (string) $id;
        if (!is_numeric($id))
            die("You called linkemperor_fulfill() with an invalid ID parameter.");
        if (!strlen($links))
            die("You called linkemperor_fulfill() with an invalid links parameter.");
        $body = '<linkbuilding_request><links>' . $this->linkemperor_cleanlinks($links) . '</links></linkbuilding_request>';
        return $this->linkemperor_exec("linkbuilding_requests/" . $id . ".xml", $body, 'PUT');
    }

    /**
     * Retrieves the spun body with links placed in it
     * @param object the request object returned by linkemperor_request()
     * @param number of links to include per article
     */
    function linkemperor_place_links_in_body($request, $number_of_links = 4) {
        $body = $this->linkemperor_spin($request->article_spun_body);
        $orders = $this->linkemperor_orders($request);
        shuffle($orders);
        $paras = explode("\n", $body);
        for ($i = 0; $i < $number_of_links; $i++) {
            $link = array_pop($orders);
            $paras[array_rand($paras)] .= " [" . $link->url . " " . $link->anchor_text . "]";
        }
        return implode("\n", $paras);
    }

    /**
     * Retrieves the orders in a request in a simple array format
     * @param object the request object returned by linkemperor_request()
     */
    function linkemperor_orders($request) {
        $orders_array = array();
        $orders = $request->linkbuilding_orders->linkbuilding_order;
        foreach ($orders as $order) {
            $orders_array[] = $order;
        }
        return $orders_array;
    }

    /**
     * Purchases linkbuilding
     * @param string URL to call back to when there is a status update
     * @param string custom identifier string (you may use anything here)
     * @param array Array of requests to go into this bundle -- see documentation
     */
    function linkemperor_purchase($callback_url, $custom, $how_pay = "credits", $requests) {
        $body = '<linkbuilding_bundle><callback_url>' . $callback_url . '</callback_url><custom>' . $custom . '</custom><how_pay>' . $how_pay . '</how_pay><requests>' . $this->linkemperor_purchase_requests($requests) . '</requests></linkbuilding_bundle>';
        return $this->linkemperor_exec("linkbuilding_bundles.xml", $body, 'POST');
    }

    /**
     * Retrieves the information and  orders associated with a linkbuilding request
     * @param integer ID of the request
     */
    function linkemperor_request($id) {
        if (!is_numeric((string) $id))
            die("You called linkemperor_request() with an invalid ID parameter.");
        return $this->linkemperor_exec("linkbuilding_requests/" . (string) $id . ".xml");
    }

    /**
     * Retrieves the list of available linkbuilding requests
     */
    function linkemperor_requests($service_id = null) {
        $path = "linkbuilding_requests.xml";
        if ($service_id)
            $path .= "?service_id=" . $service_id;
        return $this->linkemperor_exec($path)->linkbuilding_request;
    }

    /**
     * Retrieves the list of available linkbuilding requests
     */
    function linkemperor_services() {
        return $this->linkemperor_exec("linkbuilding_services.xml");
    }

    /**
     * Returns a random spin of spintax
     */
    function linkemperor_spin($spintax) {
        return preg_replace_callback('/\{([^}]*)\}/', 'linkemperor_spin_callback', $spintax);
    }

    /*
     * Internal
     */

    function linkemperor_cleanlinks($links) {
        $clean_links = array();
        foreach (explode("\n", $links) as $link) {
            $clean_links[] = preg_replace('/&$/', "", $link);
        }
        return implode("\n", $clean_links);
    }

    function linkemperor_exec($uri, $post_data = null, $method_type = null) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://app.linkemperor.com/" . $uri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->api_key . ":x");
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        if ($post_data) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        if ($method_type) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method_type);
        }
        $data = curl_exec($ch);
        if (strlen($data)) {
            $xml = simplexml_load_string($data);
            return $xml;
        } else {
            return false;
        }
    }

    function linkemperor_purchase_requests($requests) {
        $out = "";
        foreach ($requests as $request) {
            $out .= '<request>';
            if ($request['orders'] && count($request['orders'])) {
                $out .= '<orders>';
                foreach ($request['orders'] as $order) {
                    $out .= '<order>';
                    foreach (array('url', 'anchor_text', 'title', 'description', 'category') as $k) {
                        if (isset($order[$k]))
                            $out .= '<' . $k . '>' . $order[$k] . '</' . $k . '>';
                    };
                    $out .= '</order>';
                }
                $out .= '</orders>';
            }
            if ($request['services'] && count($request['services'])) {
                $out .= '<services>';
                foreach ($request['services'] as $service_id => $quantity) {
                    $out .= '<service>';
                    $out .= '<service_id>' . $service_id . '</service_id>';
                    $out .= '<quantity>' . $quantity . '</quantity>';
                    $out .= '</service>';
                }
                $out .= '</services>';
            }
            foreach (array('order_to', 'custom', 'article_body', 'article_text') as $k) {
                if (isset($request[$k]))
                    $out .= '<' . $k . '>' . $request[$k] . '</' . $k . '>';
            }
            $out .= '</request>';
        }
        return $out;
    }

    function linkemperor_spin_callback($matches) {
        $words = explode("|", $matches[1]);
        return rand_element($words);
    }

}

?>