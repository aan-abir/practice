<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'importdomains'), 'id' => 'addDomainForm', 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="file">Upload CSV/Excel File:</label>
                <input type="file" name="data[Domain][file]" id="file" title="Upload cvs/excel File" />
                <span class="help-inline blue">please follow the format of <strong><a href="http://seonitrov2.seonitro.com/samplefiles/Import_Sample.xlsx">this file</a></strong></span>
            </div>
        </div>
    </div>
    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Import Domain</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

