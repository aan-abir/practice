<style>
    .textBlue { color: #3A87AD;}
</style>
<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Rank Campaign URL Results for Campaign: <span class="textBlue"><a style="text-decoration: underline;" href="<?php echo Router::url('viewrankresult/' . @$campaignInfo['Rankcampaign']['id']); ?>"><?php echo @$campaignInfo['Rankcampaign']['campaign_name']; ?></a></span> Keyword: <span class="textBlue"><?php echo @$keyword; ?></strong></span>
                </h4>
            </div>
            <div class="content clearfix">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (@count($rankKeyword)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th><strong>URL</strong></th>
                            <th><strong>Page</strong></th>
                            <th><strong>Position</strong></th>
                            <th><strong>Title</strong></th>
                            <!--<th><strong>Description</strong></th>-->
                            <th><strong>Rank Date</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($rankKeyword) && count($rankKeyword) > 0):
                            foreach ($rankKeyword as $arr):
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td><a href="http://<?php echo str_replace(array('http://', 'https://'), '', $arr['rank_url']); ?>" target="_blank"><?php echo $arr['rank_url']; ?></a></td>
                                    <td><?php echo $arr['rank_page']; ?></td>
                                    <td><?php echo $arr['rank_position']; ?></td>
                                    <td><?php echo $arr['rank_title']; ?></td>
                                    <!--<td><?php echo $arr['rank_description']; ?></td>-->
                                    <td><?php echo date('M d, Y', strtotime($arr['rank_date'])); ?></td>
                                </tr>

                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="5">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>