<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Press Release: <?php echo $pressReleaseInfo['Pressrelease']['headline']; ?></span>
                </h4>
            </div>
            <div class="content">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="textRight" style="width: 150px;"></th>
                            <th class="textLeft"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="textRight">Summary</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['summary']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Quote</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['quote']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Quote Author</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['quote_author']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Body</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['body']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Video URL</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['video_url']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Video Script</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['video_script']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Image 1 Title</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['image_1_title']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Image 1 File</td>
                            <td class="textLeft">
                                <?php
                                if ($pressReleaseInfo['Pressrelease']['image_1_file']):
                                    echo $this->Html->image('/press_release/' . $pressReleaseInfo['Pressrelease']['image_1_file'], array('width' => '200px'));
                                endif;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="textRight">Image 2 Title</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['image_2_title']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Image 2 File</td>
                            <td class="textLeft">
                                <?php
                                if ($pressReleaseInfo['Pressrelease']['image_2_file']):
                                    echo $this->Html->image('/press_release/' . $pressReleaseInfo['Pressrelease']['image_2_file'], array('width' => '200px'));
                                endif;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="textRight">Image 3 Title</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['image_3_title']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Image 3 File</td>
                            <td class="textLeft">
                                <?php
                                if ($pressReleaseInfo['Pressrelease']['image_3_file']):
                                    echo $this->Html->image('/press_release/' . $pressReleaseInfo['Pressrelease']['image_3_file'], array('width' => '200px'));
                                endif;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="textRight">Image 4 Title</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['image_4_title']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Image 4 File</td>
                            <td class="textLeft">
                                <?php
                                if ($pressReleaseInfo['Pressrelease']['image_4_file']):
                                    echo $this->Html->image('/press_release/' . $pressReleaseInfo['Pressrelease']['image_4_file'], array('width' => '200px'));
                                endif;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="textRight">Attachment 1 Title</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['attachment_1_title']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Attachment 1 File</td>
                            <td class="textLeft">
                                <?php
                                if ($pressReleaseInfo['Pressrelease']['attachment_1_file']):
                                    $filename = APP . 'webroot/press_release/' . $pressReleaseInfo['Pressrelease']['attachment_1_file'];
                                    if (file_exists($filename)) {
                                        echo $this->Html->link('Current File', array('controller' => 'clients', 'action' => 'downloadFile', 'press_release', $pressReleaseInfo['Pressrelease']['attachment_1_file']));
                                    }
                                endif;
                                ?>                                 
                            </td>
                        </tr>
                        <tr>
                            <td class="textRight">Attachment 2 Title</td>
                            <td class="textLeft">
                                <?php echo $pressReleaseInfo['Pressrelease']['attachment_2_title']; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="textRight">Attachment 2 File</td>
                            <td class="textLeft">
                                <?php
                                if ($pressReleaseInfo['Pressrelease']['attachment_2_file']):
                                    $filename = APP . 'webroot/press_release/' . $pressReleaseInfo['Pressrelease']['attachment_2_file'];
                                    if (file_exists($filename)) {
                                        echo $this->Html->link('Current File', array('controller' => 'clients', 'action' => 'downloadFile', 'press_release', $pressReleaseInfo['Pressrelease']['attachment_2_file']));
                                    }
                                endif;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="textRight">Attachment 3 Title</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['attachment_3_title']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Attachment 3 File</td>
                            <td class="textLeft">
                                <?php
                                if ($pressReleaseInfo['Pressrelease']['attachment_3_file']):
                                    $filename = APP . 'webroot/press_release/' . $pressReleaseInfo['Pressrelease']['attachment_3_file'];
                                    if (file_exists($filename)) {
                                        echo $this->Html->link('Current File', array('controller' => 'clients', 'action' => 'downloadFile', 'press_release', $pressReleaseInfo['Pressrelease']['attachment_3_file']));
                                    }
                                endif;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="textRight">Attachment 4 Title</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['attachment_4_title']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Attachment 3 File</td>
                            <td class="textLeft">
                                <?php
                                if ($pressReleaseInfo['Pressrelease']['attachment_4_file']):
                                    $filename = APP . 'webroot/press_release/' . $pressReleaseInfo['Pressrelease']['attachment_4_file'];
                                    if (file_exists($filename)) {
                                        echo $this->Html->link('Current File', array('controller' => 'clients', 'action' => 'downloadFile', 'press_release', $pressReleaseInfo['Pressrelease']['attachment_4_file']));
                                    }
                                endif;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="textRight">Release Date</td>
                            <td class="textLeft"><?php echo date('d M, Y', strtotime($pressReleaseInfo['Pressrelease']['release_date'])); ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Keyword Tags</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['keyword_tags']; ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Industry</td>
                            <td class="textLeft"><?php echo $this->NitroCustom->getIndustryList($pressReleaseInfo['Pressrelease']['industry']); ?></td>
                        </tr>
                        <tr>
                            <td class="textRight">Contact Name</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['contact_name']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">Contact Email</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['contact_email']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">Contact Phone</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['contact_phone']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">Brand Name</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['brand_name']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">Brand Website</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['brand_website']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">Twitter</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['twitter_url']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">Facebook</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['facebook_url']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">LinkedIn</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['linkedin_url']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">Google Plus</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['google_plus_url']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">RSS</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['rss_url']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">Posted URL</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['release_url']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">Status</td>
                            <td class="textLeft"><?php echo $pressReleaseInfo['Pressrelease']['status']; ?></td>
                        </tr>                        
                        <tr>
                            <td class="textRight">Created</td>
                            <td class="textLeft"><?php echo date('d M, Y', strtotime($pressReleaseInfo['Pressrelease']['created'])); ?></td>
                        </tr>                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>