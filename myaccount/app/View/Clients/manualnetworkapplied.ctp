<div class="marginT10"></div>
<div class="content">
    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($manualsites)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
        <thead>
            <tr>
                <th>Domain</th>
                <th>CMS</th>
                <th>Age</th>
                <th>PR</th>
                <th>L.J PR</th>
                <th>C.Flow</th>
                <th>T.Flow</th>
                <th>Analyze</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($manualsites) > 0):
                foreach ($manualsites as $ms):
                    $loginUrl = 'http://' . str_replace('http://', '', $ms['Domain']['domain']) . '/wp-admin';
                    $Url = 'http://' . str_replace('http://', '', $ms['Domain']['domain']);
                    ?>
                    <tr>
                        <td><a href="<?php echo $Url; ?>" target="_blank"><?php echo $ms['Domain']['domain']; ?></a></td>
                        <td><?php echo $ms['Domain']['cms']; ?></td>
                        <td><?php echo $ms['Domain']['age']; ?></td>
                        <td><?php echo $ms['Domain']['pr']; ?></td>
                        <td><?php echo $ms['Domain']['ljpr']; ?></td>
                        <td><?php echo $ms['Domain']['cflow']; ?></td>
                        <td><?php echo $ms['Domain']['tflow']; ?></td>
                        <td>

                            <a title="Quick Summery of - <?php echo $ms['Domain']['domain']; ?> " href="<?php echo Router::url('quickviewdomain/' . $ms['Domain']['id']); ?>" class="openModalDialog">quick view</a> 
                        </td>
                        <td>
                            <div class="controls center">
                                <?php
                                $hmu = $ms['Manualsharedsite'];
                                if ($hmu['status'] == 1):
                                    ?>
                                    <span>Username: <strong><?php echo $hmu['authusername']; ?></strong></span><br/>
                                    <span>Password: <strong><?php echo $hmu['authpassword']; ?></strong></span><br/>
                                    <a href="<?php echo $loginUrl; ?>" title="Login to this domain admin panel now" class="tip" target="_blank">get in</a>
                                    <?php
                                elseif ($hmu['status'] == 0 && $hmu['applied'] == 1):
                                    ?>
                                    <span>Applied</span>    
                                    <?php
                                endif;
                                ?>
                            </div>
                        </td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="8">No Applied Manual Sites Found!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>

</div>

<script type="text/javascript">
    //--------------- Popovers ------------------//
    //using data-placement trigger
    $("a[rel=popover]")
            .popover()
            .click(function(e) {
                e.preventDefault()
            })

    //using js trigger
    $("a[rel=popoverTop]")
            .popover({placement: 'top'})
            .click(function(e) {
                e.preventDefault()
            })
</script>

