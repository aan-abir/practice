<?php 
$links = unserialize($detail['Linkemperorresults']['built_urls']); 
?>
<div class="marginB10"></div>
<div class="page-header">
    <h4>All the urls your order got posted via LinkEmperor</h4>
    <table class="table table-bordered" cellpadding="2" cellspacing="2">
          <tr>
            <td>Order ID: <?php echo $detail['Linkemperorresults']['order_id']; ?></td>
            <td>Blast ID: <?php echo $detail['Linkemperorresults']['blast_id']; ?></td>
            <td>Service ID: <?php echo $detail['Linkemperorresults']['service_id']; ?></td>
          </tr>
          <tr>
            <td>Service: <?php echo $detail['Linkemperorresults']['service']; ?></td>
            <td>Vendor: <?php echo $detail['Linkemperorresults']['vendor']; ?></td>
            <td>Date Created: <?php echo $detail['Linkemperorresults']['datecreated']; ?></td>
          </tr>
    </table>
    
</div>


<div class="content" style="max-height: 600px; overflow: hidden;">
    <div style="height: 550px; overflow: auto;">
    <table class="table table-bordered">
        <thead>
            <tr> 
                <th>Url Posted</th>
                <th>No Of Post</th>
            </tr>
        </thead> 
        <tbody>
            <?php 
            foreach ( $links as $k=> $l ):
            
            echo '<tr>';
            echo '<td><a href="'.$l['url'].'" target="blank" title="Go to Domain" class="tip">'. urldecode($l['url']).'</a></td>
                <td>'.$l['successful_links_found'].'</td>
            </tr>';
           endforeach; ?>
        </tbody>

    </table>
    </div>
</div>
 