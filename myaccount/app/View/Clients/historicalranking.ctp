<div class="marginB10"></div>
<div class="page-header">
    <h4>Historical Ranking </h4>
    <p>Please select domain and search engine location to get historical ranking.</p>
</div>
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'historicalranking'), 'method' => 'post')); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span2" for="domain_url"></label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[HRanking][is_url]" value="0" checked="checked" />
                    Domain
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[HRanking][is_url]" value="1" />
                    URL
                </div>
            </div>
        </div>
    </div>     
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span2" for="domain_url">Domain/URL</label>
                <!--<div class="span2"><span style="font-size:14px;line-height:26px;font-weight:bold;">Domain/URL</span></div>-->
                <div class="span3" style="margin-right: 20px;">
                    <?php echo $this->Form->input('HRanking.link_name', array('error' => false, 'required' => false, 'title' => 'Domain', 'placeholder' => "Your Domain/URL", 'label' => false, 'div' => false)); ?>
                </div>
                <div class="span2" style="margin-right: 20px;">
                    <?php
                    echo $this->Form->select('HRanking.se', $searchEngines, array('error' => false, 'required' => false, 'div' => false, 'empty' => false));
                    ?>                   
                </div>
                <div class="span2">
                    <button class="btn btn-info send-middle" type="submit">Get Report</button>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

<div class="row-fluid">
    <div class="span12">
        <?php
        if (isset($result)):
            if (preg_match("/^ERROR\s[0-9]+\s::[a-zA-Z0-9\s]+/i", $result)):
                echo $result;
            else:
                $data = explode("\n", trim($result));
                $fields = explode(";", array_shift($data));
                if (count($data) > 0):
                    ?>
                    <div class="box" style="background-color: #FFF;">
                        <div class="title">
                            <h4>
                                <span>Historical Ranking for Your Domain</span>
                            </h4>
                        </div>
                        <div class="content">
                            <table class="responsive table">
                                <tr>
                                    <td valign='top' style="background-color: #FFF;">
                                        <script>
                                            var domainName = "<?php echo $domainName; ?>";
                                            var db = "us";
                                            if (domainName.substring(0, 4) == "www.") {
                                                var gDomName = domainName.substring(4, domainName.length);
                                            } else {
                                                var gDomName = domainName;
                                            }
                                            document.write('<object id="graph_out" width="950" height="250">');
                                            document.write('<param name="wmode" value="transparent" />');
                                            document.write('<param name="quality" value="high" />');
                                            document.write('<param name="bgcolor" value="#ffffff" />');
                                            document.write('<param name="allowScriptAccess" value="always" />');
                                            document.write('<param name="movie" value="http://www.semrush.com/m/scripts/graph/graph_out.swf" />');
                                            document.write('<param name="FlashVars" value="domain=' + gDomName + '&gtype=1&db=' + db + '&w=850&h=250" />');
                                            document.write('<embed src="http://www.semrush.com/m/scripts/graph/graph_out.swf" FlashVars="domain=' + gDomName + '&gtype=1&db=' + db + '&w=850&h=285" quality="high" bgcolor="#ffffff" width="850" height="300" name="graph_out" align="middle" play="true" loop="false" quality="high" allowScriptAccess="always"type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer"></embed>');
                                            document.write('</object>');
                                        </script>
                                    </td>
                                </tr>
                            </table>
                            <div class="marginB10"></div>                            
                            <table class="responsive table table-bordered">
                                <thead>
                                    <tr>
                                        <?php
                                        foreach ($fields as $field):
                                            ?>
                                            <th><?php echo $field; ?></th>
                <?php
            endforeach;
            ?>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $line) {
                                        $values[] = explode(";", $line, count($fields));
                                    }
                                    foreach ($values as $line):
                                        ?>
                                        <tr>
                                            <?php
                                            $i = 0;
                                            foreach ($line as $value):
                                                ?>					<?php if ($i != 8): ?>
                                                    <td align='<?php echo ($i == 0 || $i == 4) ? "left" : "center"; ?>'><?php echo $value; ?></td>
                                                <?php else: ?>
                                                    <td align='right'><?php echo @number_format(intval($value)); ?></td>
                                                <?php endif; ?>
                                            <?php
                                            $i++;
                                        endforeach;
                                        ?>
                                        </tr>
                <?php
            endforeach;
            ?>
                                </tbody>
                            </table>
                        </div>

                    </div><!-- End .box -->            
                    <?php
                else:
                    ?>
                    No data found for your request
                <?php
                endif;
            endif;
        endif;
        ?>
    </div>
</div>