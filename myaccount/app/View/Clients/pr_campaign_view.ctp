<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <a style="position: absolute; left: 50%; z-index: 20" data-toggle="modal" title="Export all the domains" class="btn btn-link tip btn-success" href="<?php echo Router::url('report_csv') ?>"><span class="icon16 icomoon-icon-file-excel"></span> Export List </a>

        <div class="content clearfix">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($page_rank_campaign_domain_list)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th><strong>Domain</strong></th>
                        <th><strong>PR</strong></th>
                        <th><strong>Indexed</strong></th>
                        <th><strong>Created</strong></th>
                        <th><strong>Expires</strong></th>
                        <!--<th><strong>Age</strong></th>-->
                        <th><strong>Name Server</strong></th>
                        <th><strong>Seomoz Rank</strong></th>
                        <th><strong>Citation Flow</strong></th>
                        <th><strong>Trust Flow</strong></th>
                        <th><strong>Ext Back Links</strong></th>
                        <th><strong>Last Updated</strong></th>
                        <th><strong>Actions</strong></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (isset($page_rank_campaign_domain_list) && count($page_rank_campaign_domain_list) > 0):
                            foreach ($page_rank_campaign_domain_list as $row):
                                $arr = $row['Pruserdomain'];
                            ?>
                            <tr>
                                <td><?php echo $arr['domain']; ?></td>
                                <td><?php echo $arr['page_rank']; ?></td>
                                <td><?php echo $arr['indexed']; ?></td>
                                <td><?php echo $arr['created']; ?></td>
                                <td><?php echo $arr['expires']; ?></td>
                                <!--<td><?php echo $arr['age']; ?></td>-->
                                <td><?php echo $arr['name_server']; ?></td>
                                <td><?php echo $arr['seomoz_rank']; ?></td>
                                <td><?php echo $arr['citation_flow']; ?></td>
                                <td><?php echo $arr['trust_flow']; ?></td>
                                <td><?php echo $arr['ext_back_links']; ?></td>
                                <td><?php echo date("d M Y", strtotime($arr['updated_at'])); ?></td>
                                <td align="left">
                                    <div class="controls center" style="width: 100px; text-align: left;">
                                        <a href="<?php echo Router::url('pr_domain_history/' . $arr['id']); ?>" title="View History" class="tip">&rArr; history</a> <br> 
                                         <a href="<?php echo Router::url('pr_campaign_delete_domain/' . $arr['id']); ?>" title="Remove domain?" class="tip callAction">&rArr; remove</a>  
                                    </div>                                        
                                </td>
                            </tr>
                            <?php
                                endforeach;
                            else:
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="4">No record found!</td>
                        </tr>
                        <?php
                            endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>