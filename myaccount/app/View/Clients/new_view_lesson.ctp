<div class="page-header">
    <h1>
        <?php echo $item['Lesson']['title']; ?> <br>
        <span class="help-block-inline"  style="font-size: 18px;">Expert: <?php echo $item['Module']['ename']; ?> | Length: <?php echo  $item['Lesson']['lesson_length']; ?></span></h1>
</div>



<div class="content">
    <div class="row-fluid">
        <div class="span8">

            <div class="content">
                <div class="row-fluid">
                    <div class="pull-left">
                        <?php 
                            if ( $item['Module']['photo'] != '' ){ ?>
                            <a title="Image Preview" class="fancybox tip" href="<?php echo  '/myaccount/moduleexpert/' . $item['Module']['photo']; ?>">
                                <img style="width: 130px; margin-right: 10px; border: 2px solid #333333;" alt="Preview" src="<?php echo  '/myaccount/moduleexpert/' . $item['Module']['photo']; ?>" />
                            </a>
                            <?php
                        } ?>
                    </div>
                    <div class="">
                        <?php echo $item['Lesson']['content']; ?>
                    </div>
                </div>
            </div>

            <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>

            <div class="content">
                <div class="row-fluid">
                    <div class="span12" style="text-align: center;">
                        <?php 
                            if ( $item['Lesson']['source'] != '' ) {
                                echo '<div style="margin-bottom:30px;" class="clearfix span12"><iframe src="http://www.youtube.com/embed/'.$item['Lesson']['source'].'" width="90%" height="480" frameborder="0"></iframe></div>';
                            }


                        ?>
                    </div>
                </div>
            </div>


            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h2 style="color: #074f14;">Downloads <span class="help-block-inline">download related files</span></h2>
                    </div>
                </div>
            </div>


            <div class="content">
                <div class="row-fluid">
                    <div class="span12">
                        <?php if ($item['Lessonfile']): ?>
                            <?php foreach ($item['Lessonfile'] as $sl => $lesf):
                                ?>
                                <div>
                                    <a href="<?php echo Router::url('/new/clients/downloadFile/' . base64_encode('files/lesson_files/' . $lesf['filename'])) . '/' . ($sl + 1); ?>"><?php echo $lesf['ftitle']; ?></a>
                                </div>
                                <?php
                                    endforeach;
                            ?>
                            <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>

        <div class="span4" style="border-left: 1px solid #cdc; padding-left: 10px;">

            <div class="box">
                <div class="title">
                    <h4>
                        <span>Module Playlist</span>
                    </h4>
                    <div class="content noPad">
                        
                        <?php 
                         
                           if ( count($allLesson) ) {
                               echo '<ul>'; 
                               foreach ( $allLesson as $k => $v ) {
                                  echo '<li><a href="http://pageoneengine.com/myaccount/new/clients/view_lesson/'.$v['Lesson']['id'].'">'.$v['Lesson']['title'].'</a></li>';  
                               }
                               echo '</ul>';
                           }
                        
                        ?>
                        
                    </div>
                </div>
            </div>   

            <div class="box">
                <div class="title">
                    <h4>
                        <span>Course Progress</span>
                    </h4>
                    <div class="content">
                        <span class="icon16 icomoon-icon-loop left"></span>
                        <div class="progress progress-mini progress-danger left tip" oldtitle="87%" title="" data-hasqtip="true" aria-describedby="qtip-4">
                            <div style="width: 87%;" class="bar"></div>
                        </div>
                        <span class="percent">87%</span>
                        <!--<div class="stat">19419.94 / 12000 MB</div>-->
                    </div>
                </div>
            </div>   

            <div class="box">
                <div class="title">
                    <h4>
                        <span>Course Badges</span>
                    </h4>
                    <div class="content">

                        <span class="icomoon-icon-home" aria-hidden="true"></span>
                        <span class="icomoon-icon-home-2" aria-hidden="true"></span>
                        <span class="icomoon-icon-home-3" aria-hidden="true"></span>
                        <span class="icomoon-icon-home-4" aria-hidden="true"></span>
                        <span class="icomoon-icon-home-5" aria-hidden="true"></span>
                        <span class="icomoon-icon-home-6" aria-hidden="true"></span>
                        <span class="icomoon-icon-home-7" aria-hidden="true"></span>
                        <span class="icomoon-icon-home-8" aria-hidden="true"></span>
                        <span class="icomoon-icon-home-9" aria-hidden="true"></span>
                        <span class="icomoon-icon-office" aria-hidden="true"></span>
                        <span class="icomoon-icon-newspaper" aria-hidden="true"></span>
                        <span class="icomoon-icon-pencil" aria-hidden="true"></span>
                        <span class="icomoon-icon-pencil-2" aria-hidden="true"></span>
                        <span class="icomoon-icon-pencil-3" aria-hidden="true"></span>
                        <span class="icomoon-icon-pencil-4" aria-hidden="true"></span>
                        <span class="icomoon-icon-pencil-5" aria-hidden="true"></span>
                        <span class="icomoon-icon-pen" aria-hidden="true"></span>
                    </div>
                </div>
            </div>   

        </div>
    </div>
</div>



