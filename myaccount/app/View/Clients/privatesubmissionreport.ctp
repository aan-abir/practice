
<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Start</th>
                <th>End</th>
                <th>Campaign Name</th>
                <th>Total Submitted</th>
                <th>View All Posts</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($submissions) && count($submissions) > 0):
                foreach ($submissions as $arr):
                    ?>
                    <tr>
                        <td></td>
                        <td><?php echo $arr['Submission']['submissionname']; ?></td>
                        <td><?php echo $arr['Submission']['startdate']; ?></td>
                        <td><?php echo $arr['Submission']['enddate']; ?></td>
                        <td>
                            <?php
                            if (count($arr['Submission']['postcampaigns'])) {
                                $postCampaigns = $arr['Submission']['postcampaigns'];
                                foreach ($postCampaigns as $k => $pC) {
                                    ?>
                                    <a href="<?php echo Router::url('viewcampaign/' . $k); ?>"><?php echo $pC; ?></a><br/>
                                    <?php
                                }
                            }
                            ?>
                        </td>
                        <td><?php echo count($arr['Submissionreport']); ?></td>
                        <td>
                            <button href="#" style="" id="<?php echo $arr['Submission']['id']; ?>" class="openModalDialog btn btn-link">
                                <span class="btn" >View All Post</span>
                            </button>                                        
                            <div id="modal_<?php echo $arr['Submission']['id']; ?>" title="Post Urls" class="dialog">
                                <ul>
                                    <?php
                                    if (count($arr['Submissionreport'])):
                                        foreach ($arr['Submissionreport'] as $sR):
                                            ?>
                                            <li><a target="_blank" href="http://<?php echo $sR['posturl']; ?>"><?php echo $sR['posturl']; ?></a></li>    
                                            <?php
                                        endforeach;
                                    else:
                                        ?>
                                        <span>No record found!</span>
                                    <?php
                                    endif;
                                    ?>
                                </ul>
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $('.openModalDialog').click(function() {
                                        var myid = $(this).attr('id');
                                        $('#modal_' + myid).dialog('open');
                                        return false;
                                    });

                                    // JQuery UI Modal Dialog
                                    $('#modal_<?php echo $arr['Submission']['id']; ?>').dialog({
                                        autoOpen: false,
                                        modal: true,
                                        dialogClass: 'dialog',
                                        buttons: {
                                            "Close": function() {
                                                $(this).dialog("close");
                                            }
                                        }
                                    });

                                    //$("div.dialog button").addClass("btn");

                                });
                            </script>                                        

                        </td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="6">No Submission Found!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>
</div>