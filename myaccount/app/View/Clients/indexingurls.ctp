<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <?php
        if (isset($cTitle)):
            ?>
            <div id="print_it" class="box">
                <div class="title">
                    <h4>
                        <span><?php echo $cTitle; ?>
                    </h4>
                </div>

                <?php
                //$timeGap = floor((time() - strtotime($indexingReport['Linkindexing']['created'])) / (60 * 60));
                ?>

                <div class="content clearfix">
                    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($indexingReport)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th class="zeroWidth"></th>
                                <th class="textLeft">URL</th>
                                <th>Ping Count</th>
                                <th>Crawl Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($indexingReport) && count($indexingReport) > 0):
                                foreach ($indexingReport as $k => $item):
                                    $arr = $item['Indexingurl'];
                                    ?>
                                    <tr>
                                        <td class="zeroWidth"></td>
                                        <td class="textLeft"><?php echo $arr['url']; ?></td>
                                        <!--
                                        <td><?php echo $timeGap >= 24 ? '&radic;' : ''; ?></td>
                                        <td><?php echo $timeGap >= 24 ? '&radic;' : ''; ?></td>
                                        -->
                                        <td><?php echo $arr['ping_count']; ?></td>
                                        <td><?php echo $arr['crawl_count']; ?></td>
                                    </tr>
                                    <?php
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td colspan="3">No Record Found!</td>
                                </tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
        endif;
        ?>
    </div>
</div>
