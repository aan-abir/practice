<div style="margin-bottom: 20px;"></div>


<div class="page-header">
    <h4> Welcome <?php echo AuthComponent::user('firstname') . ' ' . AuthComponent::user('lastname'); ?>, this is your dashboard </h4>
    <input type="hidden" name="wsetmodified" id="wsetmodified" value="<?php echo $wmsg['Welcomenews']['modified']; ?>">
</div>

<div class="row-fluid">
    <div class="span8" style="text-align: center;">
        <div class="" style="position: relative;">
            <div dir="ltr" class="circle-stats">
                <div id="autoPremium">
                    <?php 

                        echo '<div class="span12" id="wnewsDiv">'; 
                        echo '<div class="span12">'. nl2br($wmsg['Welcomenews']['news']).'</div>'; 
                        if ( $wmsg['Welcomenews']['externallink'] != '' ) {
                            echo '<div style="margin-bottom:30px;" class="clearfix span12"><iframe src="http://www.youtube.com/embed/'.$wmsg['Welcomenews']['externallink'].'" width="100%" height="380" frameborder="0"></iframe></div>';
                        }
                        echo '</div>';

                        echo '<div class="span12"><a href="#" id="wnewsset" style="font-size:14px;">Umm, Hide This News for Now</a></div>'; 
                    ?>

                </div>
            </div>
        </div>
    </div><!-- End .span12 -->
</div>



<div class="row-fluid" style="margin-top: 20px;">
    <div class="span8">
        <div class="row-fluid">
            <div class="span12">
                <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                    <h2 style="color: #074f14;">Page One Engine Events Calendar <span class="help-block-inline">see what's coming up</span></h2>
                </div>
            </div>
        </div>
        <div class="marginB10"></div>

        <div class="box calendar gradient">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-calendar"></span>
                    <span>PageOneEngine Events Calendar</span>
                </h4>
                <!-- <a href="#" class="minimize">Minimize</a> -->
            </div>
            <div class="content noPad">
                <div id="activityLog"></div>
            </div>

        </div><!-- End .box -->
    </div><!-- End .span8 -->

    <div class="span4">
        <h2>Something will be here</h2>
    </div><!-- End .span4 -->
</div><!-- End .row-fluid -->




<script type="text/javascript">
    // document ready function
    $(document).ready(function() {


        // 2015-04-28 05:41:19;1
        //$.removeCookie('welcomeset', { path: '/' });
        var lastModifed = $("#wsetmodified").val();
        if ( typeof  $.cookie("welcomeset") == 'undefined' ) { // works for first time
            var cVal = lastModifed + ';' + '1';
            $.cookie("welcomeset", cVal, { expires : 365 }); // so User Dont want to see it anymore 
        }else{
            // so cookie is set and check if modified date is changed 
            $cok = $.cookie("welcomeset").split(';');
            if ( $cok[0] != lastModifed  ) {
               console.log('New Update Comes in');
               var cVal = lastModifed + ';' + '1';
               $.cookie("welcomeset", cVal, { expires : 365 }); // so User Dont want to see it anymore 
            }
            else{
               console.log("No New News ");    
            }
        }
        // Client click to the link and wants to change his decision...if not showing then wants to see and if showing then set to not showing
        $("#wnewsset").on('click',function(e){
            $cok = $.cookie("welcomeset").split(';');
            if ( $cok[1] == 1 ) {
               $.cookie("welcomeset", lastModifed + ';' + '-1', { expires : 365 }); // so User Dont want to see it anymore 
               $("#wnewsDiv").hide();
                $("#wnewsset").html("Okay, Show Welcome News"); 
            }else{
               $.cookie("welcomeset", lastModifed + ';' + '1', { expires : 365 }); // so User Dont want to see it anymore 
               $("#wnewsDiv").show();
                $("#wnewsset").html("Umm, Hide Welcome News"); 
            }
            
            /*if ( $.cookie("welcomeset") ) {
                if ( $.cookie("welcomeset") == '-1' ){
                    $.cookie("welcomeset", "1", { expires : 365 }); // so User Now WANT to see it again
                    $("#wnewsDiv").show();
                }else{
                    $.cookie("welcomeset", "-1", { expires : 365 }); // so User Dont want to see it anymore
                    $("#wnewsDiv").hide();
                }
            }else{ // this will work only first time
                $.cookie("welcomeset", "-1", { expires : 365 }); // so User Dont want to see it anymore
                $("#wnewsDiv").hide();
            }*/
        });
        // Set link text with link so that user can change his mind
        if ( $.cookie("welcomeset") ) {
            $cok = $.cookie("welcomeset").split(';');
            if ( $cok[1] == '-1' ){
                $("#wnewsset").html("Okay, Show Welcome News"); 
                $("#wnewsDiv").hide();
            }else{
                $("#wnewsset").html("Umm, Hide Welcome News"); 
                $("#wnewsDiv").show();
            }
        }
        

        //------------- Full calendar  -------------//
        $(function() {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            //front page calendar
            $('#activityLog').fullCalendar({
                //isRTL: true,
                //theme: true,
                header: {
                    left: 'title,today',
                    center: 'prev,next',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    prev: '<span class="icon24 icomoon-icon-arrow-left-2"></span>',
                    next: '<span class="icon24 icomoon-icon-arrow-right-2"></span>'
                },
                editable: true,
                events: [
                    <?php echo $calData; ?>
                ],
                eventRender: function(event, element) {
                    element.qtip({
                        content: event.description
                    });
                }
            });
        });

        //Boostrap modal
        $('#myModal').modal({show: false});

        //add event to modal after closed
        $('#myModal').on('hidden', function() {
            console.log('modal is closed');
        })


        //circular progrress bar
        $(function() {

            $(".greenCircle").knob({
                'min': 0,
                'max': 100,
                'readOnly': true,
                'width': 80,
                'height': 80,
                'fgColor': '#9FC569',
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true
            })
            $(".redCircle").knob({
                'min': 0,
                'max': 100,
                'readOnly': true,
                'width': 80,
                'height': 80,
                'fgColor': '#ED7A53',
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true
            })
            $(".blueCircle").knob({
                'min': 0,
                'max': 100,
                'readOnly': true,
                'width': 80,
                'height': 80,
                'fgColor': '#88BBC8',
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true
            })

        });
        $("#amclass").css({left: ($("#autoPremium").position().left - 5) + 'px'});
        $("#amclass").css({top: ($("#autoPremium").position().top - 20) + 'px'});
        $(window).resize(function() {
            $("#amclass").css({left: ($("#autoPremium").position().left - 5) + 'px'});
            $("#amclass").css({top: ($("#autoPremium").position().top - 20) + 'px'});
        });





    });

</script>