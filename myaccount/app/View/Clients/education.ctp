<div>

    <?php
    foreach ($sortedData as $category => $topic) {
        //echo '<div class="page-header"><span class="icon12 typ-icon-star blue"></span> '.$category.'</div>';
        //echo ' <h3 class="orange">'.$category.'</h3>';
        ///echo '<div class="marginB10 marginT10" style="border-bottom:1px solid #f1f2f3;"></div>';
        //  echo '<div class="content">';
        foreach ((array) $topic as $k => $t) {
            //echo '<div class="marginB10 marginT10 span10"></div>';
            echo '<div class="span10 clearfix"><h3 class="blue">' . $t['title'] . '</h3></div>';
            echo '<div class="clearfix span10" style="border-bottom: 1px dashed #c4c4c4; border-top: 1px dashed #c4c4c4;padding:5px 0 !important; ">';
            echo '<div class="span2"><span class="icon16  icomoon-icon-clock"></span><span style="color:#999;">' . date('D d, M, Y') . '</span></div>';
            echo '<div class="span5"><span class="icon16   icomoon-icon-stack"></span><span style="color:#999;">' . $category . '</span></div>';
            echo '</div>';
            echo '<div class="span10 marginT10">' . substr(strip_tags($t['body']), 0, 500) . '</div>';
            echo '<div class="span10 center marginT10 paddingB10 marginB10">
                <a class="btn btn-info btn-small" href="' . Router::url('etopics/' . $t['id']) . '" class="bold">Learn More &raquo; </a></div>';
            echo '<div class="marginB10 marginT10 span10" style="border-bottom: 1px solid #f1f2f3;" ></div>';
        }
        echo '<div class="center marginB10 marginT10 span10" style="border-bottom:2px solid #49AFCD;"></div>';
        // echo '<div class="marginB10 marginT10" style="border-bottom:4px solid #f1f2f3;"></div>';
        //  echo '</div>';
    }
    ?>
</div>
