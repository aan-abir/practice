<style type="text/css">
    .textLeft{text-align: left !important;}
    .textRight{text-align: right !important;}
</style>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>My Plan - <?php echo $userInfo['Package']['packagename']; ?></span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>Plan Name</th>
                            <th>Current Extra Credit ($)</th>
                            <th>Details</th>
                            <th>Renew Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($userInfo) && count($userInfo) > 0):
                            ?>
                            <tr>
                                <td><?php echo $userInfo['Package']['packagename']; ?></td>
                                <td><?php echo $userInfo['User']['extra_credit']; ?></td>
                                <td><?php echo $userInfo['Package']['description']; ?></td>
                                <td><?php echo date('d M, Y', strtotime($userInfo['User']['package_renew_date'])); ?></td>
                            </tr>
                            <?php
                            if ($userInfo['User']['high_pr']):
                                $hpType = $userInfo['User']['high_pr_type'];
                                $hpCount = 0;
                                if ($hpType == 'Standard'):
                                    $hpCount = 50;
                                elseif ($hpType == 'Pro'):
                                    $hpCount = 100;
                                endif;
                                ?>
                                <tr>
                                    <td>High PR <?php echo $hpType; ?> (Upgrade)</td>
                                    <td></td>
                                    <td>Access to <?php echo $hpCount; ?> High PR Sites</td>
                                    <td><?php echo $userInfo['User']['high_pr_date'] ? date('d M, Y', strtotime($userInfo['User']['high_pr_date'])) : ''; ?></td>
                                </tr>
                                <?php
                            endif;
                            if ($userInfo['User']['link_density']):
                                ?>
                                <tr>
                                    <td>Link Density (Upgrade)</td>
                                    <td></td>
                                    <td>Access to Link Density Tools</td>
                                    <td><?php echo $userInfo['User']['link_density_date'] ? date('d M, Y', strtotime($userInfo['User']['link_density_date'])) : ''; ?></td>
                                </tr>
                                <?php
                            endif;
                        else:
                            ?>
                            <tr>
                                <td colspan="4">No plan assigned!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->    
</div>
<div class="row-fluid">    
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>My Services</span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th class="textLeft">Service Name</th>
                            <th class="textRight">Unit Price ($)</th>
                            <th class="textRight">Service Limit</th>
                            <th class="textRight">Extra Units</th>
                            <th class="textRight">Consumed Units</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $serviceTotal = 0.00;
                        $serviceCurrentTotal = 0.00;
                        $consumedTotal = 0.00;
                        if (isset($serviceList) && count($serviceList) > 0):
                            foreach ($serviceList as $k => $arr):
                                if ($arr['Service']['servicename']):
                                    ?>
                                    <tr>
                                        <td class="zeroWidth"></td>
                                        <td class="textLeft"><?php echo $arr['Service']['servicename']; ?></td>
                                        <td class="textRight"><?php echo $arr['Service']['default_unit_price']; ?></td>
                                        <td class="textRight">
                                            <?php
                                            if (isset($arr['Service']['servicelimit'])) {
                                                echo $arr['Service']['servicelimit'] != '9999999' ? $arr['Service']['servicelimit'] : 'Unlimited';
                                            } else {
                                                echo '0';
                                            }
                                            ?>
                                        </td>
                                        <td class="textRight"><?php echo isset($arr['Service']['extra_unit']) ? $arr['Service']['extra_unit'] : 0; ?></td>
                                        <td class="textRight"><?php echo isset($arr['Service']['service_used']) ? $arr['Service']['service_used'] : 0; ?></td>
                                    </tr>
                                    <?php
                                endif;
                            endforeach;
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="textRight"><!--strong>Service Total: <?php echo number_format($serviceTotal, 2); ?></strong--></td>
                                <td class="textRight"><!--strong>Consumed Total: <?php echo number_format($consumedTotal, 2); ?></strong--></td>
                            </tr>    
                            <?php
                        else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="5">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div>