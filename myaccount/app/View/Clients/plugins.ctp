<div class="marginB10"></div>
<div class="page-header">
    <h4>Instructions for Plugin </h4>
    <p>Please read carefully to be familiar with how to use our Campaign Plugin in your site.</p>

</div>

<div class="row-fluid">
    <div class="span12">
        <div>
            <ul>
                <li>PHPEmbed Plugin must be installed in your site to manage campaigns.</li>
                <li>For Wordpress, you can download the plugin from <a target="_blank" href="http://tracker.seonitro.com/pluginsupdates/dclwp.zip">here</a>!</li>
                <li>For Joomla, you can download the plugin from <a target="_blank" href="http://tracker.seonitro.com/pluginsupdates/dcljoomla.zip">here</a>!</li>
                <li>Add your campaigns and get the campaign code e.g. [dcl=9]. Insert it in your content as many times you need. The PHPEmbed Plugin will handle the rest.</li>
                <li>For BlogRoll Butane to work, insert the shortcode, [dcl_sidebar] for sidebar and [dcl_footer] for footer in any widget at your sidebar or footer. PHPEmbed Plugin will handle the rest.</li>
                <li>If you don't have footer widget, you can also put the shortcode in your footer html. But for that in wordpress you have to insert the code as &lt;?php echo do_shortcode('[dcl_sidebar]'); ?&gt;</li>
                <li>If you want us to manage PHPEmbed Plugin in your wordpress sites for example to update or activate, deactivate automatically, you have to install another plugin named NitroManager we have for this purpose.</li>
                <li>You can download NitroManager Plugin from <a target="_blank" href="http://tracker.seonitro.com/pluginsupdates/nitromanager.zip">here</a>!</li>
            </ul>
            <h3>Add blogroll shortcode to sidebar widget:</h3>
            <img src="<?php echo DOMAIN; ?>/images/screenshots/blogroll_shortcode_widget.png" /><br/><br/>
            <h3>Add blogroll shortcode to footer html:</h3>
            <img src="<?php echo DOMAIN; ?>/images/screenshots/blogroll_shortcode_rawcode.png" /><br/><br/>
            <h3>Add campaign codes in content:</h3>
            <img src="<?php echo DOMAIN; ?>/images/screenshots/add_campaign_codes.png" /><br/><br/>
        </div>
    </div><!-- End .span6 -->
</div>