<style type="text/css">
    .q_color { color:#3A87AD; }
</style>
<div class="marginB10"></div>
<div class="page-header">
    <h4>Frequently Asked Questions <?php echo isset($cat_name) ? ' - '. $cat_name : ''; ?> </h4>
</div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php
                if (count($faqAllCategories)) {
                    $i = 1;
                ?>
                <div class="categories row-fluid">
                    <?php
                        $fs = isset($params) ? '13px' : '21px' ;
                        foreach ($faqAllCategories as $category) {
                        ?>
                        <div class="span4">
                            <ul>
                                <li>
                                    <span class="icon16 icomoon-icon-folder"></span>
                                    <a style="font-size: <?php echo $fs; ?>;" href="<?php echo Router::url($this->params['action'] . '/' . $category['Faqcategory']['id']); ?>">
                                    <?php echo $category['Faqcategory']['category']; ?></a>
                                    <span class="label label-info"><?php echo $category['Faqcategory']['countFaqs']; ?></span>
                                </li>
                            </ul>
                        </div>
                        <?php
                            if ($i % 3 == 0) {
                            ?>
                        </div>
                        <div class="categories row-fluid">
                            <?php
                            }
                        ?>
                        <?php
                            $i++;
                        }
                    ?>
                </div>
                 <hr>
                <?php
                }
            ?>

            <?php
                if (isset($params) && $params != 0) {
                    if (count($faqAll)) {
                        foreach ($faqAll as $faq) {
                        ?>

                        <div class="answer" style="margin-top: 20px;">
                            <div class="page-header">
                                <h4 class="q_color"><?php echo $faq['Faq']['question']; ?></h4>
                            </div>
                            <div>
                                <?php echo $faq['Faq']['answer']; ?>
                            </div>
                            <!-- <br/>
                            <br/>
                            <div class="info row-fluid">
                            <div class="well">
                            <div class="span4">
                            <a href="#"><span class="icon16 icomoon-icon-printer-2"></span> Print this answer</a>
                            </div> <!-- End span4
                            </div>
                            </div>-->

                        </div><!-- End answer -->

                        <?php
                        }
                    } else {
                    ?>
                    <div class="page-header">
                        <h4>No Question And Answer Found!</h4>
                    </div>
                    <?php
                    }
                }
            ?>

        </div><!-- End span12 -->

    </div><!-- End .row-fluid -->
</div>
<div class="marginB10"></div>