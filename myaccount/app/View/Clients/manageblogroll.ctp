<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Manage Blogroll</span>
                </h4>
            </div>
            <div class="content clearfix">
                <?php echo $this->Form->create('Client', array('novalidate' => true)); ?>
                <?php echo $this->Form->end(); ?>
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allCamps)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Domain Name</th>
                            <th style="text-align: left;">Campaign Name / Anchor</th>
                            <th>Add Link to</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (is_array($allCamps) && count($allCamps) > 0):
                            $i = 1;
                            foreach ($allCamps as $camp):
                                ?>
                                <tr>
                                    <td><?php echo $camp['Domain']['domain']; ?></td>
                                    <td style="text-align: left;">
                                        <?php
                                        if ($camp['Campaign']['custom'] == 1):
                                            echo 'Anchor: ' . @$camp['Campaign']['allAnchors'][0]['Anchortext']['anchortexts'] . '<br/>';
                                            echo 'URL: ' . @$camp['Campaign']['allAnchors'][0]['Anchortext']['internalPageUrl'];
                                        else:
                                            echo $camp['Campaign']['campignname'];
                                        endif;
                                        ?></td>
                                    <td><?php echo ($camp['Blogroll']['setting_option'] == 1) ? "Sidebar" : "Footer"; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <a href="<?php echo Router::url('editblogroll/' . $camp['Blogroll']['id']); ?>" title="Edit Blogroll?" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                            <a href="<?php echo Router::url('deleteblogroll/' . $camp['Blogroll']['id']); ?>" title="Remove Blogroll?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="3">No Blogroll Found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>              
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function submitApplyForm(evt) {
        if (confirm('Are you sure to refresh all the sitewide links?')) {
            $("html").addClass('loadstate');
            $('#ClientManageblogrollForm').submit();
        } else {
            return false;
        }
    }
</script>