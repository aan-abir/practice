<?php
$path = $this->requestAction('/clients/getcategorypath/' . $education['Education']['cat_id']);
$p = '';
$i = 1;
if (count($path)) {
    foreach ($path as $k => $ph) {
        $p .= $ph['Category']['name'] . ($i != count($path) ? " &rArr; " : "");
        $i++;
    }
}
?>
<div class="marginT10"></div>
<div class="content">
    <div class="span10 clearfix"><h3 class="blue"><?php echo preg_replace($replace, $replacement, $education['Education']['title']); ?></h3></div>
    <div class="clearfix span10" style="border-bottom: 1px dashed #c4c4c4; border-top: 1px dashed #c4c4c4;padding:5px 0 !important; ">
        <div class="span2"><span class="icon16  icomoon-icon-clock"></span><span style="color:#999;"><?php echo date('D d, M, Y'); ?> </span></div>
        <div class="span5"><span class="icon16   icomoon-icon-stack"></span><span style="color:#999;"><?php echo $p; ?></span></div>
    </div>
    <div class="span10 marginT10"><?php echo preg_replace($replace, $replacement, $education['Education']['body']); ?></div>
</div>

<div class="marginB10 marginT10 span10"></div>

<div class="content">

    <div class="page-header">
        <h3> See Related Topics </h3>
    </div>

    <?php
    foreach ($sortedData as $category => $topic) {
        //echo '<div class="page-header"><span class="icon12 typ-icon-star blue"></span> '.$category.'</div>';
        echo ' <ul class="unstyled"><li><h3 class="orange">' . $category . '</h3>';
        echo '<ul>';
        foreach ((array) $topic as $k => $t) {
            echo '<li><span class="icon12 minia-icon-checkmark red"></span><a href="' . Router::url('etopics/' . $t['id']) . '" class="bold">' . $t['title'] . '</a></li>';
        }
        echo '</ul></li></ul>';
        echo '<div class="marginB10 marginT10" style="border-bottom:4px solid #f1f2f3;"></div>';
    }
    ?>        
</div>

<script type="text/javascript">
    $(".fancypdf").click(function() {
        $.fancybox({
            type: 'html',
            autoSize: false,
            content: '<embed src="' + this.href + '#nameddest=self&page=1&view=FitH,0&zoom=80,0,0" type="application/pdf" height="99%" width="100%" />',
            beforeClose: function() {
                $(".fancybox-inner").unwrap();
            }
        }); //fancybox
        return false;
    }); //click

</script>