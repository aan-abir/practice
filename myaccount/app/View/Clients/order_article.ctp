<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span12', 'autofocus'); ?>
<div class="content">
    <?php echo $this->Form->create('Client', array('inputDefaults' => $inputDefaults, 'novalidate' => true)); ?>
    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Article Information</h4>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="title">Article Title</label>
                <div class="span8">
                    <?php echo $this->Form->input('Article.title', array('error' => false, 'type' => 'text', 'id' => 'title', 'title' => 'title of article', 'placeholder' => "enter title here", 'div' => false, 'label' => false, 'required' => true, 'class' => 'span12 tip')); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="ArticleCategory">Select Category  <span class="help-block-inline">select a category that the article related to</span></label>
                <div class="span8">
                    <?php
                        echo $this->Form->select('Article.category', $allCategs, array('error' => false, 'id' => 'ArticleCategory', 'required' => true, 'div' => false, 'class' => 'span2 select', 'empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="ArticleArtLen">Article Length</label>
                <div class="span8">
                    <?php
                        echo $this->Form->select('Article.art_len', array(150 => '150', 300 => '300', 400 => '400', 500 => '500', 700 => '700', 1000 => '1000', 2000 => '2000'), array('error' => false, 'id' => 'ArticleArtLen', 'required' => false, 'div' => false, 'class' => 'span2 select', 'empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Article Keywords <span class="help-block-inline">comma (,) separated, if multiple </span></h4>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="form-row">
                <?php echo $this->Form->input('Article.keywords', array('error' => false, 'type' => 'text', 'id' => 'title', 'title' => 'enter keywords', 'placeholder' => "enter keywords", 'div' => false, 'label' => false, 'required' => true, 'class' => 'span12 tip')); ?>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Article Writing Style <span class="help-block-inline">select which style you prefer</span></h4>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="form-row">
                <?php
                    echo $this->Form->select('Article.writing_style', array(1 => 'Friendly Tone', 2 => 'Professional Tone'), array('error' => false, 'id' => 'ArticleWritingStyle', 'required' => false, 'div' => false, 'class' => 'span2 select', 'empty' => false));
                ?>
            </div>
        </div>
    </div>



    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Article Purpose <span class="help-block-inline">explain the purpose of the article</span></h4>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="form-row">
                <?php echo $this->Form->textarea('Article.art_purpose', array('error' => false, 'id' => 'purpose', 'div' => false, 'label' => false, 'class' => 'span12', 'rows' => 2, 'cols' => 20) ); ?>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Special Instructions <span class="help-block-inline">add additional special instructions, if any</span></h4>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="form-row">
                <?php echo $this->Form->textarea('Article.special_instructions', array('error' => false, 'id' => 'purpose', 'div' => false, 'label' => false, 'class' => 'span12', 'rows' => 2, 'cols' => 20) ); ?>
            </div>
        </div>
    </div>



    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Base Article <span class="help-block-inline">enter base article for writers to follow, if desired</span></h4>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="form-row">
                <?php echo $this->Form->textarea('Article.base_article', array('error' => false, 'id' => 'purpose', 'div' => false, 'label' => false, 'class' => 'span12', 'rows' => 6, 'cols' => 20) ); ?>
            </div>
        </div>
    </div>

    <div class="marginB10"></div>
    <div class="form-actions">
        <button class="btn btn-info offset2" type="submit">Order Article</button>
    </div>
    <?php $this->Form->end(); ?>
</div>






<div class="marginB10"></div>

<div class="box">
    <div class="title"><h4><span>Ordered Articles</span></h4></div>
    <div class="content">
        <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($itemList)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
            <thead>
                <tr>
                    <th class="zeroWidth"></th>
                    <th class="textLeft">Title</th>
                    <th class="textLeft">Article</th>
                    <th class="textLeft">Keywords</th>
                    <th class="textLeft">Article Purpose</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if (isset($itemList) && count($itemList) > 0):
                        foreach ($itemList as $item):
                            $arr = $item['Article'];
                        ?>
                        <tr>
                            <td class="zeroWidth"></td>
                            <td class="textLeft"><?php echo $arr['title']; ?></td>
                            <td class="textLeft"><?php echo $arr['body']; ?></td>
                            <td class="textLeft"><?php echo $arr['keywords']; ?></td>
                            <td class="textLeft"><?php echo $arr['art_purpose']; ?></td>
                            <td>
                                <?php
                                    echo $arr['proj_id'] ? 'P:' . $arr['proj_id'] : '';
                                    echo '<br/>';
                                    echo $arr['proj_id'] ? 'A:' . $arr['article_id'] : '';
                                    echo '<br/>';
                                    if ($arr['keyword_status'] == 'available'):
                                    ?>
                                    <a href="<?php echo $arr['review_url'] ? $arr['review_url'] : '#'; ?>" target="_blank" title="Click to review the Article" class="tip">Review</a><br/>
                                    <a href="<?php echo Router::url('approve_article/' . $arr['id']); ?>" title="Clieck to approve the Article" class="tip callAction">Approve</a>
                                    <?php
                                        else:
                                        echo strtoupper($arr['keyword_status']);
                                        endif;
                                ?>
                            </td>
                            <td>
                                <?php if ($arr['requested'] == 1): ?>
                                    <a href="<?php echo Router::url('order_article/' . $arr['id']); ?>" title="Edit Order" class="tip">Edit</a>&nbsp;&nbsp;
                                    <a href="<?php echo Router::url('confirm_iw_order/' . $arr['id']); ?>" title="Confirm Order" class="tip callAction">Confirm</a>&nbsp;&nbsp;
                                    <a href="<?php echo Router::url('delitem/' . $arr['id'] . '/article'); ?>" title="Cancel Order" class="tip callAction">Cancel</a>
                                    <?php endif; ?>
                            </td>
                        </tr>
                        <?php
                            endforeach;
                        else:
                    ?>
                    <tr>
                        <td class="zeroWidth"></td>
                        <td colspan="6"><?php echo NO_RECORD_FOUND; ?></td>
                    </tr>
                    <?php
                        endif;
                ?>
            </tbody>
        </table>
    </div>
</div>
