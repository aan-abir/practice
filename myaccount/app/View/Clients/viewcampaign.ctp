<div class="submenu_tab clearfix">
    <ul>
        <li><a href="<?php echo Router::url('editcampaign/' . $campaignInfo['Campaign']['id']); ?>">Edit Campaign</a></li>
    </ul>
</div>

<div class="marginB10"></div>
<?php
if (isset($campaignInfo)):
    ?>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span>Campaign Details: <?php echo $campaignInfo['Campaign']['campignname']; ?> [dcl=<?php echo $campaignInfo['Campaign']['id']; ?>]</span>
                    </h4>
                </div>
                <div class="content clearfix">
                    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($anchorList)) { ?> dynamicTable <?php } ?> display table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th class="zeroWidth"></th>
                                <th class="textLeft">Anchor Text</th>
                                <th class="textLeft">Anchor Link</th>
                                <th class="textLeft">No Follow</th>
                                <th>Target % [ceil]</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($anchorList):
                                foreach ($anchorList as $k => $arr):
                                    ?>
                                    <tr>
                                        <td class="zeroWidth"></td>
                                        <td class="textLeft"><?php echo $arr[0]; ?></td>
                                        <td class="textLeft"><?php echo $arr[1]; ?></td>
                                        <td class="textLeft"><?php echo $arr[2] ? 'Yes' : ''; ?></td>
                                        <td><?php echo ceil($arr[3]); ?></td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span6 -->
    </div>
    <?php
endif;
?>
