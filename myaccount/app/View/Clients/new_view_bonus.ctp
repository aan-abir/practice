<div class="page-header">
    <h1><?php echo $item['Bonus']['title']; ?> <span class="help-block-inline" style="font-size: 18px;"></span></h1>

    <p>
        <?php echo $item['Bonus']['content']; ?>
    </p>
</div>