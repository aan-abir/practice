<?php include_once 'not_eligible_warning.ctp'; ?>
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'manualnetworks'), 'method' => 'post')); ?>

    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($manualsites)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
        <thead>
            <tr>
                <th style="width: 20px;"></th>
                <th style="text-align: left;">Domain</th>
                <th>Theme/Title</th>
                <th>PR</th>
                <th title="Link Juice Pr" class="tip">L.J PR</th>
                <th title="Citation Flow" class="tip">C.Flow</th>
                <th title="Trust Flow" class="tip">T.Flow</th>
                <th title="Analyze this Doamin" class="tip">Analyze</th>
                <th title="UserName" class="tip">U.Name</th>
                <th title="Password" class="tip">Pass</th>
                <th title="Actions" class="tip">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($manualsites) > 0):
                foreach ($manualsites as $ms):
                    $proxyUrl = Router::url(array('action' => 'adminproxy:allowed@' . $ms['Domain']['domain']));
                    $u = str_replace('http://', '', $ms['Domain']['domain']);
                    $Url = 'http://www.' . $u;
                    http://adminproxy:allowed@www.unionhillpizza.com/wp-admin
                    ?>
                    <tr>
                        <td><input type="checkbox" name="data[Checked][<?php echo $ms['Domain']['id']; ?>]" /></td>
                        <td style="text-align: left;"><a href="<?php echo $Url; ?>" target="_blank"><?php echo $ms['Domain']['domain']; ?></a></td>
                        <td><?php echo $ms['Domain']['title']; ?></td>
                        <td><?php echo $ms['Domain']['pr']; ?></td>
                        <td><?php echo $ms['Domain']['ljpr']; ?></td>
                        <td><?php echo $ms['Domain']['cflow']; ?></td>
                        <td><?php echo $ms['Domain']['tflow']; ?></td>
                        <td>
                            <a title="Quick Summery of - <?php echo $ms['Domain']['domain']; ?> " href="<?php echo Router::url('quickviewdomain/' . $ms['Domain']['id']); ?>" class="openModalDialog">quick view</a>
                        </td>
                        <td><?php echo $ms['Manualsharedsite']['authusername']; ?></td>
                        <td><?php echo $ms['Manualsharedsite']['authpassword']; ?></td>
                        <td>
                            <?php
                            if ($ms['Manualsharedsite']['authusername']):
                                ?>
                                <a href="<?php echo "http://adminproxy:allowed@www." . $u . "/wp-login.php"; ?>" title="Login to this domain admin panel now" class="tip" target="_blank">get in</a>
                                <?php ?>

                                <?php
                                if (AuthComponent::user('id') == 2):
                                //echo ' | <a href="' . Router::url('reapply/' . $ms['Domain']['id']) . '" title="If you found any trouble to login then just re-apply to get new access" class="tip" target="_blank">reapply</a>';
                                endif;
                            endif;
                            ?>
                        </td>

                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td colspan="9">No Manual Sites Found!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>

    <div class="form-actions">
        <button class="btn btn-success send-middle"  onclick="submitApplyForm(this);" type="button">
            <span class="icon16 icomoon-icon-checkmark white"></span> Apply for Selected Domains</button>
    </div>
    <?php echo $this->Form->end(); ?>



</div>

<script type="text/javascript">
    //--------------- Popovers ------------------//
    //using data-placement trigger
    $("a[rel=popover]")
            .popover()
            .click(function(e) {
                e.preventDefault()
            })

    //using js trigger
    $("a[rel=popoverTop]")
            .popover({placement: 'top'})
            .click(function(e) {
                e.preventDefault()
            })

    function submitApplyForm(evt) {
        if (confirm('Are you sure to apply for the selected sites?')) {
            $("html").addClass('loadstate');
            $('#UserManualnetworksForm').submit();
        } else {
            return false;
        }
    }
</script>
