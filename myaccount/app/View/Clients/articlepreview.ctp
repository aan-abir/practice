<style>
    .textBlue { color: #3A87AD;}
</style>
<?php
if (isset($article)):
    for ($i = 1; $i <= 50; $i++):
        ?>
        <h3 style="color: red;">Spun Article Version: <?php echo $i; ?></h3>
        <div style="text-align: left; margin-bottom: 30px;">
            <h2 class="textBlue"><?php echo spinContent($article['title']); ?></h2>    
            <div><?php echo spinContent($article['content']); ?></div>
        </div>
        <?php
    endfor;
endif;

function spinContent($txt) {
    $pattern = '#\{([^{}]*)\}#msi';
    $test = preg_match_all($pattern, $txt, $out);
    if (!$test)
        return $txt;
    $atrouver = array();
    $aremplacer = array();
    foreach ($out[0] as $id => $match) {
        $choisir = explode("|", $out[1][$id]);
        $atrouver[] = trim($match);
        $aremplacer[] = trim($choisir[rand(0, count($choisir) - 1)]);
    }
    $reponse = str_replace($atrouver, $aremplacer, $txt);
    return spinContent($reponse);
}
?>