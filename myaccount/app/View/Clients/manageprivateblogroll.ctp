<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Domain Name</th>
                <th>Campaign Name</th>
                <th>Add Link to</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (is_array($allCamps) && count($allCamps) > 0):
                $i = 1;
                foreach ($allCamps as $camp):
                    ?>
                    <tr>
                        <td><?php echo $camp['Domain']['id']; ?></td>
                        <td><?php echo $camp['Domain']['domain']; ?></td>
                        <td><?php echo $camp['Campaign']['campignname']; ?></td>
                        <td><?php echo ($camp['Blogroll']['setting_option'] == 1) ? "Sidebar" : "Footer"; ?></td>
                        <td>
                            <div class="controls center">
                                <a href="<?php echo Router::url('editprivateblogroll/' . $camp['Blogroll']['id']); ?>" title="Edit Private Blogroll" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                <a href="<?php echo Router::url('deleteprivateblogroll/' . $camp['Blogroll']['id']); ?>" title="Remove Private Blogroll?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $i++;
                endforeach;
            else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="5">No Blogroll Found!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>
</div>
<div id="modal" title="How to Get and Use Code into your site" class="dialog">
    <h4>Get and Use Code in Wordpress Sites</h4>
    <p>To use dynamically controlled linking in wordpress sites you must have to use our strong and secured plugin.
        <a class="btn btn-link" href="http://tracker.seonitro.com/wpplugins/dclwp.zip" target="_blank"> Click here </a> to get the plugin.
        Once you have the plugin follow the steps below  - </p>
    <ul>
        <li>Install the plugin. You can install it in 2 ways -
            <ul>
                <li> Via FTP -
                    <ul>
                        <li> Unzip the file you just downlaoded (dclwp.zip)</li>
                        <li> Login your site via FTP and Navigate your site folder and go to /wp-contents/plugins/ dir</li>
                        <li> Upload the unzipped files into this folder</li>
                        <li> Go to wp-admin > plugins > find the plugin "SeoNitro Dynamically Controlled Linking" </li>
                        <li> Activate it</li>
                    </ul>
                </li>
                <li> Via wp admin panel directly
                    <ul>
                        <li> Login to admin panel </li>
                        <li> Go to plugins section from left navigation </li>
                        <li> Click add new and at the top click on upload </li>
                        <li> Browse the attached dclwp.zip file and press the upload button and then activate it. </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>Once the plugin is installed go to any post or page or sidebar (widgets) and put the code showing under "Get Code" heading ( like [dcl=10] )</li>
    </ul>


    <h4>Get and Use Code in Joomla Sites</h4>
    <p>To use dynamically controlled linking in Joomla sites you must have to use our strong and secured plugin.
        <a class="btn btn-link" href="http://tracker.seonitro.com/joomlaplugins/scljoomla.zip" target="_blank"> Click here </a> to get the plugin.
        Once you have the plugin downloaded follow the steps below  - </p>
    <ul>
        <li>	From the backend of your Joomla site (administration) select Extensions -> Extension Manager -> Install. </li>
        <li>	Click the Browse button and select the extension package on your local machine. </li>
        <li>	Click the Upload File & Install button. </li>
        <li>	Some extensions may provide further instructions on installation. </li>
        <li>	Note that modules and plug-ins must be enabled before they will work. </li>
        <li>	To enable uploaded plug-in select Extensions -> Plug-in Manager -> Select the plug-in just uploaded </li>
        <li>	On Details page the first option is Status -> Make it Enabled and save. </li>
    </ul>




    <h4>For Static/Dynamic/Custom sites</h4>
    <p>To use dynamically controlled linking in Static/Dynamic/Custom sites you must have to follow the instruction. <br>

        <a class="btn btn-link" href="http://tracker.seonitro.com/joomlaplugins/scljoomla.zip" target="_blank"> Click here </a> to get the plugin.
        Once you have the plugin follow the steps below  - </p>
    <ul>
        <li>Step 1: Download <a class="btn btn-link btn-mini" href="http://tracker.seonitro.com/staticsite/anchorGeneratorFromSeonitro.php.inst" target="_blank"> this file </a> and rename it to anchorGeneratorFromSeonitro.php ( just remove the .inst extension from file name) </li>
        <li>Step 2: Upload the file to root of your website </li>
        <li>Step 3: if you have a common header file for all pages then put this code into that file - <br> &lt;?php include $_SERVER['DOCUMENT_ROOT'].'/anchorGeneratorFromSeonitro.php'; ?&gt;. If you dont have a common header for all pages then put it on top of that page in which you want to replace the keywords  </li>
        <li>
            Once the file is uploaded and you paste the code into desired page use replacing code for example: [dcl=9] where you want to appear the anchor texts.
        </li>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        $('.openModalDialog').click(function() {
            $('#modal').dialog('open');
            return false;
        });


        // JQuery UI Modal Dialog
        $('#modal').dialog({
            autoOpen: false,
            modal: true,
            dialogClass: 'dialog',
            buttons: {
                "Close": function() {
                    $(this).dialog("close");
                }
            }
        });

        $("div.dialog button").addClass("btn");

    });

</script>