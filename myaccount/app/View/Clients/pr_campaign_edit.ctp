<div class="marginB10"></div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'pr_campaign_edit',$data['Prcampaign']['id']), 'id' => 'addRankCampaignForm', 'method' => 'post', 'type'=>'file', 'class' => 'form-horizontal seperator')); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="campaign_name">Campaign Name:</label>
                        <?php echo $this->Form->input('Prcampaign.name', array('error' => false, 'required' => false, 'id' => 'campaign_name', 'type' => 'text', 'title' => 'Campaign Name', 'placeholder' => "Campaign Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        
                    </div>
                </div>
            </div>
            
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="c_urls">Enter domain(s) one per line:</label>
                        <div class="span4">
                            <?php echo $this->Form->textarea('Prcampaign.c_urls', array('error' => false, 'required' => false, 'id' => 'c_urls', 'title' => 'enter urls you want to track under this campaign', 'div' => false, 'label' => false, 'class' => 'span12', 'style' => 'height:220px')); ?>
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" onclick="$('html').addClass('loadstate');" class="btn btn-info marginR10">Update PR Campaign</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>

</div>

</div>


