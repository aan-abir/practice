<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Submission Report <span class="help-block-inline">Listed below are your article submissions. Click on "View Links" to view links for selected campaign. </span>
                </h4>
            </div>
        </div>
    </div>
</div>
<div class="marginB10"></div>


<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="content">
            <table cellpadding="0" cellspacing="0" border="0"
                   class="responsive <?php if (count($submissionInfo)) { ?> dynamicTable <?php } ?>display table table-bordered"
                   width="100%">
                <thead>
                <tr>
                    <th class="zeroWidth"></th>
                    <th class="textLeft">Name</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Campaign Name</th>
                    <th>Quantity</th>
                    <th>Submitted</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (isset($submissionInfo) && count($submissionInfo) > 0):
                    foreach ($submissionInfo as $arr):
                        ?>
                        <tr>
                            <td class="zeroWidth"></td>
                            <td class="textLeft"><?php echo $arr['Submission']['submissionname']; ?></td>
                            <td><?php echo $arr['Submission']['startdate']; ?></td>
                            <td><?php echo $arr['Submission']['enddate']; ?></td>
                            <td>
                                <?php
                                if (count($arr['Submission']['postcampaigns'])):
                                    $postCampaigns = $arr['Submission']['postcampaigns'];
                                    foreach ($postCampaigns as $k => $pC):
                                        ?>
                                        <a href="<?php echo Router::url('viewcampaign/' . $k); ?>"><?php echo $pC; ?></a>
                                    <?php
                                    endforeach;
                                endif;
                                ?>
                            </td>
                            <td><?php echo $arr['Submission']['howmanytopost']; ?></td>
                            <td><?php echo $arr['Submission']['totalsubmitted']; ?></td>
                            <td>
                                <a href="<?php echo Router::url('submissionlinks/' . $arr['Submission']['id']); ?>"
                                   class="">View Links</a>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                else:
                    ?>
                    <tr>
                        <td class="zeroWidth"></td>
                        <td colspan="7">No Submission Found!</td>
                    </tr>
                <?php
                endif;
                ?>
                </tbody>
            </table>
        </div>
    </div>

</div>

<script type="text/javascript">
    function submitApplyForm(evt) {
        if (confirm('Are you sure to refresh campaign links?')) {
            $("html").addClass('loadstate');
            $(evt).parent('form').submit();
        } else {
            return false;
        }
    }
</script>