<div class="marginB10"></div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'pr_user_proxy_create'), 'id' => 'addRankCampaignForm', 'method' => 'post', 'type'=>'file', 'class' => 'form-horizontal seperator')); ?>

            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                        <h4 style="color: #074f14;">Enter Proxy Information <span class="help-block-inline">enter your own proxy details</span></h4>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="span4" for="ip_address">IP address <span class="help-block-inline">enter proxy IP</span></label>
                        <div class="span8">
                            <?php echo $this->Form->input('Pruserproxy.ip_address', array('error' => false, 'required' => false, 'id' => 'ip_address', 'type' => 'text', 'title' => 'IP address', 'placeholder' => "ip", 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="span4" for="username">Username <span class="help-block-inline">enter proxy username</span></label>
                        <div class="span8">
                            <?php echo $this->Form->input('Pruserproxy.username', array('error' => false, 'required' => false, 'id' => 'username', 'type' => 'text', 'title' => 'username', 'placeholder' => "Username", 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="span4" for="password">Password <span class="help-block-inline">enter proxy password</span></label>
                        <div class="span8">
                            <?php echo $this->Form->input('Pruserproxy.password', array('error' => false, 'required' => false, 'id' => 'password', 'type' => 'text', 'title' => 'password', 'placeholder' => "Password", 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="span4" for="port">Port <span class="help-block-inline">enter proxy Port</span></label>
                        <div class="span8">
                            <?php echo $this->Form->input('Pruserproxy.port', array('error' => false, 'required' => false, 'id' => 'port', 'type' => 'text', 'title' => 'port', 'placeholder' => "Port", 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" class="btn btn-info marginR10">Save Your Proxy</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>

</div>

</div>


