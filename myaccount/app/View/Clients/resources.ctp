<div class="marginB10"></div>
<div class="page-header">
    <h4>SEORockStars Resources</h4>
    <p>Click on the colored buttons below to access your purchased resources. Items listed in gray are available for purchase (just click to reach the shopping cart).</p>
</div>

<div class="content">
    <div class="row-fluid">
        <?php $p = array('21' => 'SEORockstars Recordings', '22' => 'SEORockstar NOTES', '23' => 'SEORockstars 2012/13 Recordings');

            $boughts = array_keys($user);


            // has bought 21 and 23
            if ( key_exists(21, $user) && key_exists(23, $user) ) {
                echo '<a style="margin-right:40px; height:90px; width:560px;font-size:30px; font-weight:bold; line-height:90px;" class="btn btn-success" href="/myaccount/clients/rday/21/"> SEORockstars Recordings >> </a><br>';

                echo '<a style="margin-right:40px; margin-top:40px; margin-top:30px; height:90px; width:560px;font-size:30px; font-weight:bold; line-height:90px;background:#ff4455;"   class="btn btn-success" href="/myaccount/clients/rday/23/"> SEORockstars 2012/13 Recordings >> </a>';
            }
            // bought SEORockstars Recordings ONLY
            else if ( key_exists(21, $user) && ( !key_exists(22, $user) && !key_exists(23, $user) ) ) {
                echo '<a style="margin-right:40px; height:90px; width:560px;font-size:30px; font-weight:bold; line-height:90px;" class="btn btn-success" href="/myaccount/clients/rday/21/"> SEORockstars Recordings >> </a><br>';

                echo '<a title="Buy this 2012, 2013 videos and notes now"  style="margin-right:40px; line-height:80px; margin-top:30px; height:80px; width:560px;font-size:30px; font-weight:bold;"  target="_blank" class="btn btn-inverse tip" href="http://www.1shoppingcart.com/SecureCart/SecureCart.aspx?mid=7F7F0445-14CE-4D0A-AD57-AF078123DFE6&pid=2334f53dae2d4a2c8d29653fb5b0914e&bn=1"> <span class="icon16 icommon-icon-cancel-2 white"></span>SEORockstars 2012/13 Recordings </a>';
            }
            // bought SEORockstars 2012/13 Recordings ONLY
            else if ( key_exists(23, $user) && ( !key_exists(21, $user) && !key_exists(22, $user) ) ) {
                echo '<a style="margin-right:40px; margin-top:40px; height:90px; width:560px;font-size:30px; font-weight:bold; line-height:90px;background:#ff4455;"   class="btn btn-success" href="/myaccount/clients/rday/23/"> SEORockstars 2012/13 Recordings >> </a><br>';

                echo '<a title="Buy 2014 videos, mindMaps, SlideShows and notes  now"  style="margin-right:40px;margin-top:30px; line-height:80px; height:80px; width:560px;font-size:30px; font-weight:bold; line-height:90px; margin-top:40px"  target="_blank" class="btn btn-inverse tip" href="http://www.1shoppingcart.com/SecureCart/SecureCart.aspx?mid=7F7F0445-14CE-4D0A-AD57-AF078123DFE6&pid=e6919aa754094a77919a3efe19190d46&bn=1"> <span class="icon16 icommon-icon-cancel-2 white"></span>SEORockstars Recordings </a>';
            }

            // bought SEORockstar NOTES ONLY
            else if ( key_exists(22, $user) && ( !key_exists(21, $user) && !key_exists(23, $user) ) ) {
                echo '<a style="margin-right:40px; height:90px; width:560px;font-size:30px; font-weight:bold; line-height:90px;" class="btn btn-success" href="/myaccount/clients/rday/22/"> SEORockstar NOTES >> </a><br>';

                echo '<a title="Buy this 2012, 2013 videos and notes now"  style="margin-right:40px;margin-top:30px; line-height:80px; height:80px; width:560px;font-size:30px; font-weight:bold;"  target="_blank" class="btn btn-inverse tip" href="http://www.1shoppingcart.com/SecureCart/SecureCart.aspx?mid=7F7F0445-14CE-4D0A-AD57-AF078123DFE6&pid=2334f53dae2d4a2c8d29653fb5b0914e&bn=1"><span class="icon16 icommon-icon-cancel-2 white"></span>SEORockstars 2012/13 Recordings </a>';
            }
            // bought SEORockstar NOTES ONLY

            else  if ( key_exists(22, $user) && key_exists(23, $user) && !key_exists(21, $user) ) {
                echo '<a style="margin-right:40px; height:90px; width:560px;font-size:30px; font-weight:bold; line-height:90px;" class="btn btn-success" href="/myaccount/clients/rday/22/"> SEORockstars Notes >> </a><br>';

                echo '<a style="margin-right:40px; margin-top:40px;margin-top:30px; height:90px; width:560px;font-size:30px; font-weight:bold; line-height:90px;background:#ff4455;"   class="btn btn-success" href="/myaccount/clients/rday/23/"> SEORockstars 2012/13 Recordings >> </a>';
            }
            else  if ( key_exists(22, $user) && key_exists(21, $user) && !key_exists(23, $user) ) {
                echo '<a style="margin-right:40px; height:90px; width:560px;font-size:30px; font-weight:bold; line-height:90px;" class="btn btn-success" href="/myaccount/clients/rday/21/"> SEORockstars Recordings >> </a><br>';

                echo '<a style="margin-right:40px;margin-top:30px; height:90px; width:560px;font-size:30px; font-weight:bold; line-height:90px;" class="btn btn-success" href="/myaccount/clients/rday/22/"> SEORockstars Notes >> </a><br>';

                echo '<a title="Buy this 2012, 2013 videos and notes now"  style="margin-top:40px;margin-right:40px;  line-height:80px; height:80px; width:560px;font-size:30px; font-weight:bold;line-height:90px; background:#cccccc; color:#000000;"  target="_blank" class="btn tip" href="http://www.1shoppingcart.com/SecureCart/SecureCart.aspx?mid=7F7F0445-14CE-4D0A-AD57-AF078123DFE6&pid=2334f53dae2d4a2c8d29653fb5b0914e&bn=1"><span class="icon16 icommon-icon-cancel-2 white"></span>SEORockstars 2012/13 Recordings </a>';
            }


            else{
                if ( empty($boughts) ) {
                    echo '<a title="Buy 2014 videos, mindMaps, SlideShows and notes  now"  style="margin-right:40px; line-height:80px; height:80px; width:560px;font-size:30px; font-weight:bold;line-height:90px; background:#cccccc; color:#000000;"  target="_blank" class="btn tip" href="http://www.1shoppingcart.com/SecureCart/SecureCart.aspx?mid=7F7F0445-14CE-4D0A-AD57-AF078123DFE6&pid=e6919aa754094a77919a3efe19190d46&bn=1"> <span class="icon16 icommon-icon-cancel-2 white"></span>SEORockstars Recordings </a><br>';


                    echo '<a title="Buy this 2012, 2013 videos and notes now"  style="margin-top:40px;margin-right:40px; line-height:80px; height:80px; width:560px;font-size:30px; font-weight:bold;line-height:90px; background:#cccccc; color:#000000;"  target="_blank" class="btn tip" href="http://www.1shoppingcart.com/SecureCart/SecureCart.aspx?mid=7F7F0445-14CE-4D0A-AD57-AF078123DFE6&pid=2334f53dae2d4a2c8d29653fb5b0914e&bn=1"><span class="icon16 icommon-icon-cancel-2 white"></span>SEORockstars 2012/13 Recordings </a>';


                }
            }

        ?>

    </div>
 </div>