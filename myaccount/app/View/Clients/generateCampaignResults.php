<?php

connect_db();

generateCampaignResults();

close_db();
exit;

/* =======utility functions start======= */

function connect_db() {
    $dbConnection = mysql_connect('localhost', 'dorifrie_nitrov2', '@-!y8;sAQV)7');
    if (!$dbConnection)
        die('not connected');
    $dB = mysql_select_db('dorifrie_seonitrov2', $dbConnection);
    if (!$dB)
        die('dB not here');
}

function close_db() {
    $dbConnection = mysql_connect('localhost', 'dorifrie_nitrov2', '@-!y8;sAQV)7');
    mysql_close($dbConnection);
}

function result_array($sql) {
    $result = array();
    $query = mysql_query($sql) or die("Error: " . mysql_error());
    while ($data = mysql_fetch_array($query)) {
        $result[] = $data;
    }
    $rows = count($result);
    if ($rows) {
        $total_global_rows = count($result);
        $total_inner_rows = count($result[0]);
        $count_total_inner_rows = $total_inner_rows / 2;

        for ($i = 0; $i < $total_global_rows; $i++) {
            for ($j = 0; $j < $count_total_inner_rows; $j++) {
                unset($result[$i][$j]);
            }
        }
    }
    return $result;
}

function row_array($sql) {
    $result = array();
    $query = mysql_query($sql) or die("Error: " . mysql_error());
    $data = mysql_fetch_assoc($query);
    return $data;
}

function query($sql) {
    mysql_query($sql) or die("Error: " . mysql_error());
}

function row_count($sql) {
    $count = 0;
    $result = mysql_query($sql) or die("Error: " . mysql_error());
    $count = mysql_num_rows($result);
    return (int) $count;
}

function insert($table, $data) {
    foreach ($data as $field => $value) {
        $fields[] = '`' . $field . '`';
        $values[] = "'" . mysql_real_escape_string($value) . "'";
    }
    $field_list = join(', ', $fields);
    $value_list = join(', ', $values);
    $query = "INSERT INTO `" . $table . "` (" . $field_list . ") VALUES (" . $value_list . ")";

    #dumpVar($query);
    mysql_query($query) or die("Error: " . mysql_error());
    return mysql_insert_id();
}

function update($table, $data, $index_array) {
    foreach ($data as $field => $value) {
        $fields[] = sprintf("%s = '%s'", $field, mysql_real_escape_string($value));
    }
    $field_list = join(',', $fields);
    foreach ($index_array as $field2 => $value2) {
        $fields2[] = sprintf("%s = '%s'", $field2, mysql_real_escape_string($value2));
    }
    $where = join(' AND ', $fields2);
    $query = sprintf("UPDATE %s SET %s WHERE %s", $table, $field_list, $where);
    mysql_query($query) or die("Error: " . mysql_error());
    return mysql_affected_rows();
}

function delete($table, $column, $data) {
    $sql_delete = "DELETE FROM `$table` WHERE `$column` = '$data'";
    mysql_query($sql_delete) or die("Error: " . mysql_error());
}

function _d($data) {
    echo '<pre>';
    print_r($data);
}

function pr($data) {
    echo '<pre>';
    print_r($data);
}

function getPlainDomain($url) {
    $url = str_replace('http://', '', strtolower($url));
    $url = str_replace('https://', '', $url);
    $plainDomain = str_replace('www.', '', $url);
    if (strpos($url, '/')) {
        $plainDomain = strstr($url, '/', true);
    }
    return $plainDomain;
}

/* === generate result functions === */

function seoShuffle($items) {

    if ($items) {
        mt_srand(5);
        for ($i = count($items) - 1; $i > 0; $i--) {
            $j = @mt_rand(0, $i);
            $tmp = $items[$i];
            $items[$i] = $items[$j];
            $items[$j] = $tmp;
        }
    }
    return $items;
}

function getContentAnchorArray($anchorList, $count) {
    $arr = array();
    if ($anchorList) {
        foreach ($anchorList as $k => $aL) {
            $anchortext = $aL['anchorLink'];
            $times = @round($count * ( $aL['targetDensity'] / 100 ));
            if ($times > 0) {
                for ($i = 0; $i < $times; $i++) {
                    $arr[] = $anchortext;
                }
            }
        }
        $arr = array_slice($arr, 0, $count);
        $arr = seoShuffle($arr);
    }
    return $arr;
}

function getUniqueIds($dataArr, $fieldName) {
    $reArr = array();
    if (is_array($dataArr) && count($dataArr)) {
        foreach ($dataArr as $dA) {
            $reArr[] = $dA[$fieldName];
        }
        $reArr = array_unique($reArr);
    }
    return $reArr;
}

function getCodeFoundByCid($dataArr, $cId) {
    $reNum = 0;
    if (is_array($dataArr) && count($dataArr)) {
        $foundByCid = 0;
        foreach ($dataArr as $k => $dA) {
            $codeFound = $dA['codefound'] ? $dA['codefound'] : 0;
            if (($dA['campaign_id'] == $cId)) {
                $foundByCid += $codeFound;
            }
        }
        $reNum = $foundByCid;
    }
    return $reNum;
}

function getBlogRollByCid($dataArr, $cId) {
    $reNum = 0;
    if (is_array($dataArr) && count($dataArr)) {
        $foundByCid = 0;
        foreach ($dataArr as $k => $dA) {
            if (($dA['campaign_id'] == $cId)) {
                $foundByCid +=1;
            }
        }
        $reNum = $foundByCid;
    }
    return $reNum;
}

function getAnchorListByCid($anchorList, $cId) {
    $finalList = array();
    if (is_array($anchorList) && count($anchorList)) {
        foreach ($anchorList as $k => $data) {
            if ($data['campaign_id'] == $cId) {

                $tDensity = intval(trim($data['targetDensity']));
                $aText = trim($data['anchortexts']);
                $aNoFollow = $data['nofollow'];
                $aTextArr = (array) explode(",", $aText);
                $uText = trim($data['internalPageUrl']);
                $uTextArr = (array) explode(",", $uText);
                $tDensityEach = $tDensity / count($aTextArr);

                if (($tDensity == '') || ($aText == ''))
                    continue;

                $aCount = count($aTextArr) ? count($aTextArr) : 0;
                $aTextFullArr = array();
                if ($aCount) {
                    $uIndex = 0;
                    for ($i = 0; $i < $aCount; $i++) {
                        if (isset($uTextArr[$uIndex])) {
                            $aTextFullArr[] = $uTextArr[$uIndex];
                        } else {
                            $uIndex = 0;
                            $aTextFullArr[] = $uTextArr[$uIndex];
                        }
                        $uIndex++;
                    }
                }
                // all multiple anchor text with commas
                for ($ii = 0; $ii < count($aTextArr); $ii++) {
                    $temp = array();
                    $noFollow = '';
                    if ($aNoFollow == 1) {
                        $noFollow = ' rel="nofollow" ';
                    }
                    $interPageUrl = isset($aTextFullArr[$ii]) ? $aTextFullArr[$ii] : '#';
                    $temp['campaign_id'] = $cId;
                    $temp['anchortexts'] = isset($aTextArr[$ii]) ? $aTextArr[$ii] : '-';
                    $temp['internalPageUrl'] = $interPageUrl;
                    $temp['targetDensity'] = $tDensityEach;
                    $temp['anchorLink'] = '<a' . $noFollow . ' href="' . 'http://' . str_replace('http://', '', $temp['internalPageUrl']) . '">' . $temp['anchortexts'] . '</a>';
                    $finalList[] = $temp;
                }
            }
        }
    }
    return $finalList;
}

function getCodeFoundByDomainCampaignId($dataArr, $dId, $cId) {
    $reArr = array();
    if (is_array($dataArr) && count($dataArr)) {
        $foundForSidebar = 0;
        $foundForFooter = 0;
        foreach ($dataArr as $k => $dA) {
            if (($dA['domain_id'] == $dId) && ($dA['campaign_id'] == $cId)) {
                if ($dA['setting_option'] == 1) {
                    $foundForSidebar +=1;
                } elseif ($dA['setting_option'] == 2) {
                    $foundForFooter +=1;
                }
            }
        }
        $reArr['sidebar'] = $foundForSidebar;
        $reArr['footer'] = $foundForFooter;
    }
    return $reArr;
}

function getCodeFoundByPingerIdCampaignId($dataArr, $pId, $cId) {
    $reArr = array();
    if (is_array($dataArr) && count($dataArr)) {
        $codeFoundByCid = 0;
        foreach ($dataArr as $k => $dA) {
            if (($dA['pinger_id'] == $pId) && ($dA['campaign_id'] == $cId)) {
                $codefound = $dA['codefound'] ? $dA['codefound'] : 0;
                $codeFoundByCid += $codefound;
                $domain_id = $dA['domain_id'];
            }
        }
        $reArr['pinger_id'] = $codeFoundByCid;
        $reArr['found'] = $codeFoundByCid;
        $reArr['domain_id'] = $domain_id;
    }
    return $reArr;
}

function getPingersByCid($dataArr, $cId) {
    $reArr = array();
    if (is_array($dataArr) && count($dataArr)) {
        foreach ($dataArr as $dA) {
            if ($cId == $dA['campaign_id']) {
                $reArr[] = $dA;
            }
        }
    }
    return $reArr;
}

function generateCampaignResults() {

    //get all the active campaigns
    $allCampaigns = result_array("SELECT id,campignname FROM campaigns WHERE status = 1");

    //get all the anchors
    $allAnchors = result_array("SELECT * from anchortexts ORDER BY targetDensity DESC");

    //get previously generated results if any
    $allTrackedDomains = result_array("SELECT trackeddomains.*, campaigns.campignname, campaigns.user_id FROM trackeddomains JOIN campaigns ON campaigns.id = trackeddomains.campaign_id WHERE trackeddomains.campaign_id != -1");

    //get all the blogrolls
    $allBlogRolls = result_array("SELECT blogrolls.*,domains.domain FROM blogrolls JOIN domains ON domains.id = blogrolls.domain_id");

    //get all the domains
    $allDomains = result_array("SELECT * FROM domains WHERE status = 1");

    //get domain ids from previous results if only used for blogrolls by campaign id -1
    $allMinusDomains = result_array("SELECT domain_id FROM trackeddomains WHERE campaign_id = -1");

    //get all the unique domain id
    $uAllDomainIds = getUniqueIds($allMinusDomains, 'domain_id');

    //initialize the volume array
    $volumeArray = array();

    //loop through the campaigns
    foreach ($allCampaigns as $aC) {
        //campaign id
        $uCI = $aC['id'];

        //get all the blogrolls using this campaign id
        $blogRollCountByCid = getBlogRollByCid($allBlogRolls, $uCI);

        //get all dcl codes found by this campaign id
        $getCodeFoundByCid = getCodeFoundByCid($allTrackedDomains, $uCI);

        //add blogrolls found and dcl codes found to get the total
        $totalCodeFoundByCid = $blogRollCountByCid + $getCodeFoundByCid;

        //get anchor list by this campaign id
        $anchorListByCid = getAnchorListByCid($allAnchors, $uCI);

        //get the generated anchor list counted by the total found codes
        $anchorArrayByCid = getContentAnchorArray($anchorListByCid, $totalCodeFoundByCid);

        //get the volume array ready per campaign id
        $volumeArray[$uCI]['campaign_id'] = $uCI;
        $volumeArray[$uCI]['campaign_anchors'] = $anchorArrayByCid;


        if (is_array($allDomains) && count($allDomains)) {

            //loop through all the domains
            foreach ($allDomains as $k => $aD) {

                //domain id
                $uADI = $aD['id'];

                //initialize a counter
                $aCountByCid = 0;

                //get if any blogrolls added for this domain by this campaign id
                $getCodeFoundByDomainCampaignId = getCodeFoundByDomainCampaignId($allBlogRolls, $uADI, $uCI);

                //blogrolls for sidebar
                $sidebarCount = $getCodeFoundByDomainCampaignId['sidebar'];

                //blogrolls for footer
                $footerCount = $getCodeFoundByDomainCampaignId['footer'];

                $volumeArray[$uCI]['domains'][$k]['sidebar_anchors'] = array();
                $volumeArray[$uCI]['domains'][$k]['footer_anchors'] = array();
                $volumeArray[$uCI]['domains'][$k]['domain_id'] = $uADI;
                $volumeArray[$uCI]['domains'][$k]['campaign_id'] = $uCI;
                $volumeArray[$uCI]['domains'][$k]['sidebar_count'] = $sidebarCount;
                $volumeArray[$uCI]['domains'][$k]['footer_count'] = $footerCount;

                if (($sidebarCount + $footerCount) > 0) {
                    for ($s = 0; $s < $sidebarCount; $s++) {
                        $volumeArray[$uCI]['domains'][$k]['sidebar_anchors'][] = $anchorArrayByCid[$aCountByCid];
                        $aCountByCid++;
                    }
                    for ($f = 0; $f < $footerCount; $f++) {
                        $volumeArray[$uCI]['domains'][$k]['footer_anchors'][] = $anchorArrayByCid[$aCountByCid];
                        $aCountByCid++;
                    }
                }
            }
        }

        $anchorsForPingers = array_slice($anchorArrayByCid, $aCountByCid);

        $volumeArray[$uCI]['pinger_all_anchors'] = $anchorsForPingers;
        $volumeArray[$uCI]['pinger_anchors'] = array();

        $pingersByCid = getPingersByCid($allTrackedDomains, $uCI);

        if (is_array($pingersByCid) && count($pingersByCid)) {
            $aCount = 0;
            foreach ($pingersByCid as $k => $uP) {
                //$results = unserialize($uP['results']);
                //_d($results);

                $codeFound = $uP['codefound'];
                $volumeArray[$uCI]['pinger_anchors'][$k] = $uP;
                $volumeArray[$uCI]['pinger_anchors'][$k]['anchors'] = array();

                for ($a = 0; $a < $codeFound; $a++) {
                    $anchorToSet = isset($anchorsForPingers[$aCount]) ? $anchorsForPingers[$aCount] : '';
                    $volumeArray[$uCI]['pinger_anchors'][$k]['anchors'][$uCI][] = $anchorToSet;
                    $aCount++;
                }
            }
        }
    }


    $domainArr = array();
    $pingerArr = array();
    if (count($volumeArray)) {
        $countD = 0;
        foreach ($volumeArray as $vA) {
            if (isset($vA['domains'])) {
                foreach ($vA['domains'] as $uD) {
                    $domainArr['domains'][$countD] = $uD;

                    $domainId = $uD['domain_id'];
                    foreach ($uD['sidebar_anchors'] as $sA) {
                        $domainArr['domain_anchors'][$domainId]['sidebar_anchors'][] = $sA;
                    }
                    foreach ($uD['footer_anchors'] as $fA) {
                        $domainArr['domain_anchors'][$domainId]['footer_anchors'][] = $fA;
                    }
                    $countD++;
                }

                @ksort($domainArr['domain_anchors']);
            }
        }
        _d($domainArr);
        //exit;

        if ($domainArr['domain_anchors']) {
            foreach ($domainArr['domain_anchors'] as $dId => $dArr) {
                if (!isset($dArr['sidebar_anchors'])) {
                    $dArr['sidebar_anchors'] = array();
                }
                if (!isset($dArr['footer_anchors'])) {
                    $dArr['footer_anchors'] = array();
                }


                $sidebarHtml = '<ul>';
                if (isset($dArr['sidebar_anchors']) && is_array($dArr['sidebar_anchors'])) {
                    foreach ($dArr['sidebar_anchors'] as $sA) {
                        $sidebarHtml .= '<li>' . $sA . '</li>';
                    }
                }
                $sidebarHtml .= '</ul>';

                $dArr['sidebar_html'] = $sidebarHtml;

                //_d($dArr);

                $footerHtml = '<ul>';
                $f = 1;
                if (isset($dArr['footer_anchors']) && is_array($dArr['footer_anchors'])) {
                    foreach ($dArr['footer_anchors'] as $fA) {
                        $liBorder = ($f != count($dArr['footer_anchors'])) ? 'border-right:1px solid gray;' : '';
                        $footerHtml .= "<li style='display:inline;padding:0 3px;margin-left:0;{$liBorder}'>" . $fA . "</li>";
                        $f++;
                    }
                }
                $footerHtml .= '</ul>';

                $dArr['footer_html'] = $footerHtml;

                if (in_array($dId, $uAllDomainIds)) {
                    @update('trackeddomains', array('results' => serialize($dArr)), array('domain_id' => $dId, 'campaign_id' => -1));
                } else {
                    $domainInfo = row_array("SELECT domain FROM domains WHERE id = $dId LIMIT 1");
                    if ($domainInfo) {
                        $domainName = getPlainDomain($domainInfo['domain']);
                        $iData['domain_id'] = $dId;
                        $iData['pinger'] = $domainName;
                        $iData['campaign_id'] = -1;
                        $iData['results'] = serialize($dArr);
                        $iData['created'] = date('Y-m-d G:i:s');
                        $iData['modified'] = date('Y-m-d G:i:s');
                        @insert('trackeddomains', $iData);
                    }
                }
            }
        }

        $countP = 0;
        foreach ($volumeArray as $vA) {
            if (isset($vA['pinger_anchors'])) {
                foreach ($vA['pinger_anchors'] as $pA) {
                    $pingerArr[$countP] = $pA;
                    if (is_array($domainArr) && count($domainArr['domain_anchors'])) {
                        foreach ($domainArr['domain_anchors'] as $d => $dA) {
                            if ($pA['domain_id'] == $d) {
                                $pingerArr[$countP]['domain_anchors'] = $dA;
                            }
                        }
                    }
                    $countP++;
                }
            }
        }
    }
    _d($volumeArray);

    $finalPingerResult = array();
    if (count($pingerArr)) {
        foreach ($pingerArr as $k => $pA) {
            foreach ($pingerArr as $pAInner) {
                if ((($pA['domain_id'] == $pAInner['domain_id']) && ($pA['pinger'] == $pAInner['pinger'])) && $pA['id'] != $pAInner['id']) {
                    $allCampaignAnchors = (array) $pA['anchors'] + (array) $pAInner['anchors'];
                    @ksort($allCampaignAnchors);
                    $pingerArr[$k]['anchors'] = $allCampaignAnchors;
                }
            }
            $finalPingerResult[$k]['id'] = $pA['id'];
            $finalPingerResult[$k]['domain_id'] = $pA['domain_id'];
            $finalPingerResult[$k]['pinger'] = $pA['pinger'];
            $finalPingerResult[$k]['post_id'] = $pA['post_id'];
            $finalPingerResult[$k]['anchors'] = $pA['anchors'];
            //$finalPingerResult[$k]['sidebar_html'] = $sidebarHtml;
            //$finalPingerResult[$k]['footer_html'] = $footerHtml;
        }
    }

    //$pingTime = date('Y-m-d H:i:s');
    //mail('mainulnitro@gmail.com', 'GetCampaignStats', "Testing hello one two three... Run at $pingTime");
    //_d($finalPingerResult);
    if (count($finalPingerResult)) {
        foreach ($finalPingerResult as $fPR) {
            //_d($fPR);
            $index_array['id'] = $fPR['id'];
            $update_data['results'] = serialize($fPR);
            update('trackeddomains', $update_data, $index_array);
        }
    }
}