<div class="marginB10"></div>

<div class="content">
    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($itemList)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
        <thead>
            <tr>
                <th class="zeroWidth"></th>
                <th class="textLeft">Title</th>
                <th class="textLeft">Article</th>
                <th class="textLeft">Keywords</th>
                <th>Date</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if (isset($itemList) && count($itemList) > 0):
                    foreach ($itemList as $item):
                        $arr = $item['Article'];
                    ?>
                    <tr>
                        <td class="zeroWidth"></td>
                        <td class="textLeft"><?php echo $arr['title']; ?></td>
                        <td class="textLeft"><?php echo $arr['body']; ?></td>
                        <td class="textLeft"><?php echo $arr['keywords']; ?></td>
                        <td><?php echo date('d M Y', strtotime($arr['created'])); ?></td>
                        <td><?php echo $arr['status'] ? 'Active' : 'Inactive'; ?></td>
                        <td>
                            <div class="controls center">
                                <!--<a href="<?php echo Router::url('submission/' . $arr['id']); ?>" title="Submission History" class="tip"><span class="icon12 icomoon-icon-grid-view"></span></a>-->
                                <a href="<?php echo Router::url('article/' . $arr['id']); ?>" title="Edit Article" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td class="zeroWidth"></td>
                    <td colspan="6"><?php echo NO_RECORD_FOUND; ?></td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
    </div>