<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Add New FAQ</h4>
    <p>Please enter your required values to add a new faq.</p>
</div>
<div class="content">
    <?php echo $this->Form->create('editFaq', array('url' => array('controller' => 'sadmins', 'action' => 'editfaq', $data['Faq']['id']), 'id' => 'addFAQForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
    <?php echo $this->form->hidden('Faq.id', array('value' => $data['Faq']['id'])); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="client_type">Select Category :</label>
                <div class="span4">
                    <?php
                    echo $this->Form->select('Faq.category_id', $categoryList, array('required' => false, 'title' => 'Select Client Type', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Question  <span class="help-block-inline"> html enabled help text. Be aware about html tag completion</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('Faq.question', array('error' => false, 'required' => false, 'id' => 'description', 'title' => 'FAQ Question', 'div' => false, 'label' => false, 'class' => 'span11 limit elastic', 'rows' => 5, 'cols' => '5', 'style' => 'width:98%;')); ?>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Answer  <span class="help-block-inline"> html enabled help text. Be aware about html tag completion</span></h4>
            </div>
            <div class="form-row">

                <?php echo $this->Form->textarea('Faq.answer', array('error' => false, 'required' => false, 'id' => 'answer', 'title' => 'help guide', 'div' => false, 'label' => false, 'class' => 'span4  tinymce', 'rows' => 3, 'cols' => '5', 'style' => 'width:98%;height:300px;')); ?>
            </div>
        </div>
    </div>                
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="status">FAQ Status:</label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Faq][status]" id="UserStatus1" value="1" <?php echo $data['Faq']['status'] == 1 ? 'checked="checked"' : ''; ?> />
                    Active
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Faq][status]" id="UserStatus0" value="0" <?php echo $data['Faq']['status'] == 0 ? 'checked="checked"' : ''; ?> />
                    Inactive
                </div>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Update FAQ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php $this->Form->end(); ?>
</div>
<script type="text/javascript">

    //--------------- Tinymce ------------------//
    $('textarea.tinymce').tinymce({
        // Location of TinyMCE script
        script_url :  '<?php echo BASEURL; ?>plugins/forms/tiny_mce/tiny_mce.js',

        // General options
        theme : "advanced",
        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        //        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        //        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        //        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "<?php echo BASEURL; ?>css/main.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Replace values for the template plugin
        template_replace_values : {
            username : "SuprUser",
            staffid : "991234"
        }
    });

</script>