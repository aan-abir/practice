<div class="marginB10"></div>
<div class="page-header">
    <h4>Manage All Welcome News</h4>
    <p>the latest news will be shown at the client desktop. All old news only for history</p>

</div>


<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>source</th>
                <th>Last Modified</th>
                <th>Status</th>
                <th>news</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(isset($news) && count($news) > 0):
                     $i = 1;
                    foreach($news as $arr):
                    ?>
                    <tr>
                        <td><?php echo $arr['Welcomenews']['id']; ?></td>
                        <td><?php echo $arr['Welcomenews']['title']; ?></td>
                        <td><?php echo $arr['Welcomenews']['externallink']; ?></td>
                       <td><?php echo date("M d, Y G:i ", strtotime($arr['Welcomenews']['modified'])); ?></td>
                        <td><?php echo $arr['Welcomenews']['status']; ?></td>
                        <td><?php echo  substr(strip_tags($arr['Welcomenews']['news']),0,80); ?></td>
                        <td>
                            <div class="controls center">
                                <?php if ( $i == 1 ) { $i++; ?>
                                <a href="<?php echo Router::url('editwelcomenews/' . $arr['Welcomenews']['id']); ?>" title="Edit News?" class="">edit</a> | 
                                <?php } ?>
                                
                                <a href="<?php echo Router::url('deleteanyrecord/Welcomenews/' . $arr['Welcomenews']['id']); ?>" title="Remove News?" class="tip callAction">delete</a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="7">No Events Found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
</div>
       
