<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Add Welcome Video and/or News</h4>
    <p>you can upload a welcome video or news over here.</p>

</div>


<div class="content">
    <?php
        if(isset($data) && isset($data['Welcomenews'])):
        ?>
        <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'editwelcomenews', $data['Welcomenews']['id']), 'id' => 'addInstructionForm', 'method' => 'post', 'type'=>'file', 'class' => 'form-horizontal seperator')); ?>
        <?php echo $this->form->hidden('Welcomenews.id', array('value' => $data['Welcomenews']['id'])); ?>


        <div class="row-fluid">
            <div class="span12">
                <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                    <h4 style="color: #074f14;">Settings and Embeded File <span class="help-block-inline">upload/enter media files source</span></h4>
                </div>
            </div>
        </div>


        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="youtlink">Embeded Video Link</label>
                    <div>
                        <?php echo $this->Form->input('Welcomenews.externallink', array('error' => false, 'type' => 'text', 'id' => 'youtlink', 'title' => 'External Video Link', 'placeholder' => "External Video Link", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        <span class="help-inline blue">Exp: http://www.youtube.com/watch?v=y_wzGwPWT84</span></div>
                </div>
            </div>
        </div>

        <div class="form-row row-fluid">
            <div class="span12 inline_labels">
                <label class="form-label span3" for="MenuStatus">Status:</label>

                <div class="span7">
                    <?php echo $this->Form->radio('Welcomenews.status', array('1' => 'Active', '0' => 'Inactive'), array('legend' => false, 'default' => '1')); ?>
                </div>
            </div>
        </div>

        <div class="marginB10"></div>

<div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                <h4 style="color: #074f14;">News Title <span class="help-block-inline">enter title</span></h4>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <?php echo $this->Form->input('Welcomenews.title', array('required' => true, 'error' => false, 'type' => 'text', 'id' => 'ntitle', 'title' => 'News Title', 'placeholder' => "news title", 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                <h4 style="color: #074f14;">Enter News Details <span class="help-block-inline">html enabled help text. Be aware about html tag completion.</span></h4>
            </div>
        </div>
    </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="form-row">
                    <?php echo $this->Form->textarea('Welcomenews.news', array( 'error' => false, 'id' => 'news', 'title' => 'News text', 'div' => false, 'label' => false, 'class' => 'span4  tinymce', 'rows' => 3, 'cols' => '5', 'style' => 'width:98%;height:400px;' ) ); ?>
                </div>
            </div>
        </div>


        <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
        <div class="form-actions">
            <button class="btn btn-info send-middle" type="submit">Save News</button>
        </div>

        <?php
            $this->Form->end();
            endif;
    ?>
</div>


<div class="marginB10">
    <?php echo $this->element('tinymce'); ?>
</div>