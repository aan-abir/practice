<div class="marginB10"></div>
<div class="marginB10"></div>
<div class="page-header">
    <h4>Your Service List</h4>

    <p>all the service for clients. Please enter service name and unit price</p>
</div>

<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Service Name</th>
                <th>Default Unit Price</th>
                <th>Description</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if (isset($allServices) && count($allServices) > 0):
                    foreach ($allServices as $k => $arr):
                    ?>
                    <tr>
                        <td><?php echo $arr['Service']['id']; ?></td>
                        <td><?php echo $arr['Service']['servicename']; ?></td>
                        <td><?php echo $arr['Service']['default_unit_price']; ?></td>
                        <td><?php echo $arr['Service']['description']; ?></td>
                        <td><?php echo $arr['Service']['status'] == 1 ? 'Active' : 'Inactive'; ?></td>
                        <td>
                            <div class="controls center">
                                <a href="<?php echo Router::url('editservice/' . $arr['Service']['id']); ?>" title="Edit Service" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="5">No Service Found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
            </div>
       