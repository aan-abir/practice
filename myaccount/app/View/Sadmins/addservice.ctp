<div class="marginB10"></div>
<div class="page-header">
    <h4>Add New Service</h4>
    <p>Please enter your required values to add a new service.</p>
</div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'addservice'), 'id' => 'addPackageForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="servicename">Service Name:</label>
                        <?php echo $this->Form->input('Service.servicename', array('error' => false, 'required' => false, 'type' => 'text', 'title' => 'Service Name', 'placeholder' => "Service Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="servicetype">Service Type:</label>
                        <div class="span2">
                            <?php echo $this->Form->select('Service.servicetype', array('0' => 'Onetime', '1' => 'Monthly'), array('required' => false, 'required' => false, 'title' => 'Select Service Type', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false)); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="default_unit_price">Default Unit Price:</label>
                        <?php echo $this->Form->input('Service.default_unit_price', array('error' => false, 'required' => false, 'type' => 'text', 'id' => 'default_unit_price', 'title' => 'Default Unit Price', 'placeholder' => "Default Unit Price", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    </div>
                </div>
            </div>            
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="textarea">Service Description:</label>
                        <?php echo $this->Form->textarea('Service.description', array('error' => false, 'required' => false, 'title' => 'Service Description', 'div' => false, 'label' => false, 'class' => 'span4 limit elastic', 'rows' => 3, 'cols' => '5')); ?>
                    </div>
                </div>
            </div>                
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="status">Service Status:</label>
                        <div class="left marginT5 marginR10">
                            <input type="radio" name="data[Service][status]" id="UserStatus1" value="1" checked="checked" />
                            Active
                        </div>
                        <div class="left marginT5 marginR10">
                            <input type="radio" name="data[Service][status]" id="UserStatus0" value="0" />
                            Inactive
                        </div>
                    </div>
                </div>
            </div>                
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" class="btn btn-info marginR10">Save Service</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>
</div>