<div class="marginB10"></div>
<div class="page-header">
    <h4>Manage All Events</h4>
    <p>all the events are listed below. you can not edit any events. if needed then delete and add again</p>

</div>


<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>Title</th>
                <th>Start</th>
                <th>End</th>
                <th>url</th>
                <th>Color</th>
                <th>Status</th>
                <th>Desc</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(isset($events) && count($events) > 0):
                    foreach($events as $arr):
                    ?>
                    <tr>
                        <td><?php echo $arr['Events']['title']; ?></td>
                        <td><?php echo date("M d, Y G:i ", strtotime($arr['Events']['starttime'])); ?></td>
                        <td><?php echo date("M d, Y G:i ", strtotime($arr['Events']['endtime'])); ?></td>
                        <td><?php echo $arr['Events']['url']; ?></td>
                        <td><?php echo $arr['Events']['color']; ?></td>
                        <td><?php echo $arr['Events']['status']; ?></td>
                        <td><?php echo  substr(strip_tags($arr['Events']['description']),0,80); ?></td>
                        <td>
                            <div class="controls center">
                                <a href="<?php echo Router::url('editevents/' . $arr['Events']['id']); ?>" title="Edit Events?" class="">edit</a> | 
                                <a href="<?php echo Router::url('delevent/' . $arr['Events']['id']); ?>" title="Remove Events?" class="tip callAction">delete</a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="7">No Events Found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
</div>
       
