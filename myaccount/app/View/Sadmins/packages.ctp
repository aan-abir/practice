<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>All Package List</span>
                             
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>Package Name</th>
                            <th>Price</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(isset($allPackages) && count($allPackages) > 0):
                            foreach($allPackages as $arr):
                                ?>
                                <tr>
                                    <td><?php echo $arr['Package']['packagename']; ?></td>
                                    <td><?php echo $arr['Package']['pvalue']; ?></td>
                                    <td><?php echo $arr['Package']['description']; ?></td>
                                    <td><?php echo $arr['Package']['status'] == 1 ? 'Active' : 'Inactive'; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <a href="<?php echo Router::url('editpackage/' . $arr['Package']['id']); ?>" title="Edit Package" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="5">No Package Found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div>