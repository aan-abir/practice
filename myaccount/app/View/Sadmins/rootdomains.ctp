<?php
    $exp = '';
    $ld1 = $ld10 = $ld30 = $ld60 = $ld90 = 0; 

    if (count($roots) > 0){
        foreach ($roots as $arr){

            $time = strtotime(trim($arr['Commonmodel']['expires_on']));
            $now = time();
            $seconds = $time - $now;
            $m = floor($seconds / (86400*30));
            $days = floor($seconds / 86400);
            $seconds %= (86400*30);
            $d = floor($seconds / 86400);
            $seconds %= 86400;
            $h = floor($seconds / 3600);
            $eIn =  $m . ' Month(s) '.$d.' Day(s) '.$h.' Hour(s) ';
            if ( ($m == 0) && ($d == 0) && ($h > 0) ) {
                $ld1++;
            }
            else if ( ($days > 0) && ($days <= 10)   ) {
                $ld10++;
            }
            else if ( ($days > 10) && ($days <= 30) ) {
                $ld30++;
            }
            else if ( ($days > 30) && ($days <= 60) ) {
                $ld30++; 
            }
            else if ( ($days > 60) && ($days <= 90) ) {
                $ld90++;
            }
        }
    }
    $exp =  '<span class="red bold">'. $ld1 .' </span>   domains   will expire in less then <span class="red bold">Some Hour(s)</span> <br>';
    $exp .= '<span class="red bold">'. $ld10 .' </span> domains will expire in less then <span class="red bold">10 days</span> <br>';
    $exp .= '<span class="red bold">'. $ld30 .' </span> domains will expire in less then <span class="red bold">30 days</span> <br>';
    $exp .= '<span class="red bold">'. $ld60 .' </span> domains will expire in less then <span class="red bold">60 days</span> <br>';
    $exp .= '<span class="red bold">'. $ld90 .' </span> domains will expire in less then <span class="red bold">90 days</span> <br>';

?>
<div class="marginB10"></div>
<div class="page-header">
    <h4>All Root Domains</h4>
    <p>all the root domains listed below. 
        <span class="yellow bold">Notice: DOMAIN expires in - </span><br>
        <span><?php echo $exp; ?></span>
    </p>

</div>

<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th class="tip" title="Name of Root Domain">Domain Name</th>
                <th class="tip" title="IP address of this Domain">IP</th>
                <th class="tip" title="how many domains it has as secondary domain"># 2nd Domain</th>
                <th class="tip" title="Domain Registratin Expires Date">Expires On</th>
                <th class="tip" title="Registrar and Registration Number/Master Account">Registrar / Reg.Num</th>
                <th class="tip" title="Wordpress User name and Password">WP User / Pass</th>
                <th class="tip" title="Ftp Username and Pasword">FTP User / Pass</th>
                <th class="tip" title="Ftp Username and Pasword">Mysql dB / User / Pass</th>
                <th class="tip" title="Current HTTP Status">HTTP Status</th>
                <th width="120" class="tip" title="Take necessary Action from below">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if (count($roots) > 0):
                    foreach ($roots as $arr):
                        $domain = 'http://' . str_replace('http://', '',$arr['Commonmodel']['domain']);
                        $bgc = '';
                        $time = strtotime(trim($arr['Commonmodel']['expires_on']));
                        $now = time();
                        $seconds = $time - $now;
                        if ($seconds < 10){
                            $mm = ' <span style="color:#f23;"> ( Expired )</span>';
                            $bgc = ' class="expire" style="background-color:#d09c92 !important;"';
                        }else{
                            $m = floor($seconds / (86400*30));
                            $days = floor($seconds / 86400);
                            $seconds %= (86400*30);
                            $d = floor($seconds / 86400);
                            $seconds %= 86400;
                            $h = floor($seconds / 3600);
                            $eIn =  $m . ' Month(s) '.$d.' Day(s) '.$h.' Hour(s) ';
                            $mm = '<span style="color:#106e14;font-weight:bold;"> (In '.$m.'m '.$d.'d '.$h.'h ) </span>';
                            if ( ($m == 0) && ($d == 0) && ($h > 0) ) {
                                $bgc = ' class="inexpire" style="background-color:#ee5d06 !important;"';
                                $mm = '<span style="color:#000;font-weight:bold;"> (In '.$h.'h) </span>';
                                $ld1++;
                            }
                            else if ( ($days > 0) && ($days <= 10)   ) {
                                $bgc = ' class="inexpire" style="background-color:#d07e4c !important;"';
                                $mm = '<span style="color:#000;font-weight:bold;"> (In '.$d.'d) </span>';
                                $ld10++;
                            }
                            else if ( ($days > 10) && ($days <= 30) ) {
                                $bgc = ' class="inexpire" style="background-color:#b6876a !important;"';
                                $mm = '<span style="color:#000;font-weight:bold;"> (In '.$d.'d) </span>';
                                $ld30++;
                            }
                            else if ( ($days > 30) && ($days <= 60) ) {
                                $bgc = ' class="inexpire" style="background-color:#f2d408 !important;"';
                                $mm = '<span style="color:#8d6e01;font-weight:bold;"> (In '.$m.'m '.$d.'d) </span>';
                                $ld30++; 
                            }
                            else if ( ($days > 60) && ($days <= 90) ) {
                                $bgc = ' class="inexpire" style="background-color:#f2e408 !important;"';
                                $mm = '<span style="color:#8d6e01;font-weight:bold;"> (In '.$m.' m'.$d.'d) </span>';
                                $ld90++;
                            }
                        }


                        //pr($arr);
                    ?>
                    <tr<?php echo $bgc;?>>
                        <td><a href="<?php echo $domain; ?>" title="Click to load this domain in a new window" class="tip" target="_blank"><?php echo $arr['Commonmodel']['domain']; ?></a></td>
                        <td class="tip" title="IP of this Domain"><?php echo $arr['Commonmodel']['ip']; ?></td>
                        <td>
                            <a class="tip" title="Secondary Domain List OF - <?php echo $arr['Commonmodel']['domain']; ?> " href="<?php echo Router::url('secondarydomain/' . $arr['Commonmodel']['id']); ?>" class="openModalDialog"><?php echo $arr[0]['tD']; ?></a>

                        </td>
                        <td class="tip" title="<?php echo 'Expire in - '. $eIn ; ?>"><?php echo date("M d,Y",strtotime($arr['Commonmodel']['expires_on'])). "<br>$mm"; ?></td>
                        <td style="font-size: 10px !important;"><?php echo $arr['Commonmodel']['registrar'] .' <br> '. $arr['Commonmodel']['regnumber'] ; ?></td>
                        <td style="font-size: 10px !important;"><?php echo $arr['Commonmodel']['ausername'] .' <br> '. $arr['Commonmodel']['apassword'] ; ?></td>
                        <td style="font-size: 10px !important;"><?php echo $arr['Commonmodel']['ftpuser'] .' <br> '. $arr['Commonmodel']['ftppassword'] ; ?></td>
                        <td style="font-size: 10px !important;"><?php echo $arr['Commonmodel']['dbname'] .' <br> '. $arr['Commonmodel']['dbuser'] .' <br> '. $arr['Commonmodel']['dbpassword'] ; ?></td>
                        <td style="font-size: 10px !important;"><?php echo $arr['Commonmodel']['livestatus']; ?></td>
                        <td>
                            <div class="controls center">

                                <a title="Quick Summery of - <?php echo $arr['Commonmodel']['domain']; ?> " href="<?php echo Router::url('quickviewdomain/' . $arr['Commonmodel']['id']); ?>" class="openModalDialog">quick view</a> | 
                                <a title="Quick Summery of - <?php echo $arr['Commonmodel']['domain']; ?> " href="<?php echo Router::url('whoisinfo/' . $arr['Commonmodel']['id']); ?>" class="openModalDialog">whois</a>

                                <br>-------------<br>
                                <a href="<?php echo Router::url('adomaindetails/' . $arr['Commonmodel']['id']); ?>" title="All details about this Root Domain" class="tip">details</a>
                                <br>-------------<br>
                                <a href="<?php echo Router::url('editdomain/' . $arr['Commonmodel']['id']); ?>" title="Edit Domain" class="tip">edit</a> | 
                                <a href="<?php echo Router::url('deletedomain/' . $arr['Commonmodel']['id']); ?>" title="Update Live Status?" class="tip callAction">update live status</a>


                            </div>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="4">No Record Found!</td>
                </tr>
                <?php
                    endif;

            ?>
        </tbody>
    </table>
</div>
