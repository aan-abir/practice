<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>View Credits</span>
                </h4>
            </div>        
            <div class="content noPad clearfix">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($userCredits)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Client Name</th>
                            <th>Package Name</th>
                            <th>Current Extra Credits</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($userCredits) && count($userCredits) > 0):
                            foreach ($userCredits as $arr):
                                ?>
                                <tr>
                                    <td><?php echo $arr['User']['fullname']; ?></td>
                                    <td><?php echo $arr['User']['packagename']; ?></td>
                                    <td><?php echo $arr['User']['extra_credit']; ?></td>
                                    <td><?php echo $arr['User']['status'] == 1 ? 'Active' : 'Inactive'; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <a href="<?php echo Router::url('viewusercredits/' . $arr['User']['id']); ?>" title="View User Credit Details" class="tip">Details</a> | 
                                            <a href="<?php echo Router::url('updateusercredits/' . $arr['User']['id']); ?>" title="Edit User Credit" class="tip">Update</a>
                                        </div>
                                    </td>                                    
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td colspan="4">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- End .span12 -->
</div>
<div id="pagingPmb">
    <?php
    if ($this->Paginator->numbers() != '')
        echo '<em> Pages: </em>';
    if ($this->Paginator->hasPrev())
        echo $this->Paginator->prev(__('Previous '), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers();
    if ($this->Paginator->hasNext())
        echo $this->Paginator->next(__(' Next'), array(), null, array('class' => ' next disabled'));
    ?>
    <span> *<?php echo $this->Paginator->counter('{:count} Total'); ?></span>
</div>