<div class="marginB10"></div>
<div class="page-header">
    <h4>Manage Education Categories</h4>
    <p>add, edit, delete and reordering categories and sub-categories </p>

</div>

<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'managecats'. ( isset($id) ? '/'.$id : '' ) ), 'id' => 'addCatForm', 'method' => 'post', 'type' => 'file',  'class' => 'form-horizontal seperator')); ?>

    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Add A new Category <span class="help-block-inline">enter category related information</span></h4>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="catname">Category Name: <span class="help-block-inline">enter name of the category</span></label>
                <div class="span8">
                    <?php echo $this->Form->input('Category.name', array('error' => false, 'type' => 'text', 'id' => 'catname', 'title' => 'Category Name', 'placeholder' => "category name", 'div' => false, 'label' => false, 'required' => true, 'class' => 'span12 tip')); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="domain">Select Parent Category: <span class="help-block-inline">select under which category this category belongs to</span></label>
                <div class="span8">
                    <?php echo $this->Form->select('Category.parent_id', $cateTree , array('required' => false, 'required' => false, 'title' => 'Select Parent Category', 'div' => false, 'label' => false, 'id' => 'parent_id', 'class' => 'span2 select', 'empty' => '--Top Parent--')); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle" type="submit">Save Category</button>
    </div>
    <?php $this->Form->end(); ?>
</div>


<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Manage Existing Categories <span class="help-block-inline"> take necessary actions below </span></h4>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="content">
            <table class="responsive <?php /*if (count($cats)) echo 'dynamicTable';*/ ?> display table table-bordered">
                <thead>
                    <tr>
                        <th>Category Name</th>
                        <th>Parents/Path</th>
                        <th>Total Post</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        //pr($allPackages);
                        if ( count($cats) > 0 ):
                            foreach($cats as $k => $arr):
                                $path = $this->requestAction('/sadmins/getcategorypath/'.$arr['Category']['id']);
                                // number of post
                                $post = $this->requestAction('/sadmins/numberofpost/'.$arr['Category']['id']);

                            ?>
                            <tr>
                                <td><?php echo str_repeat('--', (count($path)-1) ). ( (count($path)-1 == 0) ? '<span style="font-weight:bold;">'. $arr['Category']['name'].'</span>' : $arr['Category']['name'] ) ; ?> </td>
                                <td style="text-align: left !important;"><?php
                                    $p =  '';
                                    $i = 1;
                                    if ( count($path) ) {
                                        foreach ( $path as $k => $ph ) {
                                            $p .= $ph['Category']['name']. ($i != count($path) ? '<span style="font-weight:bold;"> &rArr; </span>' : "") ;
                                            $i++;
                                        }
                                    }
                                    echo $p != '' ? $p :  '<span style="color:#ff3311;"> No Category Selected </span>'; ;

                                ?></td>
                                <td><?php echo $post; ?></td>

                                <td style="width: 100px !important;">
                                    <div class="controls center">
                                        <a href="<?php echo Router::url('managecats/' . $arr['Category']['id']); ?>" title="Edit Category" class="tip">
                                            <span class="icon12 icomoon-icon-pencil"></span></a>
                                        <a href="<?php echo Router::url('/deletecats/' . $arr['Category']['id']); ?>" title="Remove Category?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                                endforeach;
                            else:
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="7">No Category or Sub-Category Found!</td>
                        </tr>
                        <?php
                            endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $('#sTable').dataTable( {
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "iDisplayLength": 50,
            "aLengthMenu": [ 10, 25, 50, 75, 100, 250, 500 ],
            "bPaginate": true,
            "aaSorting": [],
            "aoColumns": [
                { "sType": "string"},
                { "sType": "string"},
                { "sType": "numeric"},
                null
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            }
        } );

        //top tooltip


    } );


</script>