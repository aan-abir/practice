<div class="marginB10"></div>
<div class="page-header">
    <h4>All your Clients</h4>
    <p>Bellow all the client listed. To take necessary action choose action under actions tab.</p>
</div>
<div class="row-fluid">
    <div class="span12">

        <div class="content">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allUser)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>User Name</th>
                        <th>Password</th>
                        <th>Client Name</th>
                        <th>Email</th>
                        <th>Package</th>
                        <th>Actions</th>
                        <!--<th>Mysites</th>-->
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if (count($allUser) > 0) {
                        foreach ($allUser as $camp) {
                            //pr($camp);
                            ?>
                            <tr>
                                <td><?php echo $camp['User']['id']; ?></td>
                                <td><a target="_blank" href="<?php echo Router::url('/users/letmeinwithoutanyusernameandpassword/' . $camp['User']['id']); ?>" title="Login into User Account" class="tip"><?php echo $camp['User']['username']; ?></a></td>
                                <td><?php echo $camp['User']['ccode']; ?></td>
                                <td><?php echo $camp['User']['fullname']; ?></td>
                                <td><?php echo $camp['User']['email']; ?></td>
                                <td><?php echo $camp['User']['packagename']; ?></td>
                                <td>
                                    <div class="controls center">
                                        <a href="<?php echo Router::url('viewclient/' . $camp['User']['id']); ?>" title="View Client" class="tip"><span class="icon12 icomoon-icon-grid-view"></span></a>
                                        <a href="<?php echo Router::url('editclient/' . $camp['User']['id']); ?>" title="Edit Client" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                        <a href="<?php echo Router::url('deleteclient/' . $camp['User']['id']); ?>" title="Remove Client?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                        <a target="_blank" href="<?php echo Router::url('/users/letmeinwithoutanyusernameandpassword/' . $camp['User']['id']); ?>" title="Log in as User" class="tip"><span class="icon12 icomoon-icon-forward"></span></a>
                                    </div>
                                </td>
                            </tr>

                            <?php
                        }
                    } else {
                        echo '<tr><td colspan="7">No Client Found!</td></tr>';
                    }
                    ?>



                </tbody>
            </table>
        </div>

    </div><!-- End .span6 -->
</div>

