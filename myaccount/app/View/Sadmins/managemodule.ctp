<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span4', 'autofocus'); ?>
<div class="marginB10"></div>
<div class="page-header">
    <h4>Manage Modules</h4>
    <p>manage modules from the list below. if you delete a module all files and lessons will be deleted also</p>
</div>

<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th class="zeroWidth"></th>
                <th class="textLeft">Icon</th>
                <th class="textLeft">Order</th>
                <th class="textLeft">Module Name</th>
                <th class="textLeft">Expert Name</th>
                <th class="textLeft">Expert Icon</th>
                <th class="textLeft">Title</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>

            <?php
                if (isset($items) && count($items) > 0):
                    foreach ($items as $item):
                        $arr = $item['Module'];
                    ?>
                    <tr>
                        <td class="zeroWidth"></td>
                        <td class="textLeft">
                            <?php
                                $icon = trim($arr['icon']) != '' ?  trim($arr['icon']) : ''; 
                                if ( $icon != '' ) {
                                    echo '<span class="icon16 '.$icon. '"></span>';
                                }else {
                                    echo '--';
                                }
                            ?>
                        </td>   
                        <td class="textLeft"><?php echo $arr['menu_order']; ?></td>
                        <td class="textLeft"><?php echo $arr['name']; ?></td>
                        <td class="textLeft"><?php echo $arr['ename']; ?></td>
                        <td class="textLeft">
                            <?php 
                                if ( $arr['photo'] != '' ){ ?>
                                <a title="Image Preview" class="fancybox tip" href="<?php echo  '/myaccount/moduleexpert/' . $arr['photo']; ?>">
                                    <img width="100" alt="Preview" src="<?php echo  '/myaccount/moduleexpert/' . $arr['photo']; ?>" />
                                </a>
                                <?php
                            } ?>

                        </td>
                        <td class="textLeft"><?php echo $arr['title']; ?></td>
                        <td><?php echo $arr['status'] ? 'Active' : 'Inactive'; ?></td>

                        <td>
                            <div class="controls center">
                                <a href="<?php echo Router::url('editmodule/' . $arr['id']); ?>" title="Edit Menu" class="tip">edit</a> | 
                                <a href="<?php echo Router::url('deletemodule/' . $arr['id']); ?>" title="Edit Menu" class="tip callAction">delete</a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td class="zeroWidth"></td>
                    <td colspan="3">No record found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
</div>

