<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span4', 'autofocus'); ?>
<div class="marginB10"></div>
<div class="page-header">
    <h4>Add / Manage Modules</h4>

    <p>to add a new module please fill up the field below.</p>
</div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create('Module', array('inputDefaults' => $inputDefaults, 'method' => 'post', 'type' => 'post', 'class' => 'form-horizontal seperator')); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="ModuleName">Menu Name:</label>
                        <?php echo $this->Form->hidden('id'); ?>
                        <?php echo $this->Form->input('name', array('placeholder' => "Menu Name")); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="ModuleMenuOrder">Menu Order:</label>
                        <?php echo $this->Form->input('menu_order', array('class' => 'span1')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="ModuleReleaseDate">Release Date:</label>
                        <div class="span6 date_cake_inline">
                            <?php echo $this->Form->input('release_date', array('class' => 'span2')); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12 inline_labels">
                    <label class="form-label span3" for="MenuStatus">Status:</label>

                    <div class="span7">
                        <?php echo $this->Form->radio('status', array('1' => 'Active', '0' => 'Inactive'), array('legend' => false, 'default' => '1')); ?>
                    </div>
                </div>
            </div>
            <div class="marginB10"></div>

            <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="submit" class="btn btn-info marginR10">Save Menu / Module</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->Form->end(); ?>
        </div>
        <!-- End .span12 -->
    </div>
</div>

<div class="marginB10"></div>

<div class="row-fluid">
    <div class="span12">

        <div class="box">
            <div class="title">
                <h4>
                    <span>Your Module List</span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                    <tr>
                        <th class="zeroWidth"></th>
                        <th class="textLeft">Name</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    if (isset($items) && count($items) > 0):
                        foreach ($items as $item):
                            $arr = $item['Module'];
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td class="textLeft"><?php echo $arr['name']; ?></td>
                                <td><?php echo $arr['status'] ? 'Active' : 'Inactive'; ?></td>
                                <td>
                                    <div class="controls center">
                                        <a href="<?php echo Router::url('modules/' . $arr['id']); ?>"
                                           title="Edit Menu" class="tip"><span
                                                class="icon12 icomoon-icon-pencil"></span></a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <tr>
                            <td class="zeroWidth"></td>
                            <td colspan="3">No record found!</td>
                        </tr>
                    <?php
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>

        </div>
        <!-- End .box -->

    </div>
    <!-- End .span6 -->
</div>