<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span11', 'autofocus'); ?>
<div class="content">
    <?php echo $this->Form->create('Backend', array('inputDefaults' => $inputDefaults, 'novalidate' => true)); ?>
    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="DomainDomain">Domain:</label>
                    <?php echo $this->Form->input('Domain.domain'); ?>
                </div>
                <div class="span4">
                    <label for="DomainUsername">Username:</label>
                    <?php echo $this->Form->input('Domain.ausername'); ?>
                </div>
                <div class="span4">
                    <label for="DomainPassword">Password:</label>
                    <?php echo $this->Form->input('Domain.apassword'); ?>
                </div>
            </div>
        </div>        
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="DomainTitle">Title:</label>
                    <?php echo $this->Form->input('Domain.title'); ?>
                </div>
                <div class="span4">
                    <label for="DomainMarkets">Markets:</label>
                    <?php echo $this->Form->input('Domain.markets'); ?>
                </div>
                <div class="span4">
                    <label for="DomainTopic">Topic:</label>
                    <?php echo $this->Form->input('Domain.topic'); ?>
                </div>
            </div>
        </div>        
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid inline_labels">
                <label class="form-label span3" for="DomainIsIndexed">Is Indexed:</label>
                <div class="span4">                    
                    <?php echo $this->Form->radio('Domain.is_indexed', array('1' => 'Yes', '0' => 'No'), array('default' => 1, 'legend' => false)); ?>
                </div>
            </div>
        </div>
    </div>    
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid inline_labels">
                <label class="form-label span3" for="DomainStatus">Status:</label>
                <div class="span4">                    
                    <?php echo $this->Form->radio('Domain.status', array('1' => 'Active', '0' => 'Inactive'), array('default' => 1, 'legend' => false)); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="marginT10"></div>
    <div class="form-actions">
        <button class="btn btn-info offset2" type="submit">Submit</button>
    </div>
    <?php echo $this->Form->end(); ?>
</div>


<div class="marginB10"></div>
