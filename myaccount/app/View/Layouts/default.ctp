<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title_for_layout; ?></title>
        <meta name="application-name" content="seonitro control panel version 2" />
        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- Le styles -->
        <!-- Use new way for google web fonts
        http://www.smashingmagazine.com/2012/07/11/avoiding-faux-weights-styles-google-web-fonts -->
        <!-- Headings -->
        <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />  -->
        <!-- Text -->
        <!-- <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' /> -->
        <!--[if lt IE 9]>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
        <![endif]-->

        <?php
            echo $this->Html->css(
                array(
                    '/css/bootstrap/bootstrap.min.css', // core sytlesheet
                    '/css/bootstrap/bootstrap-responsive.min', // core sytlesheet
                    '/css/supr-theme/jquery.ui.supr', // core sytlesheet
                    '/css/icons', // core sytlesheet
                    '/plugins/misc/qtip/jquery.qtip', // Plugins stylesheets
                    '/plugins/misc/fullcalendar/fullcalendar', // Plugins stylesheets
                    '/plugins/misc/search/tipuesearch', // Plugins stylesheets
                    '/plugins/forms/uniform/uniform.default.css', // Plugins stylesheets
                    '/css/flowplayer543/skin/minimalist.css',
                    '/css/main', // main stylesheets
                    '/css/custom', // Custom stylesheets
                    '/plugins/tables/dataTables/jquery.dataTables',
                    '/plugins/forms/smartWizzard/smart_wizard',
                    '/plugins/misc/prettify/prettify',
                    '/js/fancybox/source/jquery.fancybox.css?v=2.1.5',
                    '/plugins/forms/select/select2.css',
                    '/plugins/forms/color-picker/color-picker.css',
                )
            );
        ?>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="images/favicon.ico" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon-144-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon-114-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon-72-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon-57-precomposed.png" />




        <script type="text/javascript">
            //adding load class to body and hide page
            document.documentElement.className += 'loadstate';
        </script>

        <?php
            echo $this->Html->script(
                array(
                    '/js/jquery.min',
                    '/js/bootstrap/bootstrap.min',
                )
            );
        ?>

        <!-- Le javascript
        ================================================== -->
        <!-- Important plugins put in all pages -->

        <?php
            echo $this->Html->script(
                array(
                    '/js/jquery.cookie',
                    '/js/jquery.mousewheel',
                    '/plugins/forms/validate/jquery.validate.min',
                    '/plugins/forms/uniform/jquery.uniform.min',
                    //  Charts plugins
                    '/plugins/charts/flot/jquery.flot',
                    '/plugins/charts/flot/jquery.flot.grow',
                    '/plugins/charts/flot/jquery.flot.pie',
                    '/plugins/charts/flot/jquery.flot.resize',
                    '/plugins/charts/flot/jquery.flot.tooltip_0.4.4',
                    '/plugins/charts/flot/jquery.flot.orderBars',
                    '/plugins/charts/sparkline/jquery.sparkline.min',
                    '/plugins/charts/knob/jquery.knob',
                    // Misc plugins
                    '/plugins/misc/fullcalendar/fullcalendar.min',
                    '/plugins/misc/qtip/jquery.qtip.min',
                    '/plugins/misc/totop/jquery.ui.totop.min',
                    // Search plugin
                    '/plugins/misc/search/tipuesearch_set',
                    '/plugins/misc/search/tipuesearch_data',
                    '/plugins/misc/search/tipuesearch',
                    // Form plugins
                    '/plugins/forms/watermark/jquery.watermark.min',
                    '/plugins/forms/uniform/jquery.uniform.min',
                    '/plugins/forms/color-picker/colorpicker.js',
                    //'/plugins/forms/togglebutton/jquery.toggle.buttons',
                    //'/plugins/gallery/pretty-photo/jquery.prettyPhoto',
                    '/plugins/misc/prettify/prettify',
                    '/plugins/forms/elastic/jquery.elastic',
                    '/plugins/forms/select/select2.min',
                    '/plugins/forms/tiny_mce/jquery.tinymce',
                    '/plugins/tables/dataTables/jquery.dataTables',
                    // Fix plugins
                    // '/plugins/fix/ios-fix/ios-orientationchange-fix',
                    // Important Place before main.js
                    //'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js',
                    //'/plugins/fix/touch-punch/jquery.ui.touch-punch.min',
                    // Init plugins
                    '/js/jquery-ui.min',
                    '/js/main',
                    '/js/dashboard',
                    '/js/datatable',
                    '/js/supr-theme/jquery-ui-timepicker-addon.js',
                    //'/js/elements',
                    '/plugins/forms/smartWizzard/jquery.smartWizard-2.0.min',
                    '/js/flowplayer543/flowplayer.min.js',
                    '/js/fancybox/source/jquery.fancybox.js?v=2.1.5',
                    '/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6',
                    '/js/custom',
                    //'/js/charts'
                )
            );
        ?>

    </head>

    <body>
        <!-- loading animation -->
        <div id="qLoverlay"></div>
        <div id="qLbar"></div>

        <!-- Start #header -->
        <?php
            $action = $this->params['action'];
            // menu tree
            // parent page => submenu
            // select the array for request method
            $menuGroup = array(
                array('orderguestpost' => 'Order New', 'manageguestpost' => 'View Order', 'assignedguestpost' => 'In Process Order', 'completedguestpost' => 'Completed Order', 'editguestpost' => 'Edit Guest Post'),
                array('createmashup' => 'Create MashUp', 'managemashup' => 'Manage MashUp', 'viewmashup' => 'Report', 'editmashup' => 'Edit'),
                array('addsubmission' => 'Create Auto Post', 'managesubmission' => 'Manage Auto Post', 'editsubmission' => 'Edit Submission'),
                array('addblogroll' => 'Add Sitewide links', 'manageblogroll' => 'Manage Links', 'editblogroll' => 'Edit Blogroll'),
                array('linkemperor' => 'Order Blast', 'linkemperorinprogress' => 'Blast Reports'),
                array('bazookablast' => 'Order New Blast', 'bazookainprogress' => 'Blast Reports'),
                array('myplan' => 'My Current Plan', 'addcredits' => 'Add Extra Credits'),
                array('mysites' => 'Private Sites', 'newdomain' => 'Add Private Sites',  'editdomain' => 'Edit Domain'),
                //array('mysites' => 'Private Sites', 'newdomain' => 'Add Private Sites', 'myexclusivesites' => 'My Sites', 'editdomain' => 'Edit Domain'),
                array('manualnetwork' => 'Manual Network', 'manualpostreport' => 'Post Report', 'manualpost' => ''),
                array('content' => 'Add Content', 'contents' => 'Contents', 'order_content' => 'Order Content'),
            );

            $menuGroup[] = array('pr_campaign_create' => 'Campaign Create', 'pr_campaign_edit' => ' Edit Campaign', 'pr_campaign_view' => 'Campaign View', 'pr_campaign_list' => 'Campaign List', 'pr_user_proxy_create' => 'Add Own Proxy', 'pr_user_proxy_list' => 'Proxy List', 'pr_user_proxy_edit' => 'Proxy Edit', 'pr_instant_check' => 'Instant Check', 'pr_domain_history' => 'Page Rank Domain History', 'pr_instant_check_list' => 'Instant Check List');
            $menuGroup[] = array('ld_report_list' => 'Report List', 'ld_link_ratio' => 'Link Ratios', 'ld_anchor_metrics' => 'Anchor Metrics', 'ld_linktype_metrics' => 'Linktype Metrics', 'ld_power_link_metrics' => 'Power Link Metrics', 'ld_filter' => 'Filter By');
            if ((AuthComponent::user('id') != 0)) {
                $menuGroup[] = array('newrankcampaign' => 'Create Rank Campaign', 'rankresults' => 'Rank Results', 'viewrankresult' => 'View Rank Result', 'rankresultsmore' => 'Rank Results More', 'editrankcampaign' => 'Edit Rank Campaign'); //, 'instantranking' => 'Instant Ranking', 'instantrankresults' => 'Instant Results'
            }
            $menuGroup[] = array('orderpressrelease' => 'Order Press Release', 'pressreleases' => 'Manage Press Release', 'viewpressrelease' => 'View Press Release', 'editpressrelease' => 'Edit Press Release');

            $menuGroup[] = array('linkindexing' => 'Submit Links', 'indexingreport' => 'Indexing Report', 'indexingurls' => '');

            $excludeSubMenu = array('pr_campaign_edit', 'pr_campaign_view', 'pr_user_proxy_edit', 'editsubmission', 'submissionlinks', 'editguestpost', 'editdomain', 'editmashup', 'viewrankresult', 'rankresultsmore', 'pr_domain_history', 'editrankcampaign', 'viewpressrelease', 'editpressrelease', 'indexingurls', 'manualpost','editblogroll');

            $subMenu = '';
            $showLoader = array('linkemperor');
            foreach ($menuGroup as $k => $g) {
                if (array_key_exists($action, $g)) {
                    $activeTopMenu = key($g);
                    foreach ($g as $act => $label) {
                        if (!in_array($act, $excludeSubMenu)) {
                            $subMenu .= '<li>';
                            $class = '';
                            if (in_array($act, $showLoader)) {
                                $class .= ' loader ';
                            }
                            if ($action == $act) {
                                $class .= ' selected ';
                            }
                            $cls = ' class="' . trim($class) . '"';
                            $subMenu .= '<a ' . $cls . ' href="' . ( ($action == $act) && empty($this->params['pass']) ? '#' : Router::url($act)) . '">' . $label . '</a>';
                            $subMenu .= '</li>';
                        }
                    }
                    break;
                }
            }
            echo ( AuthComponent::user('role') != '' ) ? $this->element('header_' . AuthComponent::user('role')) : '';
        ?>
        <!-- End #header -->

        <div id="wrapper">

            <!--Responsive navigation button-->
            <div class="resBtn">
                <a href="#"><span class="icon16 minia-icon-list-3"></span></a>
            </div>

            <!--Left Sidebar collapse button-->
            <div class="collapseBtn leftbar">
                <a href="#" class="tipR" title="Hide Left Sidebar"><span class="icon12 minia-icon-layout"></span></a>
            </div>

            <!--Sidebar background-->
            <div id="sidebarbg"></div>

            <?php
                $sadminsAction = $this->params['action'];
                switch (AuthComponent::user('role')) {
                    case 'admin' :
                        $sideBarMenu = array(
                            array('dashboard'),
                            array('addservice', 'services', 'editservice'),
                            array('addpackage', 'packages', 'editpackage'),
                            array('newclient', 'manageclients', 'monthlyrenewals', 'editclient'),
                            array('manageclprvsites', 'managemysites', 'managedomains'),
                            array('activedomains', 'problematicdomains', 'updatehttpstatus'),
                            //array('assignedmysites', 'assignedclients', 'freemysites'),
                            array('viewcredits', 'creditstogroup', 'creditstouser', 'credithistory', 'debithistory', 'updateusercredits'),
                            array('addinstruction', 'manageinstruction', 'editinstruction'),
                            array('addmedia', 'managemedia', 'editinstruction'),
                            array('managecats', 'addlesson', 'managelesson', 'editlesson'),
                            array('managesetting'),
                            //array('newblogger', 'managebloggers', 'editblogger'),
                            array('bazookacategories'),
                            array('addfaqcategory', 'managefaqcategories', 'editfaqcategory'),
                            array('addfaq', 'managefaqs', 'editfaqs'),
                            array('addmenu', 'managemenu'),
                            array('addmodule', 'managemodule', 'addlessons','managelessons', 'editmodule'),
                            array('addwelcomenews', 'editwelcomenews', 'welcomenews'),
                            array('addevents','manageevents'),
                        );
                        break;
                    case 'client' :

                        $prActions = array('pr_campaign_create', 'pr_campaign_list', 'pr_campaign_view', 'pr_domain_history', 'pr_campaign_edit', 'pr_user_proxy_create', 'pr_user_proxy_list', 'pr_user_proxy_edit', 'pr_instant_check', 'pr_instant_check_list');
                        $linkden = array('ldreport', 'ld_link_ratio', 'ld_anchor_metrics', 'ld_linktype_metrics', 'ld_power_link_metrics', 'ld_filter');

                        $crActions = array('newrankcampaign', 'rankresults', 'viewrankresult', 'rankresultsmore', 'editrankcampaign', 'instantranking', 'instantrankresults');

                        $prCurrentAction = '';


                        if (in_array($sadminsAction, $prActions)) {
                            $prCurrentAction = $sadminsAction;
                        }

                        $sideBarMenu = array(
                            array('dashboard'),
                            array('addcampaign', 'managecampaign'),
                            array(in_array($sadminsAction, $crActions) ? $sadminsAction : '', 'historicalranking'),
                            array(in_array($sadminsAction, array('submissionreport', 'submissionlinks')) ? $sadminsAction : '', ''),
                            array(''),
                            array('education','etopics'),
                            array($prCurrentAction, in_array($sadminsAction, $linkden) ? $sadminsAction : ''),
                            array(in_array($sadminsAction, array('contents', 'content', 'order_content')) ? $sadminsAction : ''),
                        );
                        break;
                    case 'blogger' :
                        $sideBarMenu = array(
                            array('dashboard'),
                            array('guestsites'),
                            array('allguestposts', 'assignedposts', 'completedposts'),
                        );
                        break;
                }
                $navIndex = '';
                foreach ($sideBarMenu as $pk => $sMenu) {
                    if (in_array($sadminsAction, $sMenu)) {
                        $navIndex = (string) $pk;
                        $navIndex .= ',' . (string) array_search($sadminsAction, $sMenu);
                    }
                }
                if (in_array($sadminsAction, array('viewusercredits', 'updateusercredits'))) {
                    $navIndex = '7,99';
                }
            ?>

            <!--Sidebar content-->
            <?php 
            
            if ( $this->params['action'] != 'community') {
              echo ( AuthComponent::user('role') != '' ) ? $this->element('sidebar_' . AuthComponent::user('role')) : ''; 
            }
            
            ?>
            
            
            <!-- End #sidebar -->

            <!--Body content-->
            <div id="content" class="clearfix">
                <div class="contentwrapper"><!--Content wrapper-->
                    <!-- Build page from here: -->
                    <?php echo $this->Session->flash(); ?>
                    <?php
                        if (AuthComponent::user('role') == 'client') {
                            // check if this page has video tutorial
                            $vl = $hg = '';
                            if (isset($tutorial) && count($tutorial) && ($this->params['controller'] == 'clients')) { // from appController beforeRender
                                if ((trim($tutorial['Instruction']['video']) != '') || (trim($tutorial['Instruction']['externallink']) != '')) {
                                    if ((trim($tutorial['Instruction']['video']) != '')) {
                                        $l = Router::url('loadhelptext/' . $tutorial['Instruction']['pagename'] . '/v');
                                        $vl = '<button title="Watch the useful video. Double click on Video for full screen" style="padding: 4px !important;" href="' . $l . '" class="openModalDialog btn btn-link"><span style="vertical-align: top !important; margin-right: 3px !important;" class="icon16 eco-videos"></span>Video Tutorial</button>';
                                    } else if ((trim($tutorial['Instruction']['externallink']) != '')) {
                                        $vl = '<a title="Watch this useful video" class="fancybox-media btn btn-link" href="' . trim($tutorial['Instruction']['externallink']) . '"><span style="vertical-align: top !important; margin-right: 3px !important;" class="icon16 eco-videos"></span>Video Tutorial</a>';
                                    }
                                }
                                if (trim($tutorial['Instruction']['helpguide']) != '') {
                                    $l = Router::url('loadhelptext/' . $tutorial['Instruction']['pagename'] . '/helpguide');
                                    $hg = '<button style="padding: 4px !important;" href="' . $l . '" class="openModalDialog btn btn-link">
                                    <span  style="vertical-align: top !important; margin-right: 3px !important;" class="icon16 icomoon-icon-help"></span>
                                    Help Guide</button>';
                                }
                                if (($vl != '') || ($hg != '')) {
                                    echo '<div class="submenu_tab1" style="float: right;height:27px !important;"><ul style="float: right;padding:0 !important;margin:0 !important;"><li>';
                                    if ($vl != '') {
                                        echo $vl;
                                    }
                                    if ($hg != '') {
                                        echo $hg;
                                    }
                                    echo '  </li></ul></div>';
                                }
                            } // if found tutorial

                            /*  Start submenu */
                            if ($subMenu != '') {
                                echo '<div class="submenu_tab clearfix">';
                                echo '<ul>';
                                echo $subMenu;
                                echo '</ul>';
                                echo '</div>';
                            }
                        }
                    ?>



                    <!-- end submenu -->

                    <!--<div class="marginB10"></div>-->
                    <?php
                        if (isset($topInstruction)) : //  for admin it is not set
                            if (trim(strip_tags($topInstruction)) != ''): // for client it is set but it could be empty
                            ?>
                            <div class="page-header">
                                <?php echo $topInstruction; ?>
                            </div>
                            <?php
                                endif;
                            endif;
                    ?>

                    <?php echo $this->fetch('content'); ?>

                    <!-- End Build page from here: -->


                </div><!-- End contentwrapper -->
            </div><!-- End #content -->

        </div><!-- End #wrapper -->


        <input type="hidden" name="postErrorFields" id="postErrorFields" value="<?php echo isset($errorFields) ? $errorFields : ''; ?>" />
        <input type="hidden" class="leftParentIndex" value="<?php echo isset($navIndex) ? $navIndex : 0; ?>">

        <?php //echo $this->element('sql_dump');   ?>

        <div id="modal" style="width: 100% !important; height: 100% !important;"  title="Tutorial" class="dialog"></div>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.openModalDialog').click(function() {
                    _this = $(this);
                    $title = _this.attr('title');
                    $modal = $('#modal');
                    $("#ui-id-1").html($title);
                    $modal.css({textAlign: 'center', minHeight: '400px'}).html('<p>Loading Please Wait...</p><p><?php echo $this->Html->image('/images/loader.gif', array('alt' => false)); ?></p>');
                    $modal.load(_this.attr('href')).dialog('open');
                    return false;
                });
                // JQuery UI Modal Dialog
                $('#modal').dialog({
                    autoOpen: false,
                    modal: true,
                    autoResize: true,
                    dialogClass: 'dialog',
                    buttons: {
                        "Close": function() {
                            $(this).dialog("close");
                        }
                    }
                });
                $("div.dialog button").addClass("btn");

                $(".loader").live('click', function(e) {
                    document.documentElement.className += 'loadstate';
                });

            });  // end ready



            $('.fancybox').fancybox();
            /*
            *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
            */
            $('.fancybox-media')
            .attr('rel', 'media-gallery')
            .fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                prevEffect: 'none',
                nextEffect: 'none',
                arrows: false,
                helpers: {
                    media: {},
                    buttons: {}
                }
            });


        </script>
    </body>
</html>