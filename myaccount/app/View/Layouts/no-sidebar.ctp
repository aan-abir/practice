<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title_for_layout; ?></title>
        <meta name="application-name" content="seonitro control panel version 2" />
        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- Le styles -->
        <!-- Use new way for google web fonts
        http://www.smashingmagazine.com/2012/07/11/avoiding-faux-weights-styles-google-web-fonts -->
        <!-- Headings -->
        <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />  -->
        <!-- Text -->
        <!-- <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' /> -->
        <!--[if lt IE 9]>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
        <![endif]-->

        <?php
            echo $this->Html->css(
                array(
                    '/css/bootstrap/bootstrap.min.css', // core sytlesheet
                    '/css/bootstrap/bootstrap-responsive.min', // core sytlesheet
                    '/css/supr-theme/jquery.ui.supr', // core sytlesheet
                    '/css/icons', // core sytlesheet
                    '/plugins/misc/qtip/jquery.qtip', // Plugins stylesheets
                    '/plugins/misc/fullcalendar/fullcalendar', // Plugins stylesheets
                    '/plugins/misc/search/tipuesearch', // Plugins stylesheets
                    '/plugins/forms/uniform/uniform.default.css', // Plugins stylesheets
                    '/css/flowplayer543/skin/minimalist.css',
                    '/css/main', // main stylesheets
                    '/css/custom', // Custom stylesheets
                    '/plugins/tables/dataTables/jquery.dataTables',
                    '/plugins/forms/smartWizzard/smart_wizard',
                    '/plugins/misc/prettify/prettify',
                    '/js/fancybox/source/jquery.fancybox.css?v=2.1.5',
                    '/plugins/forms/select/select2.css',
                    '/plugins/forms/color-picker/color-picker.css',
                )
            );
        ?>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="images/favicon.ico" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon-144-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon-114-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon-72-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon-57-precomposed.png" />




        <script type="text/javascript">
            //adding load class to body and hide page
            document.documentElement.className += 'loadstate';
        </script>

        <?php
            echo $this->Html->script(
                array(
                    '/js/jquery.min',
                    '/js/bootstrap/bootstrap.min',
                )
            );
        ?>

        <!-- Le javascript
        ================================================== -->
        <!-- Important plugins put in all pages -->

        <?php
            echo $this->Html->script(
                array(
                    '/js/jquery.cookie',
                    '/js/jquery.mousewheel',
                    '/plugins/forms/validate/jquery.validate.min',
                    '/plugins/forms/uniform/jquery.uniform.min',
                    //  Charts plugins
                    '/plugins/charts/flot/jquery.flot',
                    '/plugins/charts/flot/jquery.flot.grow',
                    '/plugins/charts/flot/jquery.flot.pie',
                    '/plugins/charts/flot/jquery.flot.resize',
                    '/plugins/charts/flot/jquery.flot.tooltip_0.4.4',
                    '/plugins/charts/flot/jquery.flot.orderBars',
                    '/plugins/charts/sparkline/jquery.sparkline.min',
                    '/plugins/charts/knob/jquery.knob',
                    // Misc plugins
                    '/plugins/misc/fullcalendar/fullcalendar.min',
                    '/plugins/misc/qtip/jquery.qtip.min',
                    '/plugins/misc/totop/jquery.ui.totop.min',
                    // Search plugin
                    '/plugins/misc/search/tipuesearch_set',
                    '/plugins/misc/search/tipuesearch_data',
                    '/plugins/misc/search/tipuesearch',
                    // Form plugins
                    '/plugins/forms/watermark/jquery.watermark.min',
                    '/plugins/forms/uniform/jquery.uniform.min',
                    '/plugins/forms/color-picker/colorpicker.js',
                    //'/plugins/forms/togglebutton/jquery.toggle.buttons',
                    //'/plugins/gallery/pretty-photo/jquery.prettyPhoto',
                    '/plugins/misc/prettify/prettify',
                    '/plugins/forms/elastic/jquery.elastic',
                    '/plugins/forms/select/select2.min',
                    '/plugins/forms/tiny_mce/jquery.tinymce',
                    '/plugins/tables/dataTables/jquery.dataTables',
                    // Fix plugins
                    // '/plugins/fix/ios-fix/ios-orientationchange-fix',
                    // Important Place before main.js
                    //'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js',
                    //'/plugins/fix/touch-punch/jquery.ui.touch-punch.min',
                    // Init plugins
                    '/js/jquery-ui.min',
                    '/js/main',
                    '/js/dashboard',
                    '/js/datatable',
                    '/js/supr-theme/jquery-ui-timepicker-addon.js',
                    //'/js/elements',
                    '/plugins/forms/smartWizzard/jquery.smartWizard-2.0.min',
                    '/js/flowplayer543/flowplayer.min.js',
                    '/js/fancybox/source/jquery.fancybox.js?v=2.1.5',
                    '/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6',
                    '/js/custom',
                    //'/js/charts'
                )
            );
        ?>

    </head>



    <body>
        <!-- loading animation -->
        <div id="qLoverlay"></div>
        <div id="qLbar"></div>

        <!-- Start #header -->
        <?php
            $action = $this->params['action'];
            // menu tree
            // parent page => submenu
            // select the array for request method
            $menuGroup = array(
                array('orderguestpost' => 'Order New', 'manageguestpost' => 'View Order', 'assignedguestpost' => 'In Process Order', 'completedguestpost' => 'Completed Order', 'editguestpost' => 'Edit Guest Post'),
                array('createmashup' => 'Create MashUp', 'managemashup' => 'Manage MashUp', 'viewmashup' => 'Report', 'editmashup' => 'Edit'),
                array('addsubmission' => 'Create Auto Post', 'managesubmission' => 'Manage Auto Post', 'editsubmission' => 'Edit Submission'),
                array('addblogroll' => 'Add Sitewide links', 'manageblogroll' => 'Manage Links', 'editblogroll' => 'Edit Blogroll'),
                array('linkemperor' => 'Order Blast', 'linkemperorinprogress' => 'Blast Reports'),
                array('bazookablast' => 'Order New Blast', 'bazookainprogress' => 'Blast Reports'),
                array('myplan' => 'My Current Plan', 'addcredits' => 'Add Extra Credits'),
                array('mysites' => 'Private Sites', 'newdomain' => 'Add Private Sites',  'editdomain' => 'Edit Domain'),
                //array('mysites' => 'Private Sites', 'newdomain' => 'Add Private Sites', 'myexclusivesites' => 'My Sites', 'editdomain' => 'Edit Domain'),
                array('manualnetwork' => 'Manual Network', 'manualpostreport' => 'Post Report', 'manualpost' => ''),
                array('content' => 'Add Content', 'contents' => 'Contents', 'order_content' => 'Order Content'),
            );

            $menuGroup[] = array('pr_campaign_create' => 'Campaign Create', 'pr_campaign_edit' => ' Edit Campaign', 'pr_campaign_view' => 'Campaign View', 'pr_campaign_list' => 'Campaign List', 'pr_user_proxy_create' => 'Add Own Proxy', 'pr_user_proxy_list' => 'Proxy List', 'pr_user_proxy_edit' => 'Proxy Edit', 'pr_instant_check' => 'Instant Check', 'pr_domain_history' => 'Page Rank Domain History', 'pr_instant_check_list' => 'Instant Check List');
            $menuGroup[] = array('ld_report_list' => 'Report List', 'ld_link_ratio' => 'Link Ratios', 'ld_anchor_metrics' => 'Anchor Metrics', 'ld_linktype_metrics' => 'Linktype Metrics', 'ld_power_link_metrics' => 'Power Link Metrics', 'ld_filter' => 'Filter By');
            if ((AuthComponent::user('id') != 0)) {
                $menuGroup[] = array('newrankcampaign' => 'Create Rank Campaign', 'rankresults' => 'Rank Results', 'viewrankresult' => 'View Rank Result', 'rankresultsmore' => 'Rank Results More', 'editrankcampaign' => 'Edit Rank Campaign'); //, 'instantranking' => 'Instant Ranking', 'instantrankresults' => 'Instant Results'
            }
            $menuGroup[] = array('orderpressrelease' => 'Order Press Release', 'pressreleases' => 'Manage Press Release', 'viewpressrelease' => 'View Press Release', 'editpressrelease' => 'Edit Press Release');

            $menuGroup[] = array('linkindexing' => 'Submit Links', 'indexingreport' => 'Indexing Report', 'indexingurls' => '');

            $excludeSubMenu = array('pr_campaign_edit', 'pr_campaign_view', 'pr_user_proxy_edit', 'editsubmission', 'submissionlinks', 'editguestpost', 'editdomain', 'editmashup', 'viewrankresult', 'rankresultsmore', 'pr_domain_history', 'editrankcampaign', 'viewpressrelease', 'editpressrelease', 'indexingurls', 'manualpost','editblogroll');

            $subMenu = '';
            $showLoader = array('linkemperor');
            foreach ($menuGroup as $k => $g) {
                if (array_key_exists($action, $g)) {
                    $activeTopMenu = key($g);
                    foreach ($g as $act => $label) {
                        if (!in_array($act, $excludeSubMenu)) {
                            $subMenu .= '<li>';
                            $class = '';
                            if (in_array($act, $showLoader)) {
                                $class .= ' loader ';
                            }
                            if ($action == $act) {
                                $class .= ' selected ';
                            }
                            $cls = ' class="' . trim($class) . '"';
                            $subMenu .= '<a ' . $cls . ' href="' . ( ($action == $act) && empty($this->params['pass']) ? '#' : Router::url($act)) . '">' . $label . '</a>';
                            $subMenu .= '</li>';
                        }
                    }
                    break;
                }
            }
            echo ( AuthComponent::user('role') != '' ) ? $this->element('header_' . AuthComponent::user('role')) : '';
        ?>
        <!-- End #header -->


        <div id="wrapper">

            <!--Responsive navigation button-->  
            <div class="resBtn">
                <a href="#"><span class="icon16 minia-icon-list-3"></span></a>
            </div>

            <!--Body content-->
            <div id="content-noside" class="clearfix">
                <div class="contentwrapper"><!--Content wrapper-->


                    <?php echo $this->fetch('content'); ?>


                </div><!-- End contentwrapper -->
            </div><!-- End #content -->

        </div><!-- End #wrapper -->
        <input type="hidden" name="postErrorFields" id="postErrorFields" value="<?php echo isset($errorFields) ? $errorFields : ''; ?>" />
        <input type="hidden" class="leftParentIndex" value="<?php echo isset($navIndex) ? $navIndex : 0; ?>">
        <?php //echo $this->element('sql_dump');   ?>

    </body>
</html>