<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Enter New Blogger Data</h4>
    <p>To edit this blogger simply fill up the form below. </p>

</div>
<div class="content">
    <!--< ?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'editblogger', $data['User']['id']), 'id' => 'ncForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>-->
    <form accept-charset="utf-8" enctype="multipart/form-data" class="validate_form" method="post" id="ncForm" action="<?php echo adminHome; ?>/editblogger/<?php echo $data['User']['id']; ?>" novalidate="novalidate">
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="username">Username:</label>
                    <?php echo $this->Form->input('User.username', array('error' => false, 'id' => 'username', 'title' => 'User Name', 'readonly' => 'readonly', 'placeholder' => "User Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    <input type="hidden" name="data[User][id]" value="<?php echo $cId; ?>" >
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="normal">Password:<br><em style="font-size: 9px;">leave it blank if no change needed</em></label>
                    <div class="span4 controls">
                        <?php echo $this->Form->input('UserEx.password', array('error' => false, 'type' => 'text', 'value' => '', 'id' => 'password', 'title' => 'Enter Blogger Passord', 'placeholder' => "User password", 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="firstname">First name:</label>
                    <?php echo $this->Form->input('User.firstname', array('error' => false, 'type' => 'text', 'id' => 'firstname', 'title' => 'Blogger First Name', 'placeholder' => "Blogger First Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="name">Last name:</label>
                    <?php echo $this->Form->input('User.lastname', array('error' => false, 'type' => 'text', 'id' => 'lastname', 'title' => 'Blogger Last Name', 'placeholder' => "Blogger Last Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="email">Email:</label>
                    <?php echo $this->Form->input('User.email', array('error' => false, 'type' => 'text', 'id' => 'email', 'title' => 'Blogger Email', 'placeholder' => "Blogger Email", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="email">Phone:</label>
                    <?php echo $this->Form->input('User.phone', array('error' => false, 'type' => 'text', 'id' => 'phone', 'title' => 'Blogger Phone', 'placeholder' => "Blogger Phone", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="skype">Skype:</label>
                    <?php echo $this->Form->input('User.skype', array('error' => false, 'type' => 'text', 'id' => 'skype', 'title' => 'Blogger Skype', 'placeholder' => "Blogger Skype", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="textarea">More info</label>
                    <?php echo $this->Form->textarea('User.moreaboutclient', array('error' => false, 'id' => 'moreaboutclient', 'title' => 'Blogger Additional Contact Info', 'div' => false, 'label' => false, 'class' => 'span4 limit elastic', 'rows' => 3, 'cols' => '5')); ?>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="status">Blogger Status:</label>
                    <div class="left marginT5 marginR10">
                        <input type="radio" name="data[User][status]" id="UserStatus1" value="1" <?php
                    if ($data['User']['status'] == 1) {
                        $checked = ' checked="checked"';
                        echo $checked;
                    }
                    ?> />
                        Active
                    </div>
                    <div class="left marginT5 marginR10">
                        <input type="radio" name="data[User][status]" id="UserStatus0" value="0" <?php
                               if ($data['User']['status'] == 0) {
                                   $checked = ' checked="checked"';
                                   echo $checked;
                               }
                    ?>/>
                        InActive
                    </div>
                </div>
            </div>
        </div>
        <div class="marginT10"></div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="submit" class="btn btn-info marginR10">Update Blogger</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>