<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Add internal or external media</h4>
    <p>here you can add any external or internal meida such as - youtube video links, upload any image or video file from your local computer. once a source added you can use that in any post or tutorial or education.</p>

</div>


<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'addmedia'), 'id' => 'addMediaForm', 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="title">Title of Media</label>
                <div>
                    <?php echo $this->Form->input('Media.title', array('error' => false, 'type' => 'text', 'id' => 'title', 'title' => 'Title', 'placeholder' => "Title", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    <span class="help-inline blue">Exp: How to Add Campaign</span></div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="MediaMtypeExternal">Media Type</label>
                <div class="span8 controls">
                    <div class="left marginT5 marginR10">
                        <input type="radio" name="data[Media][type]" class="type" id="MediaMtypeExternal" value="link" checked="checked" />
                        From External source
                    </div>
                    <div class="left marginT5 marginR10">
                        <input type="radio" name="data[Media][type]" class="type" id="MediaMtypeInternalImage" value="image" />
                        Image from Local Computer
                    </div>
                    <div class="left marginT5">
                        <input type="radio" name="data[Media][type]" class="type" id="MediaMtypeInternalFile" value="files" />
                        Files from Local Computer
                    </div>
                    <div class="left marginT5">
                        <input type="radio" name="data[Media][type]" class="type" id="MediaMtypeInternalVideo" value="video" />
                        Video from Local Computer
                    </div>
                </div>
            </div>
        </div> 
    </div>



    <div class="form-row row-fluid extlink">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="src">From External Link</label>
                <div>
                    <?php echo $this->Form->input('Media.src', array('error' => false, 'type' => 'text', 'id' => 'youtlink', 'title' => 'External Video Link', 'placeholder' => "External Video Link", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    <span class="help-inline blue">Exp: http://www.youtube.com/embed/6-tU5knzx04?rel=0</span></div>
            </div>
        </div>
    </div> 



    <div class="form-row row-fluid uploadfile" style="display: none;">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="upload">Upload From Local PC</label>
                <input type="file" name="data[Media][file]" id="upload" title="Upload Media" /> 

                <span class="help-inline blue">choose image/video file.</span>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid uploaddim" style="display: none;">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="w">Resize Image<span class="help-block">crop image if dimesion goes high</span></label>
                <div>
                    <?php echo $this->Form->input('Media.w', array('error' => false, 'type' => 'text', 'id' => 'title', 'title' => 'Title', 'placeholder' => "width", 'div' => false, 'label' => false, 'class' => 'span1')); ?> X 
                    <?php echo $this->Form->input('Media.h', array('error' => false, 'type' => 'text', 'id' => 'title', 'title' => 'Title', 'placeholder' => "height", 'div' => false, 'label' => false, 'class' => 'span1')); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle loader" type="submit">Add Media</button>
    </div>

    <?php $this->Form->end(); ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $(".type").live('change',function(e){
            $val = $(this).val();
            if ( $val == 'link' ) {
                $(".uploadfile").hide();
                $(".uploaddim").hide();
                $(".extlink").show();
            }
            else if (  $val == 'image' ) {
                $(".uploadfile").show();
                $(".uploaddim").show();
                $(".extlink").hide();
            }
            else{
                $(".uploadfile").show();
                $(".uploaddim").hide();
                $(".extlink").hide();
            }
        });
        
    });

</script>