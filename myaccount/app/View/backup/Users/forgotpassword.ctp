<div class="box">
	<div class="block">
		<div class="section">
			<div class="alert dismissible alert_light">
				<img width="24" height="24" src="./images/icons/small/grey/alarm_bell.png">
				<strong>Password manager</strong> to get a new password please enter information below
			</div>
			<?php if ( $msg != '' ){ ?>
				<div class="alert alert_red">
					<img width="24" height="24" src="/images/icons/small/white/locked.png">
					<strong>Invalid username and/or email!</strong>
				</div>
				<?php
				}
			?> 

		</div>

		<?php echo $this->Form->create('User',array('action'=>'forgotpassword','id'=>'rpassform','class'=>'validate_form'));?>
		<fieldset class="label_top top">
			<label for="username">Username<span>enter your username</span></label>
			<div>
				<?php echo $this->Form->input('username',array('tabindex'=>'1', 'label'=>false,'autofocus','id'=>'username','class'=>'required text', 'maxlength'=>30, 'minlength'=>4 )); ?>
			</div>
		</fieldset>
		<fieldset class="label_top">
			<label for="password">Email<span>enter your email address</span></label>
			<div>
				<?php echo $this->Form->input('email',array('tabindex'=>'2', 'minlength'=>10, 'maxlength'=>60, 'type'=>'text','label'=>false,'id'=>'email','class'=>'required text email')); ?>

			</div>
		</fieldset>
		<div class="button_bar clearfix">
			<button class="green" type="submit">
				<img height="24" width="24" alt="Bended Arrow Right" src="/images/icons/small/grey/bended_arrow_left.png">
				<span>Generate password</span>
			</button>
			<button class="dark blue no_margin_bottom link_button send_right" data-link="/users/login">
				<div class="ui-icon ui-icon-check"></div>
				<span>Login In</span>
			</button>
		</div>
		</form>




	</div>
</div>