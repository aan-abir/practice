
<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>Full Name</th>
                <th>Email</th>
                <th>Facebook ID</th>
                <th>Twitter ID</th>
                <th>Google ID</th>
                <th>Youtube</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if (is_array($allMashup) && count($allMashup) > 0):
                    $i = 1;
                    foreach ($allMashup as $mashup):
                    ?>
                    <tr>
                        <td><?php echo $mashup['Socialbrand']['firstname']. ' ' .$mashup['Socialbrand']['lastname']; ?></td>
                        <td><?php echo $mashup['Socialbrand']['email']; ?></td>
                        <td><?php echo $mashup['Socialbrand']['facebook_page_id']; ?></td>
                        <td><?php echo $mashup['Socialbrand']['twitter_url']; ?></td>
                        <td><?php echo $mashup['Socialbrand']['google_url']; ?></td>
                        <td><?php echo $mashup['Socialbrand']['youtube_url']; ?></td>
                        <td>
                            <div class="controls center">
                   
                                 <a title="Your Current MashUp" href="<?php echo Router::url('seemashpost/' . $mashup['Socialbrand']['id']); ?>" class="openModalDialog">view mash</a>  | 
                                <a href="<?php echo Router::url('editmashup/' . $mashup['Socialbrand']['id']); ?>" title="Edit/View Mashup" class="tip">edit/view</a> | 
                                <a href="<?php echo Router::url('deletemashup/' . $mashup['Socialbrand']['id']); ?>" title="Remove Mashup?" class="tip callAction">delete</a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        $i++;
                        endforeach;
                    else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="7">No MashUp Found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
            </div>
