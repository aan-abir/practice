<div class="marginB10"></div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'pr_user_proxy_edit',$data['Pruserproxy']['id']), 'id' => 'addRankCampaignForm', 'method' => 'post', 'type'=>'file', 'class' => 'form-horizontal seperator')); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="ip_address">IP address:</label>
                        <?php echo $this->Form->input('Pruserproxy.ip_address', array('error' => false, 'required' => false, 'id' => 'ip_address', 'type' => 'text', 'title' => 'IP address', 'placeholder' => "IP address", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                   </div>
                </div>
            </div>
           <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="username">Username:</label>
                        <?php echo $this->Form->input('Pruserproxy.username', array('error' => false, 'required' => false, 'id' => 'username', 'type' => 'text', 'title' => 'username', 'placeholder' => "Username", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                   </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="password">Password:</label>
                        <?php echo $this->Form->input('Pruserproxy.password', array('error' => false, 'required' => false, 'id' => 'password', 'type' => 'text', 'title' => 'password', 'placeholder' => "Password", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                   </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="password">Port:</label>
                        <?php echo $this->Form->input('Pruserproxy.port', array('error' => false, 'required' => false, 'id' => 'port', 'type' => 'text', 'title' => 'port', 'placeholder' => "Port", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                   </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" class="btn btn-info marginR10">Update PR User Proxy</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>

</div>

</div>


