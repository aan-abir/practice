<?php
// auto refresh page to fetch  data
if (isset($refreshpage)) {
    if ($refreshpage == 'yes') {
        echo '<meta http-equiv="refresh" content="30">';
    }
}
?>

<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Instant Rank Results</span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>All Results by Keyword</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($instantRanks) && count($instantRanks) > 0):
                            foreach ($instantRanks as $arr):
                                $keyword = $arr['Rankinstant']['keyword'];
                                $status = $arr['Rankinstant']['status'];
                                $engine = $arr['Rankinstant']['engine'];
                                $locale = $arr['Rankinstant']['locale'];
                                $rankdate = $arr['Rankinstant']['update_time'];
                                ?>
                                <tr>
                                    <td><h3><?php echo $keyword; ?></h3>Engine: <?php echo $engine; ?> <?php if ($engine != 'amazon' && $engine != 'youtube'): ?> || Locale: <?php echo $locale; ?> <?php endif; ?> || Date: <?php echo $rankdate; ?> || Status: <?php echo $status; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="responsive table table-bordered" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Url</th>
                                                    <th>Position</th>
                                                    <th>Page</th>
                                                    <th>Title</th>
                                                    <th>Description</th>
                                                </tr>
                                            </thead>                                            
                                            <?php
                                            $position = unserialize($arr['Rankinstant']['position']);
                                            if (is_array($position) && count($position)):
                                                foreach ($position as $domain => $pos):
                                                    if ($pos != 0):
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $domain; ?></td>
                                                            <td><?php echo $pos[0]; ?></td>
                                                            <td><?php echo $pos[1]; ?></td>
                                                            <td><?php echo $pos[2]; ?></td>
                                                            <td><?php echo $pos[3]; ?></td>
                                                        </tr>                                                            
                                                        <?php
                                                    else:
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $domain; ?></td>
                                                            <td colspan="4">Not ranked</td>
                                                        </tr>
                                                    <?php
                                                    endif;
                                                endforeach;
                                                ?>
                                                <?php
                                            else:
                                                ?>
                                                <tr>
                                                    <td colspan="5">No results found. Please wait while we process.</td>
                                                </tr>
                                            <?php
                                            endif;
                                            ?>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td colspan="5">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
