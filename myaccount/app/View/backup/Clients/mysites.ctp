<div class="content">
    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($mysites)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
        <thead>
            <tr>
                <th>Domain</th>
                <th>Username</th>
                <th>Password</th>
                <th>Age</th>
                <th>PR</th>
                <th>Ljflow</th>
                <th>Cflow</th>
                <th>Tflow</th>
                <th>CMS</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($mysites) > 0):
                foreach ($mysites as $ms):
                    $loginUrl = 'http://' . str_replace('http://', '', $ms['Domain']['domain']) . '/wp-admin';
                    $domain = 'http://' . str_replace('http://', '', $ms['Domain']['domain']);
                    ?>
                    <tr>
                        <td><a href="<?php echo $domain; ?>" target="_blank" title="Go to Domain" class="tip"><?php echo $ms['Domain']['domain']; ?></a></td>
                        <td><?php echo $ms['Domain']['ausername'] ? $ms['Domain']['ausername'] : '--'; ?></td>
                        <td><?php echo $ms['Domain']['apassword'] ? $ms['Domain']['apassword'] : '--'; ?></td>
                        <td><?php echo $ms['Domain']['age']; ?></td>
                        <td><?php echo $ms['Domain']['pr']; ?></td>
                        <td><?php echo $ms['Domain']['ljpr']; ?></td>
                        <td><?php echo $ms['Domain']['cflow']; ?></td>
                        <td><?php echo $ms['Domain']['tflow']; ?></td>
                        <td><?php echo $ms['Domain']['cms']; ?></td>
                        <td>
                            <div class="controls center">
                                <a href="<?php echo Router::url('editdomain/' . $ms['Domain']['id']); ?>" title="Edit Domain" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                <a href="<?php echo Router::url('deletedomain/' . $ms['Domain']['id']); ?>" title="Remove Domain?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="11">You have not added any site yet!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>
</div>
