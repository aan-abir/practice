<div style="margin-bottom: 20px;"></div>
<style>
    .circular-item input{display:none;}
    .circular-item .valueDiv{display:block;position:absolute;top:20%;text-align:center;width:82px;}
    .circular-item .valueTop{padding: 3px 0;display:block;font-size:16px;}
    .circular-item .valueBottom{padding: 3px 0;display:block;font-size:18px;font-weight:bold;}
    .valueRed{color:#ED7A53;}
    .valueBlue{color:#88BBC8;}
    .valueGreen{color:#9FC569;}
</style>

<div class="row-fluid">
    <div class="span12">
        <!--<div class="centerContent">-->

        <div class="" style="position: relative;">
            <?php //echo $this->Html->image('/images/auto.png', array('class' => 'autoclass', 'id' =>'amclass', 'alt' => 'Automation Sites')); ?>
            <div dir="ltr" class="circle-stats">
                <div class="circular-item tipB" title="You have created <?php echo intval(@$finalCircleArr[12]['service_used']); ?> Campaigns out of <?php echo intval(@$finalCircleArr[12]['service_limit']); ?>">
                    <a href="./addcampaign">
                        <div class="valueDiv divRed">
                            <span class="valueTop"><?php echo @$finalCircleArr[12]['service_used']; ?></span>
                            <span class="valueBottom valueBlue"><?php echo $finalCircleArr[12]['service_limit'] != '9999999' ? @$finalCircleArr[12]['service_limit'] : '<span style="font-size:10px;">Unlimited<span>'; ?></span>
                        </div>
                        <input type="text" value="<?php echo @$finalCircleArr[12]['percentage_used']; ?>" class="redCircle" />
                        <br><span>Campaigns</span>
                    </a>
                </div>
                <div class="circular-item tipB" title="You have created <?php echo intval(@$finalCircleArr[9]['service_used']); ?> Sitewide Links out of <?php echo intval(@$finalCircleArr[9]['service_limit']); ?>">
                    <!--<span class="icon icomoon-icon-fire"></span>-->
                    <a href="./addblogroll">
                        <div class="valueDiv divRed">
                            <span class="valueTop"><?php echo @$finalCircleArr[9]['service_used']; ?></span>
                            <span class="valueBottom valueGreen"><?php echo $finalCircleArr[9]['service_limit'] != '9999999' ? @$finalCircleArr[9]['service_limit'] : '<span style="font-size:10px;">Unlimited<span>'; ?></span>
                        </div>
                        <input type="text" value="<?php echo @$finalCircleArr[9]['percentage_used']; ?>" class="redCircle" />
                        <br><span>BlogRoll</span>
                    </a>
                </div>
                <div id="autoPremium"  class="circular-item tipB" title="<?php echo intval(@$finalCircleArr[3]['service_used']); ?> Article Posted to Premium out of <?php echo intval(@$finalCircleArr[3]['service_limit']); ?>">
                    <!--<span class="icon icomoon-icon-busy"></span>-->
                    <a href="./addsubmission">
                        <div class="valueDiv divRed">
                            <span class="valueTop"><?php echo @$finalCircleArr[3]['service_used']; ?></span>
                            <span class="valueBottom valueGreen"><?php echo $finalCircleArr[3]['service_limit'] != '9999999' ? @$finalCircleArr[3]['service_limit'] : '<span style="font-size:10px;">Unlimited<span>'; ?></span>
                        </div>
                        <input type="text" value="<?php echo @$finalCircleArr[3]['percentage_used']; ?>" class="blueCircle" />
                        <br><span>Premium</span>
                    </a>
                </div>
                <!--
                <div  class="circular-item tipB" title="<?php echo intval(@$finalCircleArr[4]['service_used']); ?> Article Posted to Prime out of <?php echo intval(@$finalCircleArr[4]['service_limit']); ?>">
                    <a href="./addsubmission">
                        <div class="valueDiv divRed">
                            <span class="valueTop"><?php echo @$finalCircleArr[4]['service_used']; ?></span>
                            <span class="valueBottom valueRed"><?php echo $finalCircleArr[4]['service_limit'] != '9999999' ? @$finalCircleArr[4]['service_limit'] : '<span style="font-size:10px;">Unlimited<span>'; ?></span>
                        </div>
                        <input type="text" value="<?php echo @$finalCircleArr[4]['percentage_used']; ?>" class="greenCircle" />
                        <br><span>Prime</span>
                    </a>
                </div>

                <div  class="circular-item tipB" title="<?php echo intval(@$finalCircleArr[5]['service_used']); ?> Article Posted to LowGrade out of <?php echo intval(@$finalCircleArr[5]['service_limit']); ?>">
                    <a href="./addsubmission">
                        <div class="valueDiv divRed">
                            <span class="valueTop"><?php echo @$finalCircleArr[5]['service_used']; ?></span>
                            <span class="valueBottom valueBlue"><?php echo $finalCircleArr[5]['service_limit'] != '9999999' ? @$finalCircleArr[5]['service_limit'] : '<span style="font-size:10px;">Unlimited<span>'; ?></span>
                        </div>
                        <input type="text" value="<?php echo @$finalCircleArr[5]['percentage_used']; ?>" class="redCircle" />
                        <br><span>LowGrade</span>
                    </a>
                </div>
                -->
                <div class="circular-item">

                </div>

                <span style="border-top: 1px solid gray; display: inline-block;">
                    <div style="height: 25px; text-align: center; border-left: 1px solid; border-right: 1px solid;">Buy Extra Credits to Use</div>

                    <div  class="circular-item tipB" title="<?php echo intval(@$finalCircleArr[5]['service_used']); ?> Article Posted to LowGrade out of <?php echo intval(@$finalCircleArr[13]['service_limit']); ?>">
                        <!--<span class="icon icomoon-icon-busy"></span>-->
                        <a href="./linkemperor">
                            <div class="valueDiv divRed">
                                <span class="valueTop"><?php echo @$finalCircleArr[13]['service_used']; ?></span>
                                <span class="valueBottom valueBlue"><?php echo $finalCircleArr[13]['service_limit'] != '9999999' ? @$finalCircleArr[13]['service_limit'] : '<span style="font-size:10px;">Unlimited<span>'; ?></span>
                            </div>
                            <input type="text" value="<?php echo @$finalCircleArr[13]['percentage_used']; ?>" class="blueCircle" />
                            <br><span>LinkEmperor</span>
                        </a>
                    </div>

                    <div  class="circular-item tipB" title="<?php echo intval(@$finalCircleArr[5]['service_used']); ?> Article Posted to LowGrade out of <?php echo intval(@$finalCircleArr[11]['service_limit']); ?>">
                        <a href="./linkemperor">
                            <div class="valueDiv divRed">
                                <span class="valueTop"><?php echo @$finalCircleArr[11]['service_used']; ?></span>
                                <span class="valueBottom valueBlue"><?php echo $finalCircleArr[11]['service_limit'] != '9999999' ? @$finalCircleArr[11]['service_limit'] : '<span style="font-size:10px;">Unlimited<span>'; ?></span>
                            </div>
                            <input type="text" value="<?php echo @$finalCircleArr[11]['percentage_used']; ?>" class="greenCircle" />
                            <br><span>BazokaBlast</span>
                        </a>
                    </div>
                    <div class="circular-item tipB" title="You have used <?php echo intval(@$finalCircleArr[8]['service_used']); ?> Prease Release out of <?php echo intval(@$finalCircleArr[8]['service_limit']); ?>" style="width: 85px !important;">
                        <a href="./orderpressrelease">
                            <div class="valueDiv divRed">
                                <span class="valueTop"><?php echo @$finalCircleArr[8]['service_used']; ?></span>
                                <span class="valueBottom valueBlue"><?php echo $finalCircleArr[8]['service_limit'] != '9999999' ? @$finalCircleArr[8]['service_limit'] : '<span style="font-size:10px;">Unlimited<span>'; ?></span>
                            </div>
                            <input type="text" value="<?php echo @$finalCircleArr[8]['percentage_used']; ?>" class="blueCircle" />
                            <br><span>Press Release</span>
                        </a>
                    </div>

                    <div class="circular-item tipB" title="You have used <?php echo intval(@$finalCircleArr[8]['service_used']); ?> Link Indexing out of <?php echo intval(@$finalCircleArr[8]['service_limit']); ?>" style="width: 85px !important;">
                        <a href="./orderpressrelease">
                            <div class="valueDiv divRed">
                                <span class="valueTop"><?php echo @$finalCircleArr[8]['service_used']; ?></span>
                                <span class="valueBottom valueBlue"><?php echo $finalCircleArr[8]['service_limit'] != '9999999' ? @$finalCircleArr[8]['service_limit'] : '<span style="font-size:10px;">Unlimited<span>'; ?></span>
                            </div>
                            <input type="text" value="<?php echo @$finalCircleArr[8]['percentage_used']; ?>" class="redCircle" />
                            <br><span>Link Indexing</span>
                        </a>
                    </div>

                </span>

                <div class="circular-item">

                </div>

                <div  class="circular-item tipB" title="Extra Credits">
                    <!--<span class="icon icomoon-icon-busy"></span>-->
                    <a href="./myplan">
                        <div class="valueDiv divRed">
                            <span class="valueTop" style="padding-top: 13px;"><?php echo '$' . intval(AuthComponent::user('extra_credit')); ?></span>
                        </div>
                        <input type="text" value="100" class="redCircle" />
                        <br><span>Extra Credit</span>
                    </a>
                </div>
            </div>
        </div>


        <!--   <div class="centerContent">
        <ul class="bigBtnIcon">
        <li>
        <a href="#" title="How Many Extra Credits You have now" class="tipB">
        <span class="icon icomoon-icon-users"></span>
        <span class="txt">Extra Credit</span>
        <span class="notification">$$</span>
        </a>
        </li>
        </ul>
        </div>
        -->
    </div><!-- End .span12 -->




</div>

<div class="row-fluid" style="margin-top: 20px;">
    <div class="span8">
        <div class="box calendar gradient">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-calendar"></span>
                    <span>Previous, Current and Future Activity Log</span>
                </h4>
                <!-- <a href="#" class="minimize">Minimize</a> -->
            </div>
            <div class="content noPad">
                <div id="activityLog"></div>
            </div>

        </div><!-- End .box -->
    </div><!-- End .span8 -->

    <div class="span4">

        <div class="box">

            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span>Campaigns</span>
                </h4>
                <a onclick='window.location.href = "<?php echo Router::url('addcampaign'); ?>"' href="<?php echo Router::url('addcampaign'); ?>" style="background: none; border: none; text-indent: 0; width:55px;display: inline-block;">Add  <span class="icon16 cut-icon-plus-2"></span></a>
            </div>
            <div class="content noPad">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Campaign Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($siteAllCamps) && !empty($siteAllCamps)):
                            foreach ($siteAllCamps as $k => $camp):
                                ?>
                                <tr>
                                    <td><?php echo $k + 1; ?></td>
                                    <td><?php echo $camp['Campaign']['campignname'] ?></td>
                                    <td style="text-align: left; width: 50px;">
                                        <div class="controls">
                                            <a href="<?php echo Router::url('editcampaign/' . $camp['Campaign']['id']); ?>" title="Edit Campaign" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                            <a href="<?php echo Router::url('deletecampaign/' . $camp['Campaign']['id']); ?>" title="Remove Campaign" class="tip"><span class="icon12 icomoon-icon-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr><td colspan="3" style="text-align: center;"><a href="<?php echo Router::url('addcampaign'); ?>">Create Your Campaing Now</a></td></tr>

                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span4 -->
</div><!-- End .row-fluid -->

<script type="text/javascript">
    // document ready function
    $(document).ready(function() {

        //------------- Full calendar  -------------//
        $(function() {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            //front page calendar
            $('#activityLog').fullCalendar({
                //isRTL: true,
                //theme: true,
                header: {
                    left: 'title,today',
                    center: 'prev,next',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    prev: '<span class="icon24 icomoon-icon-arrow-left-2"></span>',
                    next: '<span class="icon24 icomoon-icon-arrow-right-2"></span>'
                },
                editable: true,
                events: [
<?php echo $calData; ?>
                ]
            });
        });

        //Boostrap modal
        $('#myModal').modal({show: false});

        //add event to modal after closed
        $('#myModal').on('hidden', function() {
            console.log('modal is closed');
        })


        //circular progrress bar
        $(function() {

            $(".greenCircle").knob({
                'min': 0,
                'max': 100,
                'readOnly': true,
                'width': 80,
                'height': 80,
                'fgColor': '#9FC569',
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true
            })
            $(".redCircle").knob({
                'min': 0,
                'max': 100,
                'readOnly': true,
                'width': 80,
                'height': 80,
                'fgColor': '#ED7A53',
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true
            })
            $(".blueCircle").knob({
                'min': 0,
                'max': 100,
                'readOnly': true,
                'width': 80,
                'height': 80,
                'fgColor': '#88BBC8',
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true
            })

        });
        $("#amclass").css({left: ($("#autoPremium").position().left - 5) + 'px'});
        $("#amclass").css({top: ($("#autoPremium").position().top - 20) + 'px'});
        $(window).resize(function() {
            $("#amclass").css({left: ($("#autoPremium").position().left - 5) + 'px'});
            $("#amclass").css({top: ($("#autoPremium").position().top - 20) + 'px'});
        });


    });

</script>