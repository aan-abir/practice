<?php
    if (AuthComponent::user('link_density')):
    ?>
    <style>
        .zeroWidth{width:0 !important; padding:0 !important; border:0 !important;}
    </style>

    <div class="content">
        <form name="ld_Frm" action="ldreport" method="post">
            <!--<span style="float:left;">New search:</span>-->
            <input name="domain" style="float:left;width: 600px;" type="text" placeholder="Enter your domain" class="top-search text" id="search_input">
            <a href="#myModal" id="modal_pop" onclick="$('#search_input_modal').val($('#search_input').val())" style="float:left;margin-left: 10px;display: none;" class="btn marginR10 marginB10" data-toggle="modal">Search</a>
            <button style="float:left;margin-left: 10px;" class="btn btn-info" type="button" onclick="search_func()">Search</button>
        </form>
    </div>

    <div class="content">
        <div class="row-fluid">
            <div class="span12">
                <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                    <h4 style="color: #074f14;">Domain Reports <span class="help-block-inline"> below all of your previous search are listed </span></h4>
                </div>
            </div>
        </div>
    </div>
    <div class="marginB10"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allDomains)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"><strong></strong></th>
                            <th><strong>Domain</strong></th>
                            <th><strong>Last Updated</strong></th>
                            <th><strong>Total Link(s)</strong></th>
                            <th><strong>Total Ref(s)</strong></th>
                            <!--<th><strong>Link Type</strong></th>-->
                            <th><strong>Actions</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (isset($allDomains) && count($allDomains) > 0):
                                foreach ($allDomains as $row):
                                    $arr = $row['Ldreport'];
                                    $top_id = $this->NitroCustom->top_id($arr['domain']);
                                    $same_domain_count = $this->NitroCustom->same_domain_count($arr['domain']);
                                ?>
                                <tr <?php if ($top_id != $arr['id'] && $top_id > 0) echo 'class="hide top_id_' . $top_id . '"'; ?>>
                                    <td class="zeroWidth"></td>
                                    <td style="text-align:left;">
                                        <?php if ($top_id == $arr['id'] && $same_domain_count > 1) echo $this->Html->image('/images/add.png', array('onclick' => 'onclick_func(' . $arr['id'] . ')', 'style' => 'cursor:pointer')); ?>
                                        <a <?php if ($top_id != $arr['id'] && $top_id > 0) echo 'style="margin-left:20px !important"'; ?> href="<?php echo Router::url('analyzedomain/' . urlencode($arr['domain']) . '/' . $arr['id']) ?>"><?php echo $arr['domain']; ?></a>
                                    </td>
                                    <td><?php echo date("d M Y", strtotime($arr['modified'])); ?></td>
                                    <td><?php echo $arr['ExtBackLinks']; ?></td>
                                    <td> <?php echo $arr['RefDomains']; ?> </td>
                                    <!--<td><?php echo 'Processed'; ?></td>-->
                                    <td>

                                        <div class="controls center">
                                            <a href="javascript: void(0);" onclick="refresh_func('<?php echo $arr['domain'] ?>')" title="Refresh" class="minia-icon-refresh "></a> |
                                            <a href="<?php echo Router::url('ldreport_delete/' . $arr['id']); ?>" title="remove this domain" class="icon-remove callAction"></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                    endforeach;
                                else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="5">No record found!</td>
                            </tr>
                            <?php
                                endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <!----------------- Modal ------------->
    <div id="myModal" class="modal hide fade" style="display: none; ">
        <form name="ld_Frm_modal" action="ldreport" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
                <h3>Your available credit: 67000</h3>
            </div>
            <div class="modal-body">
                <div class="paddingT15 paddingB15">
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="campaign_name">Select Backlink limit:</label>
                                <div class="span4">
                                    <?php echo $this->Form->select('Ldreportcron.GetTopBackLinksAnalysisResUnitsCost', array(5000 => '5000', 10000 => '10000', 20000 => '20000'), array('required' => false, 'required' => false, 'id' => 'pr', 'title' => 'Select Page Rank', 'div' => false, 'label' => false, 'class' => 'span12 select', 'empty' => false)); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <!--<label class="form-label span4" for="campaign_name">Domain:</label>-->
                                <div class="span4">
                                    <?php echo $this->Form->input('Ldreportcron.domain', array('error' => false, 'required' => false, 'id' => 'search_input_modal', 'type' => 'text', 'title' => 'Campaign Name', 'placeholder' => "Campaign Name", 'type' => 'hidden', 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<h4>Text in a modal</h4>-->
                    <!--<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem.</p>-->

                </div>

            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">Close</a>
                <a onclick="$('#myModal').hide();
                    document.forms.ld_Frm_modal.submit();" class="btn btn-primary">Submit</a>
            </div>
        </form>
    </div>
    <!----------------- Modal ------------->

    <script>
        function search_func()
        {
            var domain = $('#search_input').val();
            //alert(domain);
            if (domain == '')
            {
                alert('Search field could not be empty!');
                return false;
            }
            else
            {
                $('#modal_pop').click();
            }
            //document.forms.ld_Frm.submit();
        }

        function onclick_func(id)
        {
            //alert(id);
            $('.top_id_' + id).toggle();
        }

        function refresh_func(domain)
        {
            var con = confirm('Refresh data will be found within 24 hours. Are you sure to refresh this domain!');
            if (con)
            {
                $('#search_input').val(domain);
                search_func();
            }
        }
    </script>
    <?php
        else:
    ?>
    <div class="marginB10"></div>
    <div class="content">
        You currently don't have access to Link Density.<br/>
        <span><a target="_blank" href="https://inetinnovation.zaxaa.com/oto/y/2629408512013/1">Click here</a> to upgrade your account.</span>
    </div>
    <div class="marginB10"></div>
    <?php
        endif;
?>