<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    
                </h4>
            </div>
            <div class="content">
                <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'editprivateblogroll', $data['Blogroll']['id']), 'id' => 'editBlogrollForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="username">Choose Network Type:</label>
                            <div class="span4">
                                <select id="selectNetwork" name="data[Blogroll][type]">
                                    <option value="1" <?php echo $data['Domain']['networktype'] == 1 ? 'selected="selected"' : ''; ?> >My Sites</option>
                                    <option value="2" <?php echo $data['Domain']['networktype'] == 2 ? 'selected="selected"' : ''; ?> >Manual Sites</option>
                                </select>
                                <?php
                                //echo $this->Form->select('Blogroll.type', array('1' => 'My Sites', '2' => 'Manual Site'), array('required' => false, 'id' => 'selectNetwork', 'title' => 'Enter Domain network', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="data[Blogroll][id]" value="<?php echo $data['Blogroll']['id']; ?>" />
                <div class="form-row row-fluid" <?php echo $data['Domain']['networktype'] == 1 ? '' : 'style="display: none;"'; ?>  id="mysitenetwork">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="username">Choose From Mysite:</label>
                            <div class="span4">
                                <select name="data[Blogroll][domain_id1]">
                                    <?php
                                    foreach ($mySite as $msite) {
                                        ?>
                                        <option value="<?php echo $msite['Domain']['id']; ?>" <?php echo $data['Blogroll']['domain_id'] == $msite['Domain']['id'] ? 'selected="selected"' : ''; ?> ><?php echo $msite['Domain']['domain']; ?></option>
                                        <?php
                                    }
                                    //echo $this->Form->select('Blogroll.domain_id2', $manualSite, array( 'title' => 'Enter Domain network', 'div' => false, 'label' => false, 'class' => 'span2 select'));
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid" <?php echo $data['Domain']['networktype'] == 2 ? '' : 'style="display: none;"'; ?> id="manualsitenetwork">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="username">Choose From Manual:</label>
                            <div class="span4">
                                <select name="data[Blogroll][domain_id2]">
                                    <?php
                                    foreach ($manualSite as $msite) {
                                        ?>
                                        <option value="<?php echo $msite['Domain']['id']; ?>" <?php echo $data['Blogroll']['domain_id'] == $msite['Domain']['id'] ? 'selected="selected"' : ''; ?> ><?php echo $msite['Domain']['domain']; ?></option>
                                        <?php
                                    }
                                    //echo $this->Form->select('Blogroll.domain_id2', $manualSite, array( 'title' => 'Enter Domain network', 'div' => false, 'label' => false, 'class' => 'span2 select'));
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="username">Select Campaign:</label>
                            <div class="span4">
                                <?php
                                echo $this->Form->select('Blogroll.campaign_id', $allCampaign, array('required' => false, 'title' => 'Enter Client My Site', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="status">Choose Option:</label>
                            <div class="left marginT5 marginR10">
                                <input type="radio" name="data[Blogroll][setting_option]" id="Sidebar" value="1" <?php echo ($data['Blogroll']['setting_option'] == 1) ? 'checked="checked"' : ''; ?> />
                                Add Link to Sidebar
                            </div>
                            <div class="left marginT5 marginR10">
                                <input type="radio" name="data[Blogroll][setting_option]" id="Footer" value="2" <?php echo ($data['Blogroll']['setting_option'] == 2) ? 'checked="checked"' : ''; ?>/>
                                Add Link to footer
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="form-actions">
                                <div class="span3"></div>
                                <div class="span4 controls">
                                    <button type="submit" class="btn btn-info marginR10">Update Blogroll</button>
                                    <button class="btn btn-danger">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    // document ready function
    $(document).ready(function() {
        $('#selectNetwork').change(function() {
            if($('#selectNetwork').val() == 1){
                $('#manualsitenetwork').hide();
                $('#mysitenetwork').show();
            }
            if($('#selectNetwork').val() == 2){
                $('#mysitenetwork').hide();
                $('#manualsitenetwork').show();
            }
        });

    });
</script>