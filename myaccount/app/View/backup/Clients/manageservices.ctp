<style type="text/css">
    .textLeft{text-align: left !important;}
    .textRight{text-align: right !important;}
</style>
<div class="row-fluid">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'manageservices'), 'method' => 'post')); ?>
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Your current extra credit is: <?php echo number_format(floatval($userExtraCredit), 2); ?></span>
                    <input type="hidden" id="currentExtraCreditInput" name="data[Extra][current_extra_credit]" value="<?php echo floatval($userExtraCredit); ?>"/>
                </h4>
            </div>
            <div class="content">
                <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'manageservices'), 'method' => 'post')); ?>
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="textLeft">Service Name</th>
                            <th class="textRight">Unit Price</th>
                            <th class="textRight">Package Service Limit</th>
                            <th class="textRight">Current Service Limit</th>
                            <th class="textRight">Service Used</th>
                            <th class="textLeft">Add Extra Unit</th>
                            <th class="textRight">Extra Credit Needed</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($userCredits) && $userCredits):
                            foreach ($userCredits as $k => $arr):
                                $serviceId = $arr['Usercredit']['service_id'];
                                ?>
                                <tr>
                                    <td><?php echo $k + 1; ?></td>
                                    <td class="textLeft"><?php echo $arr['Usercredit']['servicename']; ?></td>
                                    <td class="textRight" id="unitPrice_<?php echo $serviceId; ?>"><?php echo $arr['Usercredit']['unit_price']; ?></td>
                                    <td class="textRight"><?php echo $arr['Usercredit']['service_limit']; ?></td>
                                    <td class="textRight"><?php echo $arr['Usercredit']['current_service_limit']; ?></td>
                                    <td class="textRight"><?php echo $arr['Usercredit']['service_used']; ?></td>
                                    <td class="textLeft">
                                        <?php
                                        echo $this->Form->input('Extra.extra_unit.' . $serviceId, array('error' => false, 'required' => false, 'type' => 'text', 'class' => 'extraUnitInput', 'title' => 'Service Limit', 'placeholder' => "Quantity", 'div' => false, 'label' => false, 'service_id' => $serviceId, 'value' => 0));
                                        ?>
                                    </td>
                                    <td class="textRight">
                                        <span style="color: red;" class="extraCreditPerServiceSpan" id="extraCreditPerService_<?php echo $serviceId; ?>">0.00</span>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="5"><span style="display: none;">You need to add <span id="actualExtraCreditNeededSpan" style="color: red;">0.00</span> credit to you account to apply these changes!</span></td>
                                <td><button class="btn btn-info send-middle" onclick="submitServiceUpdateForm(this);" type="button">Apply Changes</button></td>
                                <td class="textRight">
                                    Total: <span style="color: red;" id="extraCreditNeededNotificationSpan">0.00</span>
                                    <input type="hidden" id="extraCreditNeededInput" name="data[Extra][total_extra_credit_required]" />
                                </td>
                            </tr>                    
                            <?php
                        else:
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="7">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<script type="text/javascript">
    $('.extraUnitInput').on('blur', function(evt) {
        $('#actualExtraCreditNeededSpan').parent('span').hide();
        service_id = $(this).attr('service_id');
        unit_price = parseFloat($.trim($('#unitPrice_' + service_id).html())).toFixed(2);

        extra_unit = parseInt($.trim($(this).val()));
        if (!extra_unit || extra_unit < 0) {
            extra_unit = 0;
            $(this).val(extra_unit);
        }
        service_extra_credit = (unit_price * extra_unit).toFixed(2);

        //alert(service_extra_credit);

        $('#extraCreditPerService_' + service_id).empty().html(service_extra_credit);

        total_extra_credit = 0;

        $('.extraCreditPerServiceSpan').each(function() {
            service_extra_this = parseFloat($.trim($(this).html()));
            total_extra_credit = (total_extra_credit + service_extra_this);
        });

        total_extra_credit = total_extra_credit.toFixed(2);
        $('#extraCreditNeededNotificationSpan').empty().html(total_extra_credit);
        $('#extraCreditNeededInput').val(total_extra_credit);
        //alert(service_extra_credit);
        return false;
    });

    function submitServiceUpdateForm(evt) {
        $('#actualExtraCreditNeededSpan').parent('span').hide();
        extra_credit_needed = $('#extraCreditNeededInput').val();
        current_extra_credit = $('#currentExtraCreditInput').val();

        actual_extra_credit_needed = (extra_credit_needed - current_extra_credit).toFixed(2);

        if (actual_extra_credit_needed > 0) {
            $('#actualExtraCreditNeededSpan').empty().html(actual_extra_credit_needed);
            $('#actualExtraCreditNeededSpan').parent('span').show();
            return false;
        } else {
            $("html").addClass('loadstate');
            $('#UserManageservicesForm').submit();
        }
        return false;
    }
</script>