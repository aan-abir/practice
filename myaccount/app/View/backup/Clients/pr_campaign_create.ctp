<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'pr_campaign_create'), 'id' => 'addRankCampaignForm', 'method' => 'post', 'type'=>'file', 'class' => 'form-horizontal seperator')); ?>


            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                        <h4 style="color: #074f14;">Campaign Information <span class="help-block-inline">Enter campaign details</span></h4>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="span4" for="title">Campaign Name <span class="help-block-inline">enter name of the campaign</span></label>
                        <div class="span8">
                            <?php echo $this->Form->input('Prcampaign.name', array('error' => false, 'required' => false, 'id' => 'campaign_name', 'type' => 'text', 'title' => 'Campaign Name', 'placeholder' => "Campaign Name", 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                        <h4 style="color: #074f14;">Enter domain(s) <span class="help-block-inline"> Enter one domain per line. You do not have to enter WWW.</span></h4>
                    </div>
                </div>
            </div>
            
            <div class="row-fluid">
                <div class="span12">
                    <div class="form-row">

                        <?php echo $this->Form->textarea('Prcampaign.c_urls', array('error' => false, 'id' => 'c_urls', 'title' => 'enter urls you want to track under this campaign', 'div' => false, 'label' => false, 'class' => 'span12', 'style' => 'height:220px')); ?>
                    </div>
                </div>
            </div>



            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" onclick="$('html').addClass('loadstate');" class="btn btn-info marginR10">Save PR Campaign</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>

</div>

</div>


