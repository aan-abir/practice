<?php include_once 'not_eligible_warning.ctp'; ?>
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'bazookablast'), 'id' => 'bazookablast', 'method' => 'post','class' => 'form-horizontal')); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Name of the Blast * <span class="help-block-inline">enter name of this blast</span></h4>
            </div>
            <div class="form-row">
                    <div class="span12">
                       <?php echo $this->Form->input('Linkemperor.ordername', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Name Of the Submission', 'div' => false, 'label' => false, 'class' => 'span12', 'id' => 'ordername' )); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Anchor Text * <span class="help-block-inline">enter the anchor text for your links</span></h4>
            </div>
            <div class="form-row">
                    <div class="span12">
                      <?php echo $this->Form->input('Linkemperor.anchor1', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Anchor 1', 'div' => false, 'label' => false, 'class' => 'span12','id'=>'anchor1')); ?>
                </div>
            </div>
        </div>
    </div>

       <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Anchor URL * <span class="help-block-inline red">DO NOT use <b>'http://'</b> or <b>'www.'</b> in url</span></h4>
            </div>
            <div class="form-row">
                    <div class="span12">
                       <?php echo $this->Form->input('Linkemperor.url1', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Url for Anchor 1', 'div' => false, 'label' => false, 'class' => 'span12','id' => 'url1')); ?>
                </div>
            </div>
        </div>
    </div>


    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button class="btn btn-info" type="submit">Order your Bazoka Blast</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<script type="text/javascript">
    // document ready function
    $(document).ready(function() {
        $("input, textarea, select").not('.nostyle').uniform();
        $("#bazookablast").validate({
            rules: {
                'data[Linkemperor][ordername]': {
                    required: true,
                },
                'data[Linkemperor][anchor1]': {
                    required: true,
                },
                'data[Linkemperor][url1]': {
                    required: true,
                }

            },
            messages: {
                'data[Linkemperor][ordername]': {
                    required: "Please enter Order Name",
                },
                'data[Linkemperor][anchor1]': {
                    required: "Please enter Anchor Text",
                },
                'data[Linkemperor][url1]': {
                    required: "Please enter Anchor URL",
                },
            }

        });
    });
</script>