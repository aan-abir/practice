<?php $inputDefaults = array('label' => false, 'required' => false, 'error' => false, 'class' => 'span12', 'autofocus'); ?>
<div class="content">
    <?php echo $this->Form->create('Client', array('inputDefaults' => $inputDefaults, 'novalidate' => true)); ?>
    <div class="form-row row-fluid">
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label for="ManualpostPostdate" class="form-label span3" for="normal">Post Date<span class="red">*</span></label>
                <?php echo $this->Form->input('Manualpost.post_date', array('class' => 'span3 datepicker  tip', 'readonly' => 'readonly', 'type' => 'text')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label for="ManualpostDomainId" class="form-label span3">Select Domain:<span class="red">*</span></label>
                <div class="span4">
                    <?php
                    $default = '';
                    if (isset($_GET['domain'])):
                        $default = (int) trim($_GET['domain']);
                    endif;
                    echo $this->Form->select('Manualpost.domain_id', $domainList, array('empty' => array('' => 'Select Domain'), 'default' => $default));
                    ?>
                    <span class="help-inline blue">Domain | Page Rank | Trust Flow | Citation Flow</span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label for="ManualpostCategoryId" class="form-label span3">Select Post Category:<span class="red">*</span></label>
                <div class="span3">
                    <?php
                    echo $this->Form->select('Manualpost.category_id', $allCats, array('empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <label><strong>Post Title:</strong><span class="red">*</span></label>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Manualpost.post_title', array('class' => 'span8', 'type' => 'text')); ?>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <label><strong>Post Content:</strong><span class="red">*</span></label>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('Manualpost.post_content', array('class' => 'tinymce', 'required' => false, 'rows' => '25', 'cols' => '80', 'style' => 'width: 886px')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid inline_labels">
                <label class="form-label span3" for="status">Status:</label>
                <div class="span4" style="">                    
                    <?php echo $this->Form->radio('Manualpost.status', array('1' => 'Active', '0' => 'Inactive'), array('legend' => false, 'default' => 1,)); ?>
                </div>
            </div>
        </div>
    </div>     
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <?php echo $this->Form->end(); ?>
</div>

<script type="text/javascript">
    //--------------- Tinymce ------------------//
    $('textarea.tinymce').tinymce({
        // Location of TinyMCE script
        script_url: '<?php echo BASEURL; ?>plugins/forms/tiny_mce/tiny_mce.js',
        // General options
        theme: "advanced",
        plugins: "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
        // Theme options
        theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,styleselect,formatselect,fontselect,fontsizeselect,code,",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,
        // Example content CSS (should be your site CSS)
        content_css: "css/main.css",
        // Drop lists for link/image/media/template dialogs
        template_external_list_url: "lists/template_list.js",
        external_link_list_url: "lists/link_list.js",
        external_image_list_url: "lists/image_list.js",
        media_external_list_url: "lists/media_list.js",
        // Replace values for the template plugin
        template_replace_values: {
            username: "SuprUser",
            staffid: "991234"
        }
    });
    //------------- Datepicker -------------//
    if ($('.datepicker').length) {
        $('.datepicker').datepicker({
            showOtherMonths: true,
            //dateFormat: 'yy-m-d'
            minDate: '+1'
        });
    }
</script>