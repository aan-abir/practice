<?php include_once 'not_eligible_warning.ctp'; ?>
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'addblogroll'), 'id' => 'addBlogrollForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>

    <?php echo $this->Form->hidden('Custom.networktype',array('value'=>3)) ?>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="networktype">Select Network Type:</label>
                <div class="span4">
                    <?php
                    $domTypeArr = array('3' => 'PageOneEngine Network', '4' => 'My Private Sites');
                    echo $this->Form->select('Custom.networktype', $domTypeArr, array('required' => false, 'id' => 'selectNetwork', 'title' => 'Enter Domain network', 'rel' => Router::url('domaindropdown'), 'dropdownid' => 'domainDropDownAjaxLoaded', 'div' => false, 'label' => false, 'class' => 'span2 select fetchDropdown', 'empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="CustomStatus0">Campaign or Custom Link?:</label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Single][option]" id="CustomStatus0" value="0" checked="checked" />
                    Campaign
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Single][option]" id="CustomStatus1" value="1" />
                    Custom Link
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid" id="campaignDropDownSelect">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="campaign_id">Select Campaign:</label>
                <div class="span4">
                    <?php
                    echo $this->Form->select('Blogroll.campaign_id', $allCampaign, array('required' => false, 'title' => 'Enter Client My Site', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid" id="customAnchorUrlInput" style="display: none;">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">Enter Custom Anchor:</label>
                <div class="span3">
                    <?php echo $this->Form->input('Single.anchortexts', array('type' => 'text', 'title' => 'Anchor Text', 'placeholder' => 'Anchor Text', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <?php echo $this->Form->input('Single.internalPageUrl', array('type' => 'text', 'title' => 'Anchor Link', 'placeholder' => 'Anchor Link', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span2">
                    <?php echo $this->Form->checkbox('Single.nofollow', array('title' => 'check for nofollow', 'required' => false, 'div' => false, 'label' => 'Nofollow', 'class' => 'span1 nostyle', 'style' => '')); ?> No Follow
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="status">Choose Option:</label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Blogroll][setting_option]" id="UserStatus1" value="1" checked="checked" />
                    Add Link to Sidebar
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Blogroll][setting_option]" id="UserStatus0" value="2" />
                    Add Link to footer
                </div>
            </div>
        </div>
    </div>
    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Add New Sitewide Link</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

<script type="text/javascript">

    $('input[type="radio"][name="data[Single][option]"]').change(function() {
        var option_val = $(this).val();
        if (option_val == 0) {
            $('#campaignDropDownSelect').show();
            $('#customAnchorUrlInput').hide();
        } else if (option_val == 1) {
            $('#customAnchorUrlInput').show();
            $('#campaignDropDownSelect').hide();
        }
        return false;
    });
</script>