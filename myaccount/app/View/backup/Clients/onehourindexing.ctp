<div class="marginB10"></div>
<div class="page-header">
    <h4>All the urls you have posted to get indexed</h4>
    <table class="table table-bordered" cellpadding="2" cellspacing="2">
        <tr>
            <td>Batch Name: <?php echo $iR['Linkindexing']['batch_name']; ?></td>
            <td>Title: <?php echo $iR['Linkindexing']['title']; ?></td>
        </tr>
    </table>

</div>


<div class="content" style="max-height: 600px; overflow: hidden;">
    <div style="height: 550px; overflow: auto;">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>URL</th>
                <th>Process Date</th>
                <th>Index Date</th>
            </tr>
            </thead>
            <tbody>
            <?php
            /*
            * Links: [
            { Id: �c45s3�, BatchId: �GUID�, LinkUrl: �LINKURL�, Processed: �LIVEDATE�, IndexDate: �CRAWLDATE� },
            { Id: �8dk3d�, BatchId: �GUID�, LinkUrl: �LINKURL�, Processed: �LIVEDATE�, IndexDate: �� }
            ]
            }
            */
            $bData = $batchData;// json_decode($batchData,true);
            if (isset($bData->Links) && count($bData->Links)) {
                foreach ($bData->Links as $k => $l) {
                    echo '<tr>';
                    echo '<td><a href="' . $l->LinkUrl . '" target="blank" title="Go to Domain" class="tip">' . urldecode($l->LinkUrl) . '</a></td>
                            <td>' . date("M d, Y h:i a", strtotime($l->Processed)) . '</td>
                            </tr>';
                }
            } else {
                echo '<tr><td colspan="3">No Url Found!</td></tr>';
            }


            ?>
            </tbody>

        </table>
    </div>
</div>
