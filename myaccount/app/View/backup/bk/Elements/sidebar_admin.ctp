<div id="sidebar">
    <div class="sidenav">
        <div class="sidebar-widget" style="margin: -1px 0 0 0;">
            <h5 class="title" style="margin-bottom:0">Navigation</h5>
        </div><!-- End .sidenav-widget -->
        <div class="mainnav">
            <ul id="nav_side">
                <li class="top"><a href="<?php echo Router::url('dashboard'); ?>"><span class="icon16 icomoon-icon-stats-up"></span>Dashboard</a></li>
                <li class="top">
                    <a href="#"><span class="icon16 icomoon-icon-license"></span>Services</a>
                    <ul class="sub">
                        <li class="current">
                            <a href="<?php echo Router::url('addservice'); ?>"><span class="icon16 entypo-icon-plus"></span>Add Service</a>
                        </li>
                        <li class="current">
                            <a href="<?php echo Router::url('services'); ?>"><span class="icon16 entypo-icon-settings"></span>Manage Service</a>
                        </li>
                    </ul>
                </li>
                <li class="top">
                    <a href="#"><span class="icon16 icomoon-icon-stack"></span>Packages</a>
                    <ul class="sub">
                        <li class="current">
                            <a href="<?php echo Router::url('addpackage'); ?>"><span class="icon16 entypo-icon-plus"></span>Add Package</a>
                        </li>
                        <li class="current">
                            <a href="<?php echo Router::url('packages'); ?>"><span class="icon16 entypo-icon-settings"></span>Manage Package</a>
                        </li>
                    </ul>
                </li>
                <li class="top">
                    <a href="#"><span class="icon16  icomoon-icon-users"></span>Clients</a>
                    <ul class="sub">
                        <li class="current">
                            <a href="<?php echo Router::url('newclient'); ?>"><span class="icon16 icomoon-icon-user-3"></span>Add New Client</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('manageclients'); ?>"><span class="icon16   icomoon-icon-users-2"></span>Manage Clients</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('monthlyrenewals'); ?>"><span class="icon16   icomoon-icon-users-2"></span>Monthly Renewals</a>
                        </li>
                    </ul>
                </li>
                <li class="top">
                    <a href="#"><span class="icon16 entypo-icon-network"></span>Domains</a>
                    <ul class="sub">
                        <!--li class="current">
                        <a href="<?php echo Router::url('newdomain'); ?>"><span class="icon16 entypo-icon-plus"></span>New Domain</a>
                        </li>
                        <li>
                        <a href="<?php echo Router::url('importdomains'); ?>"><span class="icon16 entypo-icon-add"></span>Import Domains</a>
                        </li-->
                        <li>
                            <a href="<?php echo Router::url('manageclprvsites'); ?>"><span class="icon16 icomoon-icon-accessibility"></span>Client Private Sites</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('managemysites'); ?>"><span class="icon16 icomoon-icon-accessibility"></span>My Sites</a>
                        </li>
                        <!--li>
                        <a href="< ?php echo Router::url('managemanualsites'); ?>"><span class="icon16 icomoon-icon-hand"></span>Manual Sites</a>
                        </li>
                        <li>
                        <a href="< ?php echo Router::url('managepremium'); ?>"><span class="icon16 icomoon-icon-seven-segment-4"></span>Premium Sites</a>
                        </li>
                        <li>
                        <a href="< ?php echo Router::url('manageprime'); ?>"><span class="icon16 icomoon-icon-seven-segment-3"></span>Prime Sites</a>
                        </li>
                        <li>
                        <a href="< ?php echo Router::url('managelowgrade'); ?>"><span class="icon16 icomoon-icon-seven-segment"></span>LowGrade Sites</a>
                        </li>
                        <li>
                        <a href="< ?php echo Router::url('manageguestsites'); ?>"><span class="icon16 icomoon-icon-balance"></span>Guest Blogging Sites</a>
                        </li-->
                        <li>
                            <a href="<?php echo Router::url('managedomains'); ?>"><span class="icon16 entypo-icon-settings"></span>All Domains</a>
                        </li>
                        <!--li>
                        <a href="<?php echo Router::url('movedomains'); ?>"><span class="icon16 icomoon-icon-move"></span>Move Domains</a>
                        </li-->
                    </ul>
                </li>


                <li class="top">
                    <a href="#"><span class="icon16  icomoon-icon-eye-3"></span>Domain Control</a>
                    <ul class="sub">
                        <li>
                            <a href="<?php echo Router::url('activedomains'); ?>"><span class="icon16  icomoon-icon-checkmark"></span>Working Domains</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('problematics'); ?>"><span class="icon16 typ-icon-cross"></span>Problematic Domains</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('updatehttpstatus'); ?>"><span class="icon16 icomoon-icon-refresh"></span>Update Status</a>
                        </li>
                    </ul>
                </li>


                <li class="top">
                    <a href="#"><span class="icon16 icomoon-icon-accessibility"></span>Mysites</a>
                    <ul class="sub">
                        <li>
                            <a href="<?php echo Router::url('assignedmysites'); ?>"><span class="icon16 minia-icon-checked"></span>Assigned Sites</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('assignedclients'); ?>"><span class="icon16 minia-icon-checked"></span>Assigned Clients</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('freemysites'); ?>"><span class="icon16 minia-icon-unchecked"></span>Free Sites</a>
                        </li>
                    </ul>
                </li>
                <li class="top">
                    <a href="#"><span class="icon16 icomoon-icon-coins"></span>Manage Credits</a>
                    <ul class="sub">
                        <li class="current">
                            <a href="<?php echo Router::url('viewcredits'); ?>"><span class="icon16 icomoon-icon-file"></span>View Credits</a>
                        </li>
                        <li class="current">
                            <a href="<?php echo Router::url('creditstogroup'); ?>"><span class="icon16 icomoon-icon-file"></span>Assign to Group</a>
                        </li>
                        <li class="current">
                            <a href="<?php echo Router::url('creditstouser'); ?>"><span class="icon16 icomoon-icon-file"></span>Assign to Individual</a>
                        </li>
                        <li class="current">
                            <a href="<?php echo Router::url('credithistory'); ?>"><span class="icon16 icomoon-icon-file"></span>Credit History</a>
                        </li>
                        <li class="current">
                            <a href="<?php echo Router::url('debithistory'); ?>"><span class="icon16 icomoon-icon-file"></span>Debit History</a>
                        </li>
                    </ul>
                </li>
                <li class="top">
                    <a href="#"><span class="icon16 icomoon-icon-book-2"></span>Tutorials</a>
                    <ul class="sub">
                        <li class="current">
                            <a href="<?php echo Router::url('addinstruction'); ?>"><span class="icon16 entypo-icon-plus"></span>Add Tutorial</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('manageinstruction'); ?>"><span class="icon16 entypo-icon-settings"></span>Manage Tutorials</a>
                        </li>
                    </ul>
                </li>

                <li class="top">
                    <a href="#"><span class="icon16 entypo-icon-images"></span>Media</a>
                    <ul class="sub">
                        <li>
                            <a href="<?php echo Router::url('addmedia'); ?>"><span class="icon16 entypo-icon-add"></span>Add Media</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('managemedia'); ?>"><span class="icon16 entypo-icon-settings"></span>Manage Media</a>
                        </li>
                    </ul>
                </li>

                <li class="top">
                    <a href="#"><span class="icon16 icomoon-icon-books"></span>Education</a>
                    <ul class="sub">
                        <li>
                            <a href="<?php echo Router::url('managecats'); ?>"><span class="icon16 entypo-icon-settings"></span>Manage Category</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('addlesson'); ?>"><span class="icon16 icomoon-icon-book"></span>Add Topic</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('managelesson'); ?>"><span class="icon16 entypo-icon-settings"></span>Manage Topics</a>
                        </li>
                    </ul>
                </li>


                <li class="top">
                    <a href="#"><span class="icon16 icomoon-icon-library"></span>Recordings</a>
                    <ul class="sub">

                        <li class="current">
                            <a href="<?php echo Router::url('recordingaccess'); ?>"><span class="icon16 icomoon-icon-book"></span>Client Access List</a>
                        </li>
                        <li class="current">
                            <a href="<?php echo Router::url('addrecording'); ?>"><span class="icon16 icomoon-icon-book"></span>Add Recording</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('managerecording'); ?>"><span class="icon16 entypo-icon-settings"></span>Manage Recordings</a>
                        </li>
                    </ul>
                </li>


                <li class="top">
                    <a href="#"><span class="icon16 icomoon-icon-list-view-2"></span>Settings</a>
                    <ul class="sub">
                        <li class="current">
                            <a href="<?php echo Router::url('managesettings'); ?>"><span class="icon16 entypo-icon-settings"></span>Manage Settings</a>
                        </li>
                    </ul>
                </li>
                <!--li class="top">
                <a href="#"><span class="icon16  icomoon-icon-blogger-2"></span>Guest Bloggers</a>
                <ul class="sub">
                <li class="current">
                <a href="<?php echo Router::url('newblogger'); ?>"><span class="icon16 icomoon-icon-user-3"></span>Add New Guest Blogger</a>
                </li>
                <li>
                <a href="<?php echo Router::url('managebloggers'); ?>"><span class="icon16 icomoon-icon-users-2"></span>Manage Bloggers</a>
                </li>
                </ul>
                </li-->
                <li class="top"><a href="<?php echo Router::url('bazookacategories'); ?>"><span class="icon16  icomoon-icon-link-2"></span>Bazooka Categories</a></li>
                <li class="top">
                    <a href="#"><span class="icon16  icomoon-icon-blogger-2"></span>FAQ Category</a>
                    <ul class="sub">
                        <li class="current">
                            <a href="<?php echo Router::url('addfaqcategory'); ?>"><span class="icon16 icomoon-icon-user-3"></span>Add or Manage Category</a>
                        </li>
                        <!--                        <li>
                        <a href="<?php echo Router::url('managefaqcategories'); ?>"><span class="icon16 icomoon-icon-users-2"></span>Manage Category</a>
                        </li>-->
                    </ul>
                </li>
                <li class="top">
                    <a href="#"><span class="icon16  icomoon-icon-blogger-2"></span>FAQ</a>
                    <ul class="sub">
                        <li class="current">
                            <a href="<?php echo Router::url('addfaq'); ?>"><span class="icon16 icomoon-icon-user-3"></span>Add FAQ</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('managefaqs'); ?>"><span class="icon16 icomoon-icon-users-2"></span>Manage FAQ</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>