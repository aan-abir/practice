<style type="text/css">
    .grayed {
        color: #eeeeee !important;
        cursor: text;
    }

    .grayed:hover {
        color: #eeeeee !important;
        cursor: text;
    }

</style>
<div id="sidebar">
    <div class="sidenav">
        <div class="sidebar-widget" style="margin: -1px 0 0 0;">
            <h5 class="title" style="margin-bottom:0">Navigation</h5>
        </div>
        <!-- End .sidenav-widget -->
        <div class="mainnav">
            <ul id="nav_side">
                <li class="top"><a href="<?php echo Router::url('dashboard'); ?>"><span
                            class="icon16 icomoon-icon-stats-up"></span>Dashboard</a></li>
                <li class="top">
                    <a href="#"><span class="icon16 entypo-icon-broadcast"></span>DCL Campaigns</a>
                    <ul class="sub">
                        <li>
                            <a href="<?php echo Router::url('addcampaign'); ?>"><span
                                    class="icon16 entypo-icon-plus"></span>Add New Campaign</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('managecampaign'); ?>"><span
                                    class="icon16 entypo-icon-settings"></span>Manage Campaign</a>
                        </li>
                    </ul>
                </li>
                <li class="top">
                    <a href="#"><span class="icon16 icomoon-icon-clipboard"></span>Rank Reporting</a>
                    <ul class="sub">
                        <li>
                            <a href="<?php echo Router::url('rankresults'); ?>" class=""><span
                                    class="icon16 typ-icon-stats"></span>Current Ranking</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('historicalranking'); ?>"><span
                                    class="icon16 entypo-icon-history"></span>Historical Ranking</a>
                        </li>
                    </ul>
                </li>
                <li class="top">
                    <a href="#"><span class="icon16  icomoon-icon-link"></span>Link Reports</a>
                    <ul class="sub">
                        <li>
                            <a href="<?php echo Router::url('submissionreport'); ?>"><span
                                    class="icon16 eco-article"></span>Our Links</a>
                        </li>
                    </ul>
                </li>
                <li class="top">
                    <a href="#"><span class="icon16   entypo-icon-settings"></span>Tools</a>
                    <ul class="sub">
                        <li>
                            <a href="<?php echo Router::url('pr_campaign_create'); ?>"><span
                                    class="icon16 icomoon-icon-health"></span>Domain Digger</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('ldreport'); ?>"><span
                                    class="icon16 iconic-icon-share"></span>Link Density</a>
                        </li>
                    </ul>
                </li>
                <li class="top">
                    <a href="#"><span class="icon16 entypo-icon-network"></span>Articles</a>
                    <ul class="sub">
                        <li>
                            <a href="<?php echo Router::url('order_article'); ?>"><span
                                    class="icon16 entypo-icon-plus"></span>Order Article</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('article'); ?>"><span
                                    class="icon16 entypo-icon-plus"></span>Add Article</a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url('articles'); ?>"><span
                                    class="icon16 entypo-icon-add"></span>Article Repository</a>
                        </li>
                    </ul>
                </li>

                <li class="top"><a href="<?php echo Router::url('resources'); ?>"><span
                            class="icon16 icomoon-icon-books"></span>Education</a></li>
                <li class="top"><a href="<?php echo Router::url('faq'); ?>"><span
                            class="icon16 icomoon-icon-books"></span>FAQ</a></li>


            </ul>
        </div>
    </div>
</div>