<div id="header">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="brand" href="/">
                    <?php echo $this->Html->image("/images/rankratio-logo.png", array('width' => 160)); ?>
                </a>
                <div class="nav-no-collapse">
                    <ul class="nav">
                        <li class="active"><a href="/"><span class="icon16 icomoon-icon-screen-2"></span> Dashboard</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="icon16 eco-link "></span> My Sites
                                <b class="caret"></b>
                            </a>

                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="icon16 cut-icon-stats-2"></span>  RankRatio Sites
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'addsubmission' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('addsubmission'); ?>"><span class="icon16  icomoon-icon-link"></span>Content Links</a>
                                        </li>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'addblogroll' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('addblogroll'); ?>"><span class="icon16  icomoon-icon-link"></span>BlogRoll Links</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="icon16 eco-link"></span> Other Services
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'bazookablast' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('bazookablast'); ?>"><span class="icon16  icomoon-icon-gun"></span>Bazooka Blast</a>
                                        </li>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'linkemperor' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('linkemperor'); ?>"><span class="icon16  icomoon-icon-crown"></span>Link Emperor</a>
                                        </li>
                                        <li>
                                            <?php if (AuthComponent::user('id') != 0): ?>
                                                <a <?php echo $this->params['action'] == 'orderpressrelease' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('orderpressrelease'); ?>"><span class="icon16  icomoon-icon-power-cord"></span>Press Release</a>
                                            <?php else: ?>
                                                <span class="icon16 icomoon-icon-wrench"></span><em style="color: gray; cursor: text;">Press Release</em>
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <a <?php echo in_array($this->params['action'], array('linkindexing', 'indexingreport', 'indexingurls')) ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('linkindexing'); ?>"><span class="icon16 icomoon-icon-puzzle"></span>Link Indexing</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="http://support.seonitro.com/" target="_blank"><span class="icon16 icomoon-icon-support"></span> Support</a></li>
                    </ul>
                    <ul class="nav pull-right usernav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="icon16 icomoon-icon-happy"></span>
                                <span class="txt">
                                    <?php echo AuthComponent::user('firstname') . ' ' . AuthComponent::user('lastname'); ?>
                                </span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul>
                                        <li>
                                            <a href="#"><span class="icon16 icomoon-icon-user-3"></span>Edit profile</a>
                                        </li>
                                        <li>
                                            <a href="#"><span class="icon16 icomoon-icon-comments-2"></span>Approve comments</a>
                                        </li>
                                        <li>
                                            <a href="#"><span class="icon16 icomoon-icon-plus-2"></span>Add user</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?php echo Router::url('logout') ?>"><span class="icon16 icomoon-icon-exit"></span> Logout</a></li>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div>
        </div><!-- /navbar-inner -->
    </div>
    <!-- /navbar -->
</div>
