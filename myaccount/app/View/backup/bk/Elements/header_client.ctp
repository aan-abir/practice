<?php
    $activeTopMenu = ''; // 0 dashboard
    $topMenu = array(
        'dashboard' => array('dashboard'),
        'mysites' => array('mysites', 'myexclusivesites', 'newdomain', 'editdomain'),
        'networksites' => array('manualnetworkapplied', 'manualnetwork', 'managesubmission', 'addsubmission', 'editsubmission', 'submissionreport', 'submissionlinks', 'addblogroll', 'manageblogroll', 'blogcomments', 'manualpost', 'manualpostreport'),
        'outsidelinks' => array('bazookainprogress', 'bazookablast', 'bazookacompleted', 'linkemperorinprogress', 'linkemperor', 'linkemperorcompleted', 'manageguestpost', 'createmashup', 'managemashup', 'viewmashup', 'editmashup', 'orderguestpost', 'assignedguestpost', 'completedguestpost', 'linkindexing', 'indexingreport', 'indexingurls'),
        'profile' => array('editprofile', 'myplan', 'brands'),
        'support' => array('tour', 'faq', 'sdesk'),
    );
    foreach ($topMenu as $k => $g) {
        if (in_array($this->params['action'], $g)) {
            $activeTopMenu = $k;
            break;
        }
    }
?>

<div id="header">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="brand" href="/">
                    <?php echo $this->Html->image("/images/rankratio-logo.png", array('width' => 160)); ?>
                </a>
                <div class="nav-no-collapse">
                    <ul class="nav">
                        <li<?php echo $activeTopMenu == 'dashboard' ? ' class="active"' : ''; ?>><a href="<?php echo Router::url('dashboard'); ?>"><span class="icon16 icomoon-icon-screen-2"></span>Dashboard</a></li>

                        <li<?php echo $activeTopMenu == 'newdomain' ? ' class="active"' : ''; ?>><a href="<?php echo Router::url('newdomain'); ?>"><span class="icon16 icomoon-icon-accessibility"></span> My Sites</a></li>

                        <li<?php echo $activeTopMenu == 'networksites' ? ' class="dropdown active"' : ' class="dropdown"'; ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="icon16 cut-icon-stats-2"></span> RankRatio Sites
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'addsubmission' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('addsubmission'); ?>"><span class="icon16  icomoon-icon-link"></span>Content Links</a>
                                        </li>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'addblogroll' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('addblogroll'); ?>"><span class="icon16  icomoon-icon-link"></span>BlogRoll Links</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li<?php echo $activeTopMenu == 'outsidelinks' ? ' class="dropdown active"' : ' class="dropdown"'; ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="icon16 icomoon-icon-rocket"></span> Other Services
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'bazookablast' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('bazookablast'); ?>"><span class="icon16  icomoon-icon-gun"></span>Bazooka Blast</a>
                                        </li>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'linkemperor' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('linkemperor'); ?>"><span class="icon16  icomoon-icon-crown"></span>Link Emperor</a>
                                        </li>
                                        <li>
                                            <?php if (AuthComponent::user('id') != 0): ?>
                                                <a <?php echo $this->params['action'] == 'orderpressrelease' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('orderpressrelease'); ?>"><span class="icon16  icomoon-icon-power-cord"></span>Press Release</a>
                                                <?php else: ?>
                                                <span class="icon16 icomoon-icon-wrench"></span><em style="color: gray; cursor: text;">Press Release</em>
                                                <?php endif; ?>
                                        </li>
                                        <li>
                                            <a <?php echo in_array($this->params['action'], array('linkindexing', 'indexingreport', 'indexingurls')) ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('linkindexing'); ?>"><span class="icon16 icomoon-icon-puzzle"></span>Link Indexing</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav pull-right usernav">
                        <li<?php echo $activeTopMenu == 'profile' ? ' class="dropdown active"' : ' class="dropdown"'; ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                <span class="icon16 icomoon-icon-happy"></span>
                                <span class="txt">
                                    <?php echo AuthComponent::user('firstname') . ' ' . AuthComponent::user('lastname'); ?>
                                </span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'editprofile' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('editprofile'); ?>"><span class="icon16 icomoon-icon-user-3"></span>Profile</a>
                                        </li>

                                        <li>
                                            <a <?php echo $this->params['action'] == 'myplan' ? ' class="topnavSelected"' : ''; ?>  class="top-sub-nav" href="<?php echo Router::url('myplan'); ?>">
                                                <span class="icon16  wpzoom-share"></span>Plans & Credits</a>
                                        </li>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'brands' ? ' class="topnavSelected"' : ''; ?>  class="top-sub-nav" href="<?php echo Router::url('brands'); ?>">
                                                <span class="icon16 iconic-icon-brush"></span>Branding  </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <!-- <li< ?php echo $activeTopMenu == 'support' ? ' class="dropdown active"' : ' class="dropdown"'; ?>>
                        <a href="http://support.seonitro.com/" target="_blank"><span class="icon16  icomoon-icon-support"></span>Support</a>
                        </li>-->


                        <li<?php echo $activeTopMenu == 'support' ? ' class="dropdown active"' : ' class="dropdown"'; ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="icon16  icomoon-icon-support"></span>Support
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul>
                                        <li><a <?php echo $this->params['action'] == 'tour' ? ' class="topnavSelected"' : ''; ?>  class="top-sub-nav" href="<?php echo Router::url('tour'); ?>"><span class="icon16 entypo-icon-leaf"></span>Tour</a></li>
                                        <li><a <?php echo $this->params['action'] == 'faq' ? ' class="topnavSelected"' : ''; ?>  class="top-sub-nav" href="<?php echo Router::url('faq'); ?>"><span class="icon16 entypo-icon-share"></span>FAQ</a></li>
                                        <li><a  class="top-sub-nav" href="http://support.seonitro.com/" target="_blank"><span class="icon16 entypo-icon-support"></span>Support Desk</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>



                        <li><a href="logout"><span class="icon16 icomoon-icon-exit"></span> Logout</a></li>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div>
        </div><!-- /navbar-inner -->
    </div>
    <!-- /navbar -->
</div>