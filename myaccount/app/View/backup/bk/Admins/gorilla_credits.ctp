
<div class="marginB10"></div>
<div class="page-header">
    <h4>Gorilla Credits</h4>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="content">
            <table class="responsive table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 150px;"></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Current Credit</td>
                        <td> $<?php echo number_format($data['Setting']['gorilla_credit'], 2); ?></td>
                    </tr>

                    <tr>
                        <td>Credit needed for <?php echo date('F') ?></td>
                        <td><?php echo '$' . number_format($credit_needed, 2); ?></td>
                    </tr>
                    <tr>
                        <td>Summary of Costs</td>
                        <td>Instant Check Per Keyword: $0.03&nbsp;&nbsp;&nbsp;&nbsp;Monthly Check Per Keyword: $0.07 (Daily Check Breakdown: $0.023)</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div><!-- End .span12 -->    
</div>
<?php
if(isset($payment_form) && $payment_form == 0)
{    
echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'gorilla_credits'), 'id' => 'editSettingForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
<div class="form-row row-fluid">
    <div class="span12">
        <div class="row-fluid">
            <label class="form-label span3" for="campaign_name">Amount:</label>
            <?php echo $this->Form->input('Card.amount', array('error' => false, 'required' => true, 'id' => 'amount', 'type' => 'text', 'title' => 'Enter your amount', 'placeholder' => "Enter amout", 'div' => false, 'label' => false, 'class' => 'span2')); ?>
        </div>
    </div>
</div>

<div class="marginT10"></div>
<div class="form-row row-fluid">
    <div class="span12">
        <div class="row-fluid">
            <div class="form-actions">
                <div class="span3"></div>
                <div class="span4 controls">
                    <button type="submit" class="btn btn-info marginR10">Add Credits</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="marginB10"></div>
<?php echo $this->Form->end(); 
}

if(isset($payment_form) && $payment_form == 1)
{    
$time = time();
$key = '86c7486ll8GDh0lOBNt4'; //'obVfCY4sl8yds3L4WbP0';
$x_login = 'WSP-INTER-rceCOQAj@Q'; //'WSP-INTER-tqFrHgAiew';
$x_fp_sequence = '123456';
$x_recurring_billing_amount = $amount;
$x_currency_code = 'USD';
$data = $x_login . "^" . $x_fp_sequence . "^" . $time . "^" . $x_recurring_billing_amount . "^" . $x_currency_code;
$x_fp_hash = hash_hmac('MD5', $data, $key);
?>
<form name="payment_Frm" action="https://checkout.globalgatewaye4.firstdata.com/payment" method="post">

    <input type="hidden" name="x_login" value="<?php echo $x_login; ?>" /> 
    <input type="hidden" name="x_fp_sequence" value="<?php echo $x_fp_sequence; ?>" /> 
    <input type="hidden" name="x_fp_timestamp" value="<?php echo $time; ?>" /> 
    <input type="hidden" name="x_currency_code" value="<?php echo $x_currency_code; ?>" /> 
    <input type="hidden" name="x_amount" value="<?php echo $x_recurring_billing_amount; ?>"/> 
    <input type="hidden" name="x_fp_hash" value="<?php echo $x_fp_hash; ?>" /> 
    <input type="hidden" name="x_show_form" value="PAYMENT_FORM" />
    <!--<input type="hidden" name="x_type" value="AUTH_TOKEN" /> -->


    <!-- Fields specific to Recurring -->
    <input type="hidden" name="x_recurring_billing_amount" value="<?php echo $x_recurring_billing_amount; ?>"/> 
    <input type="hidden" name="x_recurring_billing_id" value="MB-INTER-16-58932" /> 
    <input type="hidden" name="x_recurring_billing" value="TRUE" /> 
    <input type="hidden" name="x_recurring_billing_start" value="<?php echo date("Y-m-d"); ?>" /> 
    <!--            <input type="hidden" name="x_recurring_billing_end" value="2015-01-11" /> -->
    <input type="hidden" name="x_first_name" value="Dori" /> 
    <input type="hidden" name="x_last_name" value="Friend" /> 
    <input type="hidden" name="x_company" value="SEONitro" /> 
    <input type="hidden" name="x_country" value="USA" /> 
    <input type="hidden" name="x_phone" value="233-333-6566" /> 


    <!--<div class="form-actions align-right"><input type="submit" value="Current Ranking for Gorilla Credit" class="btn btn-primary" /></div>-->
</form>
<script>document.forms.payment_Frm.submit()</script>
<?php
}
?>
