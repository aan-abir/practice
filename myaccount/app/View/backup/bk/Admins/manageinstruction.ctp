<div class="marginB10"></div>
<div class="page-header">
    <h4>All the Page Tutorial Added</h4>
    <p>so far these following page tutorial has been added. you can edit or delete any page tutorial.</span></p>

</div>


<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>Page Name</th>
                <th>Title</th>
                <th>Top Help Text</th>
                <th>Help Guide</th>
                <th>Ext Video Link</th>
                <th>Uploaded Video</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(isset($allInstruction) && count($allInstruction) > 0):
                    foreach($allInstruction as $arr):
                    ?>
                    <tr>
                        <td><?php echo $arr['Instruction']['pagename']; ?></td>
                        <td><?php echo $arr['Instruction']['title']; ?></td>
                        <td>

                            <?php 
                                if ( trim($arr['Instruction']['toptext']) != '' ) {
                                    echo '<a title="Read Help Guide to play smooth" class="openModalDialog" href="http://seonitrov2.seonitro.com/sadmins/loadhelptext/'.$arr['Instruction']['pagename'].'/toptext">preview</a>';
                                }else{
                                    echo 'x';
                                }
                            ?>
                        </td><td>

                            <?php 
                                if ( trim($arr['Instruction']['helpguide']) != '' ) {
                                    echo '<a title="Read Help Guide to play smooth" class="openModalDialog" href="http://seonitrov2.seonitro.com/sadmins/loadhelptext/'.$arr['Instruction']['pagename'].'/helpguide">preview</a>';
                                }else{
                                    echo 'x';
                                }
                            ?>
                        </td>
                        <td>
                            <?php 
                                if ( trim($arr['Instruction']['externallink']) != '' ) {
                                    echo '<a title="Watch this useful video" class="fancybox-media" href="'.trim($arr['Instruction']['externallink']).'">preview</a>';
                                }else{
                                    echo 'x';
                                }

                            ?>
                        </td>
                        <td>
                            <?php 
                                if ( trim($arr['Instruction']['video']) != '' ) {
                                    echo '<a class="openModalDialog" title="Watch the useful video. Double click on Video for full screen" href="http://seonitrov2.seonitro.com/sadmins/loadhelptext/'.$arr['Instruction']['pagename'].'/v">preview</a>';
                                  
                                }else{
                                    echo 'x';
                                }

                            ?>
                        </td>
                        <td>
                            <div class="controls center">
                                <a href="<?php echo adminHome . '/editinstruction/' . $arr['Instruction']['id']; ?>" title="Edit Instruction" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                <a href="<?php echo adminHome . '/deleteinstruction/' . $arr['Instruction']['id']; ?>" title="Remove Instruction?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="7">No Instruction Found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
</div>
       
