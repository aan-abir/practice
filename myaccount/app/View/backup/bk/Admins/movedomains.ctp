<div class="marginB10"></div>
<div class="page-header">
    <h4>Move domain into internal networks</h4>
    <p>Below is the list of all your domain for Guest blog, Premium, Prime and low grade. You can move any domains to any network. 

        <br>   
        <span style="background-color: yellow;">

            Premium: Has to have 1 of the following 3 qualifications
            -PR3
            OR Trust Flow 13 or greater
            OR SEOMoz Rank of 3 or greater <br>

            Prime: Has to have 1 of the following 3 qualifications
            -PR2-1
            OR Trust Flow of 8 - 12
            OR SEOMoz Rank of 1-2
            <br>

            Guest Blog:
            - PR greater 3 

            Low Grade
            all the rest

        </span>
    </p>

</div>
<div class="row-fluid">
    <div class="span12">
        <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'movedomains'), 'id' => 'moveToForm', 'method' => 'post')); ?>
        <input type="hidden" name="data[Moveto][networktype]" id="sendTo" value="">
        <div class="content span12">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allDomains)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th title="Domain Name" class="tip">&nbsp;</th>
                        <th title="Domain Name" class="tip">Domain Name</th>
                        <th title="Domain Assigned To Network" class="tip">Assn.To</th>
                        <th title="Page Rank" class="tip">PR</th>
                        <th title="Citation Flow" class="tip">CF</th>
                        <th title="Trust Flow" class="tip">TF</th>
                        <th title="Link Juice Page Rank" class="tip">LJPR</th>
                        <th title="SEOMoz Rank" class="tip">SM.R</th>
                        <th title="External Back Links" class="tip">BL</th>
                        <th title="What Type of CMS Using this Domain" class="tip">CMS</th>
                        <th title="Super Admin Username for this domain" class="tip">User</th>
                        <th title="Super Admin Password for this domain" class="tip">Pass</th>
                        <th title="Necessary Action" class="tip">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $color = array(
                            '1' => '#ED7A53',
                            '2' => '#0e830e',
                            '3' => '#1baa47',
                            '4' => '#b40b8a',
                            '5' => '#a06508',
                            '6' => '#09aea0',
                        );

                        if (count($allDomains) > 0):
                            foreach ($allDomains as $arr):
                                $d = 'http://'. str_replace('http://','',$arr['Domain']['domain']);
                                $clor = $color[$arr['Commonmodel']['id']];
                            ?>
                            <tr>
                                <td><input type="checkbox" class="selDomain" name="data[Moveto][ids][<?php echo $arr['Domain']['id']; ?>]" /></td>
                                <td align="left" style="text-align: left !important;"><a href="<?php echo $d; ?>" title="View Domain" target="_blank"><?php echo $arr['Domain']['domain']; ?></a></td>
                                <td style="color: <?php echo $clor; ?>"><?php echo $arr['Commonmodel']['name']; ?></td>
                                <td><?php echo $arr['Domain']['pr']; ?></td>
                                <td><?php echo $arr['Domain']['cflow']; ?></td>
                                <td><?php echo $arr['Domain']['tflow']; ?></td>
                                <td><?php echo $arr['Domain']['ljpr']; ?></td>
                                <td><?php echo $arr['Domain']['seomoz_rank']; ?></td>
                                <td><?php echo $arr['Domain']['ext_backlinks']; ?></td>
                                <td><?php echo substr($arr['Domain']['cms'],0,2); ?></td>
                                <td><?php echo $arr['Domain']['ausername']; ?></td>
                                <td><?php echo $arr['Domain']['apassword']; ?></td>
                                <td>
                                    <div class="controls center">
                                        <a href="<?php echo adminHome . '/editdomain/' . $arr['Domain']['id']; ?>" title="Edit Domain" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                        <a href="<?php echo adminHome . '/deletedomain/' . $arr['Domain']['id']; ?>" title="Remove Domain?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                    </div>
                                </td>
                            </tr>

                            <?php
                                endforeach;
                            else:
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="11">No Guest Blogging Sites Found!</td>
                        </tr>
                        <?php
                            endif;
                    ?>
                </tbody>
            </table>

        </div>
        <div class="span12 maarginB10"></div>
        <div class="content span12">
            <button class="btn btn-success sendBtn" rel="2" title="Manual Network" id="moveToPremium"><span class="icon16 icomoon-icon-move white"></span>  Move to Manual</button>
            <button class="btn btn-primary sendBtn" rel="1" title="MySites" id="moveToPremium"><span class="icon16 icomoon-icon-move white"></span>  Move to MySites</button>
            <button class="btn btn-success sendBtn" rel="3" title="Premium" ><span class="icon16 icomoon-icon-move white"></span> Move to Premium</button>
            <button class="btn btn-warning sendBtn" rel="4" title="Prime"><span class="icon16 icomoon-icon-move white"></span>  Move to Prime</button>
            <button class="btn btn-info sendBtn" rel="5" title="Low Grade" id="moveToPremium"><span class="icon16 icomoon-icon-move white"></span>  Move to Low Grade</button>
            <button class="btn btn-primary sendBtn" rel="6" title="Guest Blog" id="moveToPremium"><span class="icon16 icomoon-icon-move white"></span>  Move to Guest Blog</button>
        </div>

        <?php echo $this->Form->end(); ?>
    </div><!-- End .span6 -->
</div>
<script type="text/javascript">
    $(".sendBtn").live('click',function(e){
        e.preventDefault();
        if ( !confirm("Are you sure to move those selected domain to " + $(this).attr('title') )) {return false;}
        if ( $(".selDomain:checked").length == 0 ) {
            alert('Please select at least one domain to move');
            return false;
        }
        $("#sendTo").val($(this).attr('rel'));
        $("#moveToForm").submit();
    });
</script>