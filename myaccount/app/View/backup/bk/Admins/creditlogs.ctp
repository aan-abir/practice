<div class="heading">
    <h3>Manage Credits</h3>
</div><!-- End .heading-->
<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Credit Log</span>
                    <button href="#" style="margin-left: 50px;" class="openModalDialog btn btn-info"><span class="icon16 icomoon-icon-info-2 white"></span>Need Help?</button>                    
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Client Name</th>
                            <th>Assigned Credit</th>
                            <th>Assigned on</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(isset($creditLogs) && count($creditLogs) > 0):
                            foreach($creditLogs as $arr):
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?php echo $arr['Creditlog']['clientname']; ?></td>
                                    <td><?php echo $arr['Creditlog']['assignedcredits']; ?></td>
                                    <td><?php echo $arr['Creditlog']['created']; ?></td>
                                    <td>
                                        <div class="controls center">
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="4">No Records Found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div>
<?php include_once 'needhelp.ctp'; ?>