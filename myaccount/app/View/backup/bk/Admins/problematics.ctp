<div class="marginB10"></div>
<div class="page-header">
    <h4>All NON WORKING Domains</h4>
    <p>list of all NOT working domains. <span style="background-color: yellow;">if a domain is fixed then put back it to working mode</span><br>

        <span class="bold yellow">if you found any domain status 'OK' with 'Problematic' then it might not have wodpress installed  etc</span>  
    </p>

</div>
<div class="row-fluid">
    <div class="span12">

        <a style="position: absolute; left: 50%; z-index: 20" title="Export all the problematic domains with status" class="btn btn-link tip" target="_blank" href="<?php echo adminHome . '/exportdomainstatus/problematic'; ?>"><span class="icon16 icomoon-icon-file-excel"></span> Export List </a>

        <div class="content">


            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allDomains)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th title="Domain Name" class="tip">Domain Name</th>
                        <th title="Original Owner" class="tip">O.Owner</th>
                        <th title="Domain Assigned To" class="tip">Assn.To</th>
                        <th title="Domain Expiring Date" class="tip">Expire</th>
                        <th title="Google Page Rank / Citation Flow / Trust Flow / SeoMoz Rank / Exteral Links" class="tip">PR/CF/TF/SR/EL</th>
                        <th title="Current HTTP Status Code" class="tip">Http Code</th>
                        <th title="Current HTTP Status" class="tip">Http Status</th>
                        <th title="CMS type / Username / Password" class="tip">CMS/UserPass</th>
                        <th title="Necessary Action" class="tip">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $color = array(
                            '1' => '#ED7A53',
                            '2' => '#0e830e',
                            '3' => '#1baa47',
                            '4' => '#b40b8a',
                            '5' => '#a06508',
                            '6' => '#09aea0',
                        );

                        if (count($allDomains) > 0):
                            foreach ($allDomains as $arr):
                                $d = 'http://'. str_replace('http://','',$arr['Domain']['domain']);
                                $clor = $color[$arr['Commonmodel']['id']];
                                 $stats = 'Seo Stats for this Domain <br>---------------------------------------------<br> Google Page Rank - '. intval($arr['Domain']['pr']). ' <br> Citation Flow - ' . intval($arr['Domain']['cflow']) .' <br> Trust Flow - '  . intval($arr['Domain']['tflow']) .' <br> SeoMoz Rank - '  .  intval($arr['Domain']['seomoz_rank']) .' <br> External Back Links -  '. intval($arr['Domain']['ext_backlinks']);
                            ?>
                            <tr>
                                <td><a href="<?php echo $d; ?>" title="View Domain" target="_blank"><?php echo $arr['Domain']['domain']; ?></a></td>
                                <td><?php echo ( $arr['User']['firstname'] != '' ? '<span style="color:#b40b8a;">' .$arr['User']['firstname'] .' '.$arr['User']['lastname'].'</span>' : 'SeoNitro'); ?></td>
                                <td style="color: <?php echo $clor; ?>"><?php echo $arr['Commonmodel']['name']; ?></td>
                                <td><?php echo date('M d, Y', strtotime($arr['Domain']['expires_on'])); ?></td>

                                 <td class="tip" title="<?php echo $stats; ?>"><?php echo intval($arr['Domain']['pr']). ' / ' . intval($arr['Domain']['cflow']) .' / '  . intval($arr['Domain']['tflow']) .' <br> '  .  intval($arr['Domain']['seomoz_rank']) .' / '. intval($arr['Domain']['ext_backlinks']); ?></td>
                                
                                <td><?php echo $arr['Domain']['httpcode']; ?></td>
                                <td><?php echo $arr['Domain']['httpstatus']; ?></td>

                                <td><?php echo $arr['Domain']['cms'].'<br>'.$arr['Domain']['ausername'].'<br>'.$arr['Domain']['apassword']; ?></td>
                                <td style="text-align: left;">
                                    <div class="controls left">
                                        <a href="<?php echo adminHome . '/editdomain/' . $arr['Domain']['id']; ?>" title="Edit Domain" class="tip">edit</a>
                                        | <a href="<?php echo adminHome . '/deletedomain/' . $arr['Domain']['id']; ?>" title="Remove Domain for list?" class="tip callAction">delete</a> <br>---------<br>

                                        <a href="<?php echo adminHome . '/statuschange/' . $arr['Domain']['id'].'/ok'; ?>" title="Put it back to live mode" class="tip callAction">Mark Fixed</a> <br>---------<br>
                                      
                                        <a target="_blank" href="http://who.is/whois/<?php echo $arr['Domain']['domain']; ?>" title="See Who Is Data" class="tip">Who Is</a> | 
                                        <a target="_blank" href="http://whois.net/whois/<?php echo $arr['Domain']['domain']; ?>" title="See Who Is Data" class="tip">Who Is</a>
                                        
                                    </div>
                                </td>
                            </tr>

                            <?php
                                endforeach;
                            else:
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="11">No Problematic Domain founds!</td>
                        </tr>
                        <?php
                            endif;
                    ?>
                </tbody>
            </table>

        </div>
    </div><!-- End .span6 -->
</div>
