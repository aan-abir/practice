<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Rank Ratio</title>
        <meta name="author" content="SuggeElson" />
        <meta name="application-name" content="seonitro control panel version 2" />

        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <?php
        echo $this->Html->css(
                array(
                    '/css/bootstrap/bootstrap', // le styles
                    '/css/bootstrap/bootstrap-responsive',
                    '/css/supr-theme/jquery.ui.supr',
                    '/css/icons',
                    '/plugins/forms/uniform/uniform.default',
                    '/css/main', // main stylesheets
                )
        );
        ?>
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="/images/favicon.ico" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon-144-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon-114-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon-72-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon-57-precomposed.png" />

    </head>

    <body class="loginPage">
        <div class="container-fluid">
            <div id="header">
                <div class="row-fluid">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <div class="container">
                                <a class="brand" href="#"><?php echo $this->Html->image("/images/rankratio-logo.png", array('alt' => "RankRatio", 'width' => 250)); ?><span class="slogan">Administration Panel</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="loginContainer">

                <?php if ($msg != ''): ?>
                    <div class="alert alert-error" style="margin-top: 20px;">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Invalid/Inactive username/password!</strong>
                    </div>
                    <?php
                endif;
                ?>

                <?php echo $this->Form->create('User', array('action' => 'login', 'id' => 'loginForm', 'class' => 'form-horizontal')); ?>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span12" for="username">
                                Username:
                                <span class="icon16 icomoon-icon-user-3 right gray marginR10"></span>
                            </label>
                            <?php echo $this->Form->input('username', array('tabindex' => '1', 'label' => false, 'autofocus', 'id' => 'username', 'class' => 'span12')); ?>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span12" for="password">
                                Password:
                                <span class="icon16 icomoon-icon-locked right gray marginR10"></span>
                                <span class="forgot"><a href="/users/forgotpassword">Forgot your password?</a></span>
                            </label>
                            <?php echo $this->Form->input('password', array('tabindex' => '2', 'type' => 'password', 'label' => false, 'id' => 'password', 'class' => 'span12')); ?>
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="form-actions">
                                <div class="span12 controls">
                                    <input type="checkbox" id="keepLoged" value="Value" class="styled" name="logged" /> Keep me logged in
                                    <button type="submit" class="btn btn-info right" id="loginBtn"><span class="icon16 icomoon-icon-enter white"></span> Login</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php echo $this->Form->end(); ?>
            </div>

        </div><!-- End .container-fluid -->

        <!-- Le javascript
        ================================================== -->

        <?php
        echo $this->Html->script(
                array(
                    '/js/jquery.min',
                    '/js/bootstrap/bootstrap.min',
                    '/plugins/forms/validate/jquery.validate.min',
                    '/plugins/forms/uniform/jquery.uniform.min',
                )
        );
        ?>

        <script type="text/javascript">
            // document ready function
            $(document).ready(function() {
                $("input, textarea, select").not('.nostyle').uniform();
                $("#loginForm").validate({
                    rules: {
                        'data[User][username]': {
                            required: true,
                            minlength: 4
                        },
                        'data[User][password]': {
                            required: true,
                            minlength: 4
                        }
                    },
                    messages: {
                        'data[User][username]': {
                            required: "Please enter UserName",
                            minlength: "Enter Minimum Length"
                        },
                        'data[User][password]': {
                            required: "Please enter your Password",
                            minlength: "Enter Minimum Length"
                        }
                    }
                });
            });
        </script>


    </body>
</html>
