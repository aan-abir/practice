<style type="text/css">
    .activeBtn{
        background-color: #4e4;
        color:#000;
    }
    table tbody tr td:first-child {text-align: right !important;}
    table tbody td {text-align: left !important;}
</style>
<div class="page-header">
    <h4>Client Name: "<?php echo $data['User']['fullname']; ?>"</h4>
</div>
<div class="marginB10"></div>
<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if(isset($data['User']) && count($data['User']) > 0):
//                pr($data); exit;
                ?>
                <tr>
                    <td class="span2">Package</td>
                    <td><?php echo $data['User']['packagename']; ?></td>
                </tr>
                <tr>
                    <td class="span2">Current Credit</td>
                    <td><?php echo $data['User']['currentcredit']; ?></td>
                </tr>
                <tr>
                    <td class="span2">Username</td>
                    <td><?php echo $data['User']['username']; ?></td>
                </tr>
                <tr>
                    <td class="span2">First Name</td>
                    <td><?php echo $data['User']['firstname']; ?></td>
                </tr>
                <tr>
                    <td class="span2">Last Name</td>
                    <td><?php echo $data['User']['lastname']; ?></td>
                </tr>
                <tr>
                    <td class="span2">Email</td>
                    <td><?php echo $data['User']['email']; ?></td>
                </tr>
                <tr>
                    <td class="span2">Phone</td>
                    <td><?php echo $data['User']['phone']; ?></td>
                </tr>
                <tr>
                    <td class="span2">Skype</td>
                    <td><?php echo $data['User']['skype']; ?></td>
                </tr>
                <tr>
                    <td class="span2">More Info</td>
                    <td><?php echo $data['User']['moreaboutclient']; ?></td>
                </tr>
                <tr>
                    <td class="span2">Status</td>
                    <td><?php echo $data['User']['status'] == 1 ? 'Active' : 'Inactive'; ?></td>
                </tr>
                <?php
            else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="1">No Records Found!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>    
    <div class="marginT10"></div>
</div>