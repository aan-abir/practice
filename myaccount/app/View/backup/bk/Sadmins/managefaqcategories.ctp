<div class="heading">
    <h3>Manage Categories</h3>
</div><!-- End .heading-->

<div class="marginB10"></div>
<div class="page-header">
    <h4>All your FAQ Category</h4>
    <p>Below all the category listed. To take necessary action choose action under actions tab.</p>

</div>

<div class="row-fluid">
    <div class="span12">

        <div class="box">
            <div class="title">
                <h4>
                    <span>Your Category List</span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if (count($allCategory) > 0) {
                            foreach ($allCategory as $category) {
                                //pr($camp);
                                ?>
                                <tr>
                                    <td><?php echo $category['Faqcategory']['id']; ?></td>
                                    <td><?php echo $category['Faqcategory']['category']; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <a href="<?php echo Router::url('editfaqcategory/' . $category['Faqcategory']['id']); ?>" title="Edit Category" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                            <a href="<?php echo Router::url('deletefaqcategory/' . $category['Faqcategory']['id']); ?>" title="Remove Category?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            echo '<tr><td colspan="4">No Category Found!</td></tr>';
                        }
                        ?>



                    </tbody>
                </table>
            </div>

        </div><!-- End .box -->

    </div><!-- End .span6 -->
</div>

