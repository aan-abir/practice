<div class="row-fluid">
    <div class="span12">
        <div class="content">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'creditstouser'), 'id' => 'assignCreditsForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="client_type">Client:</label>
                        <div class="span4">
                            <?php
                            echo $this->Form->select('Credit.id', $users, array('required' => false, 'title' => 'Select User', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false));
                            ?>
                        </div>
                    </div>
                </div>
            </div>                
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="packagename">No. of Credits:</label>
                        <?php echo $this->Form->input('Credit.assign_credit', array('error' => false, 'type' => 'text', 'id' => 'noofcredits', 'title' => 'No. of Credits', 'placeholder' => "No. of Credits", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" class="btn btn-info marginR10">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
            <?php $this->Form->end(); ?>
        </div>
    </div><!-- End .span12 -->
</div>