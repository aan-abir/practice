<div class="marginB10"></div>
<div class="page-header">
    <h4>All the recording resource</h4>
    <p>all the resouces recorded during SEORockstars listed below.</p>

</div>


<div class="content">
    <table class="responsive table table-bordered <?php if (count($allRecording)) echo 'dynamicTable'; ?>">
        <thead>
            <tr>
                <th>Year</th>
                <th>Day</th>
                <th>Speaker</th>
                <th>Avatar</th>
                <th>Youtube</th>
                <th>MindMap</th>
                <th>Notes</th>
                <th>SlideShow</th>
                <th>Title</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                //pr($allPackages);
                if(isset($allRecording) && count($allRecording) > 0):
                    foreach($allRecording as $arr):
                    ?>
                    <tr>

                        <td><?php echo $arr['Recording']['ryear']; ?></td>
                        <td><?php echo $arr['Recording']['day_number']; ?></td>
                        <td><?php echo $arr['Recording']['speaker_name']; ?></td>
                        <td>
                            <?php
                                if ( $arr['Recording']['speaker_avatar'] != '' ) { ?>
                                <a title="Image Preview" class="fancybox tip" href="<?php echo  '/login/media/' . $arr['Recording']['speaker_avatar']; ?>">
                                    <img height="50" width="50" alt="Preview" src="<?php echo  '/login/media/' . $arr['Recording']['speaker_avatar']; ?>" />
                                </a>
                                <?php
                                }else{
                                    echo '';
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if ( $arr['Recording']['ytvideo_id'] != ''  ) {
                                   echo '<a title="External Resouce Preview" class="fancybox-media" href="http://www.youtube.com/watch?v='.trim($arr['Recording']['ytvideo_id']).'">preview</a>';
                                }else{
                                    echo '';
                                }

                        ?></td>
                        <td>
                            <?php
                                if ( $arr['Recording']['mindmap_file'] != '' ) {
                                    echo '<a title="Preview MindMap File" class="fancypdf tip"  href="/login/media/'.$arr['Recording']['mindmap_file'].'">preview</a>';
                                }else{
                                    echo '';
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if ( $arr['Recording']['notes_file'] != '' ) {
                                    echo '<a title="Preview Notes File" class="fancypdf tip" href="/login/media/'.$arr['Recording']['notes_file'].'">preview</a>';
                                }else{
                                    echo '';
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if ( $arr['Recording']['slideshow_file'] != '' ) {
                                    echo '<a title="Click to open Slide Show File" target="_blank" class="tip"  href="http://seonitrov2.seonitro.com/media/'.$arr['Recording']['slideshow_file'].'">preview</a>';
                                }else{
                                    echo '';
                                }
                            ?>
                        </td>
                        <td><?php echo substr( strip_tags($arr['Recording']['title']),0,500); ?></td>
                        <td><?php echo substr( strip_tags($arr['Recording']['description']),0,500); ?></td>
                        <td>
                            <div class="controls center">
                                <a href="<?php echo adminHome . '/editrecording/' . $arr['Recording']['id']; ?>" title="Edit Resource" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                <a href="<?php echo adminHome . '/deletrecording/' . $arr['Recording']['id']; ?>" title="Delete Resource?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="7">No Resources Found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(".fancypdf").click(function(){
        $.fancybox({
            type: 'html',
            autoSize: false,
            content: '<embed src="'+this.href+'#nameddest=self&page=1&view=FitH,0&zoom=80,0,0" type="application/pdf" height="99%" width="100%" />',
            beforeClose: function() {
                $(".fancybox-inner").unwrap();
            }
        }); //fancybox
        return false;
    }); //click

</script>