<div class="marginB10"></div>
<?php
if (isset($userInfo) && $userInfo):
    ?>
    <div class="page-header">
        <h4>MySites of <a href="#" title="Click to View Client Info" class="tip"><?php echo $userInfo['User']['fullname']; ?></a></h4>
        <p>Bellow are all the domains. To take necessary action choose action under actions tab.</p>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'clientmysites', $userInfo['User']['id']), 'method' => 'post')); ?>        
           
                <div class="content">
                    <table class="responsive table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 20px;"></th>
                                <th>Domain Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($allDomains) > 0):
                                foreach ($allDomains as $arr):
                                         $d = 'http://'. str_replace('http://','',$arr['Domain']['domain']);
                                    ?>
                                    <tr>
                                        <td><input type="checkbox" name="data[Checked][<?php echo $arr['Domain']['id']; ?>]" /></td>
                                         <td><a href="<?php echo $d; ?>" title="View Domain" target="_blank"><?php echo $arr['Domain']['domain']; ?></a></td>
                                        <td>
                                            <div class="controls center">
                                                <a href="<?php echo Router::url('editdomain/' . $arr['Domain']['id']); ?>" title="Edit Domain" class="tip">Edit</a> | 
                                                <a href="<?php echo Router::url('deletedomain/' . $arr['Domain']['id']); ?>" title="Remove Domain?" class="tip callAction">Remove</a> | 
                                                <a href="<?php echo Router::url('unassignmysite/' . $arr['Domain']['id']); ?>" title="Unassign Domain?" class="tip callAction">Unassign</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                                ?>
                                <tr>
                                    <td></td>
                                    <td colspan="1"></td>
                                    <td><button class="btn btn-info send-middle" type="submit">Unassign All Selected</button></td>
                                </tr>                                
                                <?php
                            else:
                                ?>
                                <tr>
                                    <td></td>
                                    <td colspan="3">No Assigned Domains Found!</td>
                                </tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
          
            <?php echo $this->Form->end(); ?>        
        </div><!-- End .span6 -->
    </div>
    <?php
endif;
?>