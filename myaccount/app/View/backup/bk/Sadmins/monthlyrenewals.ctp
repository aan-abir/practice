<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">

        <div class="box">
            <div class="title">
                <h4>
                    <span>Clients' Monthly Renewal History</span>
                </h4>
            </div>
            <div class="content">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allUsers)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th style="width: 0;"></th>
                            <th>Renew Date</th>
                            <th>User Name</th>
                            <th>Client Name</th>
                            <th>Package</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if (count($allUsers) > 0):
                            foreach ($allUsers as $arr):
                                //pr($arr);
                                ?>
                                <tr>
                                    <td style="width: 0;"></td>
                                    <td><?php echo date('M d, Y', strtotime($arr['Monthlyrenewal']['renew_date'])); ?></td>
                                    <td><?php echo $arr['User']['username']; ?></td>
                                    <td><?php echo $arr['User']['fullname']; ?></td>
                                    <td><?php echo $arr['User']['packagename']; ?></td>
                                    <td><?php echo $arr['User']['status'] == 1 ? 'Active' : 'Suspended'; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <?php
                                            $actionLabel = 'Paid';
                                            ?>
                                            <a href="<?php echo Router::url('renewal_action/'.  strtolower($actionLabel).'/' . $arr['Monthlyrenewal']['id']); ?>" title="<?php echo $actionLabel; ?>" class="tip callAction"><?php echo $actionLabel; ?></a>
                                        </div>
                                    </td>
                                </tr>

                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr><td colspan="6">No Client Found!</td></tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>

        </div><!-- End .box -->

    </div><!-- End .span6 -->
</div>

