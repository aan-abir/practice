
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'setgoal'), 'id' => 'addDomainForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">I Want To Put </label>
                <div class="left marginT5 marginR10">
                    <?php
                    echo $this->Form->input('Setgoal.campaigns', array('required' => false, 'title' => 'Enter Campaigns Goal', 'maxlength' => '50', 'div' => false, 'label' => false, 'class' => 'span3 select', 'empty' => false));
                    ?> &nbsp; Campaigns
                </div>
            </div>
        </div>
    </div>
<?php echo $this->form->hidden('Setgoal.id', array('value' => $data['Setgoal']['id'])); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">I Want To Put </label>
                <div class="left marginT5 marginR10">
                    <?php
                    echo $this->Form->input('Setgoal.anchors', array('required' => false, 'title' => 'Enter Anchors Goal', 'maxlength' => '50', 'div' => false, 'label' => false, 'class' => 'span3 select', 'empty' => false));
                    ?> &nbsp; Anchors
                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">I Want To Put </label>
                <div class="left marginT5 marginR10">
                    <?php
                    echo $this->Form->input('Setgoal.mysites', array('required' => false, 'title' => 'Enter My Sites Goal', 'maxlength' => '50', 'div' => false, 'label' => false, 'class' => 'span3 select', 'empty' => false));
                    ?> &nbsp; My Sites
                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">I Want To Put </label>
                <div class="left marginT5 marginR10">
                    <?php
                    echo $this->Form->input('Setgoal.manualsites', array('required' => false, 'title' => 'Enter Manual Sites Goal', 'maxlength' => '50', 'div' => false, 'label' => false, 'class' => 'span3 select', 'empty' => false));
                    ?> &nbsp; Manual Sites
                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">I Want To Put </label>
                <div class="left marginT5 marginR10">
                    <?php
                    echo $this->Form->input('Setgoal.postblast', array('required' => false, 'title' => 'Enter Post Blast Goal', 'maxlength' => '50', 'div' => false, 'label' => false, 'class' => 'span3 select', 'empty' => false));
                    ?> &nbsp; Post Blast
                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">I Want To Put </label>
                <div class="left marginT5 marginR10">
                    <?php
                    echo $this->Form->input('Setgoal.linksbuilt', array('required' => false, 'title' => 'Enter Links Built Goal', 'maxlength' => '50', 'div' => false, 'label' => false, 'class' => 'span3 select', 'empty' => false));
                    ?> &nbsp; Links Built
                </div>
            </div>
        </div>
    </div>

    <div class="marginT10"></div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php echo $this->Form->end(); ?>
</div>

<div class="marginB10"></div>

