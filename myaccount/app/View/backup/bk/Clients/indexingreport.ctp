<div class="marginB10"></div>
<div class="content clearfix">
    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($indexingReport)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
        <thead>
            <tr>
                <th class="zeroWidth"></th>
                <th class="textLeft">Title</th>
                <th>Batch Name</th>
                <th>Batch ID</th>
                <th>Date Created</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if (isset($indexingReport) && count($indexingReport) > 0):
                    foreach ($indexingReport as $arr):
                        //$timeGap = floor((time() - strtotime($arr['Linkindexing']['created'])) / (60 * 60));
                    ?>
                    <tr>
                        <td class="zeroWidth"></td>
                        <td class="textLeft"><?php echo $arr['Linkindexing']['title']; ?></td>
                        <td class="textLeft"><?php echo $arr['Linkindexing']['batch_name']; ?></td>
                        <td class="textLeft"><?php echo $arr['Linkindexing']['batch_id']; ?></td>
                        <td><?php echo date('d M, Y', strtotime($arr['Linkindexing']['created'])); ?></td>
                        <td>
                             <a title="Submission Details Of - <?php echo $arr['Linkindexing']['batch_name']; ?> " href="<?php echo Router::url('onehourindexing/'.$arr['Linkindexing']['id'].'/' .$arr['Linkindexing']['batch_id']); ?>" class="openModalDialog">view links and updates</a>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td class="zeroWidth"></td>
                    <td colspan="4">No record found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
</div>
