<script type="text/javascript" src="<?php echo BASEURL; ?>/tiny_mce_new/tiny_mce_gzip.js"></script>
<script language="javascript" src="<?php echo BASEURL; ?>/js-new/jquery.blockUI.js"></script>
<style>

    .synonym_i {
        width: 234px;
        cursor: hand; cursor: pointer;
        background: #fff;
        height: 25px;
        padding: 0px 10px;
        border: 1px #cdcdcd solid;
        text-align: left;
    }

    .synonym_i a {
        color: #000;
        line-height: 25px;
    }

    .synonym_a {
        width: 234px;
        cursor: hand; cursor: pointer;
        background: #7f7f7f;
        height: 25px;
        padding: 0px 10px;
        border: 1px #cdcdcd solid;
        text-align: left;
    }

    .synonym_a a {
        color: #e5e5e5;
        line-height: 25px;
    }

    .group {
        padding: 0px 5px;
        color: #000;
    }

    #thesaurus-box {
        width: 554px;
    }

    #thesaurus-head {
        width: 100%;
        height: 80px;
        text-align: right;
        background: url(/imgz/thesaurus-bg.png) no-repeat top left;
    }

    #thesaurus-mid {
        width: 100%;
        background: url(/imgz/thesaurus-mid.png) repeat-y top left;
    }

    #thesaurus-bottom {
        width: 100%;
        height: 25px;
        background: url(/imgz/thesaurus-bg.png) repeat-x bottom left;

    }

    #protected-box {
        width: 554px;
    }

    #protected-head {
        width: 100%;
        height: 80px;
        text-align: right;
        background: url(/imgz/protected-bg.png) no-repeat top left;
    }

    #protected-mid {
        width: 100%;
        background: url(/imgz/thesaurus-mid.png) repeat-y top left;
    }

    #protected-bottom {
        width: 100%;
        height: 25px;
        background: url(/imgz/protected-bg.png) repeat-x bottom left;

    }

    #syn-box {
        width: 284px;
    }

    #syn-head {
        width: 100%;
        height: 15px;
        text-align: right;
        background: url(/imgz/synonyms-bg.png) no-repeat top left;
    }

    #syn-mid {
        width: 100%;
        background: url(/imgz/synonyms-mid.png) repeat-y top left;
    }

    #syn-bottom {
        width: 100%;
        height: 25px;
        background: url(/imgz/synonyms-bg.png) repeat-x bottom left;

    }

    .td1 {
        line-height: 20px;
        height: 20px;
        background: #fafafa;
        border: 1px #fff solid;
        text-align: left;
    }

    .td0 {
        line-height: 20px;
        height: 20px;
        background: #eaeaea;
        border: 1px #fff solid;
        text-align: left;
    }

    #textfield-2 {
        width: 126px;
        height: 28px;

        float: left;

        background-image: url(/imgz/field2.png);
        background-repeat: no-repeat;
        background-position: top left;

        text-align: center;
        padding: 0px;

        margin: 10px 0px 0px 5px;
    }

    #textfield-2 input {
        border: 0px;
        width: 118px;
        height: 22px;
        line-height: 22px;
        font-size: 13px;
        text-align: left;
        background: transparent;
        margin: 2px 0px 0px 0px;
        color: #333;
    }

    #textfield-3 {
        width: 250px;
        height: 28px;

        float: left;

        background-image: url(/imgz/field3.png);
        background-repeat: no-repeat;
        background-position: top left;

        text-align: center;
        padding: 0px;

        margin: 10px 0px 0px 5px;
    }

    #textfield-3 input {
        border: 0px;
        width: 242px;
        height: 22px;
        line-height: 22px;
        font-size: 13px;
        text-align: left;
        background: transparent;
        margin: 2px 0px 0px 0px;
        color: #333;
    }


</style>

<script language="javascript">
    var original = '';
    var TheWord = '';
    var SelectedSynonyms = new Array();
    var AllSynonyms = new Array();
    var AllFavs = new Array();
    var AllProtected = new Array();

    // Detect if the browser is IE or not.
    // If it is not IE, we assume that the browser is NS.
    var IE = document.all ? true : false

    // If NS -- that is, !IE -- then set up for mouse capture
    if (!IE)
        document.captureEvents(Event.MOUSEMOVE)

    // Set-up to use getMouseXY function onMouseMove
    document.onmousemove = getMouseXY;

    // Temporary variables to hold mouse x-y pos.s
    var tempX = 0
    var tempY = 0

    // Main function to retrieve mouse x-y pos.s

    function getMouseXY(e) {
        if (IE) { // grab the x-y pos.s if browser is IE
            tempX = event.clientX + document.body.scrollLeft
            tempY = event.clientY + document.body.scrollTop
        } else {  // grab the x-y pos.s if browser is NS
            tempX = e.pageX
            tempY = e.pageY
        }
        // catch possible negative values in NS4
        if (tempX < 0) {
            tempX = 0
        }
        if (tempY < 0) {
            tempY = 0
        }
        // show the position values in the form named Show
        // in the text fields named MouseX and MouseY
        //document.Show.MouseX.value = tempX
        //document.Show.MouseY.value = tempY
        return true
    }

    function AddRemoveSynonym(syno) {
        var syno2 = syno.replace(/\s+/g, '');
        var mydiv = $('#syn_' + syno2).get(0);

        if (mydiv.className == 'synonym_i') {

            SelectedSynonyms.push(syno);
            mydiv.className = 'synonym_a';
        } else {
            var temp = new Array();

            for (var i = 0; i < SelectedSynonyms.length; i++) {
                if (SelectedSynonyms[i] != syno) {
                    temp.push(SelectedSynonyms[i]);
                }
            }
            SelectedSynonyms = temp;
            temp = null;
            mydiv.className = 'synonym_i';
        }
        //alert(SelectedSynonyms.join('|'));
    }

    function SynonymsDone() {
        if (SelectedSynonyms.length == 0) {
            alert('Please select at least one word...');
            return;
        }
        var sel = tinyMCE.activeEditor.selection.getContent();
        var space = '';
        if (sel[sel.length - 1] == ' ') {
            space = ' ';
        }
        tinyMCE.activeEditor.selection.setContent('{' + SelectedSynonyms.join('|') + '}' + space);
        //    alert(tinyMCE.activeEditor.selection.getContent());
        //    return;

        $('#favs').get(0).style.display = 'none';
        var mydiv = $('#synonyms').get(0);
        mydiv.innerHTML = '';
        $('#handlers').get(0).style.display = 'none';
    }

    function SynonymsCancel() {
        //alert(SelectedSynonyms.join('|'));
        $('#favs').get(0).style.display = 'none';
        var mydiv = $('#synonyms').get(0);
        mydiv.innerHTML = '';
        $('#handlers').get(0).style.display = 'none';
    }

    function AddNewSynonym() {
        var mydiv = $('#synonyms').get(0);

        var newguy = $('#newsyn').get(0);
        var newguyname = newguy.value;
        newguyname = newguyname.replace(/\s+/g, '');
        AllSynonyms.push(newguy.value);

        var newdiv = document.createElement('div');
        newdiv.className = 'synonym_i';
        newdiv.id = 'syn_' + newguyname;
        newdiv.innerHTML = '<a href="javascript: AddRemoveSynonym(\'' + newguy.value + '\')">' + newguy.value + '</a>';

        newguy.value = '';

        mydiv.appendChild(newdiv);
    }

    function SetFavs() {
        var synonyms = SelectedSynonyms.join('|');
        $.post('/post.php', {what: 'SetFavs', word: TheWord, words: synonyms},
            function(data) {

                if (data == 1) {
                    if (window.confirm('Done! Replace these in current text?')) {
                        SynonymsDone();
                        var mydiv = $('#synonyms').get(0);
                        mydiv.innerHTML = '';
                        $('#handlers').get(0).style.display = 'none';
                        $('#favs').get(0).style.display = 'none';
                    } else {
                        SynonymsCancel();
                    }
                } else {
                    alert('Sorry, there was an error');
                }
        });
    }

    function CancelManageFavs() {
        AllFavs = null;
        AllFavs = new Array();

        $('#myfavs').get(0).innerHTML = '';
        $('#myfavs').get(0).style.display = 'block';

        $('#managefavsx').get(0).style.display = 'none';
    }

    function UpdateManageFavs() {

        var temp = new Array();

        for (var i = 0; i < AllFavs.length; i++) {
            temp.push(AllFavs[i].join('|'));
        }

        var MyFavs = temp.join('||');
        //    alert(MyFavs);
        $.post("/post.php", {what: "UpdateManageFavs", favs: MyFavs},
            function(data) {
                if (data == 1) {
                    CancelManageFavs();
                } else {
                    alert('Sorry, error');
                }
            }
        );

    }

    function AddNewFavorite() {
        var newstring = '';

        var fav1 = $('#newfav1').get(0);
        var fav2 = $('#newfav2').get(0);

        var temp = new Array();
        var SynVal = '- not set -';
        if (fav1.value != '') {
            SynVal = fav1.value;
        }
        temp.push(SynVal);


        SynVal = '- not set -';
        if (fav2.value != '') {
            var f2 = fav2.value.split(',');

            for (var i = 0; i < f2.length; i++) {
                temp.push($.trim(f2[i]));
            }
        } else {
            temp.push(SynVal);
        }

        AllFavs.push(temp);

        ListFavorites();

        fav1.value = 'New Word';

        fav2.value = 'Synonyms';

    }

    function ListFavorites() {
        $('#myfavs').get(0).innerHTML = '';

        var TheOtherFavs = new Array();

        for (var i = 0; i < AllFavs.length; i++) {
            TheOtherFavs = null;
            TheOtherFavs = new Array();

            var newdiv = document.createElement('div');

            newdiv.className = 'group';
            newdiv.id = 'gr_' + i;

            if (i % 2 == 1) {
                var theclass = 'td1';
            } else {
                var theclass = 'td0';
            }

            if (AllFavs[i].length > 1) {
                for (var j = 1; j < AllFavs[i].length; j++) {
                    TheOtherFavs.push(AllFavs[i][j]);
                }
                var FavsString = TheOtherFavs.join(', ');
            } else {
                var FavsString = '- not set -';
            }

            var myhtml = '';
            myhtml = '<table width="" cellpadding="0" cellspacing="0"><tr>';
            myhtml += '<td class="' + theclass + '" width="113" valign="middle">&nbsp;&nbsp;' + AllFavs[i][0] + '</td>';
            myhtml += '<td class="' + theclass + '" width="390" valign="middle">&nbsp;&nbsp;' + FavsString + '</td>';
            myhtml += '<td class="' + theclass + '" width="30" align="center" valign="middle"><a href="javascript: RemoveFav(' + i + ');"><img src="/imgz/delete-btn.png" border="0" /></a></td>';
            myhtml += '</tr></table>';

            newdiv.innerHTML = myhtml;
            //                            newdiv.innerHTML = AllFavs[i][0] + ' = ' + AllFavs[i].join(',') + '&nbsp;&nbsp; <a href="javascript: RemoveFav('+i+');">x</a>';

            $('#myfavs').get(0).appendChild(newdiv);
        }

        $('#LoadingMessage').get(0).style.display = 'none';
    }

    function CancelProtectedW() {
        AllProtected = null;
        AllProtected = new Array();

        $('#protectedws').get(0).innerHTML = '';
        $('#protectedws').get(0).style.display = 'block';

        $('#protectedw').get(0).style.display = 'none';
    }

    function RemoveFav(x) {
        if (!window.confirm("Are you sure?")) {
            return;
        }
        var temp = new Array();
        for (var i = 0; i < x; i++) {
            temp.push(AllFavs[i]);
        }

        for (var i = x + 1; i < AllFavs.length; i++) {
            temp.push(AllFavs[i]);
        }
        AllFavs = temp;
        temp = null;

        ListFavorites();
    }

    function RemoveProtected(x) {
        if (!window.confirm("Are you sure?")) {
            return;
        }
        var temp = new Array();
        for (var i = 0; i < x; i++) {
            temp.push(AllProtected[i]);
        }

        for (var i = x + 1; i < AllProtected.length; i++) {
            temp.push(AllProtected[i]);
        }
        AllProtected = temp;
        temp = null;

        ListProtectedWords();
    }

    function UpdateProtectedW() {

        var MyFavs = AllProtected.join('|');
        //    alert(MyFavs);
        $.post("/post.php", {what: "UpdateProtectedWords", protected: MyFavs},
            function(data) {
                if (data == 1) {
                    CancelProtectedW();
                } else {
                    alert('Sorry, error');
                }
            }
        );

    }

    function ListProtectedWords() {
        $('#protectedws').get(0).innerHTML = '';

        for (var i = 0; i < AllProtected.length; i++) {

            var newdiv = document.createElement('div');

            newdiv.className = 'group';
            newdiv.id = 'gr_' + i;

            if (i % 2 == 1) {
                var theclass = 'td1';
            } else {
                var theclass = 'td0';
            }

            var myhtml = '';
            myhtml = '<table width="" cellpadding="0" cellspacing="0"><tr>';
            myhtml += '<td class="' + theclass + '" width="505" valign="middle">&nbsp;&nbsp;' + AllProtected[i] + '</td>';
            myhtml += '<td class="' + theclass + '" width="30" align="center" valign="middle"><a href="javascript: RemoveProtected(' + i + ');"><img src="/imgz/delete-btn.png" border="0" /></a></td>';
            myhtml += '</tr></table>';

            newdiv.innerHTML = myhtml;

            $('#protectedws').get(0).appendChild(newdiv);
        }
    }

    function AddNewProtectedW() {

        var newguy = $('#addnewprotectedword').get(0);
        AllProtected.push(newguy.value);

        newguy.value = 'Add New Protected Word';

        ListProtectedWords();
    }

</script>



<div id="managefavsx" style="width: 554px; display: none; z-index: 99;">
    <div id="thesaurus-box">
        <div id="thesaurus-head">
            <a href="javascript: UpdateManageFavs();"><img src="/imgz/save-changes-btn.png" border="0" style="margin-top: 10px;" /></a>
            <a href="javascript: CancelManageFavs();"><img src="/imgz/cancel-btn.png" border="0" style="margin-top: 10px; margin-right: 20px;" /></a>
        </div>
        <div id="thesaurus-mid">
            <div id="myfavs" style="width: 100%;"></div>


            <div style="width: 538px; margin: 10px 5px 0px 5px; height: 50px; background: url(/imgz/blue-bg.png) top left repeat-x;">
                <div id="textfield-2" style="margin: 10px 0px 0px 5px;">
                    <input type="text" name="newfav1" id="newfav1" value="New Word" onclick="javascript: if (this.value == 'New Word') {
                        this.value = '';
                        }">
                </div>
                <div id="textfield-3" style="margin: 10px 10px 0px 10px;">
                    <input type="text" name="newfav2" id="newfav2" value="Synonyms" onclick="javascript: if (this.value == 'Synonyms') {
                        this.value = '';
                        }">
                </div>
                <a href="javascript: AddNewFavorite();"><img src="/imgz/add-new-favorite-btn.png" border="0" style="margin: 10px 0px 0px 0px;" /></a>

            </div>

            <div style="width: 530px; margin: 10px 5px 0px 15px;">

                <small>When done with adding new synonyms, click on Save Changes button.</small>
            </div>

        </div>
        <div id="thesaurus-bottom"></div>
    </div>

</div>

<div id="protectedw" style="width: 554px; display: none; z-index: 99;">
    <div id="protected-box">
        <div id="protected-head">

            <a href="javascript: UpdateProtectedW();"><img src="/imgz/save-changes-btn.png" border="0" style="margin-top: 10px;" /></a>
            <a href="javascript: CancelProtectedW();"><img src="/imgz/cancel-btn.png" border="0" style="margin-top: 10px; margin-right: 20px;" /></a>
        </div>
        <div id="protected-mid">
            <div id="protectedws" style="width: 100%;"></div>
            <div style="width: 100%">
                <div style="width: 538px; margin: 10px 5px 0px 5px; height: 50px; background: url(/imgz/blue-bg.png) top left repeat-x;">
                    <div id="textfield-3" style="margin: 10px 0px 0px 10px;">
                        <input type="text" id="addnewprotectedword" value="Add New Protected Word" onclick="javascript: if (this.value == 'Add New Protected Word') {
                            this.value = '';
                            }">
                    </div>
                    <a href="javascript: AddNewProtectedW()"><img src="/imgz/add-new-word-btn.png" border="0" style="margin: 10px 0px 0px 10px;" /></a>
                </div>
            </div>
            <div style="width: 530px; margin: 10px 5px 0px 15px;">
                <small>When done with adding words, click on Save Changes button.</small>
            </div>
        </div>
        <div id="protected-bottom"></div>
    </div>
</div>
<div id="favs" style="display: none; z-index: 99;">
    <div id="syn-box">
        <div id="syn-head"></div>
        <div id="syn-mid">
            <div style="width: 264px; margin: 0px 10px;">
                <div id="synonyms"></div>
            </div>
            <div style="width: 269px; height: 50px; background: url(/imgz/blue-bg.png) top left no-repeat; margin-left: 5px; margin-top: 10px;">
                <div id="textfield-2">

                    <input type="text" id="newsyn">
                </div>
                <a href="javascript: AddNewSynonym();"><img src="/imgz/add-new-synonym-btn.png" style="margin: 10px 0px 0px 7px;" border="0" /></a>
            </div>
            <center>
                <div id="handlers">
                    <a href="javascript: SetFavs();"><img src="/imgz/save-as-favorites-btn.png" style="margin: 10px 0px 0px 0px;" border="0" /></a>
                    <br />
                    <a href="javascript: SynonymsDone();"><img src="/imgz/done-btn.png" style="margin: 10px 0px 0px 0px;" border="0" /></a>
                    <a href="javascript: SynonymsCancel();"><img src="/imgz/cancel2-btn.png" style="margin: 10px 0px 0px 0px;" border="0" /></a>
                </div>
            </center>
        </div>
        <div id="syn-bottom"></div>
    </div>
</div>


<script type="text/javascript">
    tinyMCE_GZ.init({
        plugins: 'directionality,fullscreen,inlinepopups,linkvine,media,nonbreaking,noneditable,paste,spellchecker,visualchars,wordcount,xhtmlxtras',
        themes: 'simple,advanced',
        languages: 'en',
        disk_cache: true,
        debug: false
    });
    tinyMCE.init({
        // General options
        mode: "textareas", editor_deselector: "mceNoEditor", width: "100%", theme: "advanced",
        plugins: "safari,inlinepopups,spellchecker,media,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,wordcount,linkvine",
        // Theme options
        theme_advanced_buttons1: "bold,italic,strikethrough,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyright,|,link,unlink,wp_more,|,spellchecker,fullscreen,|,rewrite1,rewrite2,rewrite3,rewrite4,rewrite5,rewrite6",
        theme_advanced_buttons2: "formatselect,underline,justifyfull,forecolor,|,pastetext,pasteword,removeformat,|,media,charmap,|,outdent,indent,|,undo,redo,|,code,|,",
        theme_advanced_buttons3: "",
        theme_advanced_buttons4: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,
        spellchecker_languages: "+English=en,Danish=da,Dutch=nl,Finnish=fi,French=fr,German=de,Italian=it,Polish=pl,Portuguese=pt,Spanish=es,Swedish=sv",
        // Example content CSS (should be your site CSS)
        //        content_css : "editor.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url: "lists/template_list.js",
        external_link_list_url: "lists/link_list.js",
        external_image_list_url: "lists/image_list.js",
        media_external_list_url: "lists/media_list.js",
        // Style formats
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ],
        // Replace values for the template plugin
        template_replace_values: {
            username: "Some User",
            staffid: "991234"
        },
        extended_valid_elements: "a[class|name|href|target|title|onclick|rel],script[type|src],iframe[src|style|width|height|scrolling|marginwidth|marginheight|frameborder],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],$elements",
    });
</script>


<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'addsubmission'), 'id' => 'addSubmissionForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="normal">Name of the Submission *</label>
                <div>
                    <?php echo $this->Form->input('Submission.submissionname', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Name Of the Submission', 'div' => false, 'label' => false, 'class' => 'span4  tip')); ?>

                    <!--span class="help-inline blue">name of submission so that you can identify each submission</span--></div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="normal">Start Date *</label>
                <?php echo $this->Form->input('Submission.startdate', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'when to start posting', 'div' => false, 'label' => false, 'class' => 'span4 datepicker  tip', 'readonly' => 'readonly')); ?>
                <span class="help-inline blue">when to start posting</span>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="normal">End Date *</label>
                <?php echo $this->Form->input('Submission.enddate', array('error' => false, 'id' => 'endDate', 'type' => 'text', 'required' => false, 'title' => 'finish posting submission by this date', 'div' => false, 'label' => false, 'class' => 'span4 datepicker  tip', 'readonly' => 'readonly')); ?>
                <span class="help-inline blue">finish posting submission by this date</span>
            </div>
        </div>
    </div>
    <?php echo $this->Form->hidden('Submission.pr_service_id',array('value'=>3)); ?>
    <!-- -->
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="normal">Select Submission Type *</label>
                <style>.radio-div{padding:6px 0}.radio-div label{display: inline;} .sradio{display:inline;width:16px!important;}</style>
                <div class="span4 radio-div">
                    <?php
                        $rAttributes = array('legend' => false, 'class' => 'sradio', 'value' => -1);
                        echo $this->Form->radio('Submission.submissiontype', $rOptions, $rAttributes);
                    ?>
                </div>
                <?php
                    if (count($rOptions) == 1):
                    ?>
                    <span class="help-inline red bold">you don't have any private network yet</span>
                    <?php
                        else:
                    ?>

                    <span class="help-inline red bold">you have <?php echo $hasPrivate; ?> private site/s</span>
                    <?php
                        endif;
                ?>

            </div>
        </div>
    </div>
    <!--<div class="form-row row-fluid" id="SubmissionTypeDropdownDiv">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="pr_service_id">Select Network:</label>
                <div class="span4">
                    < ?php
                        echo $this->Form->select('Submission.pr_service_id', $services, array('required' => false, 'title' => 'select a network where you want to post. not applicable for private newtwork', 'div' => false, 'label' => false, 'class' => 'span2 select tip', 'empty' => false));
                    ?>
                </div>
                <div class="span4"><span class="help-inline blue">select a network where you want to post. not applicable for private newtwork</span></div>
            </div>
        </div>
    </div>-->



    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="normal">How many times to post *</label>
                <?php echo $this->Form->input('Submission.howmanytopost', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Name Of the Submission. MUST NOT be grater than available number of domain in an network showing the drop down above.', 'div' => false, 'label' => false, 'class' => 'span2 tip')); ?>
                <span  id="SubmissionTypeDropdownInstruction" class="help-inline red bold">[ Max Possible Post - <?php echo '289';// $tAutoSite; ?> ]</span>

            </div>
        </div>
    </div>
    <div class="form-row row-fluid" id="SubmissionTypeDropdownDiv">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="SubmissionCategoryId">Select Category:</label>
                <div class="span2">
                    <?php
                        echo $this->Form->select('Submission.category_id', $allCats, array('required' => false, 'title' => 'select article category', 'div' => false, 'label' => false, 'class' => 'span2 select tip', 'empty' => false));
                    ?>
                </div>
                <div class="span4"><span class="help-inline blue">select article category</span></div>
            </div>
        </div>
    </div>








    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Title of the Post <span class="help-block-inline">this will be the title for a post.</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Submission.posttitle', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Title of the Post', 'div' => false, 'label' => false, 'class' => 'span12')); ?>
            </div>
        </div>
    </div>


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Enter your Article below <span class="help-block-inline">enter your article to post. be careful about spun identifier []. close it correctly</span></h4>
            </div>
            <div class="form-row">
                <center>
                    <div id="LoadingMessage" style="display: none;">
                        <table>
                            <tr>
                                <td valign="middle" align="center"><img src="<?php echo BASEURL; ?>images/ajax-loader.gif" border="0" /></td>
                                <td valign="middle" align="center">Loading, please wait ...</td>
                            </tr>

                        </table>
                    </div>
                </center>
                <?php echo $this->Form->textarea('Submission.postcontent', array('id' => 'theEditor', 'required' => false, 'rows' => '25', 'cols' => '80', 'style' => 'width: 886px')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="status">Submission Status:</label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Submission][status]" id="UserStatus1" value="1" checked="checked" />
                    Active
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Submission][status]" id="UserStatus0" value="0" />
                    Inactive
                </div>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Add New Submission</button>
                        <button type="button" class="openPreviewModalDialog btn btn-warning" href="<?php echo Router::url('articlepreview'); ?>">Preview</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

<script type="text/javascript">
    //------------- Datepicker -------------//
    if ($('.datepicker').length) {
        $('.datepicker').datepicker({
            showOtherMonths: true,
            //dateFormat: 'yy-m-d'
            minDate: '+1'
        });
    }
</script>


<div id="previewModal" style="width: 100% !important; height: 100% !important;"  title="Preview" class="dialog"></div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.openPreviewModalDialog').click(function() {
            //alert('here'); return false;
            _this = $(this);
            $title = _this.attr('title');
            $modal = $('#previewModal');
            $("#ui-id-1").html($title);

            url = _this.attr('href');

            $modal.css({textAlign: 'center', minHeight: '400px'}).html('<p>Loading Please Wait...</p><p><?php echo $this->Html->image('/images/loader.gif', array('alt' => false)); ?></p>');
            $modal.dialog('open');
            if (url.indexOf('#') == 0) {
                $(url).modal('open');
            } else {
                $.ajax({
                    type: "post",
                    url: '<?php echo Router::url(array('controller' => 'clients', 'action' => 'articlepreview')); ?>',
                    data: {
                        'post_title': $('#SubmissionPosttitle').val(),
                        'post_content': tinyMCE.activeEditor.getContent()
                    },
                    beforeSend: function() {
                    }
                }).done(function(msg) {
                    //alert(msg);
                    $modal.css({textAlign: 'center', minHeight: '200px'}).html(msg);
                });
            }

            return false;
        });
        // JQuery UI Modal Dialog
        $('#previewModal').dialog({
            autoOpen: false,
            modal: true,
            autoResize: true,
            dialogClass: 'dialog',
            buttons: {
                "Close": function() {
                    $(this).dialog("close");
                }
            }
        });

    });
</script>

<script type="text/javascript">
    $('input[type="radio"][name="data[Submission][submissiontype]"]').change(function() {
        var option_val = $(this).val();
        if (option_val == -1) {
            $('#SubmissionTypeDropdownDiv').show();
            $('#SubmissionTypeDropdownInstruction').show();
        } else if (option_val == 1) {
            $('#SubmissionTypeDropdownDiv').hide();
            $('#SubmissionTypeDropdownInstruction').hide();
        }
        return false;
    });
</script>