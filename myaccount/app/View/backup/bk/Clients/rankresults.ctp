<style>
    .textLeft{text-align:left !important;}
</style>
<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>All Existing Rank Campaigns</span>
                </h4>
            </div>
            <div class="content clearfix">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($rankCampaigns)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th><strong>Campaign Name</strong></th>
                            <th class="textLeft"><strong>Domain/URL</strong></th>
                            <th class="textLeft"><strong>Keywords</strong></th>
                            <th><strong>Engines</strong></th>
                            <th><strong>Exact</strong></th>
                            <th><strong>Status</strong></th>
                            <th><strong>Updated</strong></th>
                            <th><strong>Actions</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($rankCampaigns) && count($rankCampaigns) > 0):
                            foreach ($rankCampaigns as $campaign):
                                $arr = $campaign['Rankcampaign'];
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td><?php echo $arr['campaign_name']; ?></td>
                                    <td class="textLeft"><?php echo nl2br($arr['c_urls']); ?></td>
                                    <td class="textLeft"><?php echo nl2br($arr['c_keywords']); ?></td>
                                    <td>
                                        <?php
                                        echo $arr['google'] == 1 ? ' Google - ' . $arr['glocale'] . ' <br>' : '';
                                        echo $arr['yahoo'] == 1 ? ' Yahoo - ' . $arr['ylocale'] . ' <br>' : '';
                                        echo $arr['bing'] == 1 ? ' Bing - ' . $arr['blocale'] . ' <br>' : '';
                                        echo $arr['amazon'] == 1 ? ' Amazon <br>' : '';
                                        echo $arr['youtube'] == 1 ? ' Youtube <br>' : '';
                                        ?>
                                    </td>
                                    <td><?php echo $arr['exact_match'] == 1 ? 'Yes' : ''; ?></td>
                                    <td><?php echo $arr['status'] == 1 ? 'Active' : 'Inactive'; ?></td>
                                    <td><?php echo date('M d, Y', strtotime($arr['last_update'])); ?></td>
                                    <td>

                                        <div class="controls center">
                                            <a href="<?php echo Router::url('editrankcampaign/' . $arr['id']); ?>" title="Edit Rank Campaign" class="tip">Edit</a> | 
                                            <a href="<?php echo Router::url('activation_change/' . $arr['id'] . '/' . $arr['status']); ?>" title="Click here to <?php echo $arr['status'] == 0 ? 'Active' : 'Inactive'; ?>?" class="tip callAction"><?php echo $arr['status'] == 0 ? 'Active' : 'Inactive'; ?></a> | 
                                            <a href="<?php echo Router::url('deleterankcampaign/' . $arr['id']); ?>" title="Remove Rank Campaign?" class="tip callAction">Remove</a> | 
                                            <a href="<?php echo Router::url('viewrankresult/' . $arr['id']); ?>">View Result</a>
                                        </div>                                        
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="8">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>