<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Sitewide Keyword Results for: "<?php echo $kw; ?>" || Campaign: <a href="<?php echo Router::url('viewcampaign/' . $campaignInfo['Campaign']['id']); ?>"><?php echo $campaignInfo['Campaign']['campignname']; ?></a> [dcl=<?php echo $campaignInfo['Campaign']['id']; ?>]</span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>Domain</th>
                            <th>Keyword in Sidebar</th>
                            <th>Keyword in Footer</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($blogrollResults):
                            foreach ($blogrollResults as $arr):
                                //if ($arr['Trackeddomain']['keyword_in_sidebar'] || $arr['Trackeddomain']['keyword_in_footer']):
                                ?>
                                <tr>
                                    <td><?php echo $arr['Trackeddomain']['pinger']; ?></td>
                                    <td><?php echo $arr['Trackeddomain']['keyword_in_sidebar']; ?></td>
                                    <td><?php echo $arr['Trackeddomain']['keyword_in_footer']; ?></td>
                                </tr>
                                <?php
                                //endif;
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="2">No Record Found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span6 -->
</div>
<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>In Content Keyword Results for: "<?php echo $kw; ?>" || Campaign: <a href="<?php echo Router::url('viewcampaign/' . $campaignInfo['Campaign']['id']); ?>"><?php echo $campaignInfo['Campaign']['campignname']; ?></a> [dcl=<?php echo $campaignInfo['Campaign']['id']; ?>]</span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>URL</th>
                            <th>Keyword in Content (Maximum)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($campaignResults):
                            foreach ($campaignResults as $arr):
                                ?>
                                <tr>
                                    <td><a target="_blank" href="<?php echo $arr['Trackeddomain']['pinger']; ?>"><?php echo $arr['Trackeddomain']['pinger']; ?></a></td>
                                    <td><?php echo $arr['Trackeddomain']['keyword_in_content']; ?></td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="1">No Record Found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
                <div id="pagingPmb">
                    <?php
                    if ($this->Paginator->numbers() != '')
                        echo '<em> Pages: </em>';
                    if ($this->Paginator->hasPrev())
                        echo $this->Paginator->prev(__('Previous '), array(), null, array('class' => 'prev disabled'));
                    echo $this->Paginator->numbers();
                    if ($this->Paginator->hasNext())
                        echo $this->Paginator->next(__(' Next'), array(), null, array('class' => ' next disabled'));
                    ?>
                    <span> *<?php echo $this->Paginator->counter('{:count} Total'); ?></span>
                </div>                
            </div>
        </div><!-- End .box -->
    </div><!-- End .span6 -->
</div>