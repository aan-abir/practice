<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>Market</th>
                <th>Title</th>
                <th>Content (500 chars)</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if (isset($guestPosts) && count($guestPosts) > 0):
                    foreach ($guestPosts as $arr):
                    ?>
                    <tr>
                        <td><?php echo $arr['Guestpost']['postmarket']; ?></td>
                        <td><?php echo $arr['Guestpost']['title']; ?></td>
                        <td><?php echo substr( strip_tags($arr['Guestpost']['postdraft']),0,500); ?></td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="3">No In Process Guestpost Found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
</div>
