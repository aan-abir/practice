<div class="clear_both"></div>
<h3>Link filtering for <?php echo $_SESSION['session_domain'] ?></h3>
<div class="clear_both"></div>
<div class="marginB10"></div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'ld_filter'), 'id' => 'addRankCampaignForm', 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="campaign_name">Filter by Page Rank:</label>
                        <div class="span2">
                            <?php echo $this->Form->select('Ldreportcron.pr', $all_pr, array('required' => false, 'required' => false, 'id' => 'pr', 'title' => 'Select Page Rank', 'div' => false, 'label' => false, 'class' => 'span12 select', 'empty' => false)); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="campaign_name">Filter by Indexed:</label>
                        <div class="span2">
                            <?php echo $this->Form->select('Ldreportcron.index', array('' => 'Select Index', 1 => 'Yes', 0 => 'No'), array('required' => false, 'required' => false, 'id' => 'indexed', 'title' => 'Select indexed', 'div' => false, 'label' => false, 'class' => 'span12 select', 'empty' => false)); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="campaign_name">Filter by Link Status:</label>
                        <div class="span2">
                            <?php echo $this->Form->select('Ldreportcron.LinkType', $all_LinkType, array('required' => false, 'required' => false, 'id' => 'LinkType', 'title' => 'Select Link Type', 'div' => false, 'label' => false, 'class' => 'span12 select', 'empty' => false)); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="campaign_name">Filter by Follow/NoFollow Link:</label>
                        <div class="span3">
                            <?php echo $this->Form->select('Ldreportcron.FlagNoFollow', array('' => 'Select Follow/NoFollow', 1 => 'NoFollow', 0 => 'Follow'), array('required' => false, 'required' => false, 'id' => 'LinkType', 'title' => 'Select Link Type', 'div' => false, 'label' => false, 'class' => 'span12 select', 'empty' => false)); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" onclick="$('html').addClass('loadstate');" class="btn btn-info marginR10">Filter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  

            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>

</div>
<?php
if (isset($ld_filter)) {
    echo $this->element('ld_ajax_list');
}
?>