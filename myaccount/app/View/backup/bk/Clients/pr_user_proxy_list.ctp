<div class="content clearfix">
    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($page_rank_user_proxy) > 1) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
        <thead>
            <tr>
                <th><strong>IP address</strong></th>
                <th><strong>Username</strong></th>
                <th><strong>Password</strong></th>
                <th><strong>Port</strong></th>
                <!--<th><strong>Status</strong></th>-->
                <th><strong>Date Created</strong></th>
                <th><strong>Actions</strong></th>
            </tr>
        </thead>
        <tbody>
            <?php
                if (isset($page_rank_user_proxy) && count($page_rank_user_proxy) > 0):
                    foreach ($page_rank_user_proxy as $row):
                        $arr = $row['Pruserproxy'];
                    ?>
                    <tr>
                        <td><?php echo $arr['ip_address']; ?></td>
                        <td><?php echo $arr['username']; ?></td>
                        <td><?php echo $arr['password']; ?></td>
                        <td>
                            <?php echo $arr['port']; ?>
                        </td>
                        <!--<td><?php echo $arr['port'] == 1 ? 'Active' : 'Iancite'; ?></td>-->
                        <td><?php echo date("d M Y", strtotime($arr['created_at'])); ?></td>
                        <td>

                            <div class="controls center">
                                <a href="<?php echo Router::url('pr_user_proxy_edit/' . $arr['id']); ?>" title="Edit Page Rank User Proxy" class="tip">Edit</a> |
                                <a href="<?php echo Router::url('pr_user_proxy_delete/' . $arr['id']); ?>" title="Remove Page Rank User Proxy?" class="tip callAction">Remove</a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="4">No record found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
            </div>

