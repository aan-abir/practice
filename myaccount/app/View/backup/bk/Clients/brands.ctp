<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'brands'), 'id' => 'brandingUpdateForm', 'method' => 'post', 'type'=>'file', 'class' => 'form-horizontal seperator')); ?>



    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="email">Company Email:</label>
                <?php echo $this->Form->input('User.bemail', array('type' => 'text', 'id' => 'bemail', 'title' => 'Brand Email', 'placeholder' => "Brand  Email", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                 <span class="help-inline blue">email will be used when sending report/updates to your clients</span>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="textarea">Upload your Logo</label>
                <input type="file" name="data[User][logo]" id="logo" title="Upload Logo" />
                <input type="checkbox" name="data[User][delOld]" id="delOld" title="Delete Old One" /> Delete Old Logo

                <span class="help-inline blue">jpg, jpeg, png and gif file only. [ max dimension 300 x 300 ] </span>
                <br>
                <?php

                    $folder = WWW_ROOT . 'branding';
                    if (@file_exists( $folder . '/' . $data['User']['logo'] )) {
                    ?>
                    <a title="Your Current Logo" class="fancybox tip" href="<?php echo  '/branding/' . $data['User']['logo']; ?>">
                        <img width="100" alt="uploaded logo" src="<?php echo  '/branding/' . $data['User']['logo']; ?>" />
                    </a>
                    <?php

                    }

                ?>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Enter company contact information  <span class="help-block-inline"> data will be used when sending an email to your clients</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('User.companycontact', array('id' => 'companycontact', 'title' => 'Company Contact Info', 'div' => false, 'label' => false, 'class' => 'span11 limit elastic', 'rows' => 3, 'cols' => '5')); ?>
            </div>
        </div>
    </div>


    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Update Information</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

<script type="text/javascript">
    // document ready function
    $(document).ready(function() {

        //------------- Elastic textarea -------------//
        if ($('textarea').hasClass('elastic')) {
            $('.elastic').elastic();
        }

        //$("input, textarea, select").not('.nostyle').uniform();

        /*$.validator.addMethod(
        "atLeastOneAnchorText",
        function(value, element) {
        //alert($(element).val().length);
        return ($(element).val().length < 4) ? false : true;
        },
        "Please insert at least one anchor text with minimum 4 chars long"
        );*/


        $("#brandingUpdateForm").validate({
            rules: {
                'data[User][email]': {
                    email: true
                },
                'data[User][logo]': {
                    accept: "png|jpe?g|gif"
                },
            },
            messages: {
                'data[User][email]': {
                    email: "Please Enter Valid Email",
                },
                'data[User][logo]': {
                    accept: "Please Enter Valid File Extension",
                },
                /*'data[Campaign][settings][1][anchortexts]': {
                atLeastOneAnchorText: "Please insert at least one anchor text with minimum 4 chars long"
                }*/
            }

        });


    });


</script>