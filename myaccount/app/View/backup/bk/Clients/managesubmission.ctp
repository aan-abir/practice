<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Manage Auto Posts</span>
                </h4>
            </div>
            <div class="content">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($submissionInfo)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th class="textLeft">Name</th>
                            <th>Network</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Quantity</th>
                            <th>Submitted</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($submissionInfo) && count($submissionInfo) > 0):
                            foreach ($submissionInfo as $arr):
                                $networkType = '';
                                if ($arr['Submission']['submissiontype'] == 1) {
                                    $networkType = 'Private';
                                } elseif ($arr['Submission']['pr_service_id'] == 3) {
                                    $networkType = 'Premium';
                                } elseif ($arr['Submission']['pr_service_id'] == 4) {
                                    $networkType = 'Prime';
                                } elseif ($arr['Submission']['pr_service_id'] == 5) {
                                    $networkType = 'LowGrade';
                                }
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td class="textLeft"><?php echo $arr['Submission']['submissionname']; ?></td>
                                    <td><?php echo $networkType; ?></td>
                                    <td><?php echo $arr['Submission']['startdate']; ?></td>
                                    <td><?php echo $arr['Submission']['enddate']; ?></td>
                                    <td><?php echo $arr['Submission']['howmanytopost']; ?></td>
                                    <td><?php echo $arr['Submission']['totalsubmitted']; ?></td>
                                    <td><?php echo $this->NitroCustom->trimText($arr['Submission']['posttitle'], 150); ?></td>
                                    <!--<td><?php echo $this->NitroCustom->trimText($arr['Submission']['postcontent'], 300); ?></td>-->
                                    <td><?php echo $arr['Submission']['statuslabel']; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <?php if ($this->Session->read('iamadmin')): ?>
                                                    <!--<a href="<?php echo Router::url('submission_status/' . $arr['Submission']['id'] . '/' . $arr['Submission']['status']); ?>" title="Click here to <?php echo ($arr['Submission']['status'] == 0) ? 'Untrash' : 'Trash'; ?>?" class="tip callAction"><?php echo ($arr['Submission']['status'] == 2) ? 'Untrash' : 'Trash'; ?></a> |--> 
                                            <?php endif; ?>
                                            <a href="<?php echo Router::url('editsubmission/' . $arr['Submission']['id']); ?>" title="Edit Submission" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td colspan="10">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>