<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Manage Campaigns</span>
                </h4>
            </div>
            <div class="content clearfix">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allCamps)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Campaign Name</th>
                            <th>Total Anchor Text</th>
                            <th>Get Code</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($allCamps) > 0):
                            foreach ($allCamps as $camp):
                                //pr($camp);
                                $aTextCnt = 0;
                                foreach ((array) $camp['Anchortext'] as $jj => $aCampText):
                                    //pr($aCampText);
                                    $t = explode(",", $aCampText['anchortexts']);
                                    $c = count($t);
                                    $aTextCnt += $c;
                                endforeach;
                                ?>
                                <tr>
                                    <td><a href="<?php echo Router::url('viewcampaign/' . $camp['Campaign']['id']); ?>" title="View Campaign" class="tip"><?php echo $camp['Campaign']['campignname']; ?></a></td>

                                    <td><?php echo $aTextCnt; ?></td>
                                    <td><?php echo "[dcl=" . $camp['Campaign']['id'] . "]"; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <a href="<?php echo Router::url('viewcampaign/' . $camp['Campaign']['id']); ?>" title="View Campaign" class="tip"><span class="icon12 icomoon-icon-grid-view"></span></a>
                                            <a href="<?php echo Router::url('editcampaign/' . $camp['Campaign']['id']); ?>" title="Edit Campaign" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                            <a href="<?php echo Router::url('deletecampaign/' . $camp['Campaign']['id']); ?>" title="Remove Campaign?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>

                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="4">No Campaign Found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>