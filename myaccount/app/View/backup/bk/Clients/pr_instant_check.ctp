<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'pr_instant_check'), 'id' => 'addRankCampaignForm', 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>



            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                        <h4 style="color: #074f14;">Enter domain(s) <span class="help-block-inline">one domain per line</span></h4>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="form-row">
                        <?php echo $this->Form->textarea('Prcampaign.c_urls', array('error' => false, 'required' => false, 'id' => 'c_urls', 'title' => 'enter domains for instant check', 'div' => false, 'label' => false, 'class' => 'span12', 'style' => 'height:220px')); ?>
                    </div>
                </div>
            </div>



            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                        <h4 style="color: #074f14;">Use Your Own Proxy <span class="help-block-inline">using your own proxy does not cost any credits</span></h4>
                    </div>
                </div>
            </div>


            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="span2" for="use_my_own_proxy">Use my own proxy:</label>
                        <?php echo $this->Form->checkbox('Prcampaign.user_proxy', array('error' => false, 'required' => false, 'id' => 'use_my_own_proxy', 'type' => 'text', 'title' => 'Use my own proxy', 'placeholder' => "Use my own proxy", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    </div>
                </div>
            </div>
            <!----------- For using my own proxy start ---------------------->

            <table cellspacing="6" cellpadding="4" id="use_my_own_proxy_part" style="display: <?php
                    if (isset($user_proxy) && $user_proxy == 1)
                        echo 'block';
                    else
                        echo 'none';
                ?>;">
                <thead>
                    <tr>
                        <th> IP Address</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Port</th>
                    </tr>
                </thead>
                <tbody id="campInputRows">
                    <?php
                        if (isset($page_rank_user_proxy) && count($page_rank_user_proxy) > 0):
                            foreach ($page_rank_user_proxy as $row):
                                $arr = $row['Pruserproxy'];
                            ?>
                            <tr id="1">
                                <td valign="top">
                                    <input type="text" name="ip_address[]" value="<?php echo $arr['ip_address']; ?>">
                                </td>
                                <td valign="top">
                                    <input type="text" name="username[]" value="<?php echo $arr['username']; ?>">
                                </td>
                                <td valign="top">
                                    <input type="text" name="password[]" value="<?php echo $arr['password']; ?>">
                                </td>
                                <td valign="top">
                                <input type="text" name="port[]" value="<?php echo $arr['port']; ?>">
                            </tr>
                            <?php
                                endforeach;
                            endif;
                    ?>
                    <?php for ($i = 1; $i <= 2; $i++) { ?>
                        <tr id="1">
                            <td valign="top">
                                <input  name="ip_address[]"  type="text" value="">
                            </td>
                            <td valign="top">
                                <input name="username[]" type="text" value="">
                            </td>
                            <td valign="top">
                                <input name="password[]" type="text" value="">
                            </td>
                            <td valign="top">
                            <input name="port[]" type="text" value="">
                        </tr>
                        <?php } ?>
                </tbody>
            </table>
            <!------------- Use my own proxy end -------------------->
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" onclick="$('html').addClass('loadstate');"class="btn btn-info marginR10">GO!</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>
</div>

<!---------------Instant domain --------------->
<div style="clear:both;"></div>
<!--<a style="position: absolute; left: 50%; z-index: 20" data-toggle="modal" title="Export all the domains" class="btn btn-link tip btn-success" href="<?php echo Router::url('report_csv'); ?>"><span class="icon16 icomoon-icon-file-excel"></span> Export List </a>-->
<?php
    if (isset($all_domain)) {
    ?>
    <!------- For Csv file start ---------->

    <a style="position: absolute; left: 50%; z-index: 20" data-toggle="modal" title="Export all the domains" class="btn btn-link tip btn-success" href="<?php echo Router::url('report_csv'); ?>"><span class="icon16 icomoon-icon-file-excel"></span> Export List </a>
    <!------- For Csv file end ---------->
    <table  width="100%" cellspacing="0" cellpadding="0" border="0" class="responsive  dynamicTable display table table-bordered dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info">
        <thead>
            <tr>
                <th><strong>#</strong></th>
                <th><strong>Domain</strong></th>
                <th><strong>PR</strong></th>
                <th><strong>Indexed</strong></th>
                <th><strong>Created</strong></th>
                <th><strong>Expires</strong></th>
                <!--<th><strong>Age</strong></th>-->
                <th><strong>Name Server</strong></th>
                <th><strong>Seomoz Rank</strong></th>
                <th><strong>Citation Flow</strong></th>
                <th><strong>Trust Flow</strong></th>
                <th><strong>Ext Back Links</strong></th>
            </tr>
        </thead>

        <tbody role="alert" aria-live="polite" aria-relevant="all">
            <?php foreach ($all_domain as $row): $row = $row['Pruserdomain']; ?>
                <tr class="odd">
                    <td class=" "></td>
                    <td class="  sorting_1"><a href="http://<?php echo $row['domains']; ?>" target="_blank"><?php echo $row['domains']; ?></a></td>
                    <td class=" "><?php echo $row['page_rank']; ?></td>
                    <td class=" "><?php echo $row['indexed']; ?></td>
                    <td class=" "><?php echo $row['created']; ?></td>
                    <td class=" "><?php echo $row['expires']; ?></td>
                    <!--<td class=" "><?php echo $row['age']; ?></td>-->
                    <td><?php echo str_replace(", ", "<br/>", $row['name_server']); ?></td>
                    <td class=" "><?php echo $row['seomoz_rank']; ?></td>
                    <td class=" "><?php echo $row['citation_flow']; ?></td>
                    <td class=" "><?php echo $row['trust_flow']; ?></td>
                    <td class=" "><?php echo $row['ext_back_links']; ?></td>

                </tr>
                <?php endforeach; ?>
        </tbody>
    </table>
    <?php
    }
?>
<!------------------------------>
<script>
    $(document).ready(function() {
        $('#use_my_own_proxy').change(function(e) {
            if ($(this).is(":checked")) {
                $('#use_my_own_proxy_part').show('slow');
            }
            else
            {
                $('#use_my_own_proxy_part').hide('slow');
            }
        });
    });
</script>
