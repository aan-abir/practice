
<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>Name</th>
                <th>Anchors</th>
                <th>Services</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($orderList) && count($orderList) > 0):
                foreach ($orderList as $arr):
                    ?>
                    <tr>
                        <td><?php echo $arr['Linkemperor']['ordername']; ?></td>
                        <td>
                            <?php
                            $orders = unserialize($arr['Linkemperor']['orders']);
                            if (count($orders)):
                                ?>
                                <table style="width: 100%;">
                                    <tr>
                                        <th style="border-left: none;">Anchor Text</th>
                                        <th>Anchor URL</th>
                                    </tr>
                                    <?php
                                    foreach ($orders as $key => $order):
                                        ?>
                                        <tr>
                                            <td style="border-left: none;"><?php echo $order['anchor_text']; ?></td>
                                            <td><?php echo $order['url']; ?></td>
                                        </tr>
                                        <?php
                                    endforeach;
                                    ?>
                                </table>
                                <?php
                            endif;
                            ?>
                        </td>
                        <td>
                            <?php
                            $services = unserialize($arr['Linkemperor']['services']);
                            if (count($services)):
                                ?>
                                <table style="width: 100%;">
                                    <tr>
                                        <th style="border-left: none;">Service</th>
                                        <th>Quantity</th>
                                    </tr>
                                    <?php
                                    foreach ($services as $service => $quantity):
                                        ?>
                                        <tr>
                                            <td style="border-left: none;"><?php echo $serviceList[$service]; ?></td>
                                            <td><?php echo $quantity; ?></td>
                                            <td>

                                                <a title="Submission Details Of - <?php echo $serviceList[$service]; ?> " href="<?php echo Router::url('linkempdetails/' . $arr['Linkemperor']['order_id'] . '/' . $service); ?>" class="openModalDialog">Details</a>

                                            </td>
                                        </tr>
                                        <?php
                                    endforeach;
                                    ?>
                                </table>
                                <?php
                            endif;
                            ?>
                        </td>
                        <td><?php echo $arr['Linkemperor']['status']; ?></td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="4">No Record Found!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>
</div>
