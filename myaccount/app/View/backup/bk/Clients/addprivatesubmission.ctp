<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'addprivatesubmission'), 'id' => 'addSubmissionForm', 'method' => 'post')); ?>
    <div class="form-row row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <label class="form-label span6" for="normal">Name of the Submission *</label>
                <?php echo $this->Form->input('Submission.submissionname', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Name Of the Submission', 'div' => false, 'label' => false, 'class' => 'span6')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <label class="form-label span6" for="normal">Start Date *</label>
                <?php echo $this->Form->input('Submission.startdate', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Start Date', 'div' => false, 'label' => false, 'class' => 'span6 datepicker', 'readonly' => 'readonly')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <label class="form-label span6" for="normal">End Date *</label>
                <?php echo $this->Form->input('Submission.enddate', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'End Date', 'div' => false, 'label' => false, 'class' => 'span6 datepicker', 'readonly' => 'readonly')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <label class="form-label span6" for="normal">How many times to post *</label>
                <?php echo $this->Form->input('Submission.howmanytopost', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Name Of the Submission', 'div' => false, 'label' => false, 'class' => 'span2')); ?>
                <span class="blue" style="display: inline-block; float: right">[ Max Possible Post - <?php echo $tAutoSite; ?> ]</span>
            </div>

        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span9">
            <div class="row-fluid">
                <label class="form-label span4" for="normal">Title of the Post *</label>
                <?php echo $this->Form->input('Submission.posttitle', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Title of the Post', 'div' => false, 'label' => false, 'class' => 'span8')); ?>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Enter your Article below</h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('Submission.postcontent', array('error' => false, 'required' => false, 'title' => 'Article Content', 'div' => false, 'label' => false, 'class' => 'span6 tinymce', 'style' => 'width:99%;height:450px')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle" type="submit">Add New Submission</button>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

<script type="text/javascript">
    //------------- Datepicker -------------//
    if($('.datepicker').length) {
        $('.datepicker').datepicker({
            showOtherMonths:true
            //            dateFormat: 'yy-m-d'
        });
    }
    if($('.datepicker-inline').length) {
        $('.datepicker-inline').datepicker({
            inline: true,
            showOtherMonths:true
        });
    }
</script>

<!--div class="form-row row-fluid">
    <div class="span6">
        <div class="row-fluid">
            <label class="form-label span6" for="normal">How quick it will post *</label>
            <div class="span4">
<?php
$options = array(
    '1' => '01 Day',
    '3' => '03 Days',
    '7' => '01 Week',
    '21' => '03 Weeks',
    '30' => '01 Month'
);
//echo $this->Form->select('Submission.intervals', $options, array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Name Of the Submission', 'div' => false, 'label' => false, 'class' => 'span6', 'empty' => false));
?>
            </div>
        </div>
    </div>
</div-->