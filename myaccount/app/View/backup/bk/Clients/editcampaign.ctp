<div class="submenu_tab clearfix">
    <ul>
        <li><a href="<?php echo Router::url('viewcampaign/'.$data['Campaign']['id']); ?>">View Campaign</a></li>
    </ul>
</div>

<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Edit A Campaign Data </h4>
    <p>edit your campaign information below. You must have to enter campaign name and root domain name</p>
</div>
<div class="content">
    <?php
        $controller = $this->params['controller'];
        $action = $this->params['action'];
        echo $this->Form->create(null, array('url' => array('controller' => $controller, 'action' => $action, $data['Campaign']['id']), 'id' => 'editCampaignForm', 'method' => 'post'));
    ?>
    <div class="form-row row-fluid">
        <div class="spa6">
            <div class="row-fluid">
                <label class="form-label span3" for="normal">Name Of the Campaign *</label>
                <?php echo $this->Form->input('Campaign.campignname', array('type' => 'text', 'required' => false, 'title' => 'Name Of the Campaign', 'div' => false, 'label' => false, 'class' => 'span3')); ?>
                <input type="hidden" name="data[Campaign][id]" value="<?php echo $data['Campaign']['id']; ?>" >
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div id="settingContainer">
        <table cellpadding="4" cellspacing="6">
            <thead>
                <tr>
                    <th class="span1" style="width: 105px;color:#31BE26;">
                        Target (<span class="yellow bold" id="">100</span>) %
                    </th>
                    <th>Anchor Text <span class="help-block-inline">comma(,) seperated</span></th>
                    <th>Internal Url <span class="help-block-inline">comma(,) seperated and sequential to left anchor text</span></th>
                    <th>No Follow <span class="help-block-inline">link</span> </th>
                </tr>
            </thead>
            <tbody id="campInputRows">
                <?php
                    $allIds = array();
                    if (count($data['Anchortext']) > 0) {
                        $i = 1;
                        foreach ($data['Anchortext'] as $k => $anchorText) {
                            array_push($allIds, $anchorText['id']);
                        ?>

                        <tr id="<?php echo $i; ?>" class="sControl">

                            <td valign="top" align="center" class="span1">
                                <div class="adminControl" style="display: inline; visibility: hidden;">
                                    <a href="#" title="Remove this Row?" rel="<?php echo $anchorText['id']; ?>" class="tip deleteRow">
                                        <span class="icon12 icomoon-icon-remove"></span>
                                    </a>
                                </div>

                                <?php echo $this->Form->input('Campaign.settings.' . $i . '.targetDensity', array('type' => 'number', 'required' => false, 'title' => 'Percentage value. max 100', 'div' => false, 'label' => false, 'class' => 'span1 Percentage', 'value' => $anchorText['targetDensity'], 'maxlength' => 3, 'style' => 'width:32px !important;')); ?>
                            </td>
                            <td valign="top">
                                <?php echo $this->Form->textarea('Campaign.settings.' . $i . '.anchortexts', array('type' => 'text', 'title' => 'enter anchortexts', 'div' => false, 'label' => false, 'class' => 'span4 anchorText', 'value' => $anchorText['anchortexts'], 'style' => 'height:22px; min-height:22px')); ?>
                            </td>
                            <td valign="top">
                                <?php echo $this->Form->textarea('Campaign.settings.' . $i . '.internalPageUrl', array('type' => 'text', 'title' => 'enter urls  for anchors', 'div' => false, 'label' => false, 'class' => 'span5 anchorUrls', 'value' => $anchorText['internalPageUrl'], 'style' => 'height:22px; min-height:22px')); ?>
                            </td>
                            <td valign="top">
                                <?php echo $this->Form->checkbox('Campaign.settings.' . $i . '.noFollow', array('title' => 'check for nofollow', 'div' => false, 'label' => 'Nofollow', 'class' => 'span1 nostyle', 'style' => '', 'checked' => $anchorText['nofollow'], 'value' => $anchorText['nofollow'])); ?>
                            </td>
                        </tr>
                        <?php
                            $i++;
                        }
                    } else {
                    ?>
                    <tr id="1" class="sControl">
                        <td valign="top" align="center" class="span1">
                            <div class="adminControl" style="display: inline; visibility: hidden;">
                                <a href="#" title="Remove this Row?"  class="tip deleteRow">
                                    <span class="icon12 icomoon-icon-remove"></span>
                                </a>
                            </div>
                            <?php echo $this->Form->input('Campaign.settings.1.targetDensity', array('type' => 'number', 'required' => false, 'title' => 'Percentage value. max 100', 'div' => false, 'label' => false, 'class' => 'span1 Percentage', 'maxlength' => 3, 'style' => 'width:32px !important;')); ?>
                        </td>
                        <td valign="top">
                            <?php echo $this->Form->textarea('Campaign.settings.1.anchortexts', array('type' => 'text', 'title' => 'enter anchortexts', 'div' => false, 'label' => false, 'class' => 'span4 anchorText', 'style' => 'height:22px; min-height:22px')); ?>
                        </td>
                        <td valign="top">
                            <?php echo $this->Form->textarea('Campaign.settings.1.internalPageUrl', array('type' => 'text', 'title' => 'enter urls  for anchors', 'div' => false, 'label' => false, 'class' => 'span5 anchorUrls', 'style' => 'height:22px; min-height:22px')); ?>
                        </td>
                        <td valign="top">
                            <?php echo $this->Form->checkbox('Campaign.settings.1.noFollow', array('title' => 'check for nofollow', 'div' => false, 'label' => 'Nofollow', 'class' => 'span1 nostyle', 'style' => '')); ?>
                        </td>
                    </tr>
                    <?php
                    }
                ?>
            </tbody>
        </table>
        <table>
            <tr>

                <td class="span1" style="width: 105px;color:#31BE26;">

                    <input type="text" name="data[Campaign][tPercentage]" value="100" id="totalPercentageAdded" style="width:1px;height:1px;visibility:hidden;">
                    Total (<span class="yellow bold" id="totalPercentageAddedShow">100</span>) %
                </td>
                <td colspan="3"></td>
            </tr>
        </table>
    </div>
    <div class="form-row row-fluid">
        <div class="spa6">
            <div class="row-fluid">
                <label class="form-label span3" for="normal">Update Instantly?
                    <?php echo $this->Form->checkbox('Custom.instantly', array('title' => 'Check to update instantly', 'required' => false, 'div' => false, 'label' => false, 'class' => '')); ?>
                </label>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="spa6">
            <div class="row-fluid">
                <button class="btn btn-link btn-large" type="button" id="addMoreBtn">+ Add More</button>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle" type="button" onclick="submitApplyForm(this);">Save Campaign</button>
    </div>
    <input type="hidden" name="data[Campaign][allIds]" value="<?php echo implode(",", $allIds); ?>" >
    <?php echo $this->Form->end(); ?>
</div>


<div class="marginB10"></div>

<script type="text/javascript">
    // document ready function
    $(document).ready(function() {
        $("#addMoreBtn").live('click', function() {
            $rows = $("#campInputRows");
            $rowsId = parseInt($rows.find('tr:last').attr('id'));
            $rows.find('tr:last').clone().insertAfter($rows.find('tr:last'));
            $justInserted = $rows.find('tr:last');
            $justInserted.hide();
            $nId = $rowsId + 1;
            $justInserted.attr('id', $nId);
            $justInserted.addClass('sControl');
            $justInserted.find('input,textarea').each(function(i, e) {
                $(e).attr('id', $(e).attr('id') + $nId);
                $(e).attr('name', $(e).attr('name').replace('[' + $rowsId + ']', '[' + $nId + ']'));
                // make it required. Tell validation plugin to validate it
                if ($(e).is(":checkbox"))
                    $(e).rules('add', {'required': false});
                else
                    $(e).rules('add', {'required': true});
            });
            $justInserted.find('input,textarea').val(''); // it may copy values from first one
            $justInserted.find('input:checkbox').val(1); // for checkbox
            $justInserted.find('input:checkbox').siblings('input:hidden').val(0); // for checkbox hidden
            $justInserted.slideDown(500);
        });



        $(".sControl").live('mouseover', function(e) {
            $(this).find("div.adminControl").css({display: 'inline', visibility: 'visible'});
        });
        $(".sControl").live('mouseout', function(e) {
            $(this).find("div.adminControl").css({visibility: 'hidden'});
        });

        $(".deleteRow").live('click', function(e) {
            e.preventDefault();
            if (!confirm("This action will remove anchor text, internal url from this row. \n Confirm to delete?")) {
                return false;
            }
            $(this).parents("tr.sControl").eq(0).remove();
            // adjust percentage again
            eVal = 0;
            $(".Percentage").each(function(im, em) {
                $(this).val(parseInt($(this).val()) || 0);
                eVal += parseInt($(this).val()) || 0;
            });
            if (eVal == 100) {
                $('#totalPercentageAddedShow').parent().css({color: '#31BE26'});
            } else {
                $('#totalPercentageAddedShow').parent().css({color: '#f34'});
            }
            $("#totalPercentageAdded").val(eVal);
            $("#totalPercentageAddedShow").html(eVal);

            /*if ( $type == '_blank' ) {
            $(this).parents("tr.sControl").eq(0).remove();
            }else{
            if ( ! confirm("This action will remove anchor text, internal url from this row. \n Confirm to delete?") ) {return false;}
            $.ajax({
            type: 'get',
            url: '/seonitrov2/sadmins/delAnchorText/'+$type,
            //url: '/sadmins/delAnchorText/'+$type,
            beforeSend: function() {
            $("html").addClass('loadstate');
            }
            }).done(function(ret) {
            $("html").removeClass('loadstate');
            $(_this).parents("tr.sControl").eq(0).remove();
            // adjust percentage again
            eVal = 0;
            $(".Percentage").each(function(im,em){
            $(this).val( parseInt($(this).val()) || 0 );
            eVal += parseInt($(this).val()) || 0;
            $("#totalPercentageAdded").val(eVal);
            $("#totalPercentageAddedShow").html(eVal);
            });
            })
            }*/
        });


        $(".Percentage").live('keyup', function(e) {
            eVal = 0;
            _this = $(this);
            $(".Percentage").each(function(im, em) {
                $(this).val(parseInt($(this).val()) || 0);
                eVal += parseInt($(this).val()) || 0;
                l = $(this).val().length;
                if ((eVal > 100)) {
                    $vv = $(this).val();
                    $val = $(this).val().substring(0, l - 1);
                    $(this).val($val);
                    eVal = eVal - $vv + (parseInt($val) || 0);
                    $('#totalPercentageAddedShow').parent().delay(100).fadeTo(100, 0.5).delay(100).fadeTo(100, 1).delay(100).fadeTo(100, 0.5).delay(100).fadeTo(100, 1).delay(100).fadeTo(100, 0.5).delay(100).fadeTo(100, 1);
                }
            });
            if (eVal == 100) {
                $('#totalPercentageAddedShow').parent().css({color: '#31BE26'});
            } else {
                $('#totalPercentageAddedShow').parent().css({color: '#f34'});
            }
            //alert(eVal)
            $("#totalPercentageAdded").val(eVal);
            $("#totalPercentageAddedShow").html(eVal);

        });



        $("#editCampaignForm").validate({
            rules: {
                'data[Campaign][campignname]': {
                    required: true,
                    minlength: 4
                },
                'data[Campaign][tPercentage]': {
                    required: true,
                    min: 100
                }

            },
            messages: {
                'data[Campaign][campignname]': {
                    required: "Please enter a Campaign Name",
                    minlength: "Enter Minimum Length"
                },
                'data[Campaign][tPercentage]': {
                    min: "must equal to 100"
                }
                /*'data[Campaign][settings][1][anchortexts]': {
                atLeastOneAnchorText: "Please insert at least one anchor text with minimum 4 chars long"
                }*/
            }

        });

        $(".Percentage,.anchorText,.anchorUrls").each(function(item) {
            $(this).rules("add", {
                required: true
            });
        });

    });
    function submitApplyForm(evt) {
        $("html").addClass('loadstate');
        $('#editCampaignForm').submit();
    }

</script>
