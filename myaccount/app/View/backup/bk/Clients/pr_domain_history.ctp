<!----------------- Graph Start for page rank and seomoz rank ---------------------->

<div style="display: none; width: 80%; margin-left: -40%;" class="modal hide fade" id="graphModal-Google" aria-hidden="true">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button"><span class="icon12 minia-icon-close"></span></button>
        <h3>Graphical History of Page Rank & Seomoz Rank </h3>
    </div>
    <div class="modal-body">
        <div class="paddingT15 paddingB15">
            <div id="simple-chart-trend-Google" class="simple-chart" style="height: 300px; width:100%;"></div>
        </div>
    </div>
    <div class="modal-footer">
    </div>
</div> 

<div style="display: none; width: 80%; margin-left: -40%;" class="modal hide fade" id="graphModal-trust-flow" aria-hidden="true">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button"><span class="icon12 minia-icon-close"></span></button>
        <h3>Graphical History of Citation & Trust Flow</h3>
    </div>
    <div class="modal-body">
        <div class="paddingT15 paddingB15">
            <div id="simple-chart-trend-trust-flow" class="simple-chart" style="height: 300px; width:100%;"></div>
        </div>
    </div>
    <div class="modal-footer">
    </div>
</div> 
<!------- For Csv file start ---------->
<!--<a data-toggle="modal" class="btn btn-success" href="<?php echo Router::url('report_csv'); ?>">Download as CSV</a>-->

<!------- For Csv file end ---------->

<script>

    $(function() {
//graph options
        var options = {
            grid: {
                show: true,
                aboveData: true,
                color: "#3f3f3f",
                labelMargin: 5,
                axisMargin: 0,
                borderWidth: 0,
                borderColor: null,
                minBorderMargin: 5,
                clickable: true,
                hoverable: true,
                autoHighlight: true,
                mouseActiveRadius: 20
            },
            series: {
                grow: {active: false},
                lines: {
                    show: true,
                    fill: false,
                    lineWidth: 4,
                    steps: false
                },
                points: {
                    show: true,
                    radius: 5,
                    symbol: "circle",
                    fill: true,
                    borderColor: "#fff"
                }
            },
            xaxis: {
                mode: "time", timeformat: "%d/%m/%y", minTickSize: [1, "day"]
                        // mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
            },
            legend: {position: "se"},
            colors: chartColours,
            shadowSize: 1,
            tooltip: true, //activate tooltip
            tooltipOpts: {
                content: "%s: %y",
                shifts: {
                    x: -30,
                    y: -50
                }
            }
        };
        var plot = $.plot($("#simple-chart-trend-Google"),
                [
                    {
                        label: "Page Rank",
                        //data: [[1378764000000,0],[1378677600000,0],[1378591200000,0],[1376604000000,0],[1376517600000,0],[1376431200000,0],[1376344800000,0],[1376258400000,0],[1376172000000,0],[1375999200000,0],[1375912800000,0],[1375826400000,0],[1375653600000,0],[1375567200000,0],[1375308000000,0],[1375221600000,0],[1375135200000,0],[1375048800000,0],[1374962400000,0],[1374876000000,0],[1374789600000,0],[1374703200000,0],[1374616800000,0],[1374530400000,0],[1374444000000,0],[1374444000000,0],],
                        //*
                        data: [<?php
foreach (array_reverse($alldomaincorn) as $history):
    $all_history = $history['Pruserdomaincorn'];

    $page_rank = $all_history["page_rank"];
    echo "[" . (strtotime(date("d M Y", strtotime($all_history['updated_at']))) * 1000) . "," . $page_rank . "],";
endforeach;
foreach ($Pruserdomain as $row):
    $arr = $row['Pruserdomain'];

    $page_rank = $arr["page_rank"];
    echo "[" . (strtotime(date("d M Y", strtotime($arr['updated_at']))) * 1000) . "," . $page_rank . "],";
endforeach;
?>],
                        // */                      
                        lines: {fillColor: "#f2f7f9"},
                        points: {fillColor: "#88bbc8"}
                    },
                    {
                        label: "Seomoz Rank",
                        //data: [[1378764000000,77],[1378677600000,78],[1378591200000,85],[1376604000000,95],[1376517600000,96],[1376431200000,96],[1376344800000,97],[1376258400000,97],[1376172000000,108],[1376085600000,108],[1375999200000,99],[1375912800000,100],[1375826400000,92],[1375740000000,90],[1375653600000,94],[1375567200000,92],[1375308000000,93],[1375221600000,91],[1375135200000,91],[1375048800000,93],[1374962400000,96],[1374876000000,86],[1374789600000,96],[1374703200000,97],[1374616800000,87],[1374530400000,104],[1374444000000,93],[1374444000000,93],],
                        //*
                        data: [<?php
foreach (array_reverse($alldomaincorn) as $history):
    $all_history = $history['Pruserdomaincorn'];
    if ($all_history["seomoz_rank"] > 0)
        $seomoz_rank = $all_history["seomoz_rank"];
    else
        $seomoz_rank = 0;
    echo "[" . (strtotime(date("d M Y", strtotime($all_history['updated_at']))) * 1000) . "," . $seomoz_rank . "],";
endforeach;
foreach ($Pruserdomain as $row):
    $arr = $row['Pruserdomain'];
    if ($arr["seomoz_rank"] > 0)
        $seomoz_rank = $arr["seomoz_rank"];
    else
        $seomoz_rank = 0;
    echo "[" . (strtotime(date("d M Y", strtotime($arr['updated_at']))) * 1000) . "," . $seomoz_rank . "],";
endforeach;
?>],
                        // */
                        lines: {fillColor: "#f2f7f9"},
                        points: {fillColor: "#88bbc8"}
                    },
                ], options);
    });</script>

<script>

    $(function() {
//graph options
        var options = {
            grid: {
                show: true,
                aboveData: true,
                color: "#3f3f3f",
                labelMargin: 5,
                axisMargin: 0,
                borderWidth: 0,
                borderColor: null,
                minBorderMargin: 5,
                clickable: true,
                hoverable: true,
                autoHighlight: true,
                mouseActiveRadius: 20
            },
            series: {
                grow: {active: false},
                lines: {
                    show: true,
                    fill: false,
                    lineWidth: 4,
                    steps: false
                },
                points: {
                    show: true,
                    radius: 5,
                    symbol: "circle",
                    fill: true,
                    borderColor: "#fff"
                }
            },
            xaxis: {
                mode: "time", timeformat: "%d/%m/%y", minTickSize: [1, "day"]
                        // mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
            },
            legend: {position: "se"},
            colors: chartColours,
            shadowSize: 1,
            tooltip: true, //activate tooltip
            tooltipOpts: {
                content: "%s: %y",
                shifts: {
                    x: -30,
                    y: -50
                }
            }
        };
        var plot = $.plot($("#simple-chart-trend-trust-flow"),
                [
                    {
                        label: "Citation Flow",
                        //data: [[1378764000000,0],[1378677600000,0],[1378591200000,0],[1376604000000,0],[1376517600000,0],[1376431200000,0],[1376344800000,0],[1376258400000,0],[1376172000000,0],[1375999200000,0],[1375912800000,0],[1375826400000,0],[1375653600000,0],[1375567200000,0],[1375308000000,0],[1375221600000,0],[1375135200000,0],[1375048800000,0],[1374962400000,0],[1374876000000,0],[1374789600000,0],[1374703200000,0],[1374616800000,0],[1374530400000,0],[1374444000000,0],[1374444000000,0],],
                        //*
                        data: [<?php
foreach (array_reverse($alldomaincorn) as $history):
    $all_history = $history['Pruserdomaincorn'];

    if ($all_history["citation_flow"] > 0)
        $citation_flow = $all_history["citation_flow"];
    else
        $citation_flow = 0;

    echo "[" . (strtotime(date("d M Y", strtotime($all_history['updated_at']))) * 1000) . "," . $citation_flow . "],";
endforeach;
foreach ($Pruserdomain as $row):
    $arr = $row['Pruserdomain'];

    if ($arr["citation_flow"] > 0)
        $citation_flow = $arr["citation_flow"];
    else
        $citation_flow = 0;
    echo "[" . (strtotime(date("d M Y", strtotime($arr['updated_at']))) * 1000) . "," . $citation_flow . "],";
endforeach;
?>],
                        // */                      
                        lines: {fillColor: "#f2f7f9"},
                        points: {fillColor: "#88bbc8"}
                    },
                    {
                        label: "Trust Flow",
                        //data: [[1378764000000,77],[1378677600000,78],[1378591200000,85],[1376604000000,95],[1376517600000,96],[1376431200000,96],[1376344800000,97],[1376258400000,97],[1376172000000,108],[1376085600000,108],[1375999200000,99],[1375912800000,100],[1375826400000,92],[1375740000000,90],[1375653600000,94],[1375567200000,92],[1375308000000,93],[1375221600000,91],[1375135200000,91],[1375048800000,93],[1374962400000,96],[1374876000000,86],[1374789600000,96],[1374703200000,97],[1374616800000,87],[1374530400000,104],[1374444000000,93],[1374444000000,93],],
                        //*
                        data: [<?php
foreach (array_reverse($alldomaincorn) as $history):
    $all_history = $history['Pruserdomaincorn'];

    if ($all_history["trust_flow"] > 0)
        $trust_flow = $all_history["trust_flow"];
    else
        $trust_flow = 0;

    echo "[" . (strtotime(date("d M Y", strtotime($all_history['updated_at']))) * 1000) . "," . $trust_flow . "],";
endforeach;
foreach ($Pruserdomain as $row):
    $arr = $row['Pruserdomain'];

    if ($arr["trust_flow"] > 0)
        $trust_flow = $arr["trust_flow"];
    else
        $trust_flow = 0;

    echo "[" . (strtotime(date("d M Y", strtotime($arr['updated_at']))) * 1000) . "," . $trust_flow . "],";
endforeach;
?>],
                        // */
                        lines: {fillColor: "#f2f7f9"},
                        points: {fillColor: "#88bbc8"}
                    },
                ], options);
    });</script>
<!----------------- Graph End ---------------------->

<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <a style="position: absolute;left: 40%; z-index: 20" data-toggle="modal" title="Export all the domains" class="btn btn-link tip btn-success" href="<?php echo Router::url('report_csv'); ?>"><span class="icon16 icomoon-icon-file-excel"></span> Export List </a>
        <a style="position: absolute; left: 50%; z-index: 20" data-toggle="modal" title="Google & Seomoz Rank Graph" class="btn btn-link tip btn-success" href="#graphModal-Google"><span class="icon16 icomoon-icon-stats-up"></span>Google & Seomoz Rank</a>
        <a style="position: absolute; left: 65%; z-index: 20" data-toggle="modal" title="Citation & Trust flow Graph" class="btn btn-link tip btn-success" href="#graphModal-trust-flow"><span class="icon16 icomoon-icon-stats-up"></span>Citation & Trust flow</a>

        <div class="content clearfix">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($Pruserdomain)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th><strong>PR</strong></th>
                        <th><strong>Indexed</strong></th>
                        <th><strong>Created</strong></th>
                        <th><strong>Expires</strong></th>
                        <!--<th><strong>Age</strong></th>-->
                        <th><strong>Name Server</strong></th>
                        <th><strong>Seomoz Rank</strong></th>
                        <th><strong>Citation Flow</strong></th>
                        <th><strong>Trust Flow</strong></th>
                        <th><strong>Ext Back Links</strong></th>
                        <th><strong>Update date</strong></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($Pruserdomain) && count($Pruserdomain) > 0):
                        foreach ($Pruserdomain as $row):
                            $arr = $row['Pruserdomain'];
                            ?>
                            <tr>
                                <td><?php echo $arr['page_rank']; ?></td>
                                <td><?php echo $arr['indexed']; ?></td>
                                <td><?php echo $arr['created']; ?></td>
                                <td><?php echo $arr['expires']; ?></td>
                                <!--<td><?php echo $arr['age']; ?></td>-->
                                <td><?php echo $arr['name_server']; ?></td>
                                <td><?php echo $arr['seomoz_rank']; ?></td>
                                <td><?php echo $arr['citation_flow']; ?></td>
                                <td><?php echo $arr['trust_flow']; ?></td>
                                <td><?php echo $arr['ext_back_links']; ?></td>
                                <td><?php echo date("d M Y", strtotime($arr['updated_at'])); ?></td>

                            </tr>
                            <?php
                        endforeach;

                        if (isset($alldomaincorn) && count($alldomaincorn) > 0):
                            foreach ($alldomaincorn as $history):
                                $all_history = $history['Pruserdomaincorn'];
                                ?>
                                <tr>
                                    <td><?php echo $all_history['page_rank']; ?></td>
                                    <td><?php echo $all_history['indexed']; ?></td>
                                    <td><?php echo $all_history['created']; ?></td>
                                    <td><?php echo $all_history['expires']; ?></td>
                                    <!--<td><?php echo $all_history['age']; ?></td>-->
                                    <td><?php echo $all_history['name_server']; ?></td>
                                    <td><?php echo $all_history['seomoz_rank']; ?></td>
                                    <td><?php echo $all_history['citation_flow']; ?></td>
                                    <td><?php echo $all_history['trust_flow']; ?></td>
                                    <td><?php echo $all_history['ext_back_links']; ?></td>
                                    <td><?php echo date("d M Y", strtotime($all_history['updated_at'])); ?></td>

                                </tr>
                                <?php
                            endforeach;
                        endif;

                    else:
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="4">No record found!</td>
                        </tr>
                    <?php
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>