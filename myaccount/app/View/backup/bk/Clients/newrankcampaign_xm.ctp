<div class="marginB10"></div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'newrankcampaign'), 'id' => 'addRankCampaignForm', 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="campaign_name">Campaign Name:</label>
                        <?php echo $this->Form->input('Rankcampaign.campaign_name', array('error' => false, 'required' => false, 'id' => 'campaign_name', 'type' => 'text', 'title' => 'Campaign Name', 'placeholder' => "Campaign Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        <span class="help-inline blue">Enter a name for your ranking campaign</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="update_period">Update Type:</label>
                        <div class="span4">
                            <label class="radio inline"><input type="radio" checked="checked" value="daily" name="data[Rankcampaign][update_period]">Daily</label>
                            <label class="radio inline"><input type="radio"  value="weekly" name="data[Rankcampaign][update_period]">Weekly</label>                            
                        </div>
                        <span class="help-inline blue">Do you want your keywords to update daily, weekly or monthly?</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="google">Google:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankcampaign.google', array('error' => false, 'required' => false, 'id' => 'google', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>                            
                            <div style="float:left; padding: 0; margin: 0;">
                                <?php echo $this->Form->select('Rankcampaign.glocale', $glocale, array('error' => false, 'required' => false, 'id' => 'glocale', 'div' => false, 'label' => false, 'empty' => false, 'value' => 'en-us', 'class' => '')); ?>
                            </div>
                        </div>
                        <span class="help-inline blue">Google *check the box and select location for google</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="yahoo">Yahoo:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankcampaign.yahoo', array('error' => false, 'required' => false, 'id' => 'yahoo', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>                            
                            <div style="float:left; padding: 0; margin: 0;">
                                <?php echo $this->Form->select('Rankcampaign.ylocale', $glocale, array('error' => false, 'required' => false, 'id' => 'ylocale', 'div' => false, 'label' => false, 'empty' => false,  'value' => 'en-us', 'class' => '')); ?>
                            </div>
                        </div>
                        <span class="help-inline blue">Yahoo *check the box and select location for yahoo</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="bing">Bing:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankcampaign.bing', array('error' => false, 'required' => false, 'id' => 'bing', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>                            
                            <div style="float:left; padding: 0; margin: 0;">
                                <?php echo $this->Form->select('Rankcampaign.blocale', $glocale, array('error' => false, 'required' => false, 'id' => 'blocale', 'div' => false, 'label' => false, 'empty' => false,  'value' => 'en-us', 'class' => '')); ?>
                            </div>
                        </div>
                        <span class="help-inline blue">Bing *check the box and select location for bing</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="amazon">Amazon:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankcampaign.amazon', array('error' => false, 'required' => false, 'id' => 'amazon', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>                            
                        </div>
                        <span class="help-inline blue">Amazon *check the box to rank update from Amazon</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="youtube">Youtube:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankcampaign.youtube', array('error' => false, 'required' => false, 'id' => 'youtube', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>                            
                        </div>
                        <span class="help-inline blue">Youtube *check the box to rank update from Youtube</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">

                        <label class="form-label span3" for="c_keywords">Upload Keywords and urls From CSV file:</label>
                        <div class="span4">
                            <input type="file" name="thefile" id="thefile" /> 
                        </div>
                        <span class="help-inline blue"><a href="<?php echo BASEURL; ?>samplefiles/keywords_with_domain_sample.csv">Download sample CSV file</a></span>
                    </div>

                    <div class="row-fluid">

                        <label class="form-label span3" for="c_keywords">Keywords:</label>
                        <div class="span4">
                            <?php echo $this->Form->textarea('Rankcampaign.c_keywords', array('error' => false, 'required' => false, 'id' => 'c_keywords', 'title' => 'enter keywords you want to track under this campaign', 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                        </div>
                        <span class="help-inline blue">Enter Keywords *one per line. dupicate will be removed auto</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="c_urls">URLs:</label>
                        <div class="span4">
                            <?php echo $this->Form->textarea('Rankcampaign.c_urls', array('error' => false, 'required' => false, 'id' => 'c_urls', 'title' => 'enter urls you want to track under this campaign', 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                        </div>
                        <span class="help-inline blue">Enter URLs *one per line. dupicate will be removed auto</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="email_update">Receive Reports Via Email:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankcampaign.email_update', array('error' => false, 'required' => false, 'id' => 'email_update', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>
                        </div>
                        <span class="help-inline blue">check to receive report via email</span>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="customize_header"> Customizer Header/Footer:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankcampaign.customize_header', array('error' => false, 'required' => false, 'id' => 'customize_header', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div id="customize_div" style="display: none;">  
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="e_email">Email Address:</label>
                            <?php echo $this->Form->input('Rankcampaign.e_email', array('error' => false, 'required' => false, 'id' => 'e_email', 'type' => 'text', 'title' => 'Email Address', 'placeholder' => "Email Address", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        </div>
                        <span class="help-inline blue">Max 5 emails comma separated</span>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="e_company">Company Name:</label>
                            <?php echo $this->Form->input('Rankcampaign.e_company', array('error' => false, 'required' => false, 'id' => 'e_company', 'type' => 'text', 'title' => 'Company Name', 'placeholder' => "Company Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="textarea">Upload your Logo</label>
                            <?php //echo $this->Form->input('Rankcampaign.e_company', array('error' => false, 'required' => false, 'id' => 'e_company', 'type' => 'text', 'title' => 'Company Name', 'placeholder' => "Company Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                            <input type="file" name="data[image][e_logo]" id="logo" title="Upload Logo" /> 

                            <span class="help-inline blue">jpg, jpeg, png and gif file only. [ max dimension 300 x 300 ] </span>

                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="e_contact">Contact Name:</label>
                            <?php echo $this->Form->input('Rankcampaign.e_contact', array('error' => false, 'required' => false, 'id' => 'e_contact', 'type' => 'text', 'title' => 'Contact Name', 'placeholder' => "Contact Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="e_phone">Phone:</label>
                            <?php echo $this->Form->input('Rankcampaign.e_phone', array('error' => false, 'required' => false, 'id' => 'e_phone', 'type' => 'text', 'title' => 'Phone', 'placeholder' => "Phone", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="e_weburl">Web URL:</label>
                            <?php echo $this->Form->input('Rankcampaign.e_weburl', array('error' => false, 'required' => false, 'id' => 'e_weburl', 'type' => 'text', 'title' => 'Web URL', 'placeholder' => "Web URL", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="e_fmsg">Footer Message:</label>
                            <div class="span4">
                                <?php echo $this->Form->textarea('Rankcampaign.e_fmsg', array('error' => false, 'required' => false, 'id' => 'e_fmsg', 'title' => 'Footer Message', 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                            </div>
                        </div>
                    </div>
                </div>            
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span3" for="status">Campaign Status:</label>
                            <div class="left marginT5 marginR10">
                                <input type="radio" name="data[Rankcampaign][status]" id="UserStatus1" value="1" checked="checked" />
                                Active
                            </div>
                            <div class="left marginT5 marginR10">
                                <input type="radio" name="data[Rankcampaign][status]" id="UserStatus0" value="0" />
                                Inactive
                            </div>
                        </div>
                    </div>
                </div>  
            </div>      
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" class="btn btn-info marginR10">Save Campaign</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>

</div>

</div>

<script type="text/javascript">
    $(document).ready(function() {

        $('#thefile').change(function(e) {
            if (e.target.files != undefined) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    var str = e.target.result;
                    strSplit = str.split('\n');
                    keyword = '';
                    url = '';
                    for (i = 0; i < strSplit.length; i++) {
                        if (strSplit[i] != '')
                        {
                            var strjh = strSplit[i] + '';
                            //alert(strjh);
                            str2 = strjh.split(',');
                            //alert(str2[0]);
                            keyword += str2[0] + '\n';
                            url += str2[1] + '\n';
                        }
                    }

                    $('#c_keywords').text(keyword);
                    $('#c_urls').text(url);
                    //$('#ckeywords').text(str_split[0]+str_split.length);
                    // $('#text').text(e.target.result);
                };

                reader.readAsText(e.target.files.item(0));
            }

            return false;
        });


    });


    $(document).ready(function() {

        $('#customize_header').change(function(e) {
            if ($(this).is(":checked")) {
                $('#customize_div').show('slow');
            }
            else
            {
                $('#customize_div').hide('slow');
            }
        });
    });
</script>

