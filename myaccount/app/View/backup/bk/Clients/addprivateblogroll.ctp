
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'addprivateblogroll'), 'id' => 'addBlogrollForm', 'method' => 'post')); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">Choose Network Type:</label>
                <div class="span4">
                    <?php
                    echo $this->Form->select('Blogroll.type', array('1' => 'My Sites', '2' => 'Manual Site'), array('required' => false, 'id' => 'selectNetwork', 'title' => 'Enter Domain network', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid" style="" id="mysitenetwork">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">Choose From Mysite:</label>
                <div class="span4">
                    <?php
                    echo $this->Form->select('Blogroll.domain_id', $mySite, array('required' => false, 'title' => 'Enter Client My Site', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid" style="display: none;" id="manualsitenetwork">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">Choose From Manual:</label>
                <div class="span4">
                    <?php
                    echo $this->Form->select('noname', $manualSite, array('required' => false, 'title' => 'Enter Domain network', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">Select Campaign:</label>
                <div class="span4">
                    <?php
                    echo $this->Form->select('Blogroll.campaign_id', $allCampaign, array('required' => false, 'title' => 'Enter Client My Site', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="status">Choose Option:</label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Blogroll][setting_option]" id="UserStatus1" value="1" checked="checked" />
                    Add Link to Sidebar
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Blogroll][setting_option]" id="UserStatus0" value="2" />
                    Add Link to footer
                </div>
            </div>
        </div>
    </div>
    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Add New Blogroll</button>
                        <button class="btn btn-danger">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

<script type="text/javascript">
    var domIdName = 'data[Blogroll][domain_id]';
    $(document).ready(function() {
        $('#selectNetwork').change(function() {
            if($('#selectNetwork').val() == 1){
                $('#manualsitenetwork').hide();
                $('#manualsitenetwork select').removeAttr('name');
                $('#mysitenetwork').show();
                $('#mysitenetwork select').attr('name',domIdName);
            }
            if($('#selectNetwork').val() == 2){
                $('#mysitenetwork').hide();
                $('#mysitenetwork select').removeAttr('name');
                $('#manualsitenetwork').show();
                $('#manualsitenetwork select').attr('name',domIdName);                
            }
        });

    });
</script>