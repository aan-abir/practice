<div class="marginB10"></div>
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'editdomain', $data['Domain']['id']), 'id' => 'editDomainForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>

    <div class="form-row row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <label class="form-label span4" for="username">CMS:</label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Domain][cms]" id="UserStatus1" value="wordpress" <?php echo $data['Domain']['cms'] == 'wordpress' ? 'checked="checked"' : ''; ?> />
                    Wordpress
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Domain][cms]" id="UserStatus0" value="joomla" <?php echo $data['Domain']['cms'] == 'joomla' ? 'checked="checked"' : ''; ?> />
                    Joomla
                </div>
            </div>
        </div>
    </div>
    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="domain">Domain:</label>
                    <?php echo $this->Form->input('Domain.domain', array('error' => false, 'required' => false, 'id' => 'domain', 'title' => 'Domain Name', 'placeholder' => "Domain Name", 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="username">Username:</label>
                    <?php echo $this->Form->input('Domain.ausername', array('error' => false, 'required' => false, 'id' => 'username', 'title' => 'User Name', 'placeholder' => "User Name", 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="password">Password:</label>
                    <?php echo $this->Form->input('Domain.apassword', array('error' => false, 'required' => false, 'id' => 'password', 'title' => 'Password', 'placeholder' => "Password", 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
            </div>
        </div>        
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="registrar">Registrar:</label>
                    <?php echo $this->Form->input('Domain.registrar', array('error' => false, 'required' => false, 'id' => 'registrar', 'placeholder' => "Registrar", 'title' => 'Registrar', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="rootdomain">Root Domain:</label>
                    <?php echo $this->Form->input('Domain.rootdomain', array('error' => false, 'required' => false, 'id' => 'rootdomain', 'placeholder' => "Root Domain", 'title' => 'Root Domain', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="regnumber">Reg.Number:</label>
                    <?php echo $this->Form->input('Domain.regnumber', array('error' => false, 'required' => false, 'id' => 'regnumber', 'placeholder' => "Reg.Number", 'title' => 'Reg.Number', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
            </div>
        </div>        
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="ftpuser">Ftp Username:</label>
                    <?php echo $this->Form->input('Domain.ftpuser', array('error' => false, 'required' => false, 'id' => 'ftpuser', 'placeholder' => "Ftp Username", 'title' => 'Ftp Username', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>        
                <div class="span4">
                    <label for="ftppassword">Ftp Password:</label>
                    <?php echo $this->Form->input('Domain.ftppassword', array('error' => false, 'required' => false, 'id' => 'ftppassword', 'placeholder' => "Ftp Password", 'title' => 'Ftp Password', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="dbname">MySQL Database Name:</label>
                    <?php echo $this->Form->input('Domain.dbname', array('error' => false, 'required' => false, 'id' => 'dbname', 'placeholder' => "Database Name", 'title' => 'Database Name', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="dbuser">Database Username:</label>
                    <?php echo $this->Form->input('Domain.dbuser', array('error' => false, 'required' => false, 'id' => 'dbuser', 'placeholder' => "Database Username", 'title' => 'Database Username', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="dbpassword">Database Password:</label>
                    <?php echo $this->Form->input('Domain.dbpassword', array('error' => false, 'required' => false, 'id' => 'dbpassword', 'placeholder' => "Database Password", 'title' => 'Database Password', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
            </div>
        </div>        
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="title">Title:</label>
                    <?php echo $this->Form->input('Domain.title', array('error' => false, 'required' => false, 'id' => 'title', 'placeholder' => "Title", 'title' => 'Title', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="markets">Markets:</label>
                    <?php echo $this->Form->input('Domain.markets', array('error' => false, 'required' => false, 'id' => 'markets', 'placeholder' => "Markets", 'title' => 'Markets', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="topic">Topic:</label>
                    <?php echo $this->Form->input('Domain.topic', array('error' => false, 'required' => false, 'id' => 'topic', 'placeholder' => "Topic", 'title' => 'Topic', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
            </div>
        </div>        
    </div>    
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="tagline">Tagline:</label>
                    <?php echo $this->Form->input('Domain.tagline', array('error' => false, 'required' => false, 'id' => 'tagline', 'placeholder' => "Tagline", 'title' => 'Tagline', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>                
                <div class="span4">
                    <label for="age">Age:</label>
                    <?php echo $this->Form->input('Domain.age', array('error' => false, 'required' => false, 'id' => 'age', 'placeholder' => "Age", 'title' => 'Age', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="pr">Page Rank:</label>
                    <div class="span4">
                        <?php
                        echo $this->Form->select('Domain.pr', array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10), array('error' => false, 'id' => 'pr', 'required' => false, 'div' => false, 'class' => 'span2 select', 'empty' => false));
                        ?>
                    </div>
                </div>                
            </div>
        </div>        
    </div> 
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="dns1">DNS 1:</label>
                    <?php echo $this->Form->input('Domain.dns1', array('error' => false, 'required' => false, 'id' => 'dns1', 'placeholder' => "DNS 1", 'title' => 'DNS 1', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="dns2">DNS 2:</label>
                    <?php echo $this->Form->input('Domain.dns2', array('error' => false, 'required' => false, 'id' => 'dns2', 'placeholder' => "DNS 2", 'title' => 'DNS 2', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="ip">IP:</label>
                    <?php echo $this->Form->input('Domain.ip', array('error' => false, 'required' => false, 'id' => 'ip', 'placeholder' => "IP", 'title' => 'IP', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
            </div>
        </div>        
    </div>    
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="cflow">Citation Flow:</label>
                    <?php echo $this->Form->input('Domain.cflow', array('error' => false, 'required' => false, 'id' => 'cflow', 'placeholder' => "Citation Flow", 'title' => 'Citation Flow', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="tflow">Trust Flow:</label>
                    <?php echo $this->Form->input('Domain.tflow', array('error' => false, 'required' => false, 'id' => 'tflow', 'placeholder' => "Trust Flow", 'title' => 'Trust Flow', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="ljpr">Link Juice Pear:</label>
                    <?php echo $this->Form->input('Domain.ljpr', array('error' => false, 'required' => false, 'id' => 'ljpr', 'placeholder' => "Link Juice Pear", 'title' => 'Link Juice Pear', 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
            </div>
        </div>        
    </div>    
    <div class="form-row row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <label class="form-label span4" for="username">Google Indexed?:</label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Domain][is_indexed]" id="UserStatus1" value="1" checked="checked" />
                    Yes
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Domain][is_indexed]" id="UserStatus0" value="0" />
                    No
                </div>
            </div>
        </div>
    </div>    
    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Update Site</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>
