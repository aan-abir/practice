<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Manual Posting Report</span>
                </h4>
            </div>
            <div class="content clearfix">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($itemList)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th class="textLeft">Title</th>
                            <th class="textLeft">Domain</th>
                            <th class="textLeft">Post URL</th>
                            <th>Date</th>
                            <th>Campaign</th>
                            <th>Indexed</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($itemList) && count($itemList) > 0):
                            foreach ($itemList as $item):
                                $arr = $item['Manualpost'];
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td class="textLeft"><?php echo $arr['post_title']; ?></td>
                                    <td class="textLeft"><?php echo $arr['domain']; ?></td>
                                    <td class="textLeft">
                                        <?php
                                        $postUrl = $arr['post_url'];
                                        $postLink = $arr['post_link'];
                                        if (!$arr['post_link']):
                                            $postLink = $arr['post_url'];
                                        endif;
                                        if ($postUrl):
                                            echo $this->Html->link($postLink, $postUrl, array('target' => '_blank'));
                                        endif;
                                        ?>                                        
                                    </td>                                    
                                    <td><?php echo $this->NitroCustom->usDate($arr['post_date']); ?></td>
                                    <td>
                                        <?php
                                        if (count($arr['campaign_name'])):
                                            $postCampaigns = $arr['campaign_name'];
                                            foreach ($postCampaigns as $k => $pC):
                                                ?>
                                                <?php echo $this->Html->link($pC, 'viewcampaign/' . $k); ?>
                                                <?php echo $this->Form->create('Client', array('novalidate' => true)); ?>
                                                <?php echo $this->Form->input('Refresh.campaign_id', array('type' => 'hidden', 'value' => $k)); ?>
                                                <a href="javascript:void(0);" title="Click to Refresh Campaign Links" onclick="submitApplyForm(this);" type="button">[Refresh]</a>
                                                <?php echo $this->Form->end(); ?>                                                
                                                <br/>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </td>
                                    <td>
                                        <?php $isIndexed = $arr['is_indexed'] ? 1 : 0; ?>
                                        <!--<div class="is_indexed_span is_indexed_<?php echo $isIndexed; ?>">&nbsp;&nbsp;</div>-->
                                    </td>
                                    <td>
                                        <?php
                                        if (!$postUrl):
                                            echo $this->Html->link('Edit', 'manualpost/' . $arr['id']);
                                            echo '&nbsp;|&nbsp;';
                                        endif;
                                        echo $this->Html->link('Delete', 'deletemanualpost/' . $arr['id'], array('class' => "tip callAction"));
                                        ?>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="7">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="form-actions">
                <a class="btn btn-success send-middle tip callAction" href="<?php echo Router::url('mplinkindexing'); ?>">
                    <span class="icon16 icomoon-icon-checkmark white"></span>Send New Urls for Indexing?</a>
            </div>              
        </div>
    </div>
</div>

<script type="text/javascript">
    function submitApplyForm(evt) {
        if (confirm('Are you sure to refresh campaign links?')) {
            $("html").addClass('loadstate');
            $(evt).parent('form').submit();
        } else {
            return false;
        }
    }
</script>

<style type="text/css">
    .is_indexed_span{
        background-size: 60%;
        background-repeat: no-repeat;
        display: block;
    }
    .is_indexed_0{
        background-image: url('/images/cross-red.png');
    }
    .is_indexed_1{
        background-image: url('/images/check-green.png');
    }
</style>