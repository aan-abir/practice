<div class="submenu_tab clearfix">
    <ul>
        <li><a href="<?php echo Router::url('editcampaign/' . $campaignInfo['Campaign']['id']); ?>">Edit Campaign</a></li>
    </ul>
</div>

<div class="marginB10"></div>
<?php
if (isset($campaignInfo)):
    ?>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span>Campaign Details: <?php echo $campaignInfo['Campaign']['campignname']; ?> [dcl=<?php echo $campaignInfo['Campaign']['id']; ?>]</span>
                    </h4>
                </div>
                <div class="content clearfix">
                    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($anchorList)) { ?> dynamicTable <?php } ?> display table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th class="zeroWidth"></th>
                                <th class="textLeft">Anchor Text</th>
                                <th class="textLeft">Anchor Link</th>
                                <th>Target % [ceil]</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($anchorList):
                                foreach ($anchorList as $k => $arr):
                                    ?>
                                    <tr>
                                        <td class="zeroWidth"></td>
                                        <td class="textLeft"><?php echo $arr['anchorText']; ?></td>
                                        <td class="textLeft"><?php echo $arr['internalPageUrl']; ?></td>
                                        <td><?php echo ceil($arr['targetDensity']); ?></td>
                                    </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span6 -->
    </div>
    <div class="marginB10"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span>Sitewide</span>
                    </h4>
                </div>
                <div class="content clearfix">
                    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($blogrollFinalResults)) { ?> dynamicTable <?php } ?> display table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>Domain</th>
                                <th>In Sidebar</th>
                                <th>In Footer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($blogrollFinalResults):
                                foreach ($blogrollFinalResults as $arr):
                                    ?>
                                    <tr>
                                        <td><?php echo $arr['Blogroll']['domain']; ?></td>
                                        <td><?php echo $arr['in_sidebar']; ?></td>
                                        <td><?php echo $arr['in_footer']; ?></td>
                                    </tr>
                                    <?php
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="3">No record found!</td>
                                </tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span6 -->
    </div>
    <div class="marginB10"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span>In Content</span>
                    </h4>
                </div>
                <div class="content clearfix">
                    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($campaignResults)) { ?> dynamicTable <?php } ?> display table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>URL</th>
                                <th>In Content</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($campaignResults):
                                foreach ($campaignResults as $arr):
                                    ?>
                                    <tr>
                                        <td><a target="_blank" href="<?php echo $arr['Trackeddomain']['pinger']; ?>"><?php echo $arr['Trackeddomain']['pinger']; ?></a></td>
                                        <td><?php echo $arr['Trackeddomain']['codefound']; ?></td>
                                    </tr>
                                    <?php
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="2">No record found!</td>
                                </tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- End .box -->
        </div><!-- End .span6 -->
    </div>
    <?php
endif;
?>
