<?php
echo $this->Html->script(
        array(
            '/tiny_mce_new/tiny_mce_gzip.js',
            '/js-new/jquery.blockUI.js',
));
?>

<style>
    .synonym_i{width:234px;cursor:pointer;background:#fff;height:25px;border:1px #cdcdcd solid;text-align:left;padding:0 10px}
    .synonym_i a{color:#000;line-height:25px}
    .synonym_a{width:234px;cursor:pointer;background:#7f7f7f;height:25px;border:1px #cdcdcd solid;text-align:left;padding:0 10px}
    .synonym_a a{color:#e5e5e5;line-height:25px}
    .group{color:#000;padding:0 5px}
    #thesaurus-head{width:100%;height:80px;text-align:right;background:url(/imgz/thesaurus-bg.png) no-repeat top left}
    #thesaurus-bottom{width:100%;height:25px;background:url(/imgz/thesaurus-bg.png) repeat-x bottom left}
    #protected-head{width:100%;height:80px;text-align:right;background:url(/imgz/protected-bg.png) no-repeat top left}
    #protected-bottom{width:100%;height:25px;background:url(/imgz/protected-bg.png) repeat-x bottom left}
    #syn-box{width:284px}
    #syn-head{width:100%;height:15px;text-align:right;background:url(/imgz/synonyms-bg.png) no-repeat top left}
    #syn-mid{width:100%;background:url(/imgz/synonyms-mid.png) repeat-y top left}
    #syn-bottom{width:100%;height:25px;background:url(/imgz/synonyms-bg.png) repeat-x bottom left}
    .td1{line-height:20px;height:20px;background:#fafafa;border:1px #fff solid;text-align:left}
    .td0{line-height:20px;height:20px;background:#eaeaea;border:1px #fff solid;text-align:left}
    #textfield-2{width:126px;height:28px;float:left;background-image:url(/imgz/field2.png);background-repeat:no-repeat;background-position:top left;text-align:center;margin:10px 0 0 5px;padding:0}
    #textfield-2 input{border:0;width:118px;height:22px;line-height:22px;font-size:13px;text-align:left;background:transparent;color:#333;margin:2px 0 0}
    #textfield-3{width:250px;height:28px;float:left;background-image:url(/imgz/field3.png);background-repeat:no-repeat;background-position:top left;text-align:center;margin:10px 0 0 5px;padding:0}
    #textfield-3 input{border:0;width:242px;height:22px;line-height:22px;font-size:13px;text-align:left;background:transparent;color:#333;margin:2px 0 0}
    #thesaurus-box,#protected-box{width:554px}
    #thesaurus-mid,#protected-mid{width:100%;background:url(/imgz/thesaurus-mid.png) repeat-y top left}
</style>

<script language="javascript">var original = "";
    var TheWord = "";
    var SelectedSynonyms = new Array;
    var AllSynonyms = new Array;
    var AllFavs = new Array;
    var AllProtected = new Array;
    var IE = document.all ? true : false;
    if (!IE)
        document.captureEvents(Event.MOUSEMOVE);
    document.onmousemove = getMouseXY;
    var tempX = 0;
    var tempY = 0;
    function getMouseXY(e) {
        if (IE) {
            tempX = event.clientX + document.body.scrollLeft;
            tempY = event.clientY + document.body.scrollTop
        } else {
            tempX = e.pageX;
            tempY = e.pageY
        }
        if (tempX < 0)
            tempX = 0;
        if (tempY < 0)
            tempY = 0;
        return true
    }
    function AddRemoveSynonym(syno) {
        var syno2 = syno.replace(/\s+/g, "");
        var mydiv = $("#syn_" + syno2).get(0);
        if (mydiv.className == "synonym_i") {
            SelectedSynonyms.push(syno);
            mydiv.className = "synonym_a"
        } else {
            var temp = new Array;
            for (var i = 0; i < SelectedSynonyms.length; i++)
                if (SelectedSynonyms[i] != syno)
                    temp.push(SelectedSynonyms[i]);
            SelectedSynonyms = temp;
            temp = null;
            mydiv.className = "synonym_i"
        }
    }
    function SynonymsDone() {
        if (SelectedSynonyms.length == 0) {
            alert("Please select at least one word...");
            return
        }
        var sel = tinyMCE.activeEditor.selection.getContent();
        var space = "";
        if (sel[sel.length - 1] == " ")
            space = " ";
        tinyMCE.activeEditor.selection.setContent("{" + SelectedSynonyms.join("|") + "}" + space);
        $("#favs").get(0).style.display = "none";
        var mydiv = $("#synonyms").get(0);
        mydiv.innerHTML = "";
        $("#handlers").get(0).style.display = "none"
    }
    function SynonymsCancel() {
        $("#favs").get(0).style.display = "none";
        var mydiv = $("#synonyms").get(0);
        mydiv.innerHTML = "";
        $("#handlers").get(0).style.display = "none"
    }
    function AddNewSynonym() {
        var mydiv = $("#synonyms").get(0);
        var newguy = $("#newsyn").get(0);
        var newguyname = newguy.value;
        newguyname = newguyname.replace(/\s+/g, "");
        AllSynonyms.push(newguy.value);
        var newdiv = document.createElement("div");
        newdiv.className = "synonym_i";
        newdiv.id = "syn_" + newguyname;
        newdiv.innerHTML = "<a href=\"javascript: AddRemoveSynonym('" + newguy.value + "')\">" + newguy.value + "</a>";
        newguy.value = "";
        mydiv.appendChild(newdiv)
    }
    function SetFavs() {
        var synonyms = SelectedSynonyms.join("|");
        $.post("/post.php", {what: "SetFavs", word: TheWord, words: synonyms}, function(data) {
            if (data == 1)
                if (window.confirm("Done! Replace these in current text?")) {
                    SynonymsDone();
                    var mydiv = $("#synonyms").get(0);
                    mydiv.innerHTML = "";
                    $("#handlers").get(0).style.display = "none";
                    $("#favs").get(0).style.display = "none"
                } else
                    SynonymsCancel();
            else
                alert("Sorry, there was an error")
        })
    }
    function CancelManageFavs() {
        AllFavs = null;
        AllFavs = new Array;
        $("#myfavs").get(0).innerHTML = "";
        $("#myfavs").get(0).style.display = "block";
        $("#managefavsx").get(0).style.display = "none"
    }
    function UpdateManageFavs() {
        var temp = new Array;
        for (var i = 0; i < AllFavs.length; i++)
            temp.push(AllFavs[i].join("|"));
        var MyFavs = temp.join("||");
        $.post("/post.php", {what: "UpdateManageFavs", favs: MyFavs}, function(data) {
            if (data == 1)
                CancelManageFavs();
            else
                alert("Sorry, error")
        })
    }
    function AddNewFavorite() {
        var newstring = "";
        var fav1 = $("#newfav1").get(0);
        var fav2 = $("#newfav2").get(0);
        var temp = new Array;
        var SynVal = "- not set -";
        if (fav1.value != "")
            SynVal = fav1.value;
        temp.push(SynVal);
        SynVal = "- not set -";
        if (fav2.value != "") {
            var f2 = fav2.value.split(",");
            for (var i = 0; i < f2.length; i++)
                temp.push($.trim(f2[i]))
        } else
            temp.push(SynVal);
        AllFavs.push(temp);
        ListFavorites();
        fav1.value = "New Word";
        fav2.value = "Synonyms"
    }
    function ListFavorites() {
        $("#myfavs").get(0).innerHTML = "";
        var TheOtherFavs = new Array;
        for (var i = 0; i < AllFavs.length; i++) {
            TheOtherFavs = null;
            TheOtherFavs = new Array;
            var newdiv = document.createElement("div");
            newdiv.className = "group";
            newdiv.id = "gr_" + i;
            if (i % 2 == 1)
                var theclass = "td1";
            else
                var theclass = "td0";
            if (AllFavs[i].length > 1) {
                for (var j = 1; j < AllFavs[i].length; j++)
                    TheOtherFavs.push(AllFavs[i][j]);
                var FavsString = TheOtherFavs.join(", ")
            } else
                var FavsString = "- not set -";
            var myhtml = "";
            myhtml = '<table width="" cellpadding="0" cellspacing="0"><tr>';
            myhtml += '<td class="' + theclass + '" width="113" valign="middle">&nbsp;&nbsp;' + AllFavs[i][0] + "</td>";
            myhtml += '<td class="' + theclass + '" width="390" valign="middle">&nbsp;&nbsp;' + FavsString + "</td>";
            myhtml += '<td class="' + theclass + '" width="30" align="center" valign="middle"><a href="javascript: RemoveFav(' + i + ');"><img src="/imgz/delete-btn.png" border="0" /></a></td>';
            myhtml += "</tr></table>";
            newdiv.innerHTML = myhtml;
            $("#myfavs").get(0).appendChild(newdiv)
        }
        $("#LoadingMessage").get(0).style.display = "none"
    }
    function CancelProtectedW() {
        AllProtected = null;
        AllProtected = new Array;
        $("#protectedws").get(0).innerHTML = "";
        $("#protectedws").get(0).style.display = "block";
        $("#protectedw").get(0).style.display = "none"
    }
    function RemoveFav(x) {
        if (!window.confirm("Are you sure?"))
            return;
        var temp = new Array;
        for (var i = 0; i < x; i++)
            temp.push(AllFavs[i]);
        for (var i = x + 1; i < AllFavs.length; i++)
            temp.push(AllFavs[i]);
        AllFavs = temp;
        temp = null;
        ListFavorites()
    }
    function RemoveProtected(x) {
        if (!window.confirm("Are you sure?"))
            return;
        var temp = new Array;
        for (var i = 0; i < x; i++)
            temp.push(AllProtected[i]);
        for (var i = x + 1; i < AllProtected.length; i++)
            temp.push(AllProtected[i]);
        AllProtected = temp;
        temp = null;
        ListProtectedWords()
    }
    function UpdateProtectedW() {
        var MyFavs = AllProtected.join("|");
        $.post("/post.php", {what: "UpdateProtectedWords", "protected": MyFavs}, function(data) {
            if (data == 1)
                CancelProtectedW();
            else
                alert("Sorry, error")
        })
    }
    function ListProtectedWords() {
        $("#protectedws").get(0).innerHTML = "";
        for (var i = 0; i < AllProtected.length; i++) {
            var newdiv = document.createElement("div");
            newdiv.className = "group";
            newdiv.id = "gr_" + i;
            if (i % 2 == 1)
                var theclass = "td1";
            else
                var theclass = "td0";
            var myhtml = "";
            myhtml = '<table width="" cellpadding="0" cellspacing="0"><tr>';
            myhtml += '<td class="' + theclass + '" width="505" valign="middle">&nbsp;&nbsp;' + AllProtected[i] + "</td>";
            myhtml += '<td class="' + theclass + '" width="30" align="center" valign="middle"><a href="javascript: RemoveProtected(' +
                    i + ');"><img src="/imgz/delete-btn.png" border="0" /></a></td>';
            myhtml += "</tr></table>";
            newdiv.innerHTML = myhtml;
            $("#protectedws").get(0).appendChild(newdiv)
        }
    }
    function AddNewProtectedW() {
        var newguy = $("#addnewprotectedword").get(0);
        AllProtected.push(newguy.value);
        newguy.value = "Add New Protected Word";
        ListProtectedWords()
    }
    ;
</script>

<div id="managefavsx" style="width: 554px; display: none; z-index: 99;">
    <div id="thesaurus-box">
        <div id="thesaurus-head">
            <a href="javascript: UpdateManageFavs();"><img src="/imgz/save-changes-btn.png" border="0" style="margin-top: 10px;" /></a>
            <a href="javascript: CancelManageFavs();"><img src="/imgz/cancel-btn.png" border="0" style="margin-top: 10px; margin-right: 20px;" /></a>
        </div>
        <div id="thesaurus-mid">                                                                
            <div id="myfavs" style="width: 100%;"></div>


            <div style="width: 538px; margin: 10px 5px 0px 5px; height: 50px; background: url(/imgz/blue-bg.png) top left repeat-x;">
                <div id="textfield-2" style="margin: 10px 0px 0px 5px;">
                    <input type="text" name="newfav1" id="newfav1" value="New Word" onclick="javascript: if (this.value == 'New Word') {
                                this.value = '';
                            }">
                </div>
                <div id="textfield-3" style="margin: 10px 10px 0px 10px;">
                    <input type="text" name="newfav2" id="newfav2" value="Synonyms" onclick="javascript: if (this.value == 'Synonyms') {
                                this.value = '';
                            }">
                </div>
                <a href="javascript: AddNewFavorite();"><img src="/imgz/add-new-favorite-btn.png" border="0" style="margin: 10px 0px 0px 0px;" /></a>

            </div>  

            <div style="width: 530px; margin: 10px 5px 0px 15px;">

                <small>When done with adding new synonyms, click on Save Changes button.</small>
            </div>

        </div>    
        <div id="thesaurus-bottom"></div>
    </div>

</div>

<div id="protectedw" style="width: 554px; display: none; z-index: 99;">
    <div id="protected-box">
        <div id="protected-head">

            <a href="javascript: UpdateProtectedW();"><img src="/imgz/save-changes-btn.png" border="0" style="margin-top: 10px;" /></a>
            <a href="javascript: CancelProtectedW();"><img src="/imgz/cancel-btn.png" border="0" style="margin-top: 10px; margin-right: 20px;" /></a>
        </div>
        <div id="protected-mid">                                                                
            <div id="protectedws" style="width: 100%;"></div>
            <div style="width: 100%">                                                                                                    
                <div style="width: 538px; margin: 10px 5px 0px 5px; height: 50px; background: url(/imgz/blue-bg.png) top left repeat-x;">
                    <div id="textfield-3" style="margin: 10px 0px 0px 10px;">
                        <input type="text" id="addnewprotectedword" value="Add New Protected Word" onclick="javascript: if (this.value == 'Add New Protected Word') {
                                    this.value = '';
                                }">
                    </div>
                    <a href="javascript: AddNewProtectedW()"><img src="/imgz/add-new-word-btn.png" border="0" style="margin: 10px 0px 0px 10px;" /></a>
                </div>  
            </div>
            <div style="width: 530px; margin: 10px 5px 0px 15px;">
                <small>When done with adding words, click on Save Changes button.</small>
            </div>
        </div>
        <div id="protected-bottom"></div>
    </div>
</div>
<div id="favs" style="display: none; z-index: 99;">
    <div id="syn-box">
        <div id="syn-head"></div>
        <div id="syn-mid">
            <div style="width: 264px; margin: 0px 10px;">                                                                
                <div id="synonyms"></div>                                          
            </div>
            <div style="width: 269px; height: 50px; background: url(/imgz/blue-bg.png) top left no-repeat; margin-left: 5px; margin-top: 10px;">
                <div id="textfield-2">

                    <input type="text" id="newsyn">
                </div>
                <a href="javascript: AddNewSynonym();"><img src="/imgz/add-new-synonym-btn.png" style="margin: 10px 0px 0px 7px;" border="0" /></a>
            </div>
            <center>
                <div id="handlers">   
                    <a href="javascript: SetFavs();"><img src="/imgz/save-as-favorites-btn.png" style="margin: 10px 0px 0px 0px;" border="0" /></a> 
                    <br />
                    <a href="javascript: SynonymsDone();"><img src="/imgz/done-btn.png" style="margin: 10px 0px 0px 0px;" border="0" /></a> 
                    <a href="javascript: SynonymsCancel();"><img src="/imgz/cancel2-btn.png" style="margin: 10px 0px 0px 0px;" border="0" /></a> 
                </div>    
            </center>
        </div>
        <div id="syn-bottom"></div>
    </div>    
</div>
<script type="text/javascript">
    tinyMCE_GZ.init({
        plugins: 'directionality,fullscreen,inlinepopups,linkvine,media,nonbreaking,noneditable,paste,spellchecker,visualchars,wordcount,xhtmlxtras',
        themes: 'simple,advanced',
        languages: 'en',
        disk_cache: true,
        debug: false
    });
    tinyMCE.init({
        // General options
        mode: "textareas", editor_deselector: "mceNoEditor", width: "100%", theme: "advanced",
        plugins: "safari,inlinepopups,spellchecker,media,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,wordcount,linkvine",
        // Theme options
        theme_advanced_buttons1: "bold,italic,strikethrough,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyright,|,link,unlink,wp_more,|,spellchecker,fullscreen,|,rewrite1,rewrite2,rewrite3,rewrite4,rewrite5,rewrite6",
        theme_advanced_buttons2: "formatselect,underline,justifyfull,forecolor,|,pastetext,pasteword,removeformat,|,media,charmap,|,outdent,indent,|,undo,redo,|,code,|,",
        theme_advanced_buttons3: "",
        theme_advanced_buttons4: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,
        spellchecker_languages: "+English=en,Danish=da,Dutch=nl,Finnish=fi,French=fr,German=de,Italian=it,Polish=pl,Portuguese=pt,Spanish=es,Swedish=sv",
        // Example content CSS (should be your site CSS)
        //        content_css : "editor.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url: "lists/template_list.js",
        external_link_list_url: "lists/link_list.js",
        external_image_list_url: "lists/image_list.js",
        media_external_list_url: "lists/media_list.js",
        // Style formats
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ],
        // Replace values for the template plugin
        template_replace_values: {
            username: "Some User",
            staffid: "991234"
        },
        extended_valid_elements: "a[class|name|href|target|title|onclick|rel],script[type|src],iframe[src|style|width|height|scrolling|marginwidth|marginheight|frameborder],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],$elements",
    });
</script>

<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span12', 'autofocus'); ?>
<div class="content">
    <?php echo $this->Form->create('Backend', array('inputDefaults' => $inputDefaults, 'novalidate' => true)); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="ArticleCategory">Select Category:</label>
                <div class="span3">
                    <?php echo $this->Form->select('Article.category', $allCategs, array('empty' => false)); ?>
                </div>
            </div>
        </div>
    </div>    
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Title of the Article <span class="help-block-inline">this will be the title for a post.</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Article.title'); ?>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Enter your Article below <span class="help-block-inline">enter your article to post. be careful about spun identifier []. close it correctly</span></h4>
            </div>
            <div class="form-row">
                <center>          
                    <div id="LoadingMessage" style="display: none;">
                        <table>
                            <tr>
                                <td valign="middle" align="center"><?php echo $this->Html->image('/images/ajax-loader.gif', array('border' => 0)); ?></td>
                                <td valign="middle" align="center">Loading, please wait ...</td>
                            </tr>

                        </table>
                    </div>
                </center>                
                <?php echo $this->Form->textarea('Article.body', array('id' => 'theEditor', 'class' => 'span12 tinymce', 'rows' => 15)); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid inline_labels">
                <label class="form-label span3" for="status">Status:</label>
                <div class="span4" style="">                    
                    <?php echo $this->Form->radio('Article.status', array('1' => 'Active', '0' => 'Inactive'), array('legend' => false, 'default' => 1,)); ?>
                </div>
            </div>
        </div>
    </div>     
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info offset2" type="submit">Submit</button>
    </div>
    <?php $this->Form->end(); ?>
</div>