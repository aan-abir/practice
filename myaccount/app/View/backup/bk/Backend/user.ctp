<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span4', 'autofocus'); ?>
<div class="content">
    <div class="form-actions">
        <span class="title_text offset2"><?php echo $title_for_layout; ?></span>
    </div>
    <?php echo $this->Form->create('Backend', array('inputDefaults' => $inputDefaults, 'novalidate' => true)); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="RegisterRole">User Role:</label>
                <div class="span2">
                    <?php echo $this->Form->select('Register.role', array_map('ucfirst', array_combine($userRoles, $userRoles)), array('empty' => false, 'default' => 'admin')); ?>
                </div>
            </div>
        </div>
    </div>    
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="RegisterUsername">Username:</label>
                <div class="span8">
                    <?php echo $this->Form->input('Register.username'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="RegisterUserPassword">Password:</label>
                <div class="span8">
                    <?php echo $this->Form->input('Register.user_password'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="RegisterFirstname">First Name:</label>
                <div class="span8">
                    <?php echo $this->Form->input('Register.firstname'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="RegisterLastname">Last Name:</label>
                <div class="span8">
                    <?php echo $this->Form->input('Register.lastname'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="RegisterEmail">Email:</label>
                <div class="span8">
                    <?php echo $this->Form->input('Register.email'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="RegisterTask">User Task:</label>
                <div class="span9">
                    <?php echo $this->Form->textarea('Register.task', array('class' => 'span12')); ?>
                </div>
            </div>
        </div>
    </div>     
    <div class="marginB10"></div>
    <div class="form-actions">
        <button class="btn btn-info offset2" type="submit">Submit</button>
    </div>
    <?php $this->Form->end(); ?>
</div>