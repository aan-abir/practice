<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Enter New Blogger Data</h4>
    <p>To create a new blogger simply fill up the form below. </p>

</div>
<div class="content">
    <?php echo $this->Form->create('User', array('url' => array('controller' => 'sadmins', 'action' => 'newblogger'), 'id' => 'addBloggerForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">Username:</label>
                <?php echo $this->Form->input('User.username', array('id' => 'username', 'title' => 'User Name', 'placeholder' => "User Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>

                <!--                <span class="hint"><a href="#" class="red">Check is Available</a></span>-->
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="normal">Password:</label>
                <div class="span4 controls">
                    <?php echo $this->Form->input('User.password', array('type' => 'text', 'id' => 'password', 'title' => 'Enter Blogger Passord', 'placeholder' => "User password", 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="firstname">First name:</label>
                <?php echo $this->Form->input('User.firstname', array('type' => 'text', 'id' => 'firstname', 'title' => 'Blogger First Name', 'placeholder' => "Blogger First Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="name">Last name:</label>
                <?php echo $this->Form->input('User.lastname', array('type' => 'text', 'id' => 'lastname', 'title' => 'Blogger Last Name', 'placeholder' => "Blogger Last Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="email">Email:</label>
                <?php echo $this->Form->input('User.email', array('type' => 'text', 'id' => 'email', 'title' => 'Blogger Email', 'placeholder' => "Blogger Email", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="email">Phone:</label>
                <?php echo $this->Form->input('User.phone', array('type' => 'text', 'id' => 'phone', 'title' => 'Blogger Phone', 'placeholder' => "Blogger Phone", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="skype">Skype:</label>
                <?php echo $this->Form->input('User.skype', array('type' => 'text', 'id' => 'skype', 'title' => 'Blogger Skype', 'placeholder' => "Blogger Skype", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="textarea">More info</label>
                <?php echo $this->Form->textarea('User.moreaboutclient', array('id' => 'moreaboutclient', 'title' => 'Blogger Additional Contact Info', 'div' => false, 'label' => false, 'class' => 'span4 limit elastic', 'rows' => 3, 'cols' => '5')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="status">Blogger Status:</label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[User][status]" id="UserStatus1" value="1" checked="checked" />
                    Active
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[User][status]" id="UserStatus0" value="0" />
                    InActive
                </div>
            </div>
        </div>
    </div>
    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Save Blogger</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>