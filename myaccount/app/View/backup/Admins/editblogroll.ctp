<div class="heading">
    <h3>Edit BlogRoll</h3>
</div><!-- End .heading-->
<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Edit Blogroll links</h4>
    <p>please Edit your blogroll name and links to add and show in your site. </p>

</div>
<div class="content">
    <?php
        $controller = $this->params['controller'];
        $action = $this->params['action'];
        //pr($data);
        echo $this->Form->create(null, array('url' => array('controller' => $controller, 'action' => $action), 'id'=>'editBlogrollForm','method'=>'post'));?>

    <div class="form-row row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <label class="form-label span6" for="blogrollname">Name Of the Blogroll *</label>
                <?php echo $this->Form->input('Blogroll.blogrollname',array('type'=>'text', 'required'=>false,  'title'=>'Name Of the Blogroll', 'div'=>false,'label'=>false,'class'=>'span6' )); ?>
                <input type="hidden" name="data[Blogroll][id]" value="<?php echo $data['Blogroll']['id']; ?>" >

            </div>
        </div>
    </div>

    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div id="settingContainer">
        <table cellpadding="4" cellspacing="6">
            <thead>
                <tr>
                    <th style="width: 25px;">&nbsp;</th>
                    <th>Anchor <span class="help-block-inline">text for url</span></th>
                    <th>Url <span class="help-block-inline"> link for anchor</span></th>
                </tr>
            </thead>
            <tbody id="campInputRows">
                <?php
                    if ( count($data['Blogrolltext']) > 0 ) {
                        $i = 1;
                        $allIds = array();
                        foreach ( $data['Blogrolltext'] as $k => $anchorText ) {
                            array_push($allIds,$anchorText['id']);

                        ?>


                        <tr id="<?php echo $i; ?>" class="sControl">
                            <td valign="top" align="center" class="span1">
                                <span title="Remove this Row" rel="<?php echo $anchorText['id']; ?>" class="icon12 icomoon-icon-remove adminControl"></span>
                            </td>
                            <td valign="top">
                                <?php echo $this->Form->input('Blogroll.settings.'.$i.'.anchor',array('type'=>'text', 'title'=>'enter anchortexts', 'div'=>false,'label'=>false, 'value' => $anchorText['anchor'], 'class'=>'span4 anchorText' )); ?>
                            </td>
                            <td valign="top">
                                <?php echo $this->Form->input('Blogroll.settings.'.$i.'.link',array('type'=>'text','title'=>'enter urls  for anchors', 'div'=>false,'label'=>false, 'value' => $anchorText['link'], 'class'=>'span5 anchorUrls' )); ?>
                            </td>
                        </tr>
                        <?php
                            $i++;
                        }
                    }else{
                    ?>


                    <tr id="1" class="sControl">
                        <td width="30" valign="middle"><span title="Remove this Row" class="icon12 icomoon-icon-remove adminControl"></span></td>
                        <td valign="top">
                            <?php echo $this->Form->input('Blogroll.settings.1.anchor',array('type'=>'text', 'title'=>'enter anchortexts', 'div'=>false,'label'=>false, 'class'=>'span4 anchorText' )); ?>
                        </td>
                        <td valign="top">
                            <?php echo $this->Form->input('Blogroll.settings.1.link',array('type'=>'text','title'=>'enter urls  for anchors', 'div'=>false,'label'=>false, 'class'=>'span5 anchorUrls' )); ?>
                        </td>
                    </tr>


                    <?php } ?>

            </tbody>
        </table>

    </div>
    <div class="form-row row-fluid">
        <div class="spa6">
            <div class="row-fluid">
                <button class="btn btn-link btn-large" type="button" id="addMoreBtn">+ Add More</button>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle" type="submit">Edit Blogroll</button>
    </div>
    <input type="hidden" name="data[Blogrolltext][allIds]" value="<?php echo implode(",",$allIds); ?>" >
    <?php echo $this->Form->end(); ?>
</div>


<div class="marginB10"></div>

<script type="text/javascript">
    // document ready function
    $(document).ready(function() {
        $("#addMoreBtn").live('click',function(){
            $rows = $("#campInputRows");
            $rowsId =  parseInt($rows.find('tr:last').attr('id'));
            $rows.find('tr:last').clone().insertAfter($rows.find('tr:last'));
            $justInserted = $rows.find('tr:last');
            $justInserted.hide();
            $nId =  $rowsId + 1;
            $justInserted.attr('id',$nId);
            $justInserted.find('input,textarea').each(function(i,e){
                $(e).attr('id', $(e).attr('id') + $nId );
                $(e).attr('name',  $(e).attr('name').replace('['+$rowsId+']', '[' + $nId + ']') );
                // make it required. Tell validation plugin to validate it
                $(e).rules('add', {'required': true});
            });

            $justInserted.find('input,textarea').val(''); // it may copy values from first one
            $justInserted.slideDown(500);
        });





        $("input, textarea, select").not('.nostyle').uniform();

        $(".sControl").live('mouseover',function(e){
            $(this).find("span.adminControl").show();
        });
        $(".sControl").live('mouseout',function(e){
            $(this).find("span.adminControl").hide();
        });

        $(".adminControl").live('click',function(e){
            e.preventDefault();
            $type = $(this).attr('rel');
            _this = $(this);
            if ( ! confirm("This action will remove anchor text, internal url from this row. \n Confirm to delete?") ) {return false;}
            $(this).parents("tr.sControl").eq(0).remove();
            // adjust percentage again
            eVal = 0;
            $(".Percentage").each(function(im,em){
                $(this).val( parseInt($(this).val()) || 0 );
                eVal += parseInt($(this).val()) || 0;
                $("#totalPercentageAdded").val(eVal);
                $("#totalPercentageAddedShow").html(eVal);
            });

            /*if ( $type == '_blank' ) {
            $(this).parents("tr.sControl").eq(0).remove();
            }else{
            if ( ! confirm("This action will remove anchor text, internal url from this row. \n Confirm to delete?") ) {return false;}
            $.ajax({
            type: 'get',
            url: '/seonitrov2/sadmins/delAnchorText/'+$type,
            //url: '/sadmins/delAnchorText/'+$type,
            beforeSend: function() {
            $("html").addClass('loadstate');
            }
            }).done(function(ret) {
            $("html").removeClass('loadstate');
            $(_this).parents("tr.sControl").eq(0).remove();
            // adjust percentage again
            eVal = 0;
            $(".Percentage").each(function(im,em){
            $(this).val( parseInt($(this).val()) || 0 );
            eVal += parseInt($(this).val()) || 0;
            $("#totalPercentageAdded").val(eVal);
            $("#totalPercentageAddedShow").html(eVal);
            });
            })
            }*/
        });



        $("#editBlogrollForm").validate({
            rules: {
                'data[Blogroll][blogrollname]': {
                    required: true,
                    minlength: 4
                }
            },
            messages: {
                'data[Blogroll][blogrollname]': {
                    required: "Please enter a Blogroll Name",
                    minlength: "Enter Minimum Length"
                }
            }

        });



    });


</script>