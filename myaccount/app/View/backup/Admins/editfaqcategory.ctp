<div class="marginB10"></div>
<div class="page-header">
    <h4>Edit Category</h4>
    <p>Please enter your required values to add a new category.</p>
</div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create('faqCategory', array('url' => array('controller' => 'sadmins', 'action' => 'editfaqcategory', $data['Faqcategory']['id']), 'id' => 'addPackageForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
            <?php echo $this->form->hidden('Faqcategory.id', array('value' => $data['Faqcategory']['id'])); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="packagename">Category Name:</label>
                        <?php echo $this->Form->input('Faqcategory.category', array('error' => false, 'required' => false, 'type' => 'text', 'id' => 'packagename', 'title' => 'Category Name', 'placeholder' => "Category Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="status">Category Status:</label>
                        <div class="left marginT5 marginR10">
                            <input type="radio" name="data[Faqcategory][status]" id="UserStatus1" value="1" <?php echo $data['Faqcategory']['status'] == 1 ? 'checked="checked"' : ''; ?> />
                            Active
                        </div>
                        <div class="left marginT5 marginR10">
                            <input type="radio" name="data[Faqcategory][status]" id="UserStatus0" value="0" <?php echo $data['Faqcategory']['status'] == 0 ? 'checked="checked"' : ''; ?> />
                            Inactive
                        </div>
                    </div>
                </div>
            </div>
            <div class="marginB10"></div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" class="btn btn-info marginR10">Update Category</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>
</div>


<div class="marginB10"></div>


<div class="row-fluid">
    <div class="span12">

        <div class="box">
            <div class="title">
                <h4>
                    <span>Your Category List</span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if(isset($allCategory) && count($allCategory) > 0){
                            foreach($allCategory as $category){
                                //pr($camp);
                                ?>
                                <tr>
                                    <td><?php echo $category['Faqcategory']['id']; ?></td>
                                    <td><?php echo $category['Faqcategory']['category']; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <a href="<?php echo adminHome . '/editfaqcategory/' . $category['Faqcategory']['id']; ?>" title="Edit Category" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                            <a href="<?php echo adminHome . '/deletefaqcategory/' . $category['Faqcategory']['id']; ?>" title="Remove Category?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        }else{
                            echo '<tr><td colspan="4">No Category Found!</td></tr>';
                        }
                        ?>



                    </tbody>
                </table>
            </div>

        </div><!-- End .box -->

    </div><!-- End .span6 -->
</div>