<div class="marginB10"></div>
<div class="page-header">
    <h4>Add New Package</h4>
    <p>Please enter your required values to add a new package.</p>
</div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'addpackage'), 'id' => 'addPackageForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="packagename">Package Name:</label>
                        <?php echo $this->Form->input('Package.packagename', array('error' => false, 'required' => false, 'type' => 'text', 'id' => 'packagename', 'title' => 'Package Name', 'placeholder' => "Package Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="type">Package Type:</label>
                        <div class="span2">
                            <?php echo $this->Form->select('Package.type', array('0' => 'Free', '1' => 'Paid'), array('required' => false, 'required' => false, 'title' => 'Select Package Type', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false)); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="duration">Package Duration:</label>
                        <?php echo $this->Form->input('Package.duration', array('error' => false, 'required' => false, 'type' => 'text', 'id' => 'duration', 'title' => 'Package Duration', 'placeholder' => "Package Duration", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        <span class="help-inline blue">Enter package duration for free users. Ignore for paid users.</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="product_id">1shoppingcart Product ID:</label>
                        <?php echo $this->Form->input('Package.product_id', array('error' => false, 'required' => false, 'type' => 'text', 'id' => 'product_id', 'title' => 'Product ID', 'placeholder' => "Product ID", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        <span class="help-inline blue">Enter the corresponding product ID created in 1shoppingcart for this package.</span>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="pvalue">Package Price:</label>
                        <?php echo $this->Form->input('Package.pvalue', array('error' => false, 'required' => false, 'type' => 'text', 'id' => 'pvalue', 'title' => 'Package Price', 'placeholder' => "Package Price", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        <span class="help-inline blue">package value a user will pay</span>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="additional_credit">Additional Credit:</label>
                        <?php echo $this->Form->input('Package.additional_credit', array('error' => false, 'required' => false, 'type' => 'text', 'id' => 'additional_credit', 'title' => 'Additional Credit', 'placeholder' => "Additional Credit", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        <span class="help-inline blue">additional credit for clients</span>
                    </div>
                </div>
            </div>

            <div class="marginB10"></div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="span6">
                            <h3>Service Prices</h3>
                            <!--<span>Enter -1 for unlimited.</span>-->
                        </label>
                    </div>
                </div>
            </div>
            <?php
            if (isset($serviceList) && $serviceList):
                foreach ($serviceList as $k => $service):
                    $serviceName = $service['Service']['servicename'];
                    $serviceId = $service['Service']['id'];
                    $defaultUnitPrice = $service['Service']['default_unit_price'];
                    ?>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="packagename"><?php echo $serviceName; ?>:</label>
                                <div class="span1" style="margin-right: 20px;">
                                    <?php echo $this->Form->input('Serviceprice.unitprice.' . $serviceId, array('error' => false, 'required' => false, 'type' => 'text', 'title' => 'Unit Price', 'placeholder' => "Unit Price", 'div' => false, 'label' => false, 'value' => $defaultUnitPrice, 'readonly' => 'readonly')); ?>
                                </div>
                                <div class="span1" style="margin-right: 20px;">
                                    <?php echo $this->Form->input('Serviceprice.servicelimit.' . $serviceId, array('error' => false, 'required' => false, 'type' => 'text', 'title' => 'Service Limit', 'placeholder' => "Quantity", 'div' => false, 'label' => false)); ?>
                                </div>                                    
                                <div class="span1" style="margin-right: 20px;">
                                    <?php //echo $this->Form->checkbox('Serviceprice.status.' . $serviceId, array('title' => 'Check to exclude from package', 'required' => false, 'div' => false, 'label' => false, 'class' => 'span1', 'checked' => 'checked')); ?>
                                </div>                                    
                            </div>
                        </div>
                    </div>
                    <?php
                endforeach;
            endif;
            ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="textarea">Package Description:</label>
                        <?php echo $this->Form->textarea('Package.description', array('error' => false, 'required' => false, 'id' => 'description', 'title' => 'Package Description', 'div' => false, 'label' => false, 'class' => 'span4 limit elastic', 'rows' => 3, 'cols' => '5')); ?>
                    </div>
                </div>
            </div>                
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="status">Package Status:</label>
                        <div class="left marginT5 marginR10">
                            <input type="radio" name="data[Package][status]" id="UserStatus1" value="1" checked="checked" />
                            Active
                        </div>
                        <div class="left marginT5 marginR10">
                            <input type="radio" name="data[Package][status]" id="UserStatus0" value="0" />
                            Inactive
                        </div>
                    </div>
                </div>
            </div>                
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" class="btn btn-info marginR10">Save Package</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>
</div>