<div class="marginB10"></div>
<div class="page-header">
    <h4>All Unassigned/Free MySite</h4> 
    <p>Select Client from dropdown and assigned checked domains from the list bellow to that Client.</p>

</div>
<div class="row-fluid">
    <div class="span12">
        <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'freemysites'), 'method' => 'post')); ?>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="username">Assign To:</label>
                    <div class="span4" style="margin-right: 20px;">
                        <?php
                        echo $this->Form->select('Domain.user_id', array($users), array('error' => false, 'required' => false, 'id' => 'owner', 'div' => false, 'class' => 'span2 select', 'empty' => false));
                        ?>
                    </div>
                    <div class="span4">
                        <button class="btn btn-info send-middle" type="submit">Assign Selected Domains</button>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="clearfix" style="margin-bottom: 30px;"></div>


        <div class="content">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allDomains)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th style="width: 20px;"></th>
                        <th style="text-align: left;">Domain</th>
                        <th>PR</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($allDomains) > 0):
                        foreach ($allDomains as $arr):
                            $domain = 'http://' . str_replace('http://', '', $arr['Domain']['domain']);
                            ?>
                            <tr>
                                <td><input type="checkbox" name="data[Checked][<?php echo $arr['Domain']['id']; ?>]" /></td>
                                <td style="text-align: left;"><a href="<?php echo $domain; ?>" target="_blank" title="Go to Domain" class="tip"><?php echo $arr['Domain']['domain']; ?></a></td>
                                <td><?php echo $arr['Domain']['pr']; ?></td>
                                <td><?php echo $arr['Domain']['status'] == 1 ? 'Active' : 'Inactive'; ?></td>
                                <td>
                                    <div class="controls center">
                                        <a href="<?php echo adminHome . '/editdomain/' . $arr['Domain']['id']; ?>" title="Edit Domain" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a> | 
                                        <a href="<?php echo adminHome . '/deletedomain/' . $arr['Domain']['id']; ?>" title="Delete Domain?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                        ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><button class="btn btn-info send-middle" type="submit">Assign Selected Domains</button></td>
                        </tr>                                
                        <?php
                    else:
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="3">No Free Domains Found!</td>
                        </tr>
                    <?php
                    endif;
                    ?>
                </tbody>
            </table>

        </div>

        <?php echo $this->Form->end(); ?>
    </div><!-- End .span6 -->
</div>
