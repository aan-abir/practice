<div class="marginB10"></div>
<div class="page-header">
    <h4>All Manual Sites</h4>
    <p>Bellow are all the domains. To take necessary action choose action under actions tab.</p>

</div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Manual Site List</span>
                </h4>
            </div>
            <div class="content">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allDomains)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th title="Domain Name" class="tip">Domain Name</th>
                            <th title="Page Rank" class="tip">PR</th>
                            <th title="Citation Flow" class="tip">CF</th>
                            <th title="Trust Flow" class="tip">TF</th>
                            <th title="Link Juice Page Rank" class="tip">LJPR</th>
                            <th title="SEOMoz Rank" class="tip">SM.R</th>
                            <th title="External Back Links" class="tip">BL</th>
                            <th title="What Type of CMS Using this Domain" class="tip">CMS</th>
                            <th title="Super Admin Username for this domain" class="tip">User Name</th>
                            <th title="Super Admin Password for this domain" class="tip">Password</th>
                            <th title="How Many Users Applied so Far" class="tip">Applied</th>
                            <!--<th title="Necessary Action" class="tip">Actions</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (count($allDomains) > 0):
                                foreach ($allDomains as $arr):
                                    $d = 'http://'. str_replace('http://','',$arr['Domain']['domain']);
                                ?>
                                <tr>
                                    <td><a href="<?php echo $d; ?>" title="View Domain" target="_blank"><?php echo $arr['Domain']['domain']; ?></a></td>
                                    <td><?php echo $arr['Domain']['pr']; ?></td>
                                    <td><?php echo $arr['Domain']['cflow']; ?></td>
                                    <td><?php echo $arr['Domain']['tflow']; ?></td>
                                    <td><?php echo $arr['Domain']['ljpr']; ?></td>
                                    <td><?php echo $arr['Domain']['seomoz_rank']; ?></td>
                                    <td><?php echo $arr['Domain']['ext_backlinks']; ?></td>
                                    <td><?php echo $arr['Domain']['cms']; ?></td>
                                    <td><?php echo $arr['Domain']['ausername']; ?></td>
                                    <td><?php echo $arr['Domain']['apassword']; ?></td>
                                    <td><?php echo 'tbd'; ?></td>
                                    <!--td>
                                        <div class="controls center">

                                            <a href="<?php echo adminHome . '/editdomain/' . $arr['Domain']['id']; ?>" title="Edit Domain" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                            <a href="<?php echo adminHome . '/deletedomain/' . $arr['Domain']['id']; ?>" title="Remove Domain?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                        </div>
                                    </td-->
                                </tr>

                                <?php
                                    endforeach;
                                else:
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="12">No Manual Sites Found!</td>
                            </tr>
                            <?php
                                endif;
                        ?>
                    </tbody>
                </table>

            </div>
        </div><!-- End .box -->
    </div><!-- End .span6 -->
</div>
