<div class="marginB10"></div>
<div class="page-header">
    <h4>Update/Refresh Current HTTP Status</h4>
    <p>press the button to update/refresh current http status for live or problematic domains. <br>
        <span class="bold yellow">Please wait until the operation is done once you start it.</span> <br>


    </p>

</div>
<div class="row-fluid">
    <div class="span12">
        <div class="content span12">
            <input type="hidden" name="data[Update][actiontype]" id="actionType" value="">
            <button class="btn btn-success sendBtn" rel="ok" title="Recheck Status for Good Domains Only!">
                <span class="icon16 icomoon-icon-move white"></span> Recheck Good Domains Only</button>

            <button class="btn btn-warning sendBtn" rel="problematic" title="Update Status for Problematic Domains Only!">
                <span class="icon16 icomoon-icon-move white"></span> Recheck Problematic Domains Only</button>


            <button class="btn btn-primary" id="cancelUpdate" rel="cancelUpdate" title="Stop Processing" style="display: none;">
                <span class="icon16  brocco-icon-stop white"></span> Stop Processing...</button>

            <button class="btn btn-primary" id="reloadPage" rel="reloadPage" title="Reload Page" style="display: none;">
                <span class="icon16 icomoon-icon-loop white"></span> Reload Page</button>
        </div>


    </div><!-- End .span6 -->
</div>

<div class="span12 maarginB10"></div>

<div class="row-fluid marginT10">
    <div class="span12">

        <div class="content" style="display: none;" id="tableContainer">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th title="Domain Name" class="tip">Domain Name</th>
                        <th title="Original Owner" class="tip">O.Owner</th>
                        <th title="Domain Assigned To" class="tip">Assn.To</th>
                        <th title="Current HTTP Status Code" class="tip">Http Code</th>
                        <th title="Current HTTP Status" class="tip">Http Status</th>
                        <th title="Current Working Status." class="tip">Live Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $color = array(
                            '1' => '#ED7A53',
                            '2' => '#0e830e',
                            '3' => '#1baa47',
                            '4' => '#b40b8a',
                            '5' => '#a06508',
                            '6' => '#09aea0',
                        );


                        foreach ($allDomains as $arr):
                            $d = 'http://'. str_replace('http://','',$arr['Domain']['domain']);
                            $clor = $color[$arr['Commonmodel']['id']];
                        ?>
                        <tr style="display: none;" class="<?php echo trim($arr['Domain']['livestatus']); ?>" data-href="<?php echo adminHome.'/refreshstatus/'. $arr['Domain']['id']; ?>">
                            <td><a href="<?php echo $d; ?>" title="View Domain" target="_blank"><?php echo $arr['Domain']['domain']; ?></a></td>
                            <td><?php echo ( $arr['User']['firstname'] != '' ? '<span style="color:#b40b8a;">' .$arr['User']['firstname'] .' '.$arr['User']['lastname'].'</span>' : 'SeoNitro'); ?></td>
                            <td style="color: <?php echo $clor; ?>"><?php echo $arr['Commonmodel']['name']; ?></td>

                            <td class="httpcode"><img src="/images/loaders/3d/067.gif" alt="" /></td>
                            <td class="httpstatus"><img src="/images/loaders/3d/067.gif" alt="" /></td>
                            <td class="livestatus"><img src="/images/loaders/3d/067.gif" alt="" /></td>
                        </tr>

                        <?php
                            endforeach;
                    ?>
                </tbody>
            </table>

        </div>
    </div><!-- End .span6 -->
</div>




<script type="text/javascript">
    var canecelled = 0;
    var oneAjaxCallRunning = 0;

    $("#cancelUpdate").live('click',function(e){
        e.preventDefault();
        if ( !confirm("Are you sure to Stop Processing ") ) {return false;}
        canecelled = 1;
        $(".sendBtn").show(); // show processing buttons
        $(this).hide(); // hide cancel button
    }); 

    $(".sendBtn").live('click',function(e){
        e.preventDefault();
        if ( !confirm("Are you sure to Update/Refresh the status ") ) {return false;}

        if ( oneAjaxCallRunning ) {return false;} // not all ajax call completed yet to block it

        $("#actionType").val($(this).attr('rel'));
        if ( $("#actionType").val() == '' ) {
            return false;
        }
        $atype = $("#actionType").val();

        $felm = $("."+$atype+":first");
        if ( $($felm).length > 0 ) {
            $("#tableContainer").show();
            $(".sendBtn").hide(); // hide button
            $("#cancelUpdate").show();  // show cancell button
            checkLive($felm);
        }

    });

    function checkLive(elm) {
        _this = $(elm); // tr element
        $.ajax({
            type: "get",
            async: true,
            url: _this.attr('data-href'),
            beforeSend: function(){
                $(elm).show() // show working TR
                oneAjaxCallRunning = 1; // current TR  running
            },
            data: false
        }).done(function( status ) {
            oneAjaxCallRunning = 0;
            st = $.parseJSON(status);
            $httpcode = st.httpcode;
            $httpstatus = st.httpstatus;
            $livestatus = st.livestatus;

            _this.find("td.httpcode").html(st.httpcode);
            _this.find("td.httpstatus").html(st.httpstatus);
            _this.find("td.livestatus").html(st.livestatus);

            // if has more row then process next
            $atype = $("#actionType").val(); // okay or problematic sites..selected by button press
            $next = $(_this).closest('tr').nextAll('tr.'+$atype).eq(0);
            if ( $($next).length > 0 ) {
                if ( ! canecelled ) checkLive($next);
            }else{
                //$(".sendBtn").show(); // show processing buttons
                $("#cancelUpdate").hide(); // hide cancel button
                alert("Check Finished. Reload the page before recheck again.");
                $("#reloadPage").show().live('click',function(e){window.location = window.location.href});
                //window.location = window.location.href; // reload the page
            }

        });
    }    
</script>