<div class="marginB10"></div>
<div class="page-header">
    <h4>All Woring Domains</h4>
    <p>list of all working domains. <span style="background-color: yellow;">if you found any domain not working then put it to maintanance mode.</span>

    </p>

</div>
<div class="row-fluid">
    <div class="span12">

    <a style="position: absolute; left: 50%; z-index: 20" title="Export all the Good domains with status" class="btn btn-link tip" target="_blank" href="<?php echo adminHome . '/exportdomainstatus/ok'; ?>"><span class="icon16 icomoon-icon-file-excel"></span> Export List </a>
        <div class="content">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allDomains)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th title="Domain Name" class="tip">Domain Name</th>
                        <th title="Original Owner" class="tip">O.Owner</th>
                        <th title="Domain Assigned To" class="tip">Assn.To</th>
                        <th title="Current HTTP Status Code" class="tip">Http Code</th>
                        <th title="Current HTTP Status" class="tip">Http Status</th>
                        <th title="Current Working Status." class="tip">Live Status</th>
                        
                        <th title="What Type of CMS Using this Domain" class="tip">CMS</th>
                        <th title="Super Admin Username for this domain" class="tip">User</th>
                        <th title="Super Admin Password for this domain" class="tip">Pass</th>
                        <th title="Necessary Action" class="tip">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $color = array(
                            '1' => '#ED7A53',
                            '2' => '#0e830e',
                            '3' => '#1baa47',
                            '4' => '#b40b8a',
                            '5' => '#a06508',
                            '6' => '#09aea0',
                        );

                        if (count($allDomains) > 0):
                            foreach ($allDomains as $arr):
                                $d = 'http://'. str_replace('http://','',$arr['Domain']['domain']);
                                $clor = $color[$arr['Commonmodel']['id']];
                            ?>
                            <tr>
                                <td><a href="<?php echo $d; ?>" title="View Domain" target="_blank"><?php echo $arr['Domain']['domain']; ?></a></td>
                                <td><?php echo ( $arr['User']['firstname'] != '' ? '<span style="color:#b40b8a;">' .$arr['User']['firstname'] .' '.$arr['User']['lastname'].'</span>' : 'SeoNitro'); ?></td>
                                <td style="color: <?php echo $clor; ?>"><?php echo $arr['Commonmodel']['name']; ?></td>
                               
                                <td><?php echo $arr['Domain']['httpcode']; ?></td>
                                <td><?php echo $arr['Domain']['httpstatus']; ?></td>
                                <td><?php echo $arr['Domain']['livestatus']; ?></td>
                               
                                <td><?php echo substr($arr['Domain']['cms'],0,2); ?></td>
                                <td><?php echo $arr['Domain']['ausername']; ?></td>
                                <td><?php echo $arr['Domain']['apassword']; ?></td>
                                <td style="text-align: left;">
                                    <div class="controls left">
                                        <a href="<?php echo adminHome . '/editdomain/' . $arr['Domain']['id']; ?>" title="Edit Domain" class="tip">edit</a>
                                       | <a href="<?php echo adminHome . '/deletedomain/' . $arr['Domain']['id']; ?>" title="Remove Domain for list?" class="tip callAction">delete</a> <br>-----------<br>
                                        
                                        <a href="<?php echo adminHome . '/statuschange/' . $arr['Domain']['id'].'/problematic'; ?>" title="Put it under maintance mode" class="tip callAction">Mark Problemetic</a>
                                    </div>
                                </td>
                            </tr>

                            <?php
                                endforeach;
                            else:
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="11">No Working Domain found!</td>
                        </tr>
                        <?php
                            endif;
                    ?>
                </tbody>
            </table>

        </div>
    </div><!-- End .span6 -->
</div>
