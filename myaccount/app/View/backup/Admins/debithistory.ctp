<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Debit History</span>
                </h4>
            </div>
            <div class="content noPad clearfix">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($debitHistory)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Client Name</th>
                            <th>Amount</th>
                            <th>Remarks</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($debitHistory) && count($debitHistory) > 0):
                            foreach ($debitHistory as $arr):
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?php echo $arr['Credithistory']['clientname']; ?></td>
                                    <td><?php echo $arr['Credithistory']['amount']; ?></td>
                                    <td><?php echo $arr['Credithistory']['remarks']; ?></td>
                                    <td><?php echo $arr['Credithistory']['created']; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <a href="<?php echo adminHome . '/deletecredithistory/' . $arr['Credithistory']['id']; ?>" title="Remove History?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                        </div>                                        
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="5">No records found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- End .span12 -->
</div>
<div id="pagingPmb">
    <?php
    if ($this->Paginator->numbers() != '')
        echo '<em> Pages: </em>';
    if ($this->Paginator->hasPrev())
        echo $this->Paginator->prev(__('Previous '), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers();
    if ($this->Paginator->hasNext())
        echo $this->Paginator->next(__(' Next'), array(), null, array('class' => ' next disabled'));
    ?>
    <span> *<?php echo $this->Paginator->counter('{:count} Total'); ?></span>
</div>