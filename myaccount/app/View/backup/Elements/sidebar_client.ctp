<style type="text/css">
    .grayed {
        color: #ccc !important;
        cursor: text;
    }

    .grayed:hover {
        color: #ddd !important;
        cursor: text;
    }

</style>
<div id="sidebar">
    <div class="sidenav">
        <div class="sidebar-widget" style="margin: -1px 0 0 0;">
            <h5 class="title" style="margin-bottom:0">Navigation</h5>
        </div>
        <!-- End .sidenav-widget -->
        <div class="mainnav">
            <?php
                $cPrefix = Router::getParam('prefix', true);
                if ($cPrefix == 'new'):

                    $myMenus = ClassRegistry::init('Menu')->getEmbededMenus();
                    $myModules = ClassRegistry::init('Module')->find('all', array('order' => 'menu_order DESC'));
                    $myBonuses = ClassRegistry::init('Bonus')->getEmbededBonuses();
                ?>
                <ul id="nav_side">
                    <li class="top"><a href="<?php echo Router::url('/' . $cPrefix . '/clients/dashboard'); ?>">
                        <span class="icon16 icomoon-icon-stats-up"></span>Dashboard</a></li>

                    <?php
                        if (isset($myMenus) && count($myMenus) > 0):
                            foreach ($myMenus as $item):
                                $arr = $item['top'];
                                $icon = trim($arr['icon']) != '' ?  trim($arr['icon']) : 'icomoon-icon-home-4';
                            ?>
                            <li class="top">
                            <a href="#" class="<?php echo $arr['status'] ?: 'grayed'; ?>">
                                <span class="icon16 <?php echo $icon; ?>"></span><?php echo $arr['name']; ?>
                            </a>

                            <?php
                                if (isset($item['sub']) && $item['sub']){
                                    echo ' <ul class="sub">';
                                    foreach ($item['sub'] as $sub):
                                        $icon1 = trim($sub['icon']) != '' ?  trim($sub['icon']) : 'icomoon-icon-home-4';
                                    ?>
                                    <li>
                                        <a href="<?php echo Router::url('/new/clients/view_page/'.$sub['id']); ?>" class="<?php echo $sub['status'] ?: 'grayed'; ?>">
                                            <span class="icon16 <?php echo $icon1; ?>"></span><?php echo $sub['name']; ?>
                                        </a>
                                    </li>
                                    <?php
                                        endforeach;
                                    echo '</ul>';  
                                }
                            ?>

                            </li>
                            <?php
                                endforeach;
                            endif;
                    ?>
                    <?php
                        if ($myModules):
                            foreach ($myModules as $mMods):
                                $mod = $mMods['Module'];
                            ?>
                            <li class="top">
                                <a class="<?php echo $mod['status'] ? '' : 'grayed'; ?>"  href="<?php echo $mod['status'] ? Router::url('/new/clients/modules/' . $mod['id']) : '#' ; ?>"><span
                                    class="icon16 icomoon-icon-clipboard"></span><?php echo $mod['name']; ?></a>
                            </li>
                            <?php
                                endforeach;
                            endif;
                    ?>
                    <?php
                        if (isset($myBonuses) && count($myBonuses) > 0):
                            foreach ($myBonuses as $bonus):
                                $arrB = $bonus['top'];
                            ?>
                            <li class="top">
                            <a href="<?php echo Router::url('/new/clients/view_bonus/'.$arrB['id']); ?>" class="<?php echo $arrB['status'] ?: 'grayed'; ?>"><span class="icon16 entypo-icon-broadcast"></span><?php echo $arrB['name']; ?></a>

                            <?php
                                if (isset($bonus['sub']) && $bonus['sub']):
                                    echo ' <ul class="sub">';
                                    foreach ($bonus['sub'] as $subR):
                                    ?>
                                    <li>
                                        <a href="<?php echo Router::url('/new/clients/view_bonus/'.$subR['id']); ?>" class="<?php echo $subR['status'] ?: 'grayed'; ?>"><span
                                            class="icon16 entypo-icon-plus"></span><?php echo $subR['name']; ?>
                                        </a>
                                    </li>
                                    <?php
                                        endforeach;
                                    echo '</ul>';
                                    endif;
                            ?>

                            </li>
                            <?php
                                endforeach;
                            endif;
                    ?>
                </ul>
                <?php else: ?>
                <ul id="nav_side">
                    <li class="top"><a href="<?php echo Router::url('tools'); ?>"><span
                        class="icon16 icomoon-icon-stats-up"></span>Tools Home</a></li>
                    <li class="top">
                        <a href="#"><span class="icon16 entypo-icon-broadcast"></span>DCL Campaigns</a>
                        <ul class="sub">
                            <li>
                                <a href="<?php echo Router::url('addcampaign'); ?>"><span
                                    class="icon16 entypo-icon-plus"></span>Add New Campaign</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('managecampaign'); ?>"><span
                                    class="icon16 entypo-icon-settings"></span>Manage Campaign</a>
                            </li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#"><span class="icon16 icomoon-icon-clipboard"></span>Rank Reporting</a>
                        <ul class="sub">

                            <li>
                                <a href="#"><span class="icon16 typ-icon-stats"></span>Current Ranking</a>
                                <ul class="sub">
                                    <li><a href="<?php echo Router::url('newrankcampaign'); ?>" class=""><span  class="icon16 entypo-icon-plus"></span>Create Rank Campaign</a></li>
                                    <li> <a href="<?php echo Router::url('rankresults'); ?>" class=""><span class="icon16 typ-icon-stats"></span>Current Ranking</a></li>
                                </ul>
                            </li>


                            <li>
                                <a href="<?php echo Router::url('historicalranking'); ?>"><span
                                    class="icon16 entypo-icon-history"></span>Historical Ranking</a>
                            </li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#"><span class="icon16  icomoon-icon-link"></span>Link Reports</a>
                        <ul class="sub">
                            <li>
                                <a href="<?php echo Router::url('submissionreport'); ?>"><span
                                    class="icon16 eco-article"></span>Our Links</a>
                            </li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#"><span class="icon16   entypo-icon-settings"></span>Tools</a>
                        <ul class="sub">
                            <li>
                                <a href="<?php echo Router::url('pr_campaign_create'); ?>"><span
                                    class="icon16 icomoon-icon-health"></span>Domain Digger</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('ldreport'); ?>"><span
                                    class="icon16 iconic-icon-share"></span>Link Density</a>
                            </li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#"><span class="icon16 entypo-icon-network"></span>Articles</a>
                        <ul class="sub">
                            <li>
                                <a href="<?php echo Router::url('order_article'); ?>"><span
                                    class="icon16 entypo-icon-plus"></span>Order Article</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('article'); ?>"><span
                                    class="icon16 entypo-icon-plus"></span>Add Article</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('articles'); ?>"><span
                                    class="icon16 entypo-icon-add"></span>Article Repository</a>
                            </li>
                        </ul>
                    </li>

                    <!-- <li class="top"><a href="< ?php echo Router::url('resources'); ?>"><span class="icon16 icomoon-icon-books"></span>Education</a></li>-->
                    <li class="top"><a href="<?php echo Router::url('faq'); ?>"><span class="icon16 icomoon-icon-books"></span>FAQ</a></li>
                    <li class="top">
                        <a href="#"><span class="icon16 icomoon-icon-accessibility"></span>Mysites</a>
                        <ul class="sub">
                            <li>
                                <a href="<?php echo Router::url('newdomain'); ?>"><span
                                    class="icon16 entypo-icon-plus"></span>Add Private Site</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('mysites'); ?>"><span
                                    class="icon16 entypo-icon-add"></span>Private Sites</a>
                            </li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#"><span class="icon16 cut-icon-stats-2"></span>PageOneEngine Sites</a>
                        <ul class="sub">
                            <li>
                                <a href="<?php echo Router::url('addsubmission'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Create Auto Posts</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('managesubmission'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Manage Auto Posts</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('addblogroll'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Add Sitewide Links</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('manageblogroll'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Manage Sitewide Links</a>
                            </li>
                        </ul>
                    </li>
                    <li class="top">
                        <a href="#"><span class="icon16 icomoon-icon-rocket"></span>Other Services</a>
                        <ul class="sub">
                            <li>
                                <a href="<?php echo Router::url('bazookablast'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Order Bazooka Blast</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('bazookainprogress'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Bazooka Blast Reports</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('linkemperor'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Order Blast</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('linkemperorinprogress'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Blast Report</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('orderpressrelease'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Order Press Release</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('pressreleases'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Manage Press Release</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('linkindexing'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Link Indexing</a>
                            </li>
                            <li>
                                <a href="<?php echo Router::url('indexingreport'); ?>"><span
                                    class="icon16  icomoon-icon-link"></span>Indexing Report</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <?php endif; ?>
        </div>
    </div>
</div>