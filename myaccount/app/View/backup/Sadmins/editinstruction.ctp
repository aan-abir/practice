<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Edit Tutorial</h4>
    <p>To add/upload videos and text help guide of how a user opeate a specific page enter the details below. <span class="yellow"> Copy the page action and paste here. Example - http://seonitrov2.seonitro.com/clients/addsubmission for this page "addsubmission" is the page action so put "addsubmission" in the text box for 'Enter Page Action'</span></p>

</div>

<div class="content">
    <?php
        if(isset($data) && isset($data['Instruction'])):
        ?>
        <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'editinstruction', $data['Instruction']['id']), 'id' => 'addInstructionForm', 'method' => 'post', 'type'=>'file', 'class' => 'form-horizontal seperator')); ?>
        <?php echo $this->form->hidden('Instruction.id', array('value' => $data['Instruction']['id'])); ?>

        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="packagename">Enter Page Action*</label>
                    <div>
                        <?php echo $this->Form->input('Instruction.pagename', array('error' => false, 'type' => 'text', 'id' => 'pagename', 'title' => 'Page Name', 'placeholder' => "Action Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        <span class="help-inline blue">Enter page action. Read above</span>
                    </div>

                </div>
            </div>


        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="packagename">Title</label>
                    <div>
                        <?php echo $this->Form->input('Instruction.title', array('error' => false, 'type' => 'text', 'id' => 'title', 'title' => 'Title', 'placeholder' => "Title", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        <span class="help-inline blue">Exp: Customer Submission Page</span></div>
                </div>
            </div>
        </div>

        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="packagename">External Video Link</label>
                    <div>
                        <?php echo $this->Form->input('Instruction.externallink', array('error' => false, 'type' => 'text', 'id' => 'youtlink', 'title' => 'External Video Link', 'placeholder' => "External Video Link", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        <span class="help-inline blue">Exp: http://www.youtube.com/watch?v=y_wzGwPWT84</span></div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="textarea">Upload Video</label>
                    <div>
                        <input type="file" name="data[Instruction][video]" id="Video" title="Upload Video" />
                        <input type="checkbox" name="data[Instruction][delOld]" id="delOld" title="Delete Old One" /> Delete Old File
                    </div>
                    <span class="help-inline blue">if added this will play to user as priority. mp4, wmv, flv file only [upload new video will remove old one]</span>
                </div>
            </div>
        </div>


        <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Enter Top Help Text  <span class="help-block-inline"> html enabled help text. Be aware about html tag completion..should not MORE THAN 40/50 WORDS</span></h4>
            </div>
            <div class="form-row">

                <?php echo $this->Form->textarea('Instruction.toptext', array('error' => false, 'id' => 'toptext', 'title' => 'Top help text', 'div' => false, 'label' => false, 'class' => 'span4  tinymce', 'rows' => 3, 'cols' => '5', 'style' => 'width:98%;height:200px;' ) ); ?>
            </div>
        </div>
    </div>



    <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Enter Help Text  <span class="help-block-inline"> html enabled help text. Be aware about html tag completion</span></h4>
            </div>
            <div class="form-row">

                <?php echo $this->Form->textarea('Instruction.helpguide', array('error' => false, 'id' => 'helpguide', 'title' => 'help guide', 'div' => false, 'label' => false, 'class' => 'span4  tinymce', 'rows' => 3, 'cols' => '5', 'style' => 'width:98%;height:300px;' ) ); ?>
            </div>
        </div>
    </div>

    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle" type="submit">Save Tutorial</button>
    </div>

    <?php
        $this->Form->end();
        endif;
?>
</div>


<script type="text/javascript">

    //--------------- Tinymce ------------------//
    $('textarea.tinymce').tinymce({
        // Location of TinyMCE script
        script_url: '<?php echo BASEURL; ?>plugins/forms/tiny_mce/tiny_mce.js',
        // General options
        theme: "advanced",
        plugins: "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
        // Theme options
        theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,styleselect,formatselect,fontselect,fontsizeselect,code,",
        // theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        // theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        // theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,
        // Example content CSS (should be your site CSS)
        content_css: "css/main.css",
        // Drop lists for link/image/media/template dialogs
        template_external_list_url: "lists/template_list.js",
        external_link_list_url: "lists/link_list.js",
        external_image_list_url: "lists/image_list.js",
        media_external_list_url: "lists/media_list.js",
        // Replace values for the template plugin
        template_replace_values: {
            username: "SuprUser",
            staffid: "991234"
        }
    });

</script>