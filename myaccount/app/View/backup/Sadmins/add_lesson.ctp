<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span4', 'autofocus'); ?>
<div class="marginB10"></div>
<div class="page-header">
    <h4>Add New Lesson</h4>

    <p>to add a new lesson please fill up the field below.</p>
</div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create('Lesson', array('inputDefaults' => $inputDefaults, 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>



            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Select Module <span class="help-block-inline">this lesson belongs to</span></h4>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="span4">
                        <?php echo $this->Form->hidden('id'); ?>
                        <?php echo $this->Form->select('module_id', $modules, array('empty' => false)); ?>
                    </div>
                </div>
            </div>



            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Length of the Lesson in Minutes <span class="help-block-inline">exp - 8.15</span></h4>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <?php echo $this->Form->input('lesson_length', array('required' => true, 'type' =>'text', 'class' => 'span4', 'placeholder' => 'Lesson Length')); ?>
                    </div>
                </div>
            </div>


            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Enter Embed Source url <span class="help-block-inline">this will be a web distribution url from amazon to run in jw player </span></h4>
                    </div>
                </div>
            </div>


            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <?php echo $this->Form->input('source', array('required' => true, 'type' => 'text', 'class' => 'span6')); ?>
                    </div>

                </div>
            </div>


            <?php
                if ( isset($data['Lessonfile'])) {
                ?>

                <div class="row-fluid">
                    <div class="span12">
                        <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                            <h4 style="color: #074f14;">Uploaded Files. Check to Delete <span class="help-block-inline">you can delete uploaded files here</span></h4>
                        </div>
                    </div>
                </div>            

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <?php 
                                //pr($data);
                                if ( count($data['Lessonfile']) ) {
                                    foreach ( $data['Lessonfile'] as $k => $v ) {
                                        echo '<input type="checkbox" value="'.$v['filename'].'" name="data[Lesson][lfiles]['.$v['id'].']"> '. $v['filename']. 
                                        ' ( '. $v['ftitle'] .' ) ' . ' <br> ';  
                                    }
                                }

                            ?>
                        </div>

                    </div>
                </div>
                <?php
                }
            ?>


            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Upload New Related Files <span class="help-block-inline">upload related files</span>
                            <a href="#" style="color: #992222; margin-left: 20px;" id="addmore"> add more file <span class="icon24 icomoon-icon-plus"></span></a>
                        </h4>
                    </div>
                </div>
            </div>            


            <div class="form-row row-fluid">
                <div class="span12" id="toadd">
                    <div class="row-fluid" id="titleFiles" style="margin-top: 10px;">
                        <div class="span6">
                            <?php echo $this->Form->input('Lesson.le_files_title.1', array('error' => false, 'type' => 'text', 'id' => 'ftitle_1', 'title' => 'Enter File Title', 'placeholder' => "file title", 'div' => false, 'label' => false, 'class' => 'span11 tip', 'maxlength'=>'200', )); ?>
                        </div>
                        <div class="span6">
                            <?php echo $this->Form->input('Lesson.le_files.1', array('error' => false, 'type' => 'file', 'id' => 'file_1', 'div' => false, 'label' => false,'class' => 'nostyle')); ?>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Lesson Status <span class="help-block-inline">status</span></h4>
                    </div>
                </div>
            </div>              

            <div class="form-row row-fluid">
                <div class="span12 inline_labels">
                    <div class="span7">
                        <?php echo $this->Form->radio('status', array('1' => ' Active', '0' => ' Inactive'), array('legend' => false, 'default' => '1')); ?>
                    </div>
                </div>
            </div>
            <div class="marginB10"></div>


            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="page-header">
                        <h4>Title <span class="help-block-inline"> this will show as topic header</span></h4>
                    </div>
                    <div class="form-row">
                        <?php echo $this->Form->input('title', array('required' => true, 'title' => 'Lesson Title', 'placeholder' => "Lesson Title", 'class' => 'span12 tip')); ?>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header">
                        <h4>Enter Description here <span class="help-block-inline"> html enabled help text. Be aware about html tag completion</span>
                        </h4>
                    </div>
                    <div class="form-row">
                        <?php echo $this->Form->textarea('content', array('required' => false, 'class' => 'span12 tinymce', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:300px;')); ?>
                    </div>
                </div>
            </div>
            <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="submit" class="btn btn-info marginR10">Save Lesson</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <!-- End .span12 -->
    </div>
</div>

<div class="marginB10">
    <?php echo $this->element('tinymce'); ?>
</div>



<!--script type="text/javascript" src="js/forms.js"></script><!-- Init plugins only for page -->

<script type="text/javascript">
    $(document).ready(function() {

        // JQuery UI Modal Dialog
        $('#previewModal').dialog({
            autoOpen: false,
            modal: true,
            autoResize: true,
            dialogClass: 'dialog',
            buttons: {
                "Close": function() {
                    $(this).dialog("close");
                }
            }
        });
        $('#openModalDialog').click(function(){
            $('#previewModal').dialog('open');
            return false;
        });
        $('.box1').click(function(e){
            cls = $(this).children(":first").attr('class');
            $("#selectMenuIcon").val(cls);
            $("#previewModal").dialog("close");
            $("#sicon").removeClass().addClass(cls);
            return false;
        });

        $("#addmore").live('click',function(e){
            // get childs
            $chldc = $("#toadd div").children().length;
            $cnt = $chldc + 1;
            $justInserted = $("#titleFiles").clone();
            $justInserted.attr('id', "dynamic"+$cnt);
            $justInserted.find('input').each(function(i, e) {
                $(e).attr('name', $(e).attr('name').replace('[1]', '[' + ($cnt) + ']'));
                $(e).attr('id', $(e).attr('id').replace('_1', '_' + ($cnt)));
                $(e).val('');
            });
            $("#toadd").append($justInserted);
        });
    });
</script>