<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Add New Events</h4>
    <p>to add a new events and show in the calendar please fill the form below</p>
</div>


<div class="content">
   <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'editevents', $data['Events']['id']), 'id' => 'addInstructionForm', 'method' => 'post', 'type'=>'file', 'class' => 'form-horizontal seperator')); ?>
        <?php echo $this->form->hidden('Events.id', array('value' => $data['Events']['id'])); ?>


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                <h4 style="color: #074f14;">Enter Event Title <span class="help-block-inline">write the title</span></h4>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <?php echo $this->Form->input('Events.title', array('required' => true, 'error' => false, 'type' => 'text', 'id' => 'title', 'title' => 'Title', 'placeholder' => "Title", 'div' => false, 'label' => false, 'class' => 'span12')); ?>
            </div>
        </div>
    </div>



    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                <h4 style="color: #074f14;">Choose Event Start and End Time <span class="help-block-inline">select time</span></h4>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="starttime">Event Start At</label>
                <div>
                    <?php echo $this->Form->input('Events.starttime', array('required' => true, 'error' => false, 'type' => 'text', 'id' => 'starttime', 'title' => 'Event Start Time', 'placeholder' => "start", 'div' => false, 'label' => false, 'class' => 'span4 combined-picker')); ?>
                </div>
            </div>
        </div>
    </div> 

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="endtime">Event Ends At</label>
                <div>
                    <?php echo $this->Form->input('Events.endtime', array('required' => true, 'error' => false, 'type' => 'text', 'id' => 'endtime', 'title' => 'Event End Time', 'placeholder' => "end", 'div' => false, 'label' => false, 'class' => 'span4 combined-picker')); ?>
                </div>
            </div>
        </div>
    </div> 


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                <h4 style="color: #074f14;">Enter text color and url if any <span class="help-block-inline">if you have any url to show</span></h4>
            </div>
        </div>
    </div>   

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="url">Enter Url</label>
                <div>
                    <?php echo $this->Form->input('Events.url', array('error' => false, 'type' => 'text', 'id' => 'youtlink', 'title' => 'External Video Link', 'placeholder' => "url", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    <span class="help-inline blue">Exp: http://www.seonitro.com/event</span></div>
            </div>
        </div>
    </div> 

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="packagename">Enter Hex Color Code</label>
                <div class="span4">
                    <input type="text" id="color" class="span4"  name="data[Events][color]" value="<?php echo $data['Events']['color'] != '' ? $data['Events']['color'] : '#123456' ; ?>" />
                </div>
                <div class="span4"><div class="picker"></div></div>
            </div>
        </div>
    </div> 


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                <h4 style="color: #074f14;">Event Status <span class="help-block-inline">status</span></h4>
            </div>
        </div>
    </div>  
    <div class="form-row row-fluid">
        <div class="span12 inline_labels">
            <div class="span7">
                <?php echo $this->Form->radio('Events.status', array('1' => 'Active', '0' => 'Inactive'), array('legend' => false, 'default' => '1')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10"></div>


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                <h4 style="color: #074f14;">Enter Event Details <span class="help-block-inline">details of the event</span></h4>
            </div>
        </div>
    </div>  

    <div class="row-fluid">
        <div class="span12">
            <div class="form-row">

                <?php echo $this->Form->textarea('Events.description', array('error' => false, 'id' => 'description', 'title' => 'Top help text', 'div' => false, 'label' => false, 'class' => 'span4', 'rows' => 3, 'cols' => '5', 'style' => 'width:98%;height:200px;')); ?>
            </div>
        </div>
    </div>  





    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle" type="submit">Add Event</button>
    </div>

    <?php $this->Form->end(); ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {     
        $('.combined-picker').datetimepicker({
            dateFormat:'yy-mm-dd',
            minDate:0,
            maxDate: '+6m',
            inline:true,
        });

        if($('div').hasClass('picker')){
            $('.picker').farbtastic('#color');
        }    
    });

</script>    
