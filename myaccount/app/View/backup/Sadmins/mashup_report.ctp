<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Rogers' Mashup Report</span>
                </h4>
            </div>
            <div class="content">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allData)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th>Month Year</th>
                            <th>Week</th>
                            <th>Post Count</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($allData) && count($allData) > 0):
                            foreach ($allData as $k => $value):
                                $arr = $value[0];
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td><?php echo date('M Y', strtotime($arr['yr_mnth'])); ?></td>
                                    <!--td>< ?php echo date('M Y', strtotime($arr['yr_mnth'])) . ', ' . $arr['week_no']; ?></td-->
                                    <td><?php echo 'Week '.$arr['week_no']; ?></td>
                                    <td><?php echo $arr['post_count']; ?></td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="3">No Record Found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div>