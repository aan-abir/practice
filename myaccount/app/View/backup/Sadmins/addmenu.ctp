<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span4', 'autofocus'); ?>
<div class="marginB10"></div>
<div class="page-header">
    <h4>Add New Menu</h4>

    <p>to add a new menu please fill up the field below.</p>
</div>




<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create('Menu', array('inputDefaults' => $inputDefaults, 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>
            <?php echo $this->Form->hidden('id'); ?>

            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Select An Icon for this Menu <span class="help-block-inline">an icon before the menu name on left side bar</span></h4>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="span12">
                            <input readonly="readonly" class="span2" type="text" name="data[Menu][icon]" id="selectMenuIcon" value="<?php echo isset($this->data['Menu']['icon']) ? $this->data['Menu']['icon'] : 'icomoon-icon-home-4'; ?>">
                            <span id="sicon" class="<?php echo isset($this->data['Menu']['icon']) ? $this->data['Menu']['icon'] : 'icomoon-icon-home-4'; ?>"></span> 
                            <a href="#" style="display: inline-block;" class="btn marginR10 marginB10" id="openModalDialog">Choose an Icon</a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Enter Embed Source <span class="help-block-inline"> an youbute source  </span></h4>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <?php echo $this->Form->input('source', array( 'type' => 'text', 'class' => 'span6')); ?>
                    </div>

                </div>
            </div>    


            <?php
                if ( isset($this->data['Uploadfile'])) {
                ?>

                <div class="row-fluid">
                    <div class="span12">
                        <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                            <h4 style="color: #074f14;">Uploaded Files. Check to Delete <span class="help-block-inline">you can delete uploaded files here</span></h4>
                        </div>
                    </div>
                </div>            

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <?php 
                                //pr($data);
                                if ( count($this->data['Uploadfile']) ) {
                                    foreach ( $this->data['Uploadfile'] as $k => $v ) {
                                        echo '<input type="checkbox" value="'.$v['filename'].'" name="data[Menu][lfiles]['.$v['id'].']"> '. $v['filename']. 
                                        ' ( '. $v['ftitle'] .' ) ' . ' <br> ';  
                                    }
                                }

                            ?>
                        </div>

                    </div>
                </div>
                <?php
                }
            ?>              



            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Upload New Related Files <span class="help-block-inline">upload related files</span>
                            <a href="#" style="color: #992222; margin-left: 20px;" id="addmore"> add more file <span class="icon24 icomoon-icon-plus"></span></a>
                        </h4>
                    </div>
                </div>
            </div>            


            <div class="form-row row-fluid">
                <div class="span12" id="toadd">
                    <div class="row-fluid" id="titleFiles" style="margin-top: 10px;">
                        <div class="span6">
                            <?php echo $this->Form->input('Menu.le_files_title.1', array('error' => false, 'type' => 'text', 'id' => 'ftitle_1', 'title' => 'Enter File Title', 'placeholder' => "file title", 'div' => false, 'label' => false, 'class' => 'span11 tip', 'maxlength'=>'200', )); ?>
                        </div>
                        <div class="span6">
                            <?php echo $this->Form->input('Menu.le_files.1', array('error' => false, 'type' => 'file', 'id' => 'file_1', 'div' => false, 'label' => false,'class' => 'nostyle')); ?>
                        </div>
                    </div>
                </div>
            </div>                      


            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Select Parent Menu <span class="help-block-inline">if its a Top Menu then you dont need to enter any text below</span></h4>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="span4">
                            <?php echo $this->Form->hidden('id'); ?>
                            <?php echo $this->Form->select('parent_id', $menus, array('empty' => '--Top Text--')); ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Select Name, Order and Status <span class="help-block-inline">enter name, order and status</span></h4>
                    </div>
                </div>
            </div>            

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="MenuName">Menu Name:</label>
                        <?php echo $this->Form->input('name', array('required' => true, 'placeholder' => "Menu Name")); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="MenuMenuOrder">Menu Order:</label>
                        <?php echo $this->Form->input('menu_order', array('class' => 'span1')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12 inline_labels">
                    <label class="form-label span3" for="MenuStatus">Status:</label>

                    <div class="span7">
                        <?php echo $this->Form->radio('status', array('1' => 'Active', '0' => 'Inactive'), array('legend' => false, 'default' => '1')); ?>
                    </div>
                </div>
            </div>
            <div class="marginB10"></div>



            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Title of the Page<span class="help-block-inline"> this will show as topic header</span></h4>
                    </div>
                </div>
            </div>   


            <div class="form-row row-fluid">
                <div class="span12">

                    <div class="form-row">
                        <?php echo $this->Form->input( 'title', array('required' => true, 'title' => 'Menu Title', 'placeholder' => "Menu Title", 'class' => 'span12 tip')); ?>
                    </div>
                </div>
            </div>


            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Enter Description here <span class="help-block-inline"> html enabled help text. Be aware about html tag completion</h4>
                    </div>
                </div>
            </div>  


            <div class="row-fluid">
                <div class="span12">

                    <div class="form-row">
                        <?php echo $this->Form->textarea('content', array('required' => false, 'class' => 'span12 tinymce', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:300px;')); ?>
                    </div>
                </div>
            </div>
            <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="submit" class="btn btn-info marginR10">Save Menu</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <!-- End .span12 -->
    </div>
</div>
<div class="marginB10">
    <?php echo $this->element('tinymce'); ?>
</div>


<div id="previewModal" style="width: 100% !important; height: 100% !important;"  title="Select An Icon" class="dialog">
    <div style="height: 600px; position: relative;">
        <div style="position: absolute;" id="iconContainer">
            <div class="row-fluid">

                <div class="span12">

                    <div class="box">

                        <div class="title">

                            <h4>
                                <span class="icon16 icomoon-icon-IcoMoon"></span>
                                <span>IcoMoon by Keyamoon</span>
                            </h4>

                        </div>
                        <div class="content">

                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-home"></span>
                                &nbsp;icomoon-icon-home
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-home-2"></span>
                                &nbsp;icomoon-icon-home-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-home-3"></span>
                                &nbsp;icomoon-icon-home-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-home-4"></span>
                                &nbsp;icomoon-icon-home-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-home-5"></span>
                                &nbsp;icomoon-icon-home-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-home-6"></span>
                                &nbsp;icomoon-icon-home-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-home-7"></span>
                                &nbsp;icomoon-icon-home-7
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-home-8"></span>
                                &nbsp;icomoon-icon-home-8
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-home-9"></span>
                                &nbsp;icomoon-icon-home-9
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-office"></span>
                                &nbsp;icomoon-icon-office
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-newspaper"></span>
                                &nbsp;icomoon-icon-newspaper
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pencil"></span>
                                &nbsp;icomoon-icon-pencil
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pencil-2"></span>
                                &nbsp;icomoon-icon-pencil-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pencil-3"></span>
                                &nbsp;icomoon-icon-pencil-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pencil-4"></span>
                                &nbsp;icomoon-icon-pencil-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pencil-5"></span>
                                &nbsp;icomoon-icon-pencil-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pen"></span>
                                &nbsp;icomoon-icon-pen
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pen-2"></span>
                                &nbsp;icomoon-icon-pen-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-feather"></span>
                                &nbsp;icomoon-icon-feather
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-brush"></span>
                                &nbsp;icomoon-icon-brush
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-color-palette"></span>
                                &nbsp;icomoon-icon-color-palette
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-eyedropper"></span>
                                &nbsp;icomoon-icon-eyedropper
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-droplet"></span>
                                &nbsp;icomoon-icon-droplet
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-picture"></span>
                                &nbsp;icomoon-icon-picture
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pictures"></span>
                                &nbsp;icomoon-icon-pictures
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-picture-2"></span>
                                &nbsp;icomoon-icon-picture-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-picture-3"></span>
                                &nbsp;icomoon-icon-picture-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-camera"></span>
                                &nbsp;icomoon-icon-camera
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-camera-2"></span>
                                &nbsp;icomoon-icon-camera-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-camera-3"></span>
                                &nbsp;icomoon-icon-camera-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-camera-4"></span>
                                &nbsp;icomoon-icon-camera-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-music"></span>
                                &nbsp;icomoon-icon-music
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-music-2"></span>
                                &nbsp;icomoon-icon-music-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-piano"></span>
                                &nbsp;icomoon-icon-piano
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-guitar"></span>
                                &nbsp;icomoon-icon-guitar
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-headset"></span>
                                &nbsp;icomoon-icon-headset
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-headset-2"></span>
                                &nbsp;icomoon-icon-headset-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-play"></span>
                                &nbsp;icomoon-icon-play
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-play-2"></span>
                                &nbsp;icomoon-icon-play-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-movie"></span>
                                &nbsp;icomoon-icon-movie
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-movie-2"></span>
                                &nbsp;icomoon-icon-movie-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-movie-3"></span>
                                &nbsp;icomoon-icon-movie-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-film"></span>
                                &nbsp;icomoon-icon-film
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-film-2"></span>
                                &nbsp;icomoon-icon-film-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-film-3"></span>
                                &nbsp;icomoon-icon-film-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-camera-5"></span>
                                &nbsp;icomoon-icon-camera-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-camera-6"></span>
                                &nbsp;icomoon-icon-camera-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-camera-7"></span>
                                &nbsp;icomoon-icon-camera-7
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-dice"></span>
                                &nbsp;icomoon-icon-dice
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-gamepad"></span>
                                &nbsp;icomoon-icon-gamepad
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-gamepad-2"></span>
                                &nbsp;icomoon-icon-gamepad-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pacman"></span>
                                &nbsp;icomoon-icon-pacman
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-spades"></span>
                                &nbsp;icomoon-icon-spades
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-clubs"></span>
                                &nbsp;icomoon-icon-clubs
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-diamonds"></span>
                                &nbsp;icomoon-icon-diamonds
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-king"></span>
                                &nbsp;icomoon-icon-king
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-queen"></span>
                                &nbsp;icomoon-icon-queen
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-rock"></span>
                                &nbsp;icomoon-icon-rock
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bishop"></span>
                                &nbsp;icomoon-icon-bishop
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-knight"></span>
                                &nbsp;icomoon-icon-knight
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pawn"></span>
                                &nbsp;icomoon-icon-pawn
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-chess"></span>
                                &nbsp;icomoon-icon-chess
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-announcement"></span>
                                &nbsp;icomoon-icon-announcement
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-announcement-2"></span>
                                &nbsp;icomoon-icon-announcement-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-new"></span>
                                &nbsp;icomoon-icon-new
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-broadcast"></span>
                                &nbsp;icomoon-icon-broadcast
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-broadcast-2"></span>
                                &nbsp;icomoon-icon-broadcast-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-podcast"></span>
                                &nbsp;icomoon-icon-podcast
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-broadcast-3"></span>
                                &nbsp;icomoon-icon-broadcast-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-microphone"></span>
                                &nbsp;icomoon-icon-microphone
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-microphone-2"></span>
                                &nbsp;icomoon-icon-microphone-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-microphone-3"></span>
                                &nbsp;icomoon-icon-microphone-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-book"></span>
                                &nbsp;icomoon-icon-book
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-book-2"></span>
                                &nbsp;icomoon-icon-book-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-books"></span>
                                &nbsp;icomoon-icon-books
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-reading"></span>
                                &nbsp;icomoon-icon-reading
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-library"></span>
                                &nbsp;icomoon-icon-library
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-graduation"></span>
                                &nbsp;icomoon-icon-graduation
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file"></span>
                                &nbsp;icomoon-icon-file
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-2"></span>
                                &nbsp;icomoon-icon-file-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-add"></span>
                                &nbsp;icomoon-icon-file-add
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-remove"></span>
                                &nbsp;icomoon-icon-file-remove
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-download"></span>
                                &nbsp;icomoon-icon-file-download
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-new-2"></span>
                                &nbsp;icomoon-icon-new-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-copy"></span>
                                &nbsp;icomoon-icon-copy
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-copy-2"></span>
                                &nbsp;icomoon-icon-copy-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-stack"></span>
                                &nbsp;icomoon-icon-stack
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-folder"></span>
                                &nbsp;icomoon-icon-folder
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-folder-2"></span>
                                &nbsp;icomoon-icon-folder-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-folder-download"></span>
                                &nbsp;icomoon-icon-folder-download
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-folder-upload"></span>
                                &nbsp;icomoon-icon-folder-upload
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-folder-3"></span>
                                &nbsp;icomoon-icon-folder-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-folder-4"></span>
                                &nbsp;icomoon-icon-folder-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-license"></span>
                                &nbsp;icomoon-icon-license
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tag"></span>
                                &nbsp;icomoon-icon-tag
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tag-2"></span>
                                &nbsp;icomoon-icon-tag-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tag-3"></span>
                                &nbsp;icomoon-icon-tag-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tag-4"></span>
                                &nbsp;icomoon-icon-tag-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-ticket"></span>
                                &nbsp;icomoon-icon-ticket
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cart"></span>
                                &nbsp;icomoon-icon-cart
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cart-2"></span>
                                &nbsp;icomoon-icon-cart-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cart-3"></span>
                                &nbsp;icomoon-icon-cart-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cart-4"></span>
                                &nbsp;icomoon-icon-cart-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cart-add"></span>
                                &nbsp;icomoon-icon-cart-add
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cart-remove"></span>
                                &nbsp;icomoon-icon-cart-remove
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cart-checkout"></span>
                                &nbsp;icomoon-icon-cart-checkout
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-basket"></span>
                                &nbsp;icomoon-icon-basket
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-basket-2"></span>
                                &nbsp;icomoon-icon-basket-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bag"></span>
                                &nbsp;icomoon-icon-bag
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-coin"></span>
                                &nbsp;icomoon-icon-coin
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-coins"></span>
                                &nbsp;icomoon-icon-coins
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-calculate"></span>
                                &nbsp;icomoon-icon-calculate
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-calculate-2"></span>
                                &nbsp;icomoon-icon-calculate-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-support"></span>
                                &nbsp;icomoon-icon-support
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-phone"></span>
                                &nbsp;icomoon-icon-phone
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-phone-2"></span>
                                &nbsp;icomoon-icon-phone-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-address"></span>
                                &nbsp;icomoon-icon-address
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-address-2"></span>
                                &nbsp;icomoon-icon-address-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-notebook"></span>
                                &nbsp;icomoon-icon-notebook
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mail"></span>
                                &nbsp;icomoon-icon-mail
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mail-2"></span>
                                &nbsp;icomoon-icon-mail-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mail-3"></span>
                                &nbsp;icomoon-icon-mail-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mail-4"></span>
                                &nbsp;icomoon-icon-mail-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-location"></span>
                                &nbsp;icomoon-icon-location
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-location-2"></span>
                                &nbsp;icomoon-icon-location-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-location-3"></span>
                                &nbsp;icomoon-icon-location-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-location-4"></span>
                                &nbsp;icomoon-icon-location-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-compass"></span>
                                &nbsp;icomoon-icon-compass
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-compass-2"></span>
                                &nbsp;icomoon-icon-compass-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-map"></span>
                                &nbsp;icomoon-icon-map
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-map-2"></span>
                                &nbsp;icomoon-icon-map-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-history"></span>
                                &nbsp;icomoon-icon-history
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-clock"></span>
                                &nbsp;icomoon-icon-clock
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-clock-2"></span>
                                &nbsp;icomoon-icon-clock-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-stopwatch"></span>
                                &nbsp;icomoon-icon-stopwatch
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-alarm"></span>
                                &nbsp;icomoon-icon-alarm
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-alarm-2"></span>
                                &nbsp;icomoon-icon-alarm-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-wrist-watch"></span>
                                &nbsp;icomoon-icon-wrist-watch
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bell"></span>
                                &nbsp;icomoon-icon-bell
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bell-2"></span>
                                &nbsp;icomoon-icon-bell-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bell-3"></span>
                                &nbsp;icomoon-icon-bell-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bell-4"></span>
                                &nbsp;icomoon-icon-bell-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-calendar"></span>
                                &nbsp;icomoon-icon-calendar
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-calendar-2"></span>
                                &nbsp;icomoon-icon-calendar-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-calendar-3"></span>
                                &nbsp;icomoon-icon-calendar-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-printer"></span>
                                &nbsp;icomoon-icon-printer
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-printer-2"></span>
                                &nbsp;icomoon-icon-printer-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-printer-3"></span>
                                &nbsp;icomoon-icon-printer-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mouse"></span>
                                &nbsp;icomoon-icon-mouse
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mouse-2"></span>
                                &nbsp;icomoon-icon-mouse-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mouse-3"></span>
                                &nbsp;icomoon-icon-mouse-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mouse-4"></span>
                                &nbsp;icomoon-icon-mouse-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-keyboard"></span>
                                &nbsp;icomoon-icon-keyboard
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-screen"></span>
                                &nbsp;icomoon-icon-screen
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-screen-2"></span>
                                &nbsp;icomoon-icon-screen-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-laptop"></span>
                                &nbsp;icomoon-icon-laptop
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mobile"></span>
                                &nbsp;icomoon-icon-mobile
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tablet"></span>
                                &nbsp;icomoon-icon-tablet
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mobile-2"></span>
                                &nbsp;icomoon-icon-mobile-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tv"></span>
                                &nbsp;icomoon-icon-tv
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tv-2"></span>
                                &nbsp;icomoon-icon-tv-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cabinet"></span>
                                &nbsp;icomoon-icon-cabinet
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-drawer"></span>
                                &nbsp;icomoon-icon-drawer
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-drawer-2"></span>
                                &nbsp;icomoon-icon-drawer-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-box"></span>
                                &nbsp;icomoon-icon-box
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-box-add"></span>
                                &nbsp;icomoon-icon-box-add
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-box-remove"></span>
                                &nbsp;icomoon-icon-box-remove
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-disk"></span>
                                &nbsp;icomoon-icon-disk
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-storage"></span>
                                &nbsp;icomoon-icon-storage
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-drive"></span>
                                &nbsp;icomoon-icon-drive
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-database"></span>
                                &nbsp;icomoon-icon-database
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-undo"></span>
                                &nbsp;icomoon-icon-undo
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-redo"></span>
                                &nbsp;icomoon-icon-redo
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-flip"></span>
                                &nbsp;icomoon-icon-flip
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-flip-2"></span>
                                &nbsp;icomoon-icon-flip-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-undo-2"></span>
                                &nbsp;icomoon-icon-undo-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-redo-2"></span>
                                &nbsp;icomoon-icon-redo-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-forward"></span>
                                &nbsp;icomoon-icon-forward
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-reply"></span>
                                &nbsp;icomoon-icon-reply
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-reply-2"></span>
                                &nbsp;icomoon-icon-reply-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments"></span>
                                &nbsp;icomoon-icon-comments
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-2"></span>
                                &nbsp;icomoon-icon-comments-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-3"></span>
                                &nbsp;icomoon-icon-comments-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-4"></span>
                                &nbsp;icomoon-icon-comments-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-5"></span>
                                &nbsp;icomoon-icon-comments-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-6"></span>
                                &nbsp;icomoon-icon-comments-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-7"></span>
                                &nbsp;icomoon-icon-comments-7
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-8"></span>
                                &nbsp;icomoon-icon-comments-8
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-9"></span>
                                &nbsp;icomoon-icon-comments-9
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-10"></span>
                                &nbsp;icomoon-icon-comments-10
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-11"></span>
                                &nbsp;icomoon-icon-comments-11
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-12"></span>
                                &nbsp;icomoon-icon-comments-12
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-13"></span>
                                &nbsp;icomoon-icon-comments-13
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-14"></span>
                                &nbsp;icomoon-icon-comments-14
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-comments-15"></span>
                                &nbsp;icomoon-icon-comments-15
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-user"></span>
                                &nbsp;icomoon-icon-user
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-users"></span>
                                &nbsp;icomoon-icon-users
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-user-2"></span>
                                &nbsp;icomoon-icon-user-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-user-3"></span>
                                &nbsp;icomoon-icon-user-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-user-4"></span>
                                &nbsp;icomoon-icon-user-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tie"></span>
                                &nbsp;icomoon-icon-tie
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-user-5"></span>
                                &nbsp;icomoon-icon-user-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-users-2"></span>
                                &nbsp;icomoon-icon-users-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-vcard"></span>
                                &nbsp;icomoon-icon-vcard
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tshirt"></span>
                                &nbsp;icomoon-icon-tshirt
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-hanger"></span>
                                &nbsp;icomoon-icon-hanger
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-quote"></span>
                                &nbsp;icomoon-icon-quote
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-quote-2"></span>
                                &nbsp;icomoon-icon-quote-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-busy"></span>
                                &nbsp;icomoon-icon-busy
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-busy-2"></span>
                                &nbsp;icomoon-icon-busy-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-busy-3"></span>
                                &nbsp;icomoon-icon-busy-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-busy-4"></span>
                                &nbsp;icomoon-icon-busy-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-loading"></span>
                                &nbsp;icomoon-icon-loading
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-loading-2"></span>
                                &nbsp;icomoon-icon-loading-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-loading-3"></span>
                                &nbsp;icomoon-icon-loading-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-loading-4"></span>
                                &nbsp;icomoon-icon-loading-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-loading-5"></span>
                                &nbsp;icomoon-icon-loading-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-loading-6"></span>
                                &nbsp;icomoon-icon-loading-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-loading-7"></span>
                                &nbsp;icomoon-icon-loading-7
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-loading-8"></span>
                                &nbsp;icomoon-icon-loading-8
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-refresh"></span>
                                &nbsp;icomoon-icon-refresh
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-microscope"></span>
                                &nbsp;icomoon-icon-microscope
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-binocular"></span>
                                &nbsp;icomoon-icon-binocular
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-search"></span>
                                &nbsp;icomoon-icon-search
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-search-2"></span>
                                &nbsp;icomoon-icon-search-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-zoom-in"></span>
                                &nbsp;icomoon-icon-zoom-in
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-zoom-out"></span>
                                &nbsp;icomoon-icon-zoom-out
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-search-3"></span>
                                &nbsp;icomoon-icon-search-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-search-4"></span>
                                &nbsp;icomoon-icon-search-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-zoom-in-2"></span>
                                &nbsp;icomoon-icon-zoom-in-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-zoom-out-2"></span>
                                &nbsp;icomoon-icon-zoom-out-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-search-5"></span>
                                &nbsp;icomoon-icon-search-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-expand"></span>
                                &nbsp;icomoon-icon-expand
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-expand-2"></span>
                                &nbsp;icomoon-icon-expand-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-expand-3"></span>
                                &nbsp;icomoon-icon-expand-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-expand-4"></span>
                                &nbsp;icomoon-icon-expand-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-full-screen"></span>
                                &nbsp;icomoon-icon-full-screen
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-contract"></span>
                                &nbsp;icomoon-icon-contract
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-contract-2"></span>
                                &nbsp;icomoon-icon-contract-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-contract-3"></span>
                                &nbsp;icomoon-icon-contract-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-contract-4"></span>
                                &nbsp;icomoon-icon-contract-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-key"></span>
                                &nbsp;icomoon-icon-key
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-key-2"></span>
                                &nbsp;icomoon-icon-key-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-key-3"></span>
                                &nbsp;icomoon-icon-key-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-keyhole"></span>
                                &nbsp;icomoon-icon-keyhole
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-locked"></span>
                                &nbsp;icomoon-icon-locked
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-unlocked"></span>
                                &nbsp;icomoon-icon-unlocked
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-locked-2"></span>
                                &nbsp;icomoon-icon-locked-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-locked-3"></span>
                                &nbsp;icomoon-icon-locked-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-wrench"></span>
                                &nbsp;icomoon-icon-wrench
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-wrench-2"></span>
                                &nbsp;icomoon-icon-wrench-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-equalizer"></span>
                                &nbsp;icomoon-icon-equalizer
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-equalizer-2"></span>
                                &nbsp;icomoon-icon-equalizer-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cog"></span>
                                &nbsp;icomoon-icon-cog
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cog-2"></span>
                                &nbsp;icomoon-icon-cog-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cogs"></span>
                                &nbsp;icomoon-icon-cogs
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-factory"></span>
                                &nbsp;icomoon-icon-factory
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tools"></span>
                                &nbsp;icomoon-icon-tools
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-hammer"></span>
                                &nbsp;icomoon-icon-hammer
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-screwdriver"></span>
                                &nbsp;icomoon-icon-screwdriver
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-wand"></span>
                                &nbsp;icomoon-icon-wand
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-wand-2"></span>
                                &nbsp;icomoon-icon-wand-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-health"></span>
                                &nbsp;icomoon-icon-health
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-aid"></span>
                                &nbsp;icomoon-icon-aid
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-patch"></span>
                                &nbsp;icomoon-icon-patch
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bug"></span>
                                &nbsp;icomoon-icon-bug
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bug-2"></span>
                                &nbsp;icomoon-icon-bug-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-construction"></span>
                                &nbsp;icomoon-icon-construction
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cone"></span>
                                &nbsp;icomoon-icon-cone
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pie"></span>
                                &nbsp;icomoon-icon-pie
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pie-2"></span>
                                &nbsp;icomoon-icon-pie-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-graph"></span>
                                &nbsp;icomoon-icon-graph
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bars"></span>
                                &nbsp;icomoon-icon-bars
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bars-2"></span>
                                &nbsp;icomoon-icon-bars-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-stats-up"></span>
                                &nbsp;icomoon-icon-stats-up
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-stats-down"></span>
                                &nbsp;icomoon-icon-stats-down
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-chart"></span>
                                &nbsp;icomoon-icon-chart
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-stairs"></span>
                                &nbsp;icomoon-icon-stairs
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-stairs-2"></span>
                                &nbsp;icomoon-icon-stairs-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-ladder"></span>
                                &nbsp;icomoon-icon-ladder
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cake"></span>
                                &nbsp;icomoon-icon-cake
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-gift"></span>
                                &nbsp;icomoon-icon-gift
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-balloon"></span>
                                &nbsp;icomoon-icon-balloon
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-rating"></span>
                                &nbsp;icomoon-icon-rating
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-rating-2"></span>
                                &nbsp;icomoon-icon-rating-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-rating-3"></span>
                                &nbsp;icomoon-icon-rating-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-podium"></span>
                                &nbsp;icomoon-icon-podium
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-medal"></span>
                                &nbsp;icomoon-icon-medal
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-crown"></span>
                                &nbsp;icomoon-icon-crown
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-trophy"></span>
                                &nbsp;icomoon-icon-trophy
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-trophy-2"></span>
                                &nbsp;icomoon-icon-trophy-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-diamond"></span>
                                &nbsp;icomoon-icon-diamond
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cup"></span>
                                &nbsp;icomoon-icon-cup
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bottle"></span>
                                &nbsp;icomoon-icon-bottle
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bottle-2"></span>
                                &nbsp;icomoon-icon-bottle-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mug"></span>
                                &nbsp;icomoon-icon-mug
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mug-2"></span>
                                &nbsp;icomoon-icon-mug-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-food"></span>
                                &nbsp;icomoon-icon-food
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-coffee"></span>
                                &nbsp;icomoon-icon-coffee
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-leaf"></span>
                                &nbsp;icomoon-icon-leaf
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tree"></span>
                                &nbsp;icomoon-icon-tree
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-paw"></span>
                                &nbsp;icomoon-icon-paw
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-flower"></span>
                                &nbsp;icomoon-icon-flower
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-rocket"></span>
                                &nbsp;icomoon-icon-rocket
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-meter"></span>
                                &nbsp;icomoon-icon-meter
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-meter-slow"></span>
                                &nbsp;icomoon-icon-meter-slow
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-meter-medium"></span>
                                &nbsp;icomoon-icon-meter-medium
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-meter-fast"></span>
                                &nbsp;icomoon-icon-meter-fast
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-dashboard"></span>
                                &nbsp;icomoon-icon-dashboard
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-dashboard-2"></span>
                                &nbsp;icomoon-icon-dashboard-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-hammer-2"></span>
                                &nbsp;icomoon-icon-hammer-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-balance"></span>
                                &nbsp;icomoon-icon-balance
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bomb"></span>
                                &nbsp;icomoon-icon-bomb
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-fire"></span>
                                &nbsp;icomoon-icon-fire
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-fire-2"></span>
                                &nbsp;icomoon-icon-fire-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-lab"></span>
                                &nbsp;icomoon-icon-lab
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-atom"></span>
                                &nbsp;icomoon-icon-atom
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-magnet"></span>
                                &nbsp;icomoon-icon-magnet
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-skull"></span>
                                &nbsp;icomoon-icon-skull
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-lamp"></span>
                                &nbsp;icomoon-icon-lamp
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-lamp-2"></span>
                                &nbsp;icomoon-icon-lamp-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-lamp-3"></span>
                                &nbsp;icomoon-icon-lamp-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-remove"></span>
                                &nbsp;icomoon-icon-remove
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-remove-2"></span>
                                &nbsp;icomoon-icon-remove-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-remove-3"></span>
                                &nbsp;icomoon-icon-remove-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-remove-4"></span>
                                &nbsp;icomoon-icon-remove-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-remove-5"></span>
                                &nbsp;icomoon-icon-remove-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-remove-6"></span>
                                &nbsp;icomoon-icon-remove-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-recycle"></span>
                                &nbsp;icomoon-icon-recycle
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pin"></span>
                                &nbsp;icomoon-icon-pin
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-briefcase"></span>
                                &nbsp;icomoon-icon-briefcase
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-briefcase-2"></span>
                                &nbsp;icomoon-icon-briefcase-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-briefcase-3"></span>
                                &nbsp;icomoon-icon-briefcase-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-airplane"></span>
                                &nbsp;icomoon-icon-airplane
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-airplane-2"></span>
                                &nbsp;icomoon-icon-airplane-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-paper-plane"></span>
                                &nbsp;icomoon-icon-paper-plane
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cars"></span>
                                &nbsp;icomoon-icon-cars
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bus"></span>
                                &nbsp;icomoon-icon-bus
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-truck"></span>
                                &nbsp;icomoon-icon-truck
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bike"></span>
                                &nbsp;icomoon-icon-bike
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-road"></span>
                                &nbsp;icomoon-icon-road
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cube"></span>
                                &nbsp;icomoon-icon-cube
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cube-2"></span>
                                &nbsp;icomoon-icon-cube-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-puzzle"></span>
                                &nbsp;icomoon-icon-puzzle
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-glasses"></span>
                                &nbsp;icomoon-icon-glasses
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-glasses-2"></span>
                                &nbsp;icomoon-icon-glasses-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-3d-glasses"></span>
                                &nbsp;icomoon-icon-3d-glasses
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-glasses-3"></span>
                                &nbsp;icomoon-icon-glasses-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-sun-glasses"></span>
                                &nbsp;icomoon-icon-sun-glasses
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-accessibility"></span>
                                &nbsp;icomoon-icon-accessibility
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-accessibility-2"></span>
                                &nbsp;icomoon-icon-accessibility-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-brain"></span>
                                &nbsp;icomoon-icon-brain
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-target"></span>
                                &nbsp;icomoon-icon-target
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-target-2"></span>
                                &nbsp;icomoon-icon-target-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-gun"></span>
                                &nbsp;icomoon-icon-gun
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-shield"></span>
                                &nbsp;icomoon-icon-shield
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-shield-2"></span>
                                &nbsp;icomoon-icon-shield-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-soccer"></span>
                                &nbsp;icomoon-icon-soccer
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-football"></span>
                                &nbsp;icomoon-icon-football
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-baseball"></span>
                                &nbsp;icomoon-icon-baseball
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-basketball"></span>
                                &nbsp;icomoon-icon-basketball
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-hockey"></span>
                                &nbsp;icomoon-icon-hockey
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-racing"></span>
                                &nbsp;icomoon-icon-racing
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-golf"></span>
                                &nbsp;icomoon-icon-golf
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-lightning"></span>
                                &nbsp;icomoon-icon-lightning
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-power"></span>
                                &nbsp;icomoon-icon-power
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-power-2"></span>
                                &nbsp;icomoon-icon-power-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-switch"></span>
                                &nbsp;icomoon-icon-switch
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-power-cord"></span>
                                &nbsp;icomoon-icon-power-cord
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-socket"></span>
                                &nbsp;icomoon-icon-socket
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-clipboard"></span>
                                &nbsp;icomoon-icon-clipboard
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-clipboard-2"></span>
                                &nbsp;icomoon-icon-clipboard-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-clipboard-3"></span>
                                &nbsp;icomoon-icon-clipboard-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-list-view"></span>
                                &nbsp;icomoon-icon-list-view
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-list-view-2"></span>
                                &nbsp;icomoon-icon-list-view-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-playlist"></span>
                                &nbsp;icomoon-icon-playlist
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-grid-view"></span>
                                &nbsp;icomoon-icon-grid-view
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-grid"></span>
                                &nbsp;icomoon-icon-grid
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-grid-view-2"></span>
                                &nbsp;icomoon-icon-grid-view-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tree-view"></span>
                                &nbsp;icomoon-icon-tree-view
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-menu"></span>
                                &nbsp;icomoon-icon-menu
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-menu-2"></span>
                                &nbsp;icomoon-icon-menu-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cloud"></span>
                                &nbsp;icomoon-icon-cloud
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cloud-2"></span>
                                &nbsp;icomoon-icon-cloud-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cloud-3"></span>
                                &nbsp;icomoon-icon-cloud-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cloud-4"></span>
                                &nbsp;icomoon-icon-cloud-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cloud-5"></span>
                                &nbsp;icomoon-icon-cloud-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-download"></span>
                                &nbsp;icomoon-icon-download
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-download-2"></span>
                                &nbsp;icomoon-icon-download-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-upload"></span>
                                &nbsp;icomoon-icon-upload
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-upload-2"></span>
                                &nbsp;icomoon-icon-upload-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-upload-3"></span>
                                &nbsp;icomoon-icon-upload-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-upload-4"></span>
                                &nbsp;icomoon-icon-upload-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-upload-5"></span>
                                &nbsp;icomoon-icon-upload-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-globe"></span>
                                &nbsp;icomoon-icon-globe
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-anchor"></span>
                                &nbsp;icomoon-icon-anchor
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-network"></span>
                                &nbsp;icomoon-icon-network
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-download-3"></span>
                                &nbsp;icomoon-icon-download-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-link"></span>
                                &nbsp;icomoon-icon-link
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-link-2"></span>
                                &nbsp;icomoon-icon-link-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-link"></span>
                                &nbsp;icomoon-icon-link
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-link-2"></span>
                                &nbsp;icomoon-icon-link-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-link-3"></span>
                                &nbsp;icomoon-icon-link-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-flag"></span>
                                &nbsp;icomoon-icon-flag
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-flag-2"></span>
                                &nbsp;icomoon-icon-flag-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-flag-3"></span>
                                &nbsp;icomoon-icon-flag-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-flag-4"></span>
                                &nbsp;icomoon-icon-flag-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-attachment"></span>
                                &nbsp;icomoon-icon-attachment
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-eye"></span>
                                &nbsp;icomoon-icon-eye
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-eye-2"></span>
                                &nbsp;icomoon-icon-eye-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-eye-3"></span>
                                &nbsp;icomoon-icon-eye-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-eye-4"></span>
                                &nbsp;icomoon-icon-eye-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bookmark"></span>
                                &nbsp;icomoon-icon-bookmark
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bookmark-2"></span>
                                &nbsp;icomoon-icon-bookmark-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-starburst"></span>
                                &nbsp;icomoon-icon-starburst
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-snowflake"></span>
                                &nbsp;icomoon-icon-snowflake
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-snow-man"></span>
                                &nbsp;icomoon-icon-snow-man
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-temperature"></span>
                                &nbsp;icomoon-icon-temperature
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-temperature-2"></span>
                                &nbsp;icomoon-icon-temperature-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-weather"></span>
                                &nbsp;icomoon-icon-weather
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-weather-2"></span>
                                &nbsp;icomoon-icon-weather-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-weather-3"></span>
                                &nbsp;icomoon-icon-weather-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-windy"></span>
                                &nbsp;icomoon-icon-windy
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-fan"></span>
                                &nbsp;icomoon-icon-fan
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-umbrella"></span>
                                &nbsp;icomoon-icon-umbrella
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-weather-4"></span>
                                &nbsp;icomoon-icon-weather-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-sun"></span>
                                &nbsp;icomoon-icon-sun
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-contrast"></span>
                                &nbsp;icomoon-icon-contrast
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-contrast-2"></span>
                                &nbsp;icomoon-icon-contrast-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-moon"></span>
                                &nbsp;icomoon-icon-moon
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bed"></span>
                                &nbsp;icomoon-icon-bed
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bed-2"></span>
                                &nbsp;icomoon-icon-bed-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-star"></span>
                                &nbsp;icomoon-icon-star
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-star-2"></span>
                                &nbsp;icomoon-icon-star-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-star-3"></span>
                                &nbsp;icomoon-icon-star-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-star-4"></span>
                                &nbsp;icomoon-icon-star-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-star-5"></span>
                                &nbsp;icomoon-icon-star-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-star-6"></span>
                                &nbsp;icomoon-icon-star-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-heart"></span>
                                &nbsp;icomoon-icon-heart
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-heart-2"></span>
                                &nbsp;icomoon-icon-heart-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-heart-3"></span>
                                &nbsp;icomoon-icon-heart-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-heart-4"></span>
                                &nbsp;icomoon-icon-heart-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-heart-5"></span>
                                &nbsp;icomoon-icon-heart-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-heart-6"></span>
                                &nbsp;icomoon-icon-heart-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-heart-7"></span>
                                &nbsp;icomoon-icon-heart-7
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-heart-8"></span>
                                &nbsp;icomoon-icon-heart-8
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-thumbs-up"></span>
                                &nbsp;icomoon-icon-thumbs-up
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-thumbs-down"></span>
                                &nbsp;icomoon-icon-thumbs-down
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-thumbs-up-2"></span>
                                &nbsp;icomoon-icon-thumbs-up-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-thumbs-down-2"></span>
                                &nbsp;icomoon-icon-thumbs-down-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-thumbs-up-3"></span>
                                &nbsp;icomoon-icon-thumbs-up-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-thumbs-down-3"></span>
                                &nbsp;icomoon-icon-thumbs-down-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-people"></span>
                                &nbsp;icomoon-icon-people
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-man"></span>
                                &nbsp;icomoon-icon-man
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-male"></span>
                                &nbsp;icomoon-icon-male
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-woman"></span>
                                &nbsp;icomoon-icon-woman
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-female"></span>
                                &nbsp;icomoon-icon-female
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-peace"></span>
                                &nbsp;icomoon-icon-peace
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-yin-yang"></span>
                                &nbsp;icomoon-icon-yin-yang
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-ampersand"></span>
                                &nbsp;icomoon-icon-ampersand
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-ampersand-2"></span>
                                &nbsp;icomoon-icon-ampersand-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-happy"></span>
                                &nbsp;icomoon-icon-happy
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-happy-2"></span>
                                &nbsp;icomoon-icon-happy-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-smiley"></span>
                                &nbsp;icomoon-icon-smiley
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-smiley-2"></span>
                                &nbsp;icomoon-icon-smiley-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-neutral"></span>
                                &nbsp;icomoon-icon-neutral
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-neutral-2"></span>
                                &nbsp;icomoon-icon-neutral-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-sad"></span>
                                &nbsp;icomoon-icon-sad
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-sad-2"></span>
                                &nbsp;icomoon-icon-sad-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-shocked"></span>
                                &nbsp;icomoon-icon-shocked
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-shocked-2"></span>
                                &nbsp;icomoon-icon-shocked-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pointer"></span>
                                &nbsp;icomoon-icon-pointer
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-hand"></span>
                                &nbsp;icomoon-icon-hand
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-move"></span>
                                &nbsp;icomoon-icon-move
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-resize"></span>
                                &nbsp;icomoon-icon-resize
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-resize-2"></span>
                                &nbsp;icomoon-icon-resize-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-warning"></span>
                                &nbsp;icomoon-icon-warning
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-warning-2"></span>
                                &nbsp;icomoon-icon-warning-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-plus"></span>
                                &nbsp;icomoon-icon-plus
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-minus"></span>
                                &nbsp;icomoon-icon-minus
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-help"></span>
                                &nbsp;icomoon-icon-help
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-help-2"></span>
                                &nbsp;icomoon-icon-help-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-info"></span>
                                &nbsp;icomoon-icon-info
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-info-2"></span>
                                &nbsp;icomoon-icon-info-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-blocked"></span>
                                &nbsp;icomoon-icon-blocked
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-blocked-2"></span>
                                &nbsp;icomoon-icon-blocked-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-error"></span>
                                &nbsp;icomoon-icon-error
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cancel"></span>
                                &nbsp;icomoon-icon-cancel
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cancel-2"></span>
                                &nbsp;icomoon-icon-cancel-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cancel-3"></span>
                                &nbsp;icomoon-icon-cancel-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-cancel-4"></span>
                                &nbsp;icomoon-icon-cancel-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-checkmark"></span>
                                &nbsp;icomoon-icon-checkmark
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-checkmark-2"></span>
                                &nbsp;icomoon-icon-checkmark-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-checkmark-3"></span>
                                &nbsp;icomoon-icon-checkmark-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-spell-check"></span>
                                &nbsp;icomoon-icon-spell-check
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-minus-2"></span>
                                &nbsp;icomoon-icon-minus-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-minus-3"></span>
                                &nbsp;icomoon-icon-minus-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-plus-2"></span>
                                &nbsp;icomoon-icon-plus-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-plus-3"></span>
                                &nbsp;icomoon-icon-plus-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-enter"></span>
                                &nbsp;icomoon-icon-enter
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-exit"></span>
                                &nbsp;icomoon-icon-exit
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-exit-2"></span>
                                &nbsp;icomoon-icon-exit-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-play"></span>
                                &nbsp;icomoon-icon-play
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pause"></span>
                                &nbsp;icomoon-icon-pause
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-stop"></span>
                                &nbsp;icomoon-icon-stop
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-volume-high"></span>
                                &nbsp;icomoon-icon-volume-high
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-volume-medium"></span>
                                &nbsp;icomoon-icon-volume-medium
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-volume-low"></span>
                                &nbsp;icomoon-icon-volume-low
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mute"></span>
                                &nbsp;icomoon-icon-mute
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mute-2"></span>
                                &nbsp;icomoon-icon-mute-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-volume-increase"></span>
                                &nbsp;icomoon-icon-volume-increase
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-volume-decrease"></span>
                                &nbsp;icomoon-icon-volume-decrease
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-volume"></span>
                                &nbsp;icomoon-icon-volume
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-volume-2"></span>
                                &nbsp;icomoon-icon-volume-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-volume-3"></span>
                                &nbsp;icomoon-icon-volume-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-volume-4"></span>
                                &nbsp;icomoon-icon-volume-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-volume-5"></span>
                                &nbsp;icomoon-icon-volume-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-volume-6"></span>
                                &nbsp;icomoon-icon-volume-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-next"></span>
                                &nbsp;icomoon-icon-next
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-previous"></span>
                                &nbsp;icomoon-icon-previous
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-first"></span>
                                &nbsp;icomoon-icon-first
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-last"></span>
                                &nbsp;icomoon-icon-last
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-loop"></span>
                                &nbsp;icomoon-icon-loop
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-loop-2"></span>
                                &nbsp;icomoon-icon-loop-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-loop-3"></span>
                                &nbsp;icomoon-icon-loop-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-shuffle"></span>
                                &nbsp;icomoon-icon-shuffle
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-first"></span>
                                &nbsp;icomoon-icon-arrow-first
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-last"></span>
                                &nbsp;icomoon-icon-arrow-last
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up"></span>
                                &nbsp;icomoon-icon-arrow-up
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-right"></span>
                                &nbsp;icomoon-icon-arrow-right
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down"></span>
                                &nbsp;icomoon-icon-arrow-down
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-left"></span>
                                &nbsp;icomoon-icon-arrow-left
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-2"></span>
                                &nbsp;icomoon-icon-arrow-up-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-right-2"></span>
                                &nbsp;icomoon-icon-arrow-right-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-2"></span>
                                &nbsp;icomoon-icon-arrow-down-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-left-2"></span>
                                &nbsp;icomoon-icon-arrow-left-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-left"></span>
                                &nbsp;icomoon-icon-arrow-up-left
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-3"></span>
                                &nbsp;icomoon-icon-arrow-up-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-right"></span>
                                &nbsp;icomoon-icon-arrow-up-right
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-right-3"></span>
                                &nbsp;icomoon-icon-arrow-right-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-right"></span>
                                &nbsp;icomoon-icon-arrow-down-right
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-3"></span>
                                &nbsp;icomoon-icon-arrow-down-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-left"></span>
                                &nbsp;icomoon-icon-arrow-down-left
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-left-3"></span>
                                &nbsp;icomoon-icon-arrow-left-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-left-2"></span>
                                &nbsp;icomoon-icon-arrow-up-left-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-4"></span>
                                &nbsp;icomoon-icon-arrow-up-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-right-2"></span>
                                &nbsp;icomoon-icon-arrow-up-right-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-right-4"></span>
                                &nbsp;icomoon-icon-arrow-right-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-right-2"></span>
                                &nbsp;icomoon-icon-arrow-down-right-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-4"></span>
                                &nbsp;icomoon-icon-arrow-down-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-left-2"></span>
                                &nbsp;icomoon-icon-arrow-down-left-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-left-4"></span>
                                &nbsp;icomoon-icon-arrow-left-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-left-3"></span>
                                &nbsp;icomoon-icon-arrow-up-left-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-5"></span>
                                &nbsp;icomoon-icon-arrow-up-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-right-3"></span>
                                &nbsp;icomoon-icon-arrow-up-right-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-right-5"></span>
                                &nbsp;icomoon-icon-arrow-right-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-right-3"></span>
                                &nbsp;icomoon-icon-arrow-down-right-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-5"></span>
                                &nbsp;icomoon-icon-arrow-down-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-left-3"></span>
                                &nbsp;icomoon-icon-arrow-down-left-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-left-5"></span>
                                &nbsp;icomoon-icon-arrow-left-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-6"></span>
                                &nbsp;icomoon-icon-arrow-up-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-right-6"></span>
                                &nbsp;icomoon-icon-arrow-right-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-6"></span>
                                &nbsp;icomoon-icon-arrow-down-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-left-6"></span>
                                &nbsp;icomoon-icon-arrow-left-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-7"></span>
                                &nbsp;icomoon-icon-arrow-up-7
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-right-7"></span>
                                &nbsp;icomoon-icon-arrow-right-7
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-7"></span>
                                &nbsp;icomoon-icon-arrow-down-7
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-left-7"></span>
                                &nbsp;icomoon-icon-arrow-left-7
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-8"></span>
                                &nbsp;icomoon-icon-arrow-up-8
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-right-8"></span>
                                &nbsp;icomoon-icon-arrow-right-8
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-8"></span>
                                &nbsp;icomoon-icon-arrow-down-8
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-left-8"></span>
                                &nbsp;icomoon-icon-arrow-left-8
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-9"></span>
                                &nbsp;icomoon-icon-arrow-up-9
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-right-9"></span>
                                &nbsp;icomoon-icon-arrow-right-9
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-9"></span>
                                &nbsp;icomoon-icon-arrow-down-9
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-left-9"></span>
                                &nbsp;icomoon-icon-arrow-left-9
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-up-10"></span>
                                &nbsp;icomoon-icon-arrow-up-10
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-right-10"></span>
                                &nbsp;icomoon-icon-arrow-right-10
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-down-10"></span>
                                &nbsp;icomoon-icon-arrow-down-10
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-arrow-left-10"></span>
                                &nbsp;icomoon-icon-arrow-left-10
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-menu"></span>
                                &nbsp;icomoon-icon-menu
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-enter-2"></span>
                                &nbsp;icomoon-icon-enter-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-enter-3"></span>
                                &nbsp;icomoon-icon-enter-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-backspace"></span>
                                &nbsp;icomoon-icon-backspace
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-backspace-2"></span>
                                &nbsp;icomoon-icon-backspace-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tab"></span>
                                &nbsp;icomoon-icon-tab
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tab-2"></span>
                                &nbsp;icomoon-icon-tab-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-command"></span>
                                &nbsp;icomoon-icon-command
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-checkbox"></span>
                                &nbsp;icomoon-icon-checkbox
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-checkbox-unchecked"></span>
                                &nbsp;icomoon-icon-checkbox-unchecked
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-checkbox-partial"></span>
                                &nbsp;icomoon-icon-checkbox-partial
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-checkbox-2"></span>
                                &nbsp;icomoon-icon-checkbox-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-checkbox-unchecked-2"></span>
                                &nbsp;icomoon-icon-checkbox-unchecked-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-checkbox-partial-2"></span>
                                &nbsp;icomoon-icon-checkbox-partial-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-radio-checked"></span>
                                &nbsp;icomoon-icon-radio-checked
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-radio-unchecked"></span>
                                &nbsp;icomoon-icon-radio-unchecked
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-crop"></span>
                                &nbsp;icomoon-icon-crop
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-vector"></span>
                                &nbsp;icomoon-icon-vector
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-rulers"></span>
                                &nbsp;icomoon-icon-rulers
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-scissors"></span>
                                &nbsp;icomoon-icon-scissors
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-scissors-2"></span>
                                &nbsp;icomoon-icon-scissors-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-filter"></span>
                                &nbsp;icomoon-icon-filter
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-type"></span>
                                &nbsp;icomoon-icon-type
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-font-size"></span>
                                &nbsp;icomoon-icon-font-size
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bold"></span>
                                &nbsp;icomoon-icon-bold
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-italic"></span>
                                &nbsp;icomoon-icon-italic
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-underline"></span>
                                &nbsp;icomoon-icon-underline
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-font"></span>
                                &nbsp;icomoon-icon-font
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-paragraph-left"></span>
                                &nbsp;icomoon-icon-paragraph-left
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-paragraph-center"></span>
                                &nbsp;icomoon-icon-paragraph-center
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-paragraph-right"></span>
                                &nbsp;icomoon-icon-paragraph-right
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-left-to-right"></span>
                                &nbsp;icomoon-icon-left-to-right
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-right-to-left"></span>
                                &nbsp;icomoon-icon-right-to-left
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-out"></span>
                                &nbsp;icomoon-icon-out
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-out-2"></span>
                                &nbsp;icomoon-icon-out-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-popout"></span>
                                &nbsp;icomoon-icon-popout
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-embed"></span>
                                &nbsp;icomoon-icon-embed
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-code"></span>
                                &nbsp;icomoon-icon-code
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-seven-segment"></span>
                                &nbsp;icomoon-icon-seven-segment
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-seven-segment-2"></span>
                                &nbsp;icomoon-icon-seven-segment-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-seven-segment-3"></span>
                                &nbsp;icomoon-icon-seven-segment-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-seven-segment-4"></span>
                                &nbsp;icomoon-icon-seven-segment-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-seven-segment-5"></span>
                                &nbsp;icomoon-icon-seven-segment-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-seven-segment-6"></span>
                                &nbsp;icomoon-icon-seven-segment-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-seven-segment-7"></span>
                                &nbsp;icomoon-icon-seven-segment-7
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-seven-segment-8"></span>
                                &nbsp;icomoon-icon-seven-segment-8
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-seven-segment-9"></span>
                                &nbsp;icomoon-icon-seven-segment-9
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-seven-segment-10"></span>
                                &nbsp;icomoon-icon-seven-segment-10
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-bluetooth"></span>
                                &nbsp;icomoon-icon-bluetooth
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-share"></span>
                                &nbsp;icomoon-icon-share
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-share-2"></span>
                                &nbsp;icomoon-icon-share-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mail"></span>
                                &nbsp;icomoon-icon-mail
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-mail-2"></span>
                                &nbsp;icomoon-icon-mail-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-google-plus"></span>
                                &nbsp;icomoon-icon-google-plus
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-google-plus-2"></span>
                                &nbsp;icomoon-icon-google-plus-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-google-plus-3"></span>
                                &nbsp;icomoon-icon-google-plus-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-gplus"></span>
                                &nbsp;icomoon-icon-gplus
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-facebook"></span>
                                &nbsp;icomoon-icon-facebook
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-facebook-2"></span>
                                &nbsp;icomoon-icon-facebook-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-facebook-3"></span>
                                &nbsp;icomoon-icon-facebook-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-facebook-4"></span>
                                &nbsp;icomoon-icon-facebook-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-twitter"></span>
                                &nbsp;icomoon-icon-twitter
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-twitter-2"></span>
                                &nbsp;icomoon-icon-twitter-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-twitter-3"></span>
                                &nbsp;icomoon-icon-twitter-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-feed"></span>
                                &nbsp;icomoon-icon-feed
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-feed-2"></span>
                                &nbsp;icomoon-icon-feed-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-feed-3"></span>
                                &nbsp;icomoon-icon-feed-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-youtube"></span>
                                &nbsp;icomoon-icon-youtube
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-youtube-2"></span>
                                &nbsp;icomoon-icon-youtube-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-vimeo"></span>
                                &nbsp;icomoon-icon-vimeo
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-vimeo-2"></span>
                                &nbsp;icomoon-icon-vimeo-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-flickr"></span>
                                &nbsp;icomoon-icon-flickr
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-flickr-2"></span>
                                &nbsp;icomoon-icon-flickr-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-flickr-3"></span>
                                &nbsp;icomoon-icon-flickr-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-picassa"></span>
                                &nbsp;icomoon-icon-picassa
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-picassa-2"></span>
                                &nbsp;icomoon-icon-picassa-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-dribbble"></span>
                                &nbsp;icomoon-icon-dribbble
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-dribbble-2"></span>
                                &nbsp;icomoon-icon-dribbble-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-dribbble-3"></span>
                                &nbsp;icomoon-icon-dribbble-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-forrst"></span>
                                &nbsp;icomoon-icon-forrst
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-forrst-2"></span>
                                &nbsp;icomoon-icon-forrst-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-deviantart"></span>
                                &nbsp;icomoon-icon-deviantart
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-deviantart-2"></span>
                                &nbsp;icomoon-icon-deviantart-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-github"></span>
                                &nbsp;icomoon-icon-github
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-github-2"></span>
                                &nbsp;icomoon-icon-github-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-github-3"></span>
                                &nbsp;icomoon-icon-github-3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-github-4"></span>
                                &nbsp;icomoon-icon-github-4
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-github-5"></span>
                                &nbsp;icomoon-icon-github-5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-github-6"></span>
                                &nbsp;icomoon-icon-github-6
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-git"></span>
                                &nbsp;icomoon-icon-git
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-github-7"></span>
                                &nbsp;icomoon-icon-github-7
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-wordpress"></span>
                                &nbsp;icomoon-icon-wordpress
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-wordpress-2"></span>
                                &nbsp;icomoon-icon-wordpress-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-blogger"></span>
                                &nbsp;icomoon-icon-blogger
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-blogger-2"></span>
                                &nbsp;icomoon-icon-blogger-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tumblr"></span>
                                &nbsp;icomoon-icon-tumblr
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-tumblr-2"></span>
                                &nbsp;icomoon-icon-tumblr-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-yahoo"></span>
                                &nbsp;icomoon-icon-yahoo
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-yahoo-2"></span>
                                &nbsp;icomoon-icon-yahoo-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-amazon"></span>
                                &nbsp;icomoon-icon-amazon
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-amazon-2"></span>
                                &nbsp;icomoon-icon-amazon-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-apple"></span>
                                &nbsp;icomoon-icon-apple
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-finder"></span>
                                &nbsp;icomoon-icon-finder
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-android"></span>
                                &nbsp;icomoon-icon-android
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-windows"></span>
                                &nbsp;icomoon-icon-windows
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-soundcloud"></span>
                                &nbsp;icomoon-icon-soundcloud
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-soundcloud-2"></span>
                                &nbsp;icomoon-icon-soundcloud-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-skype"></span>
                                &nbsp;icomoon-icon-skype
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-reddit"></span>
                                &nbsp;icomoon-icon-reddit
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-linkedin"></span>
                                &nbsp;icomoon-icon-linkedin
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-lastfm"></span>
                                &nbsp;icomoon-icon-lastfm
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-lastfm-2"></span>
                                &nbsp;icomoon-icon-lastfm-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-delicious"></span>
                                &nbsp;icomoon-icon-delicious
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-stumbleupon"></span>
                                &nbsp;icomoon-icon-stumbleupon
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-stumbleupon-2"></span>
                                &nbsp;icomoon-icon-stumbleupon-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pinterest"></span>
                                &nbsp;icomoon-icon-pinterest
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-pinterest-2"></span>
                                &nbsp;icomoon-icon-pinterest-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-xing"></span>
                                &nbsp;icomoon-icon-xing
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-libreoffice"></span>
                                &nbsp;icomoon-icon-libreoffice
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-pdf"></span>
                                &nbsp;icomoon-icon-file-pdf
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-openoffice"></span>
                                &nbsp;icomoon-icon-file-openoffice
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-word"></span>
                                &nbsp;icomoon-icon-file-word
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-excel"></span>
                                &nbsp;icomoon-icon-file-excel
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-powerpoint"></span>
                                &nbsp;icomoon-icon-file-powerpoint
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-zip"></span>
                                &nbsp;icomoon-icon-file-zip
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-xml"></span>
                                &nbsp;icomoon-icon-file-xml
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-file-css"></span>
                                &nbsp;icomoon-icon-file-css
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-html5"></span>
                                &nbsp;icomoon-icon-html5
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-html5-2"></span>
                                &nbsp;icomoon-icon-html5-2
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-css3"></span>
                                &nbsp;icomoon-icon-css3
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-chrome"></span>
                                &nbsp;icomoon-icon-chrome
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-firefox"></span>
                                &nbsp;icomoon-icon-firefox
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-IE"></span>
                                &nbsp;icomoon-icon-IE
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-opera"></span>
                                &nbsp;icomoon-icon-opera
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-safari"></span>
                                &nbsp;icomoon-icon-safari
                            </span>
                            <span class="box1">
                                <span aria-hidden="true" class="icomoon-icon-IcoMoon"></span>
                                &nbsp;icomoon-icon-IcoMoon
                            </span>


                        </div>

                    </div><!-- End .box -->

                </div><!-- End .span12 -->                  

            </div><!-- End .row-fluid -->
        </div>
    </div>  
</div>
<script type="text/javascript">
    $(document).ready(function() {

        // JQuery UI Modal Dialog
        $('#previewModal').dialog({
            autoOpen: false,
            modal: true,
            autoResize: true,
            dialogClass: 'dialog',
            buttons: {
                "Close": function() {
                    $(this).dialog("close");
                }
            }
        });
        $('#openModalDialog').click(function(){
            $('#previewModal').dialog('open');
            return false;
        });
        $('.box1').click(function(e){
            cls = $(this).children(":first").attr('class');
            $("#selectMenuIcon").val(cls);
            $("#previewModal").dialog("close");
            $("#sicon").removeClass().addClass(cls);
            return false;
        });

        $("#addmore").live('click',function(e){
            // get childs
            $chldc = $("#toadd div").children().length;
            $cnt = $chldc + 1;
            $justInserted = $("#titleFiles").clone();
            $justInserted.attr('id', "dynamic"+$cnt);
            $justInserted.find('input').each(function(i, e) {
                $(e).attr('name', $(e).attr('name').replace('[1]', '[' + ($cnt) + ']'));
                $(e).attr('id', $(e).attr('id').replace('_1', '_' + ($cnt)));
                $(e).val('');
            });
            $("#toadd").append($justInserted);
        });
    });
</script>