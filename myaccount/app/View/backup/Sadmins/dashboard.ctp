<div class="marginB10"></div>
<div class="page-header">
    <h1>Welcome Back to RankRatio Admin Dashboard</h1>
    <h2><span class="yellow bold"><?php echo count($allDomain); ?> Domains Expiring in Next <?php echo $days; ?> Days </span></h2>
</div>
<div class="margin10"></div>
<div class="row-fluid">
    <div class="span10">
        <div class="centerContent">
            <ul class="bigBtnIcon">
                <!--<li>
                <a href="#" title="How Many Program you have entered so far" class="tip">
                <span class="icon icomoon-icon-users"></span>
                <span class="txt">Programs</span>
                <span class="notification">< ?php echo $programs;?></span>
                </a>
                </li>
                <li>
                <a href="#" title="How Many Registration Account your Have so far"  class="tip">
                <span class="icon icomoon-icon-support"></span>
                <span class="txt">Registration Account</span>
                <span class="notification blue">< ?php echo $regsiter;?></span>
                </a>
                </li>
                <li>
                <a href="#" title="How Many Hosting Server you have now"  class="tip">
                <span class="icon icomoon-icon-comments-2"></span>
                <span class="txt">Hosting Servers</span>
                <span class="notification green">< ?php echo $hosting;?></span>
                </a>
                </li>-->
                <li>
                    <a href="<?php echo Router::url('activedomains'); ?>" title="Domain that are active and posting to them"  class="tip">
                        <span class="icon icomoon-icon-basket"></span>
                        <span class="txt">All Active Domains</span>
                        <span class="notification"><?php echo $tD;?></span>
                    </a>
                </li>
                <!--<li>
                    <a href="< ?php echo Router::url('inactivedomains'); ?>" title="Number of domain which are not getting any post because of error"  class="tip">
                        <span class="icon icomoon-icon-basket"></span>
                        <span class="txt">Inactive Domains</span>
                        <span class="notification">< ?php echo $tiD;?></span>
                    </a>
                </li>-->
                <li>
                    <a href="<?php echo Router::url('problematics'); ?>" title="How Many Domain status marked as Problematic"  class="tip">
                        <span class="icon icomoon-icon-history"></span>
                        <span class="txt">Problematic Domains</span>
                        <span class="notification"><?php echo $pD;?></span>
                    </a>
                </li>
                <!--<li>
                <a href="#" title="How Many Access you have in your password Manager"  class="tip">
                <span class="icon icomoon-icon-meter-fast"></span>
                <span class="txt">Password Manager</span>
                <span class="notification">< ?php echo $access;?></span>

                </a>
                </li>-->
            </ul>
        </div>
    </div><!-- End .span8 -->

</div><!-- End .row-fluid -->




<div class="marginB10"></div>
<div class="page-header">
    <h3>Site Expiring Statistics</h3>
    <p>please enter a number of days in the input box below to see how many domain(s) will be expiring by that day. <span class="yellow">By Default 15 days is showing</span></p>
</div>
<div class="margin10"></div>




<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'dashboard'), 'id' => 'dashboardForm', 'method' => 'post', 'type' => 'file',  'class' => 'form-horizontal seperator')); ?>

    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Number of Days <span class="help-block-inline">enter number of days you want to see</span>
                <span class="yellow">Enter 0 (zero) to list expired domains</span> </h4>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="expire_on">Days: <span class="help-block-inline">enter how many days you wants to see</span></label>
                <div class="span8">
                    <?php echo $this->Form->input('Domain.days', array('error' => false, 'type' => 'text', 'id' => 'expire_on', 'title' => 'number of days', 'placeholder' => "number of days", 'div' => false, 'label' => false, 'required' => true, 'class' => 'span3 tip')); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle" type="submit">Search Domain</button>
    </div>
    <?php $this->Form->end(); ?>
</div>


<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Domain Expiring in next <?php echo $days; ?> DAYS <span class="help-block-inline"> take necessary actions below </span></h4>
            </div>
        </div>
    </div>
</div>
<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="content">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allDomain)) echo 'dynamicTable'; ?> display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th>Domain</th>
                        <th>Root</th>
                        <th>Is Indexed?</th>
                        <th>Expire Date</th>
                        <th>Number of Post</th>
                        <!--<th>Actions</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (isset($allDomain) && count($allDomain) > 0):
                            foreach ($allDomain as $k => $arr):
                                $d  = trim($arr['Domain']['domain']);
                                $uu = str_replace('http://','',$d);
                                $dns = @array_pop(explode(",", preg_replace("/\s/","",strtolower($arr['Domain']['dns1']))));
                                $dns = str_replace("ns1.","",$dns);
                                $dns = str_replace("ns2.","",$dns);

                            ?>
                            <tr>
                                <td><?php echo '<a target="_blank" href="http://'.$uu.'">'.$uu.'</a>'; ?></td>
                                <td><?php echo $dns; ?></td>
                                <td><?php echo $arr['Domain']['is_indexed'] ? 'Yes' : 'No'; ?></td>
                                <td><?php echo date("M d, Y", strtotime($arr['Domain']['expires_on'])); ?></td>
                                <td><?php echo $this->NitroCustom->rCount('submissionreports', " domain_id = ".$arr['Domain']['id']);  ?></td>
                                <!-- <td>
                                <div class="controls center">
                                <a href="< ?php echo adminHome . '/editdomain/' . $arr['Domain']['id']; ?>" title="Edit Domain" class="tip">edit</a>
                                </div>
                                </td>-->
                            </tr>
                            <?php
                                endforeach;
                        ?>

                        <?php
                            else:
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="5">No Domain(s) Found expiring in next <?php echo $days; ?> Days!</td>
                        </tr>
                        <?php
                            endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>