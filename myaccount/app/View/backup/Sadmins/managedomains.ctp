<style type="text/css">
#sTable_processing{
  position: absolute;
  top:300px;
  left: 50%;
}
</style>
<div class="marginB10"></div>
<div class="page-header">
    <h4>All Types of Domains</h4>
    <p>Bellow are all the domains. To take necessary action choose action under actions tab.
        <br>
        <!--span style="background-color: yellow;">

            Premium: Has to have 1 of the following 3 qualifications
            -PR3
            OR Trust Flow 13 or greater
            OR SEOMoz Rank of 3 or greater <br>

            Prime: Has to have 1 of the following 3 qualifications
            -PR2-1
            OR Trust Flow of 8 - 12
            OR SEOMoz Rank of 1-2
            <br>

            Guest Blog:
            - PR greater 3

            Low Grade
            all the rest

        </span-->
    </p>

</div>


<div class="row-fluid">
    <div class="span12">
  <a style="position: absolute; left: 70%; z-index: 20" title="Export all the domains with status" class="btn btn-link tip" target="_blank" href="<?php echo adminHome . '/exportdomainstatus'; ?>"><span class="icon16 icomoon-icon-file-excel"></span> Export List </a>

        <div class="content">
            <!--<table cellpadding="0" cellspacing="0" border="0" class="responsive < ?php if (count($allDomains)) { ?> dynamicTable < ?php } ?>display table table-bordered" width="100%">-->
                       <table cellpadding="0" cellspacing="0" border="0" id="sTable" class="responsive display table table-bordered" width="100%">

                <thead>
                    <tr>
                        <th title="Domain Name" class="tip">Domain Name</th>
                        <th title="Original Owner" class="tip">O.O</th>
                        <th title="Domain Assigned To" class="tip">A.T</th>
                        <th title="Page Rank" class="tip">PR</th>
                        <th title="Citation Flow" class="tip">CF</th>
                        <th title="Trust Flow" class="tip">TF</th>
                        <th title="Link Juice Page Rank" class="tip">LJPR</th>
                        <th title="SEOMoz Rank" class="tip">SM.R</th>
                        <th title="External Back Links" class="tip">BL</th>
                        <th title="What Type of CMS Using this Domain" class="tip">CMS / User / Pass</th>
                        <th title="Necessary Action" class="tip">Actions</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>
    </div><!-- End .span6 -->
</div>



<script type="text/javascript">

    $(document).ready(function() {
        $('#sTable').dataTable( {
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",

            "iDisplayLength": 50,
            "aLengthMenu": [ 10, 25, 50, 75, 100, 250, 500 ],
            "bPaginate": true,
            "aoColumns": [
                { "sType": "html"},
                { "sType": "html"},
                { "sType": "html"},
                { "sType": "html"},
                { "sType": "html"},
                { "sType": "html"},
                { "sType": "html"},
                { "sType": "html"},
                { "sType": "html"},
                { "sType": "html"},
                { "sType": "html"},
            ],
            "sAjaxSource": "./getdomainsajax",
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            }
        } );
        //top tooltip
    } );
</script>
