<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts.Email.html
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>

    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" valign="top" bgcolor="#726627" style="background-color:#D5D5D5;"><br/>
                    <table width="600" border="0" cellspacing="0" cellpadding="0">

                        <tr>
                            <td align="left" valign="top">
                                <!--<img src="#" width="600"  style="display:block;">-->
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" bgcolor="#006c00" style="background-color:#8b5f60; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                                    <tr>
                                        <td width="50%" align="left" valign="top" style="color:#ffffff; font-family:Verdana, Geneva, sans-serif; font-size:11px;">&nbsp;&nbsp;<?php echo date("d M Y, g:i a"); ?> </td>
                                        <td align="right" valign="top" style="color:#ffffff; font-family:Verdana, Geneva, sans-serif; font-size:11px;">From RankRatio&nbsp;&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:12px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="2" style="margin-bottom:10px;">
                                    <tr>
                                        <td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;">
                                            <div style="font-size:22px; color:#006d00;"></div>
                                            <?php echo $content_for_layout; ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" bgcolor="#006c00" style="background-color:#8b5f60;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="15">
                                    <tr>
                                        <td align="left" valign="top" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:13px;">
                                            RankRatio Team<br/>
                                            Email: <a href="mailto:support@rankratio.com" style="color:#ffffff; text-decoration:none;">support@rankratio.com </a><br/>
                                            Website: <a href="http://www.rankratio.com" target="_blank" style="color:#ffffff; text-decoration:none;">www.rankratio.com</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
