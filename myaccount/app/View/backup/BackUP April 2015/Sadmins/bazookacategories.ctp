<div class="marginB10"></div>
<div class="page-header">
    <h4>Allowed Bazooka Categories for the Clients</h4>
    <p>Check or uncheck the checkbox to enable or disable the corresponding Bazooka categories.</p>
</div>
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'bazookacategories'), 'method' => 'post')); ?>
    <?php
    if (isset($bazookaCats) && $bazookaCats):
        foreach ($bazookaCats as $k => $bCat):
            ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <span class="span3"></span>
                        <div class="span1" style="width: 30px;">
                            <?php echo $this->Form->checkbox('Bazooka.status.' . $bCat['Bazooka']['id'], array('title' => 'Check to exclude from package', 'required' => false, 'div' => false, 'label' => false, 'class' => '')); ?>
                        </div>                                    
                        <div class="span6">
                            <label class="" for="packagename"><?php echo $bCat['Bazooka']['name']; ?></label>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        endforeach;
    endif;
    ?>    
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Save Selected</button>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>