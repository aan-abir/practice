<div class="marginB10"></div>
<div class="page-header">
    <h4>All the topic for Education</h4>
    <p>all the topics for education. Based on Category</p>

</div>


<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>Category</th>
                <th>For Packages</th>
                <th>Title (500 chars)</th>
                <th>Body (500 chars)</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                //pr($allPackages);
                if(isset($allLesson) && count($allLesson) > 0):
                    foreach($allLesson as $arr):
                    ?>
                    <tr>
                        <td><?php  
                         $path = $this->requestAction('/sadmins/getcategorypath/'.$arr['Category']['id']);
                         $p =  '';
                         $i = 1;
                         if ( count($path) ) {
                             foreach ( $path as $k => $ph ) {
                                 $p .= $ph['Category']['name']. ($i != count($path) ?  " &rArr; " : "") ;
                                 $i++;
                             }
                         }
                         echo $p;
                        
                        ?></td>
                        <td><?php 
                            $for =  explode(',', trim(trim($arr['Education']['packages']),',')); 
                            foreach ( $for as $k => $p ) {
                                echo "&rArr; ". $allPackages[$p].'.<br>'; 
                            }
                        
                        ?></td>
                        <td><?php echo substr( strip_tags($arr['Education']['title']),0,500); ?></td>
                        <td><?php echo substr( strip_tags($arr['Education']['body']),0,500); ?></td>
                        <td>
                            <div class="controls center">
                                <a href="<?php echo Router::url('editlesson/' . $arr['Education']['id']); ?>" title="Edit Instruction" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                <a href="<?php echo Router::url('deletelesson/' . $arr['Education']['id']); ?>" title="Remove Instruction?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="7">No Education topic Found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
</div>
       
