<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Linkemperor Order Count for <?php echo date('M Y'); ?></span>
                </h4>
            </div>
            <div class="content">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allData)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th class="textRight">Month Year</th>
                            <th class="textLeft">Blast Type</th>
                            <th class="textRight">Blast Count</th>
                            <th class="textRight">Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($allData) && count($allData) > 0):
                            foreach ($allData as $k => $value):
                                $arr = $value[0];
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td class="textRight"><?php echo date('M Y', strtotime($arr['yr_mnth'])); ?></td>
                                    <td class="textLeft"><?php echo $value['Linkemperor']['order_type'] ? 'Linkemperor Blast' : 'Bazooka Blast'; ?></td>
                                    <td class="textRight"><?php echo $arr['order_count']; ?></td>
                                    <td class="textRight"><?php echo '$' . number_format(($value['Linkemperor']['order_type'] ? $arr['order_count'] * 9 : $arr['order_count'] * 10), 2); ?></td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="3">No Record Found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div>