<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span4', 'autofocus'); ?>
<div class="marginB10"></div>
<div class="page-header">
    <h4>Edit Modules</h4>

    <p>to edit a module please fill up the field below.</p>
</div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php // echo $this->Form->create('Module', array('inputDefaults' => $inputDefaults, 'method' => 'post', 'type' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>

            <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'editmodule', $data['Module']['id']), 'id' => 'addInstructionForm', 'method' => 'post', 'type'=>'file', 'class' => 'form-horizontal seperator'));

                echo $this->form->hidden('Module.id', array('value' => $data['Module']['id'])); ?>


            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Module Name <span class="help-block-inline">will be shown at client left nav</span></h4>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <?php echo $this->Form->hidden('id'); ?>
                        <?php echo $this->Form->input('Module.name', array('div' => false, 'label' => false, 'class' =>'span12', 'placeholder' => "Module Name",'required' => true)); ?>
                    </div>
                </div>
            </div>




            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Enter Expert Name <span class="help-block-inline">module expert details</span></h4>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <?php echo $this->Form->input('Module.ename', array('div' => false, 'label' => false,'class' => 'span12', 'placeholder' => 'Module Expert Name','required' => true)); ?>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Enter Expert Photo <span class="help-block-inline">module expert photo</span></h4>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <?php echo $this->Form->input('Module.photos', array('div' => false, 'label' => false, 'type' => 'file', 'class' => 'span3')); ?>
                        <input type="checkbox" name="data[Module][delOld]" id="delOld" title="Delete Old One" /> Delete Old File (<span class="help-inline blue">uploading new photo will remove old one </span>)
                    </div>
                    <?php 
                        if ( $data['Module']['photo'] != '' ){ ?>
                        <a title="Image Preview" class="fancybox tip" href="<?php echo  '/myaccount/moduleexpert/' . $data['Module']['photo']; ?>">
                            <img width="100" alt="Preview" src="<?php echo  '/myaccount/moduleexpert/' . $data['Module']['photo']; ?>" />
                        </a>
                        <?php
                    } ?>

                </div>
            </div>






            <!-- <div class="row-fluid">
            <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
            <h4 style="color: #074f14;">Embed File <span class="help-block-inline">embed file name</span></h4>
            </div>
            </div>
            </div>


            <div class="form-row row-fluid">
            <div class="span12">
            <div class="row-fluid">
            <div>
            < ?php echo $this->Form->input('lesson_embed', array('type' =>'text', 'class' => 'span6', 'placeholder' => 'Lesson Embed File')); ?>
            <span class="help-inline blue">Exp: http://www.youtube.com/watch?v=y_wzGwPWT84</span></div>
            </div>
            </div>
            </div>-->



            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Choose Order and release date <span class="help-block-inline">enter date and order</span></h4>
                    </div>
                </div>
            </div>


            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="ModuleMenuOrder">Menu Order:</label>
                        <?php echo $this->Form->input('Module.menu_order', array('div' => false, 'label' => false, 'type' =>'text', 'class' => 'span1', 'id' => 'ModuleMenuOrder')); ?>
                    </div>
                </div>
            </div>




            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="release_date">Release Date:</label>

                        <div class="span6">
                            <?php echo $this->Form->input('Module.release_date', array('div' => false, 'label' => false, 'type' =>'text', 'class' => 'span8', 'id' => 'release_date' ) ); ?>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Module Title <span class="help-block-inline">enter module title</span></h4>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="form-row">
                        <?php echo $this->Form->input('Module.title', array('div' => false, 'label' => false, 'title' => 'Module Title', 'placeholder' => "Module Title", 'class' => 'span12 tip')); ?>
                    </div>
                </div>
            </div>



            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Enter Details <span class="help-block-inline">details on this module</span></h4>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="form-row">
                        <?php echo $this->Form->textarea('Module.content', array('div' => false, 'label' => false, 'required' => false, 'class' => 'span12 tinymce', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:300px;')); ?>
                    </div>
                </div>
            </div>


            <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 10px !important;">
                        <h4 style="color: #074f14;">Module Status <span class="help-block-inline">inactive module will show as grayed</span></h4>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12 inline_labels">
                    <label class="form-label span3" for="MenuStatus">Status:</label>

                    <div class="span7">
                        <?php echo $this->Form->radio('Module.status', array('1' => 'Active', '0' => 'Inactive'), array('div' => false, 'label' => false, 'legend' => false, 'default' => '1')); ?>
                    </div>
                </div>
            </div>
            <div class="marginB10"></div>


            <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="submit" class="btn btn-info marginR10">Save Menu / Module</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <!-- End .span12 -->
    </div>
</div>
<div class="marginB10">
    <?php echo $this->element('tinymce'); ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {  

        $("#release_date").datepicker({
            dateFormat: "yy-mm-dd",
            minDate: '0',
            maxDate:'+6M',
        });

    });
    </script>