<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span4', 'autofocus'); ?>
<div class="marginB10"></div>
<div class="page-header">
    <h4>Add New Menu</h4>

    <p>Please enter your required values to add a new Menu.</p>
</div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create('Menu', array('inputDefaults' => $inputDefaults, 'method' => 'post', 'type' => 'post', 'class' => 'form-horizontal seperator')); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="MenuParentId">Parent Menu:</label>

                        <div class="span4">
                            <?php echo $this->Form->hidden('id'); ?>
                            <?php echo $this->Form->select('parent_id', $menus, array('empty' => 'No Parent')); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="MenuName">Menu Name:</label>
                        <?php echo $this->Form->input('name', array('placeholder' => "Menu Name")); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="MenuMenuOrder">Menu Order:</label>
                        <?php echo $this->Form->input('menu_order', array('class' => 'span1')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12 inline_labels">
                    <label class="form-label span3" for="MenuStatus">Status:</label>

                    <div class="span7">
                        <?php echo $this->Form->radio('status', array('1' => 'Active', '0' => 'Inactive'), array('legend' => false, 'default' => '1')); ?>
                    </div>
                </div>
            </div>
            <div class="marginB10"></div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="submit" class="btn btn-info marginR10">Save Menu</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->Form->end(); ?>
        </div>
        <!-- End .span12 -->
    </div>
</div>

<div class="marginB10"></div>


<div class="row-fluid">
    <div class="span12">

        <div class="box">
            <div class="title">
                <h4>
                    <span>Your Menu List</span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                    <tr>
                        <th class="zeroWidth"></th>
                        <th class="textLeft">Name</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    if (isset($items) && count($items) > 0):
                        foreach ($items as $item):
                            $arr = $item['top'];
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td class="textLeft"><?php echo $arr['name']; ?></td>
                                <td><?php echo $arr['status'] ? 'Active' : 'Inactive'; ?></td>
                                <td>
                                    <div class="controls center">
                                        <a href="<?php echo Router::url('menus/' . $arr['id']); ?>"
                                           title="Edit Menu" class="tip"><span
                                                class="icon12 icomoon-icon-pencil"></span></a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            if (isset($item['sub']) && $item['sub']):
                                foreach ($item['sub'] as $sub):
                                    ?>
                                    <tr>
                                        <td class="zeroWidth"></td>
                                        <td class="textLeft">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <?php echo $sub['name']; ?></td>
                                        <td><?php echo $arr['status'] ? 'Active' : 'Inactive'; ?></td>
                                        <td>
                                            <div class="controls center">
                                                <a href="<?php echo Router::url('menus/' . $sub['id']); ?>"
                                                   title="Edit Menu" class="tip"><span
                                                        class="icon12 icomoon-icon-pencil"></span></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                endforeach;
                            endif;
                        endforeach;
                    else:
                        echo '<tr><td colspan="5">No record found!</td></tr>';
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>

        </div>
        <!-- End .box -->

    </div>
    <!-- End .span6 -->
</div>