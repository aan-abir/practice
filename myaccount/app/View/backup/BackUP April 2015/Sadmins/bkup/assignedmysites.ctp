<div class="marginB10"></div>
<div class="page-header">
    <h4>All Assigned MySites</h4>
</div>
<div class="row-fluid">
    <div class="span12">

        <div class="content clearfix">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($clientList)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th style="width: 20px;"></th>
                        <th style="text-align: left;">Domain</th>
                        <th>Client Name</th>
                        <th>PR</th>
                        <th>CF</th>
                        <th>TF</th>
                        <th>LJPR</th>
                        <th>SM.R</th>
                        <th>BL</th>                        
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($clientList) > 0):
                        foreach ($clientList as $arr):
                        //pr($arr);exit;
                            ?>
                            <tr>
                                <td></td>
                                <td style="text-align: left;"><?php echo $arr['Domain']['domain']; ?></td>
                                <td><a target="_blank" href="<?php echo Router::url('users/letmeinwithoutanyusernameandpassword/' . $arr['User']['id']); ?>" title="Login into User Account" class="tip"><?php echo $arr['User']['fullname']; ?></a></td>
                                <td><?php echo $arr['Domain']['pr']; ?></td>
                                <td><?php echo $arr['Domain']['cflow']; ?></td>
                                <td><?php echo $arr['Domain']['tflow']; ?></td>
                                <td><?php echo $arr['Domain']['ljpr']; ?></td>
                                <td><?php echo $arr['Domain']['seomoz_rank']; ?></td>
                                <td><?php echo $arr['Domain']['ext_backlinks']; ?></td>
                                <td><?php echo $arr['Domain']['status'] == 1 ? 'Active' : 'Inactive'; ?></td>                                
                                <td>
                                    <div class="controls center">
                                        <a href="<?php echo Router::url('unassignmysite/') . $arr['Domain']['id']; ?>" title="Unassign Domain?" class="tip callAction">Unassign</a> | 
                                        <a href="<?php echo Router::url('clientmysites/') . $arr['User']['id']; ?>" title="View Client Mysites" class="tip">Client's Exclusive</a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="9">No Assigned Domains Found!</td>
                        </tr>
                    <?php
                    endif;
                    ?>
                </tbody>
            </table>
        </div>

    </div><!-- End .span6 -->
</div>
