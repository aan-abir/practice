<div class="marginB10"></div>
<div class="page-header">
    <h4>All Problematic ( NOT working) domains in RankRatio Network</h4>
    <p>domains listed below are not working. Article not posting anymore. Possible reasons ares - 1) Expired. 2) Root not working. 3) Database Connection Error. 4) Server Went Down etc</p>

</div>
<div class="row-fluid">
    <div class="span12">

        <div class="content">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($allDomains)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th title="Domain Name" class="tip">Domain Name</th>
                        <th title="Page Rank" class="tip">PR</th>
                        <th title="Citation Flow" class="tip">CF</th>
                        <th title="Trust Flow" class="tip">TF</th>
                        <th title="SEOMoz Rank" class="tip">SM.R</th>
                        <th title="External Back Links" class="tip">BL</th>
                        <th title="Domain Expire Date" class="tip">E.D</th>
                        <th title="Super Admin Username for this domain" class="tip">User Name</th>
                        <th title="Super Admin Password for this domain" class="tip">Password</th>
                        <!--<th title="Necessary Action" class="tip">Actions</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (count($allDomains) > 0):
                            foreach ($allDomains as $arr):
                                $d = 'http://'. str_replace('http://','',$arr['Domain']['domain']);

                                $expired = strtotime($arr['Domain']['expires_on']) < time() ? ' style="color:red;"' : '';


                            ?>
                            <tr>
                                <td><a href="<?php echo $d; ?>" title="View Domain" target="_blank"><?php echo $arr['Domain']['domain']; ?></a></td>
                                <td><?php echo $arr['Domain']['pr']; ?></td>
                                <td><?php echo $arr['Domain']['cflow']; ?></td>
                                <td><?php echo $arr['Domain']['tflow']; ?></td>
                                <td><?php echo $arr['Domain']['seomoz_rank']; ?></td>
                                <td><?php echo $arr['Domain']['ext_backlinks']; ?></td>
                                <td<?php echo $expired; ?>><?php echo $arr['Domain']['expires_on']; ?></td>
                                <td><?php echo $arr['Domain']['ausername']; ?></td>
                                <td><?php echo $arr['Domain']['apassword']; ?></td>
                                <!--td>
                                    <div class="controls center">
                                        <a href="<?php echo Router::url('editdomain/' . $arr['Domain']['id']); ?>" title="Edit Domain" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                        <a href="<?php echo Router::url('deletedomain/' . $arr['Domain']['id']); ?>" title="Remove Domain?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                    </div>
                                </td-->
                            </tr>

                            <?php
                                endforeach;
                            else:
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="11">No Record Found!</td>
                        </tr>
                        <?php
                            endif;
                    ?>
                </tbody>
            </table>

        </div>
    </div><!-- End .span6 -->
</div>
