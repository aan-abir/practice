<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Edit recording/resource</h4>
    <p>add new recording or resource. you can use media short code in your body text.</p>
</div>

<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'editrecording',$data['Recording']['id']), 'id' => 'addEducationForm', 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>




    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Recording Basic Information <span class="help-block-inline">enter recording related information</span></h4>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span3">
            <div class="page-header">
                <h4>Year <span class="help-block-inline">select year this resouce recorded</span></h4>
            </div>
            <div class="form-row">
                <div class="row-fluid">
                    <div class="span8">
                        <?php
                            $year = array('2012' => '2012','2013'=>'2013','2014' => '2014');
                            echo $this->Form->select('Recording.ryear', array($year), array('error' => false, 'required' => false, 'id' => 'ryear', 'div' => false, 'class' => 'span2 select', 'empty' => false));
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="span3">
            <div class="page-header">
                <h4>Day <span class="help-block-inline">select Day this resouce recorded</span></h4>
            </div>
            <div class="form-row">
                <div class="row-fluid">
                    <div class="span8">
                        <?php
                            $day = array('1'=>'Day 1','2'=>'Day 2','3'=>'Day 3');
                            echo $this->Form->select('Recording.day_number', array($day), array('error' => false, 'required' => false, 'id' => 'day_number', 'div' => false, 'class' => 'span2 select', 'empty' => false));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="span3">
            <div class="page-header">
                <h4>Speaker Name <span class="help-block-inline">enter speaker name</span></h4>
            </div>
            <div class="form-row">
                <div class="row-fluid">
                    <div class="span8">

                        <?php echo $this->Form->input('Recording.speaker_name', array('error' => false, 'type' => 'text', 'id' => 'speaker_name', 'title' => 'youtube ID', 'placeholder' => "Speaker Name", 'div' => false, 'label' => false, 'required' => true, 'class' => 'span12 tip')); ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="span3">
            <div class="page-header">
                <h4>Speaker Avatar </h4>
            </div>
            <div class="form-row">
                <div class="row-fluid">
                    <div class="span3"><?php
                            if ( $data['Recording']['speaker_avatar'] != '' ) { ?>
                            <a style="display: inline;" title="Image Preview" class="fancybox tip" href="<?php echo  '/media/' . $data['Recording']['speaker_avatar']; ?>">
                                <img  style="display: inline;" height="30" width="30" alt="Preview" src="<?php echo  '/media/' . $data['Recording']['speaker_avatar']; ?>" />
                            </a>
                            <?php
                                echo '<a href="'.adminHome.'/deletemediafile/recordings/'.$data['Recording']['id'].'/speaker_avatar/'.$data['Recording']['speaker_avatar'].'" title="Delete this Avatar File - \''.$data['Recording']['speaker_avatar'].'?\'" style="margin-left:4px;color:#ff4455;" class="callAction tip"> Delete</a>';
                            }
                        ?>
                    </div>
                    <div class="span8">
                        <?php echo $this->Form->file('Recording.uspeaker_avatar', array('error' => false, 'id' => 'uspeaker_avatar', 'title' => 'Speaker Avatar', 'placeholder' => "Speaker Avatar", 'div' => false, 'label' => false, 'class' => 'span8')); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Recording Resource Information <span class="help-block-inline">enter recording resource information</span></h4>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Youtube ID  <span class="help-block-inline">enter youtube video id</span></h4>
            </div>
            <div class="form-row">
                <div class="row-fluid">
                    <div class="span3">
                        <?php echo $this->Form->input('Recording.ytvideo_id', array('error' => false, 'type' => 'text', 'id' => 'ytvideo_id', 'title' => 'youtube ID', 'placeholder' => "Youtube ID", 'div' => false, 'label' => false, 'required' => false, 'class' => 'span12 tip'));
                        ?>

                    </div>
                    <div class="span7" style="margin-left: 20px;">
                        <?php if ( $data['Recording']['ytvideo_id'] != ''  ) {
                            echo '<a style="inline" title="See the Youtube Video popup" class="fancybox-media tip" href="http://www.youtube.com/watch?v='.trim($data['Recording']['ytvideo_id']).'">'. $data['Recording']['ytvideo_id'].'</a>';
                        } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span6">
            <div class="page-header">
                <h4>MindMap File <br><span class="help-block-inline"> <?php if ( $data['Recording']['mindmap_file'] != ''  ) {
                        echo '<a title="Preview MindMap File - '.$data['Recording']['mindmap_file'].' " class="fancypdf tip"  href="/media/'.$data['Recording']['mindmap_file'].'">'.$data['Recording']['mindmap_file'].'</a><br><a href="'.adminHome.'/deletemediafile/recordings/'.$data['Recording']['id'].'/mindmap_file/'.$data['Recording']['mindmap_file'].'" title="Delete this Note File - \''.$data['Recording']['mindmap_file'].'?\'" style="margin-left:4px;color:#ff4455;" class="callAction tip"> Delete</a> ';
                    }else{
                        echo '<br>no files uploaded<br>';
                    } ?></span></h4>
            </div>
            <div class="form-row">
                <div class="row-fluid">
                    <div class="span8">
                        <?php echo $this->Form->file('Recording.umindmap_file', array('error' => false, 'id' => 'umindmap_file', 'title' => 'Upload MindMap File', 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="span6">
            <div class="page-header">
                <h4>Notes File <br>
                    <span class="help-block-inline"> <?php if ( $data['Recording']['notes_file'] != ''  ) {
                            echo '<a title="Preview MindMap File" class="fancypdf tip"  href="/media/'.$data['Recording']['notes_file'].'">'.$data['Recording']['notes_file'].' </a><br><a href="'.adminHome.'/deletemediafile/recordings/'.$data['Recording']['id'].'/notes_file/'.$data['Recording']['notes_file'].'" title="Delete this Note File - \''.$data['Recording']['notes_file'].'?\'" style="margin-left:4px;color:#ff4455;" class="callAction tip"> Delete</a>';
                        }else{
                        echo '<br>no files uploaded<br>';
                    } ?>
                    </span></h4>
            </div>
            <div class="form-row">
                <div class="row-fluid">
                    <div class="span12">
                        <?php echo $this->Form->file('Recording.unotes_file', array('error' => false, 'id' => 'unotes_file', 'title' => 'Select Notes File', 'div' => false, 'label' => false, 'class' => 'span8')); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span6">
            <div class="page-header">
                <h4>SlideShow File <br> <span class="help-block-inline">
                        <?php if ( $data['Recording']['slideshow_file'] != ''  ) {
                            echo '<a title="Click to open Slide Show File" target="_blank" class="tip"  href="http://seonitrov2.seonitro.com/media/'.$data['Recording']['slideshow_file'].'">'.$data['Recording']['slideshow_file'].'</a><br> <a href="'.adminHome.'/deletemediafile/recordings/'.$data['Recording']['id'].'/slideshow_file/'.$data['Recording']['slideshow_file'].'" title="Delete this Note File - \''.$data['Recording']['slideshow_file'].'?\'" style="margin-left:4px;color:#ff4455;" class="callAction tip"> Delete</a>';
                        }else{
                        echo '<br>no files uploaded<br>';
                    } ?>
                    </span></h4>
            </div>
            <div class="form-row">
                <div class="row-fluid">
                    <div class="span12">
                        <?php echo $this->Form->file('Recording.uslideshow_file', array('error' => false, 'id' => 'slideshow_file', 'title' => 'Select Slide Show File', 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                    </div>

                </div>
            </div>
        </div>

         <div class="span6">
            <div class="page-header">
                <h4>Recipe File <br><span class="help-block-inline"> <?php if ( $data['Recording']['recipe_file'] != ''  ) {
                        echo '<a title="Preview Recipe File - '.$data['Recording']['recipe_file'].' " class="fancypdf tip"  href="/media/'.$data['Recording']['recipe_file'].'">'.$data['Recording']['recipe_file'].'</a><br><a href="'.adminHome.'/deletemediafile/recordings/'.$data['Recording']['id'].'/recipe_file/'.$data['Recording']['recipe_file'].'" title="Delete this Recipe File - \''.$data['Recording']['recipe_file'].'?\'" style="margin-left:4px;color:#ff4455;" class="callAction tip"> Delete</a> ';
                    }else{
                        echo '<br>no files uploaded<br>';
                    } ?></span></h4>
            </div>
            <div class="form-row">
                <div class="row-fluid">
                    <div class="span8">
                        <?php echo $this->Form->file('Recording.urecipe_file', array('error' => false, 'id' => 'urecipe_file', 'title' => 'Upload Recipe File', 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Recording Title and Description <span class="help-block-inline">enter recording title and description</span></h4>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Title <span class="help-block-inline"> this will show as topic header</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Recording.title', array('error' => false, 'type' => 'text', 'id' => 'title', 'title' => 'Resource Title', 'placeholder' => "Resource Title", 'div' => false, 'label' => false, 'required' => true, 'class' => 'span12 tip')); ?>
            </div>
        </div>
    </div>


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Enter Description here  <span class="help-block-inline"> html enabled help text. Be aware about html tag completion</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('Recording.description', array('error' => false, 'id' => 'description', 'title' => 'description', 'div' => false, 'label' => false, 'class' => 'span12 tinymce', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:500px;' ) ); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle" type="submit">Save Resouce</button>
    </div>

    <?php $this->Form->end(); ?>
</div>

<script type="text/javascript">
    //--------------- Tinymce ------------------//
    $('textarea.tinymce').tinymce({
        // Location of TinyMCE script
        script_url :  '<?php echo BASEURL; ?>plugins/forms/tiny_mce/tiny_mce.js',

        // General options
        theme : "advanced",
        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,styleselect,formatselect,fontselect,fontsizeselect,code,",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "css/main.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",
        extended_valid_elements : "iframe[src|width|height|name|align|frameborder|allowfullscreen]",

        // Replace values for the template plugin
        template_replace_values : {
            username : "SuprUser",
            staffid : "991234"
        }
    });


    $(".fancypdf").click(function(){
        $.fancybox({
            type: 'html',
            autoSize: false,
            content: '<embed src="'+this.href+'#nameddest=self&page=1&view=FitH,0&zoom=80,0,0" type="application/pdf" height="99%" width="100%" />',
            beforeClose: function() {
                $(".fancybox-inner").unwrap();
            }
        }); //fancybox
        return false;
    }); //click

</script>

