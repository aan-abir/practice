<div class="marginB10"></div>
<div class="page-header">
    <h4>Add New Menu</h4>
    <p>Manage your Menu below.</p>
</div>

<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th class="zeroWidth"></th>
                <th class="textLeft">Name</th>
                <th class="textLeft">Title</th>
                <th class="textLeft">Content</th>
                <th>Status</th>
                <th style="width: 100px !important;">Actions</th>
            </tr>
        </thead>
        <tbody>

            <?php
                if (isset($items) && count($items) > 0):
                    foreach ($items as $item):
                        $arr = $item['top'];
                    ?>
                    <tr>
                        <td class="zeroWidth"></td>
                        <td class="textLeft"><?php echo $arr['name']; ?></td>
                        <td class="textLeft"><?php echo $arr['title']; ?></td>
                        <td class="textLeft"><?php echo nl2br($arr['content']); ?></td>
                        <td><?php echo $arr['status'] ? 'Active' : 'Inactive'; ?></td>
                        <td>
                            <div class="controls center">


                                <a href="<?php echo Router::url('menus/' . $arr['id']); ?>"title="Edit Menu" class="tip">edit</a> | 
                                <a href="<?php echo Router::url('deletemenus/' . $arr['id']); ?>" title="Delete Menu" class="callAction">delete</a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        if (isset($item['sub']) && $item['sub']):
                            foreach ($item['sub'] as $sub):
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td class="textLeft">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <?php echo $sub['name']; ?></td>
                                <td class="textLeft"><?php echo $arr['title']; ?></td>
                                <td class="textLeft"><?php echo nl2br($arr['content']); ?></td>
                                <td><?php echo $arr['status'] ? 'Active' : 'Inactive'; ?></td>
                                <td>
                                    <div class="controls center">
                                        <a href="<?php echo Router::url('addmenu/' . $sub['id']); ?>"title="Edit Menu" class="tip">edit</a> | 
                                        <a href="<?php echo Router::url('deletemenus/' . $sub['id']); ?>" title="Delete Menu" class="callAction">delete</a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                                endforeach;
                            endif;
                        endforeach;
                    else:
                ?>
                <tr>
                    <td class="zeroWidth"></td>
                    <td colspan="5">No record found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
            </div>
