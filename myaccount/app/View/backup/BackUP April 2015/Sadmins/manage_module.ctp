<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span4', 'autofocus'); ?>
<div class="marginB10"></div>
<div class="page-header">
    <h4>Manage Modules</h4>

    <p>manage modules from the list below.</p>
</div>

<div class="row-fluid">
    <div class="span12">

        <div class="box">
            <div class="title">
                <h4>
                    <span>Your Module List</span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                    <tr>
                        <th class="zeroWidth"></th>
                        <th class="textLeft">Name</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    if (isset($items) && count($items) > 0):
                        foreach ($items as $item):
                            $arr = $item['Module'];
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td class="textLeft"><?php echo $arr['name']; ?></td>
                                <td><?php echo $arr['status'] ? 'Active' : 'Inactive'; ?></td>
                                <td>
                                    <div class="controls center">
                                        <a href="<?php echo Router::url('add_module/' . $arr['id']); ?>"
                                           title="Edit Menu" class="tip"><span
                                                class="icon12 icomoon-icon-pencil"></span></a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <tr>
                            <td class="zeroWidth"></td>
                            <td colspan="3">No record found!</td>
                        </tr>
                    <?php
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>

        </div>
        <!-- End .box -->

    </div>
    <!-- End .span6 -->
</div>