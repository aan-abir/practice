<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Add Welcome Video and/or News</h4>
    <p>you can upload a welcome video or news over here.</p>

</div>


<div class="content">
    <?php
        if(isset($data) && isset($data['Welcomenews'])):
        ?>
        <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'welcomenews', $data['Welcomenews']['id']), 'id' => 'addInstructionForm', 'method' => 'post', 'type'=>'file', 'class' => 'form-horizontal seperator')); ?>
        <?php echo $this->form->hidden('Welcomenews.id', array('value' => $data['Welcomenews']['id'])); ?>


 <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
            <div class="form-row row-fluid">
                <div class="span12 inline_labels">
                    <label class="form-label span3" for="MenuStatus">Status:</label>

                    <div class="span7">
                        <?php echo $this->Form->radio('Welcomenews.status', array('1' => 'Active', '0' => 'Inactive'), array('legend' => false, 'default' => '1')); ?>
                    </div>
                </div>
            </div>
            <div class="marginB10"></div>
            
            

        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="packagename">External Video Link</label>
                    <div>
                        <?php echo $this->Form->input('Welcomenews.externallink', array('error' => false, 'type' => 'text', 'id' => 'youtlink', 'title' => 'External Video Link', 'placeholder' => "External Video Link", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                        <span class="help-inline blue">Exp: http://www.youtube.com/watch?v=y_wzGwPWT84</span></div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="textarea">Upload Video</label>
                    <div>
                        <input type="file" name="data[Welcomenews][video]" id="Video" title="Upload Video" />
                        <input type="checkbox" name="data[Welcomenews][delOld]" id="delOld" title="Delete Old One" /> Delete Old File
                    </div>
                    <span class="help-inline blue">if added this will play to user as priority. mp4, wmv, flv file only [upload new video will remove old one]</span>
                </div>
            </div>
        </div>


        <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Enter News Text  <span class="help-block-inline"> html enabled help text. Be aware about html tag completion.</span></h4>
            </div>
            <div class="form-row">

                <?php echo $this->Form->textarea('Welcomenews.news', array('required' => false, 'error' => false, 'id' => 'news', 'title' => 'News text', 'div' => false, 'label' => false, 'class' => 'span4  tinymce', 'rows' => 3, 'cols' => '5', 'style' => 'width:98%;height:400px;' ) ); ?>
            </div>
        </div>
    </div>



    

    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle" type="submit">Save News</button>
    </div>

    <?php
        $this->Form->end();
        endif;
?>
</div>


<div class="marginB10">
    <?php echo $this->element('tinymce'); ?>
</div>


<script type="text/javascript" src="js/forms.js"></script><!-- Init plugins only for page -->