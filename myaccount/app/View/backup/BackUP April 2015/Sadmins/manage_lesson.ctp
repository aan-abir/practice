<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span4', 'autofocus'); ?>
<div class="marginB10"></div>
<div class="page-header">
    <h4>Manage Lesson</h4>

    <p>mange lesson from the list below.</p>
</div>


            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                    <tr>
                        <th class="zeroWidth"></th>
                        <th class="textLeft">Module</th>
                        <th class="textLeft">Title</th>
                        <th class="textLeft">Source</th>
                        <th class="textLeft">Duration</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    if (isset($items) && count($items) > 0):
                        foreach ($items as $item):
                            $arr = $item['Lesson'];
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td class="textLeft"><?php echo $item['Module']['name']; ?></td>
                                <td class="textLeft"><?php echo $arr['title']; ?></td>
                                <td class="textLeft"><?php echo $arr['source']; ?></td>
                                <td class="textLeft"><?php echo $arr['lesson_length']; ?></td>
                                <td><?php echo $arr['status'] ? 'Active' : 'Inactive'; ?></td>
                                <td>
                                    <div class="controls center">
                                        <a href="<?php echo Router::url('add_lesson/' . $arr['id']); ?>"  title="Edit Lesson" class="tip">edit</a> | 
                                         <a href="<?php echo Router::url('deletelessons/' . $arr['id']); ?>" title="Delete Lesson" class="callAction">delete</a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <tr>
                            <td class="zeroWidth"></td>
                            <td colspan="3">No record found!</td>
                        </tr>
                    <?php
                    endif;
                    ?>
                    </tbody>
                </table>
            </div>
