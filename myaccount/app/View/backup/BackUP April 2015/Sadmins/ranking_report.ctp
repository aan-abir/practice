<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Current Ranking Report | <?php echo $this->Html->link('Export to CSV', 'ranking_report_export'); ?></span>
                </h4>
            </div>
            <div class="content">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($itemList)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th class="textLeft">Name</th>
                            <th class="textLeft">Keyword</th>
                            <th class="textLeft">URL</th>
                            <th>Ranking</th>
                            <th>Engine</th>
                            <th class="textLeft">Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($itemList) && count($itemList) > 0):
                            foreach ($itemList as $arr):
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td class="textLeft"><?php echo str_replace(' ', '&nbsp;', $arr['fullname']); ?></td>
                                    <td class="textLeft"><?php echo $arr['keyword']; ?></td>
                                    <td class="textLeft"><?php echo $arr['url']; ?></td>
                                    <td><?php echo $arr['ranking']; ?></td>
                                    <td><?php echo ucfirst($arr['engine']); ?></td>
                                    <td class="textLeft"><?php echo $arr['email']; ?></td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="6">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div>