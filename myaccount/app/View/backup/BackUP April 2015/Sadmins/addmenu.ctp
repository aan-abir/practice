<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span4', 'autofocus'); ?>
<div class="marginB10"></div>
<div class="page-header">
    <h4>Add New Menu</h4>

    <p>to add a new menu please fill up the field below.</p>
</div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create('Menu', array('inputDefaults' => $inputDefaults, 'method' => 'post', 'type' => 'post', 'class' => 'form-horizontal seperator')); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="MenuParentId">Parent Menu:</label>

                        <div class="span4">
                            <?php echo $this->Form->hidden('id'); ?>
                            <?php echo $this->Form->select('parent_id', $menus, array('empty' => 'No Parent')); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="MenuName">Menu Name:</label>
                        <?php echo $this->Form->input('name', array('placeholder' => "Menu Name")); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="MenuMenuOrder">Menu Order:</label>
                        <?php echo $this->Form->input('menu_order', array('class' => 'span1')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12 inline_labels">
                    <label class="form-label span3" for="MenuStatus">Status:</label>

                    <div class="span7">
                        <?php echo $this->Form->radio('status', array('1' => 'Active', '0' => 'Inactive'), array('legend' => false, 'default' => '1')); ?>
                    </div>
                </div>
            </div>
            <div class="marginB10"></div>


            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="page-header">
                        <h4>Title <span class="help-block-inline"> this will show as topic header</span></h4>
                    </div>
                    <div class="form-row">
                        <?php echo $this->Form->input('title', array('title' => 'Menu Title', 'placeholder' => "Menu Title", 'class' => 'span12 tip')); ?>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header">
                        <h4>Enter Description here <span class="help-block-inline"> html enabled help text. Be aware about html tag completion</span>
                        </h4>
                    </div>
                    <div class="form-row">
                        <?php echo $this->Form->textarea('content', array('required' => false, 'class' => 'span12 tinymce', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:300px;')); ?>
                    </div>
                </div>
            </div>
            <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="submit" class="btn btn-info marginR10">Save Menu</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <!-- End .span12 -->
    </div>
</div>
<div class="marginB10">
    <?php echo $this->element('tinymce'); ?>
</div>