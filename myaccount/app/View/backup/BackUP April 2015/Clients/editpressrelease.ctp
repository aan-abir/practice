<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'editpressrelease', $itemId), 'id' => 'orderPrForm', 'method' => 'post', 'class' => 'form-horizontal seperator', 'type' => 'file')); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4><span class="red fs10">(required)</span> Headline <span class="help-block-inline">We recommend your headline be up to 80 characters, make sure your headline contains a subject, and active verb, and clearly states your news announcement</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.headline', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'We recommend your headline be up to 80 characters, make sure your headline contains a subject, and active verb, and clearly states your news announcement. ', 'div' => false, 'label' => false, 'class' => 'span12  tip')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4><span class="red fs10">(required)</span> Summary <span class="help-block-inline"> We recommend your summary be 1 - 2 sentences long , use the summary paragraph to your advantage - make it enticing enough to make readers read on.</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('Pressrelease.summary', array('error' => false, 'required' => false, 'id' => 'summary', 'title' => 'We recommend your summary be 1 - 2 sentences long , use the summary paragraph to your advantage - make it enticing enough to make readers read on.', 'div' => false, 'label' => false, 'class' => 'span12 tip', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:60px;')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Quote <span class="help-block-inline">if you quote anything then enter it here</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('Pressrelease.quote', array('error' => false, 'id' => 'quote', 'title' => 'Enter Quote Here', 'div' => false, 'label' => false, 'class' => 'span12 tip', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:60px;')); ?>
                <?php echo $this->Form->input('Pressrelease.quote_author', array('type' => 'text', 'id' => 'quote_author', 'required' => false, 'title' => 'Quote Author', 'div' => false, 'placeholder' => 'Quote Author', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4><span class="red small">(required)</span> Body: <span class="help-block-inline">body of your press release </span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('Pressrelease.body', array('error' => false, 'id' => 'body', 'required' => false, 'title' => 'Body of the press release', 'div' => false, 'label' => false, 'class' => 'span12 tinymce', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:200px;')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4> Video URL <span class="help-block-inline">If you'd like to add a Youtube video in your press release, please enter the Youtube video URL here. It can be found by clicking "Share" under your Youtube video when viewing it at Youtube.com. Example: http://youtu.be/nSt2CdGq3wk</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.video_url', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Video Url', 'div' => false, 'label' => false, 'class' => 'span12  tip')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4> Video Script Content <span class="help-block-inline">Typically we use the "Body" content to create a transcript for the announcer when producing a video for a press release; however, if you would like to use different copy for that transcript, please include it here.</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('Pressrelease.video_script', array('error' => false, 'id' => 'summery', 'title' => 'Video Script Url', 'div' => false, 'label' => false, 'class' => 'span12 tip', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:60px;')); ?>
            </div>
        </div>
    </div>    
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Image 1 <span class="help-block-inline">format: jpg|jpeg|png</span></h4>
            </div>
            <div class="form-row">
                <?php
                if ($data['Pressrelease']['image_1_file']):
                    echo $this->Html->image('/press_release/' . $data['Pressrelease']['image_1_file'], array('width' => '200px'));
                    echo '<br/><br/>';
                endif;
                ?>
                <input type="file" name="data[Pressrelease][image_1_file]" id="image_1_file" title="Image 1" /><br/>
                <?php echo $this->Form->input('Pressrelease.image_1_title', array('type' => 'text', 'id' => 'image_1_title', 'required' => false, 'title' => 'Image 1 Title', 'div' => false, 'placeholder' => 'Image 1 Title', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Image 2 <span class="help-block-inline">format: jpg|jpeg|png</span></h4>
            </div>
            <div class="form-row">
                <?php
                if ($data['Pressrelease']['image_2_file']):
                    echo $this->Html->image('/press_release/' . $data['Pressrelease']['image_2_file'], array('width' => '200px'));
                    echo '<br/><br/>';
                endif;
                ?>
                <input type="file" name="data[Pressrelease][image_2_file]" id="image_2_file" title="Image 2" /><br/>
                <?php echo $this->Form->input('Pressrelease.image_2_title', array('type' => 'text', 'id' => 'image_2_title', 'required' => false, 'title' => 'Image 2 Title', 'div' => false, 'placeholder' => 'Image 2 Title', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Image 3 <span class="help-block-inline">format: jpg|jpeg|png</span></h4>
            </div>
            <div class="form-row">
                <?php
                if ($data['Pressrelease']['image_3_file']):
                    echo $this->Html->image('/press_release/' . $data['Pressrelease']['image_3_file'], array('width' => '200px'));
                    echo '<br/><br/>';
                endif;
                ?>
                <input type="file" name="data[Pressrelease][image_3_file]" id="image_2_file" title="Image 3" /><br/>
                <?php echo $this->Form->input('Pressrelease.image_3_title', array('type' => 'text', 'id' => 'image_3_title', 'required' => false, 'title' => 'Image 3 Title', 'div' => false, 'placeholder' => 'Image 3 Title', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Image 4 <span class="help-block-inline">format: jpg|jpeg|png</span></h4>
            </div>
            <div class="form-row">
                <?php
                if ($data['Pressrelease']['image_4_file']):
                    echo $this->Html->image('/press_release/' . $data['Pressrelease']['image_4_file'], array('width' => '200px'));
                    echo '<br/><br/>';
                endif;
                ?>
                <input type="file" name="data[Pressrelease][image_4_file]" id="image_2_file" title="Image 4" /><br/>
                <?php echo $this->Form->input('Pressrelease.image_4_title', array('type' => 'text', 'id' => 'image_4_title', 'required' => false, 'title' => 'Image 4 Title', 'div' => false, 'placeholder' => 'Image 4 Title', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Attachment 1 <span class="help-block-inline">format: pdf|docx|xlsx</span></h4>
            </div>
            <div class="form-row">
                <?php
                if ($data['Pressrelease']['attachment_1_file']):
                    $filename = APP . 'webroot/press_release/' . $data['Pressrelease']['attachment_1_file'];
                    if (file_exists($filename)) {
                        echo $this->Html->link('Current File', array('controller' => 'clients', 'action' => 'downloadFile', 'press_release', $data['Pressrelease']['attachment_1_file']));
                        echo '<br/>';
                    }
                endif;
                ?>                
                <input type="file" name="data[Pressrelease][attachment_1_file]" id="image_2_file" title="Attachment 1" /><br/>
                <?php echo $this->Form->input('Pressrelease.attachment_1_title', array('type' => 'text', 'id' => 'attachment_1_title', 'required' => false, 'title' => 'Attachment 1 Title', 'div' => false, 'placeholder' => 'Attachment 1 Title', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Attachment 2 <span class="help-block-inline">format: pdf|docx|xlsx</span></h4>
            </div>
            <div class="form-row">
                <?php
                if ($data['Pressrelease']['attachment_2_file']):
                    $filename = APP . 'webroot/press_release/' . $data['Pressrelease']['attachment_2_file'];
                    if (file_exists($filename)) {
                        echo $this->Html->link('Current File', array('controller' => 'clients', 'action' => 'downloadFile', 'press_release', $data['Pressrelease']['attachment_2_file']));
                        echo '<br/>';
                    }
                endif;
                ?>                 
                <input type="file" name="data[Pressrelease][attachment_2_file]" id="image_2_file" title="Attachment 2" /><br/>
                <?php echo $this->Form->input('Pressrelease.attachment_2_title', array('type' => 'text', 'id' => 'attachment_2_title', 'required' => false, 'title' => 'Attachment 2 Title', 'div' => false, 'placeholder' => 'Attachment 2 Title', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Attachment 3 <span class="help-block-inline">format: pdf|docx|xlsx</span></h4>
            </div>
            <div class="form-row">
                <?php
                if ($data['Pressrelease']['attachment_3_file']):
                    $filename = APP . 'webroot/press_release/' . $data['Pressrelease']['attachment_3_file'];
                    if (file_exists($filename)) {
                        echo $this->Html->link('Current File', array('controller' => 'clients', 'action' => 'downloadFile', 'press_release', $data['Pressrelease']['attachment_3_file']));
                        echo '<br/>';
                    }
                endif;
                ?>                
                <input type="file" name="data[Pressrelease][attachment_3_file]" id="image_2_file" title="Attachment 3" /><br/>
                <?php echo $this->Form->input('Pressrelease.attachment_3_title', array('type' => 'text', 'id' => 'attachment_3_title', 'required' => false, 'title' => 'Attachment 3 Title', 'div' => false, 'placeholder' => 'Attachment 3 Title', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Attachment 4 <span class="help-block-inline">format: pdf|docx|xlsx</span></h4>
            </div>
            <div class="form-row">
                <?php
                if ($data['Pressrelease']['attachment_4_file']):
                    $filename = APP . 'webroot/press_release/' . $data['Pressrelease']['attachment_4_file'];
                    if (file_exists($filename)) {
                        echo $this->Html->link('Current File', array('controller' => 'clients', 'action' => 'downloadFile', 'press_release', $data['Pressrelease']['attachment_4_file']));
                        echo '<br/>';
                    }
                endif;
                ?>                 
                <input type="file" name="data[Pressrelease][attachment_4_file]" id="image_2_file" title="Attachment 4" /><br/>
                <?php echo $this->Form->input('Pressrelease.attachment_4_title', array('type' => 'text', 'id' => 'attachment_4_title', 'required' => false, 'title' => 'Attachment 4 Title', 'div' => false, 'placeholder' => 'Attachment 4 Title', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4><span class="red fs10">(required)</span> Release Date <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.release_date', array('type' => 'text', 'id' => 'release_date', 'required' => false, 'title' => 'Release Date', 'div' => false, 'placeholder' => 'Release Date', 'label' => false, 'class' => 'span2 marginT10 tip datepicker', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Keword Tags <span class="help-block-inline">Comma separated list of up to 5</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.keyword_tags', array('type' => 'text', 'id' => 'keyword_tags', 'required' => false, 'title' => 'Keyword Tags', 'div' => false, 'placeholder' => 'Keyword Tags', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4><span class="red fs10">(required)</span> Industry <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <div class="span3">
                    <?php
                    $industryList = $this->NitroCustom->getIndustryList();
                    echo $this->Form->select('Pressrelease.industry', array($industryList), array('error' => false, 'required' => false, 'id' => 'industry', 'div' => false, 'class' => 'span12 select', 'empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4><span class="red fs10">(required)</span> Zip Code <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.zip_code', array('error' => false, 'type' => 'text', 'id' => 'zip_code', 'required' => false, 'title' => 'Zip Code', 'div' => false, 'placeholder' => 'Zip Code', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4><span class="red fs10">(required)</span> Name <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.contact_name', array('error' => false, 'type' => 'text', 'id' => 'contact_name', 'required' => false, 'title' => 'Contact Name', 'div' => false, 'placeholder' => 'Contact Name', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4><span class="red fs10">(required)</span> Email <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.contact_email', array('error' => false, 'type' => 'text', 'id' => 'contact_email', 'required' => false, 'title' => 'Contact Email', 'div' => false, 'placeholder' => 'Contact Email', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4><span class="red fs10">(required)</span> Phone <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.contact_phone', array('error' => false, 'type' => 'text', 'id' => 'contact_phone', 'required' => false, 'title' => 'Contact Phone', 'div' => false, 'placeholder' => 'Contact Phone', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4><span class="red fs10">(required)</span> Brand Name <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.brand_name', array('error' => false, 'type' => 'text', 'id' => 'brand_name', 'required' => false, 'title' => 'Brand Name', 'div' => false, 'placeholder' => 'Brand Name', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Brand Website <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.brand_website', array('error' => false, 'type' => 'text', 'id' => 'brand_website', 'required' => false, 'title' => 'Brand Website', 'div' => false, 'placeholder' => 'Brand Website', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Twitter URL <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.twitter_url', array('error' => false, 'type' => 'text', 'id' => 'twitter_url', 'required' => false, 'title' => 'Twitter URL', 'div' => false, 'placeholder' => 'Twitter URL', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Facebook URL <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.facebook_url', array('error' => false, 'type' => 'text', 'id' => 'facebook_url', 'required' => false, 'title' => 'Facebook URL', 'div' => false, 'placeholder' => 'Facebook URL', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>LinkedIn URL <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.linkedin_url', array('error' => false, 'type' => 'text', 'id' => 'linkedin_url', 'required' => false, 'title' => 'LinkedIn URL', 'div' => false, 'placeholder' => 'LinkedIn URL', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Google Plus URL <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.google_plus_url', array('error' => false, 'type' => 'text', 'id' => 'google_plus_url', 'required' => false, 'title' => 'Google Plus URL', 'div' => false, 'placeholder' => 'Google Plus URL', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>RSS URL <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Pressrelease.rss_url', array('error' => false, 'type' => 'text', 'id' => 'rss_url', 'required' => false, 'title' => 'RSS URL', 'div' => false, 'placeholder' => 'RSS URL', 'label' => false, 'class' => 'span8 marginT10 tip', 'style' => '')); ?>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header" style="margin: 0  !important;">
                <h4>Status <span class="help-block-inline"></span></h4>
            </div>
            <div class="form-row">
                <div class="span2">
                    <?php echo $this->Form->select('Pressrelease.status', array('draft' => 'Draft', 'active' => 'Active'), array('error' => false, 'id' => 'status', 'required' => false, 'div' => false, 'class' => 'span2 select', 'empty' => false)); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Order Press Release</button>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

<script type="text/javascript">
    //------------- Datepicker -------------//
    if ($('.datepicker').length) {
        $('.datepicker').datepicker({
            showOtherMonths: true,
            //dateFormat: 'yy-m-d'
            minDate: '+1'
        });
    }
</script>


<script type="text/javascript">
    //--------------- Tinymce ------------------//
    $('textarea.tinymce').tinymce({
        // Location of TinyMCE script
        script_url: '<?php echo BASEURL; ?>plugins/forms/tiny_mce/tiny_mce.js',
        // General options
        theme: "simple",
        plugins: "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
        // Theme options
        theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,styleselect,formatselect,fontselect,fontsizeselect,code,",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        // theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        //theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,
        // Example content CSS (should be your site CSS)
        content_css: "css/main.css",
        // Drop lists for link/image/media/template dialogs
        template_external_list_url: "lists/template_list.js",
        external_link_list_url: "lists/link_list.js",
        external_image_list_url: "lists/image_list.js",
        media_external_list_url: "lists/media_list.js",
        // Replace values for the template plugin
        template_replace_values: {
            username: "SuprUser",
            staffid: "991234"
        }
    });

</script>