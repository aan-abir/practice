<?php

if (isset($domainList) && !empty($domainList)):
    echo $this->Form->select('Blogroll.domain_id', array($domainList), array('required' => false, 'id' => 'BlogrollDomainId', 'title' => 'Select Domain', 'div' => false, 'label' => false, 'class' => 'select', 'empty' => false));
else:
    echo $this->Form->select('Blogroll.domain_id', array(array('' => 'No Domain Found')), array('required' => false, 'id' => 'BlogrollDomainId', 'title' => 'No Domains Found!', 'div' => false, 'label' => false, 'class' => 'select', 'empty' => false));
endif;
?>