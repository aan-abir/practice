<?php
$total_link = $this->NitroCustom->link_count('total_link', $data['Ldreport']['total_link']);
$dead_link = $this->NitroCustom->link_count('dead_link', $data['Ldreport']['dead_link']);

$external_link = $this->NitroCustom->link_type('external', $data['Ldreport']['id'], $data['Ldreport']['domain']);
$internal_link = $this->NitroCustom->link_type('internal', $data['Ldreport']['id'], $data['Ldreport']['domain']);
//pr($external_link); 
$dead_link_array = $this->NitroCustom->link_array($data['Ldreport']['dead_link']);
$total_link_array = $this->NitroCustom->link_array($data['Ldreport']['total_link']);
$header_array = $this->NitroCustom->link_array($data['Ldreport']['header']);

$follow_link = $this->NitroCustom->link_count('follow_link', $data['Ldreport']['follow_link']);
$do_follow = $total_link - $follow_link;

$RefDomains = $data['Ldreport']['RefDomains'];
$RefIPs = $data['Ldreport']['RefIPs'];
$RefSubNets = $data['Ldreport']['RefSubNets'];
$image_link = $this->NitroCustom->link_count('image_link', $data['Ldreport']['image_link']);

$c_block_to_reference_domain = $this->NitroCustom->link_ratio($RefDomains, $RefSubNets);
$do_follow_percentage = $this->NitroCustom->link_ratio($total_link, $do_follow);

$total_urls = $data['Ldreport']['GetTopBackLinksAnalysisResUnitsCost'];
$link_to_homepage = $this->NitroCustom->link_to_homepage($data['Ldreport']['id'], $data['Ldreport']['domain']);
$link_to_innerpage = $total_urls - $link_to_homepage;

$link_to_homepage_persentage = $this->NitroCustom->link_ratio($total_urls, $link_to_homepage);
$link_to_innerpage_persentage = $this->NitroCustom->link_ratio($total_urls, $link_to_innerpage);

$anchortext_graph = $this->NitroCustom->anchortext_graph($data['Ldreport']['id']);
$text_colorCode = array(
    '0' => '#754DEB',
    '1' => '#2473A6',
    '2' => '#F18800',
    '3' => '#88A72A',
    '4' => '#3F4857',
    '5' => '#DFD80C',
    '6' => '#851B1B',
    '7' => '#3E1B85',
    '8' => '#1B7285',
    '9' => '#FCD202',
    '10' => '#B0DE09',
    '11' => '#0D52D1',
    '12' => '#2A0CD0',
    '13' => '#8A0CCF',
    '14' => '#CD0D74',
);
$anchor_text_script = '';
$total_persent = 0;
foreach ($anchortext_graph as $text_index => $text) {
    $AnchorText = $text['ldreportcron']['AnchorText'];
    $total = $text[0]['total'];
    $text_color = $text_colorCode[$text_index];
    $text_per = $link_to_innerpage_persentage = $this->NitroCustom->link_ratio($total_urls, $total);
    $total_persent += $text_per;
    $anchor_text_script .= '{ label: "' . $AnchorText . '",  data: ' . $text_per . ', color: "' . $text_color . '"},';
}
/*
  //echo $total_persent; exit;
  $AnchorText = 'Other Links';
  $text_per = 35;//99 - $total_persent;
  $text_color = $text_colorCode[14];
  $anchor_text_script .= '{ label: "'.$AnchorText.'",  data: '.$text_per.', color: "'.$text_color.'"},';
  //echo $anchor_text_script; exit;
  // */
?>
<style>
     .zeroWidth{width:0 !important; padding:0 !important; border:0 !important;}
    .clear_both{clear: both;height:20px;}
    .span6{width: 23%;}
    .box_size{height: 130px;width:100%;}
    .bg_left{margin-left:20px;}
    .bg{height:20px;width:20px;float:left;margin-left:5px;}
    .bg1color{background-color: #1C95C4;}
    .bg2color{background-color: #F18800;}
    .text{margin-left:5px;font-weight: bold;float:left;width:50%;}
    .value{margin-left:-10px;float:left;width:30%;font-weight: bold;}
    .last_text{text-align: left;font-weight: bold;}
    [class*="span"]{margin-left: 13px;}
</style>
<script>
    // document ready function
    $(document).ready(function() { 	

        var divElement = $('div'); //log all div elements

        //Boostrap modal
        $('#myModal').modal({ show: false});
	
        //add event to modal after closed
        $('#myModal').on('hidden', function () {
            console.log('modal is closed');
        })

        //Simple chart 
        if (divElement.hasClass('simple-chart')) {
            $(function () {
                var sin = [], cos = [];
<?php
for ($inc = 0; $inc < count($header_array); $inc += 3) {
    //$active_link = intval($total_link_array[$inc]);
    $active_link = intval($total_link_array[$inc]) - intval($dead_link_array[$inc]);
    ?>
                        cos.push(['<?php echo date(strtotime(date("d M Y", strtotime($header_array[$inc]))) * 1000); ?>', <?php echo $dead_link_array[$inc]; ?>]);             
                        sin.push(['<?php echo date(strtotime(date("d M Y", strtotime($header_array[$inc]))) * 1000); ?>', <?php echo $active_link; ?>]);             
    <?php
}
?>
                //graph options
                var options = {
                    grid: {
                        show: true,
                        aboveData: true,
                        color: "#3f3f3f" ,
                        labelMargin: 5,
                        axisMargin: 0, 
                        borderWidth: 0,
                        borderColor:null,
                        minBorderMargin: 5 ,
                        clickable: true, 
                        hoverable: true,
                        autoHighlight: true,
                        mouseActiveRadius: 20
                    },
                    series: {
                        grow: {active: false},
                        lines: {
                            show: true,
                            fill: false,
                            lineWidth: 3,
                            steps: false
                        },
                        points: {
                            show:true,
                            radius: 5,
                            symbol: "circle",
                            fill: true,
                            borderColor: "#fff"
                        }
                    },
                    xaxis: {
                        mode: "time", timeformat: "%d/%m/%y", minTickSize: [3, "day"]
                    },
                    legend: { position: "se" },
                    colors: chartColours,
                    shadowSize:1,
                    tooltip: true, //activate tooltip
                    tooltipOpts: {
                        content: "%s : %y.3",
                        shifts: {
                            x: -30,
                            y: -50
                        }
                    }
                };  
                var plot = $.plot($(".simple-chart"),
                [{
                        label: "Active link", 
                        data: sin,
                        lines: {fillColor: "#f2f7f9"},
                        points: {fillColor: "#88bbc8"}
                    }, 
                    {	
                        label: "Delete link", 
                        data: cos,
                        lines: {fillColor: "#fff8f2"},
                        points: {fillColor: "#ed7a53"}
                    }], options);
            });
        }//end if
                       
        //Donut simple chart
        if (divElement.hasClass('simple-donut')) {
            $(function () {
                var data = [
                    { label: "USA",  data: 38, color: "#88bbc8"},
                    { label: "Brazil",  data: 23, color: "#ed7a53"},
                    { label: "India",  data: 15, color: "#9FC569"},
                    { label: "Turkey",  data: 9, color: "#bbdce3"},
                    { label: "France",  data: 7, color: "#9a3b1b"},
                    { label: "China",  data: 5, color: "#5a8022"},
                    { label: "Germany",  data: 3, color: "#2c7282"}
                ];

                $.plot($(".simple-donut"), data, 
                {
                    series: {
                        pie: { 
                            show: true,
                            innerRadius: 0.4,
                            highlight: {
                                opacity: 0.1
                            },
                            radius: 1,
                            stroke: {
                                color: '#fff',
                                width: 8
                            },
                            startAngle: 2,
                            combine: {
                                color: '#353535',
                                threshold: 0.05
                            },
                            label: {
                                show: true,
                                radius: 1,
                                formatter: function(label, series){
                                    return '<div class="pie-chart-label">'+label+'&nbsp;'+Math.round(series.percent)+'%</div>';
                                }
                            }
                        },
                        grow: {	active: false}
                    },
                    legend:{show:false},
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    tooltip: true, //activate tooltip
                    tooltipOpts: {
                        content: "%s : %y.1"+"%",
                        shifts: {
                            x: -30,
                            y: -50
                        }
                    }
                });
            });
        }//end if

        //Pie simple chart
        if (divElement.hasClass('simple-pie')) {
            $(function () {
                var data = [
                    { label: "C Blocks",  data: <?php echo $c_block_to_reference_domain; ?>, color: "#1C95C4"},
                    { label: "Duplicate Ips",  data: <?php echo 100 - $c_block_to_reference_domain; ?>, color: "#F18800"},
                ];

                $.plot($(".simple-pie"), data, 
                {
                    series: {
                        pie: { 
                            show: true,
                            highlight: {
                                opacity: 0.1
                            },
                            radius: 1,
                            stroke: {
                                color: '#fff',
                                width: 2
                            },
                            startAngle: 2,
                            combine: {
                                color: '#353535',
                                threshold: 0.05
                            },
                            label: {
                                show: true,
                                radius: 1,
                                formatter: function(label, series){
                                    //return '<div class="pie-chart-label">'+label+'&nbsp;'+Math.round(series.percent)+'%</div>';
                                    return '';
                                }
                            }
                        },
                        grow: {	active: false}
                    },
                    legend:{show:false},
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    
                    tooltip: true, //activate tooltip
                    tooltipOpts: {
                        content: "%s : %y.1"+"%",
                        shifts: {
                            x: -30,
                            y: -50
                        }
                    }
                });
            });
        }//end if
        
        //Pie simple chart
        if (divElement.hasClass('dofollow')) {
            $(function () {
                var data = [
                    { label: "Dofollow",  data: <?php echo $do_follow_percentage; ?>, color: "#1C95C4"},
                    { label: "Nofollow",  data: <?php echo 100 - $do_follow_percentage; ?>, color: "#F18800"},
                ];

                $.plot($(".dofollow"), data, 
                {
                    series: {
                        pie: { 
                            show: true,
                            highlight: {
                                opacity: 0.1
                            },
                            radius: 1,
                            stroke: {
                                color: '#fff',
                                width: 2
                            },
                            startAngle: 2,
                            combine: {
                                color: '#353535',
                                threshold: 0.05
                            },
                            label: {
                                show: true,
                                radius: 1,
                                formatter: function(label, series){
                                    //return '<div class="pie-chart-label">'+label+'&nbsp;'+Math.round(series.percent)+'%</div>';
                                    return '';
                                }
                            }
                        },
                        grow: {	active: false}
                    },
                    legend:{show:false},
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    tooltip: true, //activate tooltip
                    tooltipOpts: {
                        content: "%s : %y.1"+"%",
                        shifts: {
                            x: -30,
                            y: -50
                        }
                    }
                });
            });
        }//end if
        
        //Link to Homepage 
        if (divElement.hasClass('homepage')) {
            $(function () {
                var data = [
                    { label: "Homepage",  data: <?php echo $link_to_homepage_persentage; ?>, color: "#1C95C4"},
                    { label: "Innerpage",  data: <?php echo 100 - $link_to_homepage_persentage; ?>, color: "#F18800"},
                ];

                $.plot($(".homepage"), data, 
                {
                    series: {
                        pie: { 
                            show: true,
                            highlight: {
                                opacity: 0.1
                            },
                            radius: 1,
                            stroke: {
                                color: '#fff',
                                width: 2
                            },
                            startAngle: 2,
                            combine: {
                                color: '#353535',
                                threshold: 0.05
                            },
                            label: {
                                show: true,
                                radius: 1,
                                formatter: function(label, series){
                                    return '';
                                }
                            }
                        },
                        grow: {	active: false}
                    },
                    legend:{show:false},
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    tooltip: true, //activate tooltip
                    tooltipOpts: {
                        content: "%s : %y.1"+"%",
                        shifts: {
                            x: -30,
                            y: -50
                        }
                    }
                });
            });
        }//end if
        
        //Link to Homepage 
        if (divElement.hasClass('innerpage')) {
            $(function () {
                var data = [
                    { label: "Homepage",  data: <?php echo $link_to_homepage_persentage; ?>, color: "#1C95C4"},
                    { label: "Innerpage",  data: <?php echo 100 - $link_to_homepage_persentage; ?>, color: "#F18800"},
                ];

                $.plot($(".innerpage"), data, 
                {
                    series: {
                        pie: { 
                            show: true,
                            highlight: {
                                opacity: 0.1
                            },
                            radius: 1,
                            stroke: {
                                color: '#fff',
                                width: 2
                            },
                            startAngle: 2,
                            combine: {
                                color: '#353535',
                                threshold: 0.05
                            },
                            label: {
                                show: true,
                                radius: 1,
                                formatter: function(label, series){
                                    return '';
                                }
                            }
                        },
                        grow: {	active: false}
                    },
                    legend:{show:false},
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    tooltip: true, //activate tooltip
                    tooltipOpts: {
                        content: "%s : %y.1"+"%",
                        shifts: {
                            x: -30,
                            y: -50
                        }
                    }
                });
            });
        }//end if
        
        //Link to anchor Matrics 
        if (divElement.hasClass('anchormatrics')) {
            $(function () {
                var data = [ <?php echo $anchor_text_script; ?>];

                $.plot($(".anchormatrics"), data, 
                {
                    series: {
                        pie: { 
                            show: true,
                            highlight: {
                                opacity: 0.1
                            },
                            radius: 1,
                            stroke: {
                                color: '#fff',
                                width: 2
                            },
                            startAngle: 2,
                            combine: {
                                color: '#353535',
                                threshold: 0.05
                            },
                            label: {
                                show: true,
                                radius: 1,
                                formatter: function(label, series){
                                    //return '<div class="pie-chart-label">'+label+'&nbsp;'+Math.round(series.percent)+'%</div>';
                                    return '';
                                }
                            }
                        },
                        grow: {	active: false}
                    },
                    legend:{show:false},
                    grid: {
                        hoverable: true,
                        clickable: true
                    },
                    tooltip: true, //activate tooltip
                    tooltipOpts: {
                        content: "%s : %y.1"+"%",
                        shifts: {
                            x: -30,
                            y: -50
                        }
                    }
                });
            });
        }//end if


    });//End document ready functions

    //generate random number for charts
    randNum = function(){
        //return Math.floor(Math.random()*101);
        return (Math.floor( Math.random()* (1+40-20) ) ) + 20;
    }

    var chartColours = ['#88bbc8', '#ed7a53', '#9FC569', '#bbdce3', '#9a3b1b', '#5a8022', '#2c7282'];

    //sparkline in sidebar area
    var positive = [1,5,3,7,8,6,10];
    var negative = [10,6,8,7,3,5,1]
    var negative1 = [7,6,8,7,6,5,4]

    $('#stat1').sparkline(positive,{
        height:15,
        spotRadius: 0,
        barColor: '#9FC569',
        type: 'bar'
    });
    $('#stat2').sparkline(negative,{
        height:15,
        spotRadius: 0,
        barColor: '#ED7A53',
        type: 'bar'
    });
    $('#stat3').sparkline(negative1,{
        height:15,
        spotRadius: 0,
        barColor: '#ED7A53',
        type: 'bar'
    });
    $('#stat4').sparkline(positive,{
        height:15,
        spotRadius: 0,
        barColor: '#9FC569',
        type: 'bar'
    });
    //sparkline in widget
    $('#stat5').sparkline(positive,{
        height:15,
        spotRadius: 0,
        barColor: '#9FC569',
        type: 'bar'
    });

    $('#stat6').sparkline(positive, { 
        width: 70,//Width of the chart - Defaults to 'auto' - May be any valid css width - 1.5em, 20px, etc (using a number without a unit specifier won't do what you want) - This option does nothing for bar and tristate chars (see barWidth)
        height: 20,//Height of the chart - Defaults to 'auto' (line height of the containing tag)
        lineColor: '#88bbc8',//Used by line and discrete charts to specify the colour of the line drawn as a CSS values string
        fillColor: '#f2f7f9',//Specify the colour used to fill the area under the graph as a CSS value. Set to false to disable fill
        spotColor: '#e72828',//The CSS colour of the final value marker. Set to false or an empty string to hide it
        maxSpotColor: '#005e20',//The CSS colour of the marker displayed for the maximum value. Set to false or an empty string to hide it
        minSpotColor: '#f7941d',//The CSS colour of the marker displayed for the mimum value. Set to false or an empty string to hide it
        spotRadius: 3,//Radius of all spot markers, In pixels (default: 1.5) - Integer
        lineWidth: 2//In pixels (default: 1) - Integer
    });
</script>
<div class="clear_both"></div>
<h3>Base link matrix for <?php echo $data['Ldreport']['domain'] ?></h3>
<div class="clear_both"></div>

<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="content clearfix">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th><strong>Total Links</strong></th>
                        <th><strong>Deleted</strong></th>
                        <th><strong>NoFollow</strong></th>
                        <th><strong>Ref Domains </strong></th>
                        <th><strong>Ref IPs</strong></th>
                        <th><strong>Ref C Blocks</strong></th>
                        <th><strong>Images</strong></th>
                        <!--<th><strong>Redirects</strong></th>-->
                        <th><strong>Citation</strong></th>
                        <th><strong>Trust</strong></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php echo $total_link; ?>
                        </td>
                        <td><?php echo $dead_link; ?></td>
                        <td><?php echo $follow_link; ?></td>
                        <td><?php echo $RefDomains; ?></td>
                        <td><?php echo $RefIPs; ?></td>
                        <td><?php echo $RefSubNets; ?></td>
                        <td><?php echo $image_link; ?></td>
                        <!--<td></td>-->
                        <td><?php echo $data['Ldreport']['CitationFlow'] ?></td>
                        <td><?php echo $data['Ldreport']['TrustFlow'] ?></td>
                    </tr>           
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="clear_both"></div>
<!----------->
<ul id="myTab2" class="nav nav-tabs">
    <li class="active"><a onclick="$('#link_ratio').show();" href="#link_ratio" data-toggle="tab">Link Ratios</a></li>
    <li><a onclick="$('#link_ratio').hide();" href="#anchor_metrics" data-toggle="tab">Anchor Metrics</a></li>
</ul>
<!------------->
<div class="tab-pane fade in active" id="link_ratio">
    <div class="clear_both"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="content clearfix">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th><strong>Current Link Density</strong></th>
                            <th><strong>Totals</strong></th>
                            <th><strong>Ratios</strong></th>
                            <th><strong>0% - 100% </strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1. Total Active Links</td>
                            <td><?php echo $total_link; ?></td>
                            <td><?php echo $this->NitroCustom->link_ratio($total_link, $total_link); ?>%</td>
                            <td><div class="progress progress-info active tip" oldtitle="40%" title="" data-hasqtip="true">
                                    <div style="width: <?php echo $this->NitroCustom->link_ratio($total_link, $total_link); ?>%;" class="bar"></div>
                                </div>
                            </td>
                        </tr>  
                        <tr>
                            <td>2. Deleted Recently or Not Found</td>
                            <td><?php echo $dead_link; ?></td>
                            <td><?php echo $this->NitroCustom->link_ratio($total_link, $dead_link); ?>%</td>
                            <td><div class="progress progress-info active tip" oldtitle="40%" title="" data-hasqtip="true">
                                    <div style="width: <?php echo $this->NitroCustom->link_ratio($total_link, $dead_link); ?>%;" class="bar"></div>
                                </div>
                            </td>
                        </tr>  
                        <tr>
                            <td>3. Nofollow</td>
                            <td><?php echo $follow_link; ?></td>
                            <td><?php echo $this->NitroCustom->link_ratio($total_link, $follow_link); ?>%</td>
                            <td><div class="progress progress-info active tip" oldtitle="40%" title="" data-hasqtip="true">
                                    <div style="width: <?php echo $this->NitroCustom->link_ratio($total_link, $follow_link); ?>%;" class="bar"></div>
                                </div>
                            </td>
                        </tr> 
                        <tr>
                            <td>4. Ref domains</td>
                            <td><?php echo $RefDomains; ?></td>
                            <td><?php echo $this->NitroCustom->link_ratio($total_link, $RefDomains); ?>%</td>
                            <td><div class="progress progress-info active tip" oldtitle="40%" title="" data-hasqtip="true">
                                    <div style="width: <?php echo $this->NitroCustom->link_ratio($total_link, $RefDomains); ?>%;" class="bar"></div>
                                </div>
                            </td>
                        </tr> 
                        <tr>
                            <td>5. C Blocks To Total Active Links</td>
                            <td><?php echo $RefSubNets; ?></td>
                            <td><?php echo $this->NitroCustom->link_ratio($total_link, $RefSubNets); ?>%</td>
                            <td><div class="progress progress-info active tip" oldtitle="40%" title="" data-hasqtip="true">
                                    <div style="width: <?php echo $this->NitroCustom->link_ratio($total_link, $RefSubNets); ?>%;" class="bar"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>6. C Blocks To Ref Domain</td>
                            <td><?php echo $RefSubNets; ?></td>
                            <td><?php echo $c_block_to_reference_domain; ?>%</td>
                            <td><div class="progress progress-info active tip" oldtitle="40%" title="" data-hasqtip="true">
                                    <div style="width: <?php echo $c_block_to_reference_domain; ?>%;" class="bar"></div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clear_both"></div>
    <div class="span6">
        <div class="box chart">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-bars"></span>
                    <span>Ref Domains / C Blocks <br/>( <?php echo $RefDomains; ?> / <?php echo $RefSubNets; ?> )</span>
                </h4>
                <a href="#" class="minimize">Minimize</a>
            </div>
            <div class="content">
                <div class="simple-pie box_size"></div>
                <div class="clear_both"></div>
                <div class="bg_left">
                    <span class="bg1color bg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
                    <span class="text">C Blocks:</span>
                    <span class="value"> <?php echo $c_block_to_reference_domain; ?>%</span>
                    <div class="clear_both"></div>
                    <span class="bg2color bg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
                    <span class="text">Duplicate Ips:</span>
                    <span class="value"> <?php echo 100 - $c_block_to_reference_domain; ?>%</span>
                    <div class="clear_both"></div>
                    <div class="last_text"> Ref Domains have <?php echo $c_block_to_reference_domain; ?>% Different C Blocks </div>
                    <div class="clear_both"></div>
                </div>
            </div>
        </div>
    </div>



    <div class="span6">
        <div class="box chart">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-bars"></span>
                    <span>Dofollow / Nofollow <br/>( <?php echo $do_follow; ?> / <?php echo $follow_link; ?> )</span>
                </h4>
                <a href="#" class="minimize">Minimize</a>
            </div>
            <div class="content">
                <div class="dofollow box_size"></div>
                <div class="clear_both"></div>
                <div class="bg_left">
                    <span class="bg1color bg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
                    <span class="text">Dofollow:</span>
                    <span class="value"> <?php echo $do_follow_percentage; ?>%</span>
                    <div class="clear_both"></div>
                    <span class="bg2color bg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
                    <span class="text">Nofollow:</span>
                    <span class="value"> <?php echo 100 - $do_follow_percentage; ?>%</span>
                    <div class="clear_both"></div>
                    <div class="last_text"> Dofollow and Nofollow Together 100% </div>
                    <div class="clear_both"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="span6">
        <div class="box chart">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-bars"></span>
                    <span>All Links/ Links to Homepage <br/>( <?php echo $total_urls; ?> / <?php echo $link_to_homepage; ?> )</span>
                </h4>
                <a href="#" class="minimize">Minimize</a>
            </div>
            <div class="content">
                <div class="homepage box_size"></div>
                <div class="clear_both"></div>
                <div class="bg_left">
                    <span class="bg1color bg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
                    <span class="text">Homepage:</span>
                    <span class="value"> <?php echo $link_to_homepage_persentage; ?>%</span>
                    <div class="clear_both"></div>
                    <span class="bg2color bg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
                    <span class="text">Innerpage:</span>
                    <span class="value"> <?php echo 100 - $link_to_homepage_persentage; ?>%</span>
                    <div class="clear_both"></div>
                    <div class="last_text"> Homepage link <?php echo $link_to_homepage_persentage; ?>% of Total 100.00% </div>
                    <div class="clear_both"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="span6">
        <div class="box chart">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-bars"></span>
                    <span>All Links/ Links to Homepage <br/>( <?php echo $total_urls; ?> / <?php echo $link_to_innerpage; ?> )</span>
                </h4>
                <a href="#" class="minimize">Minimize</a>
            </div>
            <div class="content">
                <div class="innerpage box_size"></div>
                <div class="clear_both"></div>
                <div class="bg_left">
                    <span class="bg1color bg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
                    <span class="text">Homepage:</span>
                    <span class="value"> <?php echo 100 - $link_to_innerpage_persentage; ?>%</span>
                    <div class="clear_both"></div>
                    <span class="bg2color bg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
                    <span class="text">Innerpage:</span>
                    <span class="value"> <?php echo $link_to_innerpage_persentage; ?>%</span>
                    <div class="clear_both"></div>
                    <div class="last_text"> Innerpage link <?php echo $link_to_innerpage_persentage; ?>% of Total 100.00% </div>
                    <div class="clear_both"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="clear_both"></div>
    <div class="span6" style="width: 95%;">

        <div class="box chart">

            <div class="title">

                <h4>
                    <span class="icon16 icomoon-icon-bars"></span>
                    <span>Historical Activity Link</span>
                </h4>
                <a href="#" class="minimize">Minimize</a>
            </div>
            <div class="content">
                <div class="simple-chart" style="height: 230px; width:100%;">

                </div>
            </div>

        </div><!-- End .box -->

    </div><!-- End .span6 -->

    <!--------------------- Cron Report Start -------------------------------------------->
    <div class="row-fluid">
        <div class="span6" style="width: 100%;">
            <div style="margin-bottom: 20px;">
                <ul id="myTab1" class="nav nav-tabs">
                    <li class="active"><a href="#home1" data-toggle="tab">External</a></li>
                    <li><a href="#profile1" data-toggle="tab">Internal</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade in active" id="home1">
                        <div class="content clearfix">
                            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($external_link)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th class="zeroWidth"><strong></strong></th>
                                        <th><strong>Anchor Text</strong></th>
                                        <th><strong>URL</strong></th>
                                        <th><strong>First Seen</strong></th>
                                        <th><strong>Indexed</strong></th>
                                        <th><strong>Page Rank</strong></th>
                                        <th><strong>Citation Flow</strong></th>
                                        <th><strong>Trust Flow</strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($external_link) && count($external_link) > 0):
                                        foreach ($external_link as $row):
                                            $arr = $row['ldreportcron'];
                                            $d = 'http://' . str_replace('http://', '', $arr['SourceURL']);
                                            ?>
                                            <tr>
                                                <td class="zeroWidth"></td>
                                                <td><?php echo $arr['AnchorText']; ?></td>
                                                <td><a href="<?php echo $d; ?>" target="_blank"><?php echo $arr['SourceURL']; ?></a></td>
                                                <td><?php echo date("d M Y", strtotime($arr['FirstIndexedDate'])); ?></td>
                                                <td>
                                                  <?php if($arr['index'] == '') {
                                                      echo $this->Html->image('/images/loaders/circular/017.gif'); 
                                                      }
                                                  else if($arr['index'] == 1)
                                                  echo 'Yes'; else echo 'No';
                                                  ?>  
                                                </td>
                                                <td>
                                                    <?php if($arr['pr'] == '') {
                                                      echo $this->Html->image('/images/loaders/circular/017.gif'); 
                                                      }
                                                      else echo $arr['pr'];
                                                  ?>  
                                                </td>
                                                <td><?php echo $arr['SourceCitationFlow']; ?></td>
                                                <td><?php echo $arr['SourceTrustFlow']; ?></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    else:
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td colspan="4">No record found!</td>
                                        </tr>
                                    <?php
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile1">
                        <div class="content clearfix">
                            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($external_link)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th class="zeroWidth"><strong></strong></th>
                                        <th><strong>Anchor Text</strong></th>
                                        <th><strong>URL</strong></th>
                                        <th><strong>First Seen</strong></th>
                                        <th><strong>Indexed</strong></th>
                                        <th><strong>Page Rank</strong></th>
                                        <th><strong>Citation Flow</strong></th>
                                        <th><strong>Trust Flow</strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($internal_link) && count($internal_link) > 0):
                                        foreach ($internal_link as $row):
                                            $arr = $row['ldreportcron'];
                                            $d = 'http://' . str_replace('http://', '', $arr['SourceURL']);
                                            ?>
                                            <tr>
                                                <td class="zeroWidth"></td>
                                                <td><?php echo $arr['AnchorText']; ?></td>
                                                <td><a href="<?php echo $d; ?>" target="_blank"><?php echo $arr['SourceURL']; ?></a></td>
                                                <td><?php echo date("d M Y", strtotime($arr['FirstIndexedDate'])); ?></td>
                                                <td>
                                                  <?php if($arr['index'] == '') {
                                                      echo $this->Html->image('/images/loaders/circular/017.gif'); 
                                                      }
                                                  else if($arr['index'] == 1)
                                                  echo 'Yes'; else echo 'No';
                                                  ?>  
                                                </td>
                                                <td>
                                                    <?php if($arr['pr'] == '') {
                                                      echo $this->Html->image('/images/loaders/circular/017.gif'); 
                                                      }
                                                      else echo $arr['pr'];
                                                  ?>  
                                                </td>
                                                <td><?php echo $arr['SourceCitationFlow']; ?></td>
                                                <td><?php echo $arr['SourceTrustFlow']; ?></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    else:
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td colspan="4">No record found!</td>
                                        </tr>
                                    <?php
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="tab-pane fade" id="anchor_metrics">
    <div class="clear_both"></div>
    <div class="span6" style="width: 50%;">
        <div class="box chart">
            <div class="content" style="background-color: #F9F9F9 !important;border-style: none;">
                <div class="anchormatrics" style="width: 508px; height: 270px;"></div>
                <div class="clear_both"></div>
            </div>
        </div>
    </div>
    <div style="float: left;width:47%;background-color: #F9F9F9 !important;">
        <?php
        foreach ($anchortext_graph as $text_index => $text) {
            $AnchorText = $text['ldreportcron']['AnchorText'];
            $total = $text[0]['total'];
            $text_color = $text_colorCode[$text_index];
            $text_per = $link_to_innerpage_persentage = $this->NitroCustom->link_ratio($total_urls, $total);
            ?>
            <div style="float:left;font-weight: bold;width: 35%;">
                <span style="width:17px;height:17px;background-color: <?php echo $text_color; ?>;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <?php echo $AnchorText; ?>
            </div>
            <div style="float:left;width: 35%;">
                <?php echo $text_per; ?>%
            </div>
            <div class="clear_both"></div>
            <?php
        }
        ?> 
    </div>
</div>
<!--------------------- Cron Report End -------------------------------------------->