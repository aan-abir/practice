<?php $inputDefaults = array('label' => false, 'div' => false, 'required' => false, 'error' => false, 'class' => 'span12', 'autofocus'); ?>
<div class="content">
    <?php echo $this->Form->create('Backend', array('inputDefaults' => $inputDefaults, 'novalidate' => true)); ?>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="ArticleTitle">Project Description / Title:</label>
                <div class="span8">
                    <?php echo $this->Form->input('Article.title'); ?>
                </div>
            </div>
        </div>
    </div>     
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="ArticleCategory">Select Category:</label>
                <div class="span3">
                    <?php echo $this->Form->select('Article.category', $allCategs, array('empty' => false)); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="ArticleArtLen">Article Length:</label>
                <div class="span3">
                    <?php echo $this->Form->select('Article.art_len', array(150 => '150', 300 => '300', 400 => '400', 500 => '500', 700 => '700', 1000 => '1000', 2000 => '2000'), array('empty' => false)); ?>
                </div>
            </div>
        </div>
    </div>
    <!--div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="ArticleLanguage">Article Language:</label>
                <div class="span3">
    <?php echo $this->Form->select('Article.language', $aLanguage, array('empty' => false)); ?>
                </div>
            </div>
        </div>
    </div-->
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="ArticleKeywords">Keywords:</label>
                <div class="span6">
                    <?php echo $this->Form->input('Article.keywords'); ?>
                </div>
                <div class="span3">
                    <span class="help-inline blue">comma separated if multiple</span>
                </div>
            </div>
        </div>
    </div>     
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="ArticleWritingStyle">Writing Style:</label>
                <div class="span3">
                    <?php echo $this->Form->select('Article.writing_style', array(1 => 'Friendly Tone', 2 => 'Professional Tone'), array('empty' => false)); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="ArticleArtPurpose">Article Purpose:</label>
                <div class="span9">
                    <?php echo $this->Form->textarea('Article.art_purpose', array('class' => 'span12')); ?>
                </div>
            </div>
        </div>
    </div>     
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="ArticleSpecialInstructions">Special Instructions:</label>
                <div class="span9">
                    <?php echo $this->Form->textarea('Article.special_instructions', array('class' => 'span12')); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="ArticleBaseArticle">Base Article:</label>
                <div class="span9">
                    <?php echo $this->Form->textarea('Article.BaseArticle', array('class' => 'span12')); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="marginB10"></div>
    <div class="form-actions">
        <button class="btn btn-info offset2" type="submit">Submit</button>
    </div>
    <?php $this->Form->end(); ?>

    <div class="marginB10"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="title">
                    <h4>
                        <span>Ordered Articles</span>
                    </h4>
                </div>
                <div class="content">
                    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($itemList)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th class="zeroWidth"></th>
                                <th class="textLeft">Title</th>
                                <th class="textLeft">Article</th>
                                <th class="textLeft">Keywords</th>
                                <th class="textLeft">Special Instructions</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($itemList) && count($itemList) > 0):
                                foreach ($itemList as $item):
                                    $arr = $item['Article'];
                                    ?>
                                    <tr>
                                        <td class="zeroWidth"></td>
                                        <td class="textLeft"><?php echo $arr['title']; ?></td>
                                        <td class="textLeft"><?php echo $arr['body']; ?></td>
                                        <td class="textLeft"><?php echo $arr['keywords']; ?></td>
                                        <td class="textLeft"><?php echo $arr['special_instructions']; ?></td>
                                        <td>
                                            <?php
                                            if ($arr['status'] == '1'):
                                                echo 'In Process';
                                            elseif ($arr['status'] == 2):
                                                echo 'Ordered';
                                            elseif ($arr['status'] == 3):
                                                echo 'Completed';
                                            endif;
                                            ?>
                                        </td>
                                        <td>
                                            <div class="controls center">
                                                <!--<a href="<?php echo Router::url('content/' . $arr['id']); ?>" title="Edit Article" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>-->
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td colspan="5">No record found!</td>
                                </tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>