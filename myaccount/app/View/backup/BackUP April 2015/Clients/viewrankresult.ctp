<style>
    .textBlue { color: #3A87AD;}
</style>
<?php
$searchEngines[] = 'Google';
$searchEngines[] = 'Yahoo';
$searchEngines[] = 'Bing';
$searchEngines[] = 'Amazon';
$searchEngines[] = 'Youtube';

foreach ($searchEngines as $sEngine):
    if (isset($report[$sEngine])):
        ?>
        <div class="span12 marginB10" style="margin-bottom: 30px !important;">
            <?php $SEimg = strtolower($sEngine) . '.png'; ?>
            <?php echo $this->Html->image("/images/se/$SEimg"); ?>
        </div>
        <div class="marginB12"></div>

        <div class="span10">
            <div class="content">
                <?php
                $kOnP1 = $kInPos1 = $kRMu = $kRm = 0;
                if (isset($report[$sEngine]['EngineResult']) && count($report[$sEngine]['EngineResult']) > 0) {
                    //pr($report[$sEngine]['EngineResult']);
                    $rankChange = rankChange($report[$sEngine]['EngineResult']);
                    if ($rankChange) {
                        $kOnP1 = $rankChange['noPage'];
                        $kInPos1 = $rankChange['numberoneposG'];
                        $kRMu = $rankChange['keyUpG'];
                        $kRm = $rankChange['movedG'];
                    }
                }
                ?>
                <ul class="bigBtnIcon" style="text-align: left;">
                    <li>
                        <span class="icon icomoon-icon-rating-3"></span>
                        <span class="txt">Keywords On Page 1: <?php echo $kOnP1; ?></span>
                    </li>
                    <li>
                        <span class="icon icomoon-icon-cone"></span>
                        <span class="txt">Keywords in Position 1: <?php echo $kInPos1; ?></span>
                    </li>
                    <li>
                        <span class="icon  icomoon-icon-stairs"></span>
                        <span class="txt">Recently Moved Up: <?php echo $kRMu; ?></span>
                    </li>
                    <li>
                        <span class="icon icomoon-icon-shuffle"></span>
                        <span class="txt">Recently Moved: <?php echo $kRm; ?></span>
                    </li> 
                </ul>
            </div>
        </div>
        <div class="marginB10"></div>
        <div class="row-fluid">
            <div class="span12">
                <div class="content">
                    <table class="responsive table table-bordered">
                        <thead>
                            <tr>
                                <th>Results per URL for Campaign: <span class="textBlue"><?php echo $camp['Rankcampaign']['campaign_name']; ?></span> <?php if (isset($report[$sEngine]['EngineResult']) && count($report[$sEngine]['EngineResult']) > 0): ?><a data-toggle="modal" class="btn btn-success" href="#graphModal-<?php echo $sEngine; ?>">View Graph of Trends</a><?php endif; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $graphUrl = '-';
                            if (isset($report[$sEngine]['EngineResult']) && count($report[$sEngine]['EngineResult']) > 0):
                                foreach ($report[$sEngine]['EngineResult'] as $url => $keyword):
                                    if ($graphUrl == '-') {
                                        $graphUrl = $url;
                                    }
                                    $wurl = 'http://' . str_replace('http://', '', $url);
                                    ?>
                                    <tr>
                                        <td><h3 style="margin: 0 !important;"><?php echo $url; ?></h3></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table class="responsive table table-bordered" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th>Keyword/Phrase</th>
                                                        <th>URL Results</th>
                                                        <th>Position</th>
                                                        <th>Day Change</th>
                                                        <th>Week Change</th>
                                                        <th>Start Change</th>
                                                        <th>Trends Change</th>
                                                        <th>Trends</th>
                                                    </tr>
                                                </thead>                                            
                                                <?php
                                                if (isset($keyword) && count($keyword)):
                                                    foreach ($keyword as $k => $v):
                                                        //pr($v);
                                                        $keywordRows = getKeywordRows($v);
                                                        if ($keywordRows):
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $k; ?></td>
                                                                <td><a href="<?php echo Router::url('rankresultsmore/' . $camp['Rankcampaign']['id'] . '/' . $sEngine . '/' . base64_encode($url) . '/' . base64_encode($k)); ?>"><?php echo ''; ?>More Details</a></td>
                                                                <td><?php echo $keywordRows['tPos']; ?></td>
                                                                <td><?php echo $keywordRows['dc']; ?></td>
                                                                <td><?php echo $keywordRows['wc']; ?></td>
                                                                <td><?php echo $keywordRows['mc']; ?></td>
                                                                <td><?php echo $keywordRows['ac']; ?></td>
                                                                <td>
                                                                    <button title="View Trends" class="openTrendModalDialog" href="<?php echo Router::url('ranktrends/' . $camp['Rankcampaign']['id'] . '/' . $sEngine . '/' . urlencode($k) . '/' . base64_encode($url)); ?>" style="padding: 4px !important;"><span class="icon16 icomoon-icon-stats-up"></span></button>
                                                                </td>
                                                            </tr>                                                            
                                                            <?php
                                                        else:
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $domain; ?></td>
                                                                <td colspan="8">Not ranked</td>
                                                            </tr>
                                                        <?php
                                                        endif;
                                                    endforeach;
                                                    ?>
                                                    <?php
                                                else:
                                                    ?>
                                                    <tr>
                                                        <td colspan="8">No results found. Please wait while we process.</td>
                                                    </tr>
                                                <?php
                                                endif;
                                                ?>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <?php
                                endforeach;
                            else:
                                ?>
                                <tr>
                                    <td colspan="8">No record found!</td>
                                </tr>
                            <?php
                            endif;
                            ?>
                        </tbody>
                    </table>
                    <!-- Boostrap modal dialog -->
                    <div style="display: none; width: 80%; margin-left: -40%;" class="modal hide fade" id="graphModal-<?php echo $sEngine; ?>" aria-hidden="true">
                        <div class="modal-header">
                            <button data-dismiss="modal" class="close" type="button"><span class="icon12 minia-icon-close"></span></button>
                            <h3>Keyword Trends in <?php echo $sEngine; ?> :: <?php echo $graphUrl; ?></h3>
                        </div>
                        <div class="modal-body">
                            <div class="paddingT15 paddingB15">
                                <div id="simple-chart-trend-<?php echo $sEngine; ?>" class="simple-chart" style="height: 300px; width:100%;"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>                          
                </div>

            </div>
        </div>
        <script>
            $(function() {
                //graph options
                var options = {
                    grid: {
                        show: true,
                        aboveData: true,
                        color: "#3f3f3f",
                        labelMargin: 5,
                        axisMargin: 0,
                        borderWidth: 0,
                        borderColor: null,
                        minBorderMargin: 5,
                        clickable: true,
                        hoverable: true,
                        autoHighlight: true,
                        mouseActiveRadius: 20
                    },
                    series: {
                        grow: {active: false},
                        lines: {
                            show: true,
                            fill: false,
                            lineWidth: 4,
                            steps: false
                        },
                        points: {
                            show: true,
                            radius: 5,
                            symbol: "circle",
                            fill: true,
                            borderColor: "#fff"
                        }
                    },
                    xaxis: {
                        mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
                    },
                    legend: {position: "se"},
                    colors: chartColours,
                    shadowSize: 1,
                    tooltip: true, //activate tooltip
                    tooltipOpts: {
                        content: "%s: %y",
                        shifts: {
                            x: -30,
                            y: -50
                        }
                    }
                };
                var plot = $.plot($("#simple-chart-trend-<?php echo $sEngine; ?>"),
                        [
        <?php
        $lineColors[] = '#f2f7f9';
        $lineColors[] = '#fff8f2';
        $pointColors[] = '#88bbc8';
        $pointColors[] = '#ed7a53';
        foreach ($report[$sEngine]['GraphResult'] as $key => $value):
            ?>
                                {
                                    label: "<?php echo $key; ?>",
                                    data: <?php echo $value; ?>,
                                    lines: {fillColor: "#f2f7f9"},
                                    points: {fillColor: "#88bbc8"}
                                },
            <?php
        endforeach;
        ?>
                        ], options);
            });</script>
        <?php
    endif;
endforeach;
?>

<?php

function getKeywordRows($v) {

    $reArr = array();

    $dc = $wc = $mc = $ac = 0;

    $tPos = isset($v[0][0]) ? $v[0][0] : 0;
    $yPos = isset($v[1][0]) ? $v[1][0] : $tPos;
    $wPos = isset($v['week'][0]) ? $v['week'][0] : $yPos;
    $mPos = @$v[count($v) - 2][0];
    $aPos = @$v[count($v) - 1][0];

    if ($yPos == 0 && $tPos > 0) {
        $dc = '+' . abs(100 - $tPos);
    } else if ($tPos == 0 && $yPos > 0) {
        $dc = '-' . abs(100 - $yPos);
    } else {
        $dc = ($tPos < $yPos) ? '+' . abs($yPos - $tPos) : '-' . ($tPos - $yPos);
    }
    if ($wPos == 0 && $tPos > 0) {
        $wc = '+' . abs(100 - $tPos);
    } else if ($tPos == 0 && $wPos > 0) {
        $wc = '-' . abs(100 - $wPos);
    } else {
        $wc = ($tPos < $wPos) ? '+' . abs($wPos - $tPos) : '-' . ($tPos - $wPos);
    }
    if ($mPos == 0 && $tPos > 0) {
        $mc = '+' . abs(100 - $tPos);
    } else if ($tPos == 0 && $mPos > 0) {
        $mc = '-' . abs(100 - $mPos);
    } else {
        $mc = ($tPos < $mPos) ? '+' . abs($mPos - $tPos) : '-' . ($tPos - $mPos);
    }
    if ($aPos == 0 && $tPos > 0) {
        $ac = '+' . abs(100 - $tPos);
    } else if ($tPos == 0 && $aPos > 0) {
        $ac = '-' . abs(100 - $aPos);
    } else {
        $ac = ($tPos < $aPos) ? '+' . abs($aPos - $tPos) : '-' . ($tPos - $aPos);
    }

    $dc = ($dc == '-0' || $dc == '0') ? 'NA' : $dc;
    $wc = ($wc == '-0' || $wc == 0) ? 'NA' : $wc;
    $mc = ($mc == '-0' || $mc == 0) ? 'NA' : $mc;
    $ac = ($ac == '-0' || $ac == 0) ? 'NA' : $ac;
    $tPos = ($tPos == '-0' || $tPos == 0) ? 'NA' : $tPos;

    $reArr['tPos'] = $tPos;
    $reArr['dc'] = $dc;
    $reArr['wc'] = $wc;
    $reArr['mc'] = $mc;
    $reArr['ac'] = $ac;
    //$reArr['rUrl'] = $ac;

    return $reArr;
}

function rankChange($report) {

    $totalTrackedG = 0;
    $numberoneposG = 0;
    $movedG = 0;
    $keyUpG = 0;
    $keyDownG = 0;
    $noChangeG = 0;
    $noPage = 0;

    $reArr = array();
    foreach ($report as $url => $keyword) {
        foreach ($keyword as $k => $v) {
            $totalTrackedG++;
            $dc = $wc = $mc = $ac = 0;

            $tPos = isset($v[0][0]) ? $v[0][0] : 0;
            if ($tPos == 1) {
                $numberoneposG++;
            }
            if ($tPos >= 1 && $tPos <= 10) {
                $noPage++;
            }
            $yPos = isset($v[1][0]) ? $v[1][0] : $tPos;
            $wPos = isset($v['week'][0]) ? $v['week'][0] : $yPos;
            $mPos = @$v[count($v) - 2][0];
            $aPos = @$v[count($v) - 1][0];

            $dc = ($tPos < $yPos) ? '+' . abs($yPos - $tPos) : '-' . abs($tPos - $yPos);
            if ($dc < 0) {
                $keyUpG++;
            }
            if ($dc > 0) {
                $keyDownG++;
            }

            if ($yPos == $tPos) {
                $noChangeG++;
            }
            if ($yPos != $tPos) {
                $movedG++;
            }
            $wc = ($tPos < $wPos) ? '+' . abs($tPos - $wPos) : '-' . abs($tPos - $wPos);
            $mc = ($tPos < $mPos) ? '+' . abs($tPos - $mPos) : '-' . abs($tPos - $mPos);
            $ac = ($tPos < $aPos) ? '+' . abs($aPos - $tPos) : '-' . abs($tPos - $aPos);
        }
        $reArr['noPage'] = $noPage;
        $reArr['numberoneposG'] = $numberoneposG;
        $reArr['keyUpG'] = $keyUpG;
        $reArr['movedG'] = $movedG;
    }

    return $reArr;
}
?>



<div id="trendModal" style="width: 100% !important; height: 100% !important;"  title="Trends" class="dialog"></div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.openTrendModalDialog').click(function() {
            _this = $(this);
            $title = _this.attr('title');
            $modal = $('#trendModal');
            $("#ui-id-1").html($title);
            $modal.css({textAlign: 'center', minHeight: '400px'}).html('<p>Loading Please Wait...</p><p><?php echo $this->Html->image('/images/loader.gif', array('alt' => false)); ?></p>');
            $modal.load(_this.attr('href')).dialog('open');
            return false;
        });
        // JQuery UI Modal Dialog
        $('#trendModal').dialog({
            autoOpen: false,
            modal: true,
            autoResize: true,
            dialogClass: 'dialog',
            buttons: {
                "Close": function() {
                    $(this).dialog("close");
                }
            }
        });
        $("div.dialog button").addClass("btn");
        $(".loader").live('click', function(e) {
            document.documentElement.className += 'loadstate';
        });
    });
</script>