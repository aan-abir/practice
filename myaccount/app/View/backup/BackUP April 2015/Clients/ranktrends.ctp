<div class="flat_area grid_16"  style="display: block;">
    <h2>Keyword Tracking Trends at - <?php echo $engine; ?></h2>
    <p>keyword tracking trends for last 30 days. Everyday tracking of your keyword.</p>
</div>

<h5>Tracking Trends - "<?php echo $k . '" for "' . $url; ?>"</h5>
<div class="block" style="overflow: auto; height: 200px;">
    <div class="table-overflow">
        <table class="table table-bordered table-striped table-block">
            <thead>
                <tr>
                    <th><strong>Date</strong></th>
                    <th><strong>Position</strong></th>
                    <th><strong>Page</strong></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($results as $vt => $t) {
                    $dd = unserialize($t['Rankreport']['position']);

                    $url2 = 'http://' . str_replace(array('http://', 'https://'), '', $url);

                    $position = 0;
                    $page = 0;
                    if (isset($dd[$url][0])) {
                        $position = intval($dd[$url][0]);
                        $page = intval($dd[$url][1]);
                    } elseif (isset($dd[$url2][0])) {
                        $position = intval($dd[$url2][0]);
                        $page = intval($dd[$url2][1]);
                    }
                    echo '<tr>';
                    echo '<td>' . date('M d, Y', strtotime($t['Rankreport']['rank_date'])) . '</td>';
                    echo '<td>' . $position . '</td>';
                    echo '<td>' . $page . '</td>';
                    echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>