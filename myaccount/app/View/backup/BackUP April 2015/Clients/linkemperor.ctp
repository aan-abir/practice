<style type="text/css">
    tr, td{
        padding:0 !important;
        margin:0 !important;
    }
    td input{
        height: 15px !important; margin:2px !important;
    }
    .selVendor{
        background: #e9f6f7;
    }
</style>

<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'linkemperor'), 'id' => 'linkemperor', 'method' => 'post')); ?>
    <div class="form-row row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <label class="form-label span6" for="normal">Name of the Blast *</label>
                <?php echo $this->Form->input('Linkemperor.ordername', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Name Of the Submission', 'div' => false, 'label' => false, 'class' => 'span6')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <label class="form-label span6" for="normal">Anchor / Url *</label>
                <?php echo $this->Form->input('Linkemperor.anchor1', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Anchor 1', 'div' => false, 'label' => false, 'class' => 'span6')); ?>
            </div>
        </div>
        <div class="span6">
            <div class="row-fluid input-prepend">
                <span class="add-on">http://</span> <?php echo $this->Form->input('Linkemperor.url1', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Url for Anchor 1', 'div' => false, 'label' => false, 'class' => 'span6')); ?>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <label class="form-label span6" for="normal">Anchor / Url *</label>
                <?php echo $this->Form->input('Linkemperor.anchor2', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Anchor 2', 'div' => false, 'label' => false, 'class' => 'span6')); ?>
            </div>
        </div>
        <div class="span6">
            <div class="row-fluid input-prepend">
                <span class="add-on">http://</span> <?php echo $this->Form->input('Linkemperor.url2', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Url for Anchor 2', 'div' => false, 'label' => false, 'class' => 'span6')); ?>
            </div>
        </div>
    </div>


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Select Link Building Services to Order:</h4>
            </div>
            <div class="form-row">
                <table width="100%" cellpadding="1" cellspacing="1" border="0">
                    <tr>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th style="color: #999999;">Vendor </th>   
                        <th style="color: #999999;"># Max Input Links</th>   
                        <th style="color: #999999;"># Guaranteed Links</th>  
                        <th style="color: #999999;">Qty</th>
                    </tr>

                    <?php
                        // sort vendors by category
                        $sortedVendors = array();
                        //$vendor = array_filter($vendors);
                        $categorywise = array();
                        $i = 1;
                        //if ( AuthComponent::user('id') == 2  ) {
                           /* $vendors = (array) $vendors;
                            reset($vendors); // 
                            next($vendors); // go to next index
                            $ser = key($vendors);
                            $services = $vendors[$ser];*/
                        //}
                        
                        //pr($services); exit; 

                        foreach ($vendors as $k => $v) {
                            $cat = trim($v['name']);
                            if (strtolower($cat) == 'press release submission')
                                continue;
                            if (!key_exists($cat, $sortedVendors)) {
                                $sortedVendors["$cat"] = array();
                            }
                        }
                        //pr($sortedVendors);
                        foreach ( $vendors as $k => $v ) {
                            //pr($v);
                            foreach ($sortedVendors as $kk => $vv) {
                                //echo strtolower($v->name) .'=='. strtolower($kk) .'<br>';
                                if (strtolower($v['name']) == strtolower($kk)) {
                                    $sortedVendors[$kk][] = $v;
                                }
                            }
                        }
                        
                      
                        //pr($sortedVendors);
                        foreach ($sortedVendors as $k => $v) {
                            echo '<tr bgcolor="#861616;" style="background: #861616;color:#ffffff;font-weight:bold;">
                            <td colspan="6" style="padding:5px;"><span style="padding:5px; maring-left:10px">' . $k . '</span></td></tr>';
                            foreach ($v as $service) {
                                //pr($service);
                                $srvc =  $service;
                                echo '<tr>';
                                echo '<td class="isSel"></td>';
                                echo '<td>' . $srvc['name'] . '</td>';
                                echo '<td>' . $srvc['vendor'] . '</td>';
                                echo '<td align="center" style="text-align:center;">' . $srvc['input_targets'] . '</td>';
                                echo '<td align="center" style="text-align:center;">' . $srvc['output_urls'] . '</td>';
                                echo '<td align="center" style="text-align:center;">' .
                                $this->Form->input('Linkemperor.vendor.' . $srvc['id'], array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'How Many Lins to Build', 'div' => false, 'label' => false, 'class' => 'span1 qnty', 'style' => 'width:40px;')) . '</td>';
                                echo '</tr>';
                            }
                        }
                    ?>
                    <tr><td colspan="6" style="border-top: 1px solid #eee;">&nbsp;</td></tr>
                    <tr><td colspan="4">&nbsp;</td><td style="text-align: right;">Total</td>
                        <td style="text-align: center">
                            <input type="text" style="width: 50px;" id="totalQnty" value="0" readonly="readonly"></td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle" type="submit">Order Blast</button>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<script type="text/javascript">

    // document ready function
    $(document).ready(function() {
        $(".qnty").live('blur',function(){

            $v = 0;
            $(".qnty").each(function(im,em){
                $v = $v + (parseInt($(this).val()) || 0);
                // mark as selected
                if ( (parseInt($(this).val()) || 0) > 0 ) {
                    $(this).closest('tr').addClass('selVendor'); 
                    $(this).closest('tr').find('.isSel').html('<img src="/images/i_sticky.png" alt="">'); 

                }else{
                    $(this).closest('tr').removeClass('selVendor');
                    $(this).closest('tr').find('.isSel').html(''); 
                }
            });
            $("#totalQnty").val($v);


        });
    });

</script>