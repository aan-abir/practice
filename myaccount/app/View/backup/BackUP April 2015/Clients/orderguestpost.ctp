<?php include_once 'not_eligible_warning.ctp'; ?>
<div class="content">
    <?php echo $this->Form->create('Guestpost', array('url' => array('controller' => 'clients', 'action' => 'orderguestpost'), 'id' => 'orderGuestPostForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
   
    
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4><span class="red small">(required)</span> Market Name <span class="help-block-inline"> specify market name where to post your article. Exp:  olive oil etc</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->input('Guestpost.postmarket', array('error' => false, 'type' => 'text',  'title' => 'Market Name of this Post', 'div' => false, 'label' => false, 'class' => 'span12 tip' ,'id' => 'postmarket' )); ?>
            </div>
        </div>
    </div>
    
    
    <!--<div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="keywords">Keywords *</label>
                < ?php echo $this->Form->input('Guestpost.keywords', array('error' => false, 'type' => 'text',  'title' => 'Keywords', 'div' => false, 'label' => false, 'class' => 'span4' ,'id' => 'keywords' )); ?> 
                <span class="help-inline blue"> keywords for which you needs to promot the url </span>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="promoteurl">URL to Promote *</label>
                < ?php echo $this->Form->input('Guestpost.promoteurl', array('error' => false, 'type' => 'text',  'title' => 'URL to Promote', 'div' => false, 'label' => false, 'id' => 'promoteurl', 'class' => 'span4')); ?>
                <span class="help-inline blue">website/domain you need to promote</span>
            </div>
        </div>
    </div>-->

    
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4><span class="red small">(required)</span> Post Title <span class="help-block-inline"> this will show as topic header</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('Guestpost.title', array('error' => false, 'id' => 'title', 'title' => 'Please enter your article title', 'div' => false, 'label' => false, 'class' => 'span12 tip', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:60px;' ) ); ?>
            </div>
        </div>
    </div>


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                 <h4><span class="red small">(required)</span> Full Article to Post: <span class="help-block-inline">article will be reviewed by admin and get published. Please close your html tag properly. </span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('Guestpost.postdraft', array('error' => false, 'id' => 'body', 'required' =>false,  'title' => 'Article Content', 'div' => false, 'label' => false, 'class' => 'span12 tinymce', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:500px;' ) ); ?>
            </div>
        </div>
    </div>  
    
    
   <!-- <div class="form-row row-fluid">
        <div class="spa12">
            <div class="row-fluid">
                <div class="span3"></div>
                <label class="form-label span5 bold" for="publish_client_article">
                    < ?php //echo $this->Form->checkbox('Guestpost.publish_client_article', array('title' => 'Check to post your own article bellow', 'required' => false, 'div' => false, 'label' => false, 'id' => 'publish_client_article', 'class' => '')); ?>
                    Publish Your Article Written Above?
                </label>
            </div>
        </div>
    </div> -->
    <div class="marginT10"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Order New Guest Post</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

<script type="text/javascript">
    //--------------- Tinymce ------------------//
    $('textarea.tinymce').tinymce({
        // Location of TinyMCE script
        script_url :  '<?php echo BASEURL; ?>plugins/forms/tiny_mce/tiny_mce.js',

        // General options
        theme : "advanced",
        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,styleselect,formatselect,fontselect,fontsizeselect,code,",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
       // theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        //theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "css/main.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Replace values for the template plugin
        template_replace_values : {
            username : "SuprUser",
            staffid : "991234"
        }
    });

</script>