<div class="marginB10"></div>
<div class="content">
    You have already reached your allocated quota. Or you don't have access to this service. And you don't have sufficient extra credits for this service to use.
    <span class="yellow"> You need to add additional credits to enjoy this service.</span> You have already reached your allocated quota or do not have sufficient credits to use this service. <span class="yellow"> You can add extra credits <a href="<?php echo Router::url('addcredits'); ?>">here.</a>. </span>
</div>
<div class="marginB10"></div>