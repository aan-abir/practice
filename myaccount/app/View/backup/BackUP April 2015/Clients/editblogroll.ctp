<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'editblogroll', $data['Blogroll']['id']), 'id' => 'addBlogrollForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>

    <?php echo $this->form->hidden('Blogroll.id', array('value' => $data['Blogroll']['id'])); ?>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="networktype">Select Network Type:</label>
                <div class="span4">
                    <?php
                    //$domTypeArr = array('' => 'Select Network', '1' => 'My Sites', '3' => 'Premium Network', '4' => 'Private Sites');
                    $domTypeArr = array('3' => 'RankRatio Network', '4' => 'Private Sites');
                    echo $this->Form->select('Custom.networktype', $domTypeArr, array('required' => false, 'id' => 'selectNetwork', 'title' => 'Enter Domain network', 'rel' => Router::url('domaindropdown'), 'dropdownid' => 'domainDropDownAjaxLoaded', 'div' => false, 'label' => false, 'class' => 'span2 select fetchDropdown', 'empty' => false, 'value' => $data['Domain']['networktype']));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="domain_id">Choose Domain:</label>
                <div class="span4" id="domainDropDownAjaxLoaded">
                    <?php echo $this->Form->select('Blogroll.domain_id', $allDomains, array('required' => false, 'id' => 'BlogrollDomainId', 'title' => 'Select Domain', 'div' => false, 'label' => false, 'class' => 'select', 'empty' => false)); ?>
                </div>
                <div class="span4" id="horizontalLoader" style="display: none;">
                    <img src="<?php echo BASEURL; ?>images/loaders/horizontal/053.gif" alt="Domain Dropdown" />
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="CustomStatus0">Campaign or Custom Link?:</label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Single][option]" id="CustomStatus0" value="0" <?php echo $data['Campaign']['custom'] == 0 ? 'checked="checked"' : ''; ?> />
                    Campaign
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Single][option]" id="CustomStatus1" value="1" <?php echo $data['Campaign']['custom'] == 1 ? 'checked="checked"' : ''; ?> />
                    Custom Link
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid" id="campaignDropDownSelect" style="display: <?php echo $data['Campaign']['custom'] ? 'none' : 'block'; ?>;">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="campaign_id">Select Campaign:</label>
                <div class="span4">
                    <?php
                    echo $this->Form->select('Blogroll.campaign_id', $allCampaign, array('required' => false, 'title' => 'Enter Client My Site', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false, 'value' => $data['Blogroll']['campaign_id']));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid" id="customAnchorUrlInput" style="display: <?php echo $data['Campaign']['custom'] ? 'block' : 'none'; ?>;">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="anchortexts">Enter Custom Anchor:</label>
                <div class="span3">
                    <?php echo $this->Form->input('Single.anchortexts', array('type' => 'text', 'id' => 'anchortexts', 'title' => 'Anchor Text', 'placeholder' => 'Anchor Text', 'div' => false, 'label' => false, 'class' => 'span11', 'value' => $data['Single']['anchortexts'])); ?>
                </div>
                <div class="span4">
                    <?php echo $this->Form->input('Single.internalPageUrl', array('type' => 'text', 'title' => 'Anchor Link', 'placeholder' => 'Anchor Link', 'div' => false, 'label' => false, 'class' => 'span11', 'value' => $data['Single']['internalPageUrl'])); ?>
                </div>
                <div class="span2">
                    <?php echo $this->Form->checkbox('Single.nofollow', array('title' => 'check for nofollow', 'required' => false, 'div' => false, 'label' => 'Nofollow', 'class' => 'span1 nostyle', 'style' => '', 'checked' => $data['Single']['nofollow'])); ?> No Follow
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="status">Choose Option:</label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Blogroll][setting_option]" id="UserStatus1" value="1" <?php echo $data['Blogroll']['setting_option'] == 1 ? 'checked="checked"' : ''; ?> />
                    Add Link to Sidebar
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[Blogroll][setting_option]" id="UserStatus0" value="2" <?php echo $data['Blogroll']['setting_option'] == 2 ? 'checked="checked"' : ''; ?> />
                    Add Link to footer
                </div>
            </div>
        </div>
    </div>
    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Update Sitewide Link</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

<script type="text/javascript">

    jQuery('input[type="radio"][name="data[Single][option]"]').change(function() {
        var option_val = $(this).val();
        if (option_val == 0) {
            jQuery('#campaignDropDownSelect').show();
            jQuery('#customAnchorUrlInput').hide();
        } else if (option_val == 1) {
            jQuery('#customAnchorUrlInput').show();
            jQuery('#campaignDropDownSelect').hide();
        }
        return false;
    });

    //jQuery('input[type="radio"][name="data[Single][option]"]').change();
</script>