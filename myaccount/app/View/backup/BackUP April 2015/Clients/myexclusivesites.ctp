<?php include_once 'not_eligible_warning.ctp'; ?>
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'myexclusivesites'), 'method' => 'post')); ?>
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th style="width: 20px;"></th>
                <th>Domain</th>
                <th>Username</th>
                <th>Password</th>
                <th>Action</th>
                <th>Age</th>
                <th>PR</th>
                <th>Ljflow</th>
                <th>Cflow</th>
                <th>Tflow</th>
                <th>CMS</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($mysites) > 0):
                foreach ($mysites as $ms):
                    $loginUrl = 'http://' . str_replace('http://', '', $ms['Domain']['domain']) . '/wp-admin';
                    $u = str_replace('http://', '', $ms['Domain']['domain']);
                    $domain = 'http://' . str_replace('http://', '', $ms['Domain']['domain']);
                    ?>
                    <tr>
                        <td><input type="checkbox" name="data[Checked][<?php echo $ms['Domain']['id']; ?>]" /></td>
                        <td><a href="<?php echo $domain; ?>" target="_blank" title="Go to Domain" class="tip"><?php echo $ms['Domain']['domain']; ?></a></td>
                        <td><?php echo $ms['Domain']['cusername'] ? $ms['Domain']['cusername'] : '--'; ?></td>
                        <td><?php echo $ms['Domain']['cpassword'] ? $ms['Domain']['cpassword'] : '--'; ?></td>
                        <td>
                            <div class="controls center">
                                <?php
                                if (isset($ms['Domain']['cusername']) && $ms['Domain']['cusername'] != ''):
                                    ?>
                                    <a href="<?php echo "http://adminproxy:allowed@" . $u . "/wp-login.php"; ?>" title="Login to this domain admin panel now" class="tip" target="_blank">login as admin</a>
                                    <!--<a href="<?php echo $loginUrl; ?>" title="Login now" class="tip" target="_blank">login as admin</a>-->
                                    <?php
                                endif;
                                ?>
                            </div>
                        </td>                        
                        <td><?php echo substr($ms['Domain']['age'], 0, 4); ?></td>
                        <td><?php echo $ms['Domain']['pr']; ?></td>
                        <td><?php echo $ms['Domain']['ljpr']; ?></td>
                        <td><?php echo $ms['Domain']['cflow']; ?></td>
                        <td><?php echo $ms['Domain']['tflow']; ?></td>
                        <td><?php echo $ms['Domain']['cms']; ?></td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="10">No site assigned for you exclusively!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>
    <div class="form-actions">
        <button class="btn btn-success send-middle"  onclick="submitApplyForm(this);" type="button">
            <span class="icon16 icomoon-icon-checkmark white"></span> Apply for Selected Domains</button>
    </div>
    <?php echo $this->Form->end(); ?>    
</div>

<script type="text/javascript">
    //--------------- Popovers ------------------//
    //using data-placement trigger
    $("a[rel=popover]")
            .popover()
            .click(function(e) {
                e.preventDefault()
            })

    //using js trigger
    $("a[rel=popoverTop]")
            .popover({placement: 'top'})
            .click(function(e) {
                e.preventDefault()
            })

    function submitApplyForm(evt) {
        if (confirm('Are you sure to apply for the selected sites?')) {
            $("html").addClass('loadstate');
            $('#UserMyexclusivesitesForm').submit();
        } else {
            return false;
        }
    }
</script>