
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'linkindexing'), 'id' => 'addUrlsForm', 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>
            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                        <h4 style="color: #074f14;">Indexing Campaign Information <span class="help-block-inline">enter detail of your campaign</span></h4>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="span4" for="title">Campaign Title <span class="help-block-inline">Enter a Title for Your Indexing Campaign</span></label>
                        <div class="span8">
                            <?php echo $this->Form->input('Linkindexing.title', array('error' => false, 'required' => false, 'id' => 'title', 'type' => 'text', 'title' => 'Title', 'placeholder' => "Title", 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                        <h4 style="color: #074f14;">Upload from csv file <span class="help-block-inline">upon file selection the URL box below will be fill up with uploaded file data</span></h4>

                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="span4">
                            <input type="file" name="thefile" id="thefile" />
                        </div>
                        <span class="help-inline blue"><a href="<?php echo BASEURL; ?>samplefiles/keywords_with_domain_sample.csv">Download Sample CSV File</a></span>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                        <h4 style="color: #074f14;">Enter URLs <span class="help-block-inline"> (one URL per Line). Duplicate is automatically removed.</span></h4>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="form-row">
                        <?php echo $this->Form->textarea('Linkindexing.urls', array('error' => false, 'required' => false, 'id' => 'urls', 'title' => 'enter urls you want to track under this campaign', 'div' => false, 'label' => false, 'class' => 'span12', 'style' => 'height:220px')); ?>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" class="btn btn-info marginR10">Save Campaign</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>

</div>

</div>

<script type="text/javascript">
    $(document).ready(function() {

        $('#thefile').change(function(e) {
            if (e.target.files != undefined) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var str = e.target.result;
                    strSplit = str.split('\n');
                    url = '';
                    for (i = 0; i < strSplit.length; i++) {
                        if (strSplit[i] != '') {
                            var strjh = strSplit[i] + '';
                            str2 = strjh.split(',');
                            url += str2[0] + '\n';
                        }
                    }
                    $('#urls').text(url);
                };
                reader.readAsText(e.target.files.item(0));
            }
            return false;
        });


    });
</script>