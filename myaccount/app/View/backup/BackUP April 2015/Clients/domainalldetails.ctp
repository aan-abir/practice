<div style="margin-bottom: 20px;">
    <div class="tabbable tabs-left">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab"><span class="icon16 brocco-icon-disk"></span>Domain Profile Information</a></li>
            <li class=""><a href="#tab2" data-toggle="tab"><span class="icon16 entypo-icon-history"></span>Historical Ranking</a></li>
            <li class=""><a href="#tab3" data-toggle="tab"><span class="icon16 icomoon-icon-stats-up"></span>Current Ranking</a></li>
            <li class=""><a href="#tab4" data-toggle="tab"><span class="icon16 icomoon-icon-screwdriver"></span> Site Diagnostics</a></li>
            <li class=""><a href="#tab5" data-toggle="tab"><span class="icon16 icomoon-icon-info-2"></span>Site Info</a></li>
            <li class=""><a href="#tab6" data-toggle="tab"><span class="icon16 icomoon-icon-map-2"></span>Link Density</a></li>
        </ul>

        <div class="tab-content" style="min-height: 550px;">
            <div class="tab-pane active" id="tab1">
                <div class="content">
                    <div>
                        <div class="page-header">
                            <h4>Domain Profile information <span class="help-block-inline">all available information</span></h4>
                        </div> 
                        <table class="table table-bordered">
                            <thead>
                                <tr> 
                                    <th>Property</th>
                                    <th>Value</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <tr>
                                    <td>Domain</td>
                                    <td><a href="<?php echo $domain; ?>" target="_blank" title="Go to Domain" class="tip"><?php echo $domain['Domain']['domain']; ?></a></td>
                                </tr>
                                <tr>
                                    <td>WP Admin Username</td> <td> <?php echo $domain['Domain']['cusername'] ? $domain['Domain']['cusername'] : '--'; ?></td></tr>
                                <tr>
                                    <td>WP Admin Password</td>
                                    <td><?php echo $domain['Domain']['cpassword'] ? $domain['Domain']['cpassword'] : '--'; ?></td></tr>
                                <tr>
                                    <td>Age</td>
                                    <td><?php echo $domain['Domain']['age']; ?></td></tr>
                                <tr>
                                    <td>Page Rank</td>
                                    <td><?php echo $domain['Domain']['pr']; ?></td></tr>
                                <tr>
                                    <td>Link Juice PR</td>
                                    <td><?php echo $domain['Domain']['ljpr']; ?></td></tr>
                                <tr>
                                    <td>Citation Flow</td>
                                    <td><?php echo $domain['Domain']['cflow']; ?></td></tr>
                                <tr>
                                    <td>Trust Flow</td>
                                    <td><?php echo $domain['Domain']['tflow']; ?></td></tr>
                                <tr>
                                    <td>CMS Type</td>
                                    <td><?php echo $domain['Domain']['cms']; ?></td></tr>
                                <tr>

                                <tr>
                                    <td>IP</td>
                                    <td><?php echo $domain['Domain']['ip']; ?></td></tr>
                                <tr>



                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="tab-pane" id="tab2">
                <div class="content">
                    <div>
                        <div class="page-header">
                            <h4>Historical Ranking <span class="help-block-inline">all available information</span></h4>
                        </div> 
                        <div class="margin padding">
                            <img src="/images/loaders/horizontal/053.gif" alt="" />
                        </div>

                    </div>
                </div>
            </div>

            <div class="tab-pane" id="tab3">
                <div class="content">
                    <div>
                        <div class="page-header">
                            <h4>Current Ranking <span class="help-block-inline">all available information</span></h4>
                        </div> 
                        <div class="margin padding">
                            <img src="/images/loaders/horizontal/053.gif" alt="" />
                        </div>

                    </div>
                </div>
            </div>

            <div class="tab-pane" id="tab4">
                <div class="content">
                    <div>
                        <div class="page-header">
                            <h4>Site Diagnostics <span class="help-block-inline">all available information</span></h4>
                        </div> 
                        <div class="margin padding">
                            <img src="/images/loaders/horizontal/053.gif" alt="" />
                        </div>

                    </div>
                </div>
            </div>

            <div class="tab-pane" id="tab5">
                <div class="content">
                    <div>
                        <div class="page-header">
                            <h4>Site Info <span class="help-block-inline">all available information</span></h4>
                        </div> 
                        <div class="margin padding">
                            <img src="/images/loaders/horizontal/053.gif" alt="" />
                        </div>

                    </div>
                </div>
            </div>

            <div class="tab-pane" id="tab6">
                <div class="content">
                    <div>
                        <div class="page-header">
                            <h4>Link Density <span class="help-block-inline">all available information</span></h4>
                        </div> 
                        <div class="margin padding">
                            <img src="/images/loaders/horizontal/053.gif" alt="" />
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

