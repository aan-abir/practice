<div class="content">
    <?php echo $this->Form->create('Editmashup', array('url' => array('controller' => 'clients', 'action' => 'editmashup', $data['Socialbrand']['id']), 'id' => 'createMashupForm', 'method' => 'post', 'novalidate' => 'novalidate','class' => 'form-horizontal seperator')); ?>
   
     <?php echo $this->Form->input('Socialbrand.social_user_id', array('type' => 'hidden', 'div' => false, 'label' => false)); ?>
   
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="firstname">First name* <span class="help-block-inline">Exp: John</span> </label>
                    <?php echo $this->Form->input('Socialbrand.firstname', array('error' => false, 'type' => 'text', 'id' => 'firstname', 'title' => 'First Name', 'placeholder' => "Client First Name", 'div' => false, 'label' => false, 'class' => 'span11')); ?>

                </div>
                <div class="span4">
                    <label for="lastname">Last name*  <span class="help-block-inline">Exp: Coot</span></label>
                    <?php echo $this->Form->input('Socialbrand.lastname', array('error' => false, 'type' => 'text', 'id' => 'lastname', 'title' => 'Client Last Name', 'placeholder' => "Client Last Name", 'div' => false, 'label' => false, 'class' => 'span11')); ?>

                </div>
                <div class="span4">
                    <label for="email">Email*  <span class="help-block-inline">Exp: myemail@domain.com</span></label>
                    <?php echo $this->Form->input('Socialbrand.email', array('error' => false, 'type' => 'text', 'id' => 'email', 'title' => 'Client Email', 'placeholder' => "Client Email", 'div' => false, 'label' => false, 'class' => 'span11')); ?>

                </div>
            </div>
        </div>        
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="facebook_page_id">Facebook Page ID <span class="help-block-inline">Exp: 999999999999999</span></label>
                    <?php echo $this->Form->input('Socialbrand.facebook_page_id', array('type' => 'text', 'id' => 'facebook_page_id', 'title' => 'Client Facebook Page ID', 'placeholder' => "Client Facebook Page ID", 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="twitter_url">Twitter ID <span class="help-block-inline">Exp: twitter</span></label>
                    <?php echo $this->Form->input('Socialbrand.twitter_url', array('type' => 'text', 'id' => 'twitter_url', 'title' => 'Client Twitter Url', 'placeholder' => "Client Twitter Url", 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
                <div class="span4">
                    <label for="google_url">Google ID  <span class="help-block-inline">Exp: 999999999999999999999</span></label>
                    <?php echo $this->Form->input('Socialbrand.google_url', array('type' => 'text', 'id' => 'google_url', 'title' => 'Client Google Url', 'placeholder' => "Client Google Url", 'div' => false, 'label' => false, 'class' => 'span11')); ?>
                </div>
            </div>
        </div>        
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="youtube_url">Youtube ID  <span class="help-block-inline">Exp: youtube</span></label>
                    <?php echo $this->Form->input('Socialbrand.youtube_url', array('type' => 'text', 'id' => 'youtube_url', 'title' => 'Client Youtube Url', 'placeholder' => "Client Youtube Url", 'div' => false, 'label' => false, 'class' => 'span11')); ?>

                </div>

                <div class="span4">
                    <label for="linkedin_url">Linkedin ID <span class="help-block-inline">Exp: linkedin</span></label>
                    <?php echo $this->Form->input('Socialbrand.linkedin_url', array('type' => 'text', 'id' => 'linkedin_url', 'title' => 'Client Linkedin Url', 'placeholder' => "Client Linkedin Url", 'div' => false, 'label' => false, 'class' => 'span11')); ?>

                </div>
                <div class="span4">
                    <label for="pinterest_url">Pinterest ID <span class="help-block-inline">Exp: pinterest</span></label>
                    <?php echo $this->Form->input('Socialbrand.pinterest_url', array('type' => 'text', 'id' => 'pinterest_url', 'title' => 'Client Pinterest Url', 'placeholder' => "Client Pinterest Url", 'div' => false, 'label' => false, 'class' => 'span11')); ?>

                </div>
            </div>
        </div>        
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span4">
                    <label for="instagram_url">Instagram ID <span class="help-block-inline">Exp: instagram</span></label>
                    <?php echo $this->Form->input('Socialbrand.instagram_url', array('type' => 'text', 'id' => 'instagram_url', 'title' => 'Client Instagram Url', 'placeholder' => "Client Instagram Url", 'div' => false, 'label' => false, 'class' => 'span11')); ?>

                </div>

                <div class="span4">
                    <label for="additional_rss_feed_1">Additional feed <span class="help-block-inline">Exp: http://news.yourdomain.com/?output=rss</span></label>
                    <?php echo $this->Form->input('Socialbrand.additional_rss_feed_1', array('type' => 'text', 'id' => 'additional_rss_feed_1', 'title' => 'Client Additional RSS Feed 1', 'placeholder' => "Client Additional RSS Feed 1", 'div' => false, 'label' => false, 'class' => 'span11')); ?>

                </div>
                <div class="span4">
                    <label for="additional_rss_feed_2">Additional feed <span class="help-block-inline">Exp: http://news.yourdomain.com/?output=rss</span></label>
                    <?php echo $this->Form->input('Socialbrand.additional_rss_feed_2', array('type' => 'text', 'id' => 'additional_rss_feed_2', 'title' => 'Client Additional RSS Feed 2', 'placeholder' => "Client Additional RSS Feed 2", 'div' => false, 'label' => false, 'class' => 'span11')); ?>

                </div>
            </div>
        </div>        
    </div>

    
    
    
    
    
    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>