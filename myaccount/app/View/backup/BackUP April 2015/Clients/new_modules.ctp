<div class="page-header">
    <h1><?php echo $item['Module']['title']; ?>
      <br>
      <span class="help-block-inline" style="font-size: 18px;"> 
        Expert: <?php echo $item['Module']['ename']; ?> | 
        Total Length: <?php echo $tLength; ?> | 
        Total Lessons: <?php echo count($item['Lesson']); ?></span></h1>
</div>

<div class="content">
    <div class="row-fluid">
        <div class="span2">
            <?php 
                if ( $item['Module']['photo'] != '' ){ ?>
                <a title="Image Preview" class="fancybox tip" href="<?php echo  '/myaccount/moduleexpert/' . $item['Module']['photo']; ?>">
                    <img  alt="Preview" src="<?php echo  '/myaccount/moduleexpert/' . $item['Module']['photo']; ?>" />
                </a>
                <br>
                 <p style="font-size: 14px; margin-top: 5px;"></p>
                <?php
            } ?>
        </div>
        <div class="span10">
            <?php echo $item['Module']['content']; ?>
        </div>
    </div>
</div>

<div class="marginB10" style="border-bottom: 1px solid #eee;"></div>

<div class="content">
    <div class="module">


        <div class="lesson-list">
            <table style="width:100%;">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Lesson Name</th>
                        <th class="rem-small">Progress</th>
                        <th class="rem-small">Time Remaining</th>
                        <th class="rem-small">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if ($item['Lesson']):
                            foreach ($item['Lesson'] as $sl => $les):
                            ?>
                            <tr>
                                <td><?php echo $sl + 1; ?></td>
                                <td>
                                    <div>
                                        <a href="<?php echo Router::url('/new/clients/view_lesson/' . $les['id']); ?>"><?php echo $les['title']; ?></a>
                                    </div>
                                    <div class="stats">
                                        <span
                                            class="length">Length: <?php echo number_format($les['lesson_length'], 2); ?></span>
                                        <span>Comments: <?php echo count($les['Lessoncomment']); ?></span>
                                    </div>
                                </td>
                                <td class="rem-small">
                                    <div class="prog-bar-outer">
                                        <div class="prog-bar-empty">
                                            <div class="prog-bar-full" style=" width: 100%">
                                                <?php
                                                    $doneP = ($les['done_length'] / $les['lesson_length']) * 100;
                                                    echo $doneP <= 100 ? number_format($doneP, 2) : '0';?>%
                                            </div>
                                            <span></span>
                                        </div>
                                    </div>
                                </td>
                                <td style="" class="rem-small time-rem">
                                    <?php
                                        $remT = ($les['lesson_length'] - $les['done_length']);
                                        echo $doneP >= 0 ? number_format($remT, 2) : '0';?>
                                </td>
                                <td style="" class="rem-small"></td>
                            </tr>
                            <?php
                                endforeach;
                            endif;
                    ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
<style type="text/css">
    .module .lesson-list {
        background-color: #f0f1f5;
        border-radius: 3px;
        padding-left: 1em;
        padding-right: 1em;
        padding-bottom: 1em;
        padding-top: 0;
    }

    .module th {
        font-size: small;
        text-transform: uppercase;
        text-align: left;
    }

    table thead tr th, table tfoot tr th, table tfoot tr td, table tbody tr th, table tbody tr td, table tr td {
        display: table-cell;
        line-height: 1.125rem;
    }

    table thead tr th, table thead tr td {
        padding: 0.5rem 0.625rem 0.625rem;
        font-size: 0.875rem;
        font-weight: bold;
        color: #222222;
    }

    tr {
        display: table-row;
        vertical-align: inherit;
        border-color: inherit;
    }

    tbody tr:nth-child(odd) {
        background-color: #fefefe;
    }

    tbody tr:nth-child(even) {
        background-color: #f0f1f5;
    }

    table tr th, table tr td {
        padding: 0.5625rem 0.625rem;
        font-size: 0.875rem;
        color: #222222;
        text-align: left;
    }

    .module .lesson-list a {
        color: #f35b4d;
        font-weight: bold;
    }

    .module .stats .length {
        padding-right: 4em;
        min-width: 100px;
        display: inline-block;
    }

</style>