<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Domain Name</th>
                <th>CMS</th>
                <th>User Name</th>
                <th>Password</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>

            <?php
            if (count($allCamps) > 0) {


                foreach ($allCamps as $camp) {
                    ?>
                    <tr>
                        <td><?php echo $camp['Domain']['id']; ?></td>
                        <td><?php echo $camp['Domain']['domain']; ?></td>
                        <td><?php echo $camp['Domain']['cms']; ?></td>
                        <td><?php echo $camp['Domain']['ausername']; ?></td>
                        <td><?php echo $camp['Domain']['apassword']; ?></td>
                        <td>
                            <div class="controls center">

                                <a href="<?php echo Router::url('editdomain/' . $camp['Domain']['id']); ?>" title="Edit Site" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                <a href="<?php echo Router::url('deletedomain/' . $camp['Domain']['id']); ?>" title="Remove Site?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                            </div>
                        </td>
                    </tr>

                    <?php
                }
            } else {
                echo '<tr><td colspan="6">No Manual Site Found!</td></tr>';
            }
            ?>



        </tbody>
    </table>
    <div id="pagingPmb">
        <?php
        echo $this->Paginator->counter('{:count} total');
        if ($this->Paginator->numbers() != '')
            echo '<em>Pages: </em>';
        if ($this->Paginator->hasPrev())
            echo $this->Paginator->prev(__('previous '), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers();
        if ($this->Paginator->hasNext())
            echo $this->Paginator->next(__(' next'), array(), null, array('class' => ' next disabled'));
        ?>
    </div> 
</div>