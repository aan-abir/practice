<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Debit History</span>
                </h4>
            </div>
            <div class="content clearfix">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($debitHistories)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th><?php echo $this->Paginator->sort('Credithistory.servicename', 'Service Name', array('class' => 'tip', 'title' => 'Click to Sort', 'style' => 'color:#000000;')); ?></th>
                            <th>Amount</th>
                            <th>Quantity</th>
                            <th>Remarks</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($debitHistories) && $debitHistories):
                            foreach ($debitHistories as $key => $arr):
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td><?php echo $arr['Credithistory']['servicename']; ?></td>
                                    <td><?php echo $arr['Credithistory']['amount']; ?></td>
                                    <td><?php echo $arr['Credithistory']['quantity']; ?></td>
                                    <td><?php echo $arr['Credithistory']['remarks']; ?></td>
                                    <td><?php echo date('Y-m-d', strtotime($arr['Credithistory']['created'])); ?></td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="5">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="pagingPmb">
    <?php
    echo $this->Paginator->counter('{:count} total');
    if ($this->Paginator->numbers() != '')
        echo '<em>Pages: </em>';
    if ($this->Paginator->hasPrev())
        echo $this->Paginator->prev(__('previous '), array(), null, array('class' => 'prev disabled'));
    echo $this->Paginator->numbers();
    if ($this->Paginator->hasNext())
        echo $this->Paginator->next(__(' next'), array(), null, array('class' => ' next disabled'));
    ?>
</div> 