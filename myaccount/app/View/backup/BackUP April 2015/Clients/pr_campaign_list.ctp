<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="content clearfix">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($page_rank_campaign_list)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th><strong>#</strong></th>
                        <th><strong>Campaign Name</strong></th>
                        <th><strong>No. of Domain(s)</strong></th>
                        <th><strong>Date Created</strong></th>
                        <th><strong>Actions</strong></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($page_rank_campaign_list) && count($page_rank_campaign_list) > 0):
                        foreach ($page_rank_campaign_list as $row):
                            $arr = $row['Prcampaign'];
                            ?>
                            <tr>
                                <td></td>
                                <td><?php echo $arr['name']; ?></td>
                                <td><?php echo $this->NitroCustom->number_of_domain($arr['id']); //$arr['no_of_domain'];  ?></td>

                                <td><?php echo date("d M Y", strtotime($arr['created_at'])); ?></td>
                                <td>
                                    <div class="controls center">
                                        <a href="<?php echo Router::url('pr_campaign_view/' . $arr['id']); ?>" title="View Campaigns" class="tip">View</a> |
                                        <a href="<?php echo Router::url('pr_campaign_edit/' . $arr['id']); ?>" title="Edit Campaigns" class="tip">Edit</a> |
                                        <a href="<?php echo Router::url('pr_campaign_delete/' . $arr['id']); ?>" title="Remove Campaign?" class="tip callAction">Remove</a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <tr>
                            <td></td>
                            <td colspan="4">No record found!</td>
                        </tr>
                    <?php
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>