<div class="page-header">
    <h4>User Plan</h4>
</div>
<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span><?php echo $userInfo['User']['fullname']; ?></span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>Plan Name</th>
                            <th>Current Extra Credit ($)</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($userInfo) && count($userInfo) > 0):
                            ?>
                            <tr>
                                <td><?php echo $userInfo['Package']['packagename']; ?></td>
                                <td><?php echo $userInfo['User']['extra_credit']; ?></td>
                                <td><?php echo $userInfo['Package']['description']; ?></td>
                            </tr>
                            <?php
                        else:
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="2">No plan assigned!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->    
</div>
<div class="row-fluid">    
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Service Details | <a href="<?php echo adminHome . '/updateusercredits/' . $userInfo['User']['id']; ?>" title="Update User Credit" class="tip">Update</a></span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th class="textLeft">Service Name</th>
                            <th class="textRight">Unit Price ($)</th>
                            <th class="textRight">Service Limit</th>
                            <th class="textRight">Extra Units</th>
                            <th class="textRight">Consumed Units</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $serviceTotal = 0.00;
                        $serviceCurrentTotal = 0.00;
                        $consumedTotal = 0.00;
                        if (isset($serviceList) && count($serviceList) > 0):
                            foreach ($serviceList as $k => $arr):
                                if ($arr['Service']['servicename']):
                                    ?>
                                    <tr>
                                        <td class="textLeft"><?php echo $arr['Service']['servicename']; ?></td>
                                        <td class="textRight"><?php echo $arr['Service']['default_unit_price']; ?></td>
                                        <td class="textRight">
                                            <?php
                                            if (isset($arr['Service']['servicelimit'])) {
                                                echo $arr['Service']['servicelimit'] != '9999999' ? $arr['Service']['servicelimit'] : 'Unlimited';
                                            } else {
                                                echo '0';
                                            }
                                            ?>
                                        </td>
                                        <td class="textRight"><?php echo isset($arr['Service']['extra_unit']) ? $arr['Service']['extra_unit'] : 0; ?></td>
                                        <td class="textRight"><?php echo isset($arr['Service']['service_used']) ? $arr['Service']['service_used'] : 0; ?></td>
                                    </tr>
                                    <?php
                                endif;
                            endforeach;
                            ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="textRight"></td>
                                <td class="textRight"></td>
                            </tr>    
                            <?php
                        else:
                            ?>
                            <tr>
                                <td colspan="5">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div>