<div class="marginB10"></div>
<div class="page-header">
    <h4>All Assigned MySites</h4>
</div>
<div class="row-fluid">
    <div class="span12">

        <div class="content clearfix">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($clientList)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th>Client Name</th>
                        <th>Package</th>
                        <th>Package Limit</th>
                        <th>Assigned</th>
                        <th>Due</th>
                        <th>Extra</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($clientList) > 0):
                        foreach ($clientList as $arr):
                            $serviceLimit = $arr['Serviceprice']['servicelimit'];
                            if ($serviceLimit > 0):
                                $siteAssigned = count($arr['Domain']);
                                $siteDue = $serviceLimit - $siteAssigned;
                                ?>
                                <tr>
                                    <td><a target="_blank" href="<?php echo DOMAIN . '/users/letmeinwithoutanyusernameandpassword/' . $arr['User']['id']; ?>" title="Login into User Account" class="tip"><?php echo $arr['User']['fullname']; ?></a></td>
                                    <td><?php echo $arr['User']['packagename']; ?></td>
                                    <td><?php echo $serviceLimit; ?></td>
                                    <td><?php echo $siteAssigned; ?></td>
                                    <td><?php echo $siteDue >= 0 ? $siteDue : 0; ?></td>
                                    <td><?php echo $siteDue < 0 ? '<span style="color: red;">' . abs($siteDue) . '</span>' : 0; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <a href="<?php echo adminHome . '/clientmysites/' . $arr['User']['id']; ?>" title="View Client Mysites" class="tip">Client's Exclusive</a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endif;
                        endforeach;
                    else:
                        ?>
                        <tr>
                            <td colspan="7">No Assigned Domains Found!</td>
                        </tr>
                    <?php
                    endif;
                    ?>
                </tbody>
            </table>
        </div>

    </div><!-- End .span6 -->
</div>
