<div class="marginB10"></div>
<div class="page-header">
    <h4>Media</h4>
    <p>
        here all the meida ( external resouce link,  internal images and video etc) has been added. you can use these resouce directly. 
    </p>

</div>


<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>Title</th>
                <th>Ext / Type</th>
                <th>Preview</th>
                <th>Short Code popup</th>
                <th>Short Code Embded</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(isset($allMedia) && count($allMedia) > 0):
                    foreach($allMedia as $arr):
                      
                      $ext = ( $arr['Media']['type'] != 'link')  ?  ' / '. @strtoupper(array_pop( explode(".",$arr['Media']['source']) ) ) : '';
                    
                    ?>
                    <tr>
                        <td><?php echo $arr['Media']['title']; ?></td>
                        <td><?php echo $arr['Media']['type'] .  $ext ; ?></td>
                        <td>
                            <?php 
                                if ( $arr['Media']['type'] == 'image' ){ ?>
                                <a title="Image Preview" class="fancybox tip" href="<?php echo  '/media/' . $arr['Media']['source']; ?>">
                                    <img width="100" alt="Preview" src="<?php echo  '/media/' . $arr['Media']['source']; ?>" />
                                </a>
                                <?php
                                }
                                elseif ($arr['Media']['type'] == 'video' ) {
                                    echo '<a class="openModalDialog" title="Preview. Double click on Video for full screen" href="http://seonitrov2.seonitro.com/sadmins/videopreview/'.$arr['Media']['source'].'">preview</a>';
                                }
                                elseif ($arr['Media']['type'] == 'files' ) {
                                    echo '<a class="fancypdf" title="Preview" href="/media/'.$arr['Media']['source'].'">preview</a>';
                                }
                                else {
                                    echo '<a title="External Resouce Preview" class="fancybox-media" href="'.trim($arr['Media']['source']).'">preview</a>'; 
                                }
                            ?>

                        </td>
                        <td>
                            <?php 
                                echo '[popup_'.$arr['Media']['id'].']';
                            ?>
                        </td>
                        <td>
                            <?php 
                                echo '[embed_'.$arr['Media']['id'].']';
                            ?>
                        </td>

                        <td>
                            <div class="controls center">
                                <a href="<?php echo adminHome . '/deletemedia/' . $arr['Media']['id']; ?>" title="Remove Media?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                        endforeach;
                    else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="7">No Media Found!</td>
                </tr>
                <?php
                    endif;
            ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
$(".fancypdf").click(function(){
 $.fancybox({
   type: 'html',
   autoSize: false,
   content: '<embed src="'+this.href+'#nameddest=self&page=1&view=FitH,0&zoom=80,0,0" type="application/pdf" height="99%" width="100%" />',
   beforeClose: function() {
     $(".fancybox-inner").unwrap();
   }
 }); //fancybox
 return false;
}); //click
       
</script>