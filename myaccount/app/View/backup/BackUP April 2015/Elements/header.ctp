<div id="header">
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="brand" href="/">
                   <img src="<?php echo BASE; ?>/images/side-logo.png" alt="">
				</a>
				<div class="nav-no-collapse">
					<ul class="nav">
						<li class="active"><a href="/"><span class="icon16 icomoon-icon-screen-2"></span> Dashboard</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<span class="icon16 eco-link "></span> My Sites
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li class="menu">
									<ul>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-equalizer"></span>My Sites dd 1</a>
										</li>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-wrench"></span>My Sites dd 2</a>
										</li>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-picture-2"></span>My Sites dd 3</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<span class="icon16 eco-list"></span> Network Sites
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li class="menu">
									<ul>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-equalizer"></span>Network Sites SM 1</a>
										</li>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-wrench"></span>Network Sites SM 1</a>
										</li>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-picture-2"></span>Network Sites SM 1</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<span class="icon16 eco-link"></span> Linking
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li class="menu">
									<ul>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-equalizer"></span>Linknig SM 1</a>
										</li>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-wrench"></span>Linknig SM 1</a>
										</li>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-picture-2"></span>Linknig SM 1</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>
						<li><a href="/sadmins/support"><span class="icon16 icomoon-icon-support"></span> Support</a></li>


					</ul>

					<ul class="nav pull-right usernav">

						<li class="dropdown">
							<a href="#" class="dropdown-toggle avatar" data-toggle="dropdown">
								<?php
											$default = BASEURL.'/images/avatar.jpg';
											$grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( AuthComponent::user('email') ) ) ) . "?d=" . urlencode( $default ) . "&s=55";
											echo '<img src="'.$grav_url.'"  class="image" alt="Profile Pic" />';
										?>
								<span class="txt">
								<?php echo AuthComponent::user('firstname').' '.AuthComponent::user('lastname'); ?>
								</span>
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li class="menu">
									<ul>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-user-3"></span>Edit profile</a>
										</li>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-comments-2"></span>Approve comments</a>
										</li>
										<li>
											<a href="#"><span class="icon16 icomoon-icon-plus-2"></span>Add user</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>

						<li><a href="/sadmins/logout"><span class="icon16 icomoon-icon-exit"></span> Logout</a></li>
					</ul>
				</div><!-- /.nav-collapse -->
			</div>
		</div><!-- /navbar-inner -->
	</div>
	<!-- /navbar -->
 </div>
