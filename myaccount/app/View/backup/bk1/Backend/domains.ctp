<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Manage Domains</span>
                </h4>
            </div>
            <div class="content">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($itemList)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th class="textLeft">Domain</th>
                            <th class="textLeft">Log In</th>
                            <th class="textLeft">Password</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($itemList) && count($itemList) > 0):
                            foreach ($itemList as $item):
                                $arr = $item['Domain'];
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td class="textLeft"><?php echo $arr['domain']; ?></td>
                                    <td class="textLeft"><?php echo $arr['ausername']; ?></td>
                                    <td class="textLeft"><?php echo $arr['apassword']; ?></td>
                                    <td><?php echo $arr['status'] ? 'Active' : 'Inactive'; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <a href="<?php echo Router::url('domain/' . $arr['id']); ?>" title="Edit Domain" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="5">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>