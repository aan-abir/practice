<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Manage Users</span>
                </h4>
            </div>
            <div class="content">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($itemList)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th class="textLeft">Name</th>
                            <th class="textLeft">Username</th>
                            <th class="textLeft">Plain Password</th>
                            <th class="textLeft">Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($itemList) && count($itemList) > 0):
                            foreach ($itemList as $item):
                                $arr = $item['User'];
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td class="textLeft"><?php echo $arr['fullname']; ?></td>
                                    <td class="textLeft"><?php echo $arr['username']; ?></td>
                                    <td class="textLeft"><?php echo $arr['plainpass']; ?></td>
                                    <td class="textLeft"><?php echo $arr['email']; ?></td>
                                    <td><?php echo ucfirst($arr['role']); ?></td>
                                    <td><?php echo $arr['status'] ? 'Active' : 'Inactive'; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <a href="<?php echo Router::url('user/' . $arr['id']); ?>" title="Edit User" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="7">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>