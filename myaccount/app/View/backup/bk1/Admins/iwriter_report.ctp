<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>iWriter Posting Report</span>
                </h4>
            </div>
            <div class="content">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($itemList)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th class="zeroWidth"></th>
                            <th class="textRight">Month Year</th>
                            <th class="textRight">Premium</th>
                            <th class="textRight">Prime</th>
                            <th class="textRight">Lowgrade</th>
                            <th class="textRight">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($itemList) && count($itemList) > 0):
                            $monthArr = array();
                            foreach ($itemList as $month => $arr):
                                ?>
                                <tr>
                                    <td class="zeroWidth"></td>
                                    <td class="textRight"><?php echo $month; ?></td>
                                    <td class="textRight"><?php echo isset($arr[3]) ? $arr[3] : 0; ?></td>
                                    <td class="textRight"><?php echo isset($arr[4]) ? $arr[4] : 0; ?></td>
                                    <td class="textRight"><?php echo isset($arr[5]) ? $arr[5] : 0; ?></td>
                                    <td class="textRight"><?php echo isset($arr['total']) ? $arr['total'] : 0; ?></td>
                                </tr>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td colspan="3">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div>