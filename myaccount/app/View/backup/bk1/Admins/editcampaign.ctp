<div class="heading">
	<h3>Edit Campaign</h3>
</div><!-- End .heading-->
<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
	<h4>Edit A Campaign Data</h4>
	<p>edit your campaign information below. You must have to enter campaing name and root domain name</p>

</div>
<div class="content">
	<?php
		$controller = $this->params['controller'];
		$action = $this->params['action'];
		echo $this->Form->create(null, array('url' => array('controller' => $controller, 'action' => $action), 'id'=>'editCampaignForm','method'=>'post'));?>
	<div class="form-row row-fluid">
		<div class="spa6">
			<div class="row-fluid">
				<label class="form-label span3" for="normal">Name Of the Campaign *</label>
				<?php echo $this->Form->input('Campaign.campignname',array('type'=>'text', 'required'=>false,  'title'=>'Name Of the Campaign', 'div'=>false,'label'=>false,'class'=>'span3' )); ?>


			</div>
		</div>
	</div>

	<div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
	<div id="settingContainer">
		<table cellpadding="4" cellspacing="6">
			<thead>
				<tr>
                    <th style="width: 25px;">&nbsp;</th>
					<th class="span1" style="width: 105px;color:#31BE26;">
						Target  (<span class="yellow bold" id="totalPercentageAddedShow">100</span>) %
						<input type="text" name="data[Campaign][tPercentage]" value="100" id="totalPercentageAdded" style="width:1px;height:1px;visibility:hidden;">
					</th>
					<th>Anchor Text <span class="help-block-inline">comma(,) seperated</span></th>
					<th>Internal Url <span class="help-block-inline">comma(,) seperated and sequential to left anchor text</span></th>
				</tr>
			</thead>
			<tbody id="campInputRows">

				<?php
					if ( count($data['Anchortext']) > 0 ) {
						$i = 1;
						$allIds = array();
						foreach ( $data['Anchortext'] as $k => $anchorText ) {
							array_push($allIds,$anchorText['id']);

						?>
						<tr id="<?php echo $i; ?>" class="sControl">
							<td valign="top" align="center" class="span1">
								<span title="Remove this Row" rel="<?php echo $anchorText['id']; ?>" class="icon12 icomoon-icon-remove adminControl"></span>
							</td>
							<td valign="top" align="center">
								<?php echo $this->Form->input('Campaign.anchorData.targetDensity.'.$anchorText['id'],array('class'=>'span1 Percentage', 'type'=>'number', 'required'=>false,  'title'=>'numeric max 100', 'div'=>false,'label'=>false, 'maxlength'=>3, 'max' => 100, 'value' => $anchorText['targetDensity'],'style' => 'width:32px !important;' )); ?>
							</td>
							<td valign="top">
								<?php echo $this->Form->textarea('Campaign.anchorData.anchortexts.'.$anchorText['id'],array( 'title'=>'enter anchortexts under this campaign', 'div'=>false,'label'=>false, 'class'=>'span3 anchorText', 'style' => 'height:22px; min-height:22px',  'value' => $anchorText['anchortexts'] )); ?>
							</td>
							<td valign="top">
								<?php echo $this->Form->textarea('Campaign.anchorData.internalPageUrl.'.$anchorText['id'],array('title'=>'enter urls  under this campaign', 'div'=>false,'label'=>false, 'class'=>'span5 anchorUrls', 'style' => 'height:22px; min-height:22px',  'value' => $anchorText['internalPageUrl'] )); ?>
							</td>

						</tr>
						<?php
							$i++;
						}
					}else{
					?>
					<tr id="1" class="sControl">
						<td width="30" valign="middle"><span title="Remove this Row" class="icon12 icomoon-icon-remove adminControl"></span></td>
						<td valign="top">
							<?php echo $this->Form->input('Campaign.anchorData.targetDensity.new.',array('class'=>'span2 Percentage', 'type'=>'number', 'required'=>false,  'title'=>'numeric max 100', 'div'=>false,'label'=>false, 'maxlength'=>3, 'max' => 100 )); ?>
						</td>
						<td valign="top">
							<?php echo $this->Form->textarea('Campaign.anchorData.anchortexts.new.',array('title'=>'enter anchortexts under this campaign', 'div'=>false,'label'=>false, 'class'=>'span3 anchorText')); ?>
						</td>
						<td valign="top">
							<?php echo $this->Form->textarea('Campaign.anchorData.internalPageUrl.new.',array('title'=>'enter urls  under this campaign', 'div'=>false,'label'=>false, 'class'=>'span5 anchorUrls' )); ?>
						</td>
					</tr>
					<?php
					}
				?>



			</tbody>
		</table>

	</div>
	<div class="form-row row-fluid">
		<div class="spa6">
			<div class="row-fluid">
				<button class="btn btn-link btn-large" type="button" id="addMoreBtn">+ Add More</button>
			</div>
		</div>
	</div>
	<div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
	<div class="form-actions">
		<button class="btn btn-info send-middle" type="submit">Save Campaign</button>
	</div>
	<input type="hidden" name="data[Campaign][allIds]" value="<?php echo implode(",",$allIds); ?>" >
	<?php echo $this->Form->end(); ?>
</div>


<div class="marginB10"></div>

<script type="text/javascript">
	// document ready function
	$(document).ready(function() {
		$("#addMoreBtn").live('click',function(){
			$rows = $("#campInputRows");
			$rowsId =  parseInt($rows.find('tr:last').attr('id'));
			$rows.find('tr:last').clone().insertAfter($rows.find('tr:last'));
			$justInserted = $rows.find('tr:last');
			$justInserted.hide();
			$nId =  $rowsId + 1;
			$justInserted.attr('id',$nId);
			$justInserted.find('input,textarea').each(function(i,e){
				$(e).attr('id', $(e).attr('id') + $nId );
				//$(e).attr('name',  $(e).attr('name').replace('['+$rowsId+']', '[' + $nId + ']') );
                //alert($rowsId);
				$(e).attr('name',  $(e).attr('name').replace(/\d+/g, 'new')+'[]' );
                // make it required. Tell validation plugin to validate it
				$(e).rules('add', {'required': true})
			});
			$justInserted.find('input,textarea').val(''); // it may copy values from first one
			$justInserted.slideDown(500);
		});


		$(".sControl").live('mouseover',function(e){
			$(this).find("span.adminControl").show();
		});
		$(".sControl").live('mouseout',function(e){
			$(this).find("span.adminControl").hide();
		});

		$(".adminControl").live('click',function(e){
			e.preventDefault();
			$type = $(this).attr('rel');
			_this = $(this);
			if ( ! confirm("This action will remove anchor text, internal url from this row. \n Confirm to delete?") ) {return false;}
			$(this).parents("tr.sControl").eq(0).remove();
			// adjust percentage again
			eVal = 0;
			$(".Percentage").each(function(im,em){
				$(this).val( parseInt($(this).val()) || 0 );
				eVal += parseInt($(this).val()) || 0;
				$("#totalPercentageAdded").val(eVal);
				$("#totalPercentageAddedShow").html(eVal);
			});

			/*if ( $type == '_blank' ) {
				$(this).parents("tr.sControl").eq(0).remove();
			}else{
				if ( ! confirm("This action will remove anchor text, internal url from this row. \n Confirm to delete?") ) {return false;}
				$.ajax({
					type: 'get',
					url: '/seonitrov2/sadmins/delAnchorText/'+$type,
					//url: '/sadmins/delAnchorText/'+$type,
					beforeSend: function() {
						$("html").addClass('loadstate');
					}
				}).done(function(ret) {
					$("html").removeClass('loadstate');
					$(_this).parents("tr.sControl").eq(0).remove();
					// adjust percentage again
					eVal = 0;
					$(".Percentage").each(function(im,em){
						$(this).val( parseInt($(this).val()) || 0 );
						eVal += parseInt($(this).val()) || 0;
						$("#totalPercentageAdded").val(eVal);
						$("#totalPercentageAddedShow").html(eVal);
					});
				})
			}*/
		});


		$(".Percentage").live('keyup',function(e){
		 	 eVal = 0;
			_this = $(this);
			$(".Percentage").each(function(im,em){
				$(this).val( parseInt($(this).val()) || 0 );
				eVal += parseInt($(this).val()) || 0;
				l = $(this).val().length;
				if ( (eVal > 100)  ) {
                    $vv = $(this).val();
					$val = $(this).val().substring( 0, l-1 );
					$(this).val(  $val );
					eVal = eVal - $vv +  (parseInt($val) || 0);
					$('#totalPercentageAddedShow').parent().delay(100).fadeTo(100,0.5).delay(100).fadeTo(100,1).delay(100).fadeTo(100,0.5).delay(100).fadeTo(100,1).delay(100).fadeTo(100,0.5).delay(100).fadeTo(100,1);
				}
			});
			if ( eVal == 100 ) {
				$('#totalPercentageAddedShow').parent().css({color:'#31BE26'});
			}else{
				$('#totalPercentageAddedShow').parent().css({color:'#f34'});
			}
			//alert(eVal)
			$("#totalPercentageAdded").val(eVal);
			$("#totalPercentageAddedShow").html(eVal);

		});



		$("#editCampaignForm").validate({
			rules: {
				'data[Campaign][campignname]': {
					required: true,
					minlength: 4
				},
				'data[Campaign][tPercentage]': {
					required: true,
					min:100,
				},

			},
			messages: {
				'data[Campaign][campignname]': {
					required: "Please enter a Campaign Name",
					minlength: "Enter Minimum Length"
				},
				'data[Campaign][tPercentage]': {
					min: "must equal to 100"
				},
				/*'data[Campaign][settings][1][anchortexts]': {
				atLeastOneAnchorText: "Please insert at least one anchor text with minimum 4 chars long"
				}*/
			}

		});

		$(".Percentage,.anchorText,.anchorUrls").each(function (item) {
			$(this).rules("add", {
				required: true,
			});
		});






	});


</script>