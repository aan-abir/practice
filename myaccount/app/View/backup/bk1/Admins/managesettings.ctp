<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'managesettings', $data['Setting']['id']), 'id' => 'editSettingForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="sidebarlimit">Per Site Allowed Sidebar:</label>
                <?php echo $this->Form->input('Setting.sidebar', array('id' => 'sidebarlimit', 'title' => 'Sidebar Limit', 'placeholder' => "Sidebar Limit", 'div' => false, 'label' => false, 'class' => 'span1')); ?>
            </div>
        </div>
    </div>
    <input type="hidden" name="data[Setting][id]" value="<?php echo $data['Setting']['id']; ?>" >
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="footer">Per Site Allowed Footer:</label>
                <?php echo $this->Form->input('Setting.footer', array('id' => 'footer', 'title' => 'Footer Limit', 'placeholder' => "Footer Limit", 'div' => false, 'label' => false, 'class' => 'span1')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="guest_blog_own_percentage">Guest Blog Price Percentage for Client's Own Article:</label>
                <?php echo $this->Form->input('Setting.guest_blog_own_percentage', array('id' => 'guest_blog_own_percentage', 'title' => 'Guest Blog Price Percentage', 'placeholder' => "Guest Blog Price Percentage", 'div' => false, 'label' => false, 'class' => 'span1')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="pr_instant_check_limit_per_day">PR instant check limit per day:</label>
                <?php echo $this->Form->input('Setting.pr_instant_check_limit_per_day', array('id' => 'pr_instant_check_limit_per_day', 'title' => 'PR instant check limit per day', 'placeholder' => "PR instant check limit per day", 'div' => false, 'label' => false, 'class' => 'span1')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="instant_price_for_client">Instant Price per keyword for client:</label>
                <?php echo $this->Form->input('Setting.instant_price_for_client', array('id' => 'instant_price_for_client', 'title' => 'Instant Price per keyword for client', 'placeholder' => "Instant Price per keyword for client", 'div' => false, 'label' => false, 'class' => 'span1')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="instant_price">Instant Price per keyword:</label>
                <?php echo $this->Form->input('Setting.instant_price', array('id' => 'instant_price', 'title' => 'Instant price', 'placeholder' => "Instant price", 'div' => false, 'label' => false, 'class' => 'span1')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="keyword_price ">Keyword Price:</label>
                <?php echo $this->Form->input('Setting.keyword_price', array('id' => 'keyword_price', 'title' => 'Keyword price', 'placeholder' => "Keyword price", 'div' => false, 'label' => false, 'class' => 'span1')); ?>
            </div>
        </div>
    </div>
    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Update Settings</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>
