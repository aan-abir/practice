<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
    <h4>Edit education</h4>
    <p>edit education material</p>
</div>


<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'editlesson',$data['Education']['id']), 'id' => 'editEducationForm', 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>


    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Category <span class="help-block-inline">education category or subcategory</span></h4>
            </div>
            <div class="form-row">
                <div class="row-fluid">
                    <div class="span3">
                        <?php
                            echo $this->Form->select('Education.cat_id', array($cateTree), array('error' => false, 'required' => false, 'id' => 'user_id', 'div' => false, 'class' => 'span2 select', 'empty' => false));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>For Users <span class="help-block-inline">will show only these packages users</span></h4>
            </div>
            <div class="form-row">
                <div class="row-fluid">
                    <?php foreach ($allPackages as $k => $v ) : 
                            $id =  $v['Package']['id'];
                        ?>
                        <div class="left marginT5 marginR10">
                            <div class="checker" id="uniform-inlineCheckbox1">
                                <span class="checked">
                                    <!-- <input type="checkbox" id="inlineCheckbox1" value="option1" checked="checked" class="styled" style="opacity: 0;">-->
                                    <?php 
                                        $options = array('title' => 'Check to show for this group of users', 'required' => false, 'div' => false, 'label' => false, 'class' => 'span1 styled', 'id' => "inlineCheckbox{$id}", 'style' => 'opacity: 0;', 'default' =>0);
                                        if ( preg_match("/,$id,/",$data['Education']['packages']) ) {
                                            $options['checked'] = 'checked';

                                        }else{
                                            $options['checked'] = false;
                                        }

                                        echo $this->Form->checkbox('Education.package.' . $id, $options); ?>
                                </span>
                            </div> <?php echo $v['Package']['packagename']; ?>
                        </div>
                        <?php
                            endforeach;
                    ?> 
                </div>
            </div>
        </div>
    </div>




    <div class="form-row row-fluid">
        <div class="span12">

            <div class="page-header">
                <h4>Page Header <span class="help-block-inline">this will show as lessson header</span></h4>
            </div>
            <div class="form-row">
                <?php echo $this->Form->textarea('Education.title', array('error' => false, 'id' => 'title', 'title' => 'title', 'div' => false, 'label' => false, 'class' => 'span12', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:60px;' ) ); ?>
            </div>


        </div>
    </div>





    <div class="row-fluid">
        <div class="span12">
            <div class="page-header">
                <h4>Enter Body here  <span class="help-block-inline"> html enabled help text. Be aware about html tag completion</span></h4>
            </div>
            <div class="form-row">

                <?php echo $this->Form->textarea('Education.body', array('error' => false, 'id' => 'body', 'title' => 'Body', 'div' => false, 'label' => false, 'class' => 'span12 tinymce', 'rows' => 25, 'cols' => 20, 'style' => 'width:98%;height:500px;' ) ); ?>
            </div>
        </div>
    </div>  
    <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
    <div class="form-actions">
        <button class="btn btn-info send-middle" type="submit">Save Material</button>
    </div>

    <?php $this->Form->end(); ?>
</div>
<script type="text/javascript">

    //--------------- Tinymce ------------------//
    $('textarea.tinymce').tinymce({
        // Location of TinyMCE script
        script_url :  '<?php echo BASEURL; ?>plugins/forms/tiny_mce/tiny_mce.js',

        // General options
        theme : "advanced",
        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,styleselect,formatselect,fontselect,fontsizeselect,code,",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "css/main.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Replace values for the template plugin
        template_replace_values : {
            username : "SuprUser",
            staffid : "991234"
        }
    });

</script>