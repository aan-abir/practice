<div class="marginB10"></div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'instantranking'), 'id' => 'instantRankingForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="google">Google:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankinstant.google', array('error' => false, 'required' => false, 'id' => 'google', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>                            
                            <div style="float:left; padding: 0; margin: 0;">
                                <?php echo $this->Form->select('Rankinstant.glocale', $glocale, array('error' => false, 'required' => false, 'id' => 'glocale', 'div' => false, 'label' => false, 'empty' => false, 'value' => 'en-us', 'class' => '')); ?>
                            </div>
                        </div>
                        <span class="help-inline blue">Google *check the box and select location for google</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="yahoo">Yahoo:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankinstant.yahoo', array('error' => false, 'required' => false, 'id' => 'yahoo', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>                            
                            <div style="float:left; padding: 0; margin: 0;">
                                <?php echo $this->Form->select('Rankinstant.ylocale', $glocale, array('error' => false, 'required' => false, 'id' => 'ylocale', 'div' => false, 'label' => false, 'empty' => false, 'value' => 'en-us', 'class' => '')); ?>
                            </div>
                        </div>
                        <span class="help-inline blue">Yahoo *check the box and select location for yahoo</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="bing">Bing:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankinstant.bing', array('error' => false, 'required' => false, 'id' => 'bing', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>                            
                            <div style="float:left; padding: 0; margin: 0;">
                                <?php echo $this->Form->select('Rankinstant.blocale', $glocale, array('error' => false, 'required' => false, 'id' => 'blocale', 'div' => false, 'label' => false, 'empty' => false, 'value' => 'en-us', 'class' => '')); ?>
                            </div>
                        </div>
                        <span class="help-inline blue">Bing *check the box and select location for bing</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="amazon">Amazon:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankinstant.amazon', array('error' => false, 'required' => false, 'id' => 'amazon', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>                            
                        </div>
                        <span class="help-inline blue">Amazon *check the box to rank update from Amazon</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="youtube">Youtube:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankinstant.youtube', array('error' => false, 'required' => false, 'id' => 'youtube', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>                            
                        </div>
                        <span class="help-inline blue">Youtube *check the box to rank update from Youtube</span>
                    </div>
                </div>
            </div>            
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="c_keywords">Keywords:</label>
                        <div class="span4">
                            <?php echo $this->Form->textarea('Rankinstant.c_keywords', array('error' => false, 'required' => false, 'id' => 'c_keywords', 'title' => 'enter keywords you want to track under this campaign', 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                        </div>
                        <span class="help-inline blue">Enter Keywords *one per line. dupicate will be removed auto</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="c_urls">URLs:</label>
                        <div class="span4">
                            <?php echo $this->Form->textarea('Rankinstant.c_urls', array('error' => false, 'required' => false, 'id' => 'c_urls', 'title' => 'enter urls you want to track under this campaign', 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                        </div>
                        <span class="help-inline blue">Enter URLs *one per line. dupicate will be removed auto</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="email_update">Email Update:</label>
                        <div class="span4">
                            <div class="uniform" style="float:left; padding: 0; margin-right: 10px !important; margin-top: 7px !important;">
                                <?php echo $this->Form->checkbox('Rankinstant.email_update', array('error' => false, 'required' => false, 'id' => 'email_update', 'class' => 'style', 'title' => 'Select Search Engine', 'value' => 1, 'div' => false, 'label' => false)); ?>
                            </div>
                        </div>
                        <span class="help-inline blue">Check the box if you want to get update via email</span>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="e_email">Email Address:</label>
                        <?php echo $this->Form->input('Rankinstant.e_email', array('error' => false, 'required' => false, 'id' => 'e_email', 'type' => 'text', 'title' => 'Email Address', 'placeholder' => "Email Address", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="e_company">Company Name:</label>
                        <?php echo $this->Form->input('Rankinstant.e_company', array('error' => false, 'required' => false, 'id' => 'e_company', 'type' => 'text', 'title' => 'Company Name', 'placeholder' => "Company Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="e_contact">Contact Name:</label>
                        <?php echo $this->Form->input('Rankinstant.e_contact', array('error' => false, 'required' => false, 'id' => 'e_contact', 'type' => 'text', 'title' => 'Contact Name', 'placeholder' => "Contact Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="e_phone">Phone:</label>
                        <?php echo $this->Form->input('Rankinstant.e_phone', array('error' => false, 'required' => false, 'id' => 'e_phone', 'type' => 'text', 'title' => 'Phone', 'placeholder' => "Phone", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="e_weburl">Web URL:</label>
                        <?php echo $this->Form->input('Rankinstant.e_weburl', array('error' => false, 'required' => false, 'id' => 'e_weburl', 'type' => 'text', 'title' => 'Web URL', 'placeholder' => "Web URL", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="e_fmsg">Footer Message:</label>
                        <div class="span4">
                            <?php echo $this->Form->textarea('Rankinstant.e_fmsg', array('error' => false, 'required' => false, 'id' => 'e_fmsg', 'title' => 'Footer Message', 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                        </div>
                    </div>
                </div>
            </div>            
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="status">Campaign Status:</label>
                        <div class="left marginT5 marginR10">
                            <input type="radio" name="data[Rankinstant][status]" id="UserStatus1" value="1" checked="checked" />
                            Active
                        </div>
                        <div class="left marginT5 marginR10">
                            <input type="radio" name="data[Rankinstant][status]" id="UserStatus0" value="0" />
                            Inactive
                        </div>
                    </div>
                </div>
            </div>                
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                            <div class="span3"></div>
                            <div class="span4 controls">
                                <button type="submit" class="btn btn-info marginR10">Save Campaign</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                
            <?php $this->Form->end(); ?>
        </div><!-- End .span12 -->
    </div>
</div>