<div class="marginB10"></div>
<div class="content">
    <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($manualsites)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
        <thead>
            <tr>
                <th class="zeroWidth"></th>
                <th class="textLeft">Domain</th>
                <th>Theme/Title</th>
                <th>Post Count</th>
                <th>PR</th>
                <th title="Link Juice Pr" class="tip">L.J PR</th>
                <th title="Citation Flow" class="tip">C.Flow</th>
                <th title="Trust Flow" class="tip">T.Flow</th>
                <th title="Analyze this Doamin" class="tip">Analyze</th>
                <th title="Actions" class="tip">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (count($manualsites) > 0):
                foreach ($manualsites as $ms):
                    $u = str_replace('http://', '', $ms['Domain']['domain']);
                    $Url = 'http://www.' . $u;
                    ?>
                    <tr>
                        <td class="zeroWidth"></td>
                        <td class="textLeft"><a href="<?php echo $Url; ?>" target="_blank"><?php echo $ms['Domain']['domain']; ?></a></td>
                        <td><?php echo $ms['Domain']['title']; ?></td>
                        <td><?php echo $ms['Domain']['post_count']; ?></td>
                        <td><?php echo $ms['Domain']['pr']; ?></td>
                        <td><?php echo $ms['Domain']['ljpr']; ?></td>
                        <td><?php echo $ms['Domain']['cflow']; ?></td>
                        <td><?php echo $ms['Domain']['tflow']; ?></td>
                        <td>
                            <a title="Quick Summery of - <?php echo $ms['Domain']['domain']; ?> " href="<?php echo Router::url('quickviewdomain/' . $ms['Domain']['id']); ?>" class="openModalDialog">quick view</a>
                        </td>
                        <td>
                            <?php echo $this->Html->link('Add Post', 'manualpost?domain=' . $ms['Domain']['id']); ?>
                        </td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td class="zeroWidth"></td>
                    <td colspan="9">No record found!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    //--------------- Popovers ------------------//
    //using data-placement trigger
    $("a[rel=popover]").popover().click(function(e) {
        e.preventDefault();
    });

    //using js trigger
    $("a[rel=popoverTop]").popover({placement: 'top'}).click(function(e) {
        e.preventDefault();
    });
</script>
