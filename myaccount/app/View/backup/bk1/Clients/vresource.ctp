<?php
    $pack = array('21' => 'SEORockstars Recordings', '22' => 'SEORockstar NOTES', '23' => 'SEORockstars 2012/13 Recordings');
    $path = '<a href="http://rankratio.com/login/clients/resources">Education</a> &rArr; <a href="http://rankratio.com/login/clients/rday/'.$pId.'">'.$pack[$pId].'</a>  &rArr; Day '.$data['Recording']['day_number'].' &rArr; '.$data['Recording']['title'];
?>
<div class="marginT10"></div>
<div class="content">
    <div class="span12 clearfix"><h3 class="blue"><?php echo preg_replace($replace, $replacement, $data['Recording']['title']); ?></h3></div>
    <div class="clearfix span12" style="border-bottom: 1px dashed #c4c4c4; border-top: 1px dashed #c4c4c4;padding:5px 0 !important; ">
        <div class="span8"><span class="icon16   icomoon-icon-stack"></span><span style="color:#999;"><?php echo $path; ?></span></div>
    </div>
    <div class="span10 marginT10"><?php echo preg_replace($replace, $replacement, $data['Recording']['description']); ?></div>
    <hr>
    <?php
        if ( (in_array($pId, array(21,23))) && ($data['Recording']['ytvideo_id'] != '')  ) {
            echo '<div style="margin-bottom:30px;" class="clearfix span10"><iframe src="http://www.youtube.com/embed/'.$data['Recording']['ytvideo_id'].'" width="854" height="480" frameborder="0"></iframe></div><hr>';
        }
        if (  trim($data['Recording']['mindmap_file']) != '' ) {
            echo '<div class="clearfix span12 marginT10"><a style="font-size:20px;" href="http://rankratio.com/login/media/'.$data['Recording']['mindmap_file'].'" target="_blank"><img style="margin-right:10px;" src="http://rankratio.com/login/media/1418407586_icon_mindmap.png" alt="" />MindMap: '.$data['Recording']['speaker_name'].', Click Here To Download</a></div>';
        }
        if (  trim($data['Recording']['notes_file']) != '' ) {
            echo '<div class="clearfix span12 marginT10"><a style="font-size:20px;"  href="http://rankratio.com/login/media/'.$data['Recording']['notes_file'].'" target="_blank"><img style="margin-right:10px;" src="http://rankratio.com/login/media/1418407597_icon_notes.png" alt="" />Notes: '.$data['Recording']['speaker_name'].', Click Here To Download</a></div>';
        }

        if ( (in_array($pId, array(21,23)))  ) { // Recordings Downsell does NOT get the Recipes or slideshows in it. only the notes and mindmaps
            if (  trim($data['Recording']['slideshow_file']) != '' ) {
                echo '<div class="clearfix span12 marginT10"><a style="font-size:20px;"  href="http://rankratio.com/login/media/'.$data['Recording']['slideshow_file'].'" target="_blank"><img style="margin-right:10px;" src="http://rankratio.com/login/media/1418407618_icon_slides.png" alt="" />SlideShow: '.$data['Recording']['speaker_name'].', Click Here To Download</a></div>';
            }
            if (  trim($data['Recording']['recipe_file']) != '' ) {
                echo '<div class="clearfix span12 marginT10"><a style="font-size:20px;"  href="http://rankratio.com/login/media/'.$data['Recording']['recipe_file'].'" target="_blank"><img style="margin-right:10px;" src="http://rankratio.com/login/media/1418407607_icon_recipe.png" alt="" />Recipe: '.$data['Recording']['speaker_name'].', Click Here To Download</a></div>';
            }
        }
    ?>
</div>

<div class="marginB10 marginT10 span10"></div>



<script type="text/javascript">
    $(".fancypdf").click(function() {
        $.fancybox({
            type: 'html',
            autoSize: false,
            content: '<embed src="' + this.href + '#nameddest=self&page=1&view=FitH,0&zoom=80,0,0" type="application/pdf" height="99%" width="100%" />',
            beforeClose: function() {
                $(".fancybox-inner").unwrap();
            }
        }); //fancybox
        return false;
    }); //click

</script>