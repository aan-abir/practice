<div class="page-header">
    <h4>Change Your Profile Data</h4>
    <p>you can change your profile data below. <span class="yellow"> Leave the password field blank if no change need.</span>
    </p>
</div>
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'editprofile'), 'id' => 'profileUpdateForm', 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>




    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="password">Password <span class="help-block-inline">change your password</span></label>
                <div class="span8">
                    <?php echo $this->Form->input('UserEx.password', array('type' => 'text', 'value' => '', 'id' => 'password', 'title' => 'Enter Your Passord', 'placeholder' => "password", 'div' => false, 'label' => false, 'class' => 'span6')); ?>
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Your Contact Info <span class="help-block-inline">change your contact information</span></h4>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="firstname">First name <span class="help-block-inline">your first name</span></label>
                <div class="span8">
                    <?php echo $this->Form->input('User.firstname', array('type' => 'text', 'id' => 'firstname', 'title' => 'Cleint First Name', 'placeholder' => "Your First Name", 'div' => false, 'label' => false, 'class' => 'span6')); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="lastname">Last name <span class="help-block-inline">your last name</span></label>
                <div class="span8">
                    <?php echo $this->Form->input('User.lastname', array('type' => 'text', 'id' => 'lastname', 'title' => 'Cleint Last Name', 'placeholder' => "Your  Last Name", 'div' => false, 'label' => false, 'class' => 'span6')); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="email">Email <span class="help-block-inline">enter your valid email</span></label>
                <div class="span8">
                    <?php echo $this->Form->input('User.email', array('type' => 'text', 'id' => 'email', 'title' => 'Cleint Email', 'placeholder' => "Your  Email", 'div' => false, 'label' => false, 'class' => 'span6', 'readonly' => true)); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="phone">Phone <span class="help-block-inline">enter your phone number</span></label>
                <div class="span8">
                    <?php echo $this->Form->input('User.phone', array('type' => 'text', 'id' => 'phone', 'title' => 'Cleint Phone', 'placeholder' => "Your  Phone", 'div' => false, 'label' => false, 'class' => 'span6')); ?>
                </div>
            </div>
        </div>
    </div>


    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="span4" for="skype">Skype <span class="help-block-inline">your skype name we can use to contact you</span></label>
                <div class="span8">
                    <?php echo $this->Form->input('User.skype', array('type' => 'text', 'id' => 'skype', 'title' => 'Cleint Skype', 'placeholder' => "Your  Skype", 'div' => false, 'label' => false, 'class' => 'span6')); ?>
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4 style="color: #074f14;">Write More About You <span class="help-block-inline">add more information about you
                    </span></h4>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="form-row">
                <?php echo $this->Form->textarea('User.moreaboutclient', array('id' => 'moreaboutclient', 'title' => 'Your Additional Contact Info', 'div' => false, 'label' => false, 'class' => 'span11 limit elastic', 'rows' => 3, 'cols' => '5')); ?>
            </div>
        </div>
    </div>


    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Update Profile</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

<script type="text/javascript">
    // document ready function
    $(document).ready(function() {

        //------------- Elastic textarea -------------//
        if ($('textarea').hasClass('elastic')) {
            $('.elastic').elastic();
        }

        //$("input, textarea, select").not('.nostyle').uniform();

        /*$.validator.addMethod(
        "atLeastOneAnchorText",
        function(value, element) {
        //alert($(element).val().length);
        return ($(element).val().length < 4) ? false : true;
        },
        "Please insert at least one anchor text with minimum 4 chars long"
        );*/


        $("#ncForm").validate({
            rules: {
                'data[User][firstname]': {
                    required: true,
                },
                'data[User][email]': {
                    required: true,
                    email: true
                },
            },
            messages: {
                'data[User][firstname]': {
                    required: "Please Enter Firstname"
                },
                'data[User][email]': {
                    required: "Please Enter Email",
                    email: "Please Enter Valid Email",
                },
                /*'data[Campaign][settings][1][anchortexts]': {
                atLeastOneAnchorText: "Please insert at least one anchor text with minimum 4 chars long"
                }*/
            }

        });


    });


</script>