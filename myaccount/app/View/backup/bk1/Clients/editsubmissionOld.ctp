<script type="text/javascript" src="<?php echo BASEURL; ?>/tiny_mce_new/tiny_mce_gzip.js"></script>
<script language="javascript" src="<?php echo BASEURL; ?>/js-new/jquery.blockUI.js"></script>
<style>

    .synonym_i {
        width: 234px;
        cursor: hand; cursor: pointer;      
        background: #fff;
        height: 25px;
        padding: 0px 10px;
        border: 1px #cdcdcd solid;
        text-align: left;
    }

    .synonym_i a {
        color: #000;
        line-height: 25px;
    }

    .synonym_a {
        width: 234px;
        cursor: hand; cursor: pointer;     
        background: #7f7f7f;
        height: 25px;
        padding: 0px 10px;
        border: 1px #cdcdcd solid;
        text-align: left;
    }

    .synonym_a a {
        color: #e5e5e5;
        line-height: 25px;
    }

    .group {
        padding: 0px 5px;
        color: #000;
    }

    #thesaurus-box {
        width: 554px;                                                
    }

    #thesaurus-head {
        width: 100%;
        height: 80px;
        text-align: right;
        background: url(/imgz/thesaurus-bg.png) no-repeat top left;  
    }

    #thesaurus-mid {
        width: 100%;
        background: url(/imgz/thesaurus-mid.png) repeat-y top left;
    }

    #thesaurus-bottom {
        width: 100%;
        height: 25px;
        background: url(/imgz/thesaurus-bg.png) repeat-x bottom left;

    }

    #protected-box {
        width: 554px;                                                
    }

    #protected-head {
        width: 100%;
        height: 80px;
        text-align: right;
        background: url(/imgz/protected-bg.png) no-repeat top left;  
    }

    #protected-mid {
        width: 100%;
        background: url(/imgz/thesaurus-mid.png) repeat-y top left;
    }

    #protected-bottom {
        width: 100%;
        height: 25px;
        background: url(/imgz/protected-bg.png) repeat-x bottom left;

    }

    #syn-box {
        width: 284px;                                                
    }

    #syn-head {
        width: 100%;
        height: 15px;
        text-align: right;
        background: url(/imgz/synonyms-bg.png) no-repeat top left;  
    }

    #syn-mid {
        width: 100%;
        background: url(/imgz/synonyms-mid.png) repeat-y top left;
    }

    #syn-bottom {
        width: 100%;
        height: 25px;
        background: url(/imgz/synonyms-bg.png) repeat-x bottom left;

    }

    .td1 {
        line-height: 20px;
        height: 20px;
        background: #fafafa;
        border: 1px #fff solid;
        text-align: left;
    }

    .td0 {
        line-height: 20px;
        height: 20px;
        background: #eaeaea;
        border: 1px #fff solid;
        text-align: left;
    }

    #textfield-2 {
        width: 126px;
        height: 28px;

        float: left;

        background-image: url(/imgz/field2.png);
        background-repeat: no-repeat;
        background-position: top left;        

        text-align: center;
        padding: 0px;

        margin: 10px 0px 0px 5px;
    }

    #textfield-2 input {
        border: 0px; 
        width: 118px;
        height: 22px;
        line-height: 22px;
        font-size: 13px;
        text-align: left;
        background: transparent;
        margin: 2px 0px 0px 0px;
        color: #333; 
    }

    #textfield-3 {
        width: 250px;
        height: 28px;

        float: left;

        background-image: url(/imgz/field3.png);
        background-repeat: no-repeat;
        background-position: top left;        

        text-align: center;
        padding: 0px;

        margin: 10px 0px 0px 5px;
    }

    #textfield-3 input {
        border: 0px; 
        width: 242px;
        height: 22px;
        line-height: 22px;
        font-size: 13px;
        text-align: left;
        background: transparent;
        margin: 2px 0px 0px 0px;
        color: #333; 
    }


</style>

<script language="javascript">
    var original = '';
    var TheWord = '';
    var SelectedSynonyms = new Array();
    var AllSynonyms = new Array();
    var AllFavs = new Array();
    var AllProtected = new Array();

    // Detect if the browser is IE or not.
    // If it is not IE, we assume that the browser is NS.
    var IE = document.all?true:false

    // If NS -- that is, !IE -- then set up for mouse capture
    if (!IE) document.captureEvents(Event.MOUSEMOVE)

    // Set-up to use getMouseXY function onMouseMove
    document.onmousemove = getMouseXY;

    // Temporary variables to hold mouse x-y pos.s
    var tempX = 0
    var tempY = 0

    // Main function to retrieve mouse x-y pos.s

    function getMouseXY(e) {
        if (IE) { // grab the x-y pos.s if browser is IE
            tempX = event.clientX + document.body.scrollLeft
            tempY = event.clientY + document.body.scrollTop
        } else {  // grab the x-y pos.s if browser is NS
            tempX = e.pageX
            tempY = e.pageY
        }  
        // catch possible negative values in NS4
        if (tempX < 0){tempX = 0}
        if (tempY < 0){tempY = 0}  
        // show the position values in the form named Show
        // in the text fields named MouseX and MouseY
        //document.Show.MouseX.value = tempX
        //document.Show.MouseY.value = tempY
        return true
    }

    function AddRemoveSynonym(syno) { 
        var syno2 = syno.replace(/\s+/g, '');
        var mydiv = $('#syn_'+syno2).get(0);
    
        if (mydiv.className == 'synonym_i') {
        
            SelectedSynonyms.push(syno);
            mydiv.className = 'synonym_a';
        } else {
            var temp = new Array();
        
            for (var i = 0; i < SelectedSynonyms.length; i++) {
                if (SelectedSynonyms[i] != syno) {
                    temp.push(SelectedSynonyms[i]);
                }
            }
            SelectedSynonyms = temp;
            temp = null;
            mydiv.className = 'synonym_i';
        }
        //alert(SelectedSynonyms.join('|'));
    }

    function SynonymsDone() {
        if (SelectedSynonyms.length == 0) {
            alert('Please select at least one word...'); return;
        }
        var sel = tinyMCE.activeEditor.selection.getContent();
        var space = '';
        if (sel[sel.length-1] == ' ') {
            space = ' ';
        }
        tinyMCE.activeEditor.selection.setContent('{' + SelectedSynonyms.join('|') + '}' + space);
        //    alert(tinyMCE.activeEditor.selection.getContent());
        //    return;
    
        $('#favs').get(0).style.display = 'none';
        var mydiv = $('#synonyms').get(0);
        mydiv.innerHTML = '';
        $('#handlers').get(0).style.display = 'none'; 
    }

    function SynonymsCancel() {
        //alert(SelectedSynonyms.join('|'));
        $('#favs').get(0).style.display = 'none';
        var mydiv = $('#synonyms').get(0);
        mydiv.innerHTML = '';
        $('#handlers').get(0).style.display = 'none'; 
    }

    function AddNewSynonym() {
        var mydiv = $('#synonyms').get(0);   
    
        var newguy = $('#newsyn').get(0);
        var newguyname = newguy.value;
        newguyname = newguyname.replace(/\s+/g, '');
        AllSynonyms.push(newguy.value);
                
        var newdiv = document.createElement('div');
        newdiv.className = 'synonym_i';
        newdiv.id = 'syn_' + newguyname;
        newdiv.innerHTML = '<a href="javascript: AddRemoveSynonym(\'' + newguy.value + '\')">' + newguy.value + '</a>';
    
        newguy.value = '';
    
        mydiv.appendChild(newdiv);
    }

    function SetFavs() {
        var synonyms = SelectedSynonyms.join('|');
        $.post('/post.php', {what: 'SetFavs', word: TheWord, words: synonyms}, 
        function(data){
              
            if (data == 1) {            
                if (window.confirm('Done! Replace these in current text?')) {
                    SynonymsDone();   
                    var mydiv = $('#synonyms').get(0);
                    mydiv.innerHTML = '';
                    $('#handlers').get(0).style.display = 'none';     
                    $('#favs').get(0).style.display = 'none';     
                } else {
                    SynonymsCancel();
                }
            } else {
                alert('Sorry, there was an error');
            }
        });
    }

    function CancelManageFavs() {
        AllFavs = null;
        AllFavs = new Array();
    
        $('#myfavs').get(0).innerHTML = '';
        $('#myfavs').get(0).style.display = 'block';
    
        $('#managefavsx').get(0).style.display = 'none';
    }

    function UpdateManageFavs() {
    
        var temp = new Array();
    
        for (var i = 0; i < AllFavs.length; i++) {
            temp.push(AllFavs[i].join('|'));
        }
    
        var MyFavs = temp.join('||');
        //    alert(MyFavs);
        $.post("/post.php", { what: "UpdateManageFavs", favs: MyFavs },
        function(data){ 
            if (data == 1) {
                CancelManageFavs();            
            } else {
                alert('Sorry, error');
            }
        }
    );
    
    }

    function AddNewFavorite() {
        var newstring = '';
    
        var fav1 = $('#newfav1').get(0);
        var fav2 = $('#newfav2').get(0);
        
        var temp = new Array();
        var SynVal = '- not set -';
        if (fav1.value != '') { SynVal = fav1.value; }
        temp.push(SynVal);
    
    
        SynVal = '- not set -';
        if (fav2.value != '') { 
            var f2 = fav2.value.split(',');
    
            for (var i = 0; i < f2.length; i++) {
                temp.push($.trim(f2[i]));
            }    
        } else {
            temp.push(SynVal);
        }                       
    
        AllFavs.push(temp);
    
        ListFavorites();  
    
        fav1.value = 'New Word';
    
        fav2.value = 'Synonyms';  
    
    }

    function ListFavorites() {
        $('#myfavs').get(0).innerHTML = '';
    
        var TheOtherFavs = new Array();
    
        for (var i = 0; i < AllFavs.length; i++) {
            TheOtherFavs = null;                        
            TheOtherFavs = new Array();                        
        
            var newdiv = document.createElement('div');
        
            newdiv.className = 'group';
            newdiv.id = 'gr_' + i;
        
            if (i % 2 == 1) {
                var theclass = 'td1';
            } else { var theclass = 'td0'; }
        
            if (AllFavs[i].length > 1) {
                for (var j = 1; j < AllFavs[i].length; j++) {
                    TheOtherFavs.push(AllFavs[i][j]);
                }
                var FavsString = TheOtherFavs.join(', ');
            } else {
                var FavsString = '- not set -';
            }
        
            var myhtml = '';
            myhtml = '<table width="" cellpadding="0" cellspacing="0"><tr>';
            myhtml += '<td class="'+theclass+'" width="113" valign="middle">&nbsp;&nbsp;'+AllFavs[i][0]+'</td>';
            myhtml += '<td class="'+theclass+'" width="390" valign="middle">&nbsp;&nbsp;'+FavsString+'</td>';
            myhtml += '<td class="'+theclass+'" width="30" align="center" valign="middle"><a href="javascript: RemoveFav('+i+');"><img src="/imgz/delete-btn.png" border="0" /></a></td>';
            myhtml += '</tr></table>';
        
            newdiv.innerHTML = myhtml;
            //                            newdiv.innerHTML = AllFavs[i][0] + ' = ' + AllFavs[i].join(',') + '&nbsp;&nbsp; <a href="javascript: RemoveFav('+i+');">x</a>';
        
            $('#myfavs').get(0).appendChild(newdiv);    
        }
    
        $('#LoadingMessage').get(0).style.display = 'none';
    }

    function CancelProtectedW() {
        AllProtected = null;
        AllProtected = new Array();
    
        $('#protectedws').get(0).innerHTML = '';
        $('#protectedws').get(0).style.display = 'block';
    
        $('#protectedw').get(0).style.display = 'none';
    }

    function RemoveFav(x) {
        if (!window.confirm("Are you sure?")) {
            return;
        }
        var temp = new Array();
        for (var i = 0; i < x; i++) {
            temp.push(AllFavs[i]);
        }
    
        for (var i = x+1; i < AllFavs.length; i++) {
            temp.push(AllFavs[i]);
        }
        AllFavs = temp;
        temp = null;
    
        ListFavorites();
    }

    function RemoveProtected(x) {
        if (!window.confirm("Are you sure?")) {
            return;
        }
        var temp = new Array();
        for (var i = 0; i < x; i++) {
            temp.push(AllProtected[i]);
        }
    
        for (var i = x+1; i < AllProtected.length; i++) {
            temp.push(AllProtected[i]);
        }
        AllProtected = temp;
        temp = null;
    
        ListProtectedWords();
    }

    function UpdateProtectedW() {
              
        var MyFavs = AllProtected.join('|');
        //    alert(MyFavs);
        $.post("/post.php", { what: "UpdateProtectedWords", protected: MyFavs },
        function(data){ 
            if (data == 1) {
                CancelProtectedW();            
            } else {
                alert('Sorry, error');
            }
        }
    );
    
    }

    function ListProtectedWords() {
        $('#protectedws').get(0).innerHTML = ''; 
    
        for (var i = 0; i < AllProtected.length; i++) {
                            
            var newdiv = document.createElement('div');
        
            newdiv.className = 'group';
            newdiv.id = 'gr_' + i;
        
            if (i % 2 == 1) {
                var theclass = 'td1';
            } else { var theclass = 'td0'; }
        
            var myhtml = '';
            myhtml = '<table width="" cellpadding="0" cellspacing="0"><tr>';
            myhtml += '<td class="'+theclass+'" width="505" valign="middle">&nbsp;&nbsp;'+AllProtected[i]+'</td>';     
            myhtml += '<td class="'+theclass+'" width="30" align="center" valign="middle"><a href="javascript: RemoveProtected('+i+');"><img src="/imgz/delete-btn.png" border="0" /></a></td>';
            myhtml += '</tr></table>';
        
            newdiv.innerHTML = myhtml;
        
            $('#protectedws').get(0).appendChild(newdiv);    
        }    
    }

    function AddNewProtectedW() {               
    
        var newguy = $('#addnewprotectedword').get(0);
        AllProtected.push(newguy.value);
    
        newguy.value = 'Add New Protected Word';
                
        ListProtectedWords();
    }

</script>



<div id="managefavsx" style="width: 554px; display: none; z-index: 99;">
    <div id="thesaurus-box">
        <div id="thesaurus-head">
            <a href="javascript: UpdateManageFavs();"><img src="/imgz/save-changes-btn.png" border="0" style="margin-top: 10px;" /></a>
            <a href="javascript: CancelManageFavs();"><img src="/imgz/cancel-btn.png" border="0" style="margin-top: 10px; margin-right: 20px;" /></a>
        </div>
        <div id="thesaurus-mid">                                                                
            <div id="myfavs" style="width: 100%;"></div>


            <div style="width: 538px; margin: 10px 5px 0px 5px; height: 50px; background: url(/imgz/blue-bg.png) top left repeat-x;">
                <div id="textfield-2" style="margin: 10px 0px 0px 5px;">
                    <input type="text" name="newfav1" id="newfav1" value="New Word" onclick="javascript: if (this.value == 'New Word') {this.value = '';}">
                </div>
                <div id="textfield-3" style="margin: 10px 10px 0px 10px;">
                    <input type="text" name="newfav2" id="newfav2" value="Synonyms" onclick="javascript: if (this.value == 'Synonyms') {this.value = '';}">
                </div>
                <a href="javascript: AddNewFavorite();"><img src="/imgz/add-new-favorite-btn.png" border="0" style="margin: 10px 0px 0px 0px;" /></a>

            </div>  

            <div style="width: 530px; margin: 10px 5px 0px 15px;">

                <small>When done with adding new synonyms, click on Save Changes button.</small>
            </div>

        </div>    
        <div id="thesaurus-bottom"></div>
    </div>

</div>

<div id="protectedw" style="width: 554px; display: none; z-index: 99;">
    <div id="protected-box">
        <div id="protected-head">

            <a href="javascript: UpdateProtectedW();"><img src="/imgz/save-changes-btn.png" border="0" style="margin-top: 10px;" /></a>
            <a href="javascript: CancelProtectedW();"><img src="/imgz/cancel-btn.png" border="0" style="margin-top: 10px; margin-right: 20px;" /></a>
        </div>
        <div id="protected-mid">                                                                
            <div id="protectedws" style="width: 100%;"></div>
            <div style="width: 100%">                                                                                                    
                <div style="width: 538px; margin: 10px 5px 0px 5px; height: 50px; background: url(/imgz/blue-bg.png) top left repeat-x;">
                    <div id="textfield-3" style="margin: 10px 0px 0px 10px;">
                        <input type="text" id="addnewprotectedword" value="Add New Protected Word" onclick="javascript: if (this.value == 'Add New Protected Word') {this.value = '';}">
                    </div>

                    <a href="javascript: AddNewProtectedW()"><img src="/imgz/add-new-word-btn.png" border="0" style="margin: 10px 0px 0px 10px;" /></a>
                </div>  
            </div>

            <div style="width: 530px; margin: 10px 5px 0px 15px;">
                <small>When done with adding words, click on Save Changes button.</small>
            </div>
        </div>
        <div id="protected-bottom"></div>
    </div>


</div>

<div id="favs" style="display: none; z-index: 99;">
    <div id="syn-box">
        <div id="syn-head"></div>
        <div id="syn-mid">
            <div style="width: 264px; margin: 0px 10px;">                                                                
                <div id="synonyms"></div>                                          
            </div>

            <div style="width: 269px; height: 50px; background: url(/imgz/blue-bg.png) top left no-repeat; margin-left: 5px; margin-top: 10px;">
                <div id="textfield-2">

                    <input type="text" id="newsyn">
                </div>
                <a href="javascript: AddNewSynonym();"><img src="/imgz/add-new-synonym-btn.png" style="margin: 10px 0px 0px 7px;" border="0" /></a>
            </div>

            <center>
                <div id="handlers">   
                    <a href="javascript: SetFavs();"><img src="/imgz/save-as-favorites-btn.png" style="margin: 10px 0px 0px 0px;" border="0" /></a> 

                    <br />

                    <a href="javascript: SynonymsDone();"><img src="/imgz/done-btn.png" style="margin: 10px 0px 0px 0px;" border="0" /></a> 
                    <a href="javascript: SynonymsCancel();"><img src="/imgz/cancel2-btn.png" style="margin: 10px 0px 0px 0px;" border="0" /></a> 
                </div>    
            </center>

        </div>
        <div id="syn-bottom"></div>
    </div>    
</div>


<script type="text/javascript"> 
    tinyMCE_GZ.init({
        plugins : 'directionality,fullscreen,inlinepopups,linkvine,media,nonbreaking,noneditable,paste,spellchecker,visualchars,wordcount,xhtmlxtras',
        themes : 'simple,advanced',
        languages : 'en',
        disk_cache : true,
        debug : false        
    });
    tinyMCE.init({
        // General options
        mode:"textareas", editor_deselector : "mceNoEditor", width:"100%", theme:"advanced",
        plugins : "safari,inlinepopups,spellchecker,media,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,wordcount,linkvine",
 
        // Theme options
        theme_advanced_buttons1:"bold,italic,strikethrough,|,bullist,numlist,blockquote,|,justifyleft,justifycenter,justifyright,|,link,unlink,wp_more,|,spellchecker,fullscreen,|,rewrite1,rewrite2,rewrite3,rewrite4,rewrite5,rewrite6",
        theme_advanced_buttons2:"formatselect,underline,justifyfull,forecolor,|,pastetext,pasteword,removeformat,|,media,charmap,|,outdent,indent,|,undo,redo,|,code,|,", 
        theme_advanced_buttons3:"", 
        theme_advanced_buttons4:"",

        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,
        
        spellchecker_languages:"+English=en,Danish=da,Dutch=nl,Finnish=fi,French=fr,German=de,Italian=it,Polish=pl,Portuguese=pt,Spanish=es,Swedish=sv",
 
        // Example content CSS (should be your site CSS)
        //        content_css : "editor.css",
 
        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",
 
        // Style formats
        style_formats : [
            {title : 'Bold text', inline : 'b'},
            {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
            {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
            {title : 'Example 1', inline : 'span', classes : 'example1'},
            {title : 'Example 2', inline : 'span', classes : 'example2'},
            {title : 'Table styles'},
            {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
        ],
 
        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
    });
</script> 



<div class="content">
    <?php
    if (isset($data) && isset($data['Submission'])):
        ?>
        <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'editsubmission', $data['Submission']['id']), 'id' => 'addSubmissionForm', 'method' => 'post')); ?>
        <div class="form-row row-fluid">
            <div class="span6">
                <div class="row-fluid">
                    <label class="form-label span6" for="normal">Name of the Submission *</label>
                    <?php echo $this->Form->input('Submission.submissionname', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Name Of the Submission', 'div' => false, 'label' => false, 'class' => 'span6')); ?>
                </div>
            </div>
        </div>
        <?php echo $this->form->hidden('Submission.id', array('value' => $data['Submission']['id'])); ?>
        <div class="form-row row-fluid">
            <div class="span6">
                <div class="row-fluid">
                    <label class="form-label span6" for="normal">Start Date *</label>
                    <?php echo $this->Form->input('Submission.startdate', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Start Date', 'div' => false, 'label' => false, 'class' => 'span6 datepicker', 'readonly' => 'readonly')); ?>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span6">
                <div class="row-fluid">
                    <label class="form-label span6" for="normal">End Date *</label>
                    <?php echo $this->Form->input('Submission.enddate', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'End Date', 'div' => false, 'label' => false, 'class' => 'span6 datepicker', 'readonly' => 'readonly')); ?>
                </div>
            </div>
        </div>    
        <div class="form-row row-fluid">
            <div class="span6">
                <div class="row-fluid">
                    <label class="form-label span6" for="normal">How many times to post *</label>
                    <?php echo $this->Form->input('Submission.howmanytopost', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Name Of the Submission', 'div' => false, 'label' => false, 'class' => 'span2')); ?>
                    <!--span class="blue" style="display: inline-block; float: right">[ Max Possible Post - < ?php echo $tAutoSite; ?> ]</span-->
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span12">
                <div class="row-fluid">
                    <label class="form-label span3" for="normal">Select Submission Type *</label>
                    <style>.radio-div{padding:6px 0}.radio-div label{display: inline;} .sradio{display:inline;width:16px!important;}</style>
                    <div class="span6 radio-div">
                        <?php
                        $rAttributes = array('legend' => false, 'class' => 'sradio');
                        echo $this->Form->radio('Submission.submissiontype', $rOptions, $rAttributes);
                        if (count($rOptions) == 1):
                            ?>
                            <span class="help-inline blue">you don't have any private network yet</span>              
                            <?php
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span6">
                <div class="row-fluid">
                    <label class="form-label span6" for="username">PR Network:</label>
                    <div class="span6">
                        <?php
                        echo $this->Form->select('Submission.pr_service_id', $services, array('required' => false, 'title' => 'Select PR Network', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-row row-fluid">
            <div class="span9">
                <div class="row-fluid">
                    <label class="form-label span4" for="normal">Title of the Post *</label>
                    <?php echo $this->Form->input('Submission.posttitle', array('error' => false, 'type' => 'text', 'required' => false, 'title' => 'Title of the Post', 'div' => false, 'label' => false, 'class' => 'span8')); ?>
                </div>
            </div>
        </div>    
        <div class="row-fluid">
            <div class="span12">
                <div class="page-header">
                    <h4>Enter your Article below</h4>
                </div>
                <div class="form-row">
                    <center>          
                        <div id="LoadingMessage" style="display: none;">
                            <table>
                                <tr>
                                    <td valign="middle" align="center"><img src="<?php echo BASEURL; ?>images/ajax-loader.gif" border="0" /></td>
                                    <td valign="middle" align="center">Loading, please wait ...</td>
                                </tr>

                            </table>
                        </div>
                    </center>
                    <?php echo $this->Form->textarea('Submission.postcontent', array('id' => 'theEditor', 'rows' => '25', 'cols' => '80', 'style' => 'width: 886px')); ?>
                </div>
            </div>
        </div>
        <div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
        <div class="form-actions">
            <button class="btn btn-info send-middle" type="submit">Submit to Update</button>
        </div>
        <?php echo $this->Form->end(); ?>
        <?php
    endif;
    ?>
</div>
<div class="marginB10"></div>

<script type="text/javascript">
    //------------- Datepicker -------------//
    if($('.datepicker').length) {
        $('.datepicker').datepicker({
            showOtherMonths:true,
            //dateFormat: 'yy-m-d'
            minDate: '+1'
        });
    }    
</script>
