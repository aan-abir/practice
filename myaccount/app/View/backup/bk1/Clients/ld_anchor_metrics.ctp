<?php
$total_urls = $data['Ldreport']['GetTopBackLinksAnalysisResUnitsCost'];
$anchortext_graph = $this->NitroCustom->anchortext_graph($data['Ldreport']['id']);
$text_colorCode = array(
    '0' => '#754DEB',
    '1' => '#2473A6',
    '2' => '#F18800',
    '3' => '#88A72A',
    '4' => '#3F4857',
    '5' => '#DFD80C',
    '6' => '#851B1B',
    '7' => '#3E1B85',
    '8' => '#1B7285',
    '9' => '#FCD202',
    '10' => '#B0DE09',
    '11' => '#0D52D1',
    '12' => '#2A0CD0',
    '13' => '#8A0CCF',
    '14' => '#CD0D74',
);
$anchor_text_script = '';
$total_persent = 0;
foreach ($anchortext_graph as $text_index => $text) {
    $AnchorText = $text['ldreportcron']['AnchorText'];
    $AnchorText = addslashes($AnchorText);
    $total = $text[0]['total'];
    $text_color = $text_colorCode[$text_index];
    $text_per = $link_to_innerpage_persentage = $this->NitroCustom->link_ratio($total_urls, $total);
    $total_persent += $text_per;
    $anchor_text_script .= "{ label: '$AnchorText',  data: $text_per, color:'$text_color'},";
}

$AnchorText = 'Others';
$other_links = 100 - $total_persent;
$text_color = $text_colorCode[14];
$anchor_text_script .= "{ label: '$AnchorText',  data: $other_links, color:'$text_color'},";
//echo $anchor_text_script; exit;
// */
//pr($anchor_text_script); exit;
?>
<div class="clear_both"></div>
<h3>Anchor Metrics for <?php echo $data['Ldreport']['domain'] ?></h3>
<div class="clear_both"></div>
<?php
if ($total_persent == 0) {
    echo '<h4> Data will be updated within 24 hours.</h4>';
} else {
    $all_brand = $this->NitroCustom->brand($data['Ldreport']['id'], $data['Ldreport']['domain'], 'brand');
    $all_url = $this->NitroCustom->brand($data['Ldreport']['id'], $data['Ldreport']['domain'], 'url');
    ?>
    <h3><span style="color:<?php echo $text_colorCode[2]; ?>">Brand:</span>  <span style="color:<?php echo $text_colorCode[1]; ?>"><?php echo $this->NitroCustom->link_ratio($total_urls, $all_brand); ?>%</span> </h3>
    <h3><span style="color:<?php echo $text_colorCode[2]; ?>">Url:</span>  <span style="color:<?php echo $text_colorCode[1]; ?>"><?php echo $this->NitroCustom->link_ratio($total_urls, $all_url); ?>%</span></h3>
    <script>
        // document ready function
        $(document).ready(function() {

            var divElement = $('div'); //log all div elements

            //Boostrap modal
            $('#myModal').modal({show: false});

            //add event to modal after closed
            $('#myModal').on('hidden', function() {
                console.log('modal is closed');
            })


            //Link to anchor Matrics 
            if (divElement.hasClass('anchormatrics')) {
                $(function() {
                    var data = [<?php echo $anchor_text_script; ?>];

                    $.plot($(".anchormatrics"), data,
                            {
                                series: {
                                    pie: {
                                        show: true,
                                        highlight: {
                                            opacity: 0.1
                                        },
                                        radius: 1,
                                        stroke: {
                                            color: '#fff',
                                            width: 2
                                        },
                                        startAngle: 2,
                                        combine: {},
                                        label: {
                                            show: true,
                                            radius: 1,
                                            formatter: function(label, series) {
                                                //return '<div class="pie-chart-label">'+label+'&nbsp;'+Math.round(series.percent)+'%</div>';
                                                return '';
                                            }
                                        }
                                    },
                                    grow: {active: false}
                                },
                                legend: {show: false},
                                grid: {
                                    hoverable: true,
                                    clickable: true
                                },
                                tooltip: true, //activate tooltip
                                tooltipOpts: {
                                    content: "%s : %y.1" + "%",
                                    shifts: {
                                        x: -30,
                                        y: -50
                                    }
                                }
                            });
                });
            }//end if


        });//End document ready functions

        //generate random number for charts
        randNum = function() {
            //return Math.floor(Math.random()*101);
            return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
        }

        var chartColours = ['#88bbc8', '#ed7a53', '#9FC569', '#bbdce3', '#9a3b1b', '#5a8022', '#2c7282'];

        //sparkline in sidebar area
        var positive = [1, 5, 3, 7, 8, 6, 10];
        var negative = [10, 6, 8, 7, 3, 5, 1]
        var negative1 = [7, 6, 8, 7, 6, 5, 4]

        $('#stat1').sparkline(positive, {
            height: 15,
            spotRadius: 0,
            barColor: '#9FC569',
            type: 'bar'
        });
        $('#stat2').sparkline(negative, {
            height: 15,
            spotRadius: 0,
            barColor: '#ED7A53',
            type: 'bar'
        });
        $('#stat3').sparkline(negative1, {
            height: 15,
            spotRadius: 0,
            barColor: '#ED7A53',
            type: 'bar'
        });
        $('#stat4').sparkline(positive, {
            height: 15,
            spotRadius: 0,
            barColor: '#9FC569',
            type: 'bar'
        });
        //sparkline in widget
        $('#stat5').sparkline(positive, {
            height: 15,
            spotRadius: 0,
            barColor: '#9FC569',
            type: 'bar'
        });

        $('#stat6').sparkline(positive, {
            width: 70, //Width of the chart - Defaults to 'auto' - May be any valid css width - 1.5em, 20px, etc (using a number without a unit specifier won't do what you want) - This option does nothing for bar and tristate chars (see barWidth)
            height: 20, //Height of the chart - Defaults to 'auto' (line height of the containing tag)
            lineColor: '#88bbc8', //Used by line and discrete charts to specify the colour of the line drawn as a CSS values string
            fillColor: '#f2f7f9', //Specify the colour used to fill the area under the graph as a CSS value. Set to false to disable fill
            spotColor: '#e72828', //The CSS colour of the final value marker. Set to false or an empty string to hide it
            maxSpotColor: '#005e20', //The CSS colour of the marker displayed for the maximum value. Set to false or an empty string to hide it
            minSpotColor: '#f7941d', //The CSS colour of the marker displayed for the mimum value. Set to false or an empty string to hide it
            spotRadius: 3, //Radius of all spot markers, In pixels (default: 1.5) - Integer
            lineWidth: 2//In pixels (default: 1) - Integer
        });
    </script>

    <div class="tab-pane">
        <div class="clear_both"></div>
        <div class="span6" style="width: 50%;">
            <div class="box chart">
                <div class="content" style="background-color: transparent !important;border-style: none;">
                    <div class="anchormatrics" style="width: 508px; height: 270px;"></div>
                    <div class="clear_both"></div>
                </div>
            </div>
        </div>
        <div style="float: left;width:47%;background-color: transparent !important;">
    <?php
    foreach ($anchortext_graph as $text_index => $text) {
        $AnchorText = $text['ldreportcron']['AnchorText'];
        $total = $text[0]['total'];
        $text_color = $text_colorCode[$text_index];
        $text_per = $link_to_innerpage_persentage = $this->NitroCustom->link_ratio($total_urls, $total);
        ?>
                <div style="float:left;font-weight: bold;width: 35%;">
                    <span style="width:17px;height:17px;background-color: <?php echo $text_color; ?>;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <?php echo $AnchorText; ?>
                </div>
                <div style="float:left;width: 35%;">
                    <?php echo $text_per; ?>%
                </div>
                <div class="clear_both"></div>
                    <?php
                }
                ?> 
            <div style="float:left;font-weight: bold;width: 35%;">
                <span style="width:17px;height:17px;background-color: <?php echo $text_colorCode[14]; ?>;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <?php echo 'Others'; ?>
            </div>
            <div style="float:left;width: 35%;">
                <?php echo $other_links; ?>%
            </div>
        </div>
    </div>
                <?php
            }
            ?>