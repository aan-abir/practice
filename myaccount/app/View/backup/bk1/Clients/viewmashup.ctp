<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>Social MashUp ID</th>
                <th>Post URL</th>
                <th>Post Permalink</th>
                <th>Post Date</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (is_array($postHistory) && count($postHistory) > 0):
                foreach ($postHistory as $mashup):
                    $url = 'http://' . str_replace('http://', '', $mashup['Socialposthistory']['post_url']);
                    ?>
                    <tr>
                        <td><?php echo $mashup['Socialposthistory']['social_user_id']; ?></td>
                        <td><a target="_blank" href="<?php echo $url; ?>"><?php echo $mashup['Socialposthistory']['post_url']; ?></td>
                        <td>

                            <?php
                            $postUrl = $mashup['Socialposthistory']['post_url'];
                            if ($mashup['Socialposthistory']['post_link']):
                                $postUrl = $mashup['Socialposthistory']['post_link'];
                            endif;
                            ?>
                            <a target="_blank" href="http://<?php echo $mashup['Socialposthistory']['post_url']; ?>"><?php echo $postUrl; ?></a>

                        </td>
                        <td><?php echo date("M d, Y", strtotime($mashup['Socialposthistory']['created'])); ?></td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td colspan="4">No MashUp posted Yet!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>
</div>
