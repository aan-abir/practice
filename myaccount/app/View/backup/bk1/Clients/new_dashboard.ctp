<div style="margin-bottom: 20px;"></div>

<div class="row-fluid">
    <div class="span12">
        <div class="" style="position: relative;">
            <div dir="ltr" class="circle-stats">
                <div id="autoPremium">
                    <span>
                        Welcome news goes here!
                    </span>
                </div>
            </div>
        </div>
    </div><!-- End .span12 -->
</div>

<div class="row-fluid" style="margin-top: 20px;">
    <div class="span8">
        <div class="box calendar gradient">
            <div class="title">
                <h4>
                    <span class="icon16 icomoon-icon-calendar"></span>
                    <span>Previous, Current and Future Activity Log</span>
                </h4>
                <!-- <a href="#" class="minimize">Minimize</a> -->
            </div>
            <div class="content noPad">
                <div id="activityLog"></div>
            </div>

        </div><!-- End .box -->
    </div><!-- End .span8 -->

    <div class="span4">

    </div><!-- End .span4 -->
</div><!-- End .row-fluid -->

<script type="text/javascript">
    // document ready function
    $(document).ready(function() {

        //------------- Full calendar  -------------//
        $(function() {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            //front page calendar
            $('#activityLog').fullCalendar({
                //isRTL: true,
                //theme: true,
                header: {
                    left: 'title,today',
                    center: 'prev,next',
                    right: 'month,agendaWeek,agendaDay'
                },
                buttonText: {
                    prev: '<span class="icon24 icomoon-icon-arrow-left-2"></span>',
                    next: '<span class="icon24 icomoon-icon-arrow-right-2"></span>'
                },
                editable: true,
                events: [
<?php echo $calData; ?>
                ]
            });
        });

        //Boostrap modal
        $('#myModal').modal({show: false});

        //add event to modal after closed
        $('#myModal').on('hidden', function() {
            console.log('modal is closed');
        })


        //circular progrress bar
        $(function() {

            $(".greenCircle").knob({
                'min': 0,
                'max': 100,
                'readOnly': true,
                'width': 80,
                'height': 80,
                'fgColor': '#9FC569',
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true
            })
            $(".redCircle").knob({
                'min': 0,
                'max': 100,
                'readOnly': true,
                'width': 80,
                'height': 80,
                'fgColor': '#ED7A53',
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true
            })
            $(".blueCircle").knob({
                'min': 0,
                'max': 100,
                'readOnly': true,
                'width': 80,
                'height': 80,
                'fgColor': '#88BBC8',
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true
            })

        });
        $("#amclass").css({left: ($("#autoPremium").position().left - 5) + 'px'});
        $("#amclass").css({top: ($("#autoPremium").position().top - 20) + 'px'});
        $(window).resize(function() {
            $("#amclass").css({left: ($("#autoPremium").position().left - 5) + 'px'});
            $("#amclass").css({top: ($("#autoPremium").position().top - 20) + 'px'});
        });


    });

</script>