
<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>Name</th>
                <th>Anchors</th>
                <th>Services</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($orderList) && count($orderList) > 0):
                foreach ($orderList as $arr):
                    ?>
                    <tr>
                        <td><?php echo $arr['Linkemperor']['ordername']; ?></td>
                        <td>
                            <?php
                            $orders = unserialize($arr['Linkemperor']['orders']);
                            if (count($orders)):
                                ?>
                                <table style="width: 100%;">
                                    <tr>
                                        <th style="border-left: none;">Anchor Text</th>
                                        <th class="textLeft">Anchor URL</th>
                                    </tr>
                                    <?php
                                    foreach ($orders as $key => $order):
                                        ?>
                                        <tr>
                                            <td class="textLeft" style="border-left: none;"><?php echo $order['anchor_text']; ?></td>
                                            <td class="textLeft"><?php echo $order['url']; ?></td>
                                        </tr>
                                        <?php
                                    endforeach;
                                    ?>
                                </table>
                                <?php
                            endif;
                            ?>
                        </td>
                        <td>
                            <?php
                            $services = unserialize($arr['Linkemperor']['services']);
                            if (count($services)):
                                ?>
                                <table style="width: 100%;">
                                    <tr>
                                        <th class="textLeft" style="border-left: none;">Service</th>
                                        <th class="textLeft">Built</th>
                                        <th>Details</th>
                                        <th>Date</th>
                                    </tr>
                                    <?php
                                    foreach ($arr['Linkemperor']['results'] as $k => $d):
                                        ?>
                                        <tr>
                                            <td class="textLeft" style="border-left: none;"><?php echo $d['vendor']; ?></td>
                                            <td class="textLeft"><?php echo $d['status'] == 'completed' ? count(unserialize($d['built_urls'])) : 0; ?></td>
                                            <td class="textLeft">
                                                <?php
                                                if ($d['status'] == 'completed') {
                                                    ?>
                                                    <a title="Submission Details Of - <?php echo $d['service']; ?> " href="<?php echo Router::url('linkempdetails/' . $d['order_id'] . '/' . $d['service_id']); ?>" class="openModalDialog">details</a>
                                                    <?php
                                                } else {
                                                    echo 'In Process';
                                                }
                                                ?>


                                            </td>
                                            <td class="textLeft"><?php echo date('M d, Y', strtotime($d['created'])); ?></td>
                                        </tr>
                                        <?php
                                    endforeach;
                                    ?>
                                </table>
                                <?php
                            endif;
                            ?>
                        </td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="4">No Record Found!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>
</div>