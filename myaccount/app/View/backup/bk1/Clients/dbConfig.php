<?php

error_reporting(E_ALL);

$mailheaders = "MIME-Version: 1.0" . "\r\n";
$mailheaders .= "X-Priority: 1" . "\r\n";
$mailheaders .= "Content-Type: text/html; charset=\"iso-8859-1\"" . "\r\n";
$mailheaders .= "Content-Transfer-Encoding: 7bit" . "\r\n";
$mailheaders .= "From: seonitro.com <support@seonitro.com>" . "\r\n";
$mailheaders .= "X-Mailer: PHP/" . phpversion() . "\r\n";

define('MAIL_HEADERS', $mailheaders);
define('PR_ENABLED', true);

/* =======utility functions start======= */

function connect_db_root($dbConnection) {
    if (!$dbConnection) {
        return false;
    }
    return true;
}

function connect_db($dbConnection, $dbName) {
    //$dbConnection = mysql_connect('localhost', 'dorifrie_nitrov2', '@-!y8;sAQV)7');
    if (!$dbConnection) {
        return false;
    }
    $dB = mysql_select_db($dbName, $dbConnection);
    if (!$dB) {
        return false;
    }
    return true;
}

function close_db($dbConnection) {
    mysql_close($dbConnection);
}

function result_array($sql) {
    $result = array();
    $query = mysql_query($sql) or die("Error: " . mysql_error());
    while ($data = mysql_fetch_array($query)) {
        $result[] = $data;
    }
    $rows = count($result);
    if ($rows) {
        $total_global_rows = count($result);
        $total_inner_rows = count($result[0]);
        $count_total_inner_rows = $total_inner_rows / 2;

        for ($i = 0; $i < $total_global_rows; $i++) {
            for ($j = 0; $j < $count_total_inner_rows; $j++) {
                unset($result[$i][$j]);
            }
        }
    }
    return $result;
}

function row_array($sql) {
    $result = array();
    $query = mysql_query($sql);
    if (!$query) {
        return array("Error: " . mysql_error());
    }
    $data = mysql_fetch_assoc($query);
    return $data;
}

function query($sql) {
    $query = mysql_query($sql);
    if (!$query) {
        return array("Error: " . mysql_error());
    }
    return mysql_affected_rows();
}

function row_count($sql) {
    $count = 0;
    $result = mysql_query($sql);
    if (!$result) {
        return array("Error: " . mysql_error());
    }
    $count = mysql_num_rows($result);
    return (int) $count;
}

function insert($table, $data) {
    foreach ($data as $field => $value) {
        $fields[] = '`' . $field . '`';
        $values[] = "'" . mysql_real_escape_string($value) . "'";
    }
    $field_list = join(', ', $fields);
    $value_list = join(', ', $values);
    $query = "INSERT INTO `" . $table . "` (" . $field_list . ") VALUES (" . $value_list . ")";

    #dumpVar($query);
    $result = mysql_query($query);
    if (!$result) {
        return array("Error: " . mysql_error());
    }
    return mysql_insert_id();
}

function insert_batch($table, $dbCols, $dataArr) {
    $chunkArr = array();
    $arrSize = count($dataArr);
    $maxRows = 999;
    if ($arrSize > $maxRows) {
        $chunkCount = floor($arrSize / $maxRows);
        $chunkArr = array_chunk($dataArr, $chunkCount, true);
    } else {
        $chunkArr[] = $dataArr;
    }
    $affectedRows = 0;
    foreach ($chunkArr as $cA) {
        $allRowsArr = array();
        foreach ($cA as $key => $row) {
            $singleRowArr = array();
            foreach ($dbCols as $col) {
                $singleRowArr[] = "'" . mysql_real_escape_string($row[$col]) . "'";
            }
            $singleRowString = '(' . implode(',', $singleRowArr) . ')';
            $allRowsArr[] = $singleRowString;
        }
        $allRowsString = implode(',', $allRowsArr);
        $colums = implode(',', $dbCols);
        $sql = "INSERT INTO $table ($colums) VALUES $allRowsString";
        if (!mysql_query($sql)) {
            return array("Error: " . mysql_error());
        }

        $affectedRows += mysql_affected_rows();
    }
    return $affectedRows;
}

function update($table, $data, $index_array) {
    foreach ($data as $field => $value) {
        $fields[] = sprintf("%s = '%s'", $field, mysql_real_escape_string($value));
    }
    $field_list = join(',', $fields);
    foreach ($index_array as $field2 => $value2) {
        $fields2[] = sprintf("%s = '%s'", $field2, mysql_real_escape_string($value2));
    }
    $where = join(' AND ', $fields2);
    $query = sprintf("UPDATE %s SET %s WHERE %s", $table, $field_list, $where);
    $result = mysql_query($query);
    if (!$result) {
        return array("Error: " . mysql_error());
    }
    return mysql_affected_rows();
}

function delete($table, $column, $data) {
    $sql_delete = "DELETE FROM `$table` WHERE `$column` = '$data'";
    $result = mysql_query($sql_delete);
    if (!$result) {
        return array("Error: " . mysql_error());
    }
    return mysql_affected_rows();
}

function pr($data, $exit = false) {
    $print = true;
    if (defined('PR_ENABLED') && !PR_ENABLED) {
        $print = false;
    }
    if ($print) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
    if ($exit) {
        exit;
    }
}

function vr($data, $exit = false) {
    $print = true;
    if (defined('PR_ENABLED') && !PR_ENABLED) {
        $print = false;
    }
    if ($print) {
        echo '<pre>';
        var_export($data);
        echo '</pre>';
    }
    if ($exit) {
        exit;
    }
}

function getPlainDomain($url) {
    $url = str_replace('http://', '', strtolower($url));
    $url = str_replace('https://', '', $url);
    $plainDomain = str_replace('www.', '', $url);
    if (strpos($url, '/')) {
        $plainDomain = strstr($url, '/', true);
    }
    return $plainDomain;
}