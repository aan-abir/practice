<div class="marginB10"></div>
<div class="page-header">
    <h4>All your Guest Post </h4>
    <p>Below all the guest post you ordered but not yet in process. <span class="yellow">You can edit or delete an order before it gets process.</span></p>

</div>

<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>Market</th>
                <th>Title</th>
                <th>Content (500 chars)</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($guestPosts) && count($guestPosts) > 0):
                foreach ($guestPosts as $arr):
                    ?>
                    <tr>
                        <td><?php echo $arr['Guestpost']['postmarket']; ?></td>
                        <td><?php echo $arr['Guestpost']['title']; ?></td>
                        <td><?php echo substr(strip_tags($arr['Guestpost']['postdraft']), 0, 500); ?></td>
                        <td><?php echo $arr['Guestpost']['statuslabel']; ?></td>
                        <td>
                            <div class="controls center">
                                <a href="<?php echo Router::url('editguestpost/' . $arr['Guestpost']['id']); ?>" title="Edit Guest Post" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                <a href="<?php echo Router::url('deleteguestpost/' . $arr['Guestpost']['id']); ?>" title="Remove Guest Post Order?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="5">No Guestpost Found!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>
</div>
