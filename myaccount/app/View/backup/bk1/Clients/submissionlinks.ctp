<div class="marginB10"></div>
<div class="content">
    <div class="row-fluid">
        <div class="span12">
            <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
                <h4>
                    <span>Submission Title - <?php echo $submissionInfo['Submission']['submissionname']; ?> | <a href="<?php echo Router::url('submissionlinks_export/' . $submissionInfo['Submission']['id']); ?>">Export to CSV</a> | <a onclick="printReport(this);" href="#"><span class="icon16 icomoon-icon-printer-2"></span>Print</a></span>
                </h4>
            </div>
        </div>
    </div>
</div>

<div class="marginB10"></div>

<div class="row-fluid">
    <div class="span12">
        <div class="content">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($submissionReport)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th class="zeroWidth"></th>
                        <th class="textLeft">Post Permalink</th>
                        <th>Post Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (isset($submissionReport) && count($submissionReport) > 0):
                            foreach ($submissionReport as $k => $arr):
                            ?>
                            <tr>
                                <td class="zeroWidth"></td>
                                <td class="textLeft">
                                    <?php
                                        $postUrl = $arr['Submissionreport']['post_url'];
                                        if ($arr['Submissionreport']['post_link']):
                                            $postUrl = $arr['Submissionreport']['post_link'];
                                            endif;
                                    ?>
                                    <a target="_blank" href="http://<?php echo str_replace('http://', '', $arr['Submissionreport']['post_url']); ?>"><?php echo $postUrl; ?></a>
                                </td>
                                <td><?php echo date('Y-m-d', strtotime($arr['Submissionreport']['created'])); ?></td>
                                <td>
                                    <div class="controls center">
                                        <a href="<?php echo Router::url('deletesubmittedpost/' . $arr['Submissionreport']['id']); ?>" title="Remove Submitted Article?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                                endforeach;
                            else:
                        ?>
                        <tr>
                            <td class="zeroWidth"></td>
                            <td colspan="3">No record found!</td>
                        </tr>
                        <?php
                            endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="form-actions">
    <a class="btn btn-success send-middle tip callAction" href="<?php echo Router::url('mplinkindexing/' . $submissionInfo['Submission']['id']); ?>">
        <span class="icon16 icomoon-icon-checkmark white"></span>Send New Urls for Indexing?</a>
</div>
<iframe style="display: none;" name="submission_report" id="submission_report" src="<?php echo Router::url('submissionlinks_print/' . $submissionInfo['Submission']['id']); ?>"></iframe>
<script type="text/javascript">
    function printReport(e) {
        //e.preventDefault();
        window.frames["submission_report"].window.focus();
        window.frames["submission_report"].window.print();
    }
</script>


<!--div class="row-fluid">
<div class="span12">
< ?php
if (isset($submissionInfo['Submission'])):
?>
<div id="print_it" class="box">
<div class="title">
<h4>
<span>Submission Title - < ?php echo $submissionInfo['Submission']['submissionname']; ?> | <a href="< ?php echo Router::url('submissionlinks_export/' . $submissionInfo['Submission']['id']); ?>">Export to CSV</a> | <a onclick="printReport(this);" href="#"><span class="icon16 icomoon-icon-printer-2"></span>Print</a></span>
</h4>
</div>
<div class="content clearfix">
<table cellpadding="0" cellspacing="0" border="0" class="responsive < ?php if (count($submissionReport)) { ?> dynamicTable < ?php } ?>display table table-bordered" width="100%">
<thead>
<tr>
<th class="zeroWidth"></th>
<th class="textLeft">Post Permalink</th>
<th>Post Date</th>
<th>Action</th>
</tr>
</thead>
<tbody>
< ?php
if (isset($submissionReport) && count($submissionReport) > 0):
foreach ($submissionReport as $k => $arr):
?>
<tr>
<td class="zeroWidth"></td>
<td class="textLeft">
< ?php
$postUrl = $arr['Submissionreport']['post_url'];
if ($arr['Submissionreport']['post_link']):
$postUrl = $arr['Submissionreport']['post_link'];
endif;
?>
<a target="_blank" href="http://< ?php echo str_replace('http://', '', $arr['Submissionreport']['post_url']); ?>"><?php echo $postUrl; ?></a>
</td>
<td>< ?php echo date('Y-m-d', strtotime($arr['Submissionreport']['created'])); ?></td>
<td>
<div class="controls center">
<a href="< ?php echo Router::url('deletesubmittedpost/' . $arr['Submissionreport']['id']); ?>" title="Remove Submitted Article?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
</div>
</td>
</tr>
< ?php
endforeach;
else:
?>
<tr>
<td class="zeroWidth"></td>
<td colspan="3">No record found!</td>
</tr>
< ?php
endif;
?>
</tbody>
</table>
</div>
</div>
<div class="form-actions">
<a class="btn btn-success send-middle tip callAction" href="< ?php echo Router::url('mplinkindexing/' . $submissionInfo['Submission']['id']); ?>">
<span class="icon16 icomoon-icon-checkmark white"></span>Send New Urls for Indexing?</a>
</div>
<iframe style="display: none;" name="submission_report" id="submission_report" src="< ?php echo Router::url('submissionlinks_print/' . $submissionInfo['Submission']['id']); ?>"></iframe>
<script type="text/javascript">
function printReport(e) {
//e.preventDefault();
window.frames["submission_report"].window.focus();
window.frames["submission_report"].window.print();
}
</script>
< ?php
endif;
?>
</div>
</div-->






<div id="pagingPmb">
    <?php
        if ($this->Paginator->numbers() != '')
            echo '<em> Pages: </em>';
        if ($this->Paginator->hasPrev())
            echo $this->Paginator->prev(__('Previous '), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers();
        if ($this->Paginator->hasNext())
            echo $this->Paginator->next(__(' Next'), array(), null, array('class' => ' next disabled'));
    ?>
    <span> Total Posted: <?php echo $this->Paginator->counter('{:count}'); ?></span>
</div>
