<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">
        <div class="page-header" style="margin-bottom: 2px !important; margin-top: 20px !important;">
            <h4 style="color: #074f14;">All of your Press Release Articles</h4>
        </div>
    </div>
</div>

<div class="marginB10"></div>
<div class="row-fluid">
    <div class="content">
        <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($pressReleaseList)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
            <thead>
                <tr>
                    <th class="zeroWidth"></th>
                    <th class="textLeft">Headline</th>
                    <th class="textLeft">Summary</th>
                    <th class="textLeft">URL</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if (isset($pressReleaseList) && count($pressReleaseList) > 0):
                        foreach ($pressReleaseList as $item):
                            $arr = $item['Pressrelease'];
                        ?>
                        <tr>
                            <td class="zeroWidth"></td>
                            <td class="textLeft"><?php echo $arr['headline']; ?></td>
                            <td class="textLeft"><?php echo $arr['summary']; ?></td>
                            <td class="textLeft"><a href="<?php echo $arr['release_url']; ?>" target="_blank"><?php echo $arr['release_url']; ?></a></td>
                            <td>
                                <?php echo $arr['status']; ?>
                                <?php echo $arr['status'] == 'not-approved' ? '<br/><br/>' . $arr['status_msg'] : ''; ?>
                            </td>
                            <td>
                                <div class="controls center">
                                    <a href="<?php echo Router::url('viewpressrelease/' . $arr['id']); ?>" title="View Pressrelease" class="tip">View</a><br/>
                                    <a href="<?php echo Router::url('editpressrelease/' . $arr['id']); ?>" title="Edit Pressrelease" class="tip">Edit</a>
                                </div>
                            </td>
                        </tr>
                        <?php
                            endforeach;
                        else:
                    ?>
                    <tr>
                        <td class="zeroWidth"></td>
                        <td colspan="5">No record found!</td>
                    </tr>
                    <?php
                        endif;
                ?>
            </tbody>
        </table>
    </div>
</div>
