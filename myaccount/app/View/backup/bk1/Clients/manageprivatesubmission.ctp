<div class="content">
    <table class="responsive table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Start</th>
                <th>End</th>
                <th>How Many</th>
                <th>Title</th>
                <th>Article</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($submissions) && count($submissions) > 0):
                foreach ($submissions as $arr):
                    ?>
                    <tr>
                        <td></td>
                        <td><?php echo $arr['Submission']['submissionname']; ?></td>
                        <td><?php echo $arr['Submission']['startdate']; ?></td>
                        <td><?php echo $arr['Submission']['enddate']; ?></td>
                        <td><?php echo $arr['Submission']['howmanytopost']; ?></td>
                        <td><?php echo $arr['Submission']['posttitle']; ?></td>
                        <td><?php echo $this->NitroCustom->trimText($arr['Submission']['postcontent'], 300); ?></td>
                        <td><?php echo $arr['Submission']['statuslabel']; ?></td>
                        <td>
                            <div class="controls center">
                                <a href="<?php echo Router::url('editprivatesubmission/' . $arr['Submission']['id']); ?>" title="Edit Submission" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                endforeach;
            else:
                ?>
                <tr>
                    <td></td>
                    <td colspan="6">No Submission Found!</td>
                </tr>
            <?php
            endif;
            ?>
        </tbody>
    </table>
</div>