<?php

class NitroCustomHelper extends AppHelper {

    public function trimText($text = null, $length = 8, $showDots = true, $cutFirstWord = true) {
        $textArr = explode(' ', $text);
        if ($showDots) {
            $dotString = '...';
        } else {
            $dotString = '';
        }

        if ($length == 0) {
            echo ucfirst($text);
        } else {
            if (!$cutFirstWord && strlen($textArr[0]) > $length) {
                echo ucfirst($textArr[0]) . $dotString;
            } elseif (strlen($textArr[0]) > $length) {
                echo ucfirst(substr($textArr[0], 0, $length)) . $dotString;
            } else {
                if (strlen($text) > $length) {
                    $newText = substr($text, 0, $length);
                    $newText = substr($newText, 0, strrpos($newText, " "));
                    echo ucfirst($newText) . $dotString;
                } else {
                    echo ucfirst($text);
                }
            }
        }
    }

    function seoShuffle($items) {
        if ($items) {
            mt_srand(5);
            for ($i = count($items) - 1; $i > 0; $i--) {
                $j = @mt_rand(0, $i);
                $tmp = $items[$i];
                $items[$i] = $items[$j];
                $items[$j] = $tmp;
            }
        }
        return $items;
    }

    function number_of_domain($pr_campaigns_id) {
        $number_of_domain = ClassRegistry::init('Pruserdomain')->query("SELECT * FROM `pr_user_domain` WHERE `pr_campaigns_id` = " . $pr_campaigns_id);
        return count($number_of_domain);
    }

    function getIndustryList($itemId = null) {
        $industryList = array(
            '' => 'Select an Industry',
            '1' => 'Press Release',
            '10' => 'Art', '11' => 'Automotive',
            '12' => 'Banking &amp; Investment',
            '13' => 'Biotech',
            '14' => 'Books',
            '15' => 'Business',
            '16' => 'Chemicals',
            '17' => 'Comic Book',
            '18' => 'Computers',
            '19' => 'Construction &amp; Building',
            '20' => 'Consumer',
            '21' => 'Consumer Electronics',
            '22' => 'Discoveries',
            '23' => 'Economic',
            '24' => 'Education',
            '25' => 'Elections',
            '26' => 'Entertainment',
            '27' => 'Environmental Services',
            '28' => 'Family',
            '29' => 'Fashion',
            '30' => 'Financial',
            '31' => 'Food &amp; Beverage',
            '32' => 'Gaming &amp; Casinos',
            '33' => 'Gifts &amp; Collectables',
            '34' => 'Government',
            '35' => 'Health',
            '36' => 'Healthcare &amp; Hospitals',
            '37' => 'Home &amp; Garden',
            '38' => 'Hotel &amp; Resorts',
            '39' => 'Household, Consumer &amp; Cosmetics',
            '40' => 'Insurance',
            '41' => 'Interior Design',
            '42' => 'Internet &amp; New Media',
            '43' => 'Labor',
            '44' => 'Legal',
            '45' => 'Legislation',
            '46' => 'Lifestyle',
            '47' => 'Machinery',
            '48' => 'Media',
            '49' => 'Medical &amp; Pharmaceuticals',
            '50' => 'Mining &amp; Metals',
            '51' => 'Movies',
            '52' => 'Music',
            '53' => 'New Public Offerings',
            '54' => 'Non Profit',
            '55' => 'Oil &amp; Gas',
            '56' => 'Open Source',
            '57' => 'People',
            '58' => 'Performing Arts',
            '59' => 'Politics',
            '60' => 'Public Relations',
            '61' => 'Publishing',
            '62' => 'Radio',
            '63' => 'Real Estate',
            '64' => 'Religion',
            '65' => 'Retail',
            '66' => 'Semiconductors',
            '67' => 'Software',
            '68' => 'Sports',
            '69' => 'Technology',
            '70' => 'Telecom',
            '71' => 'Television',
            '72' => 'Trade Shows',
            '73' => 'Transportation',
            '74' => 'Travel',
            '75' => 'US Hispanic',
            '76' => 'Utilities',
            '77' => 'Video Games'
        );
        if ($itemId) {
            return $industryList[$itemId];
        }
        return $industryList;
    }

    function link_ratio($total_link, $cal_link) {
        //echo $total_link; exit;
        return number_format(($cal_link * 100) / $total_link, 1);
    }

    function link_count($column_name, $value) {
        $total_val = 0;
        $value = explode("|", $value);
        foreach ($value as $i => $val) {
            if ($i != 0) {
                $total_val = $total_val + $val;
            }
        }
        return $total_val;
    }

    function link_array($value) {
        $dead_link_arr = explode("|", $value);
        //return $dead_link_array;
        foreach ($dead_link_arr as $i => $val) {
            if ($i != 0) {
                $dead_link_array[] = $val;
            }
        }
        return $dead_link_array;
    }

    function link_to_homepage($id, $domain) {
        //$domain = 'espncricinfo.com';
        $sql = "SELECT * FROM `ldreportcron` WHERE `SourceURL` LIKE 'http://" . $domain . "/' OR `SourceURL` LIKE 'http://www." . $domain . "/' OR `SourceURL` LIKE 'http://www." . $domain . "' OR `SourceURL` LIKE 'https://" . $domain . "/' OR `SourceURL` LIKE 'https://www." . $domain . "/' OR `SourceURL` LIKE 'https://www." . $domain . "' ";
        $domain = ClassRegistry::init('Ldreportcron')->query($sql);
        return count($domain);
    }

    function top_id($domain) {
        $user_id = AuthComponent::user('id');
        $sql = "SELECT * FROM `ldreport` WHERE `domain` = '$domain' AND `user_id` = $user_id ORDER BY id DESC LIMIT 1";
        //echo $sql; exit;
        $id = ClassRegistry::init('Ldreport')->query($sql);
        if (!empty($id)) {
            $id = $id[0]['ldreport']['id'];
        } else {
            $id = 0;
        }
        return $id;
    }

    function same_domain_count($domain) {
        $user_id = AuthComponent::user('id');
        $sql = "SELECT COUNT(*) as total FROM `ldreport` WHERE `domain` = '$domain' AND `user_id` = $user_id";
        $id = ClassRegistry::init('Ldreport')->query($sql);
        //pr($id); exit;
        return $id[0][0]['total'];
    }

    function anchortext_graph($id) {
        $sql = "SELECT `AnchorText`,count(*) as `total` FROM `ldreportcron` WHERE `ldreport_id` = $id AND `AnchorText` != '' group by `AnchorText` order by `total` desc limit 9 ";
        return ClassRegistry::init('Ldreportcron')->query($sql);
    }

    function pagerank_graph($id) {
        $sql = "SELECT `pr`,count(*) as `total` FROM `ldreportcron` WHERE `ldreport_id` = $id AND `pr` != '' group by `pr` order by `pr` desc limit 10";
        return ClassRegistry::init('Ldreportcron')->query($sql);
    }

    function linktype($id) {
        $sql = "SELECT `LinkType`,count(*) as `total` FROM `ldreportcron` WHERE `ldreport_id` = $id AND `LinkType` != '' group by `LinkType` order by `total` desc limit 3";
        return ClassRegistry::init('Ldreportcron')->query($sql);
    }

    function all_yes_index($id) {
        $sql = "SELECT `pr` FROM `ldreportcron` WHERE `ldreport_id` = $id AND `index` = '1'";
        $all_yes_index = ClassRegistry::init('Ldreportcron')->query($sql);
        return count($all_yes_index);
    }

    function optimal_range() {
        $sql = "SELECT * FROM ld_optimal_range LIMIT 1";
        return ClassRegistry::init('Ldoptimalrange')->query($sql);
    }

    function brand($ldreport_id, $domain, $type) {
        if ($type == 'brand') {
            $ex_domain = explode(".", $domain);
            $domain = $ex_domain[0];
            $sql = "SELECT * FROM `ldreportcron` WHERE AnchorText = '$domain'";
        } else {
            $ex_domain = explode(".", $domain);
            $domain = $ex_domain[0];
            $sql = "SELECT * FROM `ldreportcron` WHERE ldreport_id = $ldreport_id AND AnchorText != '$domain' AND AnchorText LIKE '%.%'";
        }

        //echo $sql; exit;
        $all_data = ClassRegistry::init('Ldreportcron')->query($sql);
        return count($all_data);
    }

    function usDate($date) {
        return date('F d, Y', strtotime($date));
    }

    function rCount($table = null, $cond = null) {
        $sql = "SELECT count(*) as `total` FROM `$table` WHERE $cond ";
        $data = ClassRegistry::init('Domain')->query($sql);
        return $data[0][0]['total'];
    }

}
