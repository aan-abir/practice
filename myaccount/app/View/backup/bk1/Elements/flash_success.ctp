<div class="alert alert-success" style="clear: both;">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<strong><?php echo $message; ?></strong>
</div>