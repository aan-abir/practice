
<!--------------------- Cron Report Start -------------------------------------------->

<div class="row-fluid">
    <div class="span6" style="width: 100%;">
        <div style="margin-bottom: 20px;">
            <ul id="myTab1" class="nav nav-tabs">
                <li class="active"><a href="#home1" data-toggle="tab">External</a></li>
                <li><a href="#profile1" data-toggle="tab">Internal</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade in active" id="home1">
                    <a style="position: absolute; left: 50%; z-index: 20" title="Export all the domains" class="btn btn-link tip" target="_blank" href="<?php echo Router::url('exportdomainstatus'); ?>"><span class="icon16 icomoon-icon-file-excel"></span> Export List </a>
                    <div class="content">
                        <table cellpadding="0" cellspacing="0" border="0" id="sTable" class="responsive display table table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th><strong>Anchor Text</strong></th>
                                    <th><strong>URL</strong></th>
                                    <th><strong>First Seen</strong></th>
                                    <th><strong>Indexed</strong></th>
                                    <th><strong>Page Rank</strong></th>
                                    <th><strong>LinkType</strong></th>
                                    <th><strong>Do Follow</strong></th>
                                    <th><strong>Citation Flow</strong></th>
                                    <th><strong>Trust Flow</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="tab-pane fade" id="profile1">
                    <div class="content clearfix">
                        <table cellpadding="0" cellspacing="0" border="0" id="sTable_internal" class="responsive display table table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th><strong>Anchor Text</strong></th>
                                    <th><strong>URL</strong></th>
                                    <th><strong>First Seen</strong></th>
                                    <th><strong>Indexed</strong></th>
                                    <th><strong>Page Rank</strong></th>
                                    <th><strong>LinkType</strong></th>
                                    <th><strong>Do Follow</strong></th>
                                    <th><strong>Citation Flow</strong></th>
                                    <th><strong>Trust Flow</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>

<!--------------------- Cron Report End -------------------------------------------->



<script type="text/javascript">
    $(document).ready(function() {
        $('#sTable').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "iDisplayLength": 25,
            "aLengthMenu": [10, 25, 50, 75, 100, 250, 500],
            "bPaginate": true,
            "aoColumns": [
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
            ],
            "sAjaxSource": "./domains_ajax/1",
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                //if ( iDisplayIndex == 4 ) {
                $td = $('td:eq(4)', nRow);
                $span = $($td).find('span');
                //alert($span.attr('title'));
                $($td).attr('title', $span.attr('title')).addClass('tip');

                $($('td:eq(4)', nRow)).qtip({
                    content: $span.attr('title'),
                    position: {
                        my: 'bottom center',
                        at: 'top center',
                        viewport: $(window)
                    },
                    style: {
                        classes: 'qtip-tipsy'
                    }
                });
                $('td:eq(8)', nRow).css('text-align', 'left !important');
                //}
            }
        });

        //top tooltip


    });

    $(document).ready(function() {
        $('#sTable_internal').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "iDisplayLength": 25,
            "aLengthMenu": [10, 25, 50, 75, 100, 250, 500],
            "bPaginate": true,
            "aoColumns": [
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
                {"sType": "html"},
            ],
            "sAjaxSource": "./domains_ajax/2",
            "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

                //if ( iDisplayIndex == 4 ) {
                $td = $('td:eq(4)', nRow);
                $span = $($td).find('span');
                //alert($span.attr('title'));
                $($td).attr('title', $span.attr('title')).addClass('tip');

                $($('td:eq(4)', nRow)).qtip({
                    content: $span.attr('title'),
                    position: {
                        my: 'bottom center',
                        at: 'top center',
                        viewport: $(window)
                    },
                    style: {
                        classes: 'qtip-tipsy'
                    }
                });
                $('td:eq(8)', nRow).css('text-align', 'left !important');
                //}
            }
        });

        //top tooltip


    });
</script>