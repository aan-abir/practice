<div class="marginB10"></div>
<div class="page-header">
    <h4>Clients who has access to Recording</h4>
    <p>Bellow all the client who has access to recording resouces. To take necessary action choose action under actions tab.</p>
</div>
<div class="row-fluid">
    <div class="span12">

        <div class="content">
            <table cellpadding="0" cellspacing="0" border="0" class="responsive <?php if (count($user)) { ?> dynamicTable <?php } ?>display table table-bordered" width="100%">
                <thead>
                    <tr>
                        <th>User ID</th>
                        <th>User Name</th>
                        <th>Client Name</th>
                        <th>Email</th>
                        <th>Allowed Packages</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                        if (count($user) > 0) {
                            foreach ($user as $k => $u) {

                            ?>
                            <tr>
                                <td><?php echo $u['User']['id']; ?></td>
                                <td><a target="_blank" href="<?php echo DOMAIN . '/users/letmeinwithoutanyusernameandpassword/' . $u['User']['id']; ?>" title="Login into User Account" class="tip"><?php echo $u['User']['username']; ?></a></td>
                                <td><?php echo  $u['User']['firstname'].' '.$u['User']['lastname']; ?></td>
                                <td><?php echo  $u['User']['email']; ?></td>
                                <td style="text-align: left;" id="u_<?php echo $u['User']['id']; ?>"><?php
                                        $p = array('21' => 'SEORockstars Recordings', '22' => 'SEORockstar NOTES', '23' => 'SEORockstars 2012/13 Recordings');
                                        $rp = array();
                                        if ( !empty($u['Ruser'])  ) {
                                            foreach ( $u['Ruser'] as $kk => $ru ) {
                                                $rp[] = $ru['package_id'];
                                            }
                                        }

                                        foreach ( $p as $pId => $pName ) {
                                            if ( in_array($pId, $rp) ) {
                                                echo '<input type="checkbox"  class="nostyle pckg" checked="checked" value="'.$pId.'"> '.$pName.'<br> ';

                                            }else{
                                                echo '<input type="checkbox" class="nostyle pckg" value="'.$pId.'"> '.$pName.' <br>';
                                            }
                                        }
                                    ?>
                                </td>
                                <td>
                                    <div class="controls center">
                                        <button rel="<?php echo $u['User']['id']; ?>" title="Update Access?" class="tip btn rlink">Update</button>
                                    </div>
                                </td>
                                <!--<td><a href="< ?php echo adminHome . '/managemysites/' . $camp['User']['id']; ?>" title="Manage Mysites" class="tip">Manage</a></td>-->
                            </tr>

                            <?php
                            }
                        } else {
                            echo '<tr><td colspan="5">No Client Found!</td></tr>';
                        }
                    ?>



                </tbody>
            </table>
        </div>

    </div><!-- End .span6 -->
</div>
<script type="text/javascript">
$('.rlink').click(function(){
     if ( !confirm("Are You sure") ) return 0;
    $ids = '';
    tdId = $(this).attr("rel");
    $("#u_"+tdId).find('input:checkbox').each(function(){
        if ( $(this).is(":checked") ) {
           $ids += $(this).val() + ',';
       }
    });
    window.location.replace('http://rankratio.com/login/sadmins/recordingaccess/' + tdId + '/' + $ids);
});

</script>

