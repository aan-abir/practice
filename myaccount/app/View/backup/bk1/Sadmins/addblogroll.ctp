<div class="heading">
	<h3>Add New BlogRoll</h3>
</div><!-- End .heading-->
<div class="marginB10"></div>
<?php echo $this->Session->flash(); ?>
<div class="page-header">
	<h4>Enter New Blogroll links</h4>
	<p>please enter your blogroll name and links to add and show in your site. </p>

</div>
<div class="content">
	<?php   echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'addblogroll'), 'id'=>'addBlogrollForm','method'=>'post'));?>

	<div class="form-row row-fluid">
		<div class="span6">
			<div class="row-fluid">
				<label class="form-label span6" for="blogrollname">Name Of the Blogroll *</label>
				<?php echo $this->Form->input('Blogroll.blogrollname',array('type'=>'text', 'error' => false, 'required'=>false,  'title'=>'Name Of the Blogroll', 'div'=>false,'label'=>false,'class'=>'span6' )); ?>


			</div>
		</div>
	</div>

	<div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
	<div id="settingContainer">
		<table cellpadding="4" cellspacing="6">
			<thead>
				<tr>

					<th>Anchor <span class="help-block-inline">text for url</span></th>
					<th>Url <span class="help-block-inline"> link for anchor</span></th>
				</tr>
			</thead>
			<tbody id="campInputRows">

				<tr id="1">
					<td valign="top">
						<?php echo $this->Form->input('Blogroll.settings.1.anchor',array('type'=>'text', 'error' => false, 'title'=>'enter anchortexts', 'div'=>false,'label'=>false, 'class'=>'span4 anchorText' )); ?>
					</td>
					<td valign="top">
						<?php echo $this->Form->input('Blogroll.settings.1.link',array('type'=>'text', 'error' => false, 'title'=>'enter urls  for anchors', 'div'=>false,'label'=>false, 'class'=>'span5 anchorUrls' )); ?>
					</td>
				</tr>



			</tbody>
		</table>

	</div>
	<div class="form-row row-fluid">
		<div class="spa6">
			<div class="row-fluid">
				<button class="btn btn-link btn-large" type="button" id="addMoreBtn">+ Add More</button>
			</div>
		</div>
	</div>
	<div class="marginB10" style="border-bottom: 1px solid #eee;"></div>
	<div class="form-actions">
		<button class="btn btn-info send-middle" type="submit">Add New Blogroll</button>
	</div>
	<?php echo $this->Form->end(); ?>
</div>


<div class="marginB10"></div>

<script type="text/javascript">
	// document ready function
	$(document).ready(function() {
		$("#addMoreBtn").live('click',function(){
			$rows = $("#campInputRows");
			$rowsId =  parseInt($rows.find('tr:last').attr('id'));
			$rows.find('tr:last').clone().insertAfter($rows.find('tr:last'));
			$justInserted = $rows.find('tr:last');
			$justInserted.hide();
			$nId =  $rowsId + 1;
			$justInserted.attr('id',$nId);
			$justInserted.find('input,textarea').each(function(i,e){
				$(e).attr('id', $(e).attr('id') + $nId );
				$(e).attr('name',  $(e).attr('name').replace('['+$rowsId+']', '[' + $nId + ']') );
				// make it required. Tell validation plugin to validate it
				$(e).rules('add', {'required': true});
			});

			$justInserted.find('input,textarea').val(''); // it may copy values from first one
			$justInserted.slideDown(500);
		});





		$("input, textarea, select").not('.nostyle').uniform();



		$("#addBlogrollForm").validate({
			rules: {
				'data[Blogroll][blogrollname]': {
					required: true,
					minlength: 4
				}
			},
			messages: {
				'data[Blogroll][blogrollname]': {
					required: "Please enter a Campaign Name",
					minlength: "Enter Minimum Length"
				}
			}

		});



	});


</script>