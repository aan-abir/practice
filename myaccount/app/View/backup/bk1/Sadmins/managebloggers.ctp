<div class="marginB10"></div>
<div class="row-fluid">
    <div class="span12">

        <div class="box">
            <div class="title">
                <h4>
                    <span>Your Blogger List</span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Blogger Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if (count($allUser) > 0) {
                            foreach ($allUser as $camp) {
                                //pr($camp);
                                ?>
                                <tr>
                                    <td><?php echo $camp['User']['id']; ?></td>
                                    <td><a target="_blank" href="<?php echo Router::url('users/letmeinasblogger/' . $camp['User']['id']); ?>" title="Login into User Account" class="tip"><?php echo $camp['User']['username']; ?></a></td>
                                    <td><?php echo $camp['User']['firstname'] . ' ' . $camp['User']['lastname']; ?></td>
                                    <td>
                                        <div class="controls center">
                                            <a href="<?php echo Router::url('viewblogger/' . $camp['User']['id']); ?>" title="View Blogger" class="tip"><span class="icon12 icomoon-icon-grid-view"></span></a>
                                            <a href="<?php echo Router::url('editblogger/' . $camp['User']['id']); ?>" title="Edit Blogger" class="tip"><span class="icon12 icomoon-icon-pencil"></span></a>
                                            <a href="<?php echo Router::url('deleteblogger/' . $camp['User']['id']); ?>" title="Remove Blogger?" class="tip callAction"><span class="icon12 icomoon-icon-remove"></span></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            echo '<tr><td colspan="4">No Blogger Found!</td></tr>';
                        }
                        ?>



                    </tbody>
                </table>
            </div>

        </div><!-- End .box -->

    </div><!-- End .span6 -->
</div>

