<div class="page-header">
    <h4>User Plan</h4>
</div>
<div class="marginB10"></div>
<?php echo $this->Form->create('Usercredits', array('url' => array('controller' => 'sadmins', 'action' => 'updateusercredits', $userInfo['User']['id']), 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>
<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span><?php echo $userInfo['User']['fullname']; ?></span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th>Plan Name</th>
                            <th>Current Extra Credit ($)</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($userInfo) && count($userInfo) > 0):
                            ?>
                            <tr>
                                <td><?php echo $userInfo['Package']['packagename']; ?></td>
                                <td>
                                    <input class="span3 textRight" type="text" name="data[User][extra_credit]" value="<?php echo $userInfo['User']['extra_credit']; ?>" />
                                    <input class="span3 textRight" type="hidden" name="data[User][id]" value="<?php echo $userInfo['User']['id']; ?>" />
                                </td>
                                <td><?php echo $userInfo['Package']['description']; ?></td>
                            </tr>
                            <?php
                        else:
                            ?>
                            <tr>
                                <td></td>
                                <td colspan="2">No plan assigned!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->    
</div>
<div class="row-fluid">    
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4>
                    <span>Service Details | <a href="<?php echo Router::url('updateusercredits/' . $userInfo['User']['id']); ?>" title="View User Credit Details" class="tip">View</a></span>
                </h4>
            </div>
            <div class="content">
                <table class="responsive table table-bordered">
                    <thead>
                        <tr>
                            <th class="textLeft">Service Name</th>
                            <th class="textRight">Unit Price ($)</th>
                            <th class="textRight">Service Limit</th>
                            <th class="textRight">Extra Units</th>
                            <th class="textRight">Consumed Units</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $serviceTotal = 0.00;
                        $serviceCurrentTotal = 0.00;
                        $consumedTotal = 0.00;
                        if (isset($serviceList) && count($serviceList) > 0):
                            foreach ($serviceList as $k => $arr):
                                if ($arr['Service']['servicename']):
                                    ?>
                                    <tr>
                                        <td class="textLeft"><?php echo $arr['Service']['servicename']; ?></td>
                                        <td class="textRight"><?php echo $arr['Service']['default_unit_price']; ?></td>
                                        <td class="textRight">
                                            <?php
                                            if (isset($arr['Service']['servicelimit'])) {
                                                echo $arr['Service']['servicelimit'] != '9999999' ? $arr['Service']['servicelimit'] : 'Unlimited';
                                            } else {
                                                echo '0';
                                            }
                                            ?>
                                        </td>
                                        <td class="textRight"><input class="span3 textRight" type="text" name="data[Usercredit][<?php echo $arr['Service']['id'] ?>][extra_unit]" value="<?php echo isset($arr['Service']['extra_unit']) ? $arr['Service']['extra_unit'] : 0; ?>" /></td>
                                        <td class="textRight"><input class="span3 textRight" type="text" name="data[Usercredit][<?php echo $arr['Service']['id'] ?>][service_used]" value="<?php echo isset($arr['Service']['service_used']) ? $arr['Service']['service_used'] : 0; ?>" /></td>

                                    </tr>
                                    <tr style="display: none;">
                                        <td colspan="5">
                                            <input type="hidden" name="data[Usercredit][<?php echo $arr['Service']['id'] ?>][id]" value="<?php echo isset($arr['Service']['uc_id']) ? $arr['Service']['uc_id'] : ''; ?>" />
                                            <input type="hidden" name="data[Usercredit][<?php echo $arr['Service']['id'] ?>][user_id]" value="<?php echo $userInfo['User']['id']; ?>" />
                                            <input type="hidden" name="data[Usercredit][<?php echo $arr['Service']['id'] ?>][service_id]" value="<?php echo $arr['Service']['id']; ?>" />
                                            <input type="hidden" name="data[Usercredit][<?php echo $arr['Service']['id'] ?>][servicelimit]" value="<?php echo $arr['Service']['servicelimit']; ?>" />
                                        </td>
                                    </tr>
                                    <?php
                                endif;
                            endforeach;
                            ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="textRight"></td>
                                <td class="textRight"><button type="submit" class="btn btn-info marginR10">Submit to Update</button></td>
                            </tr>    
                            <?php
                        else:
                            ?>
                            <tr>
                                <td colspan="5">No record found!</td>
                            </tr>
                        <?php
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div><!-- End .span12 -->
</div>
<?php $this->Form->end(); ?>