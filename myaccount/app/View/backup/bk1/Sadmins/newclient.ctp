<div class="marginB10"></div>
<div class="page-header">
    <h4>Enter New Client Data</h4>
    <p>To create a new client simply fill up the form below. </p>

</div>
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'newclient'), 'id' => 'addClientForm', 'method' => 'post', 'class' => 'form-horizontal seperator')); ?>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="client_type">Package:</label>
                <div class="span4">
                    <?php
                    echo $this->Form->select('User.package_id', $packageList, array('required' => false, 'title' => 'Select Client Type', 'div' => false, 'label' => false, 'class' => 'span2 select', 'empty' => false));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3">Package Transaction Receipt:</label>
                <?php echo $this->Form->input('User.trans_receipt', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                <span class="help-inline blue">Enter Zaxaa Package Transaction Receipt for this Client.</span>
            </div>
        </div>
    </div>      
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3">High PR Transaction Receipt:</label>
                <?php echo $this->Form->input('User.high_pr_trans_receipt', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                <span class="help-inline blue">Enter Zaxaa High PR Transaction Receipt.</span>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3">Link Density Transaction Receipt:</label>
                <?php echo $this->Form->input('User.link_density_trans_receipt', array('type' => 'text', 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                <span class="help-inline blue">Enter Zaxaa Link Density Transaction Receipt.</span>
            </div>
        </div>
    </div> 
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="username">Username:</label>
                <?php echo $this->Form->input('User.username', array('id' => 'username', 'title' => 'User Name', 'placeholder' => "User Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
                <!--                <span class="hint"><a href="#" class="red">Check is Available</a></span>-->
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="normal">Password:</label>
                <div class="span4 controls">
                    <?php echo $this->Form->input('User.password', array('type' => 'text', 'id' => 'password', 'title' => 'Enter Client Passord', 'placeholder' => "User password", 'div' => false, 'label' => false, 'class' => 'span12')); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="firstname">First name:</label>
                <?php echo $this->Form->input('User.firstname', array('type' => 'text', 'id' => 'firstname', 'title' => 'Client First Name', 'placeholder' => "Client First Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="name">Last name:</label>
                <?php echo $this->Form->input('User.lastname', array('type' => 'text', 'id' => 'lastname', 'title' => 'Client Last Name', 'placeholder' => "Client Last Name", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="email">Email:</label>
                <?php echo $this->Form->input('User.email', array('type' => 'text', 'id' => 'email', 'title' => 'Client Email', 'placeholder' => "Client Email", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="email">Phone:</label>
                <?php echo $this->Form->input('User.phone', array('type' => 'text', 'id' => 'phone', 'title' => 'Client Phone', 'placeholder' => "Client Phone", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="skype">Skype:</label>
                <?php echo $this->Form->input('User.skype', array('type' => 'text', 'id' => 'skype', 'title' => 'Client Skype', 'placeholder' => "Client Skype", 'div' => false, 'label' => false, 'class' => 'span4')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="textarea">More info</label>
                <?php echo $this->Form->textarea('User.moreaboutclient', array('id' => 'moreaboutclient', 'title' => 'Client Additional Contact Info', 'div' => false, 'label' => false, 'class' => 'span4 limit elastic', 'rows' => 3, 'cols' => '5')); ?>
            </div>
        </div>
    </div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="status">Client Status:</label>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[User][status]" id="UserStatus1" value="1" checked="checked" />
                    Active
                </div>
                <div class="left marginT5 marginR10">
                    <input type="radio" name="data[User][status]" id="UserStatus0" value="0" />
                    Inactive
                </div>
            </div>
        </div>
    </div>
    <div class="marginT10"></div>
    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Save Client</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--< ?php echo $this->Form->hidden('Usercredit.user_service', array()); ?>-->
    <?php echo $this->Form->end(); ?>
</div>
<div class="marginB10"></div>

<script type="text/javascript">
    // document ready function
    $(document).ready(function() {

        //------------- Elastic textarea -------------//
        if ($('textarea').hasClass('elastic')) {
            $('.elastic').elastic();
        }

        $("#addClientForm").validate({
            rules: {
                'data[User][username]': {
                    required: true,
                    minlength: 4
                },
                'data[User][password]': {
                    required: true,
                    minlength: 4
                },
                'data[User][firstname]': {
                    required: true
                },
                'data[User][email]': {
                    required: true,
                    email: true
                }
            },
            messages: {
                'data[User][username]': {
                    required: "Please enter a Username",
                    minlength: "Enter Minimum Length"
                },
                'data[User][password]': {
                    required: "Please enter Password",
                    minlength: "Enter Minimum 04 Length"
                },
                'data[User][firstname]': {
                    required: "Please Enter Firstname"
                },
                'data[User][email]': {
                    required: "Please Enter Email",
                    email: "Please Enter Valid Email"
                }
                /*'data[Campaign][settings][1][anchortexts]': {
                 atLeastOneAnchorText: "Please insert at least one anchor text with minimum 4 chars long"
                 }*/
            }

        });


    });


</script>