<div class="marginB10"></div>
<div class="page-header">
    <h4>Import Domains From File</h4>
</div>
<div class="content">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'sadmins', 'action' => 'importdomains'), 'id' => 'addDomainForm', 'method' => 'post', 'type' => 'file', 'class' => 'form-horizontal seperator')); ?>


    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <label class="form-label span3" for="file">Upload CSV File:<br><span style="font-size: 9px;">(Column Name Must be like as name, cms, login, password, type, pr(1 to 10))</span> </label>
                <input type="file" name="data[Domain][file]" id="file" title="Upload cvs/excel File" />
            </div>
        </div>
    </div>

    <div class="marginT10"></div>

    <div class="form-row row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="form-actions">
                    <div class="span3"></div>
                    <div class="span4 controls">
                        <button type="submit" class="btn btn-info marginR10">Import Domain</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>


<div class="marginB10"></div>

<script type="text/javascript">
    $('#DomainNetworktype').change(function() {
        var networktype = $(this).val();
        if (networktype == 1) {
            $('#AssignToUserDiv').show();
        } else {
            $('#AssignToUserDiv').hide();
        }
        return false;
    });
</script>