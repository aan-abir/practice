<?php
    $activeTopMenu = ''; // 0 dashboard
    $topMenu = array(
        'dashboard' => array('dashboard'),
        'mysites' => array('mysites', 'myexclusivesites', 'newdomain', 'editdomain'),
        'networksites' => array('manualnetworkapplied', 'manualnetwork', 'managesubmission', 'addsubmission', 'editsubmission', 'submissionreport', 'submissionlinks', 'addblogroll', 'manageblogroll', 'blogcomments', 'manualpost', 'manualpostreport'),
        'outsidelinks' => array('bazookainprogress', 'bazookablast', 'bazookacompleted', 'linkemperorinprogress', 'linkemperor', 'linkemperorcompleted', 'manageguestpost', 'createmashup', 'managemashup', 'viewmashup', 'editmashup', 'orderguestpost', 'assignedguestpost', 'completedguestpost', 'linkindexing', 'indexingreport', 'indexingurls'),
        'profile' => array('editprofile', 'myplan', 'brands'),
        'support' => array('tour', 'faq', 'sdesk'),
        'communit' => array('facebook', 'community'),
    );
    foreach ($topMenu as $k => $g) {
        if (in_array($this->params['action'], $g)) {
            $activeTopMenu = $k;
            break;
        }
    }
?>

<div id="header">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="brand" href="/">
                    <?php echo $this->Html->image("/images/poe-logo.png", array()); ?>
                </a>

                <div class="nav-no-collapse">
                    <ul class="nav">
                        <li<?php echo $activeTopMenu == 'dashboard' ? ' class="active"' : ''; ?>><a
                            href="<?php echo Router::url('/new/clients/dashboard'); ?>"><span
                                class="icon16 icomoon-icon-screen-2"></span>Education</a></li>

                        <li><a href="#"><span class="icon16 icomoon-icon-rocket"></span>Resources</a></li>
                        <li><a href="<?php echo Router::url('/clients/tools'); ?>"><span class="icon16 cut-icon-stats-2"></span>Tools</a>
                        </li>
                       
                       

                        <li<?php echo $activeTopMenu == 'communit' ? ' class="dropdown active"' : ' class="dropdown"'; ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="icon16 entypo-icon-network"></span> Community
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'facebook' ? ' class="topnavSelected"' : ''; ?> href="https://www.facebook.com/seonitrollc" target="_blank"><span class="icon16 icomoon-icon-facebook"></span>FaceBook Group</a>
                                           
                                        </li>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'community' ? ' class="topnavSelected"' : ''; ?>  href="<?php echo Router::url('/clients/community'); ?>"><span class="icon16  icomoon-icon-users"></span>Our Forum</a>
                                        </li>
                                        
                                       
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="nav pull-right usernav">
                        <li<?php echo $activeTopMenu == 'profile' ? ' class="dropdown active"' : ' class="dropdown"'; ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                <span class="icon16 icomoon-icon-happy"></span>
                                <span class="txt">
                                    <?php echo AuthComponent::user('firstname') . ' ' . AuthComponent::user('lastname'); ?>
                                </span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'editprofile' ? ' class="topnavSelected"' : ''; ?>
                                                href="<?php echo Router::url('/clients/editprofile'); ?>"><span
                                                    class="icon16 icomoon-icon-user-3"></span>Profile</a>
                                        </li>

                                        <li>
                                            <a <?php echo $this->params['action'] == 'myplan' ? ' class="topnavSelected"' : ''; ?>
                                                class="top-sub-nav" href="<?php echo Router::url('/clients/myplan'); ?>">
                                                <span class="icon16  wpzoom-share"></span>Plans & Credits</a>
                                        </li>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'brands' ? ' class="topnavSelected"' : ''; ?>
                                                class="top-sub-nav" href="<?php echo Router::url('/clients/brands'); ?>">
                                                <span class="icon16 iconic-icon-brush"></span>Branding </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li<?php echo $activeTopMenu == 'support' ? ' class="dropdown active"' : ' class="dropdown"'; ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="icon16  icomoon-icon-support"></span>Support
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'tour' ? ' class="topnavSelected"' : ''; ?>
                                                class="top-sub-nav" href="<?php echo Router::url('/clients/tour'); ?>"><span
                                                    class="icon16 entypo-icon-leaf"></span>Tour</a></li>
                                        <li>
                                            <a <?php echo $this->params['action'] == 'faq' ? ' class="topnavSelected"' : ''; ?>
                                                class="top-sub-nav" href="<?php echo Router::url('/clients/faq'); ?>"><span
                                                    class="icon16 entypo-icon-share"></span>FAQ</a></li>
                                        <li><a class="top-sub-nav" href="http://support.seonitro.com/"
                                            target="_blank"><span class="icon16 entypo-icon-support"></span>Support
                                            Desk</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?php echo Router::url('/users/logout'); ?>"><span class="icon16 icomoon-icon-exit"></span> Logout</a></li>
                    </ul>
                </div>
                <!-- /.nav-collapse -->
            </div>
        </div>
        <!-- /navbar-inner -->
    </div>
    <!-- /navbar -->
</div>