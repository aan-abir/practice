<div id="header">
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <a class="brand" href="/">
                    <?php echo $this->Html->image("/images/poe-logo.png", array()); ?>
                </a>
                <div class="nav-no-collapse">
                    <ul class="nav">
                        <!--<li class="active"><a href="/"><span class="icon16 icomoon-icon-screen-2"></span> Dashboard</a></li>-->
                    </ul>
                    <ul class="nav pull-right usernav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="icon16 icomoon-icon-happy"></span>
                                <span class="txt">
                                    <?php echo AuthComponent::user('firstname') . ' ' . AuthComponent::user('lastname'); ?>
                                </span>
                                <b class="caret"></b>
                            </a>
                        </li>
                        <li><a href="<?php echo Router::url('logout') ?>"><span class="icon16 icomoon-icon-exit"></span> Logout</a></li>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div>
        </div><!-- /navbar-inner -->
    </div>
    <!-- /navbar -->
</div>
