<?php

App::uses('AppModel', 'Model');

class Bazookaservice extends AppModel {

    public $name = 'Bazookaservice';
    public $useTable = 'bazooka_services';
    public $validate = array(
        'service_id' => array(
            'already_taken' => array(
                'rule' => 'isUnique',
                'message' => 'Service is already inserted.'
            ),
        )
    );

}

// end class
?>