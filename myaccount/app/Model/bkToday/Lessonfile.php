<?php

App::uses('AppModel', 'Model');

class Lessonfile extends AppModel
{

    public $name = 'Lessonfile';
    public $useTable = 'lesson_files';
    public $validate = array(
        'filename' => array(
            'rule' => 'notEmpty',
            'message' => 'File Name is required.',
            'allowEmpty' => false,
            'required' => true,
        )
    );
}
