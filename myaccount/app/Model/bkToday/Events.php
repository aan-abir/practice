<?php

    App::uses('AppModel', 'Model');

    class Events extends AppModel {

        public $name = 'Events';
        public $useTable = 'events';
        var $validate = array(
            'title' => array(
                'rule' => 'notEmpty',
                'message' => 'Enter Event Title',
                'allowEmpty' => false,
                'required' => true,
            ),
            
            'starttime' => array(
                'rule' => 'notEmpty',
                'message' => 'Enter Start Time',
                'allowEmpty' => false,
                'required' => true,
            ),
            
            'endtime' => array(
                'rule' => 'notEmpty',
                'message' => 'Enter End Time',
                'allowEmpty' => false,
                'required' => true,
            ),
            

        );

    }

?>