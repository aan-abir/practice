<?php

App::uses('AppModel', 'Model');

class Iwarticle extends AppModel {

    public $name = 'Iwarticle';
    public $useTable = 'iwarticles';
    public $validate = array(
        'post_title' => array(
            'rule' => 'notEmpty',
            'message' => 'Post title is required.',
            'allowEmpty' => false,
            'required' => true,
        ),
        'post_content' => array(
            'rule' => 'notEmpty',
            'message' => 'Post content is required.',
            'allowEmpty' => false,
            'required' => true,
        )
    );

}
