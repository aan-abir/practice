<?php

App::uses('AppModel', 'Model');

class Media extends AppModel {

    public $name = 'Media';
    public $useTable = 'medias';
    public $validate = array(
        'title' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter a title',
                'allowEmpty' => false,
                'required' => true,
            ),
        )
    );

}

// end class
?>