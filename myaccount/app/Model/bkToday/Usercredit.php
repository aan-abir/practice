<?php

App::uses('AppModel', 'Model');

class Usercredit extends AppModel {

    public $name = 'Usercredit';
    public $useTable = 'usercredits';
    public $validate = array(
        'user_id' => array(
            'unique' => array(
                'rule' => array('checkUnique', array('user_id', 'service_id')),
                'message' => 'User ID and Servie ID together must be unique.',
            )
        )
    );
    public $virtualFields = array(
        'servicename' => 'SELECT servicename FROM services WHERE services.id = Usercredit.service_id'
    );
    public $order = array("Usercredit.servicename" => "ASC");

}

?>