<?php

App::uses('AppModel', 'Model');

class Category extends AppModel {

    public $name = 'Category';
    public $useTable = 'categories';
    public $actsAs = array('Tree');

}

?>