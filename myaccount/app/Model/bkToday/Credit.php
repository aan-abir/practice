<?php

App::uses('AppModel', 'Model');

class Credit extends AppModel {

    public $name = 'Credit';
    public $useTable = 'users';
    var $validate = array(
        'assign_credit' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Credit can not be left empty',
                'allowEmpty' => false,
                'required' => true
            ),
            'onlyNumber' => array(
                'rule' => 'numeric',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Credit should be in number'
            )
        )
    );

}

?>