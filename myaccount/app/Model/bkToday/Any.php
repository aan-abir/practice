<?php

App::uses('AppModel', 'Model');

class Any extends AppModel {

    public $name = 'Any';
    public $useTable = false;

    function staction($itemId = null, $modelName = null, $cont = null) {
        $cont->autoRender = false;
        $cont->layout = 'ajax';
        if ($itemId && $modelName && $cont) {
            $modelName = ucfirst($modelName);
            $this->useTable = ClassRegistry::init($modelName)->useTable;
            $this->id = $itemId;
            $currentStatus = $this->field('status');
            if (in_array($currentStatus, array(0, 1))) {
                if ($currentStatus == 1) {
                    $newStatus = 0;
                } elseif ($currentStatus == 0) {
                    $newStatus = 1;
                }
                if ($this->saveField('status', $newStatus)) {
                    $fMsg = ($newStatus ? 'Activated' : 'Deactivated') . ' successfully!';
                    $cont->Session->setFlash($fMsg, 'flash_success');
                } else {
                    $fMsg = 'Could not be ' . ($newStatus ? 'activated' : 'deactivated') . '!';
                    $cont->Session->setFlash($fMsg, 'flash_error');
                }
            }
            $this->useTable = false;
        }
    }

    function delitem($itemId = null, $modelName = null, $cont = null) {
        $cont->autoRender = false;
        $cont->layout = 'ajax';
        if ($itemId && $modelName && $cont) {
            $modelName = ucfirst($modelName);
            $this->useTable = ClassRegistry::init($modelName)->useTable;
            if ($this->delete($itemId, false)) {
                $fMsg = sprintf(DELETE_SUCCESS, 'Item');
                $cont->Session->setFlash($fMsg, 'flash_success');
            } else {
                $fMsg = sprintf(DELETE_ERROR, 'Item');
                $cont->Session->setFlash($fMsg, 'flash_error');
            }
            $this->useTable = false;
        }
    }

}
