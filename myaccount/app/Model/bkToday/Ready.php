<?php

if (!defined('TEST_EMAIL')) {
    define('TEST_EMAIL', 'itestgo@gmail.com');
}

App::uses('AppModel', 'Model');

class Ready extends AppModel {

    public $name = 'Ready';
    public $useTable = false;

    function curl_get_contents($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }

    function _request($url, $method = "GET", $postData = array()) {

        // initialize a new curl object
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        /**
          curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($curl, CURLOPT_HEADER, true);
          curl_setopt($curl, CURLOPT_NOBODY, true);
          /* */
        // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        switch (strtoupper($method)) {
            case "GET":
                curl_setopt($curl, CURLOPT_HTTPGET, TRUE);
                break;
            case "POST":
                curl_setopt($curl, CURLOPT_POST, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
                break;
            case "DELETE":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;
            default:
                return "Unknown method $method";
                break;
        }

        $result = curl_exec($curl);
        // do the request. If FALSE, then an exception occurred
        if (FALSE === ($result))
            return "Curl failed with error " . curl_error($curl);

        $final_data = array();

        // get result code
        $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $header = curl_getinfo($curl);
        //$header = $this->parse_http_head($result);
        $final_data['response_code'] = $responseCode;
        //$final_data['header'] = $header;
        //$final_data['result'] = '0';

        if ($responseCode === 200) {
            $final_data['result'] = $result;
        }

        return $final_data;
    }

    function _getDcl($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($curl);
        if (FALSE === ($result)) {
            return 'httperror# ' . curl_error($curl);
        } else {
            return $result;
        }
    }

    function _setDclPost($url, $postData) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        curl_exec($curl);
        return 'success';
        if (FALSE === ($result)) {
            return 'httperror# ' . curl_error($curl);
        } else {
            return $result;
        }
    }

    function _post($url, $postData, $extraInfo = false) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $result = curl_exec($ch);
        if ($extraInfo) {
            $final_data['response_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $final_data['header'] = curl_getinfo($ch);
            $final_data['error'] = curl_error($ch);
            $final_data['response'] = $result;
            return $final_data;
        }
        return $result;
    }

    function _checkUrl($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        $result = curl_exec($ch);
        if (FALSE === ($result)) {
            return '0';
        } else {
            return curl_getinfo($ch, CURLINFO_HTTP_CODE);
        }
    }

}
