<?php

App::uses('AppModel', 'Model');

class GoogePageRank extends AppModel{

    public $name = 'GoogePageRank';
    public $useTable = false;

    //get google pagerank
    public function getPageRank($url, $proxyIp, $proxy_user_pass, $proxy_port) {
            if ( count($proxyIp) <= 0 ) {
                return 'noproxy';
            }
            shuffle($proxyIp);
            $sip = array_pop($proxyIp); // fetch ip for ip array

            $query = "http://toolbarqueries.google.com/tbr?client=navclient-auto&ch=". self::CheckHash(self::HashURL($url)). "&features=Rank&q=info:".$url."&num=100&filter=0";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $query);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXY, $sip);
            curl_setopt($ch, CURLOPT_PROXYUSERPWD,$proxy_user_pass);
            curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            $data = curl_exec($ch);

             // Check for errors and display the error message
            if(curl_errno($ch))
            {
                echo 'Curl error: ' . curl_error($ch); exit;
            }

            $status = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if ( $status >= 200 && $status < 300 ) {
                if ( strpos($data, "Rank_") === false ) {
                    return 0;
                } else{
                    return intval(trim(substr($data, strpos($data, "Rank_") + 9)));
                }
            }

            // maybe IP blocked to try with another IP
            return self::getPageRank($url, $proxyIp, $proxy_user_pass, $proxy_port);
        } // end function

        //index checker function
        public function checkIndex($domain, $proxyIp, $proxy_user_pass, $proxy_port){
            if ( count($proxyIp) <= 0 ) {
                return 'noproxy';
            }
            $url = 'https://www.google.com/search?q=info:'.urlencode($domain);
            $sip = array_pop($proxyIp); // fetch ip for ip array
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXY, $sip);
            curl_setopt($ch, CURLOPT_PROXYUSERPWD,$proxy_user_pass);
            curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

            $content = curl_exec($ch);
            $status = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            //pr($status);
            // 47 results
            // About 47 results
            if ( $status >= 200 && $status < 300 ) {
                preg_match_all("/(\d+) result(s*)/iUS", $content, $matches);
                return count($matches[1]) ? 'Yes' : 'No';
            }

            // maybe IP blocked to try with another IP
            return self::checkIndex($domain,$proxyIp,$proxy_user_pass,$proxy_port);
        } //end function


        public function pr($arg=null) {
            echo '<br>-----------------------------------<pre>';
            print_r($arg);
            echo '</pre><br>----------------------------<br>';
        }

        /*
        * genearate a checksum for the hash string
        */
        public static function CheckHash($Hashnum)
        {
            $CheckByte = 0;
            $Flag = 0;

            $HashStr = sprintf('%u', $Hashnum) ;
            $length = strlen($HashStr);

            for ($i = $length - 1;  $i >= 0;  $i --) {
                $Re = $HashStr{$i};
                if (1 === ($Flag % 2)) {
                    $Re += $Re;
                    $Re = (int)($Re / 10) + ($Re % 10);
                }
                $CheckByte += $Re;
                $Flag ++;
            }

            $CheckByte %= 10;
            if (0 !== $CheckByte) {
                $CheckByte = 10 - $CheckByte;
                if (1 === ($Flag % 2) ) {
                    if (1 === ($CheckByte % 2)) {
                        $CheckByte += 9;
                    }
                    $CheckByte >>= 1;
                }
            }

            return '7'.$CheckByte.$HashStr;
        }

        /*
        * Genearate a hash for a url
        */
        public static function HashURL($String)
        {
            $Check1 = self::StrToNum($String, 0x1505, 0x21);
            $Check2 = self::StrToNum($String, 0, 0x1003F);

            $Check1 >>= 2;
            $Check1 = (($Check1 >> 4) & 0x3FFFFC0 ) | ($Check1 & 0x3F);
            $Check1 = (($Check1 >> 4) & 0x3FFC00 ) | ($Check1 & 0x3FF);
            $Check1 = (($Check1 >> 4) & 0x3C000 ) | ($Check1 & 0x3FFF);

            $T1 = (((($Check1 & 0x3C0) << 4) | ($Check1 & 0x3C)) <<2 ) | ($Check2 & 0xF0F );
            $T2 = (((($Check1 & 0xFFFFC000) << 4) | ($Check1 & 0x3C00)) << 0xA) | ($Check2 & 0xF0F0000 );

            return ($T1 | $T2);
        }


        public static function StrToNum($Str, $Check, $Magic)
        {
            $Int32Unit = 4294967296;  // 2^32

            $length = strlen($Str);
            for ($i = 0; $i < $length; $i++) {
                $Check *= $Magic;
                //If the float is beyond the boundaries of integer (usually +/- 2.15e+9 = 2^31),
                //  the result of converting to integer is undefined
                //  refer to http://www.php.net/manual/en/language.types.integer.php
                if ($Check >= $Int32Unit) {
                    $Check = ($Check - $Int32Unit * (int) ($Check / $Int32Unit));
                    //if the check less than -2^31
                    $Check = ($Check < -2147483648) ? ($Check + $Int32Unit) : $Check;
                }
                $Check += ord($Str{$i});
            }
            return $Check;
        }

        public static function getDomainName($url)
        {
            $domain	= parse_url($url);
            return ltrim($domain['host'],"_");
        }

}

// end class
?>