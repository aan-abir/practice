<?php

App::uses('AppModel', 'Model');

class Indexingurl extends AppModel {

    public $name = 'Indexingurl';
    public $useTable = 'indexingurls';
    public $validate = array(
        'url' => array(
            'rule' => 'notEmpty',
            'message' => 'Url is required.',
            'allowEmpty' => false,
            'required' => true,
        )
    );

}
