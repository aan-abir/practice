<?php

App::uses('AppModel', 'Model');

class Package extends AppModel {

    public $name = 'Package';
    public $useTable = 'packages';
    var $validate = array(
        'packagename' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Package Name can not be left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'duration' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Duration can not be left empty',
                'allowEmpty' => false,
                'required' => false
            ),
            'onlyNumber' => array(
                'rule' => 'numeric',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Duration should be in number'
            )
        ),
        'pvalue' => array(
            'onlyNumber' => array(
                'rule' => 'numeric',
                'required' => false,
                'allowEmpty' => true,
                'message' => 'Package Price should be in number'
            )
        ),
        'additional_credit' => array(
            'onlyNumber' => array(
                'rule' => 'numeric',
                'required' => false,
                'allowEmpty' => true,
                'message' => 'Additional Credit should be in number'
            )
        )
    );

}

?>