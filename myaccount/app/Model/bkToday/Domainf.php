<?php

App::uses('AppModel', 'Model');

class Domainf extends AppModel {

    public $name = 'Domainf';
    public $useTable = 'domains';
    public $virtualFields = array(
        'domainlabel' => 'CONCAT(Domainf.domain, " | ", Domainf.pr," | ",Domainf.tflow," | ",Domainf.cflow)',
    );

}
