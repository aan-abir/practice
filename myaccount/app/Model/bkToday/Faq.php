<?php

App::uses('AppModel', 'Model');

class Faq extends AppModel {

    public $name = 'Faq';
    public $useTable = 'faq';
    var $validate = array(
        'question' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Question can not be left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'answer' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Answer can not be left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
    );

}

?>