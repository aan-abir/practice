<?php

App::uses('AppModel', 'Model');

class Commonareatext extends AppModel {

    public $name = 'Commonareatext';
    public $useTable = 'commonareatexts';
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Commonarea' => array(
            'className' => 'Commonarea',
            'foreignKey' => 'commonarea_id'
        ),
    );

}

// end class
?>