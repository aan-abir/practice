<?php

App::uses('AppModel', 'Model');

class Majesticseo extends AppModel {

    public $name = 'Majesticseo';
    public $useTable = false;

    //get google pagerank
    public static function GetIndexItemInfo($domain) {

        $urlToFetch = "http://enterprise.majesticseo.com/api_command?app_api_key=4A10BF1F0183ABBA8FFECE07D003A683&datasource=fresh&cmd=GetIndexItemInfo&items=1&item0=" . $domain;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $urlToFetch);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);



        $head = curl_exec($ch);

        curl_close($ch);

        $xml = simplexml_load_string($head);

        $row = explode('|', str_replace('||', ' ', $xml->DataTables->DataTable->Row[0]));

        $headers = explode('|', $xml->DataTables->DataTable->attributes()->Headers);



        return array('headers' => $headers, 'row' => $row);
    }

    /**

     *

     * To run GetTopPages cmd for Analyze Url

     *

     * @param string  $app_api_key majesticseo api key

     * @param string  $url analize url

     * @return array deleted,images,nofollow and internallink

     */
    public static function GetTopPages($app_api_key, $url, $internal = 0, $count) {

        //$urlToFetch1="http://enterprise.majesticseo.com/api_command?app_api_key=4A10BF1F0183ABBA8FFECE07D003A683&datasource=fresh&cmd=GetTopPages&Query=".$ur."&Count=100";

        $urlToFetch = "http://enterprise.majesticseo.com/api_command?app_api_key=" . $app_api_key . "&datasource=fresh&cmd=GetTopPages&Query=" . $url . "&Count=" . $count;



        //$urlToFetch1="https://www.majesticseo.com/reports/site-explorer/summary/".$ur;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $urlToFetch);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);



        $head = curl_exec($ch);

        curl_close($ch);

        $xml = simplexml_load_string($head);

        if ($internal == 1)
            $internal_lnk = $xml->DataTables->DataTable[0];

        $dataBl = explode('|', $xml->DataTables->DataTable[1]->Row[0]);

        $dataBl1 = explode('|', $xml->DataTables->DataTable[1]->Row[1]);

        if ($dataBl[0] == $url)
            $val = $dataBl;
        else
            $val = $dataBl1;

        if ($internal == 1)
            return array('val' => $val, 'internal' => $internal_lnk);
        else
            return array('val' => $val);
    }

    /**

     *

     * To run GetBackLinksHistory cmd for Analyze Url

     *

     * @param string  $app_api_key majesticseo api key

     * @param string  $url analize url

     * @return array BackLinkHistory

     */
    public static function GetBackLinksHistory($url) {

        //$urlToFetch="http://enterprise.majesticseo.com/api_command?app_api_key=4A10BF1F0183ABBA8FFECE07D003A683&cmd=GetBackLinksHistory&item0=".trim($url);
        $urlToFetch = "http://enterprise.majesticseo.com/api_command?app_api_key=4A10BF1F0183ABBA8FFECE07D003A683&datasource=fresh&cmd=GetBackLinksHistory&items=1&item0=" . trim($url);



        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $urlToFetch);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $head = curl_exec($ch);

        curl_close($ch);


        if ($head) {
            $xml = simplexml_load_string($head);
            
            if(isset($xml->attributes()->Code) && $xml->attributes()->Code == 'InsufficientRetrievalUnits')
            {
               return 'InsufficientRetrievalUnits';   
            } 
            
            $raw_header = $xml->DataTables->DataTable[1]->attributes()->Headers;

            $raw_text_link = $xml->DataTables->DataTable[1]->Row[0];

            $raw_image_link = $xml->DataTables->DataTable[1]->Row[1];

            $raw_frame_link = $xml->DataTables->DataTable[1]->Row[3];

            $raw_follow_link = $xml->DataTables->DataTable[1]->Row[5];

            $raw_deleted_link = $xml->DataTables->DataTable[1]->Row[6];

            $raw_refdomain_link = $xml->DataTables->DataTable[1]->Row[11];

            $raw_totLnk = $xml->DataTables->DataTable[1]->Row[7];

            return array('header' => $raw_header, 'text_link' => $raw_text_link, 'image_link' => $raw_image_link, 'frame_link' => $raw_frame_link, 'follow_link' => $raw_follow_link, 'dead_link' => $raw_deleted_link, 'refdomain_link' => $raw_refdomain_link, 'total_link' => $raw_totLnk);
        }
    }

    /**

     *

     * To run DownloadBackLinks cmd for Analyze Url

     *

     * @param string  $app_api_key majesticseo api key

     * @param string  $url analize url

     * @return array DownloadBackLinks

     */
    public static function DownloadBackLinks($app_api_key, $url) {

        echo $urlToFetch4 = "http://enterprise.majesticseo.com/api_command?app_api_key=" . $app_api_key . "&cmd=DownloadBackLinks&datasource=fresh&Query=" . $url . "&FlagExcludeOldCrawl=1&FlagExcludeDeleted=1&FlagExcludeAltText=1&FlagExcludeMention=1";

        exit;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $urlToFetch4);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $headGtbl = curl_exec($ch);

        curl_close($ch);



        $xmlGtbl = simplexml_load_string($headGtbl);

        $attr = $xmlGtbl->attributes();

        $attr_globalvar = $xmlGtbl->GlobalVars->attributes();

        return array('main_attribute' => $attr, 'globalvar_attribute' => $attr_globalvar);
    }

    /**

     *

     * To run GetDownloadsList cmd for Analyze Url

     *

     * @param string  $app_api_key majesticseo api key

     * @param string  $job_id job id

     * @return path csv path

     */
    public static function GetDownloadsList($app_api_key, $job_id) {

        $urlToFetch = "http://enterprise.majesticseo.com/api_command?app_api_key=" . $app_api_key . "&cmd=GetDownloadsList&DownloadJobID=" . $job_id;

        $ch1 = curl_init();

        curl_setopt($ch1, CURLOPT_URL, $urlToFetch);

        curl_setopt($ch1, CURLOPT_HEADER, 0);

        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);

        $headGtbl1 = curl_exec($ch1);

        curl_close($ch1);



        $xmlGtbl1 = simplexml_load_string($headGtbl1);

        $filenm = explode('|', $xmlGtbl1->DataTables->DataTable->Row[0]);

        $det_nm = sfConfig::get('sf_root_dir') . '/web/uploads/tmp/' . $job_id . '.csv';

        myUser::uncompress(trim($filenm[8]), $det_nm);

        return $det_nm;
    }

    /**

     *

     * To run GetBackLinkData cmd for Analyze Url

     *

     * @param string  $app_api_key majesticseo api key

     * @param string  $url site url

     * @param integer $cut_credit count for backlink

     * 

     * @return object Backlink xml Object

     */
    public static function GetBackLinkData($url, $cut_credit) {
        $app_api_key = '4A10BF1F0183ABBA8FFECE07D003A683';
        $urlToFetch4 = "http://enterprise.majesticseo.com/api_command?app_api_key=" . $app_api_key . "&cmd=GetBackLinkData&datasource=fresh&item=" . $url . "&Count=" . $cut_credit . "&ShowDomainInfo=1&Mode=1";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $urlToFetch4);

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $headGtbl = curl_exec($ch);

        curl_close($ch);



        $xmlGtbl = simplexml_load_string($headGtbl);

        $domain_info = 0;

        $cnt_datatables = $xmlGtbl->DataTables->attributes()->Count;

        for ($sd_Cnt = 0; $sd_Cnt < $cnt_datatables; $sd_Cnt++) {

            $dataGTBL_sub = $xmlGtbl->DataTables->DataTable[$sd_Cnt];

            $check_domain = $dataGTBL_sub->attributes()->Name;

            if ($check_domain == 'BackLinks') {

                $domain_info = $sd_Cnt;
            }
        }



        $dataGTBL = $xmlGtbl->DataTables->DataTable[$domain_info];

        return $dataGTBL;
    }

}

?>