<?php

App::uses('AppModel', 'Model');

class Bazooka extends AppModel {

    public $name = 'Bazooka';
    public $useTable = 'bazooka_categories';
    public $validate = array(
        'name' => array(
            'already_taken' => array(
                'rule' => 'isUnique',
                'message' => 'Category is already inserted.'
            ),
        )
    );

}

// end class
?>