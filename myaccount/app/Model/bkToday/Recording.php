<?php

App::uses('AppModel', 'Model');

class Recording extends AppModel {

    public $name = 'Recording';
    public $useTable = 'recordings';
    public $validate = array(
        'title' => array(
            'rule' => 'notEmpty',
            'message' => 'Enter Title',
            'allowEmpty' => false,
            'required' => true,
        ),
        'speaker_name' => array(
            'rule' => 'notEmpty',
            'message' => 'Enter Speaker Name',
            'allowEmpty' => false,
            'required' => true,
        ),

    );

}

// end class
?>