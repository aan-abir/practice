<?php

App::uses('AppModel', 'Model');

class Submissionreport extends AppModel {

    public $name = 'Submissionreport';
    public $useTable = 'submissionreports';
    public $order = array("Submissionreport.id" => "DESC");

}
