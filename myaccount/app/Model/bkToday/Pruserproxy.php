<?php

App::uses('AppModel', 'Model');

class Pruserproxy extends AppModel {

    public $name = 'Pruserproxy';
    public $useTable = 'pr_user_proxy';
    
    public $validate = array(
        'ip_address' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'IP address can not empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'username' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Username can not empty',
                'allowEmpty' => false,
                'required' => true,
            )
        ),
        'password' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Password can not empty',
                'allowEmpty' => false,
                'required' => true,
            )
        ),
        'port' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Port can not empty',
                'allowEmpty' => false,
                'required' => true,
            )
        )
    );

    
    /* function atleastOneKeyword(){
      $k = implode("", array_filter($this->data['Rankcampaign']['keywords']));
      return ( strlen($k) == 0 ) ? 0 : 1;
      }
      function atleastOneUrl() {
      $k = implode("", array_filter($this->data['Rankcampaign']['urls']));
      return ( strlen($k) == 0 ) ? 0 : 1;
      } */
}
