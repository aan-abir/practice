<?php

App::uses('AppModel', 'Model');

class Menu extends AppModel
{

    public $name = 'Menu';
    public $useTable = 'menus';
    public $validate = array(
        'name' => array(
            'rule' => 'notEmpty',
            'message' => 'Name is required.',
            'allowEmpty' => false,
            'required' => true,
        )
    );

    public function getEmbededMenus()
    {
        $parentIds = $this->find('list', array(
            'conditions' => "parent_id IS NULL",
            'fields' => 'id,id',
            'order' => 'menu_order asc'
        ));
        $allmenus = $this->find('all',array(
            'order' => 'menu_order asc'
        ));

        $items = array();
        if ($allmenus) {
            foreach ($allmenus as $menu) {
                $arr = $menu['Menu'];
                $mId = $arr['id'];
                if (in_array($mId, $parentIds)) {
                    $items[$mId]['top'] = $arr;
                } else {
                    if (in_array($arr['parent_id'], $parentIds)) {
                        $items[$arr['parent_id']]['sub'][] = $arr;
                    }
                }
            }
        }
        return $items;
    }

}
