<?php

App::uses('AppModel', 'Model');

class Commonarea extends AppModel {

    public $name = 'Commonarea';
    public $useTable = 'commonareas';
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );
    public $validate = array(
        'commonareaname' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Commonarea Name can not left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        )
    );

}

// end class
?>