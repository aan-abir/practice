<?php

App::uses('AppModel', 'Model');

class robowhois extends AppModel {

    public $name = 'robowhois';
    public $useTable = false;

    function whoisProperties($domain) {

        $domain = str_replace("http://", '', $domain);
        $domain = str_replace("www.", '', $domain);

        $username = '3cca276123de3777b19a6247f7c5b8d5';
        $password = 'surfin';
        $template = 'api.robowhois.com/whois/' . $domain . '/properties';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $template);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP/' . phpversion());
        $response = curl_exec($ch);
        //pr(json_decode($response));
        return json_decode($response);
    }

    function date_difference($datetime1, $datetime2) { //$datetime1 previous date

        $datetime1 = new DateTime(date("Y-m-d", strtotime($datetime1)));
        $datetime2 = new DateTime(date("Y-m-d", strtotime($datetime2)));
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%y') . ' years';
    }

    function credit() {
        $username = '3cca276123de3777b19a6247f7c5b8d5';
        $password = 'surfin';
        $template = 'api.robowhois.com/v1/account';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $template);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP/' . phpversion());
        $response = curl_exec($ch);
        $response = json_decode($response);
        return $response->account->credits_remaining;
    }

}

// end class
?>