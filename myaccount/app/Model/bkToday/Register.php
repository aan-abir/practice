<?php

App::uses('AppModel', 'Model');

class Register extends AppModel {

    public $name = 'Register';
    public $useTable = 'users';
    var $validate = array(
        'username' => array(
            'between 1 and 20 character' => array(
                'rule' => array('between', 1, 20),
                'message' => 'Username can be up to 20 characters in length.',
                'allowEmpty' => false,
                'required' => true,
            ),
            'already taken' => array(
                'rule' => 'isUnique',
                'message' => 'Username is already in use.'
            ),
        ),
        'firstname' => array(
            'rule' => 'notEmpty',
            'message' => 'Enter First Name',
            'allowEmpty' => false,
            'required' => true,
        ),
        'lastname' => array(
            'rule' => 'notEmpty',
            'message' => 'Enter Last Name',
            'allowEmpty' => false,
            'required' => true,
        ),
        'password' => array(
            'rule' => array('minLength', '4'),
            'required' => true,
            'allowEmpty' => false,
            'required' => true,
            'message' => 'Password must be at least 4 characters.'
        ),
        'email' => array(
            'between 1 and 3 character' => array(
                'rule' => 'email',
                'required' => true,
                'message' => 'Invalid email address.',
            ),
            'already taken' => array(
                'rule' => 'isUnique',
                'message' => 'Email is already in use.'
            ),
        ),
    );

}

?>