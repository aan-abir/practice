<?php

App::uses('AppModel', 'Model');

class Testdomain extends AppModel {

    public $name = 'Testdomain';
    public $useTable = 'testdomains';
    public $validate = array(
        'domain' => array(
            'domain_required' => array(
                'rule' => 'notEmpty',
                'message' => 'Domain is required',
                'allowEmpty' => false,
                'required' => true,
            ),
            'already taken' => array(
                'rule' => 'isUnique',
                'message' => 'Domain is already in use.'
            )
        ),
        'ausername' => array(
            'rule' => 'notEmpty',
            'message' => 'Username is required.',
            'allowEmpty' => false,
            'required' => false,
        ),
        'apassword' => array(
            'rule' => 'notEmpty',
            'message' => 'Password is required.',
            'allowEmpty' => false,
            'required' => false,
        )
    );

}

// end class
?>