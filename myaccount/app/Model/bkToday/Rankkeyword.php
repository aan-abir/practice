<?php

App::uses('AppModel', 'Model');

class Rankkeyword extends AppModel {

    public $name = 'Rankkeyword';
    public $useTable = 'rankkeywords';
    public $belongsTo = array(
        'Rankcampaign' => array(
            'className' => 'Rankcampaign',
            'foreignKey' => 'campaign_id'
        )
    );

}
