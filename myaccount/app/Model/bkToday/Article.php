<?php

App::uses('AppModel', 'Model');

class Article extends AppModel {

    public $name = 'Article';
    public $useTable = 'articles';
    public $validate = array(
        'user_id' => array(
            'not_empty' => array(
                'rule' => 'notEmpty',
                'message' => 'User is required.',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'title' => array(
            'not_empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Title is required.',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'body' => array(
            'not_empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Article body is required.',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
    );
    public $virtualFields = array(
        'category_name' => 'SELECT name FROM categs WHERE categs.id = Article.category'
    );

    function confirm_iw_order($itemId, $cont) {
        $cont->autoRender = false;
        $cont->layout = 'ajax';
        $fields = 'title,category,art_len,keywords,writing_style,art_purpose,special_instructions';
        $itemInfo = $this->find('first', array('conditions' => "requested = 1 AND status NOT IN(0,2) AND id = $itemId", 'fields' => $fields));

        if ($itemInfo) {
            $xmlRequest = '<request>';
            $xmlRequest .= '<user_id>Buckaroo</user_id><api_key>NWI5Njg4MWI3YjczMmQ1MjJmMzliNG</api_key>';
            $xmlRequest .= '<func_name>add_project</func_name>';
            $arr = $itemInfo['Article'];
            foreach ($arr as $key => $value) {
                $xmlRequest .= "<$key>";
                if ($key == 'keywords') {
                    $xmlRequest .= '<keyword>';
                    $xmlRequest .= $value;
                    $xmlRequest .= '</keyword>';
                } else {
                    $xmlRequest .= $value;
                }
                $xmlRequest .= "</$key>";
            }
            $xmlRequest .= '</request>';

            $xmlRequest = $cont->cleanXML($xmlRequest);
            $url = 'http://www.iwriter.com/api/';
            $cont->loadModel('Ready');
            $xmlResponse = $cont->Ready->_post($url, $xmlRequest);

            //$xmlResponse = '<response><status>ok</status><project_id>19325</project_id></response>';

            App::uses('Xml', 'Utility');
            $xmlArray = Xml::toArray(Xml::build($xmlResponse));
            //pr($xmlArray);exit;
            if (isset($xmlArray['response']['project_id'])) {
                $fMsg = 'Article request confirmed!';
                $cont->Session->setFlash($fMsg, 'flash_success');
                $trackId = $xmlArray['response']['project_id'];
                $this->id = $itemId;
                $updateData['proj_id'] = $trackId;
                $updateData['requested'] = 2;
                $updateData['request_date'] = date('Y-m-d H:i:s');
                $this->save($updateData, false);
            } else if (isset($xmlArray['result']['error_message'])) {
                $fMsg = (string) $xmlArray['result']['error_message'];
                $cont->Session->setFlash($fMsg, 'flash_error');
            } else {
                $fMsg = 'Something went wrong!';
                $cont->Session->setFlash($fMsg, 'flash_error');
            }
            $xmlArray['article_id'] = $itemId;
            $to[] = TEST_EMAIL;
            $body = $cont->pretty($xmlArray);
            $cont->cmail($to, 'iWriter Article Request RR', $body);
        }
    }

    function approve_article($itemId, $cont) {
        $cont->autoRender = false;
        $cont->layout = 'ajax';
        $fields = 'id,proj_id,article_id';
        $itemInfo = $this->find('first', array('conditions' => "id = $itemId", 'fields' => $fields));

        if ($itemInfo) {
            $xmlRequest = '<request>';
            $xmlRequest .= '<user_id>Buckaroo</user_id><api_key>NWI5Njg4MWI3YjczMmQ1MjJmMzliNG</api_key>';
            $xmlRequest .= '<func_name>review_article</func_name>';
            $xmlRequest .= '<proj_id>#PROJ_ID#</proj_id>';
            $xmlRequest .= '<article_id>#ARTICLE_ID#</article_id>';
            $xmlRequest .= '<operation>approve</operation>';
            $xmlRequest .= '<rate>5</rate>';
            $xmlRequest .= '<comment>Very good quality article.</comment>';
            $xmlRequest .= '</request>';
            $xmlRequest = $cont->cleanXML($xmlRequest);

            $arr = $itemInfo['Article'];
            $itemId = $arr['id'];
            $projId = $arr['proj_id'];
            $articleId = $arr['article_id'];
            $xmlRequest = str_replace('#PROJ_ID#', $projId, $xmlRequest);
            $xmlRequest = str_replace('#ARTICLE_ID#', $articleId, $xmlRequest);

            $url = 'http://www.iwriter.com/api/';
            $cont->loadModel('Ready');
            $xmlResult = $cont->Ready->_post($url, $xmlRequest);

            //$xmlResponse = '<response><status>ok</status><project_id>19325</project_id></response>';

            App::uses('Xml', 'Utility');
            $xmlArray = Xml::toArray(Xml::build($xmlResult));
            //pr($xmlArray);exit;
            if (isset($xmlArray['result']['status']) && $xmlArray['result']['status'] == 'ok') {
                $fMsg = 'Article request confirmed!';
                $cont->Session->setFlash($fMsg, 'flash_success');
                $this->id = $itemId;
                $updateData['approval'] = 1;
                $updateData['keyword_status'] = 'closed';
                $updateData['approval_date'] = date('Y-m-d H:i:s');
                if ($this->save($updateData, false)) {
                    $this->download_articles($itemId, $projId, $articleId, $cont);
                }
            } else if (isset($xmlArray['result']['error_message'])) {
                $fMsg = (string) $xmlArray['result']['error_message'];
                if (stripos($fMsg, "no longer in 'pending_review' state.")) {
                    $this->id = $itemId;
                    $updateData['approval'] = 1;
                    $updateData['keyword_status'] = 'closed';
                    $updateData['approval_date'] = date('Y-m-d H:i:s');
                    if ($this->save($updateData, false)) {
                        $this->download_articles($itemId, $projId, $articleId, $cont);
                    }
                    $this->download_articles($itemId, $projId, $articleId, $cont);
                }
                $cont->Session->setFlash($fMsg, 'flash_error');
            } else {
                $fMsg = 'Something went wrong!';
                $cont->Session->setFlash($fMsg, 'flash_error');
            }
            $xmlArray['article_id'] = $itemId;
            $to[] = TEST_EMAIL;
            $body = $cont->pretty($xmlArray);
            $cont->cmail($to, 'iWriter Article Approval', $body);
        }
    }

    function download_articles($itemId, $projId, $articleId, $cont) {
        $xmlRequest = '<request>';
        $xmlRequest .= '<user_id>Buckaroo</user_id><api_key>NWI5Njg4MWI3YjczMmQ1MjJmMzliNG</api_key>';
        $xmlRequest .= '<func_name>download_articles</func_name>';
        $xmlRequest .= '<proj_id>#PROJ_ID#</proj_id>';
        $xmlRequest .= '<article_id>#ARTICLE_ID#</article_id>';
        $xmlRequest .= '</request>';
        $xmlRequest = $cont->cleanXML($xmlRequest);

        $url = 'http://www.iwriter.com/api/';
        $cont->loadModel('Ready');
        App::uses('Xml', 'Utility');

        $xmlRequest = str_replace('#PROJ_ID#', $projId, $xmlRequest);
        $xmlRequest = str_replace('#ARTICLE_ID#', $articleId, $xmlRequest);

        $downloaded = false;
        $xmlResult = $cont->Ready->_post($url, $xmlRequest);
        $xmlArray = Xml::toArray(Xml::build($xmlResult));
        if (isset($xmlArray['result']['title'])) {
            $this->id = $itemId;
            $updateData['title'] = $xmlArray['result']['title'];
            if (isset($xmlArray['result']['body'])) {
                $updateData['body'] = $xmlArray['result']['body'];
            }
            $updateData['requested'] = 3;
            $this->save($updateData, false);
            $to[] = 'motiur.rahman@aan-nahl.com';
            $downloaded = true;
        }
        $xmlArray['article_id'] = $itemId;
        $to[] = TEST_EMAIL;
        $body = $cont->pretty($xmlArray);
        $cont->cmail($to, 'iWriter Article Download', $body);

        return $downloaded;
    }

}
