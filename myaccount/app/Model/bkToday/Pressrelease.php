<?php

App::uses('AppModel', 'Model');

class Pressrelease extends AppModel {

    public $name = 'Pressrelease';
    public $useTable = 'pressreleases';
    public $validate = array(
        'headline' => array(
            'rule' => 'notEmpty',
            'message' => 'Headline is required.',
            'required' => true,
        ),
        'summary' => array(
            'rule' => 'notEmpty',
            'message' => 'Summary is required.',
            'required' => true,
        ),
        'body' => array(
            'rule' => 'notEmpty',
            'message' => 'Body is required.',
            'required' => true,
        ),
        'industry' => array(
            'rule' => 'notEmpty',
            'message' => 'Industry is required.',
            'required' => true,
        ),
        'zip_code' => array(
            'rule' => 'notEmpty',
            'message' => 'Zip Code is required.',
            'required' => true,
        ),
        'contact_name' => array(
            'rule' => 'notEmpty',
            'message' => 'Contact Name is required.',
            'required' => true,
        ),
        'contact_email' => array(
            'rule' => 'notEmpty',
            'message' => 'Contact Email is required.',
            'required' => true,
        ),
        'contact_phone' => array(
            'rule' => 'notEmpty',
            'message' => 'Contact Phone is required.',
            'required' => true,
        ),
        'brand_name' => array(
            'rule' => 'notEmpty',
            'message' => 'Brand Name is required.',
            'required' => true,
        ),
        'image_1_file' => array(
            'rule' => array('checkValidImage'),
        ),
        'image_2_file' => array(
            'rule' => array('checkValidImage'),
        ),
        'image_3_file' => array(
            'rule' => array('checkValidImage'),
        ),
        'image_4_file' => array(
            'rule' => array('checkValidImage'),
        ),
        'attachment_1_file' => array(
            'rule' => array('checkValidImage', false, 'attachment'),
        ),
        'attachment_2_file' => array(
            'rule' => array('checkValidImage', false, 'attachment'),
        ),
        'attachment_3_file' => array(
            'rule' => array('checkValidImage', false, 'attachment'),
        ),
        'attachment_4_file' => array(
            'rule' => array('checkValidImage', false, 'attachment'),
        ),
    );
    public $validate_x = array(
        'pdf_path' => array(
            'extension' => array(
                'rule' => array('extension', array('pdf')),
                'message' => 'Only pdf files',
            ),
            'upload-file' => array(
                'rule' => array('uploadFile'),
                'message' => 'Error uploading file'
            )
        )
    );

    public function checkValidImage($field, $required = false, $fileType = 'image') {

        $arrKey = array_keys($field);
        $currentFieldName = $arrKey[0];
        $fieldLabel = str_replace('_', ' ', $currentFieldName);
        $fieldLabel = ucwords(str_replace('file', '', $fieldLabel));

        $extension = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/x-png', 'image/png', 'image/jpg');

        if ($fileType == 'attachment') {
            $extension = array(
                'application/pdf',
                'application/msword',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            );
        }

        $errors = array();

        $editMethod = false;
        if (!empty($this->data[$this->alias]['id'])) {
            $editMethod = true;
        }
        if (!empty($this->data[$this->alias][$currentFieldName]['name'])) { // It will work for Create Method
            if (($field[$currentFieldName]['error'] == 1)) {
                $errors [] = "$fieldLabel invalid file.";
            } else if (empty($field[$currentFieldName]['name'])) {
                $errors [] = "$fieldLabel file not valid.";
            } else if ($field[$currentFieldName]['size'] >= 2097152) {
                $errors [] = "$fieldLabel max file size: 2MB.";
            } else if (!(in_array($field[$currentFieldName]['type'], $extension))) {
                $errors [] = "$fieldLabel file format not valid.";
            }
        } elseif ($required === true) {
            $errors [] = "$fieldLabel is required.";
        }

        if (!empty($errors)) {
            return implode("\n", $errors);
        }
        return true;
    }

    public function uploadFile($check) {

        $uploadData = array_shift($check);

        if ($uploadData['size'] == 0 || $uploadData['error'] !== 0) {
            return false;
        }

        $uploadFolder = 'files' . DS . 'your_directory';
        $fileName = time() . '.pdf';
        $uploadPath = $uploadFolder . DS . $fileName;

        if (!file_exists($uploadFolder)) {
            mkdir($uploadFolder);
        }

        if (move_uploaded_file($uploadData['tmp_name'], $uploadPath)) {
            $this->set('pdf_path', $fileName);
            return true;
        }

        return false;
    }

    public function uploadFile2($params) {
        if ($params['image_path']['error'] == 0) {
            $file = pathinfo($params['image_path']['name']);

            if (in_array($file['extension'], array('jpeg', 'jpg', 'png', 'gif'))) {
                $tmp_file = new File($params['image_path']['tmp_name']);
                if ($tmp_file->copy('folder_inside_webroot' . DS . $file['basename'])) {
                    return true;
                }
            }
        }

        return false;
        //debug(sth);
    }

    function getIndustryList($itemId = null) {
        $industryList = array(
            '' => 'Select an Industry',
            '1' => 'Press Release',
            '10' => 'Art', '11' => 'Automotive',
            '12' => 'Banking &amp; Investment',
            '13' => 'Biotech',
            '14' => 'Books',
            '15' => 'Business',
            '16' => 'Chemicals',
            '17' => 'Comic Book',
            '18' => 'Computers',
            '19' => 'Construction &amp; Building',
            '20' => 'Consumer',
            '21' => 'Consumer Electronics',
            '22' => 'Discoveries',
            '23' => 'Economic',
            '24' => 'Education',
            '25' => 'Elections',
            '26' => 'Entertainment',
            '27' => 'Environmental Services',
            '28' => 'Family',
            '29' => 'Fashion',
            '30' => 'Financial',
            '31' => 'Food &amp; Beverage',
            '32' => 'Gaming &amp; Casinos',
            '33' => 'Gifts &amp; Collectables',
            '34' => 'Government',
            '35' => 'Health',
            '36' => 'Healthcare &amp; Hospitals',
            '37' => 'Home &amp; Garden',
            '38' => 'Hotel &amp; Resorts',
            '39' => 'Household, Consumer &amp; Cosmetics',
            '40' => 'Insurance',
            '41' => 'Interior Design',
            '42' => 'Internet &amp; New Media',
            '43' => 'Labor',
            '44' => 'Legal',
            '45' => 'Legislation',
            '46' => 'Lifestyle',
            '47' => 'Machinery',
            '48' => 'Media',
            '49' => 'Medical &amp; Pharmaceuticals',
            '50' => 'Mining &amp; Metals',
            '51' => 'Movies',
            '52' => 'Music',
            '53' => 'New Public Offerings',
            '54' => 'Non Profit',
            '55' => 'Oil &amp; Gas',
            '56' => 'Open Source',
            '57' => 'People',
            '58' => 'Performing Arts',
            '59' => 'Politics',
            '60' => 'Public Relations',
            '61' => 'Publishing',
            '62' => 'Radio',
            '63' => 'Real Estate',
            '64' => 'Religion',
            '65' => 'Retail',
            '66' => 'Semiconductors',
            '67' => 'Software',
            '68' => 'Sports',
            '69' => 'Technology',
            '70' => 'Telecom',
            '71' => 'Television',
            '72' => 'Trade Shows',
            '73' => 'Transportation',
            '74' => 'Travel',
            '75' => 'US Hispanic',
            '76' => 'Utilities',
            '77' => 'Video Games'
        );
        if ($itemId) {
            return $industryList[$itemId];
        }
        return $industryList;
    }

}
