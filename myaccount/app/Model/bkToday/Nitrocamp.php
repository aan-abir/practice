<?php

App::uses('AppModel', 'Model');

class Nitrocamp extends AppModel {

    public $name = 'Nitrocamp';
    public $useTable = false;

    function getCampaignStats($domain = null) {

        App::import('Vendor', 'IXR_Library');

        $fullRpcDomain = 'http://www.villo.me/nitroserver.php';
        //$fullRpcDomain = 'http://www.egyptjobsearch.com/nitroserver.php';
        // Create the client object
        $client = new IXR_Client($fullRpcDomain);


        $domainClause = '';
        if ($domain) {
            $domainClause = "AND domain like '%$domain%'";
        }

        $this->Domain = ClassRegistry::init('Domain');
        $this->Trackeddomain = ClassRegistry::init('Trackeddomain');

        $allDomains = $this->Domain->find('all', array('conditions' => "Domain.status = 1 AND Domain.owner_id = -1 $domainClause", 'fields' => 'id,domain,cms'));
        $allDomains = Set::extract('/Domain/.', $allDomains);


        $allDomainsChunks = array_chunk($allDomains, 50);

        //pr($allDomainsChunks);
        //exit;
        //pr($allDomains);

        $affRowsTotal = 0;
        $response = array();
        if ($allDomains) {
            foreach ($allDomainsChunks as $resChunk) {

                $chunkedDomains = array();
                foreach ($resChunk as $chunk) {
                    $chunkedDomains[] = $chunk['domain'];
                }

                //pr($chunkedDomains);
                //exit;

                $this->Nitro = ClassRegistry::init('Nitro');
                $dbInfo = $this->Nitro->nitroDbInfo($chunkedDomains);
                if ($dbInfo) {
                    $params = $dbInfo;

                    //pr($params);
                    //exit;

                    $response = array();

                    if (!$client->query('nitro.getCampaignStats', $params)) {
                        $response[] = 'Error: ' . $client->getErrorCode() . " : " . $client->getErrorMessage();
                        pr($response);
                    } else {
                        $response = $client->getResponse();

                        //pr($response);
                        //exit;
                        //echo count($response);
                        //pr(array('success'));
                        //exit;

                        if ($response) {
                            if ($response) {
                                foreach ($response as $domain => $resultsByDomain) {
                                    $domainId = $this->getDomainIdByPinger($allDomains, $domain);
                                    $domainId = $domainId ? $domainId : 0;

                                    //$prevDelete = $this->Trackeddomain->deleteAll(array('Trackeddomain.domain_id' => $domainId, 'Trackeddomain.pinger like' => "%$domain%"));
                                    //$prevDelete = @query("DELETE FROM $dbTableName WHERE (domain_id = $domainId || pinger like '%$domain%')");
                                    //echo '<br/>';
                                    //echo $prevDelete;
                                    //exit;
                                    $affRows = 0;
                                    if ($domainId) {
                                        foreach ($resultsByDomain as $rBD) {

                                            $campaignId = $rBD['campaign_id'];
                                            $postId = $rBD['post_id'];
                                            $cond = array(
                                                'conditions' => "Trackeddomain.domain_id = $domainId AND Trackeddomain.campaign_id = $campaignId AND Trackeddomain.post_id = $postId"
                                            );
                                            $tdInfo = $this->Trackeddomain->find('first', $cond);

                                            $resultRow['domain_id'] = $domainId;
                                            $resultRow['campaign_id'] = $campaignId;
                                            $resultRow['pinger'] = $rBD['pinger'];
                                            $resultRow['post_id'] = $postId;
                                            $resultRow['codefound'] = $rBD['codefound'];
                                            if ($tdInfo) {
                                                $resultRow['id'] = $tdInfo['Trackeddomain']['id'];
                                            } else {
                                                $this->Trackeddomain->create();
                                            }
                                            pr($resultRow);
                                            /**/
                                            if ($this->Trackeddomain->save($resultRow)) {
                                                $affRows++;
                                                $affRowsTotal++;
                                            }
                                            /* */
                                        }
                                    }
                                    pr(array($domain => $affRows));
                                }
                            }
                        }
                    }
                    pr($response);
                }
                sleep(5);
                //exit;
            }
            pr(array('total' => $affRowsTotal));
        }

        //sleep(10);
        //setResults();
    }

    function getCampaignStatsPrivate($domain = null) {

        $domainClause = '';
        if ($domain) {
            $domainClause = "AND domain like '%$domain%'";
        }

        $limit = '';

        $this->Domain = ClassRegistry::init('Domain');
        $this->Campaign = ClassRegistry::init('Campaign');
        $this->Trackeddomain = ClassRegistry::init('Trackeddomain');
        /**/

        $cond = array(
            'conditions' => "Domain.status = 1 AND Domain.owner_id != -1 $domainClause $limit"
        );

        $allDomains = $this->Domain->find('all', $cond);

        //pr($allDomains);
        //exit;
        //$allDomains = result_array("SELECT * FROM domains WHERE status = 1 AND login_ok = 1 $domainWhereClause $limit");
        $allCampaigns = $this->Campaign->find('all', array('conditions' => 'Campaign.status = 1'));

        //pr($allCampaigns);

        $campaignIds = array();
        if (!empty($allCampaigns)) {
            foreach ($allCampaigns as $aC) {
                $campaignIds[] = $aC['Campaign']['id'];
            }
        }
        $affRowsTotal = 0;
        if (is_array($allDomains) && count($allDomains)) {
            $finalArr = array();
            $count = 1;

            App::import('Vendor', 'IXR_Library');

            foreach ($allDomains as $aDomains) {
                $aD = $aDomains['Domain'];
                $domainRpc = $this->getPlainDomain($aD['domain']);
                echo $domainRpc . '<br/>';
                $domainId = $aD['id'];
                //echo $count . ' => ' . $domainRpc . '<br/>';
                if ($domainRpc) {
                    $fullRpcDomain = 'http://' . $domainRpc;
                    if ($aD['cms'] == 'wordpress') {
                        $fullRpcDomain = $fullRpcDomain;
                    } elseif ($aD['cms'] == 'joomla') {
                        $fullRpcDomain = $fullRpcDomain . '/plugins/system/dcljoomla';
                    }
                    // Create the client object
                    $client = new IXR_ClientSSL($fullRpcDomain . '/xmlrpc.php');
                    //$client->debug = true; // Set it to fase in Production Environment

                    $params = array();
                    $myresponse = array();
                    // Run a query To Read Posts From Wordpress
                    if (!$client->query('nitro.getCampaignStats', $params)) {
                        $myresponse = array($count . ' ' . $domainId . ' ' . $domainRpc . ': ' . $client->getErrorCode() . ' : ' . $client->getErrorMessage());
                    } else {
                        //echo $count . ' ' . $domainId . ' ' . $domainRpc . ': Connection was successfull!' . '<br/><br/>';
                        $myresponse = $client->getResponse();

                        //pr($myresponse);
                        /**/
                        if (is_array($myresponse) && isset($myresponse[0]['domain'])) {
                            $prevDelete = $this->Trackeddomain->deleteAll(array('Trackeddomain.domain_id' => $domainId, 'Trackeddomain.pinger like' => "%$domain%"));
                            if ($prevDelete === true) {
                                echo 'here';
                                $affRows = 0;
                                foreach ($myresponse as $mR) {
                                    if (in_array($mR['campaign_id'], $campaignIds)) {
                                        $resultRow['domain_id'] = $domainId;
                                        $resultRow['campaign_id'] = $mR['campaign_id'];
                                        $resultRow['pinger'] = $mR['pinger'];
                                        $resultRow['post_id'] = $mR['post_id'];
                                        $resultRow['codefound'] = $mR['codefound'];

                                        $this->Trackeddomain->create();
                                        if ($this->Trackeddomain->save($resultRow)) {
                                            $affRows++;
                                            $affRowsTotal++;
                                        }
                                    }
                                }
                                pr(array($domainRpc => $affRows));
                            }
                        }
                        /* */
                    }
                    pr($myresponse);
                }
                $count++;
            }
        }
        /* */
    }

    function setCampaignResultsPrivate($domain = null) {

        $domainClause = '';
        if ($domain) {
            $domainClause = "AND pinger like '%$domain%'";
        }

        $this->Domain = ClassRegistry::init('Domain');
        $this->Trackeddomain = ClassRegistry::init('Trackeddomain');


        $privateDomains = $this->Domain->find('list', array('fields' => 'id', 'conditions' => "Domain.user_id != -1 AND Domain.owner_id != -1"));

        $privateDomainsClause = '';
        if ($privateDomains) {
            $privateDomainsClauseString = implode(',', $privateDomains);
            $privateDomainsClause = "AND Trackeddomain.domain_id IN($privateDomainsClauseString)";
        }

        $cond = array(
            'conditions' => "Trackeddomain.id != 0 $domainClause $privateDomainsClause"
        );

        $allCampaignResults = $this->Trackeddomain->find('all', $cond);
        //$allCampaignResults = result_array("SELECT domains.id,domains.domain,domains.cms,trackeddomains.* FROM trackeddomains JOIN domains ON domains.id = trackeddomains.domain_id $domainClause");

        if (isset($_GET['exit'])) {
            pr($allCampaignResults);
        }

        $finalArr = array();
        if ($allCampaignResults) {
            foreach ($allCampaignResults as $aCR) {
                $campInfo = $aCR['Trackeddomain'];
                if ($campInfo['domain']) {
                    $domain = $campInfo['domain'];

                    //$temp['domain'] = $domain;
                    if ($campInfo['results']) {
                        $temp['campaign_id'] = $campInfo['campaign_id'];
                        $temp['pinger'] = $campInfo['pinger'];
                        $temp['results'] = $campInfo['results'];
                        $finalArr[$domain][] = $temp;
                    }
                }
            }
            if (isset($_GET['exit'])) {
                pr($finalArr);
                exit;
            }
            pr($finalArr);
            if ($finalArr) {

                foreach ($finalArr as $dom => $res) {

                    App::import('Vendor', 'IXR_Library');
                    echo $fullRpcDomain = 'http://' . $dom . '/xmlrpc.php';
                    //$fullRpcDomain = 'http://www.egyptjobsearch.com/nitroserver.php';
                    // Create the client object
                    $client = new IXR_ClientSSL($fullRpcDomain);

                    $params = $res;

                    $response = array();
                    if (!$client->query('nitro.setCampaignResults', $params)) {
                        $response[] = 'Error: ' . $client->getErrorCode() . " : " . $client->getErrorMessage();
                    } else {
                        $response = $client->getResponse();
                        if ($response) {
                            //pr(array('success'));
                        }
                    }
                }
                pr($response);
            }
        }
        mail('mainulnitro@gmail.com', 'Setting Campaign Results', 'Setting success!');
    }

    function getPlainDomain($url) {
        $url = str_replace('http://', '', strtolower($url));
        $url = str_replace('https://', '', $url);
        $plainDomain = str_replace('www.', '', $url);
        if (strpos($url, '/')) {
            $plainDomain = strstr($url, '/', true);
        }
        return $plainDomain;
    }

    private
            function getDomainIdByPinger($dataArr, $pinger) {
        $reId = false;
        if (is_array($dataArr) && count($dataArr)) {
            foreach ($dataArr as $dA) {
                if (strpos($pinger, $dA['domain']) !== false) {
                    $reId = $dA['id'];
                    break;
                }
            }
        }
        return $reId;
    }

    function getUniqueIdsByField($dataArr, $fieldName, $dataArr2 = array()) {
        $reArr = array();
        if (is_array($dataArr) && count($dataArr)) {
            foreach ($dataArr as $dA) {
                $reArr[] = $dA[$fieldName];
            }
            $reArr = array_unique($reArr);
        }
        if (is_array($dataArr2) && count($dataArr2)) {
            foreach ($dataArr2 as $dA) {
                $reArr[] = $dA[$fieldName];
            }
        }
        $reArr = array_unique($reArr);
        sort($reArr);
        return $reArr;
    }

    function getUniqueDomainIdsByCid($dataArr, $dataArr2, $cId) {
        $reArr = array();
        if (is_array($dataArr) && count($dataArr)) {
            foreach ($dataArr as $dA) {
                if ($dA['campaign_id'] == $cId) {
                    $reArr[] = $dA['domain_id'];
                }
            }
            $reArr = array_unique($reArr);
        }
        if (is_array($dataArr2) && count($dataArr2)) {
            foreach ($dataArr2 as $dA) {
                if ($dA['campaign_id'] == $cId) {
                    $reArr[] = $dA['domain_id'];
                }
            }
        }
        $reArr = array_unique($reArr);
        return $reArr;
    }

    function getBlogRollByCid($dataArr, $cId) {
        $reNum = 0;
        if (is_array($dataArr) && count($dataArr)) {
            $foundByCid = 0;
            foreach ($dataArr as $k => $dA) {
                if (($dA['campaign_id'] == $cId)) {
                    $foundByCid +=1;
                }
            }
            $reNum = $foundByCid;
        }
        return $reNum;
    }

    function getCodeFoundByCid($dataArr, $cId) {
        $reNum = 0;
        if (is_array($dataArr) && count($dataArr)) {
            $foundByCid = 0;
            foreach ($dataArr as $k => $dA) {
                $codeFound = $dA['codefound'] ? $dA['codefound'] : 0;
                if (($dA['campaign_id'] == $cId)) {
                    $foundByCid += $codeFound;
                }
            }
            $reNum = $foundByCid;
        }
        return $reNum;
    }

    function getAnchorListByCid($anchorList, $cId) {
        $finalList = array();
        if (is_array($anchorList) && count($anchorList)) {
            foreach ($anchorList as $k => $data) {
                if ($data['campaign_id'] == $cId) {

                    $tDensity = intval(trim($data['targetDensity']));
                    $aText = trim($data['anchortexts']);
                    $aNoFollow = $data['nofollow'];
                    $aTextArr = (array) explode(",", $aText);
                    $uText = trim($data['internalPageUrl']);
                    $uTextArr = (array) explode(",", $uText);
                    $tDensityEach = $tDensity / count($aTextArr);

                    $aTextArr = array_filter($aTextArr);
                    $uTextArr = array_filter($uTextArr);

                    if (($tDensity == '') || ($aText == ''))
                        continue;

                    $aCount = count($aTextArr) ? count($aTextArr) : 0;
                    $aTextFullArr = array();
                    if ($aCount) {
                        $uIndex = 0;
                        for ($i = 0; $i < $aCount; $i++) {
                            if (isset($uTextArr[$uIndex])) {
                                $aTextFullArr[] = trim($uTextArr[$uIndex]);
                            } else {
                                $uIndex = 0;
                                $aTextFullArr[] = trim($uTextArr[$uIndex]);
                            }
                            $uIndex++;
                        }
                    }
                    // all multiple anchor text with commas
                    for ($ii = 0; $ii < count($aTextArr); $ii++) {
                        $temp = array();
                        $noFollow = '';
                        if ($aNoFollow == 1) {
                            $noFollow = 'rel="nofollow" ';
                        }
                        $interPageUrl = isset($aTextFullArr[$ii]) ? $aTextFullArr[$ii] : '#';
                        $temp['campaign_id'] = $cId;
                        $temp['anchorText'] = isset($aTextArr[$ii]) ? $aTextArr[$ii] : '-';
                        $temp['internalPageUrl'] = str_replace(array(" ", "\r\n", "\n\r", "\n", "\r"), '', trim($interPageUrl));
                        $temp['targetDensity'] = $tDensityEach;
                        $temp['noFollow'] = $noFollow;
                        $temp['anchorText'] = str_replace(array("\r\n", "\n\r", "\n", "\r"), '', trim($temp['anchorText']));
                        //$temp['anchorLink'] = '<a' . $noFollow . ' href="' . 'http://' . str_replace('http://', '', $temp['internalPageUrl']) . '">' . $temp['anchorText'] . '</a>';
                        $finalList[] = $temp;
                        //pr($temp);
                    }
                }
            }
        }
        return $finalList;
    }

    function getContentAnchorArray($anchorList, $count) {
        $arr = array();
        $versionCount = 3;
        if ($anchorList) {
            //pr($anchorList);

            $tempURLs = array();
            $tempAnchorTexts = array();

            $countM = 0;
            foreach ($anchorList as $k => $aL) {
                $times = @ceil($count * ( $aL['targetDensity'] / 100 ));
                if ($times > 0) {
                    for ($i = 0; $i < $times; $i++) {
                        $temp['arrIndex'] = $countM;
                        $temp['campaign_id'] = $aL['campaign_id'];
                        $temp['anchorText'] = $aL['anchorText'];
                        $temp['noFollow'] = $aL['noFollow'];
                        $temp['internalPageUrl'] = str_replace(' ', '', preg_replace('/https?\:\/\//i', '', trim($aL['internalPageUrl'])));
                        $temp['finalLink'] = '<a ' . $aL['noFollow'] . 'href="http://' . preg_replace('/https?\:\/\//i', '', trim($aL['internalPageUrl'])) . '">' . trim($aL['anchorText']) . '</a>';
                        $tempURLs[] = $temp;
                        $tempAnchorTexts[] = $aL['anchorText'];
                        //$tempAnchorUrls[] = $temp['finalLink'];
                        $countM++;
                    }
                }
            }
            //pr($tempURLs);
            //pr($tempAnchorTexts);
            if ($tempURLs) {
                $tempURLs = array_slice($tempURLs, 0, $count);
                $arr['aLinks'] = $this->seoShuffleMtSrand($tempURLs, $versionCount);
                $tempAnchorTexts = array_slice($tempAnchorTexts, 0, $count);
                for ($mt = 0; $mt <= $versionCount; $mt++) {
                    //$arr['aTexts'][] = seoShuffleMtSrand($tempAnchorTexts, $mt);
                    $arr['aTexts'][] = $tempAnchorTexts;
                    //$arr['sLinks'][] = seoShuffleMtSrand($tempAnchorUrls, $mt);
                }
            }
        }
        //pr($arr);
        //exit;
        return $arr;
    }

    function getCodeFoundByDomainCampaignId($dataArr, $dId, $cId) {
        $reArr = array();
        if (is_array($dataArr) && count($dataArr)) {
            $foundForSidebar = 0;
            $foundForFooter = 0;
            foreach ($dataArr as $k => $dA) {
                if (($dA['domain_id'] == $dId) && ($dA['campaign_id'] == $cId)) {
                    if ($dA['setting_option'] == 1) {
                        $foundForSidebar +=1;
                    } elseif ($dA['setting_option'] == 2) {
                        $foundForFooter +=1;
                    }
                }
            }
            $reArr['sidebar'] = $foundForSidebar;
            $reArr['footer'] = $foundForFooter;
        }
        return $reArr;
    }

    function getAllPingersBy_DomainId_cId($dataArr, $cId, $domainId) {
        $reArr = array();
        if (is_array($dataArr) && count($dataArr)) {
            foreach ($dataArr as $k => $dA) {
                if ($dA['campaign_id'] == $cId && $dA['domain_id'] == $domainId && $dA['campaign_id'] != -1) {
                    $reArr[] = $dA;
                }
            }
        }
        return $reArr;
    }

    function getTrackedDomainsId($dataArr, $cId, $domainId, $pinger) {
        $reArr = array();
        if (is_array($dataArr) && count($dataArr)) {
            foreach ($dataArr as $k => $dA) {
                if ($dA['campaign_id'] == $cId && $dA['domain_id'] == $domainId && $dA['pinger'] == $pinger) {
                    $reArr = $dA;
                    break;
                }
            }
        }
        return $reArr;
    }

    function seoShuffleMtSrand($items, $mtSrand = 1) {

        if ($items) {
            mt_srand($mtSrand);
            for ($i = count($items) - 1; $i > 0; $i--) {
                $j = @mt_rand(0, $i);
                $tmp = $items[$i];
                $items[$i] = $items[$j];
                $items[$j] = $tmp;
            }
        }
        return $items;
    }

    function updateCodeFound($postInfo, $buffer) {
        $reCids = array();
        if (count($buffer)) {
            //$this->Trackeddomain = ClassRegistry::init('Trackeddomain');
            // check if [dcl=??] codes are found in the content
            preg_match_all('/\[dcl\=(\d+)\]/is', $buffer, $matches);
            $foundIds = $matches[1];

            // if any codes are found start the process
            if (count($foundIds)) {
                // get the unique campaign ids as multiple codes for single campaign can exist
                $cIds = array_unique($foundIds);

                // start looping through the found campaign ids
                foreach ($cIds as $cId) {
                    // get the total code found for the current campaign id
                    $totalCodeFound = 0;
                    foreach ($foundIds as $fId) {
                        if ($fId == $cId)
                            $totalCodeFound++;
                    }

                    $insertData['campaign_id'] = $cId;
                    $insertData['domain_id'] = $postInfo['domain_id'];
                    $insertData['pinger'] = $postInfo['post_url'];
                    $insertData['post_id'] = $postInfo['post_id'];
                    $insertData['codefound'] = $totalCodeFound;

                    ClassRegistry::init('Trackeddomain')->create();
                    ClassRegistry::init('Trackeddomain')->save($insertData, false);
                    $reCids[] = $cId;
                }
            }
        }
        return $reCids;
    }

    function updateCodeFound2($postInfo, $buffer) {
        $reCids = array();
        if (count($buffer)) {
            //$this->Trackeddomain = ClassRegistry::init('Trackeddomain');
            // check if [dcl=??] codes are found in the content
            preg_match_all('/\[dcl\=(\d+)\]/is', $buffer, $matches);
            $foundIds = $matches[1];

            // if any codes are found start the process
            if (count($foundIds)) {
                // get the unique campaign ids as multiple codes for single campaign can exist
                $cIds = array_unique($foundIds);

                // start looping through the found campaign ids
                foreach ($cIds as $cId) {
                    // get the total code found for the current campaign id
                    $totalCodeFound = 0;
                    foreach ($foundIds as $fId) {
                        if ($fId == $cId)
                            $totalCodeFound++;
                    }

                    $insertData['campaign_id'] = $cId;
                    $insertData['domain_id'] = $domainId = $postInfo['domain_id'];
                    $insertData['pinger'] = $pinger = $postInfo['post_url'];
                    $insertData['post_id'] = $postInfo['post_id'];
                    $insertData['codefound'] = $totalCodeFound;

                    $prev = ClassRegistry::init('Trackeddomain')->find('first', array('conditions' => array('domain_id' => $domainId, 'pinger' => $pinger)));

                    if ($prev) {
                        $insertData['id'] = $prev['Trackeddomain']['id'];
                    } else {
                        ClassRegistry::init('Trackeddomain')->create();
                    }                    
                    ClassRegistry::init('Trackeddomain')->save($insertData, false);
                    $reCids[] = $cId;
                }
            }
        }
        return $reCids;
    }

}
