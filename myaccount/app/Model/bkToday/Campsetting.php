<?php

App::uses('AppModel', 'Model');

class Campsetting extends AppModel {

    public $name = 'Campsetting';
    public $useTable = 'campsettings';
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id'
        ),
    );

}

// end class
?>