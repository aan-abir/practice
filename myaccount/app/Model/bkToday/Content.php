<?php

App::uses('AppModel', 'Model');

class Content extends AppModel {

    public $name = 'Content';
    public $useTable = 'contents';
    public $validate = array(
        'title' => array(
            'not_empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Title is required.',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'content' => array(
            'not_empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Content is required.',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
    );

}
