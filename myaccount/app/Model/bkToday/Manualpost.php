<?php

App::uses('AppModel', 'Model');

class Manualpost extends AppModel {

    public $name = 'Manualpost';
    public $useTable = 'manualposts';
    public $validate = array(
        'post_date' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Post date is required.',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'domain_id' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Domain is required.',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'post_title' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Post title is required.',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'post_content' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Post content is required.',
                'allowEmpty' => false,
                'required' => true
            )
        )
    );
    public $order = array('Manualpost.id' => 'DESC');

}
