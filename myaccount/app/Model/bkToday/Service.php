<?php

App::uses('AppModel', 'Model');

class Service extends AppModel {

    public $name = 'Service';
    public $useTable = 'services';
    public $validate = array(
        'servicename' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Service Name can not be left empty',
                'allowEmpty' => false,
                'required' => true,
            )
        ),
        'default_unit_price' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Unit Price is required',
                'allowEmpty' => false,
                'required' => true
            ),
            'onlyNumber' => array(
                'rule' => 'numeric',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Unit Price should be in number'
            )
        )
    );
    public $order = array("Service.servicename" => "ASC");

}

?>