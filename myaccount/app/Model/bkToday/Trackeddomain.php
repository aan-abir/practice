<?php

App::uses('AppModel', 'Model');

class Trackeddomain extends AppModel {

    public $name = 'Trackeddomain';
    public $useTable = 'trackeddomains';
    public $virtualFields = array(
        'campignname' => 'SELECT campignname FROM campaigns WHERE campaigns.id = Trackeddomain.campaign_id',
        'user_id' => 'SELECT user_id FROM campaigns WHERE campaigns.id = Trackeddomain.campaign_id',
        'domain' => 'SELECT domain FROM domains WHERE domains.id = Trackeddomain.domain_id'
    );

}
