<?php

App::uses('AppModel', 'Model');

class Campaign extends AppModel {

    public $name = 'Campaign';
    public $useTable = 'campaigns';
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );
    public $validate = array(
        'campignname' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Campaign Name can not left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'tPercentage' => array(
            'tPercentage' => array(
                'rule' => array('check100', 100),
                'message' => 'Percentage Must be 100%'
            )
        ),
    );

    public function check100($check, $limit) {
        return $check['tPercentage'] == 100 ? true : false;
    }
}

// end class
?>