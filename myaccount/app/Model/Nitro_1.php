<?php

App::uses('AppModel', 'Model');

class Nitro extends AppModel {

    public $name = 'NitroCustom';
    public $useTable = false;

    function curl_get_contents($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }

    function nitroDbInfo(array $reDomains = null) {

        $dbAllKey = array(
            'michaeldoverlaw.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1001_',
                'SITE_URL' => 'http://www.michaeldoverlaw.com',
                'DOMAIN' => 'michaeldoverlaw.com',
            ),
            'courtreportingstudent.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1248_',
                'SITE_URL' => 'http://www.courtreportingstudent.com',
                'DOMAIN' => 'courtreportingstudent.com',
            ),
            'dcmedicalmalpracticeattorneys.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1249_',
                'SITE_URL' => 'http://www.dcmedicalmalpracticeattorneys.com',
                'DOMAIN' => 'dcmedicalmalpracticeattorneys.com',
            ),
            'debteliminationmethods.info' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1250_',
                'SITE_URL' => 'http://www.debteliminationmethods.info',
                'DOMAIN' => 'debteliminationmethods.info',
            ),
            'dental-studio-muzevic.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1251_',
                'SITE_URL' => 'http://www.dental-studio-muzevic.com',
                'DOMAIN' => 'dental-studio-muzevic.com',
            ),
            'kkunimediteran.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1473_',
                'SITE_URL' => 'http://www.kkunimediteran.com',
                'DOMAIN' => 'kkunimediteran.com',
            ),
            'kochobisexual.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_283_',
                'SITE_URL' => 'http://www.kochobisexual.com',
                'DOMAIN' => 'kochobisexual.com',
            ),
            'highperformancemail.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_284_',
                'SITE_URL' => 'http://www.highperformancemail.com',
                'DOMAIN' => 'highperformancemail.com',
            ),
            'marcparctransportation.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_293_',
                'SITE_URL' => 'http://www.marcparctransportation.com',
                'DOMAIN' => 'marcparctransportation.com',
            ),
            'moratumv.net' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_294_',
                'SITE_URL' => 'http://www.moratumv.net',
                'DOMAIN' => 'moratumv.net',
            ),
            'spccic.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_346_',
                'SITE_URL' => 'http://www.spccic.com',
                'DOMAIN' => 'spccic.com',
            ),
            'mymunchmusic.com' =>
            array(
                'TABLE_SCHEMA' => '25kw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1648_',
                'SITE_URL' => 'http://www.mymunchmusic.com',
                'DOMAIN' => 'mymunchmusic.com',
            ),
            'atlanticoceanclub.com' =>
            array(
                'TABLE_SCHEMA' => '25kw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1677_',
                'SITE_URL' => 'http://www.atlanticoceanclub.com',
                'DOMAIN' => 'atlanticoceanclub.com',
            ),
            'datatranzx.com' =>
            array(
                'TABLE_SCHEMA' => '25kw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1678_',
                'SITE_URL' => 'http://www.datatranzx.com',
                'DOMAIN' => 'datatranzx.com',
            ),
            'lawrencedunlapllc.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1204_',
                'SITE_URL' => 'http://www.lawrencedunlapllc.com',
                'DOMAIN' => 'lawrencedunlapllc.com',
            ),
            'macappelli.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1205_',
                'SITE_URL' => 'http://www.macappelli.com',
                'DOMAIN' => 'macappelli.com',
            ),
            'garbosvintage.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1206_',
                'SITE_URL' => 'http://www.garbosvintage.com',
                'DOMAIN' => 'garbosvintage.com',
            ),
            'helledale.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1207_',
                'SITE_URL' => 'http://www.helledale.com',
                'DOMAIN' => 'helledale.com',
            ),
            'johnsonprintingco.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1208_',
                'SITE_URL' => 'http://www.johnsonprintingco.com',
                'DOMAIN' => 'johnsonprintingco.com',
            ),
            'passzivjovedelem.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1476_',
                'SITE_URL' => 'http://www.passzivjovedelem.com',
                'DOMAIN' => 'passzivjovedelem.com',
            ),
            'luke6project.org' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_812_',
                'SITE_URL' => 'http://www.luke6project.org',
                'DOMAIN' => 'luke6project.org',
            ),
            'youthyx.org' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_813_',
                'SITE_URL' => 'http://www.youthyx.org',
                'DOMAIN' => 'youthyx.org',
            ),
            'tongchengqingren.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_814_',
                'SITE_URL' => 'http://www.tongchengqingren.com',
                'DOMAIN' => 'tongchengqingren.com',
            ),
            'radion1.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_815_',
                'SITE_URL' => 'http://www.radion1.com',
                'DOMAIN' => 'radion1.com',
            ),
            'beyondphiladelphia.org' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_816_',
                'SITE_URL' => 'http://www.beyondphiladelphia.org',
                'DOMAIN' => 'beyondphiladelphia.org',
            ),
            'dearprovidence.org' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1195_',
                'SITE_URL' => 'http://www.dearprovidence.org',
                'DOMAIN' => 'dearprovidence.org',
            ),
            'desigbilbao.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1196_',
                'SITE_URL' => 'http://www.desigbilbao.com',
                'DOMAIN' => 'desigbilbao.com',
            ),
            'desigibiza.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1197_',
                'SITE_URL' => 'http://www.desigibiza.com',
                'DOMAIN' => 'desigibiza.com',
            ),
            'jokeorjoker.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1477_',
                'SITE_URL' => 'http://www.jokeorjoker.com',
                'DOMAIN' => 'jokeorjoker.com',
            ),
            'giny.us' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_802_',
                'SITE_URL' => 'http://www.giny.us',
                'DOMAIN' => 'giny.us',
            ),
            'ctfnepa.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_803_',
                'SITE_URL' => 'http://www.ctfnepa.com',
                'DOMAIN' => 'ctfnepa.com',
            ),
            'ncarborday.org' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_804_',
                'SITE_URL' => 'http://www.ncarborday.org',
                'DOMAIN' => 'ncarborday.org',
            ),
            'dearcomic.com' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1478_',
                'SITE_URL' => 'http://www.dearcomic.com',
                'DOMAIN' => 'dearcomic.com',
            ),
            'statsbook.me' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_797_',
                'SITE_URL' => 'http://www.statsbook.me',
                'DOMAIN' => 'statsbook.me',
            ),
            'marthaforcongress.org' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_798_',
                'SITE_URL' => 'http://www.marthaforcongress.org',
                'DOMAIN' => 'marthaforcongress.org',
            ),
            'jointmeeting2011bursa.org' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_799_',
                'SITE_URL' => 'http://www.jointmeeting2011bursa.org',
                'DOMAIN' => 'jointmeeting2011bursa.org',
            ),
            'mtwyouth.com' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_800_',
                'SITE_URL' => 'http://www.mtwyouth.com',
                'DOMAIN' => 'mtwyouth.com',
            ),
            'easyaccesschicago.com' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_801_',
                'SITE_URL' => 'http://www.easyaccesschicago.com',
                'DOMAIN' => 'easyaccesschicago.com',
            ),
            'ctiaustin.org' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1192_',
                'SITE_URL' => 'http://www.ctiaustin.org',
                'DOMAIN' => 'ctiaustin.org',
            ),
            'cumtyc.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1193_',
                'SITE_URL' => 'http://www.cumtyc.com',
                'DOMAIN' => 'cumtyc.com',
            ),
            'daisygrove.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1194_',
                'SITE_URL' => 'http://www.daisygrove.com',
                'DOMAIN' => 'daisygrove.com',
            ),
            '6kranch.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1469_',
                'SITE_URL' => 'http://www.6kranch.com',
                'DOMAIN' => '6kranch.com',
            ),
            'phoneapes.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1479_',
                'SITE_URL' => 'http://www.phoneapes.com',
                'DOMAIN' => 'phoneapes.com',
            ),
            'bariumspringsymca.org' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_682_',
                'SITE_URL' => 'http://www.bariumspringsymca.org',
                'DOMAIN' => 'bariumspringsymca.org',
            ),
            'eastlakesaag.org' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_683_',
                'SITE_URL' => 'http://www.eastlakesaag.org',
                'DOMAIN' => 'eastlakesaag.org',
            ),
            'bbwclubcatalina.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_684_',
                'SITE_URL' => 'http://www.bbwclubcatalina.com',
                'DOMAIN' => 'bbwclubcatalina.com',
            ),
            'marylouisekillen.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_685_',
                'SITE_URL' => 'http://www.marylouisekillen.com',
                'DOMAIN' => 'marylouisekillen.com',
            ),
            'laboratoire-bi-op.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_686_',
                'SITE_URL' => 'http://www.laboratoire-bi-op.com',
                'DOMAIN' => 'laboratoire-bi-op.com',
            ),
            'bracbusinessguide.org' =>
            array(
                'TABLE_SCHEMA' => 'AARC-MA_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1480_',
                'SITE_URL' => 'http://www.bracbusinessguide.org',
                'DOMAIN' => 'bracbusinessguide.org',
            ),
            'mendozavinoyaventura.com' =>
            array(
                'TABLE_SCHEMA' => 'AARC-MA_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_792_',
                'SITE_URL' => 'http://www.mendozavinoyaventura.com',
                'DOMAIN' => 'mendozavinoyaventura.com',
            ),
            'thebimah.org' =>
            array(
                'TABLE_SCHEMA' => 'AARC-MA_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_793_',
                'SITE_URL' => 'http://www.thebimah.org',
                'DOMAIN' => 'thebimah.org',
            ),
            'iphone4sjailbreakhome.com' =>
            array(
                'TABLE_SCHEMA' => 'AARC-MA_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_794_',
                'SITE_URL' => 'http://www.iphone4sjailbreakhome.com',
                'DOMAIN' => 'iphone4sjailbreakhome.com',
            ),
            'presidentofplanetearth.com' =>
            array(
                'TABLE_SCHEMA' => 'AARC-MA_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_795_',
                'SITE_URL' => 'http://www.presidentofplanetearth.com',
                'DOMAIN' => 'presidentofplanetearth.com',
            ),
            'jazzmonttremblant.com' =>
            array(
                'TABLE_SCHEMA' => 'AARC-MA_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_796_',
                'SITE_URL' => 'http://www.jazzmonttremblant.com',
                'DOMAIN' => 'jazzmonttremblant.com',
            ),
            'france-r.com' =>
            array(
                'TABLE_SCHEMA' => 'achimota_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1651_',
                'SITE_URL' => 'http://www.france-r.com',
                'DOMAIN' => 'france-r.com',
            ),
            'andrearakocevic.com' =>
            array(
                'TABLE_SCHEMA' => 'achimota_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1679_',
                'SITE_URL' => 'http://www.andrearakocevic.com',
                'DOMAIN' => 'andrearakocevic.com',
            ),
            'blueruinpdx.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_33_',
                'SITE_URL' => 'http://www.blueruinpdx.com',
                'DOMAIN' => 'blueruinpdx.com',
            ),
            'seiinvests.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_34_',
                'SITE_URL' => 'http://www.seiinvests.com',
                'DOMAIN' => 'seiinvests.com',
            ),
            'april28.net' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_35_',
                'SITE_URL' => 'http://www.april28.net',
                'DOMAIN' => 'april28.net',
            ),
            'hidaminoiimono.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_36_',
                'SITE_URL' => 'http://www.hidaminoiimono.com',
                'DOMAIN' => 'hidaminoiimono.com',
            ),
            'ntsblog.info' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_37_',
                'SITE_URL' => 'http://www.ntsblog.info',
                'DOMAIN' => 'ntsblog.info',
            ),
            'designersgivingback.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_95_',
                'SITE_URL' => 'http://www.designersgivingback.com',
                'DOMAIN' => 'designersgivingback.com',
            ),
            'avexdesigns.net' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_96_',
                'SITE_URL' => 'http://www.avexdesigns.net',
                'DOMAIN' => 'avexdesigns.net',
            ),
            'debrachesnut2012.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_97_',
                'SITE_URL' => 'http://www.debrachesnut2012.com',
                'DOMAIN' => 'debrachesnut2012.com',
            ),
            'leonaadvancedvirtual.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_99_',
                'SITE_URL' => 'http://www.leonaadvancedvirtual.com',
                'DOMAIN' => 'leonaadvancedvirtual.com',
            ),
            'nikaberle.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1481_',
                'SITE_URL' => 'http://www.nikaberle.com',
                'DOMAIN' => 'nikaberle.com',
            ),
            'mercresearch.org' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_782_',
                'SITE_URL' => 'http://www.mercresearch.org',
                'DOMAIN' => 'mercresearch.org',
            ),
            'naisu.info' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_783_',
                'SITE_URL' => 'http://www.naisu.info',
                'DOMAIN' => 'naisu.info',
            ),
            'mpaudiovisual.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_784_',
                'SITE_URL' => 'http://www.mpaudiovisual.com',
                'DOMAIN' => 'mpaudiovisual.com',
            ),
            'willwatkinsphotography.com' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1482_',
                'SITE_URL' => 'http://www.willwatkinsphotography.com',
                'DOMAIN' => 'willwatkinsphotography.com',
            ),
            'nervetraining.com' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_787_',
                'SITE_URL' => 'http://www.nervetraining.com',
                'DOMAIN' => 'nervetraining.com',
            ),
            'pet-rescues.com' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_788_',
                'SITE_URL' => 'http://www.pet-rescues.com',
                'DOMAIN' => 'pet-rescues.com',
            ),
            'reviewmycontracts.com' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_789_',
                'SITE_URL' => 'http://www.reviewmycontracts.com',
                'DOMAIN' => 'reviewmycontracts.com',
            ),
            'xn--vhq70fbd66i0yp6poitbw22ir69adxr.com' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_790_',
                'SITE_URL' => 'http://www.xn--vhq70fbd66i0yp6poitbw22ir69adxr.com',
                'DOMAIN' => 'xn--vhq70fbd66i0yp6poitbw22ir69adxr.com',
            ),
            'triviafriends.mobi' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_791_',
                'SITE_URL' => 'http://www.triviafriends.mobi',
                'DOMAIN' => 'triviafriends.mobi',
            ),
            'arubaisbest.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1222_',
                'SITE_URL' => 'http://www.arubaisbest.com',
                'DOMAIN' => 'arubaisbest.com',
            ),
            'auto---insurance.org' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1223_',
                'SITE_URL' => 'http://www.auto---insurance.org',
                'DOMAIN' => 'auto---insurance.org',
            ),
            'capturedreammoments.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1224_',
                'SITE_URL' => 'http://www.capturedreammoments.com',
                'DOMAIN' => 'capturedreammoments.com',
            ),
            'coffeenewsmalaysia.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1225_',
                'SITE_URL' => 'http://www.coffeenewsmalaysia.com',
                'DOMAIN' => 'coffeenewsmalaysia.com',
            ),
            'downtownautomotiverepair.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1226_',
                'SITE_URL' => 'http://www.downtownautomotiverepair.com',
                'DOMAIN' => 'downtownautomotiverepair.com',
            ),
            'vicawards.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1483_',
                'SITE_URL' => 'http://www.vicawards.com',
                'DOMAIN' => 'vicawards.com',
            ),
            'hfhwaukesha.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_832_',
                'SITE_URL' => 'http://www.hfhwaukesha.com',
                'DOMAIN' => 'hfhwaukesha.com',
            ),
            'jerryning.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_833_',
                'SITE_URL' => 'http://www.jerryning.com',
                'DOMAIN' => 'jerryning.com',
            ),
            'feastonthis.org' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_834_',
                'SITE_URL' => 'http://www.feastonthis.org',
                'DOMAIN' => 'feastonthis.org',
            ),
            'akamaspenthouse.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_835_',
                'SITE_URL' => 'http://www.akamaspenthouse.com',
                'DOMAIN' => 'akamaspenthouse.com',
            ),
            'trentonatelier.com' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1484_',
                'SITE_URL' => 'http://www.trentonatelier.com',
                'DOMAIN' => 'trentonatelier.com',
            ),
            'kibbutz4u.org' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_777_',
                'SITE_URL' => 'http://www.kibbutz4u.org',
                'DOMAIN' => 'kibbutz4u.org',
            ),
            'emnetwork.us' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_778_',
                'SITE_URL' => 'http://www.emnetwork.us',
                'DOMAIN' => 'emnetwork.us',
            ),
            'gorenewablepower.org' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_779_',
                'SITE_URL' => 'http://www.gorenewablepower.org',
                'DOMAIN' => 'gorenewablepower.org',
            ),
            'epochfilms.info' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_780_',
                'SITE_URL' => 'http://www.epochfilms.info',
                'DOMAIN' => 'epochfilms.info',
            ),
            'juicycoutureskyonline.org' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_781_',
                'SITE_URL' => 'http://www.juicycoutureskyonline.org',
                'DOMAIN' => 'juicycoutureskyonline.org',
            ),
            'occupyyourmindpgh.net' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_258_',
                'SITE_URL' => 'http://www.occupyyourmindpgh.net',
                'DOMAIN' => 'occupyyourmindpgh.net',
            ),
            'orlandomarineproducts.info' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_259_',
                'SITE_URL' => 'http://orlandomarineproducts.info',
                'DOMAIN' => 'orlandomarineproducts.info',
            ),
            'overbrookbrothers.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_260_',
                'SITE_URL' => 'http://www.overbrookbrothers.com',
                'DOMAIN' => 'overbrookbrothers.com',
            ),
            'overbrookbrothersfilm.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_261_',
                'SITE_URL' => 'http://overbrookbrothersfilm.com',
                'DOMAIN' => 'overbrookbrothersfilm.com',
            ),
            'lonestaradultstore.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1485_',
                'SITE_URL' => 'http://www.lonestaradultstore.com',
                'DOMAIN' => 'lonestaradultstore.com',
            ),
            'alojate.info' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_772_',
                'SITE_URL' => 'http://www.alojate.info',
                'DOMAIN' => 'alojate.info',
            ),
            'dallasburntraumaresearch.org' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_774_',
                'SITE_URL' => 'http://www.dallasburntraumaresearch.org',
                'DOMAIN' => 'dallasburntraumaresearch.org',
            ),
            'healthyhappyhottness.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_775_',
                'SITE_URL' => 'http://www.healthyhappyhottness.com',
                'DOMAIN' => 'healthyhappyhottness.com',
            ),
            'katesmartart.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_776_',
                'SITE_URL' => 'http://www.katesmartart.com',
                'DOMAIN' => 'katesmartart.com',
            ),
            'flexpackcon.co' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1486_',
                'SITE_URL' => 'http://www.flexpackcon.co',
                'DOMAIN' => 'flexpackcon.co',
            ),
            'cennywenner.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_767_',
                'SITE_URL' => 'http://www.cennywenner.com',
                'DOMAIN' => 'cennywenner.com',
            ),
            'cleananglingpledge.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_768_',
                'SITE_URL' => 'http://www.cleananglingpledge.com',
                'DOMAIN' => 'cleananglingpledge.com',
            ),
            'damian-szyszko.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_769_',
                'SITE_URL' => 'http://www.damian-szyszko.com',
                'DOMAIN' => 'damian-szyszko.com',
            ),
            'eaglehealthcarestaffing.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_770_',
                'SITE_URL' => 'http://www.eaglehealthcarestaffing.com',
                'DOMAIN' => 'eaglehealthcarestaffing.com',
            ),
            '888baysite.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_771_',
                'SITE_URL' => 'http://www.888baysite.com',
                'DOMAIN' => '888baysite.com',
            ),
            'vanwindensgc.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_16_',
                'SITE_URL' => 'http://www.vanwindensgc.com',
                'DOMAIN' => 'vanwindensgc.com',
            ),
            'cogrecruitingprocess.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_17_',
                'SITE_URL' => 'http://www.cogrecruitingprocess.com',
                'DOMAIN' => 'cogrecruitingprocess.com',
            ),
            'studentrightsadvocates.org' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_18_',
                'SITE_URL' => 'http://www.studentrightsadvocates.org',
                'DOMAIN' => 'studentrightsadvocates.org',
            ),
            'thefirezone.net' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_19_',
                'SITE_URL' => 'http://www.thefirezone.net',
                'DOMAIN' => 'thefirezone.net',
            ),
            'hdpornoxo.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_20_',
                'SITE_URL' => 'http://www.hdpornoxo.com',
                'DOMAIN' => 'hdpornoxo.com',
            ),
            'startuptx.us' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1487_',
                'SITE_URL' => 'http://www.startuptx.us',
                'DOMAIN' => 'startuptx.us',
            ),
            'kidztoyz.biz' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_228_',
                'SITE_URL' => 'http://www.kidztoyz.biz',
                'DOMAIN' => 'kidztoyz.biz',
            ),
            'kenyafarming.info' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_231_',
                'SITE_URL' => 'http://www.kenyafarming.info',
                'DOMAIN' => 'kenyafarming.info',
            ),
            'lambda-delta.info' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_232_',
                'SITE_URL' => 'http://www.lambda-delta.info',
                'DOMAIN' => 'lambda-delta.info',
            ),
            'lastsx.ws' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_234_',
                'SITE_URL' => 'http://www.lastsx.ws',
                'DOMAIN' => 'lastsx.ws',
            ),
            'southernsteelmc.com' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1323_',
                'SITE_URL' => 'http://www.southernsteelmc.com',
                'DOMAIN' => 'southernsteelmc.com',
            ),
            'travelostar.com' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1324_',
                'SITE_URL' => 'http://www.travelostar.com',
                'DOMAIN' => 'travelostar.com',
            ),
            'web-basedbusinesscards.com' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1325_',
                'SITE_URL' => 'http://www.web-basedbusinesscards.com',
                'DOMAIN' => 'web-basedbusinesscards.com',
            ),
            'ltspartners.net' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1488_',
                'SITE_URL' => 'http://www.ltspartners.net',
                'DOMAIN' => 'ltspartners.net',
            ),
            'articledb.mobi' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_964_',
                'SITE_URL' => 'http://www.articledb.mobi',
                'DOMAIN' => 'articledb.mobi',
            ),
            'change-management-training.net' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_965_',
                'SITE_URL' => 'http://www.change-management-training.net',
                'DOMAIN' => 'change-management-training.net',
            ),
            'finansekonomi.org' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_966_',
                'SITE_URL' => 'http://www.finansekonomi.org',
                'DOMAIN' => 'finansekonomi.org',
            ),
            '100kcheeks.org' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1489_',
                'SITE_URL' => 'http://www.100kcheeks.org',
                'DOMAIN' => '100kcheeks.org',
            ),
            'merrelltrips.com' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_220_',
                'SITE_URL' => 'http://www.merrelltrips.com',
                'DOMAIN' => 'merrelltrips.com',
            ),
            'mm36.org' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_229_',
                'SITE_URL' => 'http://www.mm36.org',
                'DOMAIN' => 'mm36.org',
            ),
            'openeraventures.com' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_230_',
                'SITE_URL' => 'http://www.openeraventures.com',
                'DOMAIN' => 'openeraventures.com',
            ),
            'myperfectpralines.com' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_235_',
                'SITE_URL' => 'http://www.myperfectpralines.com',
                'DOMAIN' => 'myperfectpralines.com',
            ),
            'openim.ws' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_242_',
                'SITE_URL' => 'http://www.openim.ws',
                'DOMAIN' => 'openim.ws',
            ),
            'markusgiesler.com' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1490_',
                'SITE_URL' => 'http://www.markusgiesler.com',
                'DOMAIN' => 'markusgiesler.com',
            ),
            'lostandfoundkeys.org' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_179_',
                'SITE_URL' => 'http://www.lostandfoundkeys.org',
                'DOMAIN' => 'lostandfoundkeys.org',
            ),
            'pradoimagen.com' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_204_',
                'SITE_URL' => 'http://www.pradoimagen.com',
                'DOMAIN' => 'pradoimagen.com',
            ),
            'leaferapp.com' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_233_',
                'SITE_URL' => 'http://www.leaferapp.com',
                'DOMAIN' => 'leaferapp.com',
            ),
            'loisfraleyfoundation.com' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_236_',
                'SITE_URL' => 'http://www.loisfraleyfoundation.com',
                'DOMAIN' => 'loisfraleyfoundation.com',
            ),
            'webschemes.net' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1326_',
                'SITE_URL' => 'http://www.webschemes.net',
                'DOMAIN' => 'webschemes.net',
            ),
            'worldofflower.net' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1327_',
                'SITE_URL' => 'http://www.worldofflower.net',
                'DOMAIN' => 'worldofflower.net',
            ),
            'themattperry.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1328_',
                'SITE_URL' => 'http://www.themattperry.com',
                'DOMAIN' => 'themattperry.com',
            ),
            'trafficforwords.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1329_',
                'SITE_URL' => 'http://www.trafficforwords.com',
                'DOMAIN' => 'trafficforwords.com',
            ),
            'mobilehomerecordings.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1666_',
                'SITE_URL' => 'http://www.mobilehomerecordings.com',
                'DOMAIN' => 'mobilehomerecordings.com',
            ),
            'stubhubtop25.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_967_',
                'SITE_URL' => 'http://www.stubhubtop25.com',
                'DOMAIN' => 'stubhubtop25.com',
            ),
            'earphoriareviews.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_968_',
                'SITE_URL' => 'http://www.earphoriareviews.com',
                'DOMAIN' => 'earphoriareviews.com',
            ),
            'bct878.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_969_',
                'SITE_URL' => 'http://www.bct878.com',
                'DOMAIN' => 'bct878.com',
            ),
            'latitude33beer.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_970_',
                'SITE_URL' => 'http://www.latitude33beer.com',
                'DOMAIN' => 'latitude33beer.com',
            ),
            'sdultimate.org' =>
            array(
                'TABLE_SCHEMA' => 'beesknee_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1491_',
                'SITE_URL' => 'http://www.sdultimate.org',
                'DOMAIN' => 'sdultimate.org',
            ),
            'israelart.me' =>
            array(
                'TABLE_SCHEMA' => 'beesknee_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_217_',
                'SITE_URL' => 'http://www.israelart.me',
                'DOMAIN' => 'israelart.me',
            ),
            'janectimm.com' =>
            array(
                'TABLE_SCHEMA' => 'beesknee_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_218_',
                'SITE_URL' => 'http://www.janectimm.com',
                'DOMAIN' => 'janectimm.com',
            ),
            'jovon2johnson.com' =>
            array(
                'TABLE_SCHEMA' => 'beesknee_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_227_',
                'SITE_URL' => 'http://jovon2johnson.com',
                'DOMAIN' => 'jovon2johnson.com',
            ),
            'overlymanufacturing.com' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1492_',
                'SITE_URL' => 'http://www.overlymanufacturing.com',
                'DOMAIN' => 'overlymanufacturing.com',
            ),
            'gobflo.org' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_170_',
                'SITE_URL' => 'http://www.gobflo.org',
                'DOMAIN' => 'gobflo.org',
            ),
            'hmelevskih.com' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_208_',
                'SITE_URL' => 'http://www.hmelevskih.com',
                'DOMAIN' => 'hmelevskih.com',
            ),
            'gruposocmedia.mobi' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_221_',
                'SITE_URL' => 'http://www.gruposocmedia.mobi',
                'DOMAIN' => 'gruposocmedia.mobi',
            ),
            'hotklip.info' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_223_',
                'SITE_URL' => 'http://www.hotklip.info',
                'DOMAIN' => 'hotklip.info',
            ),
            'in-asia.net' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_222_',
                'SITE_URL' => 'http://www.in-asia.net',
                'DOMAIN' => 'in-asia.net',
            ),
            'inovmakers.com' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_224_',
                'SITE_URL' => 'http://www.inovmakers.com',
                'DOMAIN' => 'inovmakers.com',
            ),
            'ipraybcp.com' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_225_',
                'SITE_URL' => 'http://www.ipraybcp.com',
                'DOMAIN' => 'ipraybcp.com',
            ),
            'irstaxattorneys.org' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_226_',
                'SITE_URL' => 'http://www.irstaxattorneys.org',
                'DOMAIN' => 'irstaxattorneys.org',
            ),
            'amargosasolar.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1493_',
                'SITE_URL' => 'http://www.amargosasolar.com',
                'DOMAIN' => 'amargosasolar.com',
            ),
            'free-erotic-gay-sex-stories.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_212_',
                'SITE_URL' => 'http://free-erotic-gay-sex-stories.com',
                'DOMAIN' => 'free-erotic-gay-sex-stories.com',
            ),
            'freshberryapp.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_213_',
                'SITE_URL' => 'http://www.freshberryapp.com',
                'DOMAIN' => 'freshberryapp.com',
            ),
            'gaochundy.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_214_',
                'SITE_URL' => 'http://www.gaochundy.com',
                'DOMAIN' => 'gaochundy.com',
            ),
            'getrealnhlvip.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_215_',
                'SITE_URL' => 'http://www.getrealnhlvip.com',
                'DOMAIN' => 'getrealnhlvip.com',
            ),
            'ggmap1085.info' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_216_',
                'SITE_URL' => 'http://www.ggmap1085.info',
                'DOMAIN' => 'ggmap1085.info',
            ),
            'myhappyweight.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_250_',
                'SITE_URL' => 'http://myhappyweight.com',
                'DOMAIN' => 'myhappyweight.com',
            ),
            'myprogressivealliance.org' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_251_',
                'SITE_URL' => 'http://myprogressivealliance.org',
                'DOMAIN' => 'myprogressivealliance.org',
            ),
            'nashvilletspa.org' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_253_',
                'SITE_URL' => 'http://nashvilletspa.org',
                'DOMAIN' => 'nashvilletspa.org',
            ),
            'mail.nbasalaries.net' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_427_',
                'SITE_URL' => 'http://mail.nbasalaries.net',
                'DOMAIN' => 'mail.nbasalaries.net',
            ),
            'mediaandpeople.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_196_',
                'SITE_URL' => 'http://www.mediaandpeople.com',
                'DOMAIN' => 'mediaandpeople.com',
            ),
            'emmetttillblog.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_197_',
                'SITE_URL' => 'http://www.emmetttillblog.com',
                'DOMAIN' => 'emmetttillblog.com',
            ),
            'meatissues.org' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_206_',
                'SITE_URL' => 'http://www.meatissues.org',
                'DOMAIN' => 'meatissues.org',
            ),
            'eurocenterdiursa.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_207_',
                'SITE_URL' => 'http://www.eurocenterdiursa.com',
                'DOMAIN' => 'eurocenterdiursa.com',
            ),
            'gen-engineers.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_209_',
                'SITE_URL' => 'http://www.gen-engineers.com',
                'DOMAIN' => 'gen-engineers.com',
            ),
            'yes2growth.com' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1494_',
                'SITE_URL' => 'http://www.yes2growth.com',
                'DOMAIN' => 'yes2growth.com',
            ),
            'kettlebellworkoutsx.com' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_184_',
                'SITE_URL' => 'http://www.kettlebellworkoutsx.com',
                'DOMAIN' => 'kettlebellworkoutsx.com',
            ),
            'farmtablecatering.com' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_188_',
                'SITE_URL' => 'http://www.farmtablecatering.com',
                'DOMAIN' => 'farmtablecatering.com',
            ),
            'ebsresumeservice.com' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_200_',
                'SITE_URL' => 'http://www.ebsresumeservice.com',
                'DOMAIN' => 'ebsresumeservice.com',
            ),
            'fairfieldctrealestate.net' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_219_',
                'SITE_URL' => 'http://www.fairfieldctrealestate.net',
                'DOMAIN' => 'fairfieldctrealestate.net',
            ),
            'tmcfootball.com' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1333_',
                'SITE_URL' => 'http://www.tmcfootball.com',
                'DOMAIN' => 'tmcfootball.com',
            ),
            'towniesfootball.org' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1334_',
                'SITE_URL' => 'http://www.towniesfootball.org',
                'DOMAIN' => 'towniesfootball.org',
            ),
            'webdesign359.info' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1335_',
                'SITE_URL' => 'http://www.webdesign359.info',
                'DOMAIN' => 'webdesign359.info',
            ),
            'winterspringsfloristweddings.com' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1336_',
                'SITE_URL' => 'http://www.winterspringsfloristweddings.com',
                'DOMAIN' => 'winterspringsfloristweddings.com',
            ),
            'live-chic.com' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1495_',
                'SITE_URL' => 'http://www.live-chic.com',
                'DOMAIN' => 'live-chic.com',
            ),
            'biencomun-peru.org' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_976_',
                'SITE_URL' => 'http://www.biencomun-peru.org',
                'DOMAIN' => 'biencomun-peru.org',
            ),
            'paraguaysoberano.org' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_977_',
                'SITE_URL' => 'http://www.paraguaysoberano.org',
                'DOMAIN' => 'paraguaysoberano.org',
            ),
            'summitautismacademy.org' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_978_',
                'SITE_URL' => 'http://www.summitautismacademy.org',
                'DOMAIN' => 'summitautismacademy.org',
            ),
            'k8mall.com' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_979_',
                'SITE_URL' => 'http://www.k8mall.com',
                'DOMAIN' => 'k8mall.com',
            ),
            'wallstreetchinese.org' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_980_',
                'SITE_URL' => 'http://www.wallstreetchinese.org',
                'DOMAIN' => 'wallstreetchinese.org',
            ),
            'inchocolates.net' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1218_',
                'SITE_URL' => 'http://www.inchocolates.net',
                'DOMAIN' => 'inchocolates.net',
            ),
            'kushtushorganics.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1219_',
                'SITE_URL' => 'http://www.kushtushorganics.com',
                'DOMAIN' => 'kushtushorganics.com',
            ),
            'lapetanque.org' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1220_',
                'SITE_URL' => 'http://www.lapetanque.org',
                'DOMAIN' => 'lapetanque.org',
            ),
            'artspiritsilks.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1221_',
                'SITE_URL' => 'http://www.artspiritsilks.com',
                'DOMAIN' => 'artspiritsilks.com',
            ),
            'mediacionpr.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1496_',
                'SITE_URL' => 'http://www.mediacionpr.com',
                'DOMAIN' => 'mediacionpr.com',
            ),
            'isawhow.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_827_',
                'SITE_URL' => 'http://www.isawhow.com',
                'DOMAIN' => 'isawhow.com',
            ),
            'princess-savanna.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_828_',
                'SITE_URL' => 'http://www.princess-savanna.com',
                'DOMAIN' => 'princess-savanna.com',
            ),
            'inter-track.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_829_',
                'SITE_URL' => 'http://www.inter-track.com',
                'DOMAIN' => 'inter-track.com',
            ),
            'bishopartsblog.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_830_',
                'SITE_URL' => 'http://www.bishopartsblog.com',
                'DOMAIN' => 'bishopartsblog.com',
            ),
            'helenlee.info' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_831_',
                'SITE_URL' => 'http://www.helenlee.info',
                'DOMAIN' => 'helenlee.info',
            ),
            'dmtimes.com' =>
            array(
                'TABLE_SCHEMA' => 'bnoma_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_206_',
                'SITE_URL' => 'http://dmtimes.com',
                'DOMAIN' => 'dmtimes.com',
            ),
            'jolivettes-sacresdufolklore.com' =>
            array(
                'TABLE_SCHEMA' => 'bnoma_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1653_',
                'SITE_URL' => 'http://www.jolivettes-sacresdufolklore.com',
                'DOMAIN' => 'jolivettes-sacresdufolklore.com',
            ),
            'tzipilitvak.com' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1668_',
                'SITE_URL' => 'http://www.tzipilitvak.com',
                'DOMAIN' => 'tzipilitvak.com',
            ),
            'kolumnen.me' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_202_',
                'SITE_URL' => 'http://www.kolumnen.me',
                'DOMAIN' => 'kolumnen.me',
            ),
            'llctours.com' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_203_',
                'SITE_URL' => 'http://www.llctours.com',
                'DOMAIN' => 'llctours.com',
            ),
            'marycanconetani.com' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_205_',
                'SITE_URL' => 'http://www.marycanconetani.com',
                'DOMAIN' => 'marycanconetani.com',
            ),
            'luteapress.com' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_211_',
                'SITE_URL' => 'http://www.luteapress.com',
                'DOMAIN' => 'luteapress.com',
            ),
            'artwestandbeyond.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1497_',
                'SITE_URL' => 'http://www.artwestandbeyond.com',
                'DOMAIN' => 'artwestandbeyond.com',
            ),
            'josemarroyo.org' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_176_',
                'SITE_URL' => 'http://www.josemarroyo.org',
                'DOMAIN' => 'josemarroyo.org',
            ),
            'kabbalahvictims.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_191_',
                'SITE_URL' => 'http://www.kabbalahvictims.com',
                'DOMAIN' => 'kabbalahvictims.com',
            ),
            'jasontschoppdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_194_',
                'SITE_URL' => 'http://www.jasontschoppdesign.com',
                'DOMAIN' => 'jasontschoppdesign.com',
            ),
            'justtryingtobebetter.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_199_',
                'SITE_URL' => 'http://www.justtryingtobebetter.com',
                'DOMAIN' => 'justtryingtobebetter.com',
            ),
            'keithpricegallery.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_201_',
                'SITE_URL' => 'http://www.keithpricegallery.com',
                'DOMAIN' => 'keithpricegallery.com',
            ),
            'aspireteaching.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1498_',
                'SITE_URL' => 'http://www.aspireteaching.com',
                'DOMAIN' => 'aspireteaching.com',
            ),
            'harvarddebateinstitutes.org' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_181_',
                'SITE_URL' => 'http://www.harvarddebateinstitutes.org',
                'DOMAIN' => 'harvarddebateinstitutes.org',
            ),
            'idrisgoodwin.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_183_',
                'SITE_URL' => 'http://www.idrisgoodwin.com',
                'DOMAIN' => 'idrisgoodwin.com',
            ),
            'hairsalonsinsnellville.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_185_',
                'SITE_URL' => 'http://www.hairsalonsinsnellville.com',
                'DOMAIN' => 'hairsalonsinsnellville.com',
            ),
            'hbhdmw.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_189_',
                'SITE_URL' => 'http://www.hbhdmw.com',
                'DOMAIN' => 'hbhdmw.com',
            ),
            'hope9.mobi' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_198_',
                'SITE_URL' => 'http://www.hope9.mobi',
                'DOMAIN' => 'hope9.mobi',
            ),
            'northconwaysnowboard.com' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1499_',
                'SITE_URL' => 'http://www.northconwaysnowboard.com',
                'DOMAIN' => 'northconwaysnowboard.com',
            ),
            'igtf.info' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_182_',
                'SITE_URL' => 'http://www.igtf.info',
                'DOMAIN' => 'igtf.info',
            ),
            'iphoneslutz.com' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_190_',
                'SITE_URL' => 'http://www.iphoneslutz.com',
                'DOMAIN' => 'iphoneslutz.com',
            ),
            'indianaclubhouses.org' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_192_',
                'SITE_URL' => 'http://www.indianaclubhouses.org',
                'DOMAIN' => 'indianaclubhouses.org',
            ),
            'international-divorces.com' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_193_',
                'SITE_URL' => 'http://www.international-divorces.com',
                'DOMAIN' => 'international-divorces.com',
            ),
            'info-banca.info' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_195_',
                'SITE_URL' => 'http://www.info-banca.info',
                'DOMAIN' => 'info-banca.info',
            ),
            '1st-tech-support.com' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1170_',
                'SITE_URL' => 'http://www.1st-tech-support.com',
                'DOMAIN' => '1st-tech-support.com',
            ),
            '37th.org' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1171_',
                'SITE_URL' => 'http://www.37th.org',
                'DOMAIN' => '37th.org',
            ),
            'cancerservicesprogram.org' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1500_',
                'SITE_URL' => 'http://www.cancerservicesprogram.org',
                'DOMAIN' => 'cancerservicesprogram.org',
            ),
            'cadreintegremali.org' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_860_',
                'SITE_URL' => 'http://www.cadreintegremali.org',
                'DOMAIN' => 'cadreintegremali.org',
            ),
            'hypnotized-by-puffy.com' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_861_',
                'SITE_URL' => 'http://www.hypnotized-by-puffy.com',
                'DOMAIN' => 'hypnotized-by-puffy.com',
            ),
            'in-the-gutter.info' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_862_',
                'SITE_URL' => 'http://www.in-the-gutter.info',
                'DOMAIN' => 'in-the-gutter.info',
            ),
            'scottcountyartcrawl.com' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_863_',
                'SITE_URL' => 'http://www.scottcountyartcrawl.com',
                'DOMAIN' => 'scottcountyartcrawl.com',
            ),
            'chapiteaunews.net' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_864_',
                'SITE_URL' => 'http://www.chapiteaunews.net',
                'DOMAIN' => 'chapiteaunews.net',
            ),
            'paadstudy.org' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1502_',
                'SITE_URL' => 'http://www.paadstudy.org',
                'DOMAIN' => 'paadstudy.org',
            ),
            'guarbecque.net' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_171_',
                'SITE_URL' => 'http://www.guarbecque.net',
                'DOMAIN' => 'guarbecque.net',
            ),
            'hairsalonsinduluth.com' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_177_',
                'SITE_URL' => 'http://www.hairsalonsinduluth.com',
                'DOMAIN' => 'hairsalonsinduluth.com',
            ),
            'hairsalonsinlilburn.com' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_178_',
                'SITE_URL' => 'http://www.hairsalonsinlilburn.com',
                'DOMAIN' => 'hairsalonsinlilburn.com',
            ),
            'guolufengji.com' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_180_',
                'SITE_URL' => 'http://www.guolufengji.com',
                'DOMAIN' => 'guolufengji.com',
            ),
            'hairsalonsindunwoody.com' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_187_',
                'SITE_URL' => 'http://www.hairsalonsindunwoody.com',
                'DOMAIN' => 'hairsalonsindunwoody.com',
            ),
            'criaroutraescola.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1184_',
                'SITE_URL' => 'http://www.criaroutraescola.com',
                'DOMAIN' => 'criaroutraescola.com',
            ),
            'cornerstone-construction-remodeling.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1188_',
                'SITE_URL' => 'http://www.cornerstone-construction-remodeling.com',
                'DOMAIN' => 'cornerstone-construction-remodeling.com',
            ),
            'cramerperformance.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1190_',
                'SITE_URL' => 'http://www.cramerperformance.com',
                'DOMAIN' => 'cramerperformance.com',
            ),
            'premiorochedeperiodismo.org' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1501_',
                'SITE_URL' => 'http://www.premiorochedeperiodismo.org',
                'DOMAIN' => 'premiorochedeperiodismo.org',
            ),
            'causalive.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_890_',
                'SITE_URL' => 'http://www.causalive.com',
                'DOMAIN' => 'causalive.com',
            ),
            'crestorinjuryattorneys.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_891_',
                'SITE_URL' => 'http://www.crestorinjuryattorneys.com',
                'DOMAIN' => 'crestorinjuryattorneys.com',
            ),
            'csfmanagement.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_892_',
                'SITE_URL' => 'http://www.csfmanagement.com',
                'DOMAIN' => 'csfmanagement.com',
            ),
            'customcateringbyplum.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_893_',
                'SITE_URL' => 'http://www.customcateringbyplum.com',
                'DOMAIN' => 'customcateringbyplum.com',
            ),
            'allisonveltz.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_894_',
                'SITE_URL' => 'http://www.allisonveltz.com',
                'DOMAIN' => 'allisonveltz.com',
            ),
            'ns2.cdcwilmingtonde.com' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_190_',
                'SITE_URL' => 'http://ns2.cdcwilmingtonde.com',
                'DOMAIN' => 'ns2.cdcwilmingtonde.com',
            ),
            'azurebestpractices.net' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_191_',
                'SITE_URL' => 'http://www.azurebestpractices.net',
                'DOMAIN' => 'azurebestpractices.net',
            ),
            'azurecomponents.net' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_192_',
                'SITE_URL' => 'http://azurecomponents.net',
                'DOMAIN' => 'azurecomponents.net',
            ),
            'azureprogramming.net' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_193_',
                'SITE_URL' => 'http://azureprogramming.net',
                'DOMAIN' => 'azureprogramming.net',
            ),
            'bjgsu.com' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1654_',
                'SITE_URL' => 'http://www.bjgsu.com',
                'DOMAIN' => 'bjgsu.com',
            ),
            'kimballpartnergroup.com' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1682_',
                'SITE_URL' => 'http://www.kimballpartnergroup.com',
                'DOMAIN' => 'kimballpartnergroup.com',
            ),
            'powertoolsinfo.com' =>
            array(
                'TABLE_SCHEMA' => 'celebite_tkp',
                'TABLE_PREFIX' => 'wp_pkp1_1472_',
                'SITE_URL' => 'http://www.powertoolsinfo.com',
                'DOMAIN' => 'powertoolsinfo.com',
            ),
            'zunepower.com' =>
            array(
                'TABLE_SCHEMA' => 'celebite_tkp',
                'TABLE_PREFIX' => 'wp_pkp1_1473_',
                'SITE_URL' => 'http://www.zunepower.com',
                'DOMAIN' => 'zunepower.com',
            ),
            'ncttechnologies.com' =>
            array(
                'TABLE_SCHEMA' => 'celebite_tkp',
                'TABLE_PREFIX' => 'wp_pkp1_1474_',
                'SITE_URL' => 'http://www.ncttechnologies.com',
                'DOMAIN' => 'ncttechnologies.com',
            ),
            'ispotles.com' =>
            array(
                'TABLE_SCHEMA' => 'celebite_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1655_',
                'SITE_URL' => 'http://www.ispotles.com',
                'DOMAIN' => 'ispotles.com',
            ),
            'dflongfa.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1503_',
                'SITE_URL' => 'http://www.dflongfa.com',
                'DOMAIN' => 'dflongfa.com',
            ),
            'gardnernewsnow.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_172_',
                'SITE_URL' => 'http://www.gardnernewsnow.com',
                'DOMAIN' => 'gardnernewsnow.com',
            ),
            'galvestonpiratesc.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_173_',
                'SITE_URL' => 'http://www.galvestonpiratesc.com',
                'DOMAIN' => 'galvestonpiratesc.com',
            ),
            'glenscottallen.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_174_',
                'SITE_URL' => 'http://www.glenscottallen.com',
                'DOMAIN' => 'glenscottallen.com',
            ),
            'gohawaii-activities.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_175_',
                'SITE_URL' => 'http://www.gohawaii-activities.com',
                'DOMAIN' => 'gohawaii-activities.com',
            ),
            'lordgorevail.com' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1504_',
                'SITE_URL' => 'http://www.lordgorevail.com',
                'DOMAIN' => 'lordgorevail.com',
            ),
            'energee.me' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_150_',
                'SITE_URL' => 'http://www.energee.me',
                'DOMAIN' => 'energee.me',
            ),
            'ericvick.com' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_167_',
                'SITE_URL' => 'http://www.ericvick.com',
                'DOMAIN' => 'ericvick.com',
            ),
            'forensicanth.com' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_168_',
                'SITE_URL' => 'http://www.forensicanth.com',
                'DOMAIN' => 'forensicanth.com',
            ),
            'fuckluxury.com' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_169_',
                'SITE_URL' => 'http://www.fuckluxury.com',
                'DOMAIN' => 'fuckluxury.com',
            ),
            'jerseysmall2012.us' =>
            array(
                'TABLE_SCHEMA' => 'cicsearc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1657_',
                'SITE_URL' => 'http://www.jerseysmall2012.us',
                'DOMAIN' => 'jerseysmall2012.us',
            ),
            'plowboymansion.com' =>
            array(
                'TABLE_SCHEMA' => 'cicsearc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1680_',
                'SITE_URL' => 'http://www.plowboymansion.com',
                'DOMAIN' => 'plowboymansion.com',
            ),
            'tppdallas.org' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_11_',
                'SITE_URL' => 'http://www.tppdallas.org',
                'DOMAIN' => 'tppdallas.org',
            ),
            'awsboard.com' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_12_',
                'SITE_URL' => 'http://www.awsboard.com',
                'DOMAIN' => 'awsboard.com',
            ),
            'mtadamsacademy.com' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_13_',
                'SITE_URL' => 'http://www.mtadamsacademy.com',
                'DOMAIN' => 'mtadamsacademy.com',
            ),
            'wildwoodcrestmotelbeaurivage.info' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_14_',
                'SITE_URL' => 'http://www.wildwoodcrestmotelbeaurivage.info',
                'DOMAIN' => 'wildwoodcrestmotelbeaurivage.info',
            ),
            'deweysamericangrill.com' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_15_',
                'SITE_URL' => 'http://www.deweysamericangrill.com',
                'DOMAIN' => 'deweysamericangrill.com',
            ),
            'sokerhanem.com' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1656_',
                'SITE_URL' => 'http://www.sokerhanem.com',
                'DOMAIN' => 'sokerhanem.com',
            ),
            'dogualmanya.info' =>
            array(
                'TABLE_SCHEMA' => 'cleaning_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_141_',
                'SITE_URL' => 'http://www.dogualmanya.info',
                'DOMAIN' => 'dogualmanya.info',
            ),
            'djantoinequa.com' =>
            array(
                'TABLE_SCHEMA' => 'cleaning_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_147_',
                'SITE_URL' => 'http://www.djantoinequa.com',
                'DOMAIN' => 'djantoinequa.com',
            ),
            'nlscanada.com' =>
            array(
                'TABLE_SCHEMA' => 'cleaning_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1505_',
                'SITE_URL' => 'http://www.nlscanada.com',
                'DOMAIN' => 'nlscanada.com',
            ),
            'e-lat.org' =>
            array(
                'TABLE_SCHEMA' => 'cleaning_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_157_',
                'SITE_URL' => 'http://www.e-lat.org',
                'DOMAIN' => 'e-lat.org',
            ),
            'emu-solutions.com' =>
            array(
                'TABLE_SCHEMA' => 'cleaning_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_158_',
                'SITE_URL' => 'http://www.emu-solutions.com',
                'DOMAIN' => 'emu-solutions.com',
            ),
            'eastafricametaproject.org' =>
            array(
                'TABLE_SCHEMA' => 'cleaning_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_160_',
                'SITE_URL' => 'http://www.eastafricametaproject.org',
                'DOMAIN' => 'eastafricametaproject.org',
            ),
            'detroitpistonsjerseys.info' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_144_',
                'SITE_URL' => 'http://www.detroitpistonsjerseys.info',
                'DOMAIN' => 'detroitpistonsjerseys.info',
            ),
            'chicagopilatescollective.com' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1506_',
                'SITE_URL' => 'http://www.chicagopilatescollective.com',
                'DOMAIN' => 'chicagopilatescollective.com',
            ),
            'daemoniq.org' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_154_',
                'SITE_URL' => 'http://www.daemoniq.org',
                'DOMAIN' => 'daemoniq.org',
            ),
            'digitalharvestdomains.com' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_155_',
                'SITE_URL' => 'http://www.digitalharvestdomains.com',
                'DOMAIN' => 'digitalharvestdomains.com',
            ),
            'discoverkalamazoo.org' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_156_',
                'SITE_URL' => 'http://www.discoverkalamazoo.org',
                'DOMAIN' => 'discoverkalamazoo.org',
            ),
            'diamondselectonline.com' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_165_',
                'SITE_URL' => 'http://www.diamondselectonline.com',
                'DOMAIN' => 'diamondselectonline.com',
            ),
            '95zqw.com' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1507_',
                'SITE_URL' => 'http://www.95zqw.com',
                'DOMAIN' => '95zqw.com',
            ),
            'clintondalecommunitytheater.org' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_151_',
                'SITE_URL' => 'http://www.clintondalecommunitytheater.org',
                'DOMAIN' => 'clintondalecommunitytheater.org',
            ),
            'conect2012.org' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_152_',
                'SITE_URL' => 'http://www.conect2012.org',
                'DOMAIN' => 'conect2012.org',
            ),
            'coopamare.com' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_153_',
                'SITE_URL' => 'http://www.coopamare.com',
                'DOMAIN' => 'coopamare.com',
            ),
            'coamg.org' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_159_',
                'SITE_URL' => 'http://www.coamg.org',
                'DOMAIN' => 'coamg.org',
            ),
            'cropdusterband.com' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_164_',
                'SITE_URL' => 'http://www.cropdusterband.com',
                'DOMAIN' => 'cropdusterband.com',
            ),
            'calfirelookouts.org' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_138_',
                'SITE_URL' => 'http://www.calfirelookouts.org',
                'DOMAIN' => 'calfirelookouts.org',
            ),
            'chinarainbowaward.org' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_143_',
                'SITE_URL' => 'http://www.chinarainbowaward.org',
                'DOMAIN' => 'chinarainbowaward.org',
            ),
            'celticcoin.com' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_148_',
                'SITE_URL' => 'http://www.celticcoin.com',
                'DOMAIN' => 'celticcoin.com',
            ),
            'chinascienceandbelief.org' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_149_',
                'SITE_URL' => 'http://www.chinascienceandbelief.org',
                'DOMAIN' => 'chinascienceandbelief.org',
            ),
            'ciaochao.me' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_163_',
                'SITE_URL' => 'http://www.ciaochao.me',
                'DOMAIN' => 'ciaochao.me',
            ),
            'tylerspitehouse.com' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1670_',
                'SITE_URL' => 'http://www.tylerspitehouse.com',
                'DOMAIN' => 'tylerspitehouse.com',
            ),
            '3peasartlounge.com' =>
            array(
                'TABLE_SCHEMA' => 'coffeeho_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_136_',
                'SITE_URL' => 'http://www.3peasartlounge.com',
                'DOMAIN' => '3peasartlounge.com',
            ),
            '90n.us' =>
            array(
                'TABLE_SCHEMA' => 'coffeeho_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_145_',
                'SITE_URL' => 'http://www.90n.us',
                'DOMAIN' => '90n.us',
            ),
            'buffalo-mountain-ranch.com' =>
            array(
                'TABLE_SCHEMA' => 'coffeeho_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1508_',
                'SITE_URL' => 'http://www.buffalo-mountain-ranch.com',
                'DOMAIN' => 'buffalo-mountain-ranch.com',
            ),
            '515mag.net' =>
            array(
                'TABLE_SCHEMA' => 'coffeeho_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_98_',
                'SITE_URL' => 'http://www.515mag.net',
                'DOMAIN' => '515mag.net',
            ),
            'attigotherapy.com' =>
            array(
                'TABLE_SCHEMA' => 'cold-dea_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_137_',
                'SITE_URL' => 'http://www.attigotherapy.com',
                'DOMAIN' => 'attigotherapy.com',
            ),
            'bruiserbox.com' =>
            array(
                'TABLE_SCHEMA' => 'cold-dea_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_140_',
                'SITE_URL' => 'http://www.bruiserbox.com',
                'DOMAIN' => 'bruiserbox.com',
            ),
            'butterflies-n-birds.com' =>
            array(
                'TABLE_SCHEMA' => 'cold-dea_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_142_',
                'SITE_URL' => 'http://www.butterflies-n-birds.com',
                'DOMAIN' => 'butterflies-n-birds.com',
            ),
            'accordcounseling.info' =>
            array(
                'TABLE_SCHEMA' => 'cold-dea_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_146_',
                'SITE_URL' => 'http://www.accordcounseling.info',
                'DOMAIN' => 'accordcounseling.info',
            ),
            'drugalcoholaddictionwebsite.com' =>
            array(
                'TABLE_SCHEMA' => 'cold-dea_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1509_',
                'SITE_URL' => 'http://www.drugalcoholaddictionwebsite.com',
                'DOMAIN' => 'drugalcoholaddictionwebsite.com',
            ),
            'yardandgarden.us' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1337_',
                'SITE_URL' => 'http://www.yardandgarden.us',
                'DOMAIN' => 'yardandgarden.us',
            ),
            'caution-twins-at-play.com' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1510_',
                'SITE_URL' => 'http://www.caution-twins-at-play.com',
                'DOMAIN' => 'caution-twins-at-play.com',
            ),
            'coldstudio.us' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_981_',
                'SITE_URL' => 'http://www.coldstudio.us',
                'DOMAIN' => 'coldstudio.us',
            ),
            'lorenamonsalve.com' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_982_',
                'SITE_URL' => 'http://www.lorenamonsalve.com',
                'DOMAIN' => 'lorenamonsalve.com',
            ),
            'massagem-tantrica.com' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_983_',
                'SITE_URL' => 'http://www.massagem-tantrica.com',
                'DOMAIN' => 'massagem-tantrica.com',
            ),
            'neighborbeenyc.com' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_984_',
                'SITE_URL' => 'http://www.neighborbeenyc.com',
                'DOMAIN' => 'neighborbeenyc.com',
            ),
            'montibellomakeover.com' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_985_',
                'SITE_URL' => 'http://www.montibellomakeover.com',
                'DOMAIN' => 'montibellomakeover.com',
            ),
            'africanplanit.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_116_',
                'SITE_URL' => 'http://www.africanplanit.com',
                'DOMAIN' => 'africanplanit.com',
            ),
            '1710dodge.info' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_117_',
                'SITE_URL' => 'http://www.1710dodge.info',
                'DOMAIN' => '1710dodge.info',
            ),
            'chicago-real-estate.org' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_132_',
                'SITE_URL' => 'http://www.chicago-real-estate.org',
                'DOMAIN' => 'chicago-real-estate.org',
            ),
            'curiositysavedthehuman.info' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_133_',
                'SITE_URL' => 'http://www.curiositysavedthehuman.info',
                'DOMAIN' => 'curiositysavedthehuman.info',
            ),
            '0731lady.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_134_',
                'SITE_URL' => 'http://www.0731lady.com',
                'DOMAIN' => '0731lady.com',
            ),
            'buyfreerunonline.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1511_',
                'SITE_URL' => 'http://www.buyfreerunonline.com',
                'DOMAIN' => 'buyfreerunonline.com',
            ),
            'dezlab.us' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_115_',
                'SITE_URL' => 'http://www.dezlab.us',
                'DOMAIN' => 'dezlab.us',
            ),
            'cowtownart.com' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_129_',
                'SITE_URL' => 'http://www.cowtownart.com',
                'DOMAIN' => 'cowtownart.com',
            ),
            'daretoloveprojecthaiti.org' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_130_',
                'SITE_URL' => 'http://www.daretoloveprojecthaiti.org',
                'DOMAIN' => 'daretoloveprojecthaiti.org',
            ),
            'aldahlawi.net' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_131_',
                'SITE_URL' => 'http://www.aldahlawi.net',
                'DOMAIN' => 'aldahlawi.net',
            ),
            'losangelesgreenpages.com' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1512_',
                'SITE_URL' => 'http://www.losangelesgreenpages.com',
                'DOMAIN' => 'losangelesgreenpages.com',
            ),
            'cps3lourdeshospital.org' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_121_',
                'SITE_URL' => 'http://www.cps3lourdeshospital.org',
                'DOMAIN' => 'cps3lourdeshospital.org',
            ),
            'cheapfoodsaver.us' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_125_',
                'SITE_URL' => 'http://www.cheapfoodsaver.us',
                'DOMAIN' => 'cheapfoodsaver.us',
            ),
            'cursotenis.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_126_',
                'SITE_URL' => 'http://www.cursotenis.com',
                'DOMAIN' => 'cursotenis.com',
            ),
            'disbennettonline.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_127_',
                'SITE_URL' => 'http://www.disbennettonline.com',
                'DOMAIN' => 'disbennettonline.com',
            ),
            'diabloofftherecord.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1461_',
                'SITE_URL' => 'http://www.diabloofftherecord.com',
                'DOMAIN' => 'diabloofftherecord.com',
            ),
            'marketingstrategyinsights.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1513_',
                'SITE_URL' => 'http://www.marketingstrategyinsights.com',
                'DOMAIN' => 'marketingstrategyinsights.com',
            ),
            'painfreeinfo.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1317_',
                'SITE_URL' => 'http://www.painfreeinfo.com',
                'DOMAIN' => 'painfreeinfo.com',
            ),
            'pregnantaftermiscarriage.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1318_',
                'SITE_URL' => 'http://www.pregnantaftermiscarriage.com',
                'DOMAIN' => 'pregnantaftermiscarriage.com',
            ),
            'rehabercise.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1319_',
                'SITE_URL' => 'http://www.rehabercise.com',
                'DOMAIN' => 'rehabercise.com',
            ),
            'promisepreschool.org' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1320_',
                'SITE_URL' => 'http://www.promisepreschool.org',
                'DOMAIN' => 'promisepreschool.org',
            ),
            'santacruzboardsports.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1321_',
                'SITE_URL' => 'http://www.santacruzboardsports.com',
                'DOMAIN' => 'santacruzboardsports.com',
            ),
            'brandstrategyinsights.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1516_',
                'SITE_URL' => 'http://www.brandstrategyinsights.com',
                'DOMAIN' => 'brandstrategyinsights.com',
            ),
            'credit-turbo.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_959_',
                'SITE_URL' => 'http://www.credit-turbo.com',
                'DOMAIN' => 'credit-turbo.com',
            ),
            'wirelessgalicia.org' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_960_',
                'SITE_URL' => 'http://www.wirelessgalicia.org',
                'DOMAIN' => 'wirelessgalicia.org',
            ),
            'cyclr.me' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_961_',
                'SITE_URL' => 'http://www.cyclr.me',
                'DOMAIN' => 'cyclr.me',
            ),
            'buildinghealthycommunities2011.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_962_',
                'SITE_URL' => 'http://www.buildinghealthycommunities2011.com',
                'DOMAIN' => 'buildinghealthycommunities2011.com',
            ),
            'pumpitupcyfair.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_963_',
                'SITE_URL' => 'http://www.pumpitupcyfair.com',
                'DOMAIN' => 'pumpitupcyfair.com',
            ),
            'amtraktrainsportationvideocontest.com' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_103_',
                'SITE_URL' => 'http://www.amtraktrainsportationvideocontest.com',
                'DOMAIN' => 'amtraktrainsportationvideocontest.com',
            ),
            'availableroomsjungfrau.com' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_113_',
                'SITE_URL' => 'http://www.availableroomsjungfrau.com',
                'DOMAIN' => 'availableroomsjungfrau.com',
            ),
            'ad2westmichigan.org' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_122_',
                'SITE_URL' => 'http://www.ad2westmichigan.org',
                'DOMAIN' => 'ad2westmichigan.org',
            ),
            'dudewheresmycar.me' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_128_',
                'SITE_URL' => 'http://www.dudewheresmycar.me',
                'DOMAIN' => 'dudewheresmycar.me',
            ),
            'camels-sale.com' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_139_',
                'SITE_URL' => 'http://www.camels-sale.com',
                'DOMAIN' => 'camels-sale.com',
            ),
            'low-carbon-products.com' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1514_',
                'SITE_URL' => 'http://www.low-carbon-products.com',
                'DOMAIN' => 'low-carbon-products.com',
            ),
            'altavoces.info' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_108_',
                'SITE_URL' => 'http://www.altavoces.info',
                'DOMAIN' => 'altavoces.info',
            ),
            'arizonansforabrighterfuture.com' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_118_',
                'SITE_URL' => 'http://www.arizonansforabrighterfuture.com',
                'DOMAIN' => 'arizonansforabrighterfuture.com',
            ),
            '91ddy.com' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_123_',
                'SITE_URL' => 'http://www.91ddy.com',
                'DOMAIN' => '91ddy.com',
            ),
            'abovegroundcontainment.net' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_124_',
                'SITE_URL' => 'http://www.abovegroundcontainment.net',
                'DOMAIN' => 'abovegroundcontainment.net',
            ),
            'achmmpdx.org' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_135_',
                'SITE_URL' => 'http://www.achmmpdx.org',
                'DOMAIN' => 'achmmpdx.org',
            ),
            'girls-schools.us' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1515_',
                'SITE_URL' => 'http://www.girls-schools.us',
                'DOMAIN' => 'girls-schools.us',
            ),
            'njspokes.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_68_',
                'SITE_URL' => 'http://www.njspokes.com',
                'DOMAIN' => 'njspokes.com',
            ),
            'qualitysplitac.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_83_',
                'SITE_URL' => 'http://www.qualitysplitac.com',
                'DOMAIN' => 'qualitysplitac.com',
            ),
            'cheaperchevy.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_109_',
                'SITE_URL' => 'http://www.cheaperchevy.com',
                'DOMAIN' => 'cheaperchevy.com',
            ),
            'bpcavt.org' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_119_',
                'SITE_URL' => 'http://www.bpcavt.org',
                'DOMAIN' => 'bpcavt.org',
            ),
            'canoncitycolorado.org' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_120_',
                'SITE_URL' => 'http://www.canoncitycolorado.org',
                'DOMAIN' => 'canoncitycolorado.org',
            ),
            'charpo-alberta.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1460_',
                'SITE_URL' => 'http://www.charpo-alberta.com',
                'DOMAIN' => 'charpo-alberta.com',
            ),
            'hustleup.net' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1517_',
                'SITE_URL' => 'http://www.hustleup.net',
                'DOMAIN' => 'hustleup.net',
            ),
            'ccp2010.org' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_162_',
                'SITE_URL' => 'http://www.ccp2010.org',
                'DOMAIN' => 'ccp2010.org',
            ),
            '0356bentu.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_27_',
                'SITE_URL' => 'http://www.0356bentu.com',
                'DOMAIN' => '0356bentu.com',
            ),
            'ttcradio.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_28_',
                'SITE_URL' => 'http://www.ttcradio.com',
                'DOMAIN' => 'ttcradio.com',
            ),
            'meipimatic.org' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_29_',
                'SITE_URL' => 'http://www.meipimatic.org',
                'DOMAIN' => 'meipimatic.org',
            ),
            'statefairproject.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_30_',
                'SITE_URL' => 'http://www.statefairproject.com',
                'DOMAIN' => 'statefairproject.com',
            ),
            'neogenix.us' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_31_',
                'SITE_URL' => 'http://www.neogenix.us',
                'DOMAIN' => 'neogenix.us',
            ),
            'thenewtrover.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_32_',
                'SITE_URL' => 'http://www.thenewtrover.com',
                'DOMAIN' => 'thenewtrover.com',
            ),
            'thecurrentsband.info' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_104_',
                'SITE_URL' => 'http://www.thecurrentsband.info',
                'DOMAIN' => 'thecurrentsband.info',
            ),
            'smartpocket.info' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_105_',
                'SITE_URL' => 'http://www.smartpocket.info',
                'DOMAIN' => 'smartpocket.info',
            ),
            'stsamuel.org' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_106_',
                'SITE_URL' => 'http://www.stsamuel.org',
                'DOMAIN' => 'stsamuel.org',
            ),
            'manualtherapyresearch.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1518_',
                'SITE_URL' => 'http://www.manualtherapyresearch.com',
                'DOMAIN' => 'manualtherapyresearch.com',
            ),
            'renhuan.org' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_90_',
                'SITE_URL' => 'http://www.renhuan.org',
                'DOMAIN' => 'renhuan.org',
            ),
            'murgemusic.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_92_',
                'SITE_URL' => 'http://www.murgemusic.com',
                'DOMAIN' => 'murgemusic.com',
            ),
            'hgq168.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_101_',
                'SITE_URL' => 'http://www.hgq168.com',
                'DOMAIN' => 'hgq168.com',
            ),
            'upennifc.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_107_',
                'SITE_URL' => 'http://www.upennifc.com',
                'DOMAIN' => 'upennifc.com',
            ),
            'alejandrorivasfoto.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_110_',
                'SITE_URL' => 'http://www.alejandrorivasfoto.com',
                'DOMAIN' => 'alejandrorivasfoto.com',
            ),
            'womenathleticshoes.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1663_',
                'SITE_URL' => 'http://www.womenathleticshoes.com',
                'DOMAIN' => 'womenathleticshoes.com',
            ),
            'eternalunlimited.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_95_',
                'SITE_URL' => 'http://www.eternalunlimited.com',
                'DOMAIN' => 'eternalunlimited.com',
            ),
            'somalireal.net' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_178_',
                'SITE_URL' => 'http://somalireal.net',
                'DOMAIN' => 'somalireal.net',
            ),
            'szul.info' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_179_',
                'SITE_URL' => 'http://szul.info',
                'DOMAIN' => 'szul.info',
            ),
            'tiwayoilturkey.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_180_',
                'SITE_URL' => 'http://www.tiwayoilturkey.com',
                'DOMAIN' => 'tiwayoilturkey.com',
            ),
            'xptv.mobi' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_181_',
                'SITE_URL' => 'http://www.xptv.mobi',
                'DOMAIN' => 'xptv.mobi',
            ),
            'thefamilyplayground.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1441_',
                'SITE_URL' => 'http://www.thefamilyplayground.com',
                'DOMAIN' => 'thefamilyplayground.com',
            ),
            'autostry.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1442_',
                'SITE_URL' => 'http://www.autostry.com',
                'DOMAIN' => 'autostry.com',
            ),
            'ukghdscheap.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1443_',
                'SITE_URL' => 'http://www.ukghdscheap.com',
                'DOMAIN' => 'ukghdscheap.com',
            ),
            'europeanpitchpoint.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1444_',
                'SITE_URL' => 'http://www.europeanpitchpoint.com',
                'DOMAIN' => 'europeanpitchpoint.com',
            ),
            'deltafinance.biz' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1445_',
                'SITE_URL' => 'http://www.deltafinance.biz',
                'DOMAIN' => 'deltafinance.biz',
            ),
            'billiglouisvuittontaschende.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1519_',
                'SITE_URL' => 'http://www.billiglouisvuittontaschende.com',
                'DOMAIN' => 'billiglouisvuittontaschende.com',
            ),
            '321debtconsolidationnews.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_599_',
                'SITE_URL' => 'http://www.321debtconsolidationnews.com',
                'DOMAIN' => '321debtconsolidationnews.com',
            ),
            'hanamicron.net' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_655_',
                'SITE_URL' => 'http://www.hanamicron.net',
                'DOMAIN' => 'hanamicron.net',
            ),
            'eegga.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_656_',
                'SITE_URL' => 'http://www.eegga.com',
                'DOMAIN' => 'eegga.com',
            ),
            'knoxvillevoice.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_657_',
                'SITE_URL' => 'http://www.knoxvillevoice.com',
                'DOMAIN' => 'knoxvillevoice.com',
            ),
            'homedecorok.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_672_',
                'SITE_URL' => 'http://www.homedecorok.com',
                'DOMAIN' => 'homedecorok.com',
            ),
            'tutorialicious.info' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1436_',
                'SITE_URL' => 'http://www.tutorialicious.info',
                'DOMAIN' => 'tutorialicious.info',
            ),
            'allabout-weightloss.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1437_',
                'SITE_URL' => 'http://www.allabout-weightloss.com',
                'DOMAIN' => 'allabout-weightloss.com',
            ),
            'livecricketsite.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1438_',
                'SITE_URL' => 'http://www.livecricketsite.com',
                'DOMAIN' => 'livecricketsite.com',
            ),
            'healthy-body-exercises.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1439_',
                'SITE_URL' => 'http://www.healthy-body-exercises.com',
                'DOMAIN' => 'healthy-body-exercises.com',
            ),
            'western-light.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1440_',
                'SITE_URL' => 'http://www.western-light.com',
                'DOMAIN' => 'western-light.com',
            ),
            'howardcountymastergardeners.org' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1671_',
                'SITE_URL' => 'http://www.howardcountymastergardeners.org',
                'DOMAIN' => 'howardcountymastergardeners.org',
            ),
            'dli-generics.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_653_',
                'SITE_URL' => 'http://www.dli-generics.com',
                'DOMAIN' => 'dli-generics.com',
            ),
            'learningstylesuk.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_665_',
                'SITE_URL' => 'http://www.learningstylesuk.com',
                'DOMAIN' => 'learningstylesuk.com',
            ),
            'pittsburghalt.net' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_668_',
                'SITE_URL' => 'http://www.pittsburghalt.net',
                'DOMAIN' => 'pittsburghalt.net',
            ),
            'birthdaygift.us' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_670_',
                'SITE_URL' => 'http://www.birthdaygift.us',
                'DOMAIN' => 'birthdaygift.us',
            ),
            'plentyrestaurant.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_671_',
                'SITE_URL' => 'http://www.plentyrestaurant.com',
                'DOMAIN' => 'plentyrestaurant.com',
            ),
            'clementinescafe.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_717_',
                'SITE_URL' => 'http://www.clementinescafe.com',
                'DOMAIN' => 'clementinescafe.com',
            ),
            'adelekamp.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_718_',
                'SITE_URL' => 'http://www.adelekamp.com',
                'DOMAIN' => 'adelekamp.com',
            ),
            'emergency-response-systems.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_719_',
                'SITE_URL' => 'http://www.emergency-response-systems.com',
                'DOMAIN' => 'emergency-response-systems.com',
            ),
            'offshoreprimebrokerage.org' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_720_',
                'SITE_URL' => 'http://www.offshoreprimebrokerage.org',
                'DOMAIN' => 'offshoreprimebrokerage.org',
            ),
            'garyjonesbusinessenglish.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_721_',
                'SITE_URL' => 'http://www.garyjonesbusinessenglish.com',
                'DOMAIN' => 'garyjonesbusinessenglish.com',
            ),
            'cosmetic-enhancements.info' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1431_',
                'SITE_URL' => 'http://www.cosmetic-enhancements.info',
                'DOMAIN' => 'cosmetic-enhancements.info',
            ),
            'ballpitplus.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1432_',
                'SITE_URL' => 'http://www.ballpitplus.com',
                'DOMAIN' => 'ballpitplus.com',
            ),
            'heroineaddict.us' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1433_',
                'SITE_URL' => 'http://www.heroineaddict.us',
                'DOMAIN' => 'heroineaddict.us',
            ),
            'knightbikesandskateboards.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1434_',
                'SITE_URL' => 'http://www.knightbikesandskateboards.com',
                'DOMAIN' => 'knightbikesandskateboards.com',
            ),
            'outdoor-lighting-for-sale.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1435_',
                'SITE_URL' => 'http://www.outdoor-lighting-for-sale.com',
                'DOMAIN' => 'outdoor-lighting-for-sale.com',
            ),
            'miting.biz' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1520_',
                'SITE_URL' => 'http://www.miting.biz',
                'DOMAIN' => 'miting.biz',
            ),
            'lawnprognv.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_652_',
                'SITE_URL' => 'http://www.lawnprognv.com',
                'DOMAIN' => 'lawnprognv.com',
            ),
            'emaxmarketing.net' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_654_',
                'SITE_URL' => 'http://www.emaxmarketing.net',
                'DOMAIN' => 'emaxmarketing.net',
            ),
            'nymatlaw.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_659_',
                'SITE_URL' => 'http://www.nymatlaw.com',
                'DOMAIN' => 'nymatlaw.com',
            ),
            'teaguefarms.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_667_',
                'SITE_URL' => 'http://www.teaguefarms.com',
                'DOMAIN' => 'teaguefarms.com',
            ),
            'yourmotorizedworld.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_676_',
                'SITE_URL' => 'http://www.yourmotorizedworld.com',
                'DOMAIN' => 'yourmotorizedworld.com',
            ),
            'miraclemotorsports.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1307_',
                'SITE_URL' => 'http://www.miraclemotorsports.com',
                'DOMAIN' => 'miraclemotorsports.com',
            ),
            'morphisat.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1308_',
                'SITE_URL' => 'http://www.morphisat.com',
                'DOMAIN' => 'morphisat.com',
            ),
            'mypinkmotorcyclehelmets.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1309_',
                'SITE_URL' => 'http://www.mypinkmotorcyclehelmets.com',
                'DOMAIN' => 'mypinkmotorcyclehelmets.com',
            ),
            'neo-vitamin.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1310_',
                'SITE_URL' => 'http://www.neo-vitamin.com',
                'DOMAIN' => 'neo-vitamin.com',
            ),
            'nike06.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1527_',
                'SITE_URL' => 'http://www.nike06.com',
                'DOMAIN' => 'nike06.com',
            ),
            'duoyi.org' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_949_',
                'SITE_URL' => 'http://www.duoyi.org',
                'DOMAIN' => 'duoyi.org',
            ),
            'sosein-visualstory.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_951_',
                'SITE_URL' => 'http://www.sosein-visualstory.com',
                'DOMAIN' => 'sosein-visualstory.com',
            ),
            'rsu-ids2012.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_952_',
                'SITE_URL' => 'http://www.rsu-ids2012.com',
                'DOMAIN' => 'rsu-ids2012.com',
            ),
            'jhwyzs.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_953_',
                'SITE_URL' => 'http://www.jhwyzs.com',
                'DOMAIN' => 'jhwyzs.com',
            ),
            'insecurerockstar.com' =>
            array(
                'TABLE_SCHEMA' => 'ebi-onli_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1423_',
                'SITE_URL' => 'http://www.insecurerockstar.com',
                'DOMAIN' => 'insecurerockstar.com',
            ),
            'confrontingculture.info' =>
            array(
                'TABLE_SCHEMA' => 'ebi-onli_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1424_',
                'SITE_URL' => 'http://www.confrontingculture.info',
                'DOMAIN' => 'confrontingculture.info',
            ),
            'bestnethosting.info' =>
            array(
                'TABLE_SCHEMA' => 'ebi-onli_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1425_',
                'SITE_URL' => 'http://www.bestnethosting.info',
                'DOMAIN' => 'bestnethosting.info',
            ),
            'e-weightlosspills.com' =>
            array(
                'TABLE_SCHEMA' => 'ebi-onli_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1427_',
                'SITE_URL' => 'http://www.e-weightlosspills.com',
                'DOMAIN' => 'e-weightlosspills.com',
            ),
            'estatesalesdc.com' =>
            array(
                'TABLE_SCHEMA' => 'ebi-onli_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1521_',
                'SITE_URL' => 'http://www.estatesalesdc.com',
                'DOMAIN' => 'estatesalesdc.com',
            ),
            'scarabgames.com' =>
            array(
                'TABLE_SCHEMA' => 'ebi-onli_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_648_',
                'SITE_URL' => 'http://www.scarabgames.com',
                'DOMAIN' => 'scarabgames.com',
            ),
            'beavasatmosphere.com' =>
            array(
                'TABLE_SCHEMA' => 'ebi-onli_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_650_',
                'SITE_URL' => 'http://www.beavasatmosphere.com',
                'DOMAIN' => 'beavasatmosphere.com',
            ),
            'pennsylvania-injury-lawyer-attorney-law-firm.com' =>
            array(
                'TABLE_SCHEMA' => 'ebi-onli_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_658_',
                'SITE_URL' => 'http://www.pennsylvania-injury-lawyer-attorney-law-firm.com',
                'DOMAIN' => 'pennsylvania-injury-lawyer-attorney-law-firm.com',
            ),
            'tayzac.com' =>
            array(
                'TABLE_SCHEMA' => 'ebi-onli_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_661_',
                'SITE_URL' => 'http://www.tayzac.com',
                'DOMAIN' => 'tayzac.com',
            ),
            'yeast-infection-facts.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1428_',
                'SITE_URL' => 'http://www.yeast-infection-facts.com',
                'DOMAIN' => 'yeast-infection-facts.com',
            ),
            'yogamart.net' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1429_',
                'SITE_URL' => 'http://www.yogamart.net',
                'DOMAIN' => 'yogamart.net',
            ),
            'beautysalony.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1430_',
                'SITE_URL' => 'http://www.beautysalony.com',
                'DOMAIN' => 'beautysalony.com',
            ),
            'colorblockheadphones.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1522_',
                'SITE_URL' => 'http://www.colorblockheadphones.com',
                'DOMAIN' => 'colorblockheadphones.com',
            ),
            'lowridernews.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_651_',
                'SITE_URL' => 'http://www.lowridernews.com',
                'DOMAIN' => 'lowridernews.com',
            ),
            'fitforherswokc.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_662_',
                'SITE_URL' => 'http://www.fitforherswokc.com',
                'DOMAIN' => 'fitforherswokc.com',
            ),
            'hacksim.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_663_',
                'SITE_URL' => 'http://www.hacksim.com',
                'DOMAIN' => 'hacksim.com',
            ),
            'longestwalk2011.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_664_',
                'SITE_URL' => 'http://www.longestwalk2011.com',
                'DOMAIN' => 'longestwalk2011.com',
            ),
            'engineblox.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_674_',
                'SITE_URL' => 'http://www.engineblox.com',
                'DOMAIN' => 'engineblox.com',
            ),
            'soyuzenerg.info' =>
            array(
                'TABLE_SCHEMA' => 'emall-us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1420_',
                'SITE_URL' => 'http://www.soyuzenerg.info',
                'DOMAIN' => 'soyuzenerg.info',
            ),
            'smokemirror.org' =>
            array(
                'TABLE_SCHEMA' => 'emall-us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1421_',
                'SITE_URL' => 'http://www.smokemirror.org',
                'DOMAIN' => 'smokemirror.org',
            ),
            'justanotherforum.com' =>
            array(
                'TABLE_SCHEMA' => 'emall-us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1422_',
                'SITE_URL' => 'http://www.justanotherforum.com',
                'DOMAIN' => 'justanotherforum.com',
            ),
            'fantasy-flix.com' =>
            array(
                'TABLE_SCHEMA' => 'emall-us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1523_',
                'SITE_URL' => 'http://www.fantasy-flix.com',
                'DOMAIN' => 'fantasy-flix.com',
            ),
            'natural-weight-loss.biz' =>
            array(
                'TABLE_SCHEMA' => 'emall-us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_583_',
                'SITE_URL' => 'http://www.natural-weight-loss.biz',
                'DOMAIN' => 'natural-weight-loss.biz',
            ),
            'bodybeautifulonline.com' =>
            array(
                'TABLE_SCHEMA' => 'emall-us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_587_',
                'SITE_URL' => 'http://www.bodybeautifulonline.com',
                'DOMAIN' => 'bodybeautifulonline.com',
            ),
            'americanflyerppg.com' =>
            array(
                'TABLE_SCHEMA' => 'emall-us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_642_',
                'SITE_URL' => 'http://www.americanflyerppg.com',
                'DOMAIN' => 'americanflyerppg.com',
            ),
            'carefreeanxietycoaching.com' =>
            array(
                'TABLE_SCHEMA' => 'emall-us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_646_',
                'SITE_URL' => 'http://www.carefreeanxietycoaching.com',
                'DOMAIN' => 'carefreeanxietycoaching.com',
            ),
            'flavoredspace.com' =>
            array(
                'TABLE_SCHEMA' => 'emall-us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_647_',
                'SITE_URL' => 'http://www.flavoredspace.com',
                'DOMAIN' => 'flavoredspace.com',
            ),
            'simplicitysake.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1416_',
                'SITE_URL' => 'http://www.simplicitysake.com',
                'DOMAIN' => 'simplicitysake.com',
            ),
            'theswornin.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1417_',
                'SITE_URL' => 'http://www.theswornin.com',
                'DOMAIN' => 'theswornin.com',
            ),
            'out-of-debt.org' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1418_',
                'SITE_URL' => 'http://www.out-of-debt.org',
                'DOMAIN' => 'out-of-debt.org',
            ),
            'primehealthcoaching.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1419_',
                'SITE_URL' => 'http://www.primehealthcoaching.com',
                'DOMAIN' => 'primehealthcoaching.com',
            ),
            'covistawireless.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1524_',
                'SITE_URL' => 'http://www.covistawireless.com',
                'DOMAIN' => 'covistawireless.com',
            ),
            'golftipsweb.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_574_',
                'SITE_URL' => 'http://golftipsweb.com',
                'DOMAIN' => 'golftipsweb.com',
            ),
            'flatsinmelbourne.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_629_',
                'SITE_URL' => 'http://www.flatsinmelbourne.com',
                'DOMAIN' => 'flatsinmelbourne.com',
            ),
            'azurerealestateagents.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_639_',
                'SITE_URL' => 'http://www.azurerealestateagents.com',
                'DOMAIN' => 'azurerealestateagents.com',
            ),
            'baltimore-injury-lawyer-attorney-law-firm.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_640_',
                'SITE_URL' => 'http://www.baltimore-injury-lawyer-attorney-law-firm.com',
                'DOMAIN' => 'baltimore-injury-lawyer-attorney-law-firm.com',
            ),
            'alltherightwords.biz' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_645_',
                'SITE_URL' => 'http://www.alltherightwords.biz',
                'DOMAIN' => 'alltherightwords.biz',
            ),
            'somanabolicmusclemaximizerreviewz.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_10_',
                'SITE_URL' => 'http://www.somanabolicmusclemaximizerreviewz.com',
                'DOMAIN' => 'somanabolicmusclemaximizerreviewz.com',
            ),
            'acsbrightspotscampaign.org' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_6_',
                'SITE_URL' => 'http://www.acsbrightspotscampaign.org',
                'DOMAIN' => 'acsbrightspotscampaign.org',
            ),
            'presidiotravelodge.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_7_',
                'SITE_URL' => 'http://www.presidiotravelodge.com',
                'DOMAIN' => 'presidiotravelodge.com',
            ),
            'ieaaa.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_8_',
                'SITE_URL' => 'http://www.ieaaa.com',
                'DOMAIN' => 'ieaaa.com',
            ),
            'hunqing.biz' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_9_',
                'SITE_URL' => 'http://www.hunqing.biz',
                'DOMAIN' => 'hunqing.biz',
            ),
            'boatingandsailingaccessories.net' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1411_',
                'SITE_URL' => 'http://www.boatingandsailingaccessories.net',
                'DOMAIN' => 'boatingandsailingaccessories.net',
            ),
            'didzisdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1412_',
                'SITE_URL' => 'http://www.didzisdesign.com',
                'DOMAIN' => 'didzisdesign.com',
            ),
            'atlantisstudiosripoff.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1413_',
                'SITE_URL' => 'http://www.atlantisstudiosripoff.com',
                'DOMAIN' => 'atlantisstudiosripoff.com',
            ),
            'bjstudent.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1414_',
                'SITE_URL' => 'http://www.bjstudent.com',
                'DOMAIN' => 'bjstudent.com',
            ),
            'azzeitouna.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1415_',
                'SITE_URL' => 'http://www.azzeitouna.com',
                'DOMAIN' => 'azzeitouna.com',
            ),
            'giantsteamfans.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1525_',
                'SITE_URL' => 'http://www.giantsteamfans.com',
                'DOMAIN' => 'giantsteamfans.com',
            ),
            'world-series-of-poker-1996.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_608_',
                'SITE_URL' => 'http://www.world-series-of-poker-1996.com',
                'DOMAIN' => 'world-series-of-poker-1996.com',
            ),
            'followmenyc.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_615_',
                'SITE_URL' => 'http://www.followmenyc.com',
                'DOMAIN' => 'followmenyc.com',
            ),
            'nyinternationalantiquesshow.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_630_',
                'SITE_URL' => 'http://www.nyinternationalantiquesshow.com',
                'DOMAIN' => 'nyinternationalantiquesshow.com',
            ),
            'capexperience.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_631_',
                'SITE_URL' => 'http://www.capexperience.com',
                'DOMAIN' => 'capexperience.com',
            ),
            'tarheel-webdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_641_',
                'SITE_URL' => 'http://www.tarheel-webdesign.com',
                'DOMAIN' => 'tarheel-webdesign.com',
            ),
            'medprescribe.com' =>
            array(
                'TABLE_SCHEMA' => 'family-w_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1406_',
                'SITE_URL' => 'http://www.medprescribe.com',
                'DOMAIN' => 'medprescribe.com',
            ),
            'djgamble.net' =>
            array(
                'TABLE_SCHEMA' => 'family-w_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1407_',
                'SITE_URL' => 'http://www.djgamble.net',
                'DOMAIN' => 'djgamble.net',
            ),
            'infrastructure2009.com' =>
            array(
                'TABLE_SCHEMA' => 'family-w_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1408_',
                'SITE_URL' => 'http://www.infrastructure2009.com',
                'DOMAIN' => 'infrastructure2009.com',
            ),
            'amarspices.com' =>
            array(
                'TABLE_SCHEMA' => 'family-w_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1409_',
                'SITE_URL' => 'http://www.amarspices.com',
                'DOMAIN' => 'amarspices.com',
            ),
            'all-patch.info' =>
            array(
                'TABLE_SCHEMA' => 'family-w_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1410_',
                'SITE_URL' => 'http://www.all-patch.info',
                'DOMAIN' => 'all-patch.info',
            ),
            'software-blog.info' =>
            array(
                'TABLE_SCHEMA' => 'family-w_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1526_',
                'SITE_URL' => 'http://www.software-blog.info',
                'DOMAIN' => 'software-blog.info',
            ),
            'lakelouisehotelscanada.com' =>
            array(
                'TABLE_SCHEMA' => 'family-w_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_605_',
                'SITE_URL' => 'http://www.lakelouisehotelscanada.com',
                'DOMAIN' => 'lakelouisehotelscanada.com',
            ),
            'uofnkonatech.com' =>
            array(
                'TABLE_SCHEMA' => 'family-w_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_624_',
                'SITE_URL' => 'http://www.uofnkonatech.com',
                'DOMAIN' => 'uofnkonatech.com',
            ),
            'tawasanet.org' =>
            array(
                'TABLE_SCHEMA' => 'family-w_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_625_',
                'SITE_URL' => 'http://www.tawasanet.org',
                'DOMAIN' => 'tawasanet.org',
            ),
            'worldpokertour2005.com' =>
            array(
                'TABLE_SCHEMA' => 'family-w_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_626_',
                'SITE_URL' => 'http://www.worldpokertour2005.com',
                'DOMAIN' => 'worldpokertour2005.com',
            ),
            'worldpeacementor.com' =>
            array(
                'TABLE_SCHEMA' => 'family-w_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_638_',
                'SITE_URL' => 'http://www.worldpeacementor.com',
                'DOMAIN' => 'worldpeacementor.com',
            ),
            'sellvoip.net' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1402_',
                'SITE_URL' => 'http://www.sellvoip.net',
                'DOMAIN' => 'sellvoip.net',
            ),
            'trueoriginorganics.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1403_',
                'SITE_URL' => 'http://www.trueoriginorganics.com',
                'DOMAIN' => 'trueoriginorganics.com',
            ),
            'wealth-online.net' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1404_',
                'SITE_URL' => 'http://www.wealth-online.net',
                'DOMAIN' => 'wealth-online.net',
            ),
            'webtoolscollection.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1405_',
                'SITE_URL' => 'http://www.webtoolscollection.com',
                'DOMAIN' => 'webtoolscollection.com',
            ),
            'shopbedstuy.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1528_',
                'SITE_URL' => 'http://www.shopbedstuy.com',
                'DOMAIN' => 'shopbedstuy.com',
            ),
            'goodautotransport.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_614_',
                'SITE_URL' => 'http://www.goodautotransport.com',
                'DOMAIN' => 'goodautotransport.com',
            ),
            'susifineart.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_623_',
                'SITE_URL' => 'http://www.susifineart.com',
                'DOMAIN' => 'susifineart.com',
            ),
            'golf-bluesky.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_636_',
                'SITE_URL' => 'http://www.golf-bluesky.com',
                'DOMAIN' => 'golf-bluesky.com',
            ),
            'shopearthessentials.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_637_',
                'SITE_URL' => 'http://www.shopearthessentials.com',
                'DOMAIN' => 'shopearthessentials.com',
            ),
            'sunwisesolarus.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_644_',
                'SITE_URL' => 'http://www.sunwisesolarus.com',
                'DOMAIN' => 'sunwisesolarus.com',
            ),
            'xn-----uldfbofebqo6f7eta.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_54_',
                'SITE_URL' => 'http://www.xn-----uldfbofebqo6f7eta.com',
                'DOMAIN' => 'xn-----uldfbofebqo6f7eta.com',
            ),
            'west-houston-dentist.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1397_',
                'SITE_URL' => 'http://www.west-houston-dentist.com',
                'DOMAIN' => 'west-houston-dentist.com',
            ),
            'hopohosting.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1398_',
                'SITE_URL' => 'http://www.hopohosting.com',
                'DOMAIN' => 'hopohosting.com',
            ),
            'labsconf.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1399_',
                'SITE_URL' => 'http://www.labsconf.com',
                'DOMAIN' => 'labsconf.com',
            ),
            'beatrizargimon.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1400_',
                'SITE_URL' => 'http://www.beatrizargimon.com',
                'DOMAIN' => 'beatrizargimon.com',
            ),
            'outlaw-performance.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1401_',
                'SITE_URL' => 'http://www.outlaw-performance.com',
                'DOMAIN' => 'outlaw-performance.com',
            ),
            'cosmeticscheapmac.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1529_',
                'SITE_URL' => 'http://www.cosmeticscheapmac.com',
                'DOMAIN' => 'cosmeticscheapmac.com',
            ),
            'imagereel.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_601_',
                'SITE_URL' => 'http://www.imagereel.com',
                'DOMAIN' => 'imagereel.com',
            ),
            'home-drugtesting.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_612_',
                'SITE_URL' => 'http://www.home-drugtesting.com',
                'DOMAIN' => 'home-drugtesting.com',
            ),
            'motormapusa.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_621_',
                'SITE_URL' => 'http://www.motormapusa.com',
                'DOMAIN' => 'motormapusa.com',
            ),
            'gabaroundtable.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_634_',
                'SITE_URL' => 'http://www.gabaroundtable.com',
                'DOMAIN' => 'gabaroundtable.com',
            ),
            'iigonline.org' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_635_',
                'SITE_URL' => 'http://www.iigonline.org',
                'DOMAIN' => 'iigonline.org',
            ),
            'thenicestrealtors.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1391_',
                'SITE_URL' => 'http://www.thenicestrealtors.com',
                'DOMAIN' => 'thenicestrealtors.com',
            ),
            'washington-dc-injury-lawyer-attorney-law-firm.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1393_',
                'SITE_URL' => 'http://www.washington-dc-injury-lawyer-attorney-law-firm.com',
                'DOMAIN' => 'washington-dc-injury-lawyer-attorney-law-firm.com',
            ),
            'weddingsitesweb.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1394_',
                'SITE_URL' => 'http://www.weddingsitesweb.com',
                'DOMAIN' => 'weddingsitesweb.com',
            ),
            'uk-ghdscheap.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1395_',
                'SITE_URL' => 'http://www.uk-ghdscheap.com',
                'DOMAIN' => 'uk-ghdscheap.com',
            ),
            'thehomeopathicdoctor.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1396_',
                'SITE_URL' => 'http://www.thehomeopathicdoctor.com',
                'DOMAIN' => 'thehomeopathicdoctor.com',
            ),
            'quilterspaletteonline.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1530_',
                'SITE_URL' => 'http://www.quilterspaletteonline.com',
                'DOMAIN' => 'quilterspaletteonline.com',
            ),
            'congosoccer.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_610_',
                'SITE_URL' => 'http://www.congosoccer.com',
                'DOMAIN' => 'congosoccer.com',
            ),
            'master-handyman.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_619_',
                'SITE_URL' => 'http://www.master-handyman.com',
                'DOMAIN' => 'master-handyman.com',
            ),
            'banishedthefilm.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_632_',
                'SITE_URL' => 'http://www.banishedthefilm.com',
                'DOMAIN' => 'banishedthefilm.com',
            ),
            'leap-and-shedding.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_633_',
                'SITE_URL' => 'http://www.leap-and-shedding.com',
                'DOMAIN' => 'leap-and-shedding.com',
            ),
            'englishtownfire.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_643_',
                'SITE_URL' => 'http://www.englishtownfire.com',
                'DOMAIN' => 'englishtownfire.com',
            ),
            'leoceballosrealty.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1386_',
                'SITE_URL' => 'http://www.leoceballosrealty.com',
                'DOMAIN' => 'leoceballosrealty.com',
            ),
            'lilivideos.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1387_',
                'SITE_URL' => 'http://www.lilivideos.com',
                'DOMAIN' => 'lilivideos.com',
            ),
            'studio3bphotography.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1388_',
                'SITE_URL' => 'http://www.studio3bphotography.com',
                'DOMAIN' => 'studio3bphotography.com',
            ),
            'talkweed.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1389_',
                'SITE_URL' => 'http://www.talkweed.com',
                'DOMAIN' => 'talkweed.com',
            ),
            'thenaturalwellness.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1390_',
                'SITE_URL' => 'http://www.thenaturalwellness.com',
                'DOMAIN' => 'thenaturalwellness.com',
            ),
            'louisvuittonoutletbll.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1531_',
                'SITE_URL' => 'http://www.louisvuittonoutletbll.com',
                'DOMAIN' => 'louisvuittonoutletbll.com',
            ),
            'emoticonstudio.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_598_',
                'SITE_URL' => 'http://www.emoticonstudio.com',
                'DOMAIN' => 'emoticonstudio.com',
            ),
            'coachbagsstore.info' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_606_',
                'SITE_URL' => 'http://www.coachbagsstore.info',
                'DOMAIN' => 'coachbagsstore.info',
            ),
            'sbvsa.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_622_',
                'SITE_URL' => 'http://www.sbvsa.com',
                'DOMAIN' => 'sbvsa.com',
            ),
            'sarawear.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_627_',
                'SITE_URL' => 'http://www.sarawear.com',
                'DOMAIN' => 'sarawear.com',
            ),
            'southwestfutsalchampionship.info' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_628_',
                'SITE_URL' => 'http://www.southwestfutsalchampionship.info',
                'DOMAIN' => 'southwestfutsalchampionship.info',
            ),
            'princetonanalytics.org' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_63_',
                'SITE_URL' => 'http://www.princetonanalytics.org',
                'DOMAIN' => 'princetonanalytics.org',
            ),
            'fyghw.net' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_64_',
                'SITE_URL' => 'http://www.fyghw.net',
                'DOMAIN' => 'fyghw.net',
            ),
            'shihohoshino.com' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_65_',
                'SITE_URL' => 'http://www.shihohoshino.com',
                'DOMAIN' => 'shihohoshino.com',
            ),
            'justinrothmusic.com' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_66_',
                'SITE_URL' => 'http://www.justinrothmusic.com',
                'DOMAIN' => 'justinrothmusic.com',
            ),
            'gotohudson.org' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_67_',
                'SITE_URL' => 'http://www.gotohudson.org',
                'DOMAIN' => 'gotohudson.org',
            ),
            'integritychrysler.com' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1298_',
                'SITE_URL' => 'http://www.integritychrysler.com',
                'DOMAIN' => 'integritychrysler.com',
            ),
            'clangirlfight.com' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1382_',
                'SITE_URL' => 'http://www.clangirlfight.com',
                'DOMAIN' => 'clangirlfight.com',
            ),
            'aidaonline.info' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1383_',
                'SITE_URL' => 'http://www.aidaonline.info',
                'DOMAIN' => 'aidaonline.info',
            ),
            'elcanelorestaurant.com' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1384_',
                'SITE_URL' => 'http://www.elcanelorestaurant.com',
                'DOMAIN' => 'elcanelorestaurant.com',
            ),
            'excalibur-toys.com' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1385_',
                'SITE_URL' => 'http://www.excalibur-toys.com',
                'DOMAIN' => 'excalibur-toys.com',
            ),
            'louisvuittonoutletcsv.com' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1532_',
                'SITE_URL' => 'http://www.louisvuittonoutletcsv.com',
                'DOMAIN' => 'louisvuittonoutletcsv.com',
            ),
            'bsawebhost.com' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_602_',
                'SITE_URL' => 'http://www.bsawebhost.com',
                'DOMAIN' => 'bsawebhost.com',
            ),
            'bald4brains.org' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_611_',
                'SITE_URL' => 'http://www.bald4brains.org',
                'DOMAIN' => 'bald4brains.org',
            ),
            'thezonemckinney.com' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_617_',
                'SITE_URL' => 'http://www.thezonemckinney.com',
                'DOMAIN' => 'thezonemckinney.com',
            ),
            'soschildabduction.com' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_618_',
                'SITE_URL' => 'http://www.soschildabduction.com',
                'DOMAIN' => 'soschildabduction.com',
            ),
            'reusebuildingmaterials.com' =>
            array(
                'TABLE_SCHEMA' => 'focuson-_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_620_',
                'SITE_URL' => 'http://www.reusebuildingmaterials.com',
                'DOMAIN' => 'reusebuildingmaterials.com',
            ),
            'republicanrenaissancepac.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1378_',
                'SITE_URL' => 'http://www.republicanrenaissancepac.com',
                'DOMAIN' => 'republicanrenaissancepac.com',
            ),
            'freshcostsless.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1379_',
                'SITE_URL' => 'http://www.freshcostsless.com',
                'DOMAIN' => 'freshcostsless.com',
            ),
            'mikiesbagel.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1380_',
                'SITE_URL' => 'http://www.mikiesbagel.com',
                'DOMAIN' => 'mikiesbagel.com',
            ),
            'advertisebetter.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1381_',
                'SITE_URL' => 'http://www.advertisebetter.com',
                'DOMAIN' => 'advertisebetter.com',
            ),
            'discountlouboutinushoe.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1533_',
                'SITE_URL' => 'http://www.discountlouboutinushoe.com',
                'DOMAIN' => 'discountlouboutinushoe.com',
            ),
            'proactivesportsmassage.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_572_',
                'SITE_URL' => 'http://www.proactivesportsmassage.com',
                'DOMAIN' => 'proactivesportsmassage.com',
            ),
            'tornadostock.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_597_',
                'SITE_URL' => 'http://www.tornadostock.com',
                'DOMAIN' => 'tornadostock.com',
            ),
            'carrollogos.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_604_',
                'SITE_URL' => 'http://www.carrollogos.com',
                'DOMAIN' => 'carrollogos.com',
            ),
            'libertyunchained.us' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_607_',
                'SITE_URL' => 'http://www.libertyunchained.us',
                'DOMAIN' => 'libertyunchained.us',
            ),
            'lostinthecrowd.org' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_616_',
                'SITE_URL' => 'http://www.lostinthecrowd.org',
                'DOMAIN' => 'lostinthecrowd.org',
            ),
            'compatibili.me' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1161_',
                'SITE_URL' => 'http://www.compatibili.me',
                'DOMAIN' => 'compatibili.me',
            ),
            'chatyourmouth.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1185_',
                'SITE_URL' => 'http://www.chatyourmouth.com',
                'DOMAIN' => 'chatyourmouth.com',
            ),
            'clbgrls.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1186_',
                'SITE_URL' => 'http://www.clbgrls.com',
                'DOMAIN' => 'clbgrls.com',
            ),
            'companerismoensuamor.org' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1187_',
                'SITE_URL' => 'http://www.companerismoensuamor.org',
                'DOMAIN' => 'companerismoensuamor.org',
            ),
            'ugg-boots-uk.net' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1534_',
                'SITE_URL' => 'http://www.ugg-boots-uk.net',
                'DOMAIN' => 'ugg-boots-uk.net',
            ),
            'francinelemay.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_885_',
                'SITE_URL' => 'http://www.francinelemay.com',
                'DOMAIN' => 'francinelemay.com',
            ),
            'standinghealthacupuncture.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_886_',
                'SITE_URL' => 'http://www.standinghealthacupuncture.com',
                'DOMAIN' => 'standinghealthacupuncture.com',
            ),
            'makesomethingtoday.me' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_887_',
                'SITE_URL' => 'http://www.makesomethingtoday.me',
                'DOMAIN' => 'makesomethingtoday.me',
            ),
            'reconinspectionsllc.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_888_',
                'SITE_URL' => 'http://www.reconinspectionsllc.com',
                'DOMAIN' => 'reconinspectionsllc.com',
            ),
            'amayavillazan.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_889_',
                'SITE_URL' => 'http://www.amayavillazan.com',
                'DOMAIN' => 'amayavillazan.com',
            ),
            '1-credit-card-debt.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1374_',
                'SITE_URL' => 'http://www.1-credit-card-debt.com',
                'DOMAIN' => '1-credit-card-debt.com',
            ),
            'carrollogos.org' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1375_',
                'SITE_URL' => 'http://www.carrollogos.org',
                'DOMAIN' => 'carrollogos.org',
            ),
            'cleanupalbanynow.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1376_',
                'SITE_URL' => 'http://www.cleanupalbanynow.com',
                'DOMAIN' => 'cleanupalbanynow.com',
            ),
            'nmlegionriders.us' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1377_',
                'SITE_URL' => 'http://www.nmlegionriders.us',
                'DOMAIN' => 'nmlegionriders.us',
            ),
            'coachoutletgf.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1535_',
                'SITE_URL' => 'http://www.coachoutletgf.com',
                'DOMAIN' => 'coachoutletgf.com',
            ),
            'sanddollarsports.net' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_581_',
                'SITE_URL' => 'http://www.sanddollarsports.net',
                'DOMAIN' => 'sanddollarsports.net',
            ),
            'proposal-puzzles.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_591_',
                'SITE_URL' => 'http://www.proposal-puzzles.com',
                'DOMAIN' => 'proposal-puzzles.com',
            ),
            'telescopesonlineguide.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_593_',
                'SITE_URL' => 'http://www.telescopesonlineguide.com',
                'DOMAIN' => 'telescopesonlineguide.com',
            ),
            'thealibiinn.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_600_',
                'SITE_URL' => 'http://www.thealibiinn.com',
                'DOMAIN' => 'thealibiinn.com',
            ),
            'top100cosmetic.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_613_',
                'SITE_URL' => 'http://www.top100cosmetic.com',
                'DOMAIN' => 'top100cosmetic.com',
            ),
            'angelsofsoccer.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1369_',
                'SITE_URL' => 'http://www.angelsofsoccer.com',
                'DOMAIN' => 'angelsofsoccer.com',
            ),
            'r-wildhorseranch.info' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1370_',
                'SITE_URL' => 'http://www.r-wildhorseranch.info',
                'DOMAIN' => 'r-wildhorseranch.info',
            ),
            'nyfbags.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1371_',
                'SITE_URL' => 'http://www.nyfbags.com',
                'DOMAIN' => 'nyfbags.com',
            ),
            'rc-sports.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1372_',
                'SITE_URL' => 'http://www.rc-sports.com',
                'DOMAIN' => 'rc-sports.com',
            ),
            'bulk-sms-business.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1373_',
                'SITE_URL' => 'http://www.bulk-sms-business.com',
                'DOMAIN' => 'bulk-sms-business.com',
            ),
            'authenticmichaelkorsbags.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1536_',
                'SITE_URL' => 'http://www.authenticmichaelkorsbags.com',
                'DOMAIN' => 'authenticmichaelkorsbags.com',
            ),
            'constructoracorwell.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_578_',
                'SITE_URL' => 'http://www.constructoracorwell.com',
                'DOMAIN' => 'constructoracorwell.com',
            ),
            'debteliminationspro.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_579_',
                'SITE_URL' => 'http://www.debteliminationspro.com',
                'DOMAIN' => 'debteliminationspro.com',
            ),
            'kresztesztek.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_586_',
                'SITE_URL' => 'http://www.kresztesztek.com',
                'DOMAIN' => 'kresztesztek.com',
            ),
            'shopplayandlearn.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_588_',
                'SITE_URL' => 'http://www.shopplayandlearn.com',
                'DOMAIN' => 'shopplayandlearn.com',
            ),
            'realestatemasterymembership.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_590_',
                'SITE_URL' => 'http://www.realestatemasterymembership.com',
                'DOMAIN' => 'realestatemasterymembership.com',
            ),
            'aldanasheatair.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1359_',
                'SITE_URL' => 'http://www.aldanasheatair.com',
                'DOMAIN' => 'aldanasheatair.com',
            ),
            '1sttrustsecurity.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1360_',
                'SITE_URL' => 'http://www.1sttrustsecurity.com',
                'DOMAIN' => '1sttrustsecurity.com',
            ),
            'armstrongtrack.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1361_',
                'SITE_URL' => 'http://www.armstrongtrack.com',
                'DOMAIN' => 'armstrongtrack.com',
            ),
            'bfcoopermusic.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1362_',
                'SITE_URL' => 'http://www.bfcoopermusic.com',
                'DOMAIN' => 'bfcoopermusic.com',
            ),
            'clickbankcashmall.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1363_',
                'SITE_URL' => 'http://www.clickbankcashmall.com',
                'DOMAIN' => 'clickbankcashmall.com',
            ),
            'louisvuittonpurseoutletss.net' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1537_',
                'SITE_URL' => 'http://www.louisvuittonpurseoutletss.net',
                'DOMAIN' => 'louisvuittonpurseoutletss.net',
            ),
            'steveschalchlin.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_580_',
                'SITE_URL' => 'http://www.steveschalchlin.com',
                'DOMAIN' => 'steveschalchlin.com',
            ),
            'haus-kaufen24.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_589_',
                'SITE_URL' => 'http://www.haus-kaufen24.com',
                'DOMAIN' => 'haus-kaufen24.com',
            ),
            'idhouston.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_592_',
                'SITE_URL' => 'http://www.idhouston.com',
                'DOMAIN' => 'idhouston.com',
            ),
            'freedavidthorne.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_594_',
                'SITE_URL' => 'http://www.freedavidthorne.com',
                'DOMAIN' => 'freedavidthorne.com',
            ),
            'anywhereflower.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_609_',
                'SITE_URL' => 'http://www.anywhereflower.com',
                'DOMAIN' => 'anywhereflower.com',
            ),
            'gamblingthrills.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1364_',
                'SITE_URL' => 'http://www.gamblingthrills.com',
                'DOMAIN' => 'gamblingthrills.com',
            ),
            'everykindofinsurance.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1365_',
                'SITE_URL' => 'http://www.everykindofinsurance.com',
                'DOMAIN' => 'everykindofinsurance.com',
            ),
            'trussvillesoccer.org' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1366_',
                'SITE_URL' => 'http://www.trussvillesoccer.org',
                'DOMAIN' => 'trussvillesoccer.org',
            ),
            'garagedoctorz.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1367_',
                'SITE_URL' => 'http://www.garagedoctorz.com',
                'DOMAIN' => 'garagedoctorz.com',
            ),
            '800wg.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1368_',
                'SITE_URL' => 'http://www.800wg.com',
                'DOMAIN' => '800wg.com',
            ),
            'coachoutletat.net' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1538_',
                'SITE_URL' => 'http://www.coachoutletat.net',
                'DOMAIN' => 'coachoutletat.net',
            ),
            'oneactdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_584_',
                'SITE_URL' => 'http://www.oneactdesign.com',
                'DOMAIN' => 'oneactdesign.com',
            ),
            'denmadesigns.us' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_585_',
                'SITE_URL' => 'http://www.denmadesigns.us',
                'DOMAIN' => 'denmadesigns.us',
            ),
            'lifestyle-enhancement.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_595_',
                'SITE_URL' => 'http://www.lifestyle-enhancement.com',
                'DOMAIN' => 'lifestyle-enhancement.com',
            ),
            'hirkan.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_596_',
                'SITE_URL' => 'http://www.hirkan.com',
                'DOMAIN' => 'hirkan.com',
            ),
            'beachvacation-sandiego.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_603_',
                'SITE_URL' => 'http://www.beachvacation-sandiego.com',
                'DOMAIN' => 'beachvacation-sandiego.com',
            ),
            'louisvuittonbeltslb.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1539_',
                'SITE_URL' => 'http://www.louisvuittonbeltslb.com',
                'DOMAIN' => 'louisvuittonbeltslb.com',
            ),
            'nomoredeductible.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_545_',
                'SITE_URL' => 'http://www.nomoredeductible.com',
                'DOMAIN' => 'nomoredeductible.com',
            ),
            'prepnewsreport.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_554_',
                'SITE_URL' => 'http://www.prepnewsreport.com',
                'DOMAIN' => 'prepnewsreport.com',
            ),
            'morristowns.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_557_',
                'SITE_URL' => 'http://www.morristowns.com',
                'DOMAIN' => 'morristowns.com',
            ),
            'pqcarpets.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_558_',
                'SITE_URL' => 'http://www.pqcarpets.com',
                'DOMAIN' => 'pqcarpets.com',
            ),
            'mi-ann.org' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_565_',
                'SITE_URL' => 'http://www.mi-ann.org',
                'DOMAIN' => 'mi-ann.org',
            ),
            'personalfinancedirectory.info' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1354_',
                'SITE_URL' => 'http://www.personalfinancedirectory.info',
                'DOMAIN' => 'personalfinancedirectory.info',
            ),
            'netshottest.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1355_',
                'SITE_URL' => 'http://www.netshottest.com',
                'DOMAIN' => 'netshottest.com',
            ),
            'inetprogramming.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1356_',
                'SITE_URL' => 'http://www.inetprogramming.com',
                'DOMAIN' => 'inetprogramming.com',
            ),
            'casinoslosangeles.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1357_',
                'SITE_URL' => 'http://www.casinoslosangeles.com',
                'DOMAIN' => 'casinoslosangeles.com',
            ),
            'dy-law.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1358_',
                'SITE_URL' => 'http://www.dy-law.com',
                'DOMAIN' => 'dy-law.com',
            ),
            'jordanretro12sforsale.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1540_',
                'SITE_URL' => 'http://www.jordanretro12sforsale.com',
                'DOMAIN' => 'jordanretro12sforsale.com',
            ),
            'cinemaeyehonors2010.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_573_',
                'SITE_URL' => 'http://www.cinemaeyehonors2010.com',
                'DOMAIN' => 'cinemaeyehonors2010.com',
            ),
            '30minutefinance.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_575_',
                'SITE_URL' => 'http://www.30minutefinance.com',
                'DOMAIN' => '30minutefinance.com',
            ),
            'biminnesota.org' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_576_',
                'SITE_URL' => 'http://www.biminnesota.org',
                'DOMAIN' => 'biminnesota.org',
            ),
            'lmtmemorial.org' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_577_',
                'SITE_URL' => 'http://www.lmtmemorial.org',
                'DOMAIN' => 'lmtmemorial.org',
            ),
            'jennanthony.info' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_582_',
                'SITE_URL' => 'http://www.jennanthony.info',
                'DOMAIN' => 'jennanthony.info',
            ),
            'lessthanthreecomics.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1075_',
                'SITE_URL' => 'http://www.lessthanthreecomics.com',
                'DOMAIN' => 'lessthanthreecomics.com',
            ),
            'golfclubcomponents.info' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1092_',
                'SITE_URL' => 'http://www.golfclubcomponents.info',
                'DOMAIN' => 'golfclubcomponents.info',
            ),
            '3years3minutes.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1094_',
                'SITE_URL' => 'http://www.3years3minutes.com',
                'DOMAIN' => '3years3minutes.com',
            ),
            'hlbexhzx.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1541_',
                'SITE_URL' => 'http://www.hlbexhzx.com',
                'DOMAIN' => 'hlbexhzx.com',
            ),
            'percyspersonals.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_541_',
                'SITE_URL' => 'http://www.percyspersonals.com',
                'DOMAIN' => 'percyspersonals.com',
            ),
            'rochestercost.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_546_',
                'SITE_URL' => 'http://www.rochestercost.com',
                'DOMAIN' => 'rochestercost.com',
            ),
            'nq12.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_550_',
                'SITE_URL' => 'http://www.nq12.com',
                'DOMAIN' => 'nq12.com',
            ),
            'signtecs.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_560_',
                'SITE_URL' => 'http://www.signtecs.com',
                'DOMAIN' => 'signtecs.com',
            ),
            'tahoewelcome.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_566_',
                'SITE_URL' => 'http://www.tahoewelcome.com',
                'DOMAIN' => 'tahoewelcome.com',
            ),
            'tomsfitness.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1089_',
                'SITE_URL' => 'http://www.tomsfitness.com',
                'DOMAIN' => 'tomsfitness.com',
            ),
            'cheapjordanretro7online.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1542_',
                'SITE_URL' => 'http://www.cheapjordanretro7online.com',
                'DOMAIN' => 'cheapjordanretro7online.com',
            ),
            'meridianhomes-sc.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_536_',
                'SITE_URL' => 'http://www.meridianhomes-sc.com',
                'DOMAIN' => 'meridianhomes-sc.com',
            ),
            'naturalgroupcr.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_538_',
                'SITE_URL' => 'http://www.naturalgroupcr.com',
                'DOMAIN' => 'naturalgroupcr.com',
            ),
            'plusfortyone.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_540_',
                'SITE_URL' => 'http://www.plusfortyone.com',
                'DOMAIN' => 'plusfortyone.com',
            ),
            'noevalleyacupuncture.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_543_',
                'SITE_URL' => 'http://www.noevalleyacupuncture.com',
                'DOMAIN' => 'noevalleyacupuncture.com',
            ),
            'padreislandtv.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_544_',
                'SITE_URL' => 'http://www.padreislandtv.com',
                'DOMAIN' => 'padreislandtv.com',
            ),
            'houndsummitteam.org' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1467_',
                'SITE_URL' => 'http://www.houndsummitteam.org',
                'DOMAIN' => 'houndsummitteam.org',
            ),
            'tele8mercedes.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_91_',
                'SITE_URL' => 'http://www.tele8mercedes.com',
                'DOMAIN' => 'tele8mercedes.com',
            ),
            'thehivegalleryslc.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_92_',
                'SITE_URL' => 'http://www.thehivegalleryslc.com',
                'DOMAIN' => 'thehivegalleryslc.com',
            ),
            'bariatricsurgery.biz' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_93_',
                'SITE_URL' => 'http://www.bariatricsurgery.biz',
                'DOMAIN' => 'bariatricsurgery.biz',
            ),
            'chowderman.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1468_',
                'SITE_URL' => 'http://www.chowderman.com',
                'DOMAIN' => 'chowderman.com',
            ),
            'videojunky.net' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1090_',
                'SITE_URL' => 'http://www.videojunky.net',
                'DOMAIN' => 'videojunky.net',
            ),
            'g4-ranch.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1091_',
                'SITE_URL' => 'http://www.g4-ranch.com',
                'DOMAIN' => 'g4-ranch.com',
            ),
            'louisvuittonpursepi.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1543_',
                'SITE_URL' => 'http://www.louisvuittonpursepi.com',
                'DOMAIN' => 'louisvuittonpursepi.com',
            ),
            'spiritoflifefoundation.org' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_542_',
                'SITE_URL' => 'http://www.spiritoflifefoundation.org',
                'DOMAIN' => 'spiritoflifefoundation.org',
            ),
            'mynewslab.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_547_',
                'SITE_URL' => 'http://www.mynewslab.com',
                'DOMAIN' => 'mynewslab.com',
            ),
            'rocknrollz.net' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_548_',
                'SITE_URL' => 'http://www.rocknrollz.net',
                'DOMAIN' => 'rocknrollz.net',
            ),
            'niangsou.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_551_',
                'SITE_URL' => 'http://www.niangsou.com',
                'DOMAIN' => 'niangsou.com',
            ),
            'misswaynecounty.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_552_',
                'SITE_URL' => 'http://www.misswaynecounty.com',
                'DOMAIN' => 'misswaynecounty.com',
            ),
            'amygotliffe.org' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1153_',
                'SITE_URL' => 'http://www.amygotliffe.org',
                'DOMAIN' => 'amygotliffe.org',
            ),
            'announceitevents.com' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1154_',
                'SITE_URL' => 'http://www.announceitevents.com',
                'DOMAIN' => 'announceitevents.com',
            ),
            'aramazd.net' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1155_',
                'SITE_URL' => 'http://www.aramazd.net',
                'DOMAIN' => 'aramazd.net',
            ),
            'britneyes.com' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1156_',
                'SITE_URL' => 'http://www.britneyes.com',
                'DOMAIN' => 'britneyes.com',
            ),
            'louissvuittononlineshop.net' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1544_',
                'SITE_URL' => 'http://www.louissvuittononlineshop.net',
                'DOMAIN' => 'louissvuittononlineshop.net',
            ),
            'goodbuildings.info' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_846_',
                'SITE_URL' => 'http://www.goodbuildings.info',
                'DOMAIN' => 'goodbuildings.info',
            ),
            'mianmoman.com' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_847_',
                'SITE_URL' => 'http://www.mianmoman.com',
                'DOMAIN' => 'mianmoman.com',
            ),
            'petlossgriefguide.com' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_848_',
                'SITE_URL' => 'http://www.petlossgriefguide.com',
                'DOMAIN' => 'petlossgriefguide.com',
            ),
            'svgcl.org' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_849_',
                'SITE_URL' => 'http://www.svgcl.org',
                'DOMAIN' => 'svgcl.org',
            ),
            'srsla.org' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_850_',
                'SITE_URL' => 'http://www.srsla.org',
                'DOMAIN' => 'srsla.org',
            ),
            'monclerjacketbay.com' =>
            array(
                'TABLE_SCHEMA' => 'guesthou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1658_',
                'SITE_URL' => 'http://www.monclerjacketbay.com',
                'DOMAIN' => 'monclerjacketbay.com',
            ),
            'becas.ws' =>
            array(
                'TABLE_SCHEMA' => 'guesthou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1681_',
                'SITE_URL' => 'http://www.becas.ws',
                'DOMAIN' => 'becas.ws',
            ),
            'gps2doublecad.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_57_',
                'SITE_URL' => 'http://www.gps2doublecad.com',
                'DOMAIN' => 'gps2doublecad.com',
            ),
            'mmsoline.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_58_',
                'SITE_URL' => 'http://www.mmsoline.com',
                'DOMAIN' => 'mmsoline.com',
            ),
            'southjerseycenter.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_59_',
                'SITE_URL' => 'http://www.southjerseycenter.com',
                'DOMAIN' => 'southjerseycenter.com',
            ),
            'camerawhore.me' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_60_',
                'SITE_URL' => 'http://www.camerawhore.me',
                'DOMAIN' => 'camerawhore.me',
            ),
            'sucompraweb.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_61_',
                'SITE_URL' => 'http://www.sucompraweb.com',
                'DOMAIN' => 'sucompraweb.com',
            ),
            'vickipeeki.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_62_',
                'SITE_URL' => 'http://www.vickipeeki.com',
                'DOMAIN' => 'vickipeeki.com',
            ),
            'weaco.org' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1088_',
                'SITE_URL' => 'http://www.weaco.org',
                'DOMAIN' => 'weaco.org',
            ),
            'hermesbags0.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1545_',
                'SITE_URL' => 'http://www.hermesbags0.com',
                'DOMAIN' => 'hermesbags0.com',
            ),
            'mariatmejia.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_534_',
                'SITE_URL' => 'http://www.mariatmejia.com',
                'DOMAIN' => 'mariatmejia.com',
            ),
            'matthewgreen.us' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_535_',
                'SITE_URL' => 'http://www.matthewgreen.us',
                'DOMAIN' => 'matthewgreen.us',
            ),
            'pompapetriyasos.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_564_',
                'SITE_URL' => 'http://www.pompapetriyasos.com',
                'DOMAIN' => 'pompapetriyasos.com',
            ),
            'mcas2003.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_569_',
                'SITE_URL' => 'http://www.mcas2003.com',
                'DOMAIN' => 'mcas2003.com',
            ),
            'stud8b.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1086_',
                'SITE_URL' => 'http://www.stud8b.com',
                'DOMAIN' => 'stud8b.com',
            ),
            'coachfactory2011.net' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1546_',
                'SITE_URL' => 'http://www.coachfactory2011.net',
                'DOMAIN' => 'coachfactory2011.net',
            ),
            'ldspw.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_522_',
                'SITE_URL' => 'http://www.ldspw.com',
                'DOMAIN' => 'ldspw.com',
            ),
            'lesjeuneschangentlafrique.org' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_523_',
                'SITE_URL' => 'http://www.lesjeuneschangentlafrique.org',
                'DOMAIN' => 'lesjeuneschangentlafrique.org',
            ),
            'kumordzi.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_529_',
                'SITE_URL' => 'http://www.kumordzi.com',
                'DOMAIN' => 'kumordzi.com',
            ),
            'labourbroadheath.org' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_530_',
                'SITE_URL' => 'http://www.labourbroadheath.org',
                'DOMAIN' => 'labourbroadheath.org',
            ),
            'pietvonline.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_559_',
                'SITE_URL' => 'http://www.pietvonline.com',
                'DOMAIN' => 'pietvonline.com',
            ),
            'digitalounge.net' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1299_',
                'SITE_URL' => 'http://www.digitalounge.net',
                'DOMAIN' => 'digitalounge.net',
            ),
            'mp4online.info' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1300_',
                'SITE_URL' => 'http://www.mp4online.info',
                'DOMAIN' => 'mp4online.info',
            ),
            'mountnmemories.net' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1301_',
                'SITE_URL' => 'http://www.mountnmemories.net',
                'DOMAIN' => 'mountnmemories.net',
            ),
            'myluckydoglottery.com' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1302_',
                'SITE_URL' => 'http://www.myluckydoglottery.com',
                'DOMAIN' => 'myluckydoglottery.com',
            ),
            'hqszw.com' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1547_',
                'SITE_URL' => 'http://www.hqszw.com',
                'DOMAIN' => 'hqszw.com',
            ),
            'hazenprojects.com' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_939_',
                'SITE_URL' => 'http://www.hazenprojects.com',
                'DOMAIN' => 'hazenprojects.com',
            ),
            'ocdaryarestaurant.com' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_941_',
                'SITE_URL' => 'http://www.ocdaryarestaurant.com',
                'DOMAIN' => 'ocdaryarestaurant.com',
            ),
            '365harvest.com' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_942_',
                'SITE_URL' => 'http://www.365harvest.com',
                'DOMAIN' => '365harvest.com',
            ),
            'garrett-hedlund.info' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_943_',
                'SITE_URL' => 'http://www.garrett-hedlund.info',
                'DOMAIN' => 'garrett-hedlund.info',
            ),
            'trackfourdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1087_',
                'SITE_URL' => 'http://www.trackfourdesign.com',
                'DOMAIN' => 'trackfourdesign.com',
            ),
            'noiznet.com' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1548_',
                'SITE_URL' => 'http://www.noiznet.com',
                'DOMAIN' => 'noiznet.com',
            ),
            'mamatemplate.com' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_481_',
                'SITE_URL' => 'http://www.mamatemplate.com',
                'DOMAIN' => 'mamatemplate.com',
            ),
            'mahaon.us' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_525_',
                'SITE_URL' => 'http://www.mahaon.us',
                'DOMAIN' => 'mahaon.us',
            ),
            'lucid-links.com' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_526_',
                'SITE_URL' => 'http://www.lucid-links.com',
                'DOMAIN' => 'lucid-links.com',
            ),
            'lifeforcefamilies.com' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_531_',
                'SITE_URL' => 'http://www.lifeforcefamilies.com',
                'DOMAIN' => 'lifeforcefamilies.com',
            ),
            'manchestersoccersd.com' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_533_',
                'SITE_URL' => 'http://www.manchestersoccersd.com',
                'DOMAIN' => 'manchestersoccersd.com',
            ),
            'anelagerqvist.net' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_10_',
                'SITE_URL' => 'http://anelagerqvist.net',
                'DOMAIN' => 'anelagerqvist.net',
            ),
            'angloarabictranslation.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_11_',
                'SITE_URL' => 'http://angloarabictranslation.com',
                'DOMAIN' => 'angloarabictranslation.com',
            ),
            'aplacenextdoor.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_12_',
                'SITE_URL' => 'http://aplacenextdoor.com',
                'DOMAIN' => 'aplacenextdoor.com',
            ),
            'allthingsmexico.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_9_',
                'SITE_URL' => 'http://allthingsmexico.com',
                'DOMAIN' => 'allthingsmexico.com',
            ),
            'itsmyweddingsite.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1084_',
                'SITE_URL' => 'http://www.itsmyweddingsite.com',
                'DOMAIN' => 'itsmyweddingsite.com',
            ),
            'que-es-la-diabetes.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1085_',
                'SITE_URL' => 'http://www.que-es-la-diabetes.com',
                'DOMAIN' => 'que-es-la-diabetes.com',
            ),
            'choijeungyun.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1549_',
                'SITE_URL' => 'http://www.choijeungyun.com',
                'DOMAIN' => 'choijeungyun.com',
            ),
            'kimberlygrant.net' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_511_',
                'SITE_URL' => 'http://www.kimberlygrant.net',
                'DOMAIN' => 'kimberlygrant.net',
            ),
            'kcconvoyofhope.org' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_515_',
                'SITE_URL' => 'http://www.kcconvoyofhope.org',
                'DOMAIN' => 'kcconvoyofhope.org',
            ),
            'kolbodagarden.info' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_518_',
                'SITE_URL' => 'http://www.kolbodagarden.info',
                'DOMAIN' => 'kolbodagarden.info',
            ),
            'koleurzmusic.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_519_',
                'SITE_URL' => 'http://www.koleurzmusic.com',
                'DOMAIN' => 'koleurzmusic.com',
            ),
            'kidsrkidscarrollton.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_528_',
                'SITE_URL' => 'http://www.kidsrkidscarrollton.com',
                'DOMAIN' => 'kidsrkidscarrollton.com',
            ),
            'truereligionjeanssusa.net' =>
            array(
                'TABLE_SCHEMA' => 'hnchangs_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1659_',
                'SITE_URL' => 'http://www.truereligionjeanssusa.net',
                'DOMAIN' => 'truereligionjeanssusa.net',
            ),
            'unitedfoxwoodscasinoworkers.org' =>
            array(
                'TABLE_SCHEMA' => 'hnchangs_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1676_',
                'SITE_URL' => 'http://www.unitedfoxwoodscasinoworkers.org',
                'DOMAIN' => 'unitedfoxwoodscasinoworkers.org',
            ),
            'ccofei.com' =>
            array(
                'TABLE_SCHEMA' => 'homesfor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1660_',
                'SITE_URL' => 'http://www.ccofei.com',
                'DOMAIN' => 'ccofei.com',
            ),
            'dumbbutrich.info' =>
            array(
                'TABLE_SCHEMA' => 'homesfor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1683_',
                'SITE_URL' => 'http://www.dumbbutrich.info',
                'DOMAIN' => 'dumbbutrich.info',
            ),
            'allthedumps.com' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1176_',
                'SITE_URL' => 'http://www.allthedumps.com',
                'DOMAIN' => 'allthedumps.com',
            ),
            'annualreport2010-11.com' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1177_',
                'SITE_URL' => 'http://www.annualreport2010-11.com',
                'DOMAIN' => 'annualreport2010-11.com',
            ),
            'artemroz.com' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1179_',
                'SITE_URL' => 'http://www.artemroz.com',
                'DOMAIN' => 'artemroz.com',
            ),
            'arkofrefugetabernacle.org' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1180_',
                'SITE_URL' => 'http://www.arkofrefugetabernacle.org',
                'DOMAIN' => 'arkofrefugetabernacle.org',
            ),
            'cheapmiaclarisonic.com' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1550_',
                'SITE_URL' => 'http://www.cheapmiaclarisonic.com',
                'DOMAIN' => 'cheapmiaclarisonic.com',
            ),
            'honkytonkhighway.info' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_870_',
                'SITE_URL' => 'http://www.honkytonkhighway.info',
                'DOMAIN' => 'honkytonkhighway.info',
            ),
            'pumps.ws' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_871_',
                'SITE_URL' => 'http://www.pumps.ws',
                'DOMAIN' => 'pumps.ws',
            ),
            'sissweets.net' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_872_',
                'SITE_URL' => 'http://www.sissweets.net',
                'DOMAIN' => 'sissweets.net',
            ),
            'salmonlake.org' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_873_',
                'SITE_URL' => 'http://www.salmonlake.org',
                'DOMAIN' => 'salmonlake.org',
            ),
            'obamaonisrael.org' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_874_',
                'SITE_URL' => 'http://www.obamaonisrael.org',
                'DOMAIN' => 'obamaonisrael.org',
            ),
            'locatesalesjobs.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1080_',
                'SITE_URL' => 'http://www.locatesalesjobs.com',
                'DOMAIN' => 'locatesalesjobs.com',
            ),
            'fyshoes.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1081_',
                'SITE_URL' => 'http://www.fyshoes.com',
                'DOMAIN' => 'fyshoes.com',
            ),
            'poker986.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1551_',
                'SITE_URL' => 'http://www.poker986.com',
                'DOMAIN' => 'poker986.com',
            ),
            'isdvf.org' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_496_',
                'SITE_URL' => 'http://www.isdvf.org',
                'DOMAIN' => 'isdvf.org',
            ),
            'intersport-travel.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_504_',
                'SITE_URL' => 'http://www.intersport-travel.com',
                'DOMAIN' => 'intersport-travel.com',
            ),
            'ipswichtownfootballclub.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_505_',
                'SITE_URL' => 'http://www.ipswichtownfootballclub.com',
                'DOMAIN' => 'ipswichtownfootballclub.com',
            ),
            'immigrationcz.org' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_521_',
                'SITE_URL' => 'http://www.immigrationcz.org',
                'DOMAIN' => 'immigrationcz.org',
            ),
            '997kjox.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_5_',
                'SITE_URL' => 'http://997kjox.com',
                'DOMAIN' => '997kjox.com',
            ),
            'advantageofinsurance.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_6_',
                'SITE_URL' => 'http://www.advantageofinsurance.com',
                'DOMAIN' => 'advantageofinsurance.com',
            ),
            'adsosmhu.org' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_7_',
                'SITE_URL' => 'http://adsosmhu.org',
                'DOMAIN' => 'adsosmhu.org',
            ),
            'aeroklubprijedor.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_8_',
                'SITE_URL' => 'http://aeroklubprijedor.com',
                'DOMAIN' => 'aeroklubprijedor.com',
            ),
            'hnczsm.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1082_',
                'SITE_URL' => 'http://www.hnczsm.com',
                'DOMAIN' => 'hnczsm.com',
            ),
            'funjet-travel.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1083_',
                'SITE_URL' => 'http://www.funjet-travel.com',
                'DOMAIN' => 'funjet-travel.com',
            ),
            'greengenllc.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1552_',
                'SITE_URL' => 'http://www.greengenllc.com',
                'DOMAIN' => 'greengenllc.com',
            ),
            'istanbulescortmisra.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_507_',
                'SITE_URL' => 'http://www.istanbulescortmisra.com',
                'DOMAIN' => 'istanbulescortmisra.com',
            ),
            'jadn.tv' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_513_',
                'SITE_URL' => 'http://www.jadn.tv',
                'DOMAIN' => 'jadn.tv',
            ),
            'juttaneumann-newyork.info' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_516_',
                'SITE_URL' => 'http://www.juttaneumann-newyork.info',
                'DOMAIN' => 'juttaneumann-newyork.info',
            ),
            'ispairportlimo.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_527_',
                'SITE_URL' => 'http://www.ispairportlimo.com',
                'DOMAIN' => 'ispairportlimo.com',
            ),
            'jewsgonemeshuga.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_532_',
                'SITE_URL' => 'http://www.jewsgonemeshuga.com',
                'DOMAIN' => 'jewsgonemeshuga.com',
            ),
            'cricketwife.com' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1078_',
                'SITE_URL' => 'http://www.cricketwife.com',
                'DOMAIN' => 'cricketwife.com',
            ),
            'firstdieting.com' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1079_',
                'SITE_URL' => 'http://www.firstdieting.com',
                'DOMAIN' => 'firstdieting.com',
            ),
            'buying-tax-free-newport-cigarettes.com' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1553_',
                'SITE_URL' => 'http://www.buying-tax-free-newport-cigarettes.com',
                'DOMAIN' => 'buying-tax-free-newport-cigarettes.com',
            ),
            'iacampsandretreats.org' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_498_',
                'SITE_URL' => 'http://www.iacampsandretreats.org',
                'DOMAIN' => 'iacampsandretreats.org',
            ),
            'hxedu365.org' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_500_',
                'SITE_URL' => 'http://www.hxedu365.org',
                'DOMAIN' => 'hxedu365.org',
            ),
            'iloveoldorchardbeach.com' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_501_',
                'SITE_URL' => 'http://www.iloveoldorchardbeach.com',
                'DOMAIN' => 'iloveoldorchardbeach.com',
            ),
            'ilmta15.org' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_510_',
                'SITE_URL' => 'http://www.ilmta15.org',
                'DOMAIN' => 'ilmta15.org',
            ),
            'icfmanagement.com' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_520_',
                'SITE_URL' => 'http://www.icfmanagement.com',
                'DOMAIN' => 'icfmanagement.com',
            ),
            'e-hospitalbeds.com' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1076_',
                'SITE_URL' => 'http://www.e-hospitalbeds.com',
                'DOMAIN' => 'e-hospitalbeds.com',
            ),
            'best-food-recipes.net' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1077_',
                'SITE_URL' => 'http://www.best-food-recipes.net',
                'DOMAIN' => 'best-food-recipes.net',
            ),
            'anytome.com' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1554_',
                'SITE_URL' => 'http://www.anytome.com',
                'DOMAIN' => 'anytome.com',
            ),
            'hicooking.com' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_506_',
                'SITE_URL' => 'http://www.hicooking.com',
                'DOMAIN' => 'hicooking.com',
            ),
            'homeownerswakeup.com' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_508_',
                'SITE_URL' => 'http://www.homeownerswakeup.com',
                'DOMAIN' => 'homeownerswakeup.com',
            ),
            'holistichealthcareinsurance.com' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_514_',
                'SITE_URL' => 'http://www.holistichealthcareinsurance.com',
                'DOMAIN' => 'holistichealthcareinsurance.com',
            ),
            'ht360.net' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_517_',
                'SITE_URL' => 'http://www.ht360.net',
                'DOMAIN' => 'ht360.net',
            ),
            'heizilanqiu.com' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_524_',
                'SITE_URL' => 'http://www.heizilanqiu.com',
                'DOMAIN' => 'heizilanqiu.com',
            ),
            'consumerhealthnet.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1073_',
                'SITE_URL' => 'http://www.consumerhealthnet.com',
                'DOMAIN' => 'consumerhealthnet.com',
            ),
            'beathousemusicgroup.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1555_',
                'SITE_URL' => 'http://www.beathousemusicgroup.com',
                'DOMAIN' => 'beathousemusicgroup.com',
            ),
            'golf-for-fun.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_497_',
                'SITE_URL' => 'http://www.golf-for-fun.com',
                'DOMAIN' => 'golf-for-fun.com',
            ),
            'goodwinfamilydentistry.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_509_',
                'SITE_URL' => 'http://www.goodwinfamilydentistry.com',
                'DOMAIN' => 'goodwinfamilydentistry.com',
            ),
            'nicoleluxuryestates.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_549_',
                'SITE_URL' => 'http://www.nicoleluxuryestates.com',
                'DOMAIN' => 'nicoleluxuryestates.com',
            ),
            'oohlytv.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_563_',
                'SITE_URL' => 'http://www.oohlytv.com',
                'DOMAIN' => 'oohlytv.com',
            ),
            'ykbyby.com' =>
            array(
                'TABLE_SCHEMA' => 'igreigra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1662_',
                'SITE_URL' => 'http://www.ykbyby.com',
                'DOMAIN' => 'ykbyby.com',
            ),
            'egyptdotnow.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_48_',
                'SITE_URL' => 'http://www.egyptdotnow.com',
                'DOMAIN' => 'egyptdotnow.com',
            ),
            'martinearle.me' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_49_',
                'SITE_URL' => 'http://www.martinearle.me',
                'DOMAIN' => 'martinearle.me',
            ),
            'xn--vck1fsa3852buxc48hh1wp62b7g0b.net' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_50_',
                'SITE_URL' => 'http://www.xn--vck1fsa3852buxc48hh1wp62b7g0b.net',
                'DOMAIN' => 'xn--vck1fsa3852buxc48hh1wp62b7g0b.net',
            ),
            'ifoundtheperfect.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_51_',
                'SITE_URL' => 'http://www.ifoundtheperfect.com',
                'DOMAIN' => 'ifoundtheperfect.com',
            ),
            'agadir-airport.info' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_52_',
                'SITE_URL' => 'http://www.agadir-airport.info',
                'DOMAIN' => 'agadir-airport.info',
            ),
            'bilgininsesi.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_53_',
                'SITE_URL' => 'http://www.bilgininsesi.com',
                'DOMAIN' => 'bilgininsesi.com',
            ),
            'humanobeing.org' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_55_',
                'SITE_URL' => 'http://www.humanobeing.org',
                'DOMAIN' => 'humanobeing.org',
            ),
            'e-renda.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_702_',
                'SITE_URL' => 'http://www.e-renda.com',
                'DOMAIN' => 'e-renda.com',
            ),
            'agu-eco-racing.info' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_703_',
                'SITE_URL' => 'http://www.agu-eco-racing.info',
                'DOMAIN' => 'agu-eco-racing.info',
            ),
            'executivecorrespondence.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_704_',
                'SITE_URL' => 'http://www.executivecorrespondence.com',
                'DOMAIN' => 'executivecorrespondence.com',
            ),
            'whitehouseclaremi.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_706_',
                'SITE_URL' => 'http://www.whitehouseclaremi.com',
                'DOMAIN' => 'whitehouseclaremi.com',
            ),
            'realestatehollywoodbeach.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1071_',
                'SITE_URL' => 'http://www.realestatehollywoodbeach.com',
                'DOMAIN' => 'realestatehollywoodbeach.com',
            ),
            'springcreekoaksnews.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1072_',
                'SITE_URL' => 'http://www.springcreekoaksnews.com',
                'DOMAIN' => 'springcreekoaksnews.com',
            ),
            'sabq.us' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1556_',
                'SITE_URL' => 'http://www.sabq.us',
                'DOMAIN' => 'sabq.us',
            ),
            'fotobrio.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_484_',
                'SITE_URL' => 'http://www.fotobrio.com',
                'DOMAIN' => 'fotobrio.com',
            ),
            'exclusiveconsultants.net' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_490_',
                'SITE_URL' => 'http://www.exclusiveconsultants.net',
                'DOMAIN' => 'exclusiveconsultants.net',
            ),
            'fellowshipregionalchurch.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_492_',
                'SITE_URL' => 'http://www.fellowshipregionalchurch.com',
                'DOMAIN' => 'fellowshipregionalchurch.com',
            ),
            'evannalynchbr.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_503_',
                'SITE_URL' => 'http://evannalynchbr.com/',
                'DOMAIN' => 'evannalynchbr.com',
            ),
            'newbornbabychildphotographer.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_562_',
                'SITE_URL' => 'http://www.newbornbabychildphotographer.com',
                'DOMAIN' => 'newbornbabychildphotographer.com',
            ),
            '618oakton-1.info' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1172_',
                'SITE_URL' => 'http://www.618oakton-1.info',
                'DOMAIN' => '618oakton-1.info',
            ),
            '60daysick.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1173_',
                'SITE_URL' => 'http://www.60daysick.com',
                'DOMAIN' => '60daysick.com',
            ),
            'agrorematescebollati.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1174_',
                'SITE_URL' => 'http://www.agrorematescebollati.com',
                'DOMAIN' => 'agrorematescebollati.com',
            ),
            'abbeyhultonweather.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1175_',
                'SITE_URL' => 'http://www.abbeyhultonweather.com',
                'DOMAIN' => 'abbeyhultonweather.com',
            ),
            'uggbootssaleonlinestore.net' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1557_',
                'SITE_URL' => 'http://www.uggbootssaleonlinestore.net',
                'DOMAIN' => 'uggbootssaleonlinestore.net',
            ),
            'ilikeprogramming.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_865_',
                'SITE_URL' => 'http://www.ilikeprogramming.com',
                'DOMAIN' => 'ilikeprogramming.com',
            ),
            'justicefactorxlegal.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_866_',
                'SITE_URL' => 'http://www.justicefactorxlegal.com',
                'DOMAIN' => 'justicefactorxlegal.com',
            ),
            'fsbenchmarks.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_867_',
                'SITE_URL' => 'http://www.fsbenchmarks.com',
                'DOMAIN' => 'fsbenchmarks.com',
            ),
            'zimnak.info' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_868_',
                'SITE_URL' => 'http://www.zimnak.info',
                'DOMAIN' => 'zimnak.info',
            ),
            'buckminsterfullerlive.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_869_',
                'SITE_URL' => 'http://www.buckminsterfullerlive.com',
                'DOMAIN' => 'buckminsterfullerlive.com',
            ),
            'mountknowledge.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1069_',
                'SITE_URL' => 'http://www.mountknowledge.com',
                'DOMAIN' => 'mountknowledge.com',
            ),
            'buying-tax-free-camel-cigarettes.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1558_',
                'SITE_URL' => 'http://www.buying-tax-free-camel-cigarettes.com',
                'DOMAIN' => 'buying-tax-free-camel-cigarettes.com',
            ),
            'electricavenuesf.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_489_',
                'SITE_URL' => 'http://www.electricavenuesf.com',
                'DOMAIN' => 'electricavenuesf.com',
            ),
            'long-islandhomeinspection.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_491_',
                'SITE_URL' => 'http://www.long-islandhomeinspection.com',
                'DOMAIN' => 'long-islandhomeinspection.com',
            ),
            'mcdialogue.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_493_',
                'SITE_URL' => 'http://www.mcdialogue.com',
                'DOMAIN' => 'mcdialogue.com',
            ),
            'eatmoreprotein.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_494_',
                'SITE_URL' => 'http://www.eatmoreprotein.com',
                'DOMAIN' => 'eatmoreprotein.com',
            ),
            'readrecipes.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1070_',
                'SITE_URL' => 'http://www.readrecipes.com',
                'DOMAIN' => 'readrecipes.com',
            ),
            'canadagoose2013sverige.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1559_',
                'SITE_URL' => 'http://www.canadagoose2013sverige.com',
                'DOMAIN' => 'canadagoose2013sverige.com',
            ),
            'emhimn.org' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_482_',
                'SITE_URL' => 'http://www.emhimn.org',
                'DOMAIN' => 'emhimn.org',
            ),
            'energia-spa.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_485_',
                'SITE_URL' => 'http://www.energia-spa.com',
                'DOMAIN' => 'energia-spa.com',
            ),
            'eticarett.net' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_486_',
                'SITE_URL' => 'http://www.eticarett.net',
                'DOMAIN' => 'eticarett.net',
            ),
            'escapadesafaris.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_495_',
                'SITE_URL' => 'http://www.escapadesafaris.com',
                'DOMAIN' => 'escapadesafaris.com',
            ),
            'eugenescottishfestival2012.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_499_',
                'SITE_URL' => 'http://www.eugenescottishfestival2012.com',
                'DOMAIN' => 'eugenescottishfestival2012.com',
            ),
            'myteamspeakhost.com' =>
            array(
                'TABLE_SCHEMA' => 'infra-as_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1065_',
                'SITE_URL' => 'http://www.myteamspeakhost.com',
                'DOMAIN' => 'myteamspeakhost.com',
            ),
            'weddingdaydiet.com' =>
            array(
                'TABLE_SCHEMA' => 'infra-as_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1066_',
                'SITE_URL' => 'http://www.weddingdaydiet.com',
                'DOMAIN' => 'weddingdaydiet.com',
            ),
            'forlook.biz' =>
            array(
                'TABLE_SCHEMA' => 'infra-as_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_474_',
                'SITE_URL' => 'http://www.forlook.biz',
                'DOMAIN' => 'forlook.biz',
            ),
            'fmherbalrx.net' =>
            array(
                'TABLE_SCHEMA' => 'infra-as_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_475_',
                'SITE_URL' => 'http://www.fmherbalrx.net',
                'DOMAIN' => 'fmherbalrx.net',
            ),
            'hauntedatchison.com' =>
            array(
                'TABLE_SCHEMA' => 'infra-as_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_476_',
                'SITE_URL' => 'http://www.hauntedatchison.com',
                'DOMAIN' => 'hauntedatchison.com',
            ),
            'grandentertainment.info' =>
            array(
                'TABLE_SCHEMA' => 'infra-as_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_478_',
                'SITE_URL' => 'http://www.grandentertainment.info',
                'DOMAIN' => 'grandentertainment.info',
            ),
            'gear-it-up.com' =>
            array(
                'TABLE_SCHEMA' => 'infra-as_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_479_',
                'SITE_URL' => 'http://www.gear-it-up.com',
                'DOMAIN' => 'gear-it-up.com',
            ),
            'weddingsbyrobin.com' =>
            array(
                'TABLE_SCHEMA' => 'inlet-le_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1067_',
                'SITE_URL' => 'http://www.weddingsbyrobin.com',
                'DOMAIN' => 'weddingsbyrobin.com',
            ),
            'waystomakemoneytutorials.com' =>
            array(
                'TABLE_SCHEMA' => 'inlet-le_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1068_',
                'SITE_URL' => 'http://www.waystomakemoneytutorials.com',
                'DOMAIN' => 'waystomakemoneytutorials.com',
            ),
            'ozgur-nakliyat.com' =>
            array(
                'TABLE_SCHEMA' => 'inlet-le_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1561_',
                'SITE_URL' => 'http://www.ozgur-nakliyat.com',
                'DOMAIN' => 'ozgur-nakliyat.com',
            ),
            'healingheartartistry.com' =>
            array(
                'TABLE_SCHEMA' => 'inlet-le_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_477_',
                'SITE_URL' => 'http://www.healingheartartistry.com',
                'DOMAIN' => 'healingheartartistry.com',
            ),
            'hphouse.net' =>
            array(
                'TABLE_SCHEMA' => 'inlet-le_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_480_',
                'SITE_URL' => 'http://www.hphouse.net',
                'DOMAIN' => 'hphouse.net',
            ),
            'jj1.me' =>
            array(
                'TABLE_SCHEMA' => 'inlet-le_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_483_',
                'SITE_URL' => 'http://www.jj1.me',
                'DOMAIN' => 'jj1.me',
            ),
            'lamatsultrimkhandro.com' =>
            array(
                'TABLE_SCHEMA' => 'inlet-le_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_487_',
                'SITE_URL' => 'http://www.lamatsultrimkhandro.com',
                'DOMAIN' => 'lamatsultrimkhandro.com',
            ),
            'hopepregnancyofnj.com' =>
            array(
                'TABLE_SCHEMA' => 'inlet-le_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_568_',
                'SITE_URL' => 'http://www.hopepregnancyofnj.com',
                'DOMAIN' => 'hopepregnancyofnj.com',
            ),
            'rottingcorpse.net' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1064_',
                'SITE_URL' => 'http://www.rottingcorpse.net',
                'DOMAIN' => 'rottingcorpse.net',
            ),
            'officialbroncosfanstore.com' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1562_',
                'SITE_URL' => 'http://www.officialbroncosfanstore.com',
                'DOMAIN' => 'officialbroncosfanstore.com',
            ),
            'eatologies.com' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_470_',
                'SITE_URL' => 'http://www.eatologies.com',
                'DOMAIN' => 'eatologies.com',
            ),
            'exposedacnereviews.org' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_471_',
                'SITE_URL' => 'http://www.exposedacnereviews.org',
                'DOMAIN' => 'exposedacnereviews.org',
            ),
            'facemerk.com' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_472_',
                'SITE_URL' => 'http://www.facemerk.com',
                'DOMAIN' => 'facemerk.com',
            ),
            'femsa.us' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_473_',
                'SITE_URL' => 'http://www.femsa.us',
                'DOMAIN' => 'femsa.us',
            ),
            'mortgagefocus.org' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_555_',
                'SITE_URL' => 'http://www.mortgagefocus.org',
                'DOMAIN' => 'mortgagefocus.org',
            ),
            'txqw.net' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1063_',
                'SITE_URL' => 'http://www.txqw.net',
                'DOMAIN' => 'txqw.net',
            ),
            'energyefficiencypsi.com' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1563_',
                'SITE_URL' => 'http://www.energyefficiencypsi.com',
                'DOMAIN' => 'energyefficiencypsi.com',
            ),
            'londonseomarketing.com' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_537_',
                'SITE_URL' => 'http://www.londonseomarketing.com',
                'DOMAIN' => 'londonseomarketing.com',
            ),
            'richardkainmarketing.com' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_556_',
                'SITE_URL' => 'http://www.richardkainmarketing.com',
                'DOMAIN' => 'richardkainmarketing.com',
            ),
            'powerbiblechurch.org' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_561_',
                'SITE_URL' => 'http://www.powerbiblechurch.org',
                'DOMAIN' => 'powerbiblechurch.org',
            ),
            'radianthomeloans.com' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_567_',
                'SITE_URL' => 'http://www.radianthomeloans.com',
                'DOMAIN' => 'radianthomeloans.com',
            ),
            'rockthemansion2012.com' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_571_',
                'SITE_URL' => 'http://www.rockthemansion2012.com',
                'DOMAIN' => 'rockthemansion2012.com',
            ),
            'neuralstudios.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1059_',
                'SITE_URL' => 'http://www.neuralstudios.com',
                'DOMAIN' => 'neuralstudios.com',
            ),
            'realfitnessresults.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1060_',
                'SITE_URL' => 'http://www.realfitnessresults.com',
                'DOMAIN' => 'realfitnessresults.com',
            ),
            'premioroche.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1564_',
                'SITE_URL' => 'http://www.premioroche.com',
                'DOMAIN' => 'premioroche.com',
            ),
            'bombedoutradio.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_435_',
                'SITE_URL' => 'http://www.bombedoutradio.com',
                'DOMAIN' => 'bombedoutradio.com',
            ),
            'c8social.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_441_',
                'SITE_URL' => 'http://www.c8social.com',
                'DOMAIN' => 'c8social.com',
            ),
            'cleanol.mobi' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_451_',
                'SITE_URL' => 'http://www.cleanol.mobi',
                'DOMAIN' => 'cleanol.mobi',
            ),
            'brainevolution.org' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_458_',
                'SITE_URL' => 'http://www.brainevolution.org',
                'DOMAIN' => 'brainevolution.org',
            ),
            'sinfullywholesomearganoil.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_462_',
                'SITE_URL' => 'http://www.sinfullywholesomearganoil.com',
                'DOMAIN' => 'sinfullywholesomearganoil.com',
            ),
            'thepurpleelephantantiques.com' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1061_',
                'SITE_URL' => 'http://www.thepurpleelephantantiques.com',
                'DOMAIN' => 'thepurpleelephantantiques.com',
            ),
            'avtarinc.com' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1062_',
                'SITE_URL' => 'http://www.avtarinc.com',
                'DOMAIN' => 'avtarinc.com',
            ),
            'ayudafoundation.org' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1565_',
                'SITE_URL' => 'http://www.ayudafoundation.org',
                'DOMAIN' => 'ayudafoundation.org',
            ),
            'deitchwoodwindworkshop.com' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_445_',
                'SITE_URL' => 'http://www.deitchwoodwindworkshop.com',
                'DOMAIN' => 'deitchwoodwindworkshop.com',
            ),
            'fujairahmediaacademy.com' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_452_',
                'SITE_URL' => 'http://www.fujairahmediaacademy.com',
                'DOMAIN' => 'fujairahmediaacademy.com',
            ),
            'financialleadership.net' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_456_',
                'SITE_URL' => 'http://www.financialleadership.net',
                'DOMAIN' => 'financialleadership.net',
            ),
            'cnohaiti.org' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_459_',
                'SITE_URL' => 'http://www.cnohaiti.org',
                'DOMAIN' => 'cnohaiti.org',
            ),
            'shopquench.com' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_465_',
                'SITE_URL' => 'http://www.shopquench.com',
                'DOMAIN' => 'shopquench.com',
            ),
            'greenwichconstruct.com' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1055_',
                'SITE_URL' => 'http://www.greenwichconstruct.com',
                'DOMAIN' => 'greenwichconstruct.com',
            ),
            'flowercompanyinc.com' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1056_',
                'SITE_URL' => 'http://www.flowercompanyinc.com',
                'DOMAIN' => 'flowercompanyinc.com',
            ),
            '269o.com' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1566_',
                'SITE_URL' => 'http://www.269o.com',
                'DOMAIN' => '269o.com',
            ),
            'wearethefuture.us' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_436_',
                'SITE_URL' => 'http://www.wearethefuture.us',
                'DOMAIN' => 'wearethefuture.us',
            ),
            'thewunder.org' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_438_',
                'SITE_URL' => 'http://www.thewunder.org',
                'DOMAIN' => 'thewunder.org',
            ),
            'thewomaninthechild.info' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_439_',
                'SITE_URL' => 'http://www.thewomaninthechild.info',
                'DOMAIN' => 'thewomaninthechild.info',
            ),
            'verizonwirelessstores.net' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_440_',
                'SITE_URL' => 'http://www.verizonwirelessstores.net',
                'DOMAIN' => 'verizonwirelessstores.net',
            ),
            'occupyfairhope.com' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_460_',
                'SITE_URL' => 'http://www.occupyfairhope.com',
                'DOMAIN' => 'occupyfairhope.com',
            ),
            'your-pets-health-care.com' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1047_',
                'SITE_URL' => 'http://www.your-pets-health-care.com',
                'DOMAIN' => 'your-pets-health-care.com',
            ),
            'rainfireprotection.com' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1459_',
                'SITE_URL' => 'http://www.rainfireprotection.com',
                'DOMAIN' => 'rainfireprotection.com',
            ),
            'premioresponsabilidadambiental.org' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1667_',
                'SITE_URL' => 'http://www.premioresponsabilidadambiental.org',
                'DOMAIN' => 'premioresponsabilidadambiental.org',
            ),
            'portervilleevents.com' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_392_',
                'SITE_URL' => 'http://www.portervilleevents.com',
                'DOMAIN' => 'portervilleevents.com',
            ),
            'poklibpcc.org' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_418_',
                'SITE_URL' => 'http://www.poklibpcc.org',
                'DOMAIN' => 'poklibpcc.org',
            ),
            'perfectpartyplannerfl.com' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_422_',
                'SITE_URL' => 'http://www.perfectpartyplannerfl.com',
                'DOMAIN' => 'perfectpartyplannerfl.com',
            ),
            'moodyafbchapel.org' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_453_',
                'SITE_URL' => 'http://www.moodyafbchapel.org',
                'DOMAIN' => 'moodyafbchapel.org',
            ),
            'moodychapel.org' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_454_',
                'SITE_URL' => 'http://www.moodychapel.org',
                'DOMAIN' => 'moodychapel.org',
            ),
            'horsetackforyou.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1057_',
                'SITE_URL' => 'http://www.horsetackforyou.com',
                'DOMAIN' => 'horsetackforyou.com',
            ),
            'mp3indirek.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1058_',
                'SITE_URL' => 'http://www.mp3indirek.com',
                'DOMAIN' => 'mp3indirek.com',
            ),
            'iwojimatribute.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1567_',
                'SITE_URL' => 'http://www.iwojimatribute.com',
                'DOMAIN' => 'iwojimatribute.com',
            ),
            'yhmap1129.info' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_415_',
                'SITE_URL' => 'http://www.yhmap1129.info',
                'DOMAIN' => 'yhmap1129.info',
            ),
            'optika-sasa.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_442_',
                'SITE_URL' => 'http://www.optika-sasa.com',
                'DOMAIN' => 'optika-sasa.com',
            ),
            'adventistwomensblog.org' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_443_',
                'SITE_URL' => 'http://www.adventistwomensblog.org',
                'DOMAIN' => 'adventistwomensblog.org',
            ),
            'ynsee.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_447_',
                'SITE_URL' => 'http://www.ynsee.com',
                'DOMAIN' => 'ynsee.com',
            ),
            'alexdavis.us' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_457_',
                'SITE_URL' => 'http://www.alexdavis.us',
                'DOMAIN' => 'alexdavis.us',
            ),
            'eqxdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1054_',
                'SITE_URL' => 'http://www.eqxdesign.com',
                'DOMAIN' => 'eqxdesign.com',
            ),
            'saskping.org' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1568_',
                'SITE_URL' => 'http://www.saskping.org',
                'DOMAIN' => 'saskping.org',
            ),
            'trabajosyexamenes.net' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_430_',
                'SITE_URL' => 'http://www.trabajosyexamenes.net',
                'DOMAIN' => 'trabajosyexamenes.net',
            ),
            'thenoahsphere.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_431_',
                'SITE_URL' => 'http://www.thenoahsphere.com',
                'DOMAIN' => 'thenoahsphere.com',
            ),
            'thesinghs.biz' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_437_',
                'SITE_URL' => 'http://www.thesinghs.biz',
                'DOMAIN' => 'thesinghs.biz',
            ),
            'web-hero.net' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_446_',
                'SITE_URL' => 'http://www.web-hero.net',
                'DOMAIN' => 'web-hero.net',
            ),
            'ntierdata.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_464_',
                'SITE_URL' => 'http://www.ntierdata.com',
                'DOMAIN' => 'ntierdata.com',
            ),
            'comebackin.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_42_',
                'SITE_URL' => 'http://www.comebackin.com',
                'DOMAIN' => 'comebackin.com',
            ),
            'savannahervin.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_43_',
                'SITE_URL' => 'http://www.savannahervin.com',
                'DOMAIN' => 'savannahervin.com',
            ),
            'fishingcanadablog.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_44_',
                'SITE_URL' => 'http://www.fishingcanadablog.com',
                'DOMAIN' => 'fishingcanadablog.com',
            ),
            'teabeans.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_45_',
                'SITE_URL' => 'http://www.teabeans.com',
                'DOMAIN' => 'teabeans.com',
            ),
            '1000plus.me' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_46_',
                'SITE_URL' => 'http://www.1000plus.me',
                'DOMAIN' => '1000plus.me',
            ),
            'questionsandcocktails.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_47_',
                'SITE_URL' => 'http://www.questionsandcocktails.com',
                'DOMAIN' => 'questionsandcocktails.com',
            ),
            'terroirstudio.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_82_',
                'SITE_URL' => 'http://www.terroirstudio.com',
                'DOMAIN' => 'terroirstudio.com',
            ),
            'aguilasdemexico.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1052_',
                'SITE_URL' => 'http://www.aguilasdemexico.com',
                'DOMAIN' => 'aguilasdemexico.com',
            ),
            'lifeandlivingcoaching.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1053_',
                'SITE_URL' => 'http://www.lifeandlivingcoaching.com',
                'DOMAIN' => 'lifeandlivingcoaching.com',
            ),
            'dzpkb.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1569_',
                'SITE_URL' => 'http://www.dzpkb.com',
                'DOMAIN' => 'dzpkb.com',
            ),
            'xn--eckvc2a7gj5g6832ahd9e.net' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_427_',
                'SITE_URL' => 'http://www.xn--eckvc2a7gj5g6832ahd9e.net',
                'DOMAIN' => 'xn--eckvc2a7gj5g6832ahd9e.net',
            ),
            'venezuelaspeaks.org' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_429_',
                'SITE_URL' => 'http://www.venezuelaspeaks.org',
                'DOMAIN' => 'venezuelaspeaks.org',
            ),
            'victoryforchangeparty.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_432_',
                'SITE_URL' => 'http://www.victoryforchangeparty.com',
                'DOMAIN' => 'victoryforchangeparty.com',
            ),
            'zahrady-fotogalerie.info' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_433_',
                'SITE_URL' => 'http://www.zahrady-fotogalerie.info',
                'DOMAIN' => 'zahrady-fotogalerie.info',
            ),
            'zen1th.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_434_',
                'SITE_URL' => 'http://www.zen1th.com',
                'DOMAIN' => 'zen1th.com',
            ),
            'bcforms.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_17_',
                'SITE_URL' => 'http://www.bcforms.com',
                'DOMAIN' => 'bcforms.com',
            ),
            'bike-gear.ws' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_20_',
                'SITE_URL' => 'http://bike-gear.ws',
                'DOMAIN' => 'bike-gear.ws',
            ),
            'blackdragonarts.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_21_',
                'SITE_URL' => 'http://blackdragonarts.com',
                'DOMAIN' => 'blackdragonarts.com',
            ),
            'bigmessmedia.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_4_',
                'SITE_URL' => 'http://bigmessmedia.com',
                'DOMAIN' => 'bigmessmedia.com',
            ),
            'fjjinlin.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1570_',
                'SITE_URL' => 'http://www.fjjinlin.com',
                'DOMAIN' => 'fjjinlin.com',
            ),
            'ritzandrevelry.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_410_',
                'SITE_URL' => 'http://www.ritzandrevelry.com',
                'DOMAIN' => 'ritzandrevelry.com',
            ),
            'roofingdistributor.biz' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_416_',
                'SITE_URL' => 'http://www.roofingdistributor.biz',
                'DOMAIN' => 'roofingdistributor.biz',
            ),
            'recyclobin.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_419_',
                'SITE_URL' => 'http://www.recyclobin.com',
                'DOMAIN' => 'recyclobin.com',
            ),
            'new-bmw-335d-sedan.info' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_455_',
                'SITE_URL' => 'http://www.new-bmw-335d-sedan.info',
                'DOMAIN' => 'new-bmw-335d-sedan.info',
            ),
            'njsafes.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_463_',
                'SITE_URL' => 'http://www.njsafes.com',
                'DOMAIN' => 'njsafes.com',
            ),
            'china-ecommerce-logistics.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1157_',
                'SITE_URL' => 'http://www.china-ecommerce-logistics.com',
                'DOMAIN' => 'china-ecommerce-logistics.com',
            ),
            'choyleefutnorthamerica.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1158_',
                'SITE_URL' => 'http://www.choyleefutnorthamerica.com',
                'DOMAIN' => 'choyleefutnorthamerica.com',
            ),
            'coolridesfabrication.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1159_',
                'SITE_URL' => 'http://www.coolridesfabrication.com',
                'DOMAIN' => 'coolridesfabrication.com',
            ),
            'constructionattorneys.biz' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1160_',
                'SITE_URL' => 'http://www.constructionattorneys.biz',
                'DOMAIN' => 'constructionattorneys.biz',
            ),
            'crossfit-rio.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1163_',
                'SITE_URL' => 'http://www.crossfit-rio.com',
                'DOMAIN' => 'crossfit-rio.com',
            ),
            'fitium-reviews.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1571_',
                'SITE_URL' => 'http://www.fitium-reviews.com',
                'DOMAIN' => 'fitium-reviews.com',
            ),
            'jabirds.org' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_851_',
                'SITE_URL' => 'http://www.jabirds.org',
                'DOMAIN' => 'jabirds.org',
            ),
            'gpcounselorsforchange.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_852_',
                'SITE_URL' => 'http://www.gpcounselorsforchange.com',
                'DOMAIN' => 'gpcounselorsforchange.com',
            ),
            'richardtaylorfor50thdistrictjudge.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_853_',
                'SITE_URL' => 'http://www.richardtaylorfor50thdistrictjudge.com',
                'DOMAIN' => 'richardtaylorfor50thdistrictjudge.com',
            ),
            'createmyfirstwebsite.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_855_',
                'SITE_URL' => 'http://www.createmyfirstwebsite.com',
                'DOMAIN' => 'createmyfirstwebsite.com',
            ),
            'a1bargainprice.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1050_',
                'SITE_URL' => 'http://www.a1bargainprice.com',
                'DOMAIN' => 'a1bargainprice.com',
            ),
            'hqsoftconsult.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1051_',
                'SITE_URL' => 'http://www.hqsoftconsult.com',
                'DOMAIN' => 'hqsoftconsult.com',
            ),
            'hoopilitown.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1572_',
                'SITE_URL' => 'http://www.hoopilitown.com',
                'DOMAIN' => 'hoopilitown.com',
            ),
            'uwclubhockey.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_400_',
                'SITE_URL' => 'http://www.uwclubhockey.com',
                'DOMAIN' => 'uwclubhockey.com',
            ),
            'tacotrail.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_411_',
                'SITE_URL' => 'http://www.tacotrail.com',
                'DOMAIN' => 'tacotrail.com',
            ),
            'thejudassyndrome.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_421_',
                'SITE_URL' => 'http://www.thejudassyndrome.com',
                'DOMAIN' => 'thejudassyndrome.com',
            ),
            'towelbathrobe.net' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_424_',
                'SITE_URL' => 'http://www.towelbathrobe.net',
                'DOMAIN' => 'towelbathrobe.net',
            ),
            'uscpfa-wr.org' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_428_',
                'SITE_URL' => 'http://www.uscpfa-wr.org',
                'DOMAIN' => 'uscpfa-wr.org',
            ),
            'markvasakat.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_707_',
                'SITE_URL' => 'http://www.markvasakat.com',
                'DOMAIN' => 'markvasakat.com',
            ),
            'reflectionsoflifephotography.org' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_708_',
                'SITE_URL' => 'http://www.reflectionsoflifephotography.org',
                'DOMAIN' => 'reflectionsoflifephotography.org',
            ),
            'jussphotos.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_709_',
                'SITE_URL' => 'http://www.jussphotos.com',
                'DOMAIN' => 'jussphotos.com',
            ),
            'alisapeek.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_710_',
                'SITE_URL' => 'http://www.alisapeek.com',
                'DOMAIN' => 'alisapeek.com',
            ),
            'matthewbraney.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_711_',
                'SITE_URL' => 'http://www.matthewbraney.com',
                'DOMAIN' => 'matthewbraney.com',
            ),
            'elections2010.info' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1049_',
                'SITE_URL' => 'http://www.elections2010.info',
                'DOMAIN' => 'elections2010.info',
            ),
            'stayhoo.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1573_',
                'SITE_URL' => 'http://www.stayhoo.com',
                'DOMAIN' => 'stayhoo.com',
            ),
            'stellasavestheday.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_414_',
                'SITE_URL' => 'http://www.stellasavestheday.com',
                'DOMAIN' => 'stellasavestheday.com',
            ),
            'suclumuyum.org' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_420_',
                'SITE_URL' => 'http://www.suclumuyum.org',
                'DOMAIN' => 'suclumuyum.org',
            ),
            'scsgschools.org' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_423_',
                'SITE_URL' => 'http://www.scsgschools.org',
                'DOMAIN' => 'scsgschools.org',
            ),
            'sissycollective.org' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_425_',
                'SITE_URL' => 'http://www.sissycollective.org',
                'DOMAIN' => 'sissycollective.org',
            ),
            'soularch.info' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_426_',
                'SITE_URL' => 'http://www.soularch.info',
                'DOMAIN' => 'soularch.info',
            ),
            'jezam.biz' =>
            array(
                'TABLE_SCHEMA' => 'jezam_tkp',
                'TABLE_PREFIX' => 'wp_pkp1_1729_',
                'SITE_URL' => 'http://www.jezam.biz',
                'DOMAIN' => 'jezam.biz',
            ),
            'wenyiwang.net' =>
            array(
                'TABLE_SCHEMA' => 'jezam_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1661_',
                'SITE_URL' => 'http://www.wenyiwang.net',
                'DOMAIN' => 'wenyiwang.net',
            ),
            'cheap-health-insurance-quote.com' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1045_',
                'SITE_URL' => 'http://www.cheap-health-insurance-quote.com',
                'DOMAIN' => 'cheap-health-insurance-quote.com',
            ),
            'tempehpf.com' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1046_',
                'SITE_URL' => 'http://www.tempehpf.com',
                'DOMAIN' => 'tempehpf.com',
            ),
            'apaliceo.org' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1574_',
                'SITE_URL' => 'http://www.apaliceo.org',
                'DOMAIN' => 'apaliceo.org',
            ),
            'ofmap1067.info' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_395_',
                'SITE_URL' => 'http://www.ofmap1067.info',
                'DOMAIN' => 'ofmap1067.info',
            ),
            'nonprofitpathfinder.org' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_407_',
                'SITE_URL' => 'http://www.nonprofitpathfinder.org',
                'DOMAIN' => 'nonprofitpathfinder.org',
            ),
            'osuntrc.org' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_408_',
                'SITE_URL' => 'http://www.osuntrc.org',
                'DOMAIN' => 'osuntrc.org',
            ),
            'ohiocea.biz' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_413_',
                'SITE_URL' => 'http://www.ohiocea.biz',
                'DOMAIN' => 'ohiocea.biz',
            ),
            'pattinson-online.com' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_417_',
                'SITE_URL' => 'http://www.pattinson-online.com',
                'DOMAIN' => 'pattinson-online.com',
            ),
            'thatonefilmblog.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1146_',
                'SITE_URL' => 'http://www.thatonefilmblog.com',
                'DOMAIN' => 'thatonefilmblog.com',
            ),
            '5libertyroad.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1147_',
                'SITE_URL' => 'http://www.5libertyroad.com',
                'DOMAIN' => '5libertyroad.com',
            ),
            'guardingheartsalliance.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1148_',
                'SITE_URL' => 'http://www.guardingheartsalliance.com',
                'DOMAIN' => 'guardingheartsalliance.com',
            ),
            'dewiyuhana.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1149_',
                'SITE_URL' => 'http://www.dewiyuhana.com',
                'DOMAIN' => 'dewiyuhana.com',
            ),
            'rotaryclubofplacencia.org' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1575_',
                'SITE_URL' => 'http://www.rotaryclubofplacencia.org',
                'DOMAIN' => 'rotaryclubofplacencia.org',
            ),
            'kamaymacollege.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_924_',
                'SITE_URL' => 'http://www.kamaymacollege.com',
                'DOMAIN' => 'kamaymacollege.com',
            ),
            'mykickasslife.me' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_925_',
                'SITE_URL' => 'http://www.mykickasslife.me',
                'DOMAIN' => 'mykickasslife.me',
            ),
            'marcelvreuls.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_926_',
                'SITE_URL' => 'http://www.marcelvreuls.com',
                'DOMAIN' => 'marcelvreuls.com',
            ),
            'zgwne.info' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_927_',
                'SITE_URL' => 'http://www.zgwne.info',
                'DOMAIN' => 'zgwne.info',
            ),
            'unitedafricancongress.org' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_928_',
                'SITE_URL' => 'http://www.unitedafricancongress.org',
                'DOMAIN' => 'unitedafricancongress.org',
            ),
            'puckethotels.com' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_270_',
                'SITE_URL' => 'http://puckethotels.com',
                'DOMAIN' => 'puckethotels.com',
            ),
            'pycca.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_271_',
                'SITE_URL' => 'http://www.pycca.info',
                'DOMAIN' => 'pycca.info',
            ),
            'reelclips.tv' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_272_',
                'SITE_URL' => 'http://reelclips.tv',
                'DOMAIN' => 'reelclips.tv',
            ),
            'ricoaviation.us' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_273_',
                'SITE_URL' => 'http://www.ricoaviation.us',
                'DOMAIN' => 'ricoaviation.us',
            ),
            'davidortegaphotography.com' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1041_',
                'SITE_URL' => 'http://www.davidortegaphotography.com',
                'DOMAIN' => 'davidortegaphotography.com',
            ),
            'baycolattorneys.com' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1042_',
                'SITE_URL' => 'http://www.baycolattorneys.com',
                'DOMAIN' => 'baycolattorneys.com',
            ),
            'ehns2010-athens.com' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1576_',
                'SITE_URL' => 'http://www.ehns2010-athens.com',
                'DOMAIN' => 'ehns2010-athens.com',
            ),
            'nflatlantafalconsjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_398_',
                'SITE_URL' => 'http://www.nflatlantafalconsjerseysusa.info',
                'DOMAIN' => 'nflatlantafalconsjerseysusa.info',
            ),
            'nfldallascowboysjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_401_',
                'SITE_URL' => 'http://www.nfldallascowboysjerseysusa.info',
                'DOMAIN' => 'nfldallascowboysjerseysusa.info',
            ),
            'new-toyota-tacoma-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_402_',
                'SITE_URL' => 'http://www.new-toyota-tacoma-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-tacoma-rockland-county-ny.info',
            ),
            'new-toyota-venza-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_404_',
                'SITE_URL' => 'http://www.new-toyota-venza-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-venza-rockland-county-ny.info',
            ),
            'new-toyota-tundra-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_409_',
                'SITE_URL' => 'http://www.new-toyota-tundra-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-tundra-rockland-county-ny.info',
            ),
            'baycollawyer.com' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1043_',
                'SITE_URL' => 'http://www.baycollawyer.com',
                'DOMAIN' => 'baycollawyer.com',
            ),
            'binocularsonlineguide.com' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1044_',
                'SITE_URL' => 'http://www.binocularsonlineguide.com',
                'DOMAIN' => 'binocularsonlineguide.com',
            ),
            'nfltennesseetitansjerseysus.info' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1458_',
                'SITE_URL' => 'http://www.nfltennesseetitansjerseysus.info',
                'DOMAIN' => 'nfltennesseetitansjerseysus.info',
            ),
            'sjbclub.com' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1578_',
                'SITE_URL' => 'http://www.sjbclub.com',
                'DOMAIN' => 'sjbclub.com',
            ),
            'nfldetroitlionsjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_399_',
                'SITE_URL' => 'http://www.nfldetroitlionsjerseysusa.info',
                'DOMAIN' => 'nfldetroitlionsjerseysusa.info',
            ),
            'nflmiamidolphinsjerseysus.info' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_405_',
                'SITE_URL' => 'http://www.nflmiamidolphinsjerseysus.info',
                'DOMAIN' => 'nflmiamidolphinsjerseysus.info',
            ),
            'nflpittsburghsteelersjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_406_',
                'SITE_URL' => 'http://www.nflpittsburghsteelersjerseysusa.info',
                'DOMAIN' => 'nflpittsburghsteelersjerseysusa.info',
            ),
            'nflnewyorkjetsjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_412_',
                'SITE_URL' => 'http://www.nflnewyorkjetsjerseysusa.info',
                'DOMAIN' => 'nflnewyorkjetsjerseysusa.info',
            ),
            'maverickcountyprecinct1.com' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_461_',
                'SITE_URL' => 'http://www.maverickcountyprecinct1.com',
                'DOMAIN' => 'maverickcountyprecinct1.com',
            ),
            'accomplisheddesigns.com' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1040_',
                'SITE_URL' => 'http://www.accomplisheddesigns.com',
                'DOMAIN' => 'accomplisheddesigns.com',
            ),
            'powerofslow.org' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1577_',
                'SITE_URL' => 'http://www.powerofslow.org',
                'DOMAIN' => 'powerofslow.org',
            ),
            'new-toyota-matrix-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_382_',
                'SITE_URL' => 'http://www.new-toyota-matrix-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-matrix-rockland-county-ny.info',
            ),
            'new-toyota-sequoia-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_390_',
                'SITE_URL' => 'http://www.new-toyota-sequoia-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-sequoia-rockland-county-ny.info',
            ),
            'new-toyota-prius-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_391_',
                'SITE_URL' => 'http://www.new-toyota-prius-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-prius-rockland-county-ny.info',
            ),
            'new-toyota-sienna-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_403_',
                'SITE_URL' => 'http://www.new-toyota-sienna-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-sienna-rockland-county-ny.info',
            ),
            'masschems.com' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_449_',
                'SITE_URL' => 'http://www.masschems.com',
                'DOMAIN' => 'masschems.com',
            ),
            'i-love-psp.com' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_70_',
                'SITE_URL' => 'http://i-love-psp.com',
                'DOMAIN' => 'i-love-psp.com',
            ),
            'ibeinspired.com' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_71_',
                'SITE_URL' => 'http://ibeinspired.com',
                'DOMAIN' => 'ibeinspired.com',
            ),
            'idafladen.com' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_72_',
                'SITE_URL' => 'http://idafladen.com',
                'DOMAIN' => 'idafladen.com',
            ),
            'infoproductsliquidators.com' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_73_',
                'SITE_URL' => 'http://infoproductsliquidators.com',
                'DOMAIN' => 'infoproductsliquidators.com',
            ),
            'injury-lawyers-ny.com' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1038_',
                'SITE_URL' => 'http://www.injury-lawyers-ny.com',
                'DOMAIN' => 'injury-lawyers-ny.com',
            ),
            'outlawsaloon.org' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1039_',
                'SITE_URL' => 'http://www.outlawsaloon.org',
                'DOMAIN' => 'outlawsaloon.org',
            ),
            'tsjsgc.com' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1579_',
                'SITE_URL' => 'http://www.tsjsgc.com',
                'DOMAIN' => 'tsjsgc.com',
            ),
            'new-toyota-fj-cruiser-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_393_',
                'SITE_URL' => 'http://www.new-toyota-fj-cruiser-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-fj-cruiser-rockland-county-ny.info',
            ),
            'new-toyota-camry-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_394_',
                'SITE_URL' => 'http://www.new-toyota-camry-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-camry-rockland-county-ny.info',
            ),
            'new-toyota-land-cruiser-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_396_',
                'SITE_URL' => 'http://www.new-toyota-land-cruiser-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-land-cruiser-rockland-county-ny.info',
            ),
            'new-toyota-corolla-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_397_',
                'SITE_URL' => 'http://www.new-toyota-corolla-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-corolla-rockland-county-ny.info',
            ),
            'new-toyota-highlander-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_468_',
                'SITE_URL' => 'http://www.new-toyota-highlander-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-highlander-rockland-county-ny.info',
            ),
            'fatherhoodonline.com' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1036_',
                'SITE_URL' => 'http://www.fatherhoodonline.com',
                'DOMAIN' => 'fatherhoodonline.com',
            ),
            'hostingandbeyond.com' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1037_',
                'SITE_URL' => 'http://www.hostingandbeyond.com',
                'DOMAIN' => 'hostingandbeyond.com',
            ),
            'kimberlynicolefoster.com' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1580_',
                'SITE_URL' => 'http://www.kimberlynicolefoster.com',
                'DOMAIN' => 'kimberlynicolefoster.com',
            ),
            'nbadallasmavericks.info' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_376_',
                'SITE_URL' => 'http://www.nbadallasmavericks.info',
                'DOMAIN' => 'nbadallasmavericks.info',
            ),
            'nba-san-antonio-spurs-jerseys.info' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_378_',
                'SITE_URL' => 'http://www.nba-san-antonio-spurs-jerseys.info',
                'DOMAIN' => 'nba-san-antonio-spurs-jerseys.info',
            ),
            'nba-san-antonio-spurs.info' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_383_',
                'SITE_URL' => 'http://www.nba-san-antonio-spurs.info',
                'DOMAIN' => 'nba-san-antonio-spurs.info',
            ),
            'mykaef.org' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_387_',
                'SITE_URL' => 'http://www.mykaef.org',
                'DOMAIN' => 'mykaef.org',
            ),
            'new-toyota-avalon-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_389_',
                'SITE_URL' => 'http://www.new-toyota-avalon-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-avalon-rockland-county-ny.info',
            ),
            'healthyrecoveryformula.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1034_',
                'SITE_URL' => 'http://www.healthyrecoveryformula.com',
                'DOMAIN' => 'healthyrecoveryformula.com',
            ),
            'lockportwebdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1035_',
                'SITE_URL' => 'http://www.lockportwebdesign.com',
                'DOMAIN' => 'lockportwebdesign.com',
            ),
            'theverybestme.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1581_',
                'SITE_URL' => 'http://www.theverybestme.com',
                'DOMAIN' => 'theverybestme.com',
            ),
            'soundforus.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_380_',
                'SITE_URL' => 'http://www.soundforus.com',
                'DOMAIN' => 'soundforus.com',
            ),
            'montrealtweetup.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_384_',
                'SITE_URL' => 'http://www.montrealtweetup.com',
                'DOMAIN' => 'montrealtweetup.com',
            ),
            'sugaranni.info' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_385_',
                'SITE_URL' => 'http://www.sugaranni.info',
                'DOMAIN' => 'sugaranni.info',
            ),
            'startingoutsideways.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_388_',
                'SITE_URL' => 'http://www.startingoutsideways.com',
                'DOMAIN' => 'startingoutsideways.com',
            ),
            'luxuryshoreproperties.biz' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_448_',
                'SITE_URL' => 'http://www.luxuryshoreproperties.biz',
                'DOMAIN' => 'luxuryshoreproperties.biz',
            ),
            'refinance-internet.com' =>
            array(
                'TABLE_SCHEMA' => 'lighthou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1031_',
                'SITE_URL' => 'http://www.refinance-internet.com',
                'DOMAIN' => 'refinance-internet.com',
            ),
            'marxismovivo.org' =>
            array(
                'TABLE_SCHEMA' => 'lighthou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1032_',
                'SITE_URL' => 'http://www.marxismovivo.org',
                'DOMAIN' => 'marxismovivo.org',
            ),
            'jsunphotography.com' =>
            array(
                'TABLE_SCHEMA' => 'lighthou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1583_',
                'SITE_URL' => 'http://www.jsunphotography.com',
                'DOMAIN' => 'jsunphotography.com',
            ),
            'pecmi.org' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1033_',
                'SITE_URL' => 'http://www.pecmi.org',
                'DOMAIN' => 'pecmi.org',
            ),
            'autinspectors.com' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1582_',
                'SITE_URL' => 'http://www.autinspectors.com',
                'DOMAIN' => 'autinspectors.com',
            ),
            'simplysocialmedia.me' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_375_',
                'SITE_URL' => 'http://www.simplysocialmedia.me',
                'DOMAIN' => 'simplysocialmedia.me',
            ),
            'swamptours.info' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_377_',
                'SITE_URL' => 'http://www.swamptours.info',
                'DOMAIN' => 'swamptours.info',
            ),
            'shopcrankcountydaredevils.com' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_381_',
                'SITE_URL' => 'http://www.shopcrankcountydaredevils.com',
                'DOMAIN' => 'shopcrankcountydaredevils.com',
            ),
            'stephenaubrey.net' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_386_',
                'SITE_URL' => 'http://www.stephenaubrey.net',
                'DOMAIN' => 'stephenaubrey.net',
            ),
            'syscooklahoma.com' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_467_',
                'SITE_URL' => 'http://www.syscooklahoma.com',
                'DOMAIN' => 'syscooklahoma.com',
            ),
            'nwphoenixrealestateguide.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1029_',
                'SITE_URL' => 'http://www.nwphoenixrealestateguide.com',
                'DOMAIN' => 'nwphoenixrealestateguide.com',
            ),
            'real-home-based-business-opportunity.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1030_',
                'SITE_URL' => 'http://www.real-home-based-business-opportunity.com',
                'DOMAIN' => 'real-home-based-business-opportunity.com',
            ),
            'advertisingstrategyinsights.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1584_',
                'SITE_URL' => 'http://www.advertisingstrategyinsights.com',
                'DOMAIN' => 'advertisingstrategyinsights.com',
            ),
            'save-our-sperm.org' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_370_',
                'SITE_URL' => 'http://www.save-our-sperm.org',
                'DOMAIN' => 'save-our-sperm.org',
            ),
            'saaccnh.org' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_371_',
                'SITE_URL' => 'http://www.saaccnh.org',
                'DOMAIN' => 'saaccnh.org',
            ),
            'roaringlambspublishing.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_373_',
                'SITE_URL' => 'http://www.roaringlambspublishing.com',
                'DOMAIN' => 'roaringlambspublishing.com',
            ),
            'shaikabdulhameed.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_374_',
                'SITE_URL' => 'http://www.shaikabdulhameed.com',
                'DOMAIN' => 'shaikabdulhameed.com',
            ),
            'shavetheadmirals.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_379_',
                'SITE_URL' => 'http://www.shavetheadmirals.com',
                'DOMAIN' => 'shavetheadmirals.com',
            ),
            'childsplaykennels.com' =>
            array(
                'TABLE_SCHEMA' => 'manthaso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1025_',
                'SITE_URL' => 'http://www.childsplaykennels.com',
                'DOMAIN' => 'childsplaykennels.com',
            ),
            'doctorgaber.com' =>
            array(
                'TABLE_SCHEMA' => 'manthaso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1026_',
                'SITE_URL' => 'http://www.doctorgaber.com',
                'DOMAIN' => 'doctorgaber.com',
            ),
            'brandingstrategyinsights.com' =>
            array(
                'TABLE_SCHEMA' => 'manthaso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1585_',
                'SITE_URL' => 'http://www.brandingstrategyinsights.com',
                'DOMAIN' => 'brandingstrategyinsights.com',
            ),
            'newsouthernmedia.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1312_',
                'SITE_URL' => 'http://www.newsouthernmedia.com',
                'DOMAIN' => 'newsouthernmedia.com',
            ),
            'northspringsbasketball.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1313_',
                'SITE_URL' => 'http://www.northspringsbasketball.com',
                'DOMAIN' => 'northspringsbasketball.com',
            ),
            'number1homebusiness.net' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1314_',
                'SITE_URL' => 'http://www.number1homebusiness.net',
                'DOMAIN' => 'number1homebusiness.net',
            ),
            'ozpokertour.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1315_',
                'SITE_URL' => 'http://www.ozpokertour.com',
                'DOMAIN' => 'ozpokertour.com',
            ),
            'pacific-coastwellness.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1316_',
                'SITE_URL' => 'http://www.pacific-coastwellness.com',
                'DOMAIN' => 'pacific-coastwellness.com',
            ),
            'affiliate-atm.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1586_',
                'SITE_URL' => 'http://www.affiliate-atm.com',
                'DOMAIN' => 'affiliate-atm.com',
            ),
            'margaritapizzabar.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_954_',
                'SITE_URL' => 'http://www.margaritapizzabar.com',
                'DOMAIN' => 'margaritapizzabar.com',
            ),
            'hsprtpl.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_955_',
                'SITE_URL' => 'http://www.hsprtpl.com',
                'DOMAIN' => 'hsprtpl.com',
            ),
            'liberalatheistvegetarian.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_956_',
                'SITE_URL' => 'http://www.liberalatheistvegetarian.com',
                'DOMAIN' => 'liberalatheistvegetarian.com',
            ),
            'xiphiasgladius.org' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_957_',
                'SITE_URL' => 'http://www.xiphiasgladius.org',
                'DOMAIN' => 'xiphiasgladius.org',
            ),
            'tweetupkzoo.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_958_',
                'SITE_URL' => 'http://www.tweetupkzoo.com',
                'DOMAIN' => 'tweetupkzoo.com',
            ),
            'kesem.biz' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_78_',
                'SITE_URL' => 'http://kesem.biz',
                'DOMAIN' => 'kesem.biz',
            ),
            'kickbackcommunications.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_79_',
                'SITE_URL' => 'http://kickbackcommunications.com',
                'DOMAIN' => 'kickbackcommunications.com',
            ),
            'klivekravenbeats.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_80_',
                'SITE_URL' => 'http://klivekravenbeats.com',
                'DOMAIN' => 'klivekravenbeats.com',
            ),
            'ko-n-ur.org' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_81_',
                'SITE_URL' => 'http://ko-n-ur.org',
                'DOMAIN' => 'ko-n-ur.org',
            ),
            'lawofattractionsuccessstories.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1027_',
                'SITE_URL' => 'http://www.lawofattractionsuccessstories.com',
                'DOMAIN' => 'lawofattractionsuccessstories.com',
            ),
            'southeastohioadvertising.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1028_',
                'SITE_URL' => 'http://www.southeastohioadvertising.com',
                'DOMAIN' => 'southeastohioadvertising.com',
            ),
            'greatworldpolo.org' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1587_',
                'SITE_URL' => 'http://www.greatworldpolo.org',
                'DOMAIN' => 'greatworldpolo.org',
            ),
            'original-individuals.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_366_',
                'SITE_URL' => 'http://www.original-individuals.com',
                'DOMAIN' => 'original-individuals.com',
            ),
            'osoconalas.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_367_',
                'SITE_URL' => 'http://www.osoconalas.com',
                'DOMAIN' => 'osoconalas.com',
            ),
            'patientcompliancehypnosis.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_368_',
                'SITE_URL' => 'http://www.patientcompliancehypnosis.com',
                'DOMAIN' => 'patientcompliancehypnosis.com',
            ),
            'radiofolcloreportugues.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_369_',
                'SITE_URL' => 'http://www.radiofolcloreportugues.com',
                'DOMAIN' => 'radiofolcloreportugues.com',
            ),
            're-energizenow.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_372_',
                'SITE_URL' => 'http://www.re-energizenow.com',
                'DOMAIN' => 're-energizenow.com',
            ),
            'accident-lawyers-new-york.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1023_',
                'SITE_URL' => 'http://www.accident-lawyers-new-york.com',
                'DOMAIN' => 'accident-lawyers-new-york.com',
            ),
            'adaboatyard.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1024_',
                'SITE_URL' => 'http://www.adaboatyard.com',
                'DOMAIN' => 'adaboatyard.com',
            ),
            'labyrinthofthepsychonaut.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1588_',
                'SITE_URL' => 'http://www.labyrinthofthepsychonaut.com',
                'DOMAIN' => 'labyrinthofthepsychonaut.com',
            ),
            'lionjewelry-rickcamerondesign.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_340_',
                'SITE_URL' => 'http://www.lionjewelry-rickcamerondesign.com',
                'DOMAIN' => 'lionjewelry-rickcamerondesign.com',
            ),
            'internetoglasavanje.info' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_344_',
                'SITE_URL' => 'http://www.internetoglasavanje.info',
                'DOMAIN' => 'internetoglasavanje.info',
            ),
            'pantherjewelry-rickcamerondesign.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_350_',
                'SITE_URL' => 'http://www.pantherjewelry-rickcamerondesign.com',
                'DOMAIN' => 'pantherjewelry-rickcamerondesign.com',
            ),
            'beppumasjid.org' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_351_',
                'SITE_URL' => 'http://www.beppumasjid.org',
                'DOMAIN' => 'beppumasjid.org',
            ),
            'outsourcejob.info' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_352_',
                'SITE_URL' => 'http://www.outsourcejob.info',
                'DOMAIN' => 'outsourcejob.info',
            ),
            'discounthotelcanada.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1021_',
                'SITE_URL' => 'http://www.discounthotelcanada.com',
                'DOMAIN' => 'discounthotelcanada.com',
            ),
            '825credit.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1022_',
                'SITE_URL' => 'http://www.825credit.com',
                'DOMAIN' => '825credit.com',
            ),
            'noimpactmandoc.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1457_',
                'SITE_URL' => 'http://www.noimpactmandoc.com',
                'DOMAIN' => 'noimpactmandoc.com',
            ),
            'wlas2013.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1589_',
                'SITE_URL' => 'http://www.wlas2013.com',
                'DOMAIN' => 'wlas2013.com',
            ),
            'creativelyendeavored.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_333_',
                'SITE_URL' => 'http://www.creativelyendeavored.com',
                'DOMAIN' => 'creativelyendeavored.com',
            ),
            'millionsmarchharlem.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_339_',
                'SITE_URL' => 'http://www.millionsmarchharlem.com',
                'DOMAIN' => 'millionsmarchharlem.com',
            ),
            'wcsafford.org' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_347_',
                'SITE_URL' => 'http://www.wcsafford.org',
                'DOMAIN' => 'wcsafford.org',
            ),
            'eaglejewelry-rickcamerondesign.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_348_',
                'SITE_URL' => 'http://www.eaglejewelry-rickcamerondesign.com',
                'DOMAIN' => 'eaglejewelry-rickcamerondesign.com',
            ),
            'mongoriverrun.net' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_355_',
                'SITE_URL' => 'http://www.mongoriverrun.net',
                'DOMAIN' => 'mongoriverrun.net',
            ),
            'colossusfinance.com' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1020_',
                'SITE_URL' => 'http://www.colossusfinance.com',
                'DOMAIN' => 'colossusfinance.com',
            ),
            'ayhankebab.info' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1672_',
                'SITE_URL' => 'http://www.ayhankebab.info',
                'DOMAIN' => 'ayhankebab.info',
            ),
            'hiddengrove.info' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_318_',
                'SITE_URL' => 'http://www.hiddengrove.info',
                'DOMAIN' => 'hiddengrove.info',
            ),
            'villo.me' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_345_',
                'SITE_URL' => 'http://villo.me',
                'DOMAIN' => 'villo.me',
            ),
            'egyptjobsearch.com' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_349_',
                'SITE_URL' => 'http://www.egyptjobsearch.com',
                'DOMAIN' => 'egyptjobsearch.com',
            ),
            'pdv2.com' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_353_',
                'SITE_URL' => 'http://www.pdv2.com',
                'DOMAIN' => 'pdv2.com',
            ),
            'sufiotetahi.org' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_358_',
                'SITE_URL' => 'http://www.sufiotetahi.org',
                'DOMAIN' => 'sufiotetahi.org',
            ),
            'brendabuckley.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1233_',
                'SITE_URL' => 'http://www.brendabuckley.com',
                'DOMAIN' => 'brendabuckley.com',
            ),
            'brucebeaton.biz' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1234_',
                'SITE_URL' => 'http://www.brucebeaton.biz',
                'DOMAIN' => 'brucebeaton.biz',
            ),
            'carlsaganbarranca.org' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1235_',
                'SITE_URL' => 'http://www.carlsaganbarranca.org',
                'DOMAIN' => 'carlsaganbarranca.org',
            ),
            'bhsallclassreunion.org' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1590_',
                'SITE_URL' => 'http://www.bhsallclassreunion.org',
                'DOMAIN' => 'bhsallclassreunion.org',
            ),
            'pezavalpalmas.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_260_',
                'SITE_URL' => 'http://www.pezavalpalmas.com',
                'DOMAIN' => 'pezavalpalmas.com',
            ),
            'theartoftransplant.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_268_',
                'SITE_URL' => 'http://www.theartoftransplant.com',
                'DOMAIN' => 'theartoftransplant.com',
            ),
            'resultspropertiestenerife.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_274_',
                'SITE_URL' => 'http://www.resultspropertiestenerife.com',
                'DOMAIN' => 'resultspropertiestenerife.com',
            ),
            'whateverendeavor.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_275_',
                'SITE_URL' => 'http://www.whateverendeavor.com',
                'DOMAIN' => 'whateverendeavor.com',
            ),
            'uasnews.net' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_997_',
                'SITE_URL' => 'http://www.uasnews.net',
                'DOMAIN' => 'uasnews.net',
            ),
            'waterqualityday.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_998_',
                'SITE_URL' => 'http://www.waterqualityday.com',
                'DOMAIN' => 'waterqualityday.com',
            ),
            'dailycoinflip.com' =>
            array(
                'TABLE_SCHEMA' => 'moolarbc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1673_',
                'SITE_URL' => 'http://www.dailycoinflip.com',
                'DOMAIN' => 'dailycoinflip.com',
            ),
            'moolarb.com' =>
            array(
                'TABLE_SCHEMA' => 'moolarbc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_880_',
                'SITE_URL' => 'http://www.moolarb.com',
                'DOMAIN' => 'moolarb.com',
            ),
            'scholarshipz.info' =>
            array(
                'TABLE_SCHEMA' => 'moolarbc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_881_',
                'SITE_URL' => 'http://www.scholarshipz.info',
                'DOMAIN' => 'scholarshipz.info',
            ),
            'seattlemetropolitanfashionweek.com' =>
            array(
                'TABLE_SCHEMA' => 'moolarbc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_882_',
                'SITE_URL' => 'http://www.seattlemetropolitanfashionweek.com',
                'DOMAIN' => 'seattlemetropolitanfashionweek.com',
            ),
            'basilbernstein7.com' =>
            array(
                'TABLE_SCHEMA' => 'moolarbc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_883_',
                'SITE_URL' => 'http://www.basilbernstein7.com',
                'DOMAIN' => 'basilbernstein7.com',
            ),
            'whydidnti.org' =>
            array(
                'TABLE_SCHEMA' => 'moolarbc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_884_',
                'SITE_URL' => 'http://www.whydidnti.org',
                'DOMAIN' => 'whydidnti.org',
            ),
            'shhxtv.com' =>
            array(
                'TABLE_SCHEMA' => 'multicul_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1591_',
                'SITE_URL' => 'http://www.shhxtv.com',
                'DOMAIN' => 'shhxtv.com',
            ),
            'tntrap.com' =>
            array(
                'TABLE_SCHEMA' => 'multicul_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_993_',
                'SITE_URL' => 'http://www.tntrap.com',
                'DOMAIN' => 'tntrap.com',
            ),
            'twdnc.com' =>
            array(
                'TABLE_SCHEMA' => 'multicul_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_994_',
                'SITE_URL' => 'http://www.twdnc.com',
                'DOMAIN' => 'twdnc.com',
            ),
            'ase13.com' =>
            array(
                'TABLE_SCHEMA' => 'myalignh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1594_',
                'SITE_URL' => 'http://www.ase13.com',
                'DOMAIN' => 'ase13.com',
            ),
            'myalignhr.com' =>
            array(
                'TABLE_SCHEMA' => 'myalignh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_697_',
                'SITE_URL' => 'http://www.myalignhr.com',
                'DOMAIN' => 'myalignhr.com',
            ),
            'mydragonfly.info' =>
            array(
                'TABLE_SCHEMA' => 'myalignh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_698_',
                'SITE_URL' => 'http://www.mydragonfly.info',
                'DOMAIN' => 'mydragonfly.info',
            ),
            'mykvutza.com' =>
            array(
                'TABLE_SCHEMA' => 'myalignh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_699_',
                'SITE_URL' => 'http://www.mykvutza.com',
                'DOMAIN' => 'mykvutza.com',
            ),
            'naero.org' =>
            array(
                'TABLE_SCHEMA' => 'myalignh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_700_',
                'SITE_URL' => 'http://www.naero.org',
                'DOMAIN' => 'naero.org',
            ),
            'narutoroom.info' =>
            array(
                'TABLE_SCHEMA' => 'myalignh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_701_',
                'SITE_URL' => 'http://www.narutoroom.info',
                'DOMAIN' => 'narutoroom.info',
            ),
            'apocalyptomedia.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1227_',
                'SITE_URL' => 'http://www.apocalyptomedia.com',
                'DOMAIN' => 'apocalyptomedia.com',
            ),
            'aromasensual.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1228_',
                'SITE_URL' => 'http://www.aromasensual.com',
                'DOMAIN' => 'aromasensual.com',
            ),
            'chequeboutique.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1229_',
                'SITE_URL' => 'http://www.chequeboutique.com',
                'DOMAIN' => 'chequeboutique.com',
            ),
            'curvesneedham.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1230_',
                'SITE_URL' => 'http://www.curvesneedham.com',
                'DOMAIN' => 'curvesneedham.com',
            ),
            'dotclassik.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1231_',
                'SITE_URL' => 'http://www.dotclassik.com',
                'DOMAIN' => 'dotclassik.com',
            ),
            'charityofchoice.cc' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1595_',
                'SITE_URL' => 'http://www.charityofchoice.cc',
                'DOMAIN' => 'charityofchoice.cc',
            ),
            '8thstreetdesigndistrict.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_258_',
                'SITE_URL' => 'http://www.8thstreetdesigndistrict.com',
                'DOMAIN' => '8thstreetdesigndistrict.com',
            ),
            'jellisphotography.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_259_',
                'SITE_URL' => 'http://www.jellisphotography.com',
                'DOMAIN' => 'jellisphotography.com',
            ),
            'savannahsinfonietta.org' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_267_',
                'SITE_URL' => 'http://www.savannahsinfonietta.org',
                'DOMAIN' => 'savannahsinfonietta.org',
            ),
            'lookoutshuttersinc.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_278_',
                'SITE_URL' => 'http://www.lookoutshuttersinc.com',
                'DOMAIN' => 'lookoutshuttersinc.com',
            ),
            'safemoveoregon.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_279_',
                'SITE_URL' => 'http://www.safemoveoregon.com',
                'DOMAIN' => 'safemoveoregon.com',
            ),
            'xoopsers.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_995_',
                'SITE_URL' => 'http://www.xoopsers.com',
                'DOMAIN' => 'xoopsers.com',
            ),
            'therrsfeedsound.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_996_',
                'SITE_URL' => 'http://www.therrsfeedsound.com',
                'DOMAIN' => 'therrsfeedsound.com',
            ),
            'aarons-bar-mitzvah.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_183_',
                'SITE_URL' => 'http://aarons-bar-mitzvah.com',
                'DOMAIN' => 'aarons-bar-mitzvah.com',
            ),
            'accessabilitymodifications.us' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_184_',
                'SITE_URL' => 'http://accessabilitymodifications.us',
                'DOMAIN' => 'accessabilitymodifications.us',
            ),
            'acesthetics.org' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_185_',
                'SITE_URL' => 'http://acesthetics.org',
                'DOMAIN' => 'acesthetics.org',
            ),
            'waterfowlersbootcamp.org' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_4_',
                'SITE_URL' => 'http://www.waterfowlersbootcamp.org',
                'DOMAIN' => 'waterfowlersbootcamp.org',
            ),
            'ocalabridal.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_5_',
                'SITE_URL' => 'http://www.ocalabridal.com',
                'DOMAIN' => 'ocalabridal.com',
            ),
            'indiansong-radio.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1101_',
                'SITE_URL' => 'http://www.indiansong-radio.com',
                'DOMAIN' => 'indiansong-radio.com',
            ),
            'phillycommunitycinema.org' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1102_',
                'SITE_URL' => 'http://www.phillycommunitycinema.org',
                'DOMAIN' => 'phillycommunitycinema.org',
            ),
            'buymonclersite.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1103_',
                'SITE_URL' => 'http://www.buymonclersite.com',
                'DOMAIN' => 'buymonclersite.com',
            ),
            'geoose.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1104_',
                'SITE_URL' => 'http://www.geoose.com',
                'DOMAIN' => 'geoose.com',
            ),
            'ekoaffaren.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1592_',
                'SITE_URL' => 'http://www.ekoaffaren.com',
                'DOMAIN' => 'ekoaffaren.com',
            ),
            'cdmstudycentre.org' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_249_',
                'SITE_URL' => 'http://cdmstudycentre.org',
                'DOMAIN' => 'cdmstudycentre.org',
            ),
            'kidsandmoney.tv' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_250_',
                'SITE_URL' => 'http://www.kidsandmoney.tv',
                'DOMAIN' => 'kidsandmoney.tv',
            ),
            'infrawebs-eu.org' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_251_',
                'SITE_URL' => 'http://www.infrawebs-eu.org',
                'DOMAIN' => 'infrawebs-eu.org',
            ),
            'archifind.net' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_252_',
                'SITE_URL' => 'http://www.archifind.net',
                'DOMAIN' => 'archifind.net',
            ),
            'upshots.biz' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_255_',
                'SITE_URL' => 'http://www.upshots.biz',
                'DOMAIN' => 'upshots.biz',
            ),
            'weswatsonweb.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_991_',
                'SITE_URL' => 'http://www.weswatsonweb.com',
                'DOMAIN' => 'weswatsonweb.com',
            ),
            'thestudiosky.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_992_',
                'SITE_URL' => 'http://www.thestudiosky.com',
                'DOMAIN' => 'thestudiosky.com',
            ),
            'handlvmade.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1095_',
                'SITE_URL' => 'http://www.handlvmade.com',
                'DOMAIN' => 'handlvmade.com',
            ),
            'mycreditcardpayment.net' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1098_',
                'SITE_URL' => 'http://www.mycreditcardpayment.net',
                'DOMAIN' => 'mycreditcardpayment.net',
            ),
            'hackingwindowsvista.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1099_',
                'SITE_URL' => 'http://www.hackingwindowsvista.com',
                'DOMAIN' => 'hackingwindowsvista.com',
            ),
            'tactologic.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1100_',
                'SITE_URL' => 'http://www.tactologic.com',
                'DOMAIN' => 'tactologic.com',
            ),
            'konsep.mobi' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1111_',
                'SITE_URL' => 'http://www.konsep.mobi',
                'DOMAIN' => 'konsep.mobi',
            ),
            'learnatnorthwoods.mobi' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1112_',
                'SITE_URL' => 'http://www.learnatnorthwoods.mobi',
                'DOMAIN' => 'learnatnorthwoods.mobi',
            ),
            'mu-md.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1113_',
                'SITE_URL' => 'http://www.mu-md.com',
                'DOMAIN' => 'mu-md.com',
            ),
            'netbeans-serbia.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1114_',
                'SITE_URL' => 'http://www.netbeans-serbia.com',
                'DOMAIN' => 'netbeans-serbia.com',
            ),
            'nuaazs.net' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1596_',
                'SITE_URL' => 'http://www.nuaazs.net',
                'DOMAIN' => 'nuaazs.net',
            ),
            'elombu.info' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1664_',
                'SITE_URL' => 'http://www.elombu.info',
                'DOMAIN' => 'elombu.info',
            ),
            'jobdestruction.info' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_247_',
                'SITE_URL' => 'http://www.jobdestruction.info',
                'DOMAIN' => 'jobdestruction.info',
            ),
            'thedamesite.net' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_248_',
                'SITE_URL' => 'http://www.thedamesite.net',
                'DOMAIN' => 'thedamesite.net',
            ),
            'kreszvizsga.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_253_',
                'SITE_URL' => 'http://www.kreszvizsga.com',
                'DOMAIN' => 'kreszvizsga.com',
            ),
            'startwhereyouareprovt.org' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_254_',
                'SITE_URL' => 'http://www.startwhereyouareprovt.org',
                'DOMAIN' => 'startwhereyouareprovt.org',
            ),
            'nxdqk.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_256_',
                'SITE_URL' => 'http://www.nxdqk.com',
                'DOMAIN' => 'nxdqk.com',
            ),
            'sudanmap.org' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_755_',
                'SITE_URL' => 'http://www.sudanmap.org',
                'DOMAIN' => 'sudanmap.org',
            ),
            'takeahikefitness.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_756_',
                'SITE_URL' => 'http://www.takeahikefitness.com',
                'DOMAIN' => 'takeahikefitness.com',
            ),
            's-swuw.org' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_764_',
                'SITE_URL' => 'http://www.s-swuw.org',
                'DOMAIN' => 's-swuw.org',
            ),
            'purpleminibikemedia.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_765_',
                'SITE_URL' => 'http://www.purpleminibikemedia.com',
                'DOMAIN' => 'purpleminibikemedia.com',
            ),
            'austrianaccommodation.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_766_',
                'SITE_URL' => 'http://www.austrianaccommodation.com',
                'DOMAIN' => 'austrianaccommodation.com',
            ),
            'fantasyarrows.com' =>
            array(
                'TABLE_SCHEMA' => 'ncaafoot_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1599_',
                'SITE_URL' => 'http://www.fantasyarrows.com',
                'DOMAIN' => 'fantasyarrows.com',
            ),
            'ncaafootball12.net' =>
            array(
                'TABLE_SCHEMA' => 'ncaafoot_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_702_',
                'SITE_URL' => 'http://www.ncaafootball12.net',
                'DOMAIN' => 'ncaafootball12.net',
            ),
            'networkinglinuxbook.com' =>
            array(
                'TABLE_SCHEMA' => 'ncaafoot_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_703_',
                'SITE_URL' => 'http://www.networkinglinuxbook.com',
                'DOMAIN' => 'networkinglinuxbook.com',
            ),
            'new-toyota-yaris-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'ncaafoot_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_704_',
                'SITE_URL' => 'http://www.new-toyota-yaris-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-yaris-rockland-county-ny.info',
            ),
            'nflarizonacardinalsjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'ncaafoot_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_705_',
                'SITE_URL' => 'http://www.nflarizonacardinalsjerseysusa.info',
                'DOMAIN' => 'nflarizonacardinalsjerseysusa.info',
            ),
            'nflkansascitychiefsjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'ncaafoot_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_706_',
                'SITE_URL' => 'http://www.nflkansascitychiefsjerseysusa.info',
                'DOMAIN' => 'nflkansascitychiefsjerseysusa.info',
            ),
            'studiosavvystore.com' =>
            array(
                'TABLE_SCHEMA' => 'nicholsc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1598_',
                'SITE_URL' => 'http://www.studiosavvystore.com',
                'DOMAIN' => 'studiosavvystore.com',
            ),
            'nicholsct.org' =>
            array(
                'TABLE_SCHEMA' => 'nicholsc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_707_',
                'SITE_URL' => 'http://www.nicholsct.org',
                'DOMAIN' => 'nicholsct.org',
            ),
            'nflnewyorkgiantsjerseysus.info' =>
            array(
                'TABLE_SCHEMA' => 'nicholsc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_708_',
                'SITE_URL' => 'http://www.nflnewyorkgiantsjerseysus.info',
                'DOMAIN' => 'nflnewyorkgiantsjerseysus.info',
            ),
            'niezgodafitness.com' =>
            array(
                'TABLE_SCHEMA' => 'nicholsc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_709_',
                'SITE_URL' => 'http://www.niezgodafitness.com',
                'DOMAIN' => 'niezgodafitness.com',
            ),
            'nihul-atarim.com' =>
            array(
                'TABLE_SCHEMA' => 'nicholsc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_710_',
                'SITE_URL' => 'http://www.nihul-atarim.com',
                'DOMAIN' => 'nihul-atarim.com',
            ),
            'andrewwmullins.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_102_',
                'SITE_URL' => 'http://www.andrewwmullins.com',
                'DOMAIN' => 'andrewwmullins.com',
            ),
            'thompsonpeakretreat.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_112_',
                'SITE_URL' => 'http://www.thompsonpeakretreat.com',
                'DOMAIN' => 'thompsonpeakretreat.com',
            ),
            'opresume.net' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_114_',
                'SITE_URL' => 'http://www.opresume.net',
                'DOMAIN' => 'opresume.net',
            ),
            'gizzahoon.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1597_',
                'SITE_URL' => 'http://www.gizzahoon.com',
                'DOMAIN' => 'gizzahoon.com',
            ),
            'redhillchina.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_161_',
                'SITE_URL' => 'http://www.redhillchina.com',
                'DOMAIN' => 'redhillchina.com',
            ),
            'studiomaxim.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_753_',
                'SITE_URL' => 'http://www.studiomaxim.com',
                'DOMAIN' => 'studiomaxim.com',
            ),
            'strictlyswingak.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_754_',
                'SITE_URL' => 'http://www.strictlyswingak.com',
                'DOMAIN' => 'strictlyswingak.com',
            ),
            'dnfjiejinqi.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_84_',
                'SITE_URL' => 'http://www.dnfjiejinqi.com',
                'DOMAIN' => 'dnfjiejinqi.com',
            ),
            'socialmarketing4travel.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_85_',
                'SITE_URL' => 'http://www.socialmarketing4travel.com',
                'DOMAIN' => 'socialmarketing4travel.com',
            ),
            'sbdist.org' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_86_',
                'SITE_URL' => 'http://www.sbdist.org',
                'DOMAIN' => 'sbdist.org',
            ),
            'selenamcarthur.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_87_',
                'SITE_URL' => 'http://www.selenamcarthur.com',
                'DOMAIN' => 'selenamcarthur.com',
            ),
            'ranchosports.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_88_',
                'SITE_URL' => 'http://www.ranchosports.com',
                'DOMAIN' => 'ranchosports.com',
            ),
            'challwamar.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_89_',
                'SITE_URL' => 'http://www.challwamar.com',
                'DOMAIN' => 'challwamar.com',
            ),
            'yellowticket2.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_111_',
                'SITE_URL' => 'http://www.yellowticket2.com',
                'DOMAIN' => 'yellowticket2.com',
            ),
            'thegratefuldads.net' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1602_',
                'SITE_URL' => 'http://www.thegratefuldads.net',
                'DOMAIN' => 'thegratefuldads.net',
            ),
            'thelifestylehunter.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_752_',
                'SITE_URL' => 'http://www.thelifestylehunter.com',
                'DOMAIN' => 'thelifestylehunter.com',
            ),
            'itvexploration.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_86_',
                'SITE_URL' => 'http://www.itvexploration.com',
                'DOMAIN' => 'itvexploration.com',
            ),
            'ecoscreener.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_91_',
                'SITE_URL' => 'http://www.ecoscreener.com',
                'DOMAIN' => 'ecoscreener.com',
            ),
            'manakkudiyan.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_93_',
                'SITE_URL' => 'http://www.manakkudiyan.com',
                'DOMAIN' => 'manakkudiyan.com',
            ),
            'r3ma.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_99_',
                'SITE_URL' => 'http://www.r3ma.com',
                'DOMAIN' => 'r3ma.com',
            ),
            'kimberley-fire-project.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1600_',
                'SITE_URL' => 'http://www.kimberley-fire-project.com',
                'DOMAIN' => 'kimberley-fire-project.com',
            ),
            'st-louis-auto-accidents-lawyers.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_750_',
                'SITE_URL' => 'http://www.st-louis-auto-accidents-lawyers.com',
                'DOMAIN' => 'st-louis-auto-accidents-lawyers.com',
            ),
            'steadfastbags.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_751_',
                'SITE_URL' => 'http://www.steadfastbags.com',
                'DOMAIN' => 'steadfastbags.com',
            ),
            'inet-oll.info' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_78_',
                'SITE_URL' => 'http://www.inet-oll.info',
                'DOMAIN' => 'inet-oll.info',
            ),
            'horsebookdepot.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_79_',
                'SITE_URL' => 'http://www.horsebookdepot.com',
                'DOMAIN' => 'horsebookdepot.com',
            ),
            'slavonski-brod.info' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_82_',
                'SITE_URL' => 'http://www.slavonski-brod.info',
                'DOMAIN' => 'slavonski-brod.info',
            ),
            'naturalawakeningsswva.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_96_',
                'SITE_URL' => 'http://www.naturalawakeningsswva.com',
                'DOMAIN' => 'naturalawakeningsswva.com',
            ),
            'mvhsfreelancer.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_97_',
                'SITE_URL' => 'http://www.mvhsfreelancer.com',
                'DOMAIN' => 'mvhsfreelancer.com',
            ),
            'siamintelligenceunit.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1601_',
                'SITE_URL' => 'http://www.siamintelligenceunit.com',
                'DOMAIN' => 'siamintelligenceunit.com',
            ),
            'spokenbyzurek.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_748_',
                'SITE_URL' => 'http://www.spokenbyzurek.com',
                'DOMAIN' => 'spokenbyzurek.com',
            ),
            'st-ives.biz' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_749_',
                'SITE_URL' => 'http://www.st-ives.biz',
                'DOMAIN' => 'st-ives.biz',
            ),
            'gaillourgroup.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_75_',
                'SITE_URL' => 'http://www.gaillourgroup.com',
                'DOMAIN' => 'gaillourgroup.com',
            ),
            'prepare4rain.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_83_',
                'SITE_URL' => 'http://www.prepare4rain.com',
                'DOMAIN' => 'prepare4rain.com',
            ),
            'chattanoogacrash.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_87_',
                'SITE_URL' => 'http://www.chattanoogacrash.com',
                'DOMAIN' => 'chattanoogacrash.com',
            ),
            'hidrobiologia.info' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_88_',
                'SITE_URL' => 'http://www.hidrobiologia.info',
                'DOMAIN' => 'hidrobiologia.info',
            ),
            'uniteamasia.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_94_',
                'SITE_URL' => 'http://www.uniteamasia.com',
                'DOMAIN' => 'uniteamasia.com',
            ),
            'grupo-ift.com' =>
            array(
                'TABLE_SCHEMA' => 'nmllyqco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1603_',
                'SITE_URL' => 'http://www.grupo-ift.com',
                'DOMAIN' => 'grupo-ift.com',
            ),
            'nmllyq.com' =>
            array(
                'TABLE_SCHEMA' => 'nmllyqco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_712_',
                'SITE_URL' => 'http://www.nmllyq.com',
                'DOMAIN' => 'nmllyq.com',
            ),
            'nirman.me' =>
            array(
                'TABLE_SCHEMA' => 'nmllyqco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_713_',
                'SITE_URL' => 'http://www.nirman.me',
                'DOMAIN' => 'nirman.me',
            ),
            'nysignaturefilms.com' =>
            array(
                'TABLE_SCHEMA' => 'nmllyqco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_714_',
                'SITE_URL' => 'http://www.nysignaturefilms.com',
                'DOMAIN' => 'nysignaturefilms.com',
            ),
            'occupy837.com' =>
            array(
                'TABLE_SCHEMA' => 'nmllyqco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_715_',
                'SITE_URL' => 'http://www.occupy837.com',
                'DOMAIN' => 'occupy837.com',
            ),
            'xb179.com' =>
            array(
                'TABLE_SCHEMA' => 'nmllyqco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_760_',
                'SITE_URL' => 'http://www.xb179.com',
                'DOMAIN' => 'xb179.com',
            ),
            'medicalmarijuanaindustrygroup.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_242_',
                'SITE_URL' => 'http://www.medicalmarijuanaindustrygroup.com',
                'DOMAIN' => 'medicalmarijuanaindustrygroup.com',
            ),
            'metromailadvertising.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_243_',
                'SITE_URL' => 'http://www.metromailadvertising.com',
                'DOMAIN' => 'metromailadvertising.com',
            ),
            'mail.miaomiao003.info' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_244_',
                'SITE_URL' => 'http://mail.miaomiao003.info',
                'DOMAIN' => 'mail.miaomiao003.info',
            ),
            'mkchildcare.info' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_245_',
                'SITE_URL' => 'http://www.mkchildcare.info',
                'DOMAIN' => 'mkchildcare.info',
            ),
            'elevationlive.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1214_',
                'SITE_URL' => 'http://www.elevationlive.com',
                'DOMAIN' => 'elevationlive.com',
            ),
            'fredplimley.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1215_',
                'SITE_URL' => 'http://www.fredplimley.com',
                'DOMAIN' => 'fredplimley.com',
            ),
            'goalkitsap.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1216_',
                'SITE_URL' => 'http://www.goalkitsap.com',
                'DOMAIN' => 'goalkitsap.com',
            ),
            'gymguyunderwear.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1217_',
                'SITE_URL' => 'http://www.gymguyunderwear.com',
                'DOMAIN' => 'gymguyunderwear.com',
            ),
            'martinhadselldonascimento.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1471_',
                'SITE_URL' => 'http://www.martinhadselldonascimento.com',
                'DOMAIN' => 'martinhadselldonascimento.com',
            ),
            'cbtrails.org' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1610_',
                'SITE_URL' => 'http://www.cbtrails.org',
                'DOMAIN' => 'cbtrails.org',
            ),
            'pamravenscroft.org' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_822_',
                'SITE_URL' => 'http://www.pamravenscroft.org',
                'DOMAIN' => 'pamravenscroft.org',
            ),
            'zeitgeist-jalisco.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_823_',
                'SITE_URL' => 'http://www.zeitgeist-jalisco.com',
                'DOMAIN' => 'zeitgeist-jalisco.com',
            ),
            'cetskip.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_824_',
                'SITE_URL' => 'http://www.cetskip.com',
                'DOMAIN' => 'cetskip.com',
            ),
            'enchiladaenduro.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_825_',
                'SITE_URL' => 'http://www.enchiladaenduro.com',
                'DOMAIN' => 'enchiladaenduro.com',
            ),
            'andreverdenskrig.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_826_',
                'SITE_URL' => 'http://www.andreverdenskrig.com',
                'DOMAIN' => 'andreverdenskrig.com',
            ),
            'apeshill.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1604_',
                'SITE_URL' => 'http://www.apeshill.com',
                'DOMAIN' => 'apeshill.com',
            ),
            'singaporeasiaexpo.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_744_',
                'SITE_URL' => 'http://www.singaporeasiaexpo.com',
                'DOMAIN' => 'singaporeasiaexpo.com',
            ),
            'sisterforgiveme.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_745_',
                'SITE_URL' => 'http://www.sisterforgiveme.com',
                'DOMAIN' => 'sisterforgiveme.com',
            ),
            'parquesihunchen.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_77_',
                'SITE_URL' => 'http://www.parquesihunchen.com',
                'DOMAIN' => 'parquesihunchen.com',
            ),
            'usacejacksonvilleregulatory.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_84_',
                'SITE_URL' => 'http://www.usacejacksonvilleregulatory.com',
                'DOMAIN' => 'usacejacksonvilleregulatory.com',
            ),
            'evolutionaryvoters.us' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_85_',
                'SITE_URL' => 'http://www.evolutionaryvoters.us',
                'DOMAIN' => 'evolutionaryvoters.us',
            ),
            'interactivejaguar.me' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1605_',
                'SITE_URL' => 'http://www.interactivejaguar.me',
                'DOMAIN' => 'interactivejaguar.me',
            ),
            'slowjanuary.com' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_746_',
                'SITE_URL' => 'http://www.slowjanuary.com',
                'DOMAIN' => 'slowjanuary.com',
            ),
            'smokeythewellnessguy.net' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_747_',
                'SITE_URL' => 'http://www.smokeythewellnessguy.net',
                'DOMAIN' => 'smokeythewellnessguy.net',
            ),
            'queenannefb.org' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_74_',
                'SITE_URL' => 'http://www.queenannefb.org',
                'DOMAIN' => 'queenannefb.org',
            ),
            'mtsbmlyym.info' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_80_',
                'SITE_URL' => 'http://www.mtsbmlyym.info',
                'DOMAIN' => 'mtsbmlyym.info',
            ),
            'williamrowson.com' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_81_',
                'SITE_URL' => 'http://www.williamrowson.com',
                'DOMAIN' => 'williamrowson.com',
            ),
            'poslovnapolitika.com' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_89_',
                'SITE_URL' => 'http://www.poslovnapolitika.com',
                'DOMAIN' => 'poslovnapolitika.com',
            ),
            'ecoenergybrasil.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1606_',
                'SITE_URL' => 'http://www.ecoenergybrasil.com',
                'DOMAIN' => 'ecoenergybrasil.com',
            ),
            'casino-hotel.us' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_61_',
                'SITE_URL' => 'http://www.casino-hotel.us',
                'DOMAIN' => 'casino-hotel.us',
            ),
            'digitalmark.me' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_66_',
                'SITE_URL' => 'http://www.digitalmark.me',
                'DOMAIN' => 'digitalmark.me',
            ),
            'cantuss.info' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_679_',
                'SITE_URL' => 'http://www.cantuss.info',
                'DOMAIN' => 'cantuss.info',
            ),
            '1windows8.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_72_',
                'SITE_URL' => 'http://www.1windows8.com',
                'DOMAIN' => '1windows8.com',
            ),
            'shippinghazmat.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_742_',
                'SITE_URL' => 'http://www.shippinghazmat.com',
                'DOMAIN' => 'shippinghazmat.com',
            ),
            'shoplederniercri.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_743_',
                'SITE_URL' => 'http://www.shoplederniercri.com',
                'DOMAIN' => 'shoplederniercri.com',
            ),
            'occupy-public-space.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_76_',
                'SITE_URL' => 'http://www.occupy-public-space.com',
                'DOMAIN' => 'occupy-public-space.com',
            ),
            'deadonguns.com' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1164_',
                'SITE_URL' => 'http://www.deadonguns.com',
                'DOMAIN' => 'deadonguns.com',
            ),
            'deboraharkrader.com' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1165_',
                'SITE_URL' => 'http://www.deboraharkrader.com',
                'DOMAIN' => 'deboraharkrader.com',
            ),
            '1866999.com' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1166_',
                'SITE_URL' => 'http://www.1866999.com',
                'DOMAIN' => '1866999.com',
            ),
            'dreambigcasting.net' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1167_',
                'SITE_URL' => 'http://www.dreambigcasting.net',
                'DOMAIN' => 'dreambigcasting.net',
            ),
            'disastersolutionslc.com' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1168_',
                'SITE_URL' => 'http://www.disastersolutionslc.com',
                'DOMAIN' => 'disastersolutionslc.com',
            ),
            'tapestrywalk.com' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1609_',
                'SITE_URL' => 'http://www.tapestrywalk.com',
                'DOMAIN' => 'tapestrywalk.com',
            ),
            'oemsoftware.info' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_854_',
                'SITE_URL' => 'http://www.oemsoftware.info',
                'DOMAIN' => 'oemsoftware.info',
            ),
            'nykmc.org' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_856_',
                'SITE_URL' => 'http://www.nykmc.org',
                'DOMAIN' => 'nykmc.org',
            ),
            'hermajestyssecretplayers.org' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_857_',
                'SITE_URL' => 'http://www.hermajestyssecretplayers.org',
                'DOMAIN' => 'hermajestyssecretplayers.org',
            ),
            'speakupvictoria.com' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_858_',
                'SITE_URL' => 'http://www.speakupvictoria.com',
                'DOMAIN' => 'speakupvictoria.com',
            ),
            'furgetmenot.biz' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_859_',
                'SITE_URL' => 'http://www.furgetmenot.biz',
                'DOMAIN' => 'furgetmenot.biz',
            ),
            '193.235.227.112' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_86_',
                'SITE_URL' => 'http://193.235.227.112//',
                'DOMAIN' => '193.235.227.112',
            ),
            'linksystocisco.com' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_87_',
                'SITE_URL' => 'http://linksystocisco.com',
                'DOMAIN' => 'linksystocisco.com',
            ),
            'livegiveskate.com' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_88_',
                'SITE_URL' => 'http://www.livegiveskate.com',
                'DOMAIN' => 'livegiveskate.com',
            ),
            'mail.lusofolie.com' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_89_',
                'SITE_URL' => 'http://mail.lusofolie.com',
                'DOMAIN' => 'mail.lusofolie.com',
            ),
            'parsons-table.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1290_',
                'SITE_URL' => 'http://www.parsons-table.com',
                'DOMAIN' => 'parsons-table.com',
            ),
            'propiedadesconestilo.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1291_',
                'SITE_URL' => 'http://www.propiedadesconestilo.com',
                'DOMAIN' => 'propiedadesconestilo.com',
            ),
            'qctqpus.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1292_',
                'SITE_URL' => 'http://www.qctqpus.com',
                'DOMAIN' => 'qctqpus.com',
            ),
            'rabighcip.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1293_',
                'SITE_URL' => 'http://www.rabighcip.com',
                'DOMAIN' => 'rabighcip.com',
            ),
            'oliveoakranch.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1294_',
                'SITE_URL' => 'http://www.oliveoakranch.com',
                'DOMAIN' => 'oliveoakranch.com',
            ),
            'bethemanofyourcave.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1608_',
                'SITE_URL' => 'http://www.bethemanofyourcave.com',
                'DOMAIN' => 'bethemanofyourcave.com',
            ),
            'occhiavarichairs4rent.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_929_',
                'SITE_URL' => 'http://www.occhiavarichairs4rent.com',
                'DOMAIN' => 'occhiavarichairs4rent.com',
            ),
            'orangecountyladjs.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_930_',
                'SITE_URL' => 'http://www.orangecountyladjs.com',
                'DOMAIN' => 'orangecountyladjs.com',
            ),
            'socalchiavarichairs.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_931_',
                'SITE_URL' => 'http://www.socalchiavarichairs.com',
                'DOMAIN' => 'socalchiavarichairs.com',
            ),
            'descriptiontoronto.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_932_',
                'SITE_URL' => 'http://www.descriptiontoronto.com',
                'DOMAIN' => 'descriptiontoronto.com',
            ),
            'ipisinternational.org' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_933_',
                'SITE_URL' => 'http://www.ipisinternational.org',
                'DOMAIN' => 'ipisinternational.org',
            ),
            'super8movie.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1607_',
                'SITE_URL' => 'http://www.super8movie.com',
                'DOMAIN' => 'super8movie.com',
            ),
            'jqueryrefuge.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_65_',
                'SITE_URL' => 'http://www.jqueryrefuge.com',
                'DOMAIN' => 'jqueryrefuge.com',
            ),
            'rentalmedia.net' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_68_',
                'SITE_URL' => 'http://www.rentalmedia.net',
                'DOMAIN' => 'rentalmedia.net',
            ),
            'uweillustration2012.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_70_',
                'SITE_URL' => 'http://www.uweillustration2012.com',
                'DOMAIN' => 'uweillustration2012.com',
            ),
            'lasemainedelapub.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_71_',
                'SITE_URL' => 'http://www.lasemainedelapub.com',
                'DOMAIN' => 'lasemainedelapub.com',
            ),
            'selmannforstaterep.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_740_',
                'SITE_URL' => 'http://www.selmannforstaterep.com',
                'DOMAIN' => 'selmannforstaterep.com',
            ),
            'shamrocked2012.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_741_',
                'SITE_URL' => 'http://www.shamrocked2012.com',
                'DOMAIN' => 'shamrocked2012.com',
            ),
            'educationreverse.com' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1611_',
                'SITE_URL' => 'http://www.educationreverse.com',
                'DOMAIN' => 'educationreverse.com',
            ),
            'optometricexpress.net' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_50_',
                'SITE_URL' => 'http://www.optometricexpress.net',
                'DOMAIN' => 'optometricexpress.net',
            ),
            'sotmary.com' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_62_',
                'SITE_URL' => 'http://www.sotmary.com',
                'DOMAIN' => 'sotmary.com',
            ),
            'quhuizhijiatexiaoyao.com' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_63_',
                'SITE_URL' => 'http://www.quhuizhijiatexiaoyao.com',
                'DOMAIN' => 'quhuizhijiatexiaoyao.com',
            ),
            'cisallecce.com' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_64_',
                'SITE_URL' => 'http://www.cisallecce.com',
                'DOMAIN' => 'cisallecce.com',
            ),
            'fatina.net' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_67_',
                'SITE_URL' => 'http://www.fatina.net',
                'DOMAIN' => 'fatina.net',
            ),
            'sail-ssa.org' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_737_',
                'SITE_URL' => 'http://www.sail-ssa.org',
                'DOMAIN' => 'sail-ssa.org',
            ),
            'sarahpalinwillshootyouintheface.com' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_738_',
                'SITE_URL' => 'http://www.sarahpalinwillshootyouintheface.com',
                'DOMAIN' => 'sarahpalinwillshootyouintheface.com',
            ),
            '0260groupnyc.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1593_',
                'SITE_URL' => 'http://www.0260groupnyc.com',
                'DOMAIN' => '0260groupnyc.com',
            ),
            'shepherdstownchalkartfestival.org' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_54_',
                'SITE_URL' => 'http://www.shepherdstownchalkartfestival.org',
                'DOMAIN' => 'shepherdstownchalkartfestival.org',
            ),
            'post342.org' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_55_',
                'SITE_URL' => 'http://www.post342.org',
                'DOMAIN' => 'post342.org',
            ),
            'yellowpandaartstudio.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_58_',
                'SITE_URL' => 'http://www.yellowpandaartstudio.com',
                'DOMAIN' => 'yellowpandaartstudio.com',
            ),
            'adphi-middlesex.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_60_',
                'SITE_URL' => 'http://www.adphi-middlesex.com',
                'DOMAIN' => 'adphi-middlesex.com',
            ),
            'route66restorationsinc.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_735_',
                'SITE_URL' => 'http://www.route66restorationsinc.com',
                'DOMAIN' => 'route66restorationsinc.com',
            ),
            'royalsulu.org' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_736_',
                'SITE_URL' => 'http://www.royalsulu.org',
                'DOMAIN' => 'royalsulu.org',
            ),
            'koacon2013.org' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1130_',
                'SITE_URL' => 'http://www.koacon2013.org',
                'DOMAIN' => 'koacon2013.org',
            ),
            'hudsonvalleylgbtqcenter.com' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1131_',
                'SITE_URL' => 'http://www.hudsonvalleylgbtqcenter.com',
                'DOMAIN' => 'hudsonvalleylgbtqcenter.com',
            ),
            'collegegoalsundaypr.com' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1132_',
                'SITE_URL' => 'http://www.collegegoalsundaypr.com',
                'DOMAIN' => 'collegegoalsundaypr.com',
            ),
            'hotelsinleedscitycentre.net' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1133_',
                'SITE_URL' => 'http://www.hotelsinleedscitycentre.net',
                'DOMAIN' => 'hotelsinleedscitycentre.net',
            ),
            'respect-tradition.us' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1134_',
                'SITE_URL' => 'http://www.respect-tradition.us',
                'DOMAIN' => 'respect-tradition.us',
            ),
            'fullsailsinfortlauderdale.com' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1612_',
                'SITE_URL' => 'http://www.fullsailsinfortlauderdale.com',
                'DOMAIN' => 'fullsailsinfortlauderdale.com',
            ),
            'oxfordindiansociety.com' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_909_',
                'SITE_URL' => 'http://www.oxfordindiansociety.com',
                'DOMAIN' => 'oxfordindiansociety.com',
            ),
            'jews4change.org' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_910_',
                'SITE_URL' => 'http://www.jews4change.org',
                'DOMAIN' => 'jews4change.org',
            ),
            'arduousblog.com' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_911_',
                'SITE_URL' => 'http://www.arduousblog.com',
                'DOMAIN' => 'arduousblog.com',
            ),
            'berkeleycivilsidewalks.com' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_912_',
                'SITE_URL' => 'http://www.berkeleycivilsidewalks.com',
                'DOMAIN' => 'berkeleycivilsidewalks.com',
            ),
            'cidhm.org' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_913_',
                'SITE_URL' => 'http://www.cidhm.org',
                'DOMAIN' => 'cidhm.org',
            ),
            'roanokepartyinthepark.com' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1613_',
                'SITE_URL' => 'http://www.roanokepartyinthepark.com',
                'DOMAIN' => 'roanokepartyinthepark.com',
            ),
            'camaemlak.com' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_47_',
                'SITE_URL' => 'http://www.camaemlak.com',
                'DOMAIN' => 'camaemlak.com',
            ),
            'ns-designer.com' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_49_',
                'SITE_URL' => 'http://www.ns-designer.com',
                'DOMAIN' => 'ns-designer.com',
            ),
            'coldsore.biz' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_56_',
                'SITE_URL' => 'http://www.coldsore.biz',
                'DOMAIN' => 'coldsore.biz',
            ),
            'genericana.com' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_57_',
                'SITE_URL' => 'http://www.genericana.com',
                'DOMAIN' => 'genericana.com',
            ),
            'fb68.info' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_59_',
                'SITE_URL' => 'http://www.fb68.info',
                'DOMAIN' => 'fb68.info',
            ),
            'roomba-review.org' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_732_',
                'SITE_URL' => 'http://www.roomba-review.org',
                'DOMAIN' => 'roomba-review.org',
            ),
            'rogermillerresortsales.com' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_733_',
                'SITE_URL' => 'http://www.rogermillerresortsales.com',
                'DOMAIN' => 'rogermillerresortsales.com',
            ),
            'stormforcewrestling.com' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_712_',
                'SITE_URL' => 'http://www.stormforcewrestling.com',
                'DOMAIN' => 'stormforcewrestling.com',
            ),
            'janddgraphics.com' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_713_',
                'SITE_URL' => 'http://www.janddgraphics.com',
                'DOMAIN' => 'janddgraphics.com',
            ),
            'adagencynewjersey.com' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_714_',
                'SITE_URL' => 'http://www.adagencynewjersey.com',
                'DOMAIN' => 'adagencynewjersey.com',
            ),
            'wcwestwinds.com' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_715_',
                'SITE_URL' => 'http://www.wcwestwinds.com',
                'DOMAIN' => 'wcwestwinds.com',
            ),
            'jameshburns.com' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_716_',
                'SITE_URL' => 'http://www.jameshburns.com',
                'DOMAIN' => 'jameshburns.com',
            ),
            'antarcticadventurebook.com' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1614_',
                'SITE_URL' => 'http://www.antarcticadventurebook.com',
                'DOMAIN' => 'antarcticadventurebook.com',
            ),
            'guiana1838.com' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_23_',
                'SITE_URL' => 'http://www.guiana1838.com',
                'DOMAIN' => 'guiana1838.com',
            ),
            'christianmilitarywives.net' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_48_',
                'SITE_URL' => 'http://www.christianmilitarywives.net',
                'DOMAIN' => 'christianmilitarywives.net',
            ),
            'chincoteagueislandrentals.com' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_51_',
                'SITE_URL' => 'http://www.chincoteagueislandrentals.com',
                'DOMAIN' => 'chincoteagueislandrentals.com',
            ),
            'austin-kimbermodernhotel.info' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_52_',
                'SITE_URL' => 'http://www.austin-kimbermodernhotel.info',
                'DOMAIN' => 'austin-kimbermodernhotel.info',
            ),
            'celg.org' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_53_',
                'SITE_URL' => 'http://www.celg.org',
                'DOMAIN' => 'celg.org',
            ),
            'rkvy-wbuafs.com' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_731_',
                'SITE_URL' => 'http://www.rkvy-wbuafs.com',
                'DOMAIN' => 'rkvy-wbuafs.com',
            ),
            'treasurecoastmustangs.com' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1330_',
                'SITE_URL' => 'http://www.treasurecoastmustangs.com',
                'DOMAIN' => 'treasurecoastmustangs.com',
            ),
            'vacationsgetawayincentives.com' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1331_',
                'SITE_URL' => 'http://www.vacationsgetawayincentives.com',
                'DOMAIN' => 'vacationsgetawayincentives.com',
            ),
            'yankeenetwork.net' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1332_',
                'SITE_URL' => 'http://www.yankeenetwork.net',
                'DOMAIN' => 'yankeenetwork.net',
            ),
            'aauw-atascadero.org' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1615_',
                'SITE_URL' => 'http://www.aauw-atascadero.org',
                'DOMAIN' => 'aauw-atascadero.org',
            ),
            'plantoplaymoredurham.com' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_971_',
                'SITE_URL' => 'http://www.plantoplaymoredurham.com',
                'DOMAIN' => 'plantoplaymoredurham.com',
            ),
            'fss2020.org' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_972_',
                'SITE_URL' => 'http://www.fss2020.org',
                'DOMAIN' => 'fss2020.org',
            ),
            'mauriciomessafotografia.com' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_973_',
                'SITE_URL' => 'http://www.mauriciomessafotografia.com',
                'DOMAIN' => 'mauriciomessafotografia.com',
            ),
            'distributeurdelest.com' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_974_',
                'SITE_URL' => 'http://www.distributeurdelest.com',
                'DOMAIN' => 'distributeurdelest.com',
            ),
            'lightstar.info' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_975_',
                'SITE_URL' => 'http://www.lightstar.info',
                'DOMAIN' => 'lightstar.info',
            ),
            'aclimocab.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1151_',
                'SITE_URL' => 'http://www.aclimocab.com',
                'DOMAIN' => 'aclimocab.com',
            ),
            'alteredrootsquartet.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1152_',
                'SITE_URL' => 'http://www.alteredrootsquartet.com',
                'DOMAIN' => 'alteredrootsquartet.com',
            ),
            'allthingssookie.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1472_',
                'SITE_URL' => 'http://www.allthingssookie.com',
                'DOMAIN' => 'allthingssookie.com',
            ),
            'parklandpresbyterian.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1674_',
                'SITE_URL' => 'http://www.parklandpresbyterian.com',
                'DOMAIN' => 'parklandpresbyterian.com',
            ),
            'poui.org' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_841_',
                'SITE_URL' => 'http://www.poui.org',
                'DOMAIN' => 'poui.org',
            ),
            'rachelibd.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_842_',
                'SITE_URL' => 'http://www.rachelibd.com',
                'DOMAIN' => 'rachelibd.com',
            ),
            'in-nell.org' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_843_',
                'SITE_URL' => 'http://www.in-nell.org',
                'DOMAIN' => 'in-nell.org',
            ),
            'assalcr.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_844_',
                'SITE_URL' => 'http://www.assalcr.com',
                'DOMAIN' => 'assalcr.com',
            ),
            'cucinamoreno.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_845_',
                'SITE_URL' => 'http://www.cucinamoreno.com',
                'DOMAIN' => 'cucinamoreno.com',
            ),
            'storyteller-artist.com' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1296_',
                'SITE_URL' => 'http://www.storyteller-artist.com',
                'DOMAIN' => 'storyteller-artist.com',
            ),
            'kfirbendet.com' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1616_',
                'SITE_URL' => 'http://www.kfirbendet.com',
                'DOMAIN' => 'kfirbendet.com',
            ),
            'rafalupion.com' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_934_',
                'SITE_URL' => 'http://www.rafalupion.com',
                'DOMAIN' => 'rafalupion.com',
            ),
            'boomeranked.com' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_935_',
                'SITE_URL' => 'http://www.boomeranked.com',
                'DOMAIN' => 'boomeranked.com',
            ),
            'esesme.org' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_936_',
                'SITE_URL' => 'http://www.esesme.org',
                'DOMAIN' => 'esesme.org',
            ),
            'ht-sc.org' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_937_',
                'SITE_URL' => 'http://www.ht-sc.org',
                'DOMAIN' => 'ht-sc.org',
            ),
            'graphoquebec.com' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_938_',
                'SITE_URL' => 'http://www.graphoquebec.com',
                'DOMAIN' => 'graphoquebec.com',
            ),
            'paintforthecause.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_70_',
                'SITE_URL' => 'http://www.paintforthecause.org',
                'DOMAIN' => 'paintforthecause.org',
            ),
            '4hinoc.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_71_',
                'SITE_URL' => 'http://www.4hinoc.org',
                'DOMAIN' => '4hinoc.org',
            ),
            'developnp.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_72_',
                'SITE_URL' => 'http://www.developnp.com',
                'DOMAIN' => 'developnp.com',
            ),
            'winnerschool.net' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_73_',
                'SITE_URL' => 'http://www.winnerschool.net',
                'DOMAIN' => 'winnerschool.net',
            ),
            'unetusmundos.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_74_',
                'SITE_URL' => 'http://www.unetusmundos.com',
                'DOMAIN' => 'unetusmundos.com',
            ),
            'eosiowa.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_75_',
                'SITE_URL' => 'http://www.eosiowa.com',
                'DOMAIN' => 'eosiowa.com',
            ),
            'museotextildeoaxaca.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_76_',
                'SITE_URL' => 'http://www.museotextildeoaxaca.org',
                'DOMAIN' => 'museotextildeoaxaca.org',
            ),
            'toddgrebeandcoldcountry.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_77_',
                'SITE_URL' => 'http://www.toddgrebeandcoldcountry.com',
                'DOMAIN' => 'toddgrebeandcoldcountry.com',
            ),
            'ycyc-registration.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_78_',
                'SITE_URL' => 'http://www.ycyc-registration.com',
                'DOMAIN' => 'ycyc-registration.com',
            ),
            'online-drugs.info' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1462_',
                'SITE_URL' => 'http://online-drugs.info',
                'DOMAIN' => 'online-drugs.info',
            ),
            'imaginingpericles.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_15_',
                'SITE_URL' => 'http://www.imaginingpericles.com',
                'DOMAIN' => 'imaginingpericles.com',
            ),
            'mu-sci.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1617_',
                'SITE_URL' => 'http://www.mu-sci.org',
                'DOMAIN' => 'mu-sci.org',
            ),
            'cfnpc.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_16_',
                'SITE_URL' => 'http://www.cfnpc.org',
                'DOMAIN' => 'cfnpc.org',
            ),
            'treat-hemorrhoids.us' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_22_',
                'SITE_URL' => 'http://www.treat-hemorrhoids.us',
                'DOMAIN' => 'treat-hemorrhoids.us',
            ),
            'hbxftc.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_24_',
                'SITE_URL' => 'http://www.hbxftc.org',
                'DOMAIN' => 'hbxftc.org',
            ),
            'anovadiaspora.org' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1125_',
                'SITE_URL' => 'http://www.anovadiaspora.org',
                'DOMAIN' => 'anovadiaspora.org',
            ),
            'faxbrt.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1126_',
                'SITE_URL' => 'http://www.faxbrt.com',
                'DOMAIN' => 'faxbrt.com',
            ),
            'mysummerwoodhome.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1127_',
                'SITE_URL' => 'http://www.mysummerwoodhome.com',
                'DOMAIN' => 'mysummerwoodhome.com',
            ),
            'artspacesailboatbend.org' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1128_',
                'SITE_URL' => 'http://www.artspacesailboatbend.org',
                'DOMAIN' => 'artspacesailboatbend.org',
            ),
            'clubsambil.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1129_',
                'SITE_URL' => 'http://www.clubsambil.com',
                'DOMAIN' => 'clubsambil.com',
            ),
            'juanjosemolina.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1618_',
                'SITE_URL' => 'http://www.juanjosemolina.com',
                'DOMAIN' => 'juanjosemolina.com',
            ),
            'saldarriagacompany.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_904_',
                'SITE_URL' => 'http://www.saldarriagacompany.com',
                'DOMAIN' => 'saldarriagacompany.com',
            ),
            'thcorodeopageant.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_905_',
                'SITE_URL' => 'http://www.thcorodeopageant.com',
                'DOMAIN' => 'thcorodeopageant.com',
            ),
            'artwurst.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_906_',
                'SITE_URL' => 'http://www.artwurst.com',
                'DOMAIN' => 'artwurst.com',
            ),
            'thecheesemongerswife.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_907_',
                'SITE_URL' => 'http://www.thecheesemongerswife.com',
                'DOMAIN' => 'thecheesemongerswife.com',
            ),
            'amartransgenics.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_908_',
                'SITE_URL' => 'http://www.amartransgenics.com',
                'DOMAIN' => 'amartransgenics.com',
            ),
            'namebuilder.net' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1351_',
                'SITE_URL' => 'http://www.namebuilder.net',
                'DOMAIN' => 'namebuilder.net',
            ),
            'p-q-a.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1352_',
                'SITE_URL' => 'http://www.p-q-a.com',
                'DOMAIN' => 'p-q-a.com',
            ),
            '101010runforpasigriver.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1353_',
                'SITE_URL' => 'http://www.101010runforpasigriver.com',
                'DOMAIN' => '101010runforpasigriver.com',
            ),
            'tcgme.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1619_',
                'SITE_URL' => 'http://www.tcgme.com',
                'DOMAIN' => 'tcgme.com',
            ),
            'sarah-coleman.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_687_',
                'SITE_URL' => 'http://www.sarah-coleman.com',
                'DOMAIN' => 'sarah-coleman.com',
            ),
            'selecthotelnashville.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_688_',
                'SITE_URL' => 'http://www.selecthotelnashville.com',
                'DOMAIN' => 'selecthotelnashville.com',
            ),
            'shevetmezada.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_689_',
                'SITE_URL' => 'http://www.shevetmezada.com',
                'DOMAIN' => 'shevetmezada.com',
            ),
            'smizgraffiti.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_690_',
                'SITE_URL' => 'http://www.smizgraffiti.com',
                'DOMAIN' => 'smizgraffiti.com',
            ),
            'silente.info' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_691_',
                'SITE_URL' => 'http://www.silente.info',
                'DOMAIN' => 'silente.info',
            ),
            'ciaraconsidine.com' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1096_',
                'SITE_URL' => 'http://www.ciaraconsidine.com',
                'DOMAIN' => 'ciaraconsidine.com',
            ),
            'crappyholidays.net' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1097_',
                'SITE_URL' => 'http://www.crappyholidays.net',
                'DOMAIN' => 'crappyholidays.net',
            ),
            'thecolossus.me' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1620_',
                'SITE_URL' => 'http://www.thecolossus.me',
                'DOMAIN' => 'thecolossus.me',
            ),
            'eyelovefashion.com' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_836_',
                'SITE_URL' => 'http://www.eyelovefashion.com',
                'DOMAIN' => 'eyelovefashion.com',
            ),
            'smhsevents.org' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_837_',
                'SITE_URL' => 'http://www.smhsevents.org',
                'DOMAIN' => 'smhsevents.org',
            ),
            'dagnabbitdcp.com' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_838_',
                'SITE_URL' => 'http://www.dagnabbitdcp.com',
                'DOMAIN' => 'dagnabbitdcp.com',
            ),
            'teamgleasonexperience.org' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_839_',
                'SITE_URL' => 'http://www.teamgleasonexperience.org',
                'DOMAIN' => 'teamgleasonexperience.org',
            ),
            'b672.com' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_840_',
                'SITE_URL' => 'http://www.b672.com',
                'DOMAIN' => 'b672.com',
            ),
            'theroflnetwork.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1140_',
                'SITE_URL' => 'http://www.theroflnetwork.com',
                'DOMAIN' => 'theroflnetwork.com',
            ),
            'nuitdesmines.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1141_',
                'SITE_URL' => 'http://www.nuitdesmines.com',
                'DOMAIN' => 'nuitdesmines.com',
            ),
            'savetheigloo.info' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1142_',
                'SITE_URL' => 'http://www.savetheigloo.info',
                'DOMAIN' => 'savetheigloo.info',
            ),
            'misanunciosprofesionales.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1143_',
                'SITE_URL' => 'http://www.misanunciosprofesionales.com',
                'DOMAIN' => 'misanunciosprofesionales.com',
            ),
            'viewvanguard.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1144_',
                'SITE_URL' => 'http://www.viewvanguard.com',
                'DOMAIN' => 'viewvanguard.com',
            ),
            'nemanjakrecelj.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1347_',
                'SITE_URL' => 'http://www.nemanjakrecelj.com',
                'DOMAIN' => 'nemanjakrecelj.com',
            ),
            'willhobbsoregon.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1348_',
                'SITE_URL' => 'http://www.willhobbsoregon.com',
                'DOMAIN' => 'willhobbsoregon.com',
            ),
            'advantagepandrcorp.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1349_',
                'SITE_URL' => 'http://www.advantagepandrcorp.com',
                'DOMAIN' => 'advantagepandrcorp.com',
            ),
            'elkmtnwy.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1350_',
                'SITE_URL' => 'http://www.elkmtnwy.com',
                'DOMAIN' => 'elkmtnwy.com',
            ),
            'calendarionuez.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1621_',
                'SITE_URL' => 'http://www.calendarionuez.com',
                'DOMAIN' => 'calendarionuez.com',
            ),
            'soudertoncareandshare.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_919_',
                'SITE_URL' => 'http://www.soudertoncareandshare.com',
                'DOMAIN' => 'soudertoncareandshare.com',
            ),
            'yeuxdor.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_920_',
                'SITE_URL' => 'http://www.yeuxdor.com',
                'DOMAIN' => 'yeuxdor.com',
            ),
            'avntubes.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_921_',
                'SITE_URL' => 'http://www.avntubes.com',
                'DOMAIN' => 'avntubes.com',
            ),
            'curtinaiesec.org' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_922_',
                'SITE_URL' => 'http://www.curtinaiesec.org',
                'DOMAIN' => 'curtinaiesec.org',
            ),
            'festivaldetiteresrosetearanda.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_923_',
                'SITE_URL' => 'http://www.festivaldetiteresrosetearanda.com',
                'DOMAIN' => 'festivaldetiteresrosetearanda.com',
            ),
            'businesslinc-fw.org' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1338_',
                'SITE_URL' => 'http://www.businesslinc-fw.org',
                'DOMAIN' => 'businesslinc-fw.org',
            ),
            'whypledging.net' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1622_',
                'SITE_URL' => 'http://www.whypledging.net',
                'DOMAIN' => 'whypledging.net',
            ),
            'stanzelcommunications.com' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_986_',
                'SITE_URL' => 'http://www.stanzelcommunications.com',
                'DOMAIN' => 'stanzelcommunications.com',
            ),
            'jorgealonso.net' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_987_',
                'SITE_URL' => 'http://www.jorgealonso.net',
                'DOMAIN' => 'jorgealonso.net',
            ),
            'regenerationfestival.us' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_988_',
                'SITE_URL' => 'http://www.regenerationfestival.us',
                'DOMAIN' => 'regenerationfestival.us',
            ),
            'sckappadelta.com' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_989_',
                'SITE_URL' => 'http://www.sckappadelta.com',
                'DOMAIN' => 'sckappadelta.com',
            ),
            'themagicposition.net' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_990_',
                'SITE_URL' => 'http://www.themagicposition.net',
                'DOMAIN' => 'themagicposition.net',
            ),
            'thetaxcredits.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1623_',
                'SITE_URL' => 'http://www.thetaxcredits.com',
                'DOMAIN' => 'thetaxcredits.com',
            ),
            'southeastfutsalchampionship.info' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_27_',
                'SITE_URL' => 'http://www.southeastfutsalchampionship.info',
                'DOMAIN' => 'southeastfutsalchampionship.info',
            ),
            'gryffynskeep.org' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_32_',
                'SITE_URL' => 'http://www.gryffynskeep.org',
                'DOMAIN' => 'gryffynskeep.org',
            ),
            'jltele1000.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_40_',
                'SITE_URL' => 'http://www.jltele1000.com',
                'DOMAIN' => 'jltele1000.com',
            ),
            'pemra.org' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_721_',
                'SITE_URL' => 'http://www.pemra.org',
                'DOMAIN' => 'pemra.org',
            ),
            'daniellenicoleadornments.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_73_',
                'SITE_URL' => 'http://www.daniellenicoleadornments.com',
                'DOMAIN' => 'daniellenicoleadornments.com',
            ),
            'faqjajote.com' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1675_',
                'SITE_URL' => 'http://www.faqjajote.com',
                'DOMAIN' => 'faqjajote.com',
            ),
            'katherinemorrison.com' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_3_',
                'SITE_URL' => 'http://www.katherinemorrison.com',
                'DOMAIN' => 'katherinemorrison.com',
            ),
            'ohioaidshivstdhotline.info' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_4_',
                'SITE_URL' => 'http://www.ohioaidshivstdhotline.info',
                'DOMAIN' => 'ohioaidshivstdhotline.info',
            ),
            'victory-fellowship.org' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_5_',
                'SITE_URL' => 'http://www.victory-fellowship.org',
                'DOMAIN' => 'victory-fellowship.org',
            ),
            'wheel-of-chance-wheels.com' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_6_',
                'SITE_URL' => 'http://www.wheel-of-chance-wheels.com',
                'DOMAIN' => 'wheel-of-chance-wheels.com',
            ),
            'ourfloridapromise.org' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_718_',
                'SITE_URL' => 'http://www.ourfloridapromise.org',
                'DOMAIN' => 'ourfloridapromise.org',
            ),
            'orangehara.com' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_719_',
                'SITE_URL' => 'http://www.orangehara.com',
                'DOMAIN' => 'orangehara.com',
            ),
            'newjeweleddogcollars.com' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1303_',
                'SITE_URL' => 'http://www.newjeweleddogcollars.com',
                'DOMAIN' => 'newjeweleddogcollars.com',
            ),
            'pellegrinosrestaurant.com' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1304_',
                'SITE_URL' => 'http://www.pellegrinosrestaurant.com',
                'DOMAIN' => 'pellegrinosrestaurant.com',
            ),
            'mfathleticclub.com' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1306_',
                'SITE_URL' => 'http://www.mfathleticclub.com',
                'DOMAIN' => 'mfathleticclub.com',
            ),
            'suntechautoglasslascrucesnm.com' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1624_',
                'SITE_URL' => 'http://www.suntechautoglasslascrucesnm.com',
                'DOMAIN' => 'suntechautoglasslascrucesnm.com',
            ),
            'tecaltema.org' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_944_',
                'SITE_URL' => 'http://www.tecaltema.org',
                'DOMAIN' => 'tecaltema.org',
            ),
            'montanaclimatechange.net' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_945_',
                'SITE_URL' => 'http://www.montanaclimatechange.net',
                'DOMAIN' => 'montanaclimatechange.net',
            ),
            'lsondemand.com' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_946_',
                'SITE_URL' => 'http://www.lsondemand.com',
                'DOMAIN' => 'lsondemand.com',
            ),
            'roadrunnernh.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1625_',
                'SITE_URL' => 'http://www.roadrunnernh.com',
                'DOMAIN' => 'roadrunnernh.com',
            ),
            'xinhualou.info' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_25_',
                'SITE_URL' => 'http://xinhualou.info',
                'DOMAIN' => 'xinhualou.info',
            ),
            'nywzsh.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_26_',
                'SITE_URL' => 'http://www.nywzsh.com',
                'DOMAIN' => 'nywzsh.com',
            ),
            'hao6767.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_39_',
                'SITE_URL' => 'http://www.hao6767.com',
                'DOMAIN' => 'hao6767.com',
            ),
            'foodnotlawnslincoln.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_41_',
                'SITE_URL' => 'http://www.foodnotlawnslincoln.com',
                'DOMAIN' => 'foodnotlawnslincoln.com',
            ),
            'pacific-ecosystems-climate.org' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_43_',
                'SITE_URL' => 'http://www.pacific-ecosystems-climate.org',
                'DOMAIN' => 'pacific-ecosystems-climate.org',
            ),
            'rjwtransport.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_729_',
                'SITE_URL' => 'http://www.rjwtransport.com',
                'DOMAIN' => 'rjwtransport.com',
            ),
            'resourcedrillalert.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_730_',
                'SITE_URL' => 'http://www.resourcedrillalert.com',
                'DOMAIN' => 'resourcedrillalert.com',
            ),
            'habitech2013.com' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1665_',
                'SITE_URL' => 'http://www.habitech2013.com',
                'DOMAIN' => 'habitech2013.com',
            ),
            'amitycares.org' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_29_',
                'SITE_URL' => 'http://www.amitycares.org',
                'DOMAIN' => 'amitycares.org',
            ),
            'apocalypseguild.info' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_45_',
                'SITE_URL' => 'http://www.apocalypseguild.info',
                'DOMAIN' => 'apocalypseguild.info',
            ),
            'greenprojectblog.org' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_46_',
                'SITE_URL' => 'http://www.greenprojectblog.org',
                'DOMAIN' => 'greenprojectblog.org',
            ),
            'fatsuperman.net' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_681_',
                'SITE_URL' => 'http://www.fatsuperman.net',
                'DOMAIN' => 'fatsuperman.net',
            ),
            'redobservatoriosterritoriales.com' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_727_',
                'SITE_URL' => 'http://www.redobservatoriosterritoriales.com',
                'DOMAIN' => 'redobservatoriosterritoriales.com',
            ),
            'resikla.com' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_728_',
                'SITE_URL' => 'http://www.resikla.com',
                'DOMAIN' => 'resikla.com',
            ),
            'wzeqy.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1626_',
                'SITE_URL' => 'http://www.wzeqy.com',
                'DOMAIN' => 'wzeqy.com',
            ),
            'posiciona2.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_31_',
                'SITE_URL' => 'http://www.posiciona2.com',
                'DOMAIN' => 'posiciona2.com',
            ),
            'persimmonparkfremont.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_34_',
                'SITE_URL' => 'http://www.persimmonparkfremont.com',
                'DOMAIN' => 'persimmonparkfremont.com',
            ),
            'kristiriskforcongress.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_36_',
                'SITE_URL' => 'http://www.kristiriskforcongress.com',
                'DOMAIN' => 'kristiriskforcongress.com',
            ),
            'hagane.us' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_42_',
                'SITE_URL' => 'http://www.hagane.us',
                'DOMAIN' => 'hagane.us',
            ),
            'kusadasikentkonseyi.org' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_44_',
                'SITE_URL' => 'http://www.kusadasikentkonseyi.org',
                'DOMAIN' => 'kusadasikentkonseyi.org',
            ),
            'rcgjy.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_725_',
                'SITE_URL' => 'http://www.rcgjy.com',
                'DOMAIN' => 'rcgjy.com',
            ),
            'puntozerodesign.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_726_',
                'SITE_URL' => 'http://www.puntozerodesign.com',
                'DOMAIN' => 'puntozerodesign.com',
            ),
            'cfa2012tampa.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1115_',
                'SITE_URL' => 'http://www.cfa2012tampa.com',
                'DOMAIN' => 'cfa2012tampa.com',
            ),
            'youthhonoravet.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1116_',
                'SITE_URL' => 'http://www.youthhonoravet.com',
                'DOMAIN' => 'youthhonoravet.com',
            ),
            'onyxtheater.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1117_',
                'SITE_URL' => 'http://www.onyxtheater.com',
                'DOMAIN' => 'onyxtheater.com',
            ),
            'cd4dfl.org' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1118_',
                'SITE_URL' => 'http://www.cd4dfl.org',
                'DOMAIN' => 'cd4dfl.org',
            ),
            'duatmush.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1119_',
                'SITE_URL' => 'http://www.duatmush.com',
                'DOMAIN' => 'duatmush.com',
            ),
            'freebb.info' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1627_',
                'SITE_URL' => 'http://www.freebb.info',
                'DOMAIN' => 'freebb.info',
            ),
            'thetexanwhowouldbeking.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_896_',
                'SITE_URL' => 'http://www.thetexanwhowouldbeking.com',
                'DOMAIN' => 'thetexanwhowouldbeking.com',
            ),
            'thebestavailablescience.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_898_',
                'SITE_URL' => 'http://thebestavailablescience.com',
                'DOMAIN' => 'thebestavailablescience.com',
            ),
            'djsmithaccounting.net' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_722_',
                'SITE_URL' => 'http://www.djsmithaccounting.net',
                'DOMAIN' => 'djsmithaccounting.net',
            ),
            'nationwidescooters.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_723_',
                'SITE_URL' => 'http://www.nationwidescooters.com',
                'DOMAIN' => 'nationwidescooters.com',
            ),
            'business-communication.info' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_724_',
                'SITE_URL' => 'http://www.business-communication.info',
                'DOMAIN' => 'business-communication.info',
            ),
            'cruise-ship-jobs.us' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_725_',
                'SITE_URL' => 'http://www.cruise-ship-jobs.us',
                'DOMAIN' => 'cruise-ship-jobs.us',
            ),
            'tubesdirectory.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_726_',
                'SITE_URL' => 'http://www.tubesdirectory.com',
                'DOMAIN' => 'tubesdirectory.com',
            ),
            'sebastiancomedy.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_13_',
                'SITE_URL' => 'http://www.sebastiancomedy.com',
                'DOMAIN' => 'sebastiancomedy.com',
            ),
            'ampaharmonia.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1456_',
                'SITE_URL' => 'http://www.ampaharmonia.com',
                'DOMAIN' => 'ampaharmonia.com',
            ),
            'ecnucoast.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_14_',
                'SITE_URL' => 'http://www.ecnucoast.com',
                'DOMAIN' => 'ecnucoast.com',
            ),
            'connectnapier.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1628_',
                'SITE_URL' => 'http://www.connectnapier.com',
                'DOMAIN' => 'connectnapier.com',
            ),
            'admclubman.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_17_',
                'SITE_URL' => 'http://www.admclubman.com',
                'DOMAIN' => 'admclubman.com',
            ),
            'postleftanarchy.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_723_',
                'SITE_URL' => 'http://www.postleftanarchy.com',
                'DOMAIN' => 'postleftanarchy.com',
            ),
            'desigvalencia.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1199_',
                'SITE_URL' => 'http://www.desigvalencia.com',
                'DOMAIN' => 'desigvalencia.com',
            ),
            'digitalgraphicsinform.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1200_',
                'SITE_URL' => 'http://www.digitalgraphicsinform.com',
                'DOMAIN' => 'digitalgraphicsinform.com',
            ),
            'dreadfilms.org' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1201_',
                'SITE_URL' => 'http://www.dreadfilms.org',
                'DOMAIN' => 'dreadfilms.org',
            ),
            'earlzpearlz.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1202_',
                'SITE_URL' => 'http://www.earlzpearlz.com',
                'DOMAIN' => 'earlzpearlz.com',
            ),
            'jactellconstruction.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1203_',
                'SITE_URL' => 'http://www.jactellconstruction.com',
                'DOMAIN' => 'jactellconstruction.com',
            ),
            'estuariosanjuan.org' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1475_',
                'SITE_URL' => 'http://www.estuariosanjuan.org',
                'DOMAIN' => 'estuariosanjuan.org',
            ),
            'gamecocksocialrewards.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_807_',
                'SITE_URL' => 'http://www.gamecocksocialrewards.com',
                'DOMAIN' => 'gamecocksocialrewards.com',
            ),
            'outletshoes-christianlouboutin.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_808_',
                'SITE_URL' => 'http://www.outletshoes-christianlouboutin.com',
                'DOMAIN' => 'outletshoes-christianlouboutin.com',
            ),
            'sniblog.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_809_',
                'SITE_URL' => 'http://www.sniblog.com',
                'DOMAIN' => 'sniblog.com',
            ),
            'vmgsails.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_810_',
                'SITE_URL' => 'http://www.vmgsails.com',
                'DOMAIN' => 'vmgsails.com',
            ),
            'isspa2009.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_811_',
                'SITE_URL' => 'http://www.isspa2009.com',
                'DOMAIN' => 'isspa2009.com',
            ),
            'ppvalencia.com' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_10_',
                'SITE_URL' => 'http://www.ppvalencia.com',
                'DOMAIN' => 'ppvalencia.com',
            ),
            'retrojordansalescheap.com' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_11_',
                'SITE_URL' => 'http://www.retrojordansalescheap.com',
                'DOMAIN' => 'retrojordansalescheap.com',
            ),
            'santyesprogramadorynografista.net' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_12_',
                'SITE_URL' => 'http://www.santyesprogramadorynografista.net',
                'DOMAIN' => 'santyesprogramadorynografista.net',
            ),
            'conservation-usa.com' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1629_',
                'SITE_URL' => 'http://www.conservation-usa.com',
                'DOMAIN' => 'conservation-usa.com',
            ),
            'pivotpointfilms.com' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_722_',
                'SITE_URL' => 'http://www.pivotpointfilms.com',
                'DOMAIN' => 'pivotpointfilms.com',
            ),
            'grupo-de-apoyo.org' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_8_',
                'SITE_URL' => 'http://www.grupo-de-apoyo.org',
                'DOMAIN' => 'grupo-de-apoyo.org',
            ),
            'inletemergencyservices.com' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_9_',
                'SITE_URL' => 'inletemergencyservices.com',
                'DOMAIN' => 'inletemergencyservices.com',
            ),
            'calvaryshreveport.net' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1019_',
                'SITE_URL' => 'http://www.calvaryshreveport.net',
                'DOMAIN' => 'calvaryshreveport.net',
            ),
            'insightliberia.com' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1630_',
                'SITE_URL' => 'http://www.insightliberia.com',
                'DOMAIN' => 'insightliberia.com',
            ),
            'bendcyclery.org' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_322_',
                'SITE_URL' => 'http://www.bendcyclery.org',
                'DOMAIN' => 'bendcyclery.org',
            ),
            'illustratorsforkids.com' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_324_',
                'SITE_URL' => 'http://www.illustratorsforkids.com',
                'DOMAIN' => 'illustratorsforkids.com',
            ),
            'fucopez.net' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_337_',
                'SITE_URL' => 'http://www.fucopez.net',
                'DOMAIN' => 'fucopez.net',
            ),
            'chambersburgreads.org' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_343_',
                'SITE_URL' => 'http://www.chambersburgreads.org',
                'DOMAIN' => 'chambersburgreads.org',
            ),
            'whatisproblem.com' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_365_',
                'SITE_URL' => 'http://www.whatisproblem.com',
                'DOMAIN' => 'whatisproblem.com',
            ),
            'anualhost.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1017_',
                'SITE_URL' => 'http://www.anualhost.com',
                'DOMAIN' => 'anualhost.com',
            ),
            'baltimorefitnesstraining.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1018_',
                'SITE_URL' => 'http://www.baltimorefitnesstraining.com',
                'DOMAIN' => 'baltimorefitnesstraining.com',
            ),
            'genericviagraorder.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1631_',
                'SITE_URL' => 'http://www.genericviagraorder.com',
                'DOMAIN' => 'genericviagraorder.com',
            ),
            'wyandottefsc.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_314_',
                'SITE_URL' => 'http://www.wyandottefsc.com',
                'DOMAIN' => 'wyandottefsc.com',
            ),
            'leminotaure.org' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_325_',
                'SITE_URL' => 'http://www.leminotaure.org',
                'DOMAIN' => 'leminotaure.org',
            ),
            'johnmuirpta.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_330_',
                'SITE_URL' => 'http://www.johnmuirpta.com',
                'DOMAIN' => 'johnmuirpta.com',
            ),
            'rousseau-2012.net' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_332_',
                'SITE_URL' => 'http://www.rousseau-2012.net',
                'DOMAIN' => 'rousseau-2012.net',
            ),
            'extreme-linux.org' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_334_',
                'SITE_URL' => 'http://www.extreme-linux.org',
                'DOMAIN' => 'extreme-linux.org',
            ),
            'beachresorts.biz' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1016_',
                'SITE_URL' => 'http://www.beachresorts.biz',
                'DOMAIN' => 'beachresorts.biz',
            ),
            'forex-now.info' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1285_',
                'SITE_URL' => 'http://www.forex-now.info',
                'DOMAIN' => 'forex-now.info',
            ),
            'forexadvice101.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1286_',
                'SITE_URL' => 'http://www.forexadvice101.com',
                'DOMAIN' => 'forexadvice101.com',
            ),
            'freedommedicalrobotics.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1287_',
                'SITE_URL' => 'http://www.freedommedicalrobotics.com',
                'DOMAIN' => 'freedommedicalrobotics.com',
            ),
            'gardenstatebhangra.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1288_',
                'SITE_URL' => 'http://www.gardenstatebhangra.com',
                'DOMAIN' => 'gardenstatebhangra.com',
            ),
            'rightbelstaff.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1632_',
                'SITE_URL' => 'http://www.rightbelstaff.com',
                'DOMAIN' => 'rightbelstaff.com',
            ),
            'zglf1890.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_315_',
                'SITE_URL' => 'http://www.zglf1890.com',
                'DOMAIN' => 'zglf1890.com',
            ),
            'playgroundmusic.us' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_327_',
                'SITE_URL' => 'http://www.playgroundmusic.us',
                'DOMAIN' => 'playgroundmusic.us',
            ),
            'wetestyouprofit.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_328_',
                'SITE_URL' => 'http://www.wetestyouprofit.com',
                'DOMAIN' => 'wetestyouprofit.com',
            ),
            'epicstreetchoir.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_331_',
                'SITE_URL' => 'http://www.epicstreetchoir.com',
                'DOMAIN' => 'epicstreetchoir.com',
            ),
            'studentemp.org' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_336_',
                'SITE_URL' => 'http://www.studentemp.org',
                'DOMAIN' => 'studentemp.org',
            ),
            'keaganspub.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1209_',
                'SITE_URL' => 'http://www.keaganspub.com',
                'DOMAIN' => 'keaganspub.com',
            ),
            'leoestevez.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1210_',
                'SITE_URL' => 'http://www.leoestevez.com',
                'DOMAIN' => 'leoestevez.com',
            ),
            'levismithmusic.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1211_',
                'SITE_URL' => 'http://www.levismithmusic.com',
                'DOMAIN' => 'levismithmusic.com',
            ),
            'loathinglola.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1212_',
                'SITE_URL' => 'http://www.loathinglola.com',
                'DOMAIN' => 'loathinglola.com',
            ),
            'marketingbooster.biz' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1213_',
                'SITE_URL' => 'http://www.marketingbooster.biz',
                'DOMAIN' => 'marketingbooster.biz',
            ),
            'cityberryclub.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1474_',
                'SITE_URL' => 'http://www.cityberryclub.com',
                'DOMAIN' => 'cityberryclub.com',
            ),
            'karagozmuzesi.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_817_',
                'SITE_URL' => 'http://www.karagozmuzesi.com',
                'DOMAIN' => 'karagozmuzesi.com',
            ),
            'lospinos50k.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_818_',
                'SITE_URL' => 'http://www.lospinos50k.com',
                'DOMAIN' => 'lospinos50k.com',
            ),
            'mansfieldoilonline.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_819_',
                'SITE_URL' => 'http://www.mansfieldoilonline.com',
                'DOMAIN' => 'mansfieldoilonline.com',
            ),
            'siegenprofessional.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_820_',
                'SITE_URL' => 'http://www.siegenprofessional.com',
                'DOMAIN' => 'siegenprofessional.com',
            ),
            'sigmanuhouston.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_821_',
                'SITE_URL' => 'http://www.sigmanuhouston.com',
                'DOMAIN' => 'sigmanuhouston.com',
            ),
            'abaima.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_21_',
                'SITE_URL' => 'http://www.abaima.com',
                'DOMAIN' => 'abaima.com',
            ),
            'accas2011.org' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_22_',
                'SITE_URL' => 'http://www.accas2011.org',
                'DOMAIN' => 'accas2011.org',
            ),
            'thechicagohubrestaurant.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_23_',
                'SITE_URL' => 'http://www.thechicagohubrestaurant.com',
                'DOMAIN' => 'thechicagohubrestaurant.com',
            ),
            'thesalesoperator.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_24_',
                'SITE_URL' => 'http://www.thesalesoperator.com',
                'DOMAIN' => 'thesalesoperator.com',
            ),
            'cheapcomputers.biz' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_25_',
                'SITE_URL' => 'http://www.cheapcomputers.biz',
                'DOMAIN' => 'cheapcomputers.biz',
            ),
            'roanokesbdc.org' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_26_',
                'SITE_URL' => 'http://www.roanokesbdc.org',
                'DOMAIN' => 'roanokesbdc.org',
            ),
            'omurraygolf.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1014_',
                'SITE_URL' => 'http://www.omurraygolf.com',
                'DOMAIN' => 'omurraygolf.com',
            ),
            'orangetiecomputers.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1015_',
                'SITE_URL' => 'http://www.orangetiecomputers.com',
                'DOMAIN' => 'orangetiecomputers.com',
            ),
            'greenherbgarden.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1280_',
                'SITE_URL' => 'http://www.greenherbgarden.com',
                'DOMAIN' => 'greenherbgarden.com',
            ),
            'greenteaandhealth.info' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1281_',
                'SITE_URL' => 'http://www.greenteaandhealth.info',
                'DOMAIN' => 'greenteaandhealth.info',
            ),
            'interui.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1282_',
                'SITE_URL' => 'http://www.interui.com',
                'DOMAIN' => 'interui.com',
            ),
            'equityhomeloanscredit.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1283_',
                'SITE_URL' => 'http://www.equityhomeloanscredit.com',
                'DOMAIN' => 'equityhomeloanscredit.com',
            ),
            'emarketingmeister.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1284_',
                'SITE_URL' => 'http://www.emarketingmeister.com',
                'DOMAIN' => 'emarketingmeister.com',
            ),
            'buildhealthyhomes.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1451_',
                'SITE_URL' => 'http://www.buildhealthyhomes.com',
                'DOMAIN' => 'buildhealthyhomes.com',
            ),
            'buildmyadobehome.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1452_',
                'SITE_URL' => 'http://www.buildmyadobehome.com',
                'DOMAIN' => 'buildmyadobehome.com',
            ),
            'cebglobalhealth.org' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1453_',
                'SITE_URL' => 'http://www.cebglobalhealth.org',
                'DOMAIN' => 'cebglobalhealth.org',
            ),
            'chickenrecipesno1.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1454_',
                'SITE_URL' => 'http://www.chickenrecipesno1.com',
                'DOMAIN' => 'chickenrecipesno1.com',
            ),
            'chipmandentalproducts.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1455_',
                'SITE_URL' => 'http://www.chipmandentalproducts.com',
                'DOMAIN' => 'chipmandentalproducts.com',
            ),
            'ourbelstaff.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1633_',
                'SITE_URL' => 'http://www.ourbelstaff.com',
                'DOMAIN' => 'ourbelstaff.com',
            ),
            'pallatraxusa.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_311_',
                'SITE_URL' => 'http://www.pallatraxusa.com',
                'DOMAIN' => 'pallatraxusa.com',
            ),
            'delshannoncarshow.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_312_',
                'SITE_URL' => 'http://www.delshannoncarshow.com',
                'DOMAIN' => 'delshannoncarshow.com',
            ),
            'bcpba.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_323_',
                'SITE_URL' => 'http://www.bcpba.com',
                'DOMAIN' => 'bcpba.com',
            ),
            'merchantaccountusa.us' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_329_',
                'SITE_URL' => 'http://www.merchantaccountusa.us',
                'DOMAIN' => 'merchantaccountusa.us',
            ),
            'upstairsroomwithaview.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_364_',
                'SITE_URL' => 'http://www.upstairsroomwithaview.com',
                'DOMAIN' => 'upstairsroomwithaview.com',
            ),
            'ecommercedesigns.biz' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1275_',
                'SITE_URL' => 'http://www.ecommercedesigns.biz',
                'DOMAIN' => 'ecommercedesigns.biz',
            ),
            'fitness-health-nutrition.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1276_',
                'SITE_URL' => 'http://www.fitness-health-nutrition.com',
                'DOMAIN' => 'fitness-health-nutrition.com',
            ),
            'fusefitnessplan.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1277_',
                'SITE_URL' => 'http://www.fusefitnessplan.com',
                'DOMAIN' => 'fusefitnessplan.com',
            ),
            'footfetishpictures.org' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1278_',
                'SITE_URL' => 'http://www.footfetishpictures.org',
                'DOMAIN' => 'footfetishpictures.org',
            ),
            'g2gtomyspace.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1279_',
                'SITE_URL' => 'http://www.g2gtomyspace.com',
                'DOMAIN' => 'g2gtomyspace.com',
            ),
            'auctiontwo.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1446_',
                'SITE_URL' => 'http://www.auctiontwo.com',
                'DOMAIN' => 'auctiontwo.com',
            ),
            'atlantahostexpert.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1447_',
                'SITE_URL' => 'http://www.atlantahostexpert.com',
                'DOMAIN' => 'atlantahostexpert.com',
            ),
            'xxx-cite.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1448_',
                'SITE_URL' => 'http://www.xxx-cite.com',
                'DOMAIN' => 'xxx-cite.com',
            ),
            'bookclique.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1449_',
                'SITE_URL' => 'http://www.bookclique.com',
                'DOMAIN' => 'bookclique.com',
            ),
            'brewcitybloodbath.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1450_',
                'SITE_URL' => 'http://www.brewcitybloodbath.com',
                'DOMAIN' => 'brewcitybloodbath.com',
            ),
            'cheapviagraonlinetng.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1636_',
                'SITE_URL' => 'http://www.cheapviagraonlinetng.com',
                'DOMAIN' => 'cheapviagraonlinetng.com',
            ),
            'noplan2006.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_316_',
                'SITE_URL' => 'http://www.noplan2006.com',
                'DOMAIN' => 'noplan2006.com',
            ),
            'cchexp.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_317_',
                'SITE_URL' => 'http://www.cchexp.com',
                'DOMAIN' => 'cchexp.com',
            ),
            'ramthestrait.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_319_',
                'SITE_URL' => 'http://www.ramthestrait.com',
                'DOMAIN' => 'ramthestrait.com',
            ),
            'streetkidscusco.org' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_320_',
                'SITE_URL' => 'http://www.streetkidscusco.org',
                'DOMAIN' => 'streetkidscusco.org',
            ),
            'belize-maya-2012.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_321_',
                'SITE_URL' => 'http://www.belize-maya-2012.com',
                'DOMAIN' => 'belize-maya-2012.com',
            ),
            'aunikefreerun.net' =>
            array(
                'TABLE_SCHEMA' => 'videohdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1634_',
                'SITE_URL' => 'http://www.aunikefreerun.net',
                'DOMAIN' => 'aunikefreerun.net',
            ),
            'videohd.ws' =>
            array(
                'TABLE_SCHEMA' => 'videohdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_692_',
                'SITE_URL' => 'http://www.videohd.ws',
                'DOMAIN' => 'videohd.ws',
            ),
            'mingyegongfutaijiclub.com' =>
            array(
                'TABLE_SCHEMA' => 'videohdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_694_',
                'SITE_URL' => 'http://www.mingyegongfutaijiclub.com',
                'DOMAIN' => 'mingyegongfutaijiclub.com',
            ),
            'my-form.biz' =>
            array(
                'TABLE_SCHEMA' => 'videohdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_696_',
                'SITE_URL' => 'http://www.my-form.biz',
                'DOMAIN' => 'my-form.biz',
            ),
            'tri4number1.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1135_',
                'SITE_URL' => 'http://www.tri4number1.com',
                'DOMAIN' => 'tri4number1.com',
            ),
            'abjjcphotos.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1136_',
                'SITE_URL' => 'http://www.abjjcphotos.com',
                'DOMAIN' => 'abjjcphotos.com',
            ),
            '70for70.org' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1137_',
                'SITE_URL' => 'http://www.70for70.org',
                'DOMAIN' => '70for70.org',
            ),
            'frying-bacon-naked.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1138_',
                'SITE_URL' => 'http://www.frying-bacon-naked.com',
                'DOMAIN' => 'frying-bacon-naked.com',
            ),
            'aimencryption.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1139_',
                'SITE_URL' => 'http://www.aimencryption.com',
                'DOMAIN' => 'aimencryption.com',
            ),
            'affordableshadesonline.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1343_',
                'SITE_URL' => 'http://www.affordableshadesonline.com',
                'DOMAIN' => 'affordableshadesonline.com',
            ),
            'democratsforsmith.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1344_',
                'SITE_URL' => 'http://www.democratsforsmith.com',
                'DOMAIN' => 'democratsforsmith.com',
            ),
            'kongbaige.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1345_',
                'SITE_URL' => 'http://www.kongbaige.com',
                'DOMAIN' => 'kongbaige.com',
            ),
            'mercy25th.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1346_',
                'SITE_URL' => 'http://www.mercy25th.com',
                'DOMAIN' => 'mercy25th.com',
            ),
            'emubootsoutlet.net' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1635_',
                'SITE_URL' => 'http://www.emubootsoutlet.net',
                'DOMAIN' => 'emubootsoutlet.net',
            ),
            'vip-kidsonline.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_914_',
                'SITE_URL' => 'http://www.vip-kidsonline.com',
                'DOMAIN' => 'vip-kidsonline.com',
            ),
            'rednegocioscaintra.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_915_',
                'SITE_URL' => 'http://www.rednegocioscaintra.com',
                'DOMAIN' => 'rednegocioscaintra.com',
            ),
            'thepublicstudio.org' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_916_',
                'SITE_URL' => 'http://www.thepublicstudio.org',
                'DOMAIN' => 'thepublicstudio.org',
            ),
            'nribusiness.com' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1120_',
                'SITE_URL' => 'http://www.nribusiness.com',
                'DOMAIN' => 'nribusiness.com',
            ),
            'vsdonline.us' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1121_',
                'SITE_URL' => 'http://www.vsdonline.us',
                'DOMAIN' => 'vsdonline.us',
            ),
            'makersofheaven.net' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1122_',
                'SITE_URL' => 'http://www.makersofheaven.net',
                'DOMAIN' => 'makersofheaven.net',
            ),
            'sherlockplayground.org' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1123_',
                'SITE_URL' => 'http://www.sherlockplayground.org',
                'DOMAIN' => 'sherlockplayground.org',
            ),
            'homeandentertaining.com' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1124_',
                'SITE_URL' => 'http://www.homeandentertaining.com',
                'DOMAIN' => 'homeandentertaining.com',
            ),
            'masa99.com' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1637_',
                'SITE_URL' => 'http://www.masa99.com',
                'DOMAIN' => 'masa99.com',
            ),
            'mictabscoob.com' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_900_',
                'SITE_URL' => 'http://www.mictabscoob.com',
                'DOMAIN' => 'mictabscoob.com',
            ),
            'visualshared.com' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_901_',
                'SITE_URL' => 'http://www.visualshared.com',
                'DOMAIN' => 'visualshared.com',
            ),
            'gphtc.org' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_902_',
                'SITE_URL' => 'http://www.gphtc.org',
                'DOMAIN' => 'gphtc.org',
            ),
            'myguitarsherpa.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_100_',
                'SITE_URL' => 'http://www.myguitarsherpa.com',
                'DOMAIN' => 'myguitarsherpa.com',
            ),
            'nithyadesigns.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_101_',
                'SITE_URL' => 'http://www.nithyadesigns.com',
                'DOMAIN' => 'nithyadesigns.com',
            ),
            'snapstylus.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_102_',
                'SITE_URL' => 'http://www.snapstylus.com',
                'DOMAIN' => 'snapstylus.com',
            ),
            'wemonster.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_103_',
                'SITE_URL' => 'http://www.wemonster.com',
                'DOMAIN' => 'wemonster.com',
            ),
            'twosisterscandybouquets.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_104_',
                'SITE_URL' => 'http://www.twosisterscandybouquets.com',
                'DOMAIN' => 'twosisterscandybouquets.com',
            ),
            'vcestudent.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_38_',
                'SITE_URL' => 'http://www.vcestudent.com',
                'DOMAIN' => 'vcestudent.com',
            ),
            'dustinjswatson.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_39_',
                'SITE_URL' => 'http://www.dustinjswatson.com',
                'DOMAIN' => 'dustinjswatson.com',
            ),
            'nostrum.ws' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_40_',
                'SITE_URL' => 'http://www.nostrum.ws',
                'DOMAIN' => 'nostrum.ws',
            ),
            'flagman-mmm.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_41_',
                'SITE_URL' => 'http://www.flagman-mmm.com',
                'DOMAIN' => 'flagman-mmm.com',
            ),
            'spellbound-wordgame.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1012_',
                'SITE_URL' => 'http://www.spellbound-wordgame.com',
                'DOMAIN' => 'spellbound-wordgame.com',
            ),
            'steelhorseauto.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1013_',
                'SITE_URL' => 'http://www.steelhorseauto.com',
                'DOMAIN' => 'steelhorseauto.com',
            ),
            'bedbugspictures.net' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1638_',
                'SITE_URL' => 'http://www.bedbugspictures.net',
                'DOMAIN' => 'bedbugspictures.net',
            ),
            'fourseasonsistanbulnews.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_265_',
                'SITE_URL' => 'http://www.fourseasonsistanbulnews.com',
                'DOMAIN' => 'fourseasonsistanbulnews.com',
            ),
            'rebelalliance.ws' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_309_',
                'SITE_URL' => 'http://www.rebelalliance.ws',
                'DOMAIN' => 'rebelalliance.ws',
            ),
            '4onthefloormusic.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_310_',
                'SITE_URL' => 'http://www.4onthefloormusic.com',
                'DOMAIN' => '4onthefloormusic.com',
            ),
            'edu-osha.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_313_',
                'SITE_URL' => 'http://www.edu-osha.com',
                'DOMAIN' => 'edu-osha.com',
            ),
            'bunjoscomedyclub.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_326_',
                'SITE_URL' => 'http://www.bunjoscomedyclub.com',
                'DOMAIN' => 'bunjoscomedyclub.com',
            ),
            'officialcowboysfanstore.com' =>
            array(
                'TABLE_SCHEMA' => 'weecours_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1652_',
                'SITE_URL' => 'http://www.officialcowboysfanstore.com',
                'DOMAIN' => 'officialcowboysfanstore.com',
            ),
            'realestatecentrum.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1010_',
                'SITE_URL' => 'http://www.realestatecentrum.com',
                'DOMAIN' => 'realestatecentrum.com',
            ),
            'savingwithrain.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1011_',
                'SITE_URL' => 'http://www.savingwithrain.com',
                'DOMAIN' => 'savingwithrain.com',
            ),
            'jobsatworthingtonschools.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1271_',
                'SITE_URL' => 'http://www.jobsatworthingtonschools.com',
                'DOMAIN' => 'jobsatworthingtonschools.com',
            ),
            'keadilansarawak.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1272_',
                'SITE_URL' => 'http://www.keadilansarawak.com',
                'DOMAIN' => 'keadilansarawak.com',
            ),
            'kelsogc.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1273_',
                'SITE_URL' => 'http://www.kelsogc.com',
                'DOMAIN' => 'kelsogc.com',
            ),
            'lucidmover.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1274_',
                'SITE_URL' => 'http://www.lucidmover.com',
                'DOMAIN' => 'lucidmover.com',
            ),
            'zhangbingsh.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1639_',
                'SITE_URL' => 'http://www.zhangbingsh.com',
                'DOMAIN' => 'zhangbingsh.com',
            ),
            'katie-faulkner.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_297_',
                'SITE_URL' => 'http://www.katie-faulkner.com',
                'DOMAIN' => 'katie-faulkner.com',
            ),
            'pozycjonowaniekatowice.info' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_299_',
                'SITE_URL' => 'http://www.pozycjonowaniekatowice.info',
                'DOMAIN' => 'pozycjonowaniekatowice.info',
            ),
            'southeastfutsalchampionship.org' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_305_',
                'SITE_URL' => 'http://www.southeastfutsalchampionship.org',
                'DOMAIN' => 'southeastfutsalchampionship.org',
            ),
            'pozycjonowanie-poznan.info' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_307_',
                'SITE_URL' => 'http://www.pozycjonowanie-poznan.info',
                'DOMAIN' => 'pozycjonowanie-poznan.info',
            ),
            'pozycjonowanie-warszawa.info' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_308_',
                'SITE_URL' => 'http://www.pozycjonowanie-warszawa.info',
                'DOMAIN' => 'pozycjonowanie-warszawa.info',
            ),
            'petexpose.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1008_',
                'SITE_URL' => 'http://www.petexpose.com',
                'DOMAIN' => 'petexpose.com',
            ),
            'platformofcreativity.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1009_',
                'SITE_URL' => 'http://www.platformofcreativity.com',
                'DOMAIN' => 'platformofcreativity.com',
            ),
            'faxchicago.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1266_',
                'SITE_URL' => 'http://www.faxchicago.com',
                'DOMAIN' => 'faxchicago.com',
            ),
            'infozaradaklub.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1267_',
                'SITE_URL' => 'http://www.infozaradaklub.com',
                'DOMAIN' => 'infozaradaklub.com',
            ),
            'mchistoricalsociety.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1268_',
                'SITE_URL' => 'http://www.mchistoricalsociety.com',
                'DOMAIN' => 'mchistoricalsociety.com',
            ),
            'httpforums.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1269_',
                'SITE_URL' => 'http://www.httpforums.com',
                'DOMAIN' => 'httpforums.com',
            ),
            'hot-xxx-porn-directory.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1270_',
                'SITE_URL' => 'http://www.hot-xxx-porn-directory.com',
                'DOMAIN' => 'hot-xxx-porn-directory.com',
            ),
            'jplouboutin.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1641_',
                'SITE_URL' => 'http://www.jplouboutin.com',
                'DOMAIN' => 'jplouboutin.com',
            ),
            'thepizzaplaceomaha.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_271_',
                'SITE_URL' => 'http://www.thepizzaplaceomaha.com',
                'DOMAIN' => 'thepizzaplaceomaha.com',
            ),
            '10footgames.net' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_288_',
                'SITE_URL' => 'http://www.10footgames.net',
                'DOMAIN' => '10footgames.net',
            ),
            'rcraiders.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_301_',
                'SITE_URL' => 'http://www.rcraiders.com',
                'DOMAIN' => 'rcraiders.com',
            ),
            'visiteastnorwich.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_302_',
                'SITE_URL' => 'http://www.visiteastnorwich.com',
                'DOMAIN' => 'visiteastnorwich.com',
            ),
            'sauktrail.org' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_303_',
                'SITE_URL' => 'http://www.sauktrail.org',
                'DOMAIN' => 'sauktrail.org',
            ),
            'smeoracle.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1006_',
                'SITE_URL' => 'http://www.smeoracle.com',
                'DOMAIN' => 'smeoracle.com',
            ),
            'spastikwax.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1007_',
                'SITE_URL' => 'http://www.spastikwax.com',
                'DOMAIN' => 'spastikwax.com',
            ),
            'laughingyogini.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1261_',
                'SITE_URL' => 'http://www.laughingyogini.com',
                'DOMAIN' => 'laughingyogini.com',
            ),
            'longbeachrehab.net' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1262_',
                'SITE_URL' => 'http://www.longbeachrehab.net',
                'DOMAIN' => 'longbeachrehab.net',
            ),
            'magiquestmb.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1263_',
                'SITE_URL' => 'http://www.magiquestmb.com',
                'DOMAIN' => 'magiquestmb.com',
            ),
            'meilleurs-vins.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1264_',
                'SITE_URL' => 'http://www.meilleurs-vins.com',
                'DOMAIN' => 'meilleurs-vins.com',
            ),
            'easyfreecodes.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1265_',
                'SITE_URL' => 'http://www.easyfreecodes.com',
                'DOMAIN' => 'easyfreecodes.com',
            ),
            'tadanafil4u.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1640_',
                'SITE_URL' => 'http://www.tadanafil4u.com',
                'DOMAIN' => 'tadanafil4u.com',
            ),
            'lepetitspirou.net' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_292_',
                'SITE_URL' => 'http://www.lepetitspirou.net',
                'DOMAIN' => 'lepetitspirou.net',
            ),
            'angoltanar.info' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_298_',
                'SITE_URL' => 'http://www.angoltanar.info',
                'DOMAIN' => 'angoltanar.info',
            ),
            'mediawestpost.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_300_',
                'SITE_URL' => 'http://www.mediawestpost.com',
                'DOMAIN' => 'mediawestpost.com',
            ),
            'donnertrailfruit.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_304_',
                'SITE_URL' => 'http://www.donnertrailfruit.com',
                'DOMAIN' => 'donnertrailfruit.com',
            ),
            'jeffhunterbailey.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_306_',
                'SITE_URL' => 'http://www.jeffhunterbailey.com',
                'DOMAIN' => 'jeffhunterbailey.com',
            ),
            'auffiliates.com' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1182_',
                'SITE_URL' => 'http://www.auffiliates.com',
                'DOMAIN' => 'auffiliates.com',
            ),
            'fjz3.com' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1642_',
                'SITE_URL' => 'http://www.fjz3.com',
                'DOMAIN' => 'fjz3.com',
            ),
            'winkingbear.com' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_875_',
                'SITE_URL' => 'http://www.winkingbear.com',
                'DOMAIN' => 'winkingbear.com',
            ),
            'ilatlon.com' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_876_',
                'SITE_URL' => 'http://www.ilatlon.com',
                'DOMAIN' => 'ilatlon.com',
            ),
            'levinrozalis.com' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_877_',
                'SITE_URL' => 'http://www.levinrozalis.com',
                'DOMAIN' => 'levinrozalis.com',
            ),
            'decaturheights.com' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_878_',
                'SITE_URL' => 'http://www.decaturheights.com',
                'DOMAIN' => 'decaturheights.com',
            ),
            'mizzoulax.org' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_879_',
                'SITE_URL' => 'http://www.mizzoulax.org',
                'DOMAIN' => 'mizzoulax.org',
            ),
            'pmengraving.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1004_',
                'SITE_URL' => 'http://www.pmengraving.com',
                'DOMAIN' => 'pmengraving.com',
            ),
            'puertoricoisbest.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1005_',
                'SITE_URL' => 'http://www.puertoricoisbest.com',
                'DOMAIN' => 'puertoricoisbest.com',
            ),
            'flagstaffbikers.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1256_',
                'SITE_URL' => 'http://www.flagstaffbikers.com',
                'DOMAIN' => 'flagstaffbikers.com',
            ),
            'freedomtour08.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1257_',
                'SITE_URL' => 'http://www.freedomtour08.com',
                'DOMAIN' => 'freedomtour08.com',
            ),
            'ixten.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1258_',
                'SITE_URL' => 'http://www.ixten.com',
                'DOMAIN' => 'ixten.com',
            ),
            'jocuri3.net' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1259_',
                'SITE_URL' => 'http://www.jocuri3.net',
                'DOMAIN' => 'jocuri3.net',
            ),
            'lasvegasrealtyexperts.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1260_',
                'SITE_URL' => 'http://www.lasvegasrealtyexperts.com',
                'DOMAIN' => 'lasvegasrealtyexperts.com',
            ),
            'outlet-juicycouture-outlet.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1643_',
                'SITE_URL' => 'http://www.outlet-juicycouture-outlet.com',
                'DOMAIN' => 'outlet-juicycouture-outlet.com',
            ),
            'emarketingpark.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_270_',
                'SITE_URL' => 'http://www.emarketingpark.com',
                'DOMAIN' => 'emarketingpark.com',
            ),
            'mgphotographe.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_289_',
                'SITE_URL' => 'http://www.mgphotographe.com',
                'DOMAIN' => 'mgphotographe.com',
            ),
            'volksbuehne-bochum.info' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_290_',
                'SITE_URL' => 'http://www.volksbuehne-bochum.info',
                'DOMAIN' => 'volksbuehne-bochum.info',
            ),
            'fxxgqt.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_296_',
                'SITE_URL' => 'http://www.fxxgqt.com',
                'DOMAIN' => 'fxxgqt.com',
            ),
            'nuestracasaarequipa.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_356_',
                'SITE_URL' => 'http://www.nuestracasaarequipa.com',
                'DOMAIN' => 'nuestracasaarequipa.com',
            ),
            'approachnutrition.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1244_',
                'SITE_URL' => 'http://www.approachnutrition.com',
                'DOMAIN' => 'approachnutrition.com',
            ),
            'askmyrealestateagent.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1245_',
                'SITE_URL' => 'http://www.askmyrealestateagent.com',
                'DOMAIN' => 'askmyrealestateagent.com',
            ),
            'casanoblemortgages.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1246_',
                'SITE_URL' => 'http://www.casanoblemortgages.com',
                'DOMAIN' => 'casanoblemortgages.com',
            ),
            'costumesforhaloween.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1247_',
                'SITE_URL' => 'http://www.costumesforhaloween.com',
                'DOMAIN' => 'costumesforhaloween.com',
            ),
            'tni-mba.net' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1644_',
                'SITE_URL' => 'http://www.tni-mba.net',
                'DOMAIN' => 'tni-mba.net',
            ),
            'votewaynenorman.net' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_257_',
                'SITE_URL' => 'http://www.votewaynenorman.net',
                'DOMAIN' => 'votewaynenorman.net',
            ),
            'jewishstlouis.info' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_264_',
                'SITE_URL' => 'http://www.jewishstlouis.info',
                'DOMAIN' => 'jewishstlouis.info',
            ),
            'noblebrothersviolin.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_281_',
                'SITE_URL' => 'http://www.noblebrothersviolin.com',
                'DOMAIN' => 'noblebrothersviolin.com',
            ),
            'getapersonaldoctor.org' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_282_',
                'SITE_URL' => 'http://www.getapersonaldoctor.org',
                'DOMAIN' => 'getapersonaldoctor.org',
            ),
            'compeces.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_291_',
                'SITE_URL' => 'http://www.compeces.com',
                'DOMAIN' => 'compeces.com',
            ),
            'mariacandler.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_758_',
                'SITE_URL' => 'http://www.mariacandler.com',
                'DOMAIN' => 'mariacandler.com',
            ),
            'modernacupuncturestudio.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1002_',
                'SITE_URL' => 'http://www.modernacupuncturestudio.com',
                'DOMAIN' => 'modernacupuncturestudio.com',
            ),
            'mullinsdrums.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1003_',
                'SITE_URL' => 'http://www.mullinsdrums.com',
                'DOMAIN' => 'mullinsdrums.com',
            ),
            'dogpursecarriers.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1253_',
                'SITE_URL' => 'http://www.dogpursecarriers.com',
                'DOMAIN' => 'dogpursecarriers.com',
            ),
            'easttaylor.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1254_',
                'SITE_URL' => 'http://www.easttaylor.com',
                'DOMAIN' => 'easttaylor.com',
            ),
            'erikslaws.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1255_',
                'SITE_URL' => 'http://www.erikslaws.com',
                'DOMAIN' => 'erikslaws.com',
            ),
            'goforthays.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1645_',
                'SITE_URL' => 'http://www.goforthays.com',
                'DOMAIN' => 'goforthays.com',
            ),
            'sudokupuzzles.biz' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_276_',
                'SITE_URL' => 'http://www.sudokupuzzles.biz',
                'DOMAIN' => 'sudokupuzzles.biz',
            ),
            'signalshow.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_285_',
                'SITE_URL' => 'http://www.signalshow.com',
                'DOMAIN' => 'signalshow.com',
            ),
            'iabg2009.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_287_',
                'SITE_URL' => 'http://www.iabg2009.com',
                'DOMAIN' => 'iabg2009.com',
            ),
            'mukorom.net' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_295_',
                'SITE_URL' => 'http://www.mukorom.net',
                'DOMAIN' => 'mukorom.net',
            ),
            'xn----6hckibbsbfgb3iya.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_363_',
                'SITE_URL' => 'http://www.xn----6hckibbsbfgb3iya.com',
                'DOMAIN' => 'xn----6hckibbsbfgb3iya.com',
            ),
            'beachplaceapartments.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_194_',
                'SITE_URL' => 'http://beachplaceapartments.com',
                'DOMAIN' => 'beachplaceapartments.com',
            ),
            'bebekolay.net' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_195_',
                'SITE_URL' => 'http://www.bebekolay.net',
                'DOMAIN' => 'bebekolay.net',
            ),
            'bullshitproduction.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_196_',
                'SITE_URL' => 'http://bullshitproduction.com',
                'DOMAIN' => 'bullshitproduction.com',
            ),
            'burgerbarnyc.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_197_',
                'SITE_URL' => 'http://burgerbarnyc.com',
                'DOMAIN' => 'burgerbarnyc.com',
            ),
            'earthtelevision.org' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1242_',
                'SITE_URL' => 'http://www.earthtelevision.org',
                'DOMAIN' => 'earthtelevision.org',
            ),
            'jobsinjapanese.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1646_',
                'SITE_URL' => 'http://www.jobsinjapanese.com',
                'DOMAIN' => 'jobsinjapanese.com',
            ),
            'femmes-entreprises-bretagne.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_263_',
                'SITE_URL' => 'http://www.femmes-entreprises-bretagne.com',
                'DOMAIN' => 'femmes-entreprises-bretagne.com',
            ),
            'sellandmarketyourbook.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_272_',
                'SITE_URL' => 'http://www.sellandmarketyourbook.com',
                'DOMAIN' => 'sellandmarketyourbook.com',
            ),
            'aplanet4creation.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_273_',
                'SITE_URL' => 'http://www.aplanet4creation.com',
                'DOMAIN' => 'aplanet4creation.com',
            ),
            'kalispellphoto.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_280_',
                'SITE_URL' => 'http://www.kalispellphoto.com',
                'DOMAIN' => 'kalispellphoto.com',
            ),
            'counselinginmetairie.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_286_',
                'SITE_URL' => 'http://www.counselinginmetairie.com',
                'DOMAIN' => 'counselinginmetairie.com',
            ),
            'tidbitsofmarketing.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_762_',
                'SITE_URL' => 'http://www.tidbitsofmarketing.com',
                'DOMAIN' => 'tidbitsofmarketing.com',
            ),
            'tokelau-govt.info' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_763_',
                'SITE_URL' => 'http://www.tokelau-govt.info',
                'DOMAIN' => 'tokelau-govt.info',
            ),
            'dirty-panties.us' =>
            array(
                'TABLE_SCHEMA' => 'your-bes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1237_',
                'SITE_URL' => 'http://www.dirty-panties.us',
                'DOMAIN' => 'dirty-panties.us',
            ),
            'alfonsorealestatephotography.com' =>
            array(
                'TABLE_SCHEMA' => 'your-bes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1238_',
                'SITE_URL' => 'http://www.alfonsorealestatephotography.com',
                'DOMAIN' => 'alfonsorealestatephotography.com',
            ),
            'arabastahentai.com' =>
            array(
                'TABLE_SCHEMA' => 'your-bes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1239_',
                'SITE_URL' => 'http://www.arabastahentai.com',
                'DOMAIN' => 'arabastahentai.com',
            ),
            'dsquaredgurtel.com' =>
            array(
                'TABLE_SCHEMA' => 'your-bes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1647_',
                'SITE_URL' => 'http://www.dsquaredgurtel.com',
                'DOMAIN' => 'dsquaredgurtel.com',
            ),
            'artoflivingboulder.org' =>
            array(
                'TABLE_SCHEMA' => 'your-bes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_261_',
                'SITE_URL' => 'http://www.artoflivingboulder.org',
                'DOMAIN' => 'artoflivingboulder.org',
            ),
            'iplibertad.org' =>
            array(
                'TABLE_SCHEMA' => 'your-bes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_266_',
                'SITE_URL' => 'http://www.iplibertad.org',
                'DOMAIN' => 'iplibertad.org',
            ),
            'cambiaelestatus.org' =>
            array(
                'TABLE_SCHEMA' => 'your-bes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_269_',
                'SITE_URL' => 'http://www.cambiaelestatus.org',
                'DOMAIN' => 'cambiaelestatus.org',
            ),
            'wemip.com' =>
            array(
                'TABLE_SCHEMA' => 'your-bes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_277_',
                'SITE_URL' => 'http://www.wemip.com',
                'DOMAIN' => 'wemip.com',
            ),
            'wrigglingonline.com' =>
            array(
                'TABLE_SCHEMA' => 'your-bes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_362_',
                'SITE_URL' => 'http://www.wrigglingonline.com',
                'DOMAIN' => 'wrigglingonline.com',
            ),
            'dtm-studio.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1232_',
                'SITE_URL' => 'http://www.dtm-studio.com',
                'DOMAIN' => 'dtm-studio.com',
            ),
        );

        if (is_array($reDomains) && $reDomains) {
            $reArr = array();
            foreach ($reDomains as $domain) {
                if (isset($dbAllKey[$domain])) {
                    $reArr[$domain] = $dbAllKey[$domain];
                }
            }
            return $reArr;
        } else {
            return $dbAllKey;
        }

        echo count($dbAllKey);
    }

}

// end class
?>