<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    function getlist($cond = null, $order = null, $limit = null, $key = null, $val = null) {
        return $this->find("list", array(
                    'conditions' => $cond,
                    'order' => $order,
                    'limit' => $limit,
                    'fields' => array(str_replace('{n}.', '', $key), str_replace('{n}.', '', $val))
                ));
    }

    // get file extension
    function fileExt($fName = null) {
        if (empty($fName))
            return '';
        return strtolower(array_pop(explode('.', $fName)));
    }

    // delete files
    function unlinkFiles($iName = array(), $path = null) {
        if (empty($iName))
            return false;
        foreach ($iName as $k => $v) {
            @unlink($path . DS . $v);
            @unlink($path . DS . $v);
        }
        return 1;
    }

    function checkUnique($data, $fields) {
        // check if the param contains multiple columns or a single one
        if (!is_array($fields)) {
            $fields = array($fields);
        }

        // go trough all columns and get their values from the parameters
        foreach ($fields as $key) {
            $unique[$key] = $this->data[$this->name][$key];
        }

        // primary key value must be different from the posted value
        if (isset($this->data[$this->name][$this->primaryKey])) {
            $unique[$this->primaryKey] = "<>" . $this->data[$this->name][$this->primaryKey];
        }

        // use the model's isUnique function to check the unique rule
        return $this->isUnique($unique, false);
    }
    
    function unbindValidation($type, $fields, $require=false)
{
    if ($type === 'remove')
    {
        $this->validate = array_diff_key($this->validate, array_flip($fields));
    }
    else
    if ($type === 'keep')
    {
        $this->validate = array_intersect_key($this->validate, array_flip($fields));
    }
    
    if ($require === true)
    {
        foreach ($this->validate as $field=>$rules)
        {
            if (is_array($rules))
            {
                $rule = key($rules);
                
                $this->validate[$field][$rule]['required'] = true;
            }
            else
            {
                $ruleName = (ctype_alpha($rules)) ? $rules : 'required';
                
                $this->validate[$field] = array($ruleName=>array('rule'=>$rules,'required'=>true));
            }
        }
    }
} 

}
