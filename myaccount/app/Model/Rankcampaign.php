<?php

App::uses('AppModel', 'Model');

class Rankcampaign extends AppModel {

    public $name = 'Rankcampaign';
    public $useTable = 'rankcampaigns';
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );
    public $validate = array(
        'campaign_name' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Campaign Name can not left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
            'one_se' => array(
                'rule' => array('atleastOneEngine'),
                'message' => 'Select a search engine',
            ),
        ),
        'c_keywords' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter your keyword',
                'allowEmpty' => false,
                'required' => true,
            )
        ),
        'c_urls' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter your keyword',
                'allowEmpty' => false,
                'required' => true,
            )
        )
    );

    function atleastOneEngine() {
        return ( ($this->data['Rankcampaign']['google'] == 'false') && ( $this->data['Rankcampaign']['yahoo'] == 'false' ) && ( $this->data['Rankcampaign']['bing'] == 'false' ) ) ? 0 : 1;
    }

    /* function atleastOneKeyword(){
      $k = implode("", array_filter($this->data['Rankcampaign']['keywords']));
      return ( strlen($k) == 0 ) ? 0 : 1;
      }
      function atleastOneUrl() {
      $k = implode("", array_filter($this->data['Rankcampaign']['urls']));
      return ( strlen($k) == 0 ) ? 0 : 1;
      } */
}
