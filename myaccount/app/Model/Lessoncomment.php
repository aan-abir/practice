<?php

App::uses('AppModel', 'Model');

class Lessoncomment extends AppModel
{
    public $name = 'Lessoncomment';
    public $useTable = 'lesson_comments';
    public $validate = array(
        'comment' => array(
            'rule' => 'notEmpty',
            'message' => 'Comment is required.',
            'allowEmpty' => false,
            'required' => true,
        )
    );
}
