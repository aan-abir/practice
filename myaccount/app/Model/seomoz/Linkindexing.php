<?php
    App::uses('AppModel', 'Model');
    class Linkindexing extends AppModel {

        public $name = 'Linkindexing';
        public $useTable = 'indexingcampaigns';

        public $AppId = "50aaa284-d780-4f33-895d-18eb57dbe1eb";
        public $ApiKey = "cec9656c-9ae0-463f-b0f6-6338cd5da4bc";
        public $apiAppKey = "AppId=50aaa284-d780-4f33-895d-18eb57dbe1eb&ApiKey=cec9656c-9ae0-463f-b0f6-6338cd5da4bc";
        public $EndPoint = "http://onehourindexing.co/api";

        public $validate = array(
            'title' => array(
                'rule' => 'notEmpty',
                'message' => 'Title is required.',
                'allowEmpty' => false,
                'required' => true,
            )
        );

        public $virtualFields = array(
            'statuslabel' => "SELECT CASE WHEN Linkindexing.status = 1 THEN 'Sent for Indexing' WHEN Linkindexing.status = 2 THEN 'Completed' ELSE 'Inactive' END",
        );


        /*
        AppId    Guid    Yes    Your Application ID. Emailed to you after registering application.
        ApiKey    Guid    Yes    User API Key. Found under users� My Account > Linklicious API Key.
        Name    String    Yes    The name of the batch (displayed on users� Batches Page).

        Sample Success
        { BatchId: �GUID� }
        Sample Failure
        {Error = true, ErrorMessage = �REASON FOR FAILURE� }

        */
        public function  CreateBatch ( $batchName = null ) {
            if ( empty($batchName) ) return false;
            $url = $this->EndPoint."/createbatch?".$this->apiAppKey."&Name=$batchName";
            $linksToPOst = "www.rankratio.com";
            $params = array('http' =>array(
                'method' =>'POST',
                'content' => $linksToPOst
            ));
            $ctx = stream_context_create($params);
            $fp = fopen($url, 'rb', false, $ctx);
            if ( $fp ) {
                $response = stream_get_contents($fp);
                return @json_decode($response);
            }
            else{
                return  false;
            }
        } // end functions

        /*
        * AppId    Guid    Yes    Your Application ID. Emailed to you after registering application.
        ApiKey    Guid    Yes    User API Key. Found under users� My Account > Linklicious API Key.
        BatchId    Guid    Yes    Given in response to Upload Links.
        Sort    String    No    Sort the links returned using the following:
        �-CrawlDate� � Most recently crawled first
        �CrawlDate� � First crawled first
        �-LiveDate� � Order by date link will go live. Last link first.
        �LiveDate� � (Default) First link first.
        Page    Int    No    0-Based; Only 25 links per page.

        Response (JSON)
        You will receive one of two JSON responses.
        Dates are expressed in RFC-3339 format(e.g. �1985-04-12T23:20:50.52Z�).
        CrawlDate is empty when the link has yet to be crawled.
        Sample Success
        {
        Links: [
        { Id: �c45s3�, BatchId: �GUID�, LinkUrl: �LINKURL�, Processed: �LIVEDATE�, IndexDate: �CRAWLDATE� },
        { Id: �8dk3d�, BatchId: �GUID�, LinkUrl: �LINKURL�, Processed: �LIVEDATE�, IndexDate: �� }
        ]
        }
        Sample Failure
        { Error = true,ErrorMessage = �REASON FOR FAILURE�}


        */
        public function getBatch($batchId = null ) {
            if ( empty($batchId) ) return false;
            $url = $this->EndPoint."/getbatch?".$this->apiAppKey."&BatchId=$batchId";
            $fp = fopen($url, 'rb');
            $response = stream_get_contents($fp);
            return @json_decode($response);
        }


        /*
        * Response (JSON)
        You will receive one of two JSON responses.
        Sample Success
        {Remaining: 1234 }
        Sample Failure
        {Error = true, ErrorMessage = �REASON FOR FAILURE� }

        */

        public function getRemaining () {
            $url = $this->EndPoint."/getremaining?".$this->apiAppKey;
            $fp = @fopen($url, 'rb');
            $response = @stream_get_contents($fp);
            return @json_decode($response);
        }

        /*
        * AppId    Guid    Yes    Your Application ID. Emailed to you after registering application.
        ApiKey    Guid    Yes    User API Key. Found under users� My Account > Linklicious API Key.
        LinksPerDay    Int    Yes    How many links the user wants to go live. 0 = Ping All Now
        BatchId    Guid    No    If you want to add to a batch, pass in the ID.
        BatchName    String    Yes*    *Required if BatchId is not passed. The name of the batch (displayed on users� Batches Page)
        Links    String[]    Yes    Uploaded to the POSTs Request Body. Every link must be on a new line.
        Minimum of 10 links without BatchID.

        Response (JSON)
        You will receive one of two JSON responses.
        Sample Success-  { LinksAdded: 10, LinksRejected: 1, RejectedLinks: ["REJECTEDURL"], BatchId: "GUID" }
        Sample Failure-  { Error = true, ErrorMessage = �REASON FOR FAILURE� }

        *
        */

        public function uploadLinks($links = null, $BatchName = null) {
            //$links = "http://google.com\nhttp://yahoo.com";
            // remove duplicate data
            $urls = explode("\n", trim($links));
            print_r($urls);
            $batchData = "";
            foreach ($urls as $u) {
                if ( !in_array($u, $urls) )
                    $batchData .= $u."\n";
            }
            $batchData = substr($batchData, 0, -2); //remove last \n
            return $batchData;
            /*$url = $this->EndPoint."/uploadlinks?".$this->apiAppKey."&BatchName=".$BatchName;
            $params = array('http' =>array(
                'method' =>'POST',
                'header'=> "Content-Length: " . strlen($batchData) . "\r\n",
                'content' => $batchData
            ));
            $ctx = stream_context_create($params);
            $fp = fopen($url, 'rb', false, $ctx);
            $response = @stream_get_contents($fp);
            return @json_decode($response);*/
        }


    }
