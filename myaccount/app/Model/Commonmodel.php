<?php

App::uses('AppModel', 'Model');

class Commonmodel extends AppModel {

    public $name = 'Commonmodel';
    public $useTable = false;

    function curl_get_contents($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }

    function _request($url, $method = "POST", $postData = array()) {

        // initialize a new curl object
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        /**
          curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($curl, CURLOPT_HEADER, true);
          curl_setopt($curl, CURLOPT_NOBODY, true);
          /* */
        // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        switch (strtoupper($method)) {
            case "GET":
                curl_setopt($curl, CURLOPT_HTTPGET, TRUE);
                break;
            case "POST":
                curl_setopt($curl, CURLOPT_POST, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
                break;
            case "DELETE":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;
            default:
                return "Unknown method $method";
                break;
        }

        $result = curl_exec($curl);
        // do the request. If FALSE, then an exception occurred
        if (FALSE === ($result))
            return "Curl failed with error " . curl_error($curl);

        $final_data = array();

        // get result code
        $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $header = curl_getinfo($curl);
        //$header = $this->parse_http_head($result);
        $final_data['response_code'] = $responseCode;
        $final_data['header'] = $header;
        $final_data['result'] = '0';

        if ($responseCode === 200) {
            $final_data['result'] = $result;
        }

        return $final_data;
    }

    function parse_http_head($str) {

        $result = array();

        // Split into lines
        $lines = explode("\r\n", $str);

        // Handle response line
        $line = explode(' ', array_shift($lines), 3);
        $version = explode('/', $line[0]);
        $result['version'] = (float) $version[1];
        $result['code'] = (int) $line[1];
        $result['text'] = $line[2];

        // Parse headers
        $result['headers'] = array();
        while ($line = trim(array_shift($lines))) {
            list($name, $val) = explode(':', $line, 2);
            $name = strtolower(trim($name)); // Header names are case-insensitive, so convert them all to lower case so we can easily use isset()
            if (isset($result['headers'][$name])) { // Some headers (like Set-Cookie:) may appear more than once, so convert them to an array if necessary
                $result['headers'][$name] = (array) $result['headers'][$name];
                $result['headers'][$name][] = trim($val);
            } else {
                $result['headers'][$name] = trim($val);
            }
        }

        return $result;
    }

}

// end class
?>