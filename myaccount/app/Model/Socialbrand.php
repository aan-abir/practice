<?php

App::uses('AppModel', 'Model');

class Socialbrand extends AppModel {

    public $name = 'Socialbrand';
    public $useTable = 'social_brand';
    var $validate = array(
        'firstname' => array(
            'rule' => 'notEmpty',
            'message' => 'Enter First Name',
            'allowEmpty' => false,
            'required' => true,
        ),
        'lastname' => array(
            'rule' => 'notEmpty',
            'message' => 'Enter Last Name',
            'allowEmpty' => false,
            'required' => true,
        ),
        'email' => array(
            'between 1 and 3 character' => array(
                'rule' => 'email',
                'required' => true,
                'message' => 'Invalid email address.',
            ),
        ),
        'facebook_page_id' => array(
            'rule' => array('checkValidFacebookId'),
            'message' => 'Please enter a valid facebook id or username.'
        ),
    );

    public function checkValidFacebookId($check) {
        $facebook_page_id = $check['facebook_page_id'];
        if ($facebook_page_id) {
            $fbUrl = "https://graph.facebook.com/$facebook_page_id?fields=id";
            $fbResult = ClassRegistry::init('Commonmodel')->curl_get_contents($fbUrl);
            $finalResult = json_decode($fbResult);
            if (isset($finalResult->id)) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

}

// end class
?>