
<?php

App::uses('AppModel', 'Model');

class Nitroprev extends AppModel {

    public $name = 'NitroCustom';
    public $useTable = false;

    function curl_get_contents($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }

    function nitroDbInfo(array $reDomains = null) {

        $dbAllKey = array(
            '007protection.net' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wpK_126_',
                'SITE_URL' => 'http://www.007protection.net',
                'DOMAIN' => '007protection.net',
            ),
            '007security.net' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wpK_127_',
                'SITE_URL' => 'http://www.007security.net',
                'DOMAIN' => '007security.net',
            ),
            'egyptiantearoom.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wpK_132_',
                'SITE_URL' => 'http://www.egyptiantearoom.com',
                'DOMAIN' => 'egyptiantearoom.com',
            ),
            'kimballpartnergroup.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wpK_133_',
                'SITE_URL' => 'http://www.kimballpartnergroup.com',
                'DOMAIN' => 'kimballpartnergroup.com',
            ),
            'michaeldoverlaw.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1001_',
                'SITE_URL' => 'http://www.michaeldoverlaw.com',
                'DOMAIN' => 'michaeldoverlaw.com',
            ),
            'courtreportingstudent.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1248_',
                'SITE_URL' => 'http://www.courtreportingstudent.com',
                'DOMAIN' => 'courtreportingstudent.com',
            ),
            'dcmedicalmalpracticeattorneys.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1249_',
                'SITE_URL' => 'http://www.dcmedicalmalpracticeattorneys.com',
                'DOMAIN' => 'dcmedicalmalpracticeattorneys.com',
            ),
            'debteliminationmethods.info' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1250_',
                'SITE_URL' => 'http://www.debteliminationmethods.info',
                'DOMAIN' => 'debteliminationmethods.info',
            ),
            'dental-studio-muzevic.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1251_',
                'SITE_URL' => 'http://www.dental-studio-muzevic.com',
                'DOMAIN' => 'dental-studio-muzevic.com',
            ),
            'kochobisexual.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_283_',
                'SITE_URL' => 'http://www.kochobisexual.com',
                'DOMAIN' => 'kochobisexual.com',
            ),
            'highperformancemail.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_284_',
                'SITE_URL' => 'http://www.highperformancemail.com',
                'DOMAIN' => 'highperformancemail.com',
            ),
            'marcparctransportation.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_293_',
                'SITE_URL' => 'http://www.marcparctransportation.com',
                'DOMAIN' => 'marcparctransportation.com',
            ),
            'moratumv.net' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_294_',
                'SITE_URL' => 'http://www.moratumv.net',
                'DOMAIN' => 'moratumv.net',
            ),
            'spccic.com' =>
            array(
                'TABLE_SCHEMA' => '007PROTE_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_346_',
                'SITE_URL' => 'http://www.spccic.com',
                'DOMAIN' => 'spccic.com',
            ),
            '25kw.info' =>
            array(
                'TABLE_SCHEMA' => '25kw_tkp',
                'TABLE_PREFIX' => 'wp_1_2109_',
                'SITE_URL' => 'http://www.25kw.info',
                'DOMAIN' => '25kw.info',
            ),
            '2myth.com' =>
            array(
                'TABLE_SCHEMA' => '25kw_tkp',
                'TABLE_PREFIX' => 'wp_1_2110_',
                'SITE_URL' => 'http://www.2myth.com',
                'DOMAIN' => '2myth.com',
            ),
            '36re.net' =>
            array(
                'TABLE_SCHEMA' => '25kw_tkp',
                'TABLE_PREFIX' => 'wp_1_2111_',
                'SITE_URL' => 'http://www.36re.net',
                'DOMAIN' => '36re.net',
            ),
            '5150cables.com' =>
            array(
                'TABLE_SCHEMA' => '25kw_tkp',
                'TABLE_PREFIX' => 'wp_1_2112_',
                'SITE_URL' => 'http://www.5150cables.com',
                'DOMAIN' => '5150cables.com',
            ),
            '798pr.org' =>
            array(
                'TABLE_SCHEMA' => '25kw_tkp',
                'TABLE_PREFIX' => 'wp_1_2113_',
                'SITE_URL' => 'http://www.798pr.org',
                'DOMAIN' => '798pr.org',
            ),
            '2insurerestaurants.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_1_82784_',
                'SITE_URL' => 'http://www.2insurerestaurants.com',
                'DOMAIN' => '2insurerestaurants.com',
            ),
            'deloresmae.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_1_82785_',
                'SITE_URL' => 'http://www.deloresmae.com',
                'DOMAIN' => 'deloresmae.com',
            ),
            'bottlesandtubes.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_1_82786_',
                'SITE_URL' => 'http://www.bottlesandtubes.com',
                'DOMAIN' => 'bottlesandtubes.com',
            ),
            'formasound.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_1_82787_',
                'SITE_URL' => 'http://www.formasound.com',
                'DOMAIN' => 'formasound.com',
            ),
            'heatherl.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_1_82788_',
                'SITE_URL' => 'http://www.heatherl.com',
                'DOMAIN' => 'heatherl.com',
            ),
            'lawrencedunlapllc.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1204_',
                'SITE_URL' => 'http://www.lawrencedunlapllc.com',
                'DOMAIN' => 'lawrencedunlapllc.com',
            ),
            'macappelli.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1205_',
                'SITE_URL' => 'http://www.macappelli.com',
                'DOMAIN' => 'macappelli.com',
            ),
            'garbosvintage.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1206_',
                'SITE_URL' => 'http://www.garbosvintage.com',
                'DOMAIN' => 'garbosvintage.com',
            ),
            'helledale.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1207_',
                'SITE_URL' => 'http://www.helledale.com',
                'DOMAIN' => 'helledale.com',
            ),
            'johnsonprintingco.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1208_',
                'SITE_URL' => 'http://www.johnsonprintingco.com',
                'DOMAIN' => 'johnsonprintingco.com',
            ),
            'luke6project.org' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_812_',
                'SITE_URL' => 'http://www.luke6project.org',
                'DOMAIN' => 'luke6project.org',
            ),
            'youthyx.org' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_813_',
                'SITE_URL' => 'http://www.youthyx.org',
                'DOMAIN' => 'youthyx.org',
            ),
            'tongchengqingren.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_814_',
                'SITE_URL' => 'http://www.tongchengqingren.com',
                'DOMAIN' => 'tongchengqingren.com',
            ),
            'radion1.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_815_',
                'SITE_URL' => 'http://www.radion1.com',
                'DOMAIN' => 'radion1.com',
            ),
            'beyondphiladelphia.org' =>
            array(
                'TABLE_SCHEMA' => '2insurer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_816_',
                'SITE_URL' => 'http://www.beyondphiladelphia.org',
                'DOMAIN' => 'beyondphiladelphia.org',
            ),
            'constellatio.info' =>
            array(
                'TABLE_SCHEMA' => '2insurer_wp658',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://constellatio.info',
                'DOMAIN' => 'constellatio.info',
            ),
            'ffpcllc.com' =>
            array(
                'TABLE_SCHEMA' => '2insurer_wp204',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ffpcllc.com',
                'DOMAIN' => 'ffpcllc.com',
            ),
            'cwruaaa.org' =>
            array(
                'TABLE_SCHEMA' => '2insurer_wp738',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://cwruaaa.org',
                'DOMAIN' => 'cwruaaa.org',
            ),
            'alpost201.org' =>
            array(
                'TABLE_SCHEMA' => '2insurer_wp505',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://alpost201.org',
                'DOMAIN' => 'alpost201.org',
            ),
            'spedng.org' =>
            array(
                'TABLE_SCHEMA' => '2insurer_wp938',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://spedng.org',
                'DOMAIN' => 'spedng.org',
            ),
            '4wardit.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_1_83165_',
                'SITE_URL' => 'http://www.4wardit.com',
                'DOMAIN' => '4wardit.com',
            ),
            'abbysdigitalphotography.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_1_83166_',
                'SITE_URL' => 'http://www.abbysdigitalphotography.com',
                'DOMAIN' => 'abbysdigitalphotography.com',
            ),
            'acornsojo.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_1_83167_',
                'SITE_URL' => 'http://www.acornsojo.com',
                'DOMAIN' => 'acornsojo.com',
            ),
            'adistock.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_wp320',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.adistock.com',
                'DOMAIN' => 'adistock.com',
            ),
            'affordableshadesonline.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1343_',
                'SITE_URL' => 'http://www.affordableshadesonline.com',
                'DOMAIN' => 'affordableshadesonline.com',
            ),
            'dearprovidence.org' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1195_',
                'SITE_URL' => 'http://www.dearprovidence.org',
                'DOMAIN' => 'dearprovidence.org',
            ),
            'desigibiza.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1197_',
                'SITE_URL' => 'http://www.desigibiza.com',
                'DOMAIN' => 'desigibiza.com',
            ),
            'giny.us' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_802_',
                'SITE_URL' => 'http://www.giny.us',
                'DOMAIN' => 'giny.us',
            ),
            'ctfnepa.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_803_',
                'SITE_URL' => 'http://www.ctfnepa.com',
                'DOMAIN' => 'ctfnepa.com',
            ),
            'ncarborday.org' =>
            array(
                'TABLE_SCHEMA' => '4wardit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_804_',
                'SITE_URL' => 'http://www.ncarborday.org',
                'DOMAIN' => 'ncarborday.org',
            ),
            'traductores-traducciones.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_wp10',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.traductores-traducciones.com',
                'DOMAIN' => 'traductores-traducciones.com',
            ),
            'tucazo.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_wp160',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.tucazo.com',
                'DOMAIN' => 'tucazo.com',
            ),
            'umc-kzo.com' =>
            array(
                'TABLE_SCHEMA' => '4wardit_wp226',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.umc-kzo.com',
                'DOMAIN' => 'umc-kzo.com',
            ),
            'translator-translations.org' =>
            array(
                'TABLE_SCHEMA' => '4wardit_wp352',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.translator-translations.org',
                'DOMAIN' => 'translator-translations.org',
            ),
            '520wdw.com' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_1_81532_',
                'SITE_URL' => 'http://www.520wdw.com',
                'DOMAIN' => '520wdw.com',
            ),
            '520zzw.com' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_1_81533_',
                'SITE_URL' => 'http://www.520zzw.com',
                'DOMAIN' => '520zzw.com',
            ),
            '5wjc.com' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_1_81534_',
                'SITE_URL' => 'http://www.5wjc.com',
                'DOMAIN' => '5wjc.com',
            ),
            '64beresford.com' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_1_81535_',
                'SITE_URL' => 'http://www.64beresford.com',
                'DOMAIN' => '64beresford.com',
            ),
            '8w8a.com' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_1_81536_',
                'SITE_URL' => 'http://www.8w8a.com',
                'DOMAIN' => '8w8a.com',
            ),
            'statsbook.me' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_797_',
                'SITE_URL' => 'http://www.statsbook.me',
                'DOMAIN' => 'statsbook.me',
            ),
            'marthaforcongress.org' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_798_',
                'SITE_URL' => 'http://www.marthaforcongress.org',
                'DOMAIN' => 'marthaforcongress.org',
            ),
            'jointmeeting2011bursa.org' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_799_',
                'SITE_URL' => 'http://www.jointmeeting2011bursa.org',
                'DOMAIN' => 'jointmeeting2011bursa.org',
            ),
            'mtwyouth.com' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_800_',
                'SITE_URL' => 'http://www.mtwyouth.com',
                'DOMAIN' => 'mtwyouth.com',
            ),
            'easyaccesschicago.com' =>
            array(
                'TABLE_SCHEMA' => '520wdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_801_',
                'SITE_URL' => 'http://www.easyaccesschicago.com',
                'DOMAIN' => 'easyaccesschicago.com',
            ),
            '6kranch.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1469_',
                'SITE_URL' => 'http://www.6kranch.com',
                'DOMAIN' => '6kranch.com',
            ),
            'bizcommprop.com' =>
            array(
                'TABLE_SCHEMA' => 'AARC-MA_wp907',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.bizcommprop.com',
                'DOMAIN' => 'bizcommprop.com',
            ),
            'champenv.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_1_83478_',
                'SITE_URL' => 'http://www.champenv.com',
                'DOMAIN' => 'champenv.com',
            ),
            'cooldudedirect.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_1_83479_',
                'SITE_URL' => 'http://www.cooldudedirect.com',
                'DOMAIN' => 'cooldudedirect.com',
            ),
            'ehealthtransactions.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_1_83480_',
                'SITE_URL' => 'http://www.ehealthtransactions.com',
                'DOMAIN' => 'ehealthtransactions.com',
            ),
            'ctiaustin.org' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1192_',
                'SITE_URL' => 'http://www.ctiaustin.org',
                'DOMAIN' => 'ctiaustin.org',
            ),
            'cumtyc.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1193_',
                'SITE_URL' => 'http://www.cumtyc.com',
                'DOMAIN' => 'cumtyc.com',
            ),
            'daisygrove.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1194_',
                'SITE_URL' => 'http://www.daisygrove.com',
                'DOMAIN' => 'daisygrove.com',
            ),
            'bariumspringsymca.org' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_682_',
                'SITE_URL' => 'http://www.bariumspringsymca.org',
                'DOMAIN' => 'bariumspringsymca.org',
            ),
            'eastlakesaag.org' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_683_',
                'SITE_URL' => 'http://www.eastlakesaag.org',
                'DOMAIN' => 'eastlakesaag.org',
            ),
            'bbwclubcatalina.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_684_',
                'SITE_URL' => 'http://www.bbwclubcatalina.com',
                'DOMAIN' => 'bbwclubcatalina.com',
            ),
            'marylouisekillen.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_685_',
                'SITE_URL' => 'http://www.marylouisekillen.com',
                'DOMAIN' => 'marylouisekillen.com',
            ),
            'laboratoire-bi-op.com' =>
            array(
                'TABLE_SCHEMA' => '6kranch_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_686_',
                'SITE_URL' => 'http://www.laboratoire-bi-op.com',
                'DOMAIN' => 'laboratoire-bi-op.com',
            ),
            'virtual-emiratespalace-uk.com' =>
            array(
                'TABLE_SCHEMA' => 'achimota_wp88',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.virtual-emiratespalace-uk.com',
                'DOMAIN' => 'virtual-emiratespalace-uk.com',
            ),
            'achimotafund.org' =>
            array(
                'TABLE_SCHEMA' => 'achimota_wp965',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.achimotafund.org',
                'DOMAIN' => 'achimotafund.org',
            ),
            'designersgivingback.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_95_',
                'SITE_URL' => 'http://www.designersgivingback.com',
                'DOMAIN' => 'designersgivingback.com',
            ),
            'airboygsm.net' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_wp904',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.airboygsm.net',
                'DOMAIN' => 'airboygsm.net',
            ),
            'huskerplus.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_1_83898_',
                'SITE_URL' => 'http://www.huskerplus.com',
                'DOMAIN' => 'huskerplus.com',
            ),
            'ballyseedyhotel.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_1_83899_',
                'SITE_URL' => 'http://www.ballyseedyhotel.com',
                'DOMAIN' => 'ballyseedyhotel.com',
            ),
            'ideality-1.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_1_83900_',
                'SITE_URL' => 'http://www.ideality-1.com',
                'DOMAIN' => 'ideality-1.com',
            ),
            'oregonwormtea.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_1_83901_',
                'SITE_URL' => 'http://www.oregonwormtea.com',
                'DOMAIN' => 'oregonwormtea.com',
            ),
            'blueruinpdx.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_wp364',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://blueruinpdx.com',
                'DOMAIN' => 'blueruinpdx.com',
            ),
            'seiinvests.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_wp519',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://seiinvests.com',
                'DOMAIN' => 'seiinvests.com',
            ),
            'april28.net' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_wp784',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://april28.net',
                'DOMAIN' => 'april28.net',
            ),
            'hidaminoiimono.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_wp786',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://hidaminoiimono.com',
                'DOMAIN' => 'hidaminoiimono.com',
            ),
            'ntsblog.info' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_wp910',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ntsblog.info',
                'DOMAIN' => 'ntsblog.info',
            ),
            'avexdesigns.net' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_96_',
                'SITE_URL' => 'http://www.avexdesigns.net',
                'DOMAIN' => 'avexdesigns.net',
            ),
            'debrachesnut2012.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_97_',
                'SITE_URL' => 'http://www.debrachesnut2012.com',
                'DOMAIN' => 'debrachesnut2012.com',
            ),
            'leonaadvancedvirtual.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_99_',
                'SITE_URL' => 'http://www.leonaadvancedvirtual.com',
                'DOMAIN' => 'leonaadvancedvirtual.com',
            ),
            'mercresearch.org' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_782_',
                'SITE_URL' => 'http://www.mercresearch.org',
                'DOMAIN' => 'mercresearch.org',
            ),
            'naisu.info' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_783_',
                'SITE_URL' => 'http://www.naisu.info',
                'DOMAIN' => 'naisu.info',
            ),
            'mpaudiovisual.com' =>
            array(
                'TABLE_SCHEMA' => 'airboygs_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_784_',
                'SITE_URL' => 'http://www.mpaudiovisual.com',
                'DOMAIN' => 'mpaudiovisual.com',
            ),
            'aituofa.com' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_1_2119_',
                'SITE_URL' => 'http://www.aituofa.com',
                'DOMAIN' => 'aituofa.com',
            ),
            'nervetraining.com' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_787_',
                'SITE_URL' => 'http://www.nervetraining.com',
                'DOMAIN' => 'nervetraining.com',
            ),
            'pet-rescues.com' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_788_',
                'SITE_URL' => 'http://www.pet-rescues.com',
                'DOMAIN' => 'pet-rescues.com',
            ),
            'reviewmycontracts.com' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_789_',
                'SITE_URL' => 'http://www.reviewmycontracts.com',
                'DOMAIN' => 'reviewmycontracts.com',
            ),
            'xn--vhq70fbd66i0yp6poitbw22ir69adxr.com' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_790_',
                'SITE_URL' => 'http://www.xn--vhq70fbd66i0yp6poitbw22ir69adxr.com',
                'DOMAIN' => 'xn--vhq70fbd66i0yp6poitbw22ir69adxr.com',
            ),
            'triviafriends.mobi' =>
            array(
                'TABLE_SCHEMA' => 'aituofac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_791_',
                'SITE_URL' => 'http://www.triviafriends.mobi',
                'DOMAIN' => 'triviafriends.mobi',
            ),
            'alago.org' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_1_2556_',
                'SITE_URL' => 'http://www.alago.org',
                'DOMAIN' => 'alago.org',
            ),
            'alaskacaribouhunts.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_1_2557_',
                'SITE_URL' => 'http://www.alaskacaribouhunts.com',
                'DOMAIN' => 'alaskacaribouhunts.com',
            ),
            'alienwebresearch.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_1_2558_',
                'SITE_URL' => 'http://www.alienwebresearch.com',
                'DOMAIN' => 'alienwebresearch.com',
            ),
            'amyberryforchanceryclerk.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_1_2560_',
                'SITE_URL' => 'http://www.amyberryforchanceryclerk.com',
                'DOMAIN' => 'amyberryforchanceryclerk.com',
            ),
            'arubaisbest.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1222_',
                'SITE_URL' => 'http://www.arubaisbest.com',
                'DOMAIN' => 'arubaisbest.com',
            ),
            'auto---insurance.org' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1223_',
                'SITE_URL' => 'http://www.auto---insurance.org',
                'DOMAIN' => 'auto---insurance.org',
            ),
            'capturedreammoments.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1224_',
                'SITE_URL' => 'http://www.capturedreammoments.com',
                'DOMAIN' => 'capturedreammoments.com',
            ),
            'coffeenewsmalaysia.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1225_',
                'SITE_URL' => 'http://www.coffeenewsmalaysia.com',
                'DOMAIN' => 'coffeenewsmalaysia.com',
            ),
            'downtownautomotiverepair.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1226_',
                'SITE_URL' => 'http://www.downtownautomotiverepair.com',
                'DOMAIN' => 'downtownautomotiverepair.com',
            ),
            'hfhwaukesha.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_832_',
                'SITE_URL' => 'http://www.hfhwaukesha.com',
                'DOMAIN' => 'hfhwaukesha.com',
            ),
            'jerryning.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_833_',
                'SITE_URL' => 'http://www.jerryning.com',
                'DOMAIN' => 'jerryning.com',
            ),
            'feastonthis.org' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_834_',
                'SITE_URL' => 'http://www.feastonthis.org',
                'DOMAIN' => 'feastonthis.org',
            ),
            'akamaspenthouse.com' =>
            array(
                'TABLE_SCHEMA' => 'alago_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_835_',
                'SITE_URL' => 'http://www.akamaspenthouse.com',
                'DOMAIN' => 'akamaspenthouse.com',
            ),
            'biz-tips.com' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_wp752',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.biz-tips.com',
                'DOMAIN' => 'biz-tips.com',
            ),
            'bizboxipadbonus.com' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_wp779',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.bizboxipadbonus.com',
                'DOMAIN' => 'bizboxipadbonus.com',
            ),
            'bringawesomeback.com' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_wp755',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.bringawesomeback.com',
                'DOMAIN' => 'bringawesomeback.com',
            ),
            'bringawesomeback.org' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_wp52',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.bringawesomeback.org',
                'DOMAIN' => 'bringawesomeback.org',
            ),
            'kibbutz4u.org' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_777_',
                'SITE_URL' => 'http://www.kibbutz4u.org',
                'DOMAIN' => 'kibbutz4u.org',
            ),
            'emnetwork.us' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_778_',
                'SITE_URL' => 'http://www.emnetwork.us',
                'DOMAIN' => 'emnetwork.us',
            ),
            'gorenewablepower.org' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_779_',
                'SITE_URL' => 'http://www.gorenewablepower.org',
                'DOMAIN' => 'gorenewablepower.org',
            ),
            'epochfilms.info' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_780_',
                'SITE_URL' => 'http://www.epochfilms.info',
                'DOMAIN' => 'epochfilms.info',
            ),
            'juicycoutureskyonline.org' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_781_',
                'SITE_URL' => 'http://www.juicycoutureskyonline.org',
                'DOMAIN' => 'juicycoutureskyonline.org',
            ),
            'relationshipinc.org' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_wp138',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://relationshipinc.org',
                'DOMAIN' => 'relationshipinc.org',
            ),
            'albanycomiccon.com' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_wp17',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.albanycomiccon.com',
                'DOMAIN' => 'albanycomiccon.com',
            ),
            'elanewebsites.com' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_wp435',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://elanewebsites.com',
                'DOMAIN' => 'elanewebsites.com',
            ),
            'csssite.info' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_wp483',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://csssite.info',
                'DOMAIN' => 'csssite.info',
            ),
            '7yin.cc' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_wp507',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://7yin.cc',
                'DOMAIN' => '7yin.cc',
            ),
            'dressesandotherdrugs.com' =>
            array(
                'TABLE_SCHEMA' => 'albanyco_wp851',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://dressesandotherdrugs.com',
                'DOMAIN' => 'dressesandotherdrugs.com',
            ),
            'thuyheartsjohn.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_1_83121_',
                'SITE_URL' => 'http://www.thuyheartsjohn.com',
                'DOMAIN' => 'thuyheartsjohn.com',
            ),
            'clipnsavegreen.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_1_83122_',
                'SITE_URL' => 'http://www.clipnsavegreen.com',
                'DOMAIN' => 'clipnsavegreen.com',
            ),
            'tedinaction.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_1_83123_',
                'SITE_URL' => 'http://www.tedinaction.com',
                'DOMAIN' => 'tedinaction.com',
            ),
            'wpgrafiks.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_1_83124_',
                'SITE_URL' => 'http://www.wpgrafiks.com',
                'DOMAIN' => 'wpgrafiks.com',
            ),
            'orlandomarineproducts.info' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_259_',
                'SITE_URL' => 'http://www.orlandomarineproducts.info',
                'DOMAIN' => 'orlandomarineproducts.info',
            ),
            'overbrookbrothers.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_260_',
                'SITE_URL' => 'http://overbrookbrothers.com',
                'DOMAIN' => 'overbrookbrothers.com',
            ),
            'overbrookbrothersfilm.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_261_',
                'SITE_URL' => 'http://www.overbrookbrothersfilm.com',
                'DOMAIN' => 'overbrookbrothersfilm.com',
            ),
            'alojate.info' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_772_',
                'SITE_URL' => 'http://www.alojate.info',
                'DOMAIN' => 'alojate.info',
            ),
            'dallasburntraumaresearch.org' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_774_',
                'SITE_URL' => 'http://www.dallasburntraumaresearch.org',
                'DOMAIN' => 'dallasburntraumaresearch.org',
            ),
            'healthyhappyhottness.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_775_',
                'SITE_URL' => 'http://www.healthyhappyhottness.com',
                'DOMAIN' => 'healthyhappyhottness.com',
            ),
            'katesmartart.com' =>
            array(
                'TABLE_SCHEMA' => 'ameriven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_776_',
                'SITE_URL' => 'http://www.katesmartart.com',
                'DOMAIN' => 'katesmartart.com',
            ),
            'dailycitizenss.net' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_dailycitizenss',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://dailycitizenss.net',
                'DOMAIN' => 'dailycitizenss.net',
            ),
            'suntatmacau.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_suntatmacau',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://suntatmacau.com',
                'DOMAIN' => 'suntatmacau.com',
            ),
            'angelsoflightchurch.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_1_2700_',
                'SITE_URL' => 'http://www.angelsoflightchurch.com',
                'DOMAIN' => 'angelsoflightchurch.com',
            ),
            'atomichammer.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_1_2701_',
                'SITE_URL' => 'http://www.atomichammer.com',
                'DOMAIN' => 'atomichammer.com',
            ),
            'babe-oftheday.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_1_2702_',
                'SITE_URL' => 'http://www.babe-oftheday.com',
                'DOMAIN' => 'babe-oftheday.com',
            ),
            'berlinflights.us' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_1_2703_',
                'SITE_URL' => 'http://www.berlinflights.us',
                'DOMAIN' => 'berlinflights.us',
            ),
            'bethelbaptistbrooklyn.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_1_2704_',
                'SITE_URL' => 'http://www.bethelbaptistbrooklyn.com',
                'DOMAIN' => 'bethelbaptistbrooklyn.com',
            ),
            'cennywenner.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_767_',
                'SITE_URL' => 'http://www.cennywenner.com',
                'DOMAIN' => 'cennywenner.com',
            ),
            'cleananglingpledge.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_768_',
                'SITE_URL' => 'http://www.cleananglingpledge.com',
                'DOMAIN' => 'cleananglingpledge.com',
            ),
            'damian-szyszko.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_769_',
                'SITE_URL' => 'http://www.damian-szyszko.com',
                'DOMAIN' => 'damian-szyszko.com',
            ),
            'eaglehealthcarestaffing.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_770_',
                'SITE_URL' => 'http://www.eaglehealthcarestaffing.com',
                'DOMAIN' => 'eaglehealthcarestaffing.com',
            ),
            '888baysite.com' =>
            array(
                'TABLE_SCHEMA' => 'angelsof_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_771_',
                'SITE_URL' => 'http://www.888baysite.com',
                'DOMAIN' => '888baysite.com',
            ),
            'getartoffourbacks.com' =>
            array(
                'TABLE_SCHEMA' => 'antispam_getartoff',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://getartoffourbacks.com',
                'DOMAIN' => 'getartoffourbacks.com',
            ),
            'mechanicalcirculatorysupport.com' =>
            array(
                'TABLE_SCHEMA' => 'antispam_mechanical',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://mechanicalcirculatorysupport.com',
                'DOMAIN' => 'mechanicalcirculatorysupport.com',
            ),
            'learnatnorthwoods.mobi' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1112_',
                'SITE_URL' => 'http://www.learnatnorthwoods.mobi',
                'DOMAIN' => 'learnatnorthwoods.mobi',
            ),
            'antrofetish.com' =>
            array(
                'TABLE_SCHEMA' => 'antispam_tkp',
                'TABLE_PREFIX' => 'wp_1_2130_',
                'SITE_URL' => 'http://www.antrofetish.com',
                'DOMAIN' => 'antrofetish.com',
            ),
            'appdelisi.net' =>
            array(
                'TABLE_SCHEMA' => 'antispam_tkp',
                'TABLE_PREFIX' => 'wp_1_2131_',
                'SITE_URL' => 'http://www.appdelisi.net',
                'DOMAIN' => 'appdelisi.net',
            ),
            'applixor.com' =>
            array(
                'TABLE_SCHEMA' => 'antispam_tkp',
                'TABLE_PREFIX' => 'wp_1_2132_',
                'SITE_URL' => 'http://www.applixor.com',
                'DOMAIN' => 'applixor.com',
            ),
            'aracgenclikdernegi.com' =>
            array(
                'TABLE_SCHEMA' => 'antispam_tkp',
                'TABLE_PREFIX' => 'wp_1_2133_',
                'SITE_URL' => 'http://www.aracgenclikdernegi.com',
                'DOMAIN' => 'aracgenclikdernegi.com',
            ),
            'arizonacardinalsticketsnfl.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_1_83175_',
                'SITE_URL' => 'http://www.arizonacardinalsticketsnfl.com',
                'DOMAIN' => 'arizonacardinalsticketsnfl.com',
            ),
            'artesdubai.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_1_83176_',
                'SITE_URL' => 'http://www.artesdubai.com',
                'DOMAIN' => 'artesdubai.com',
            ),
            'atlantafalconsticketsnfl.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_1_83177_',
                'SITE_URL' => 'http://www.atlantafalconsticketsnfl.com',
                'DOMAIN' => 'atlantafalconsticketsnfl.com',
            ),
            'auroramayor.org' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_1_83178_',
                'SITE_URL' => 'http://www.auroramayor.org',
                'DOMAIN' => 'auroramayor.org',
            ),
            'banciella2010.info' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_1_83179_',
                'SITE_URL' => 'http://www.banciella2010.info',
                'DOMAIN' => 'banciella2010.info',
            ),
            'vanwindensgc.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_16_',
                'SITE_URL' => 'http://www.vanwindensgc.com',
                'DOMAIN' => 'vanwindensgc.com',
            ),
            'cogrecruitingprocess.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_17_',
                'SITE_URL' => 'http://www.cogrecruitingprocess.com',
                'DOMAIN' => 'cogrecruitingprocess.com',
            ),
            'studentrightsadvocates.org' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_18_',
                'SITE_URL' => 'http://www.studentrightsadvocates.org',
                'DOMAIN' => 'studentrightsadvocates.org',
            ),
            'thefirezone.net' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_19_',
                'SITE_URL' => 'http://www.thefirezone.net',
                'DOMAIN' => 'thefirezone.net',
            ),
            'hdpornoxo.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_20_',
                'SITE_URL' => 'http://www.hdpornoxo.com',
                'DOMAIN' => 'hdpornoxo.com',
            ),
            'kidztoyz.biz' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_228_',
                'SITE_URL' => 'http://www.kidztoyz.biz',
                'DOMAIN' => 'kidztoyz.biz',
            ),
            'kenyafarming.info' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_231_',
                'SITE_URL' => 'http://www.kenyafarming.info',
                'DOMAIN' => 'kenyafarming.info',
            ),
            'lambda-delta.info' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_232_',
                'SITE_URL' => 'http://www.lambda-delta.info',
                'DOMAIN' => 'lambda-delta.info',
            ),
            'lastsx.ws' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_234_',
                'SITE_URL' => 'http://www.lastsx.ws',
                'DOMAIN' => 'lastsx.ws',
            ),
            'zx334.info' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_wp242',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.zx334.info',
                'DOMAIN' => 'zx334.info',
            ),
            'worldwaterdayla.org' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_wp489',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.worldwaterdayla.org',
                'DOMAIN' => 'worldwaterdayla.org',
            ),
            'worldwaterdayla.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_wp68',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.worldwaterdayla.com',
                'DOMAIN' => 'worldwaterdayla.com',
            ),
            'westpalmbridalshow.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_wp693',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.westpalmbridalshow.com',
                'DOMAIN' => 'westpalmbridalshow.com',
            ),
            'venicefloridawebsites.com' =>
            array(
                'TABLE_SCHEMA' => 'arizonac_wp99',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.venicefloridawebsites.com',
                'DOMAIN' => 'venicefloridawebsites.com',
            ),
            'southernsteelmc.com' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1323_',
                'SITE_URL' => 'http://www.southernsteelmc.com',
                'DOMAIN' => 'southernsteelmc.com',
            ),
            'travelostar.com' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1324_',
                'SITE_URL' => 'http://www.travelostar.com',
                'DOMAIN' => 'travelostar.com',
            ),
            'web-basedbusinesscards.com' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1325_',
                'SITE_URL' => 'http://www.web-basedbusinesscards.com',
                'DOMAIN' => 'web-basedbusinesscards.com',
            ),
            'articledb.mobi' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_964_',
                'SITE_URL' => 'http://www.articledb.mobi',
                'DOMAIN' => 'articledb.mobi',
            ),
            'change-management-training.net' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_965_',
                'SITE_URL' => 'http://www.change-management-training.net',
                'DOMAIN' => 'change-management-training.net',
            ),
            'finansekonomi.org' =>
            array(
                'TABLE_SCHEMA' => 'articled_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_966_',
                'SITE_URL' => 'http://www.finansekonomi.org',
                'DOMAIN' => 'finansekonomi.org',
            ),
            'atwisdomstable.org' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_1_84269_',
                'SITE_URL' => 'http://www.atwisdomstable.org',
                'DOMAIN' => 'atwisdomstable.org',
            ),
            'blogbydc.com' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_1_84270_',
                'SITE_URL' => 'http://www.blogbydc.com',
                'DOMAIN' => 'blogbydc.com',
            ),
            'chiquigymnic.com' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_1_84271_',
                'SITE_URL' => 'http://www.chiquigymnic.com',
                'DOMAIN' => 'chiquigymnic.com',
            ),
            'climatekits.com' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_1_84272_',
                'SITE_URL' => 'http://www.climatekits.com',
                'DOMAIN' => 'climatekits.com',
            ),
            'cobusinessleads.com' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_1_84273_',
                'SITE_URL' => 'http://www.cobusinessleads.com',
                'DOMAIN' => 'cobusinessleads.com',
            ),
            'merrelltrips.com' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_220_',
                'SITE_URL' => 'http://www.merrelltrips.com',
                'DOMAIN' => 'merrelltrips.com',
            ),
            'mm36.org' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_229_',
                'SITE_URL' => 'http://www.mm36.org',
                'DOMAIN' => 'mm36.org',
            ),
            'openeraventures.com' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_230_',
                'SITE_URL' => 'http://www.openeraventures.com',
                'DOMAIN' => 'openeraventures.com',
            ),
            'myperfectpralines.com' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_235_',
                'SITE_URL' => 'http://www.myperfectpralines.com',
                'DOMAIN' => 'myperfectpralines.com',
            ),
            'openim.ws' =>
            array(
                'TABLE_SCHEMA' => 'atwisdom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_242_',
                'SITE_URL' => 'http://www.openim.ws',
                'DOMAIN' => 'openim.ws',
            ),
            'israelimages.net' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_israelimages',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.israelimages.net',
                'DOMAIN' => 'israelimages.net',
            ),
            'mitalphaphi.com' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_mitalphaphi',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://mitalphaphi.com',
                'DOMAIN' => 'mitalphaphi.com',
            ),
            'bagswiththebasics.org' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_1_84362_',
                'SITE_URL' => 'http://www.bagswiththebasics.org',
                'DOMAIN' => 'bagswiththebasics.org',
            ),
            'goodtoday.info' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_1_84363_',
                'SITE_URL' => 'http://www.goodtoday.info',
                'DOMAIN' => 'goodtoday.info',
            ),
            'hdsanc.org' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_1_84364_',
                'SITE_URL' => 'http://www.hdsanc.org',
                'DOMAIN' => 'hdsanc.org',
            ),
            'make3200.com' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_1_84365_',
                'SITE_URL' => 'http://www.make3200.com',
                'DOMAIN' => 'make3200.com',
            ),
            'martialartsmaster.org' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_1_84366_',
                'SITE_URL' => 'http://www.martialartsmaster.org',
                'DOMAIN' => 'martialartsmaster.org',
            ),
            'lostandfoundkeys.org' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_179_',
                'SITE_URL' => 'http://www.lostandfoundkeys.org',
                'DOMAIN' => 'lostandfoundkeys.org',
            ),
            'pradoimagen.com' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_204_',
                'SITE_URL' => 'http://www.pradoimagen.com',
                'DOMAIN' => 'pradoimagen.com',
            ),
            'leaferapp.com' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_233_',
                'SITE_URL' => 'http://www.leaferapp.com',
                'DOMAIN' => 'leaferapp.com',
            ),
            'loisfraleyfoundation.com' =>
            array(
                'TABLE_SCHEMA' => 'bagswith_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_236_',
                'SITE_URL' => 'http://www.loisfraleyfoundation.com',
                'DOMAIN' => 'loisfraleyfoundation.com',
            ),
            'webschemes.net' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1326_',
                'SITE_URL' => 'http://www.webschemes.net',
                'DOMAIN' => 'webschemes.net',
            ),
            'worldofflower.net' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1327_',
                'SITE_URL' => 'http://www.worldofflower.net',
                'DOMAIN' => 'worldofflower.net',
            ),
            'themattperry.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1328_',
                'SITE_URL' => 'http://www.themattperry.com',
                'DOMAIN' => 'themattperry.com',
            ),
            'trafficforwords.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1329_',
                'SITE_URL' => 'http://www.trafficforwords.com',
                'DOMAIN' => 'trafficforwords.com',
            ),
            'stubhubtop25.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_967_',
                'SITE_URL' => 'http://www.stubhubtop25.com',
                'DOMAIN' => 'stubhubtop25.com',
            ),
            'earphoriareviews.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_968_',
                'SITE_URL' => 'http://www.earphoriareviews.com',
                'DOMAIN' => 'earphoriareviews.com',
            ),
            'bct878.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_969_',
                'SITE_URL' => 'http://www.bct878.com',
                'DOMAIN' => 'bct878.com',
            ),
            'latitude33beer.com' =>
            array(
                'TABLE_SCHEMA' => 'bcteight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_970_',
                'SITE_URL' => 'http://www.latitude33beer.com',
                'DOMAIN' => 'latitude33beer.com',
            ),
            'maeaas.com' =>
            array(
                'TABLE_SCHEMA' => 'beesknee_tkp',
                'TABLE_PREFIX' => 'wp_1_81492_',
                'SITE_URL' => 'http://www.maeaas.com',
                'DOMAIN' => 'maeaas.com',
            ),
            'mechanicalsealindia.com' =>
            array(
                'TABLE_SCHEMA' => 'beesknee_tkp',
                'TABLE_PREFIX' => 'wp_1_81493_',
                'SITE_URL' => 'http://www.mechanicalsealindia.com',
                'DOMAIN' => 'mechanicalsealindia.com',
            ),
            'israelart.me' =>
            array(
                'TABLE_SCHEMA' => 'beesknee_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_217_',
                'SITE_URL' => 'http://www.israelart.me',
                'DOMAIN' => 'israelart.me',
            ),
            'janectimm.com' =>
            array(
                'TABLE_SCHEMA' => 'beesknee_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_218_',
                'SITE_URL' => 'http://www.janectimm.com',
                'DOMAIN' => 'janectimm.com',
            ),
            'jovon2johnson.com' =>
            array(
                'TABLE_SCHEMA' => 'beesknee_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_227_',
                'SITE_URL' => 'http://jovon2johnson.com',
                'DOMAIN' => 'jovon2johnson.com',
            ),
            'mirpf.com' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_mirpf',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.mirpf.com',
                'DOMAIN' => 'mirpf.com',
            ),
            'pictureitcompany.com' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_pictureitcompany',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://pictureitcompany.com',
                'DOMAIN' => 'pictureitcompany.com',
            ),
            'bellacasaapex.com' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_1_81557_',
                'SITE_URL' => 'http://www.bellacasaapex.com',
                'DOMAIN' => 'bellacasaapex.com',
            ),
            'bemood.com' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_1_81558_',
                'SITE_URL' => 'http://www.bemood.com',
                'DOMAIN' => 'bemood.com',
            ),
            'bestchoicebrazil.com' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_1_81559_',
                'SITE_URL' => 'http://www.bestchoicebrazil.com',
                'DOMAIN' => 'bestchoicebrazil.com',
            ),
            'beveragefiller.com' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_1_81560_',
                'SITE_URL' => 'http://www.beveragefiller.com',
                'DOMAIN' => 'beveragefiller.com',
            ),
            'bhaketzev.com' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_1_81561_',
                'SITE_URL' => 'http://www.bhaketzev.com',
                'DOMAIN' => 'bhaketzev.com',
            ),
            'gobflo.org' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_170_',
                'SITE_URL' => 'http://www.gobflo.org',
                'DOMAIN' => 'gobflo.org',
            ),
            'hmelevskih.com' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_208_',
                'SITE_URL' => 'http://www.hmelevskih.com',
                'DOMAIN' => 'hmelevskih.com',
            ),
            'gruposocmedia.mobi' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_221_',
                'SITE_URL' => 'http://www.gruposocmedia.mobi',
                'DOMAIN' => 'gruposocmedia.mobi',
            ),
            'hotklip.info' =>
            array(
                'TABLE_SCHEMA' => 'bellacas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_223_',
                'SITE_URL' => 'http://www.hotklip.info',
                'DOMAIN' => 'hotklip.info',
            ),
            'bellyopt.com' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_1_83461_',
                'SITE_URL' => 'http://www.bellyopt.com',
                'DOMAIN' => 'bellyopt.com',
            ),
            'emf-bioshield.net' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_1_83462_',
                'SITE_URL' => 'http://www.emf-bioshield.net',
                'DOMAIN' => 'emf-bioshield.net',
            ),
            'aceconline.org' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_1_83463_',
                'SITE_URL' => 'http://www.aceconline.org',
                'DOMAIN' => 'aceconline.org',
            ),
            'americani.org' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_1_83464_',
                'SITE_URL' => 'http://www.americani.org',
                'DOMAIN' => 'americani.org',
            ),
            'schleutker-sites.org' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_1_83465_',
                'SITE_URL' => 'http://www.schleutker-sites.org',
                'DOMAIN' => 'schleutker-sites.org',
            ),
            'in-asia.net' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_222_',
                'SITE_URL' => 'http://www.in-asia.net',
                'DOMAIN' => 'in-asia.net',
            ),
            'inovmakers.com' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_224_',
                'SITE_URL' => 'http://www.inovmakers.com',
                'DOMAIN' => 'inovmakers.com',
            ),
            'ipraybcp.com' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_225_',
                'SITE_URL' => 'http://www.ipraybcp.com',
                'DOMAIN' => 'ipraybcp.com',
            ),
            'irstaxattorneys.org' =>
            array(
                'TABLE_SCHEMA' => 'bellyopt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_226_',
                'SITE_URL' => 'http://www.irstaxattorneys.org',
                'DOMAIN' => 'irstaxattorneys.org',
            ),
            'bergstrasse-kiel.com' =>
            array(
                'TABLE_SCHEMA' => 'bergstra_tkp',
                'TABLE_PREFIX' => 'wp_1_82951_',
                'SITE_URL' => 'http://www.bergstrasse-kiel.com',
                'DOMAIN' => 'bergstrasse-kiel.com',
            ),
            'bobbyjhayes.com' =>
            array(
                'TABLE_SCHEMA' => 'bergstra_tkp',
                'TABLE_PREFIX' => 'wp_1_82952_',
                'SITE_URL' => 'http://www.bobbyjhayes.com',
                'DOMAIN' => 'bobbyjhayes.com',
            ),
            'clinicwebsite.net' =>
            array(
                'TABLE_SCHEMA' => 'bergstra_tkp',
                'TABLE_PREFIX' => 'wp_1_82953_',
                'SITE_URL' => 'http://www.clinicwebsite.net',
                'DOMAIN' => 'clinicwebsite.net',
            ),
            'colorandappearance.com' =>
            array(
                'TABLE_SCHEMA' => 'bergstra_tkp',
                'TABLE_PREFIX' => 'wp_1_82954_',
                'SITE_URL' => 'http://www.colorandappearance.com',
                'DOMAIN' => 'colorandappearance.com',
            ),
            'comermejor.org' =>
            array(
                'TABLE_SCHEMA' => 'bergstra_tkp',
                'TABLE_PREFIX' => 'wp_1_82955_',
                'SITE_URL' => 'http://www.comermejor.org',
                'DOMAIN' => 'comermejor.org',
            ),
            'quantumcollaborations.org' =>
            array(
                'TABLE_SCHEMA' => 'bergstra_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_122_',
                'SITE_URL' => 'http://www.quantumcollaborations.org',
                'DOMAIN' => 'quantumcollaborations.org',
            ),
            'radiotrainwreck.com' =>
            array(
                'TABLE_SCHEMA' => 'bergstra_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_123_',
                'SITE_URL' => 'http://www.radiotrainwreck.com',
                'DOMAIN' => 'radiotrainwreck.com',
            ),
            'radiousora.info' =>
            array(
                'TABLE_SCHEMA' => 'bergstra_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_124_',
                'SITE_URL' => 'http://www.radiousora.info',
                'DOMAIN' => 'radiousora.info',
            ),
            'reviveourcommunity.com' =>
            array(
                'TABLE_SCHEMA' => 'bergstra_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_125_',
                'SITE_URL' => 'http://reviveourcommunity.com',
                'DOMAIN' => 'reviveourcommunity.com',
            ),
            'kontaktotal.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_kontaktotal',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://kontaktotal.com',
                'DOMAIN' => 'kontaktotal.com',
            ),
            'bestmeatball.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_1_82621_',
                'SITE_URL' => 'http://www.bestmeatball.com',
                'DOMAIN' => 'bestmeatball.com',
            ),
            'biggestlosercville.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_1_82622_',
                'SITE_URL' => 'http://www.biggestlosercville.com',
                'DOMAIN' => 'biggestlosercville.com',
            ),
            'cjohnsonmedia.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_1_82623_',
                'SITE_URL' => 'http://www.cjohnsonmedia.com',
                'DOMAIN' => 'cjohnsonmedia.com',
            ),
            'dominicacottage.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_1_82624_',
                'SITE_URL' => 'http://www.dominicacottage.com',
                'DOMAIN' => 'dominicacottage.com',
            ),
            'greatchickenwingfestival.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_1_82625_',
                'SITE_URL' => 'http://www.greatchickenwingfestival.com',
                'DOMAIN' => 'greatchickenwingfestival.com',
            ),
            'free-erotic-gay-sex-stories.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_212_',
                'SITE_URL' => 'http://free-erotic-gay-sex-stories.com',
                'DOMAIN' => 'free-erotic-gay-sex-stories.com',
            ),
            'freshberryapp.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_213_',
                'SITE_URL' => 'http://www.freshberryapp.com',
                'DOMAIN' => 'freshberryapp.com',
            ),
            'gaochundy.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_214_',
                'SITE_URL' => 'http://www.gaochundy.com',
                'DOMAIN' => 'gaochundy.com',
            ),
            'getrealnhlvip.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_215_',
                'SITE_URL' => 'http://www.getrealnhlvip.com',
                'DOMAIN' => 'getrealnhlvip.com',
            ),
            'ggmap1085.info' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_216_',
                'SITE_URL' => 'http://www.ggmap1085.info',
                'DOMAIN' => 'ggmap1085.info',
            ),
            'voiedelhetre.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmeat_voiedelhetre',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://voiedelhetre.com',
                'DOMAIN' => 'voiedelhetre.com',
            ),
            'bestmult.info' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_1_83110_',
                'SITE_URL' => 'http://www.bestmult.info',
                'DOMAIN' => 'bestmult.info',
            ),
            'clicktrot.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_1_83111_',
                'SITE_URL' => 'http://www.clicktrot.com',
                'DOMAIN' => 'clicktrot.com',
            ),
            '9000ipadwallpapers.info' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_1_83112_',
                'SITE_URL' => 'http://www.9000ipadwallpapers.info',
                'DOMAIN' => '9000ipadwallpapers.info',
            ),
            'anniemyachen.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_1_83113_',
                'SITE_URL' => 'http://www.anniemyachen.com',
                'DOMAIN' => 'anniemyachen.com',
            ),
            'daviesinteractive.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_1_83114_',
                'SITE_URL' => 'http://www.daviesinteractive.com',
                'DOMAIN' => 'daviesinteractive.com',
            ),
            'myhappyweight.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_250_',
                'SITE_URL' => 'http://www.myhappyweight.com',
                'DOMAIN' => 'myhappyweight.com',
            ),
            'myprogressivealliance.org' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_251_',
                'SITE_URL' => 'http://www.myprogressivealliance.org',
                'DOMAIN' => 'myprogressivealliance.org',
            ),
            'nashvilletspa.org' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_253_',
                'SITE_URL' => 'http://www.nashvilletspa.org',
                'DOMAIN' => 'nashvilletspa.org',
            ),
            'nbasalaries.net' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_427_',
                'SITE_URL' => 'http://www.nbasalaries.net',
                'DOMAIN' => 'nbasalaries.net',
            ),
            'mediaandpeople.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_196_',
                'SITE_URL' => 'http://www.mediaandpeople.com',
                'DOMAIN' => 'mediaandpeople.com',
            ),
            'emmetttillblog.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_197_',
                'SITE_URL' => 'http://www.emmetttillblog.com',
                'DOMAIN' => 'emmetttillblog.com',
            ),
            'meatissues.org' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_206_',
                'SITE_URL' => 'http://www.meatissues.org',
                'DOMAIN' => 'meatissues.org',
            ),
            'eurocenterdiursa.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_207_',
                'SITE_URL' => 'http://www.eurocenterdiursa.com',
                'DOMAIN' => 'eurocenterdiursa.com',
            ),
            'gen-engineers.com' =>
            array(
                'TABLE_SCHEMA' => 'bestmult_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_209_',
                'SITE_URL' => 'http://www.gen-engineers.com',
                'DOMAIN' => 'gen-engineers.com',
            ),
            'flawlesscalendar.com' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_flawlesscalendar',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://flawlesscalendar.com',
                'DOMAIN' => 'flawlesscalendar.com',
            ),
            'beteradigital.net' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_1_83947_',
                'SITE_URL' => 'http://www.beteradigital.net',
                'DOMAIN' => 'beteradigital.net',
            ),
            'cartoonistsforjapan.com' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_1_83948_',
                'SITE_URL' => 'http://www.cartoonistsforjapan.com',
                'DOMAIN' => 'cartoonistsforjapan.com',
            ),
            'talkspam.com' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_1_83949_',
                'SITE_URL' => 'http://www.talkspam.com',
                'DOMAIN' => 'talkspam.com',
            ),
            'kobonze.com' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_1_83950_',
                'SITE_URL' => 'http://www.kobonze.com',
                'DOMAIN' => 'kobonze.com',
            ),
            'theplunge2011.org' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_1_83951_',
                'SITE_URL' => 'http://www.theplunge2011.org',
                'DOMAIN' => 'theplunge2011.org',
            ),
            'kettlebellworkoutsx.com' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_184_',
                'SITE_URL' => 'http://www.kettlebellworkoutsx.com',
                'DOMAIN' => 'kettlebellworkoutsx.com',
            ),
            'farmtablecatering.com' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_188_',
                'SITE_URL' => 'http://www.farmtablecatering.com',
                'DOMAIN' => 'farmtablecatering.com',
            ),
            'ebsresumeservice.com' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_200_',
                'SITE_URL' => 'http://www.ebsresumeservice.com',
                'DOMAIN' => 'ebsresumeservice.com',
            ),
            'fairfieldctrealestate.net' =>
            array(
                'TABLE_SCHEMA' => 'beteradi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_219_',
                'SITE_URL' => 'http://www.fairfieldctrealestate.net',
                'DOMAIN' => 'fairfieldctrealestate.net',
            ),
            'biagio.info' =>
            array(
                'TABLE_SCHEMA' => 'biagio_tkp',
                'TABLE_PREFIX' => 'wp_1_83421_',
                'SITE_URL' => 'http://www.biagio.info',
                'DOMAIN' => 'biagio.info',
            ),
            'gloomcorps.com' =>
            array(
                'TABLE_SCHEMA' => 'biagio_tkp',
                'TABLE_PREFIX' => 'wp_1_83422_',
                'SITE_URL' => 'http://www.gloomcorps.com',
                'DOMAIN' => 'gloomcorps.com',
            ),
            'hreye.com' =>
            array(
                'TABLE_SCHEMA' => 'biagio_tkp',
                'TABLE_PREFIX' => 'wp_1_83423_',
                'SITE_URL' => 'http://www.hreye.com',
                'DOMAIN' => 'hreye.com',
            ),
            '1autoinsurances.info' =>
            array(
                'TABLE_SCHEMA' => 'biagio_tkp',
                'TABLE_PREFIX' => 'wp_1_83424_',
                'SITE_URL' => 'http://www.1autoinsurances.info',
                'DOMAIN' => '1autoinsurances.info',
            ),
            'myrailtrain.info' =>
            array(
                'TABLE_SCHEMA' => 'biagio_tkp',
                'TABLE_PREFIX' => 'wp_1_83425_',
                'SITE_URL' => 'http://www.myrailtrain.info',
                'DOMAIN' => 'myrailtrain.info',
            ),
            'biencomun-peru.org' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_976_',
                'SITE_URL' => 'http://www.biencomun-peru.org',
                'DOMAIN' => 'biencomun-peru.org',
            ),
            'paraguaysoberano.org' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_977_',
                'SITE_URL' => 'http://www.paraguaysoberano.org',
                'DOMAIN' => 'paraguaysoberano.org',
            ),
            'summitautismacademy.org' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_978_',
                'SITE_URL' => 'http://www.summitautismacademy.org',
                'DOMAIN' => 'summitautismacademy.org',
            ),
            'k8mall.com' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_979_',
                'SITE_URL' => 'http://www.k8mall.com',
                'DOMAIN' => 'k8mall.com',
            ),
            'wallstreetchinese.org' =>
            array(
                'TABLE_SCHEMA' => 'biencomu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_980_',
                'SITE_URL' => 'http://www.wallstreetchinese.org',
                'DOMAIN' => 'wallstreetchinese.org',
            ),
            'bjthwm.com' =>
            array(
                'TABLE_SCHEMA' => 'bjthwm_tkp',
                'TABLE_PREFIX' => 'wp_1_2154_',
                'SITE_URL' => 'http://www.bjthwm.com',
                'DOMAIN' => 'bjthwm.com',
            ),
            'bollywoodportfolio.com' =>
            array(
                'TABLE_SCHEMA' => 'bjthwm_tkp',
                'TABLE_PREFIX' => 'wp_1_2155_',
                'SITE_URL' => 'http://www.bollywoodportfolio.com',
                'DOMAIN' => 'bollywoodportfolio.com',
            ),
            'bolmevelevha.com' =>
            array(
                'TABLE_SCHEMA' => 'bjthwm_tkp',
                'TABLE_PREFIX' => 'wp_1_2156_',
                'SITE_URL' => 'http://www.bolmevelevha.com',
                'DOMAIN' => 'bolmevelevha.com',
            ),
            'bonmuavn.com' =>
            array(
                'TABLE_SCHEMA' => 'bjthwm_tkp',
                'TABLE_PREFIX' => 'wp_1_2157_',
                'SITE_URL' => 'http://www.bonmuavn.com',
                'DOMAIN' => 'bonmuavn.com',
            ),
            'booklerical.info' =>
            array(
                'TABLE_SCHEMA' => 'bjthwm_tkp',
                'TABLE_PREFIX' => 'wp_1_2158_',
                'SITE_URL' => 'http://www.booklerical.info',
                'DOMAIN' => 'booklerical.info',
            ),
            'bjzljy.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_1_2571_',
                'SITE_URL' => 'http://www.bjzljy.com',
                'DOMAIN' => 'bjzljy.com',
            ),
            'blogcity.us' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_1_2572_',
                'SITE_URL' => 'http://www.blogcity.us',
                'DOMAIN' => 'blogcity.us',
            ),
            'bortisztitas.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_1_2573_',
                'SITE_URL' => 'http://www.bortisztitas.com',
                'DOMAIN' => 'bortisztitas.com',
            ),
            'bouncyleaf.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_1_2574_',
                'SITE_URL' => 'http://www.bouncyleaf.com',
                'DOMAIN' => 'bouncyleaf.com',
            ),
            'campmoneypit.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_1_2575_',
                'SITE_URL' => 'http://www.campmoneypit.com',
                'DOMAIN' => 'campmoneypit.com',
            ),
            'inchocolates.net' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1218_',
                'SITE_URL' => 'http://www.inchocolates.net',
                'DOMAIN' => 'inchocolates.net',
            ),
            'lapetanque.org' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1220_',
                'SITE_URL' => 'http://www.lapetanque.org',
                'DOMAIN' => 'lapetanque.org',
            ),
            'artspiritsilks.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1221_',
                'SITE_URL' => 'http://www.artspiritsilks.com',
                'DOMAIN' => 'artspiritsilks.com',
            ),
            'isawhow.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_827_',
                'SITE_URL' => 'http://www.isawhow.com',
                'DOMAIN' => 'isawhow.com',
            ),
            'princess-savanna.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_828_',
                'SITE_URL' => 'http://www.princess-savanna.com',
                'DOMAIN' => 'princess-savanna.com',
            ),
            'inter-track.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_829_',
                'SITE_URL' => 'http://www.inter-track.com',
                'DOMAIN' => 'inter-track.com',
            ),
            'bishopartsblog.com' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_830_',
                'SITE_URL' => 'http://www.bishopartsblog.com',
                'DOMAIN' => 'bishopartsblog.com',
            ),
            'helenlee.info' =>
            array(
                'TABLE_SCHEMA' => 'bjzljy_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_831_',
                'SITE_URL' => 'http://www.helenlee.info',
                'DOMAIN' => 'helenlee.info',
            ),
            'blackduckflyfishing.com' =>
            array(
                'TABLE_SCHEMA' => 'blackduc_tkp',
                'TABLE_PREFIX' => 'wp_1_82849_',
                'SITE_URL' => 'http://www.blackduckflyfishing.com',
                'DOMAIN' => 'blackduckflyfishing.com',
            ),
            'bluesfoundry.com' =>
            array(
                'TABLE_SCHEMA' => 'blackduc_tkp',
                'TABLE_PREFIX' => 'wp_1_82850_',
                'SITE_URL' => 'http://www.bluesfoundry.com',
                'DOMAIN' => 'bluesfoundry.com',
            ),
            'bomberosyumbo.org' =>
            array(
                'TABLE_SCHEMA' => 'blackduc_tkp',
                'TABLE_PREFIX' => 'wp_1_82851_',
                'SITE_URL' => 'http://www.bomberosyumbo.org',
                'DOMAIN' => 'bomberosyumbo.org',
            ),
            'bubleshare.com' =>
            array(
                'TABLE_SCHEMA' => 'blackduc_tkp',
                'TABLE_PREFIX' => 'wp_1_82852_',
                'SITE_URL' => 'http://www.bubleshare.com',
                'DOMAIN' => 'bubleshare.com',
            ),
            'buybeachesrealestate.com' =>
            array(
                'TABLE_SCHEMA' => 'blackduc_tkp',
                'TABLE_PREFIX' => 'wp_1_82853_',
                'SITE_URL' => 'http://www.buybeachesrealestate.com',
                'DOMAIN' => 'buybeachesrealestate.com',
            ),
            'debtwatchharrisburg.com' =>
            array(
                'TABLE_SCHEMA' => 'blackduc_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_18_',
                'SITE_URL' => 'http://debtwatchharrisburg.com',
                'DOMAIN' => 'debtwatchharrisburg.com',
            ),
            'cristianoronaldo.ws' =>
            array(
                'TABLE_SCHEMA' => 'blackduc_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_42_',
                'SITE_URL' => 'http://cristianoronaldo.ws',
                'DOMAIN' => 'cristianoronaldo.ws',
            ),
            'crookedtrailsresort.com' =>
            array(
                'TABLE_SCHEMA' => 'blackduc_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_43_',
                'SITE_URL' => 'http://crookedtrailsresort.com',
                'DOMAIN' => 'crookedtrailsresort.com',
            ),
            'csulbultimate.com' =>
            array(
                'TABLE_SCHEMA' => 'blackduc_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_44_',
                'SITE_URL' => 'http://csulbultimate.com',
                'DOMAIN' => 'csulbultimate.com',
            ),
            'bluebirdfarms.com' =>
            array(
                'TABLE_SCHEMA' => 'bluebird_tkp',
                'TABLE_PREFIX' => 'wp_1_83500_',
                'SITE_URL' => 'http://www.bluebirdfarms.com',
                'DOMAIN' => 'bluebirdfarms.com',
            ),
            'gouldie.com' =>
            array(
                'TABLE_SCHEMA' => 'bluebird_wp825',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.gouldie.com',
                'DOMAIN' => 'gouldie.com',
            ),
            'micrasoft.net' =>
            array(
                'TABLE_SCHEMA' => 'bluebird_tkp',
                'TABLE_PREFIX' => 'wp_1_83502_',
                'SITE_URL' => 'http://www.micrasoft.net',
                'DOMAIN' => 'micrasoft.net',
            ),
            'netsplash.net' =>
            array(
                'TABLE_SCHEMA' => 'bluebird_tkp',
                'TABLE_PREFIX' => 'wp_1_83503_',
                'SITE_URL' => 'http://www.netsplash.net',
                'DOMAIN' => 'netsplash.net',
            ),
            'robotrends.net' =>
            array(
                'TABLE_SCHEMA' => 'bluebird_tkp',
                'TABLE_PREFIX' => 'wp_1_83504_',
                'SITE_URL' => 'http://www.robotrends.net',
                'DOMAIN' => 'robotrends.net',
            ),
            'bnoma.net' =>
            array(
                'TABLE_SCHEMA' => 'bnoma_tkp',
                'TABLE_PREFIX' => 'wp_1_83056_',
                'SITE_URL' => 'http://www.bnoma.net',
                'DOMAIN' => 'bnoma.net',
            ),
            'bodymaxpt.com' =>
            array(
                'TABLE_SCHEMA' => 'bnoma_tkp',
                'TABLE_PREFIX' => 'wp_1_83057_',
                'SITE_URL' => 'http://www.bodymaxpt.com',
                'DOMAIN' => 'bodymaxpt.com',
            ),
            'bpfboston.org' =>
            array(
                'TABLE_SCHEMA' => 'bnoma_tkp',
                'TABLE_PREFIX' => 'wp_1_83058_',
                'SITE_URL' => 'http://www.bpfboston.org',
                'DOMAIN' => 'bpfboston.org',
            ),
            'broadwovenfabric.com' =>
            array(
                'TABLE_SCHEMA' => 'bnoma_tkp',
                'TABLE_PREFIX' => 'wp_1_83059_',
                'SITE_URL' => 'http://www.broadwovenfabric.com',
                'DOMAIN' => 'broadwovenfabric.com',
            ),
            'broadwovenfabricmill.com' =>
            array(
                'TABLE_SCHEMA' => 'bnoma_tkp',
                'TABLE_PREFIX' => 'wp_1_83060_',
                'SITE_URL' => 'http://www.broadwovenfabricmill.com',
                'DOMAIN' => 'broadwovenfabricmill.com',
            ),
            'doubleexposuremedia.tv' =>
            array(
                'TABLE_SCHEMA' => 'bnoma_wp858',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://doubleexposuremedia.tv/wp',
                'DOMAIN' => 'doubleexposuremedia.tv',
            ),
            'e-angelfund.ws' =>
            array(
                'TABLE_SCHEMA' => 'bnoma_wp815',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://e-angelfund.ws',
                'DOMAIN' => 'e-angelfund.ws',
            ),
            'dressvane.com' =>
            array(
                'TABLE_SCHEMA' => 'bnoma_wp875',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.dressvane.com',
                'DOMAIN' => 'dressvane.com',
            ),
            'bostickhoghunting.com' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_1_83962_',
                'SITE_URL' => 'http://www.bostickhoghunting.com',
                'DOMAIN' => 'bostickhoghunting.com',
            ),
            'bostickboarhunting.com' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_1_83963_',
                'SITE_URL' => 'http://www.bostickboarhunting.com',
                'DOMAIN' => 'bostickboarhunting.com',
            ),
            'another-me.net' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_1_83964_',
                'SITE_URL' => 'http://www.another-me.net',
                'DOMAIN' => 'another-me.net',
            ),
            'aspirewebsites.com' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_1_83965_',
                'SITE_URL' => 'http://www.aspirewebsites.com',
                'DOMAIN' => 'aspirewebsites.com',
            ),
            'securechurches.com' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_1_83966_',
                'SITE_URL' => 'http://www.securechurches.com',
                'DOMAIN' => 'securechurches.com',
            ),
            'kolumnen.me' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_202_',
                'SITE_URL' => 'http://www.kolumnen.me',
                'DOMAIN' => 'kolumnen.me',
            ),
            'llctours.com' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_203_',
                'SITE_URL' => 'http://www.llctours.com',
                'DOMAIN' => 'llctours.com',
            ),
            'marycanconetani.com' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_205_',
                'SITE_URL' => 'http://www.marycanconetani.com',
                'DOMAIN' => 'marycanconetani.com',
            ),
            'luteapress.com' =>
            array(
                'TABLE_SCHEMA' => 'bostickh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_211_',
                'SITE_URL' => 'http://www.luteapress.com',
                'DOMAIN' => 'luteapress.com',
            ),
            'bostonavenuephoto.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_1_83892_',
                'SITE_URL' => 'http://www.bostonavenuephoto.com',
                'DOMAIN' => 'bostonavenuephoto.com',
            ),
            'dtechnoart.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_1_83893_',
                'SITE_URL' => 'http://www.dtechnoart.com',
                'DOMAIN' => 'dtechnoart.com',
            ),
            'valeriepattersonwatercolors.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_1_83894_',
                'SITE_URL' => 'http://www.valeriepattersonwatercolors.com',
                'DOMAIN' => 'valeriepattersonwatercolors.com',
            ),
            'doblesparejas.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_1_83895_',
                'SITE_URL' => 'http://www.doblesparejas.com',
                'DOMAIN' => 'doblesparejas.com',
            ),
            'thecolorstudio.net' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_1_83896_',
                'SITE_URL' => 'http://www.thecolorstudio.net',
                'DOMAIN' => 'thecolorstudio.net',
            ),
            'josemarroyo.org' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_176_',
                'SITE_URL' => 'http://www.josemarroyo.org',
                'DOMAIN' => 'josemarroyo.org',
            ),
            'kabbalahvictims.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_191_',
                'SITE_URL' => 'http://www.kabbalahvictims.com',
                'DOMAIN' => 'kabbalahvictims.com',
            ),
            'jasontschoppdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_194_',
                'SITE_URL' => 'http://www.jasontschoppdesign.com',
                'DOMAIN' => 'jasontschoppdesign.com',
            ),
            'justtryingtobebetter.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_199_',
                'SITE_URL' => 'http://www.justtryingtobebetter.com',
                'DOMAIN' => 'justtryingtobebetter.com',
            ),
            'keithpricegallery.com' =>
            array(
                'TABLE_SCHEMA' => 'bostonav_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_201_',
                'SITE_URL' => 'http://www.keithpricegallery.com',
                'DOMAIN' => 'keithpricegallery.com',
            ),
            'brujeriagay.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_1_83061_',
                'SITE_URL' => 'http://www.brujeriagay.com',
                'DOMAIN' => 'brujeriagay.com',
            ),
            'carprop.net' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_1_83062_',
                'SITE_URL' => 'http://www.carprop.net',
                'DOMAIN' => 'carprop.net',
            ),
            'cloakfb.info' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_1_83063_',
                'SITE_URL' => 'http://www.cloakfb.info',
                'DOMAIN' => 'cloakfb.info',
            ),
            'commercialbaker.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_1_83064_',
                'SITE_URL' => 'http://www.commercialbaker.com',
                'DOMAIN' => 'commercialbaker.com',
            ),
            'custodyactions.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_1_83065_',
                'SITE_URL' => 'http://www.custodyactions.com',
                'DOMAIN' => 'custodyactions.com',
            ),
            'harvarddebateinstitutes.org' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_181_',
                'SITE_URL' => 'http://www.harvarddebateinstitutes.org',
                'DOMAIN' => 'harvarddebateinstitutes.org',
            ),
            'idrisgoodwin.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_183_',
                'SITE_URL' => 'http://www.idrisgoodwin.com',
                'DOMAIN' => 'idrisgoodwin.com',
            ),
            'hairsalonsinsnellville.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_185_',
                'SITE_URL' => 'http://www.hairsalonsinsnellville.com',
                'DOMAIN' => 'hairsalonsinsnellville.com',
            ),
            'hbhdmw.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_189_',
                'SITE_URL' => 'http://www.hbhdmw.com',
                'DOMAIN' => 'hbhdmw.com',
            ),
            'hope9.mobi' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_198_',
                'SITE_URL' => 'http://www.hope9.mobi',
                'DOMAIN' => 'hope9.mobi',
            ),
            'robinqueen.net' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_wp149',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://robinqueen.net',
                'DOMAIN' => 'robinqueen.net',
            ),
            'epausac.info' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_wp164',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://epausac.info',
                'DOMAIN' => 'epausac.info',
            ),
            'overtownyouthcenter.org' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_wp182',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://overtownyouthcenter.org',
                'DOMAIN' => 'overtownyouthcenter.org',
            ),
            'epass5000.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_wp380',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://epass5000.com',
                'DOMAIN' => 'epass5000.com',
            ),
            'homoyoudidnt.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_wp431',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://homoyoudidnt.com',
                'DOMAIN' => 'homoyoudidnt.com',
            ),
            'eangelfund.ws' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_wp479',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://eangelfund.ws',
                'DOMAIN' => 'eangelfund.ws',
            ),
            'bowlbattlecreek.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_wp720',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://bowlbattlecreek.com',
                'DOMAIN' => 'bowlbattlecreek.com',
            ),
            'goldenartsoftibet.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_wp773',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://goldenartsoftibet.com',
                'DOMAIN' => 'goldenartsoftibet.com',
            ),
            'epass4000.com' =>
            array(
                'TABLE_SCHEMA' => 'brujeria_wp920',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://epass4000.com',
                'DOMAIN' => 'epass4000.com',
            ),
            'dominovt.com' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wpK_134_',
                'SITE_URL' => 'http://www.dominovt.com',
                'DOMAIN' => 'dominovt.com',
            ),
            'bff-wargame.com' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wpK_135_',
                'SITE_URL' => 'http://www.bff-wargame.com',
                'DOMAIN' => 'bff-wargame.com',
            ),
            'businessfinancesource.com' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_1_82508_',
                'SITE_URL' => 'http://www.businessfinancesource.com',
                'DOMAIN' => 'businessfinancesource.com',
            ),
            'tanara-ad.com' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_1_82509_',
                'SITE_URL' => 'http://www.tanara-ad.com',
                'DOMAIN' => 'tanara-ad.com',
            ),
            'unofficiallexfire.com' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_1_82510_',
                'SITE_URL' => 'http://www.unofficiallexfire.com',
                'DOMAIN' => 'unofficiallexfire.com',
            ),
            'alexyrs.org' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_1_82511_',
                'SITE_URL' => 'http://www.alexyrs.org',
                'DOMAIN' => 'alexyrs.org',
            ),
            'igtf.info' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_182_',
                'SITE_URL' => 'http://www.igtf.info',
                'DOMAIN' => 'igtf.info',
            ),
            'iphoneslutz.com' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_190_',
                'SITE_URL' => 'http://www.iphoneslutz.com',
                'DOMAIN' => 'iphoneslutz.com',
            ),
            'indianaclubhouses.org' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_192_',
                'SITE_URL' => 'http://www.indianaclubhouses.org',
                'DOMAIN' => 'indianaclubhouses.org',
            ),
            'international-divorces.com' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_193_',
                'SITE_URL' => 'http://www.international-divorces.com',
                'DOMAIN' => 'international-divorces.com',
            ),
            'info-banca.info' =>
            array(
                'TABLE_SCHEMA' => 'busines1_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_195_',
                'SITE_URL' => 'http://www.info-banca.info',
                'DOMAIN' => 'info-banca.info',
            ),
            '1st-tech-support.com' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1170_',
                'SITE_URL' => 'http://www.1st-tech-support.com',
                'DOMAIN' => '1st-tech-support.com',
            ),
            '37th.org' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1171_',
                'SITE_URL' => 'http://www.37th.org',
                'DOMAIN' => '37th.org',
            ),
            'cadreintegremali.org' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_860_',
                'SITE_URL' => 'http://www.cadreintegremali.org',
                'DOMAIN' => 'cadreintegremali.org',
            ),
            'hypnotized-by-puffy.com' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_861_',
                'SITE_URL' => 'http://www.hypnotized-by-puffy.com',
                'DOMAIN' => 'hypnotized-by-puffy.com',
            ),
            'in-the-gutter.info' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_862_',
                'SITE_URL' => 'http://www.in-the-gutter.info',
                'DOMAIN' => 'in-the-gutter.info',
            ),
            'scottcountyartcrawl.com' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_863_',
                'SITE_URL' => 'http://www.scottcountyartcrawl.com',
                'DOMAIN' => 'scottcountyartcrawl.com',
            ),
            'chapiteaunews.net' =>
            array(
                'TABLE_SCHEMA' => 'cadreint_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_864_',
                'SITE_URL' => 'http://www.chapiteaunews.net',
                'DOMAIN' => 'chapiteaunews.net',
            ),
            'cafer.info' =>
            array(
                'TABLE_SCHEMA' => 'cafer_tkp',
                'TABLE_PREFIX' => 'wp_1_83854_',
                'SITE_URL' => 'http://www.cafer.info',
                'DOMAIN' => 'cafer.info',
            ),
            'scrumti.com' =>
            array(
                'TABLE_SCHEMA' => 'cafer_tkp',
                'TABLE_PREFIX' => 'wp_1_83855_',
                'SITE_URL' => 'http://www.scrumti.com',
                'DOMAIN' => 'scrumti.com',
            ),
            'cedargrovephotography.com' =>
            array(
                'TABLE_SCHEMA' => 'cafer_tkp',
                'TABLE_PREFIX' => 'wp_1_83856_',
                'SITE_URL' => 'http://www.cedargrovephotography.com',
                'DOMAIN' => 'cedargrovephotography.com',
            ),
            '18jeux.com' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wpK_136_',
                'SITE_URL' => 'http://www.18jeux.com',
                'DOMAIN' => '18jeux.com',
            ),
            'nextleveladvantage.com' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wpK_137_',
                'SITE_URL' => 'http://www.nextleveladvantage.com',
                'DOMAIN' => 'nextleveladvantage.com',
            ),
            'calvarygracebrethren.org' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_1_82503_',
                'SITE_URL' => 'http://www.calvarygracebrethren.org',
                'DOMAIN' => 'calvarygracebrethren.org',
            ),
            'ghanawisdom.org' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_1_82504_',
                'SITE_URL' => 'http://www.ghanawisdom.org',
                'DOMAIN' => 'ghanawisdom.org',
            ),
            'lockc.org' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_1_82505_',
                'SITE_URL' => 'http://www.lockc.org',
                'DOMAIN' => 'lockc.org',
            ),
            'worldventure.org' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_1_82506_',
                'SITE_URL' => 'http://www.worldventure.org',
                'DOMAIN' => 'worldventure.org',
            ),
            'alphagreek.com' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_1_82507_',
                'SITE_URL' => 'http://www.alphagreek.com',
                'DOMAIN' => 'alphagreek.com',
            ),
            'guarbecque.net' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_171_',
                'SITE_URL' => 'http://www.guarbecque.net',
                'DOMAIN' => 'guarbecque.net',
            ),
            'hairsalonsinduluth.com' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_177_',
                'SITE_URL' => 'http://www.hairsalonsinduluth.com',
                'DOMAIN' => 'hairsalonsinduluth.com',
            ),
            'hairsalonsinlilburn.com' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_178_',
                'SITE_URL' => 'http://www.hairsalonsinlilburn.com',
                'DOMAIN' => 'hairsalonsinlilburn.com',
            ),
            'guolufengji.com' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_180_',
                'SITE_URL' => 'http://www.guolufengji.com',
                'DOMAIN' => 'guolufengji.com',
            ),
            'hairsalonsindunwoody.com' =>
            array(
                'TABLE_SCHEMA' => 'calvaryg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_187_',
                'SITE_URL' => 'http://www.hairsalonsindunwoody.com',
                'DOMAIN' => 'hairsalonsindunwoody.com',
            ),
            'criaroutraescola.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1184_',
                'SITE_URL' => 'http://www.criaroutraescola.com',
                'DOMAIN' => 'criaroutraescola.com',
            ),
            'cornerstone-construction-remodeling.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1188_',
                'SITE_URL' => 'http://www.cornerstone-construction-remodeling.com',
                'DOMAIN' => 'cornerstone-construction-remodeling.com',
            ),
            'cramerperformance.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1190_',
                'SITE_URL' => 'http://www.cramerperformance.com',
                'DOMAIN' => 'cramerperformance.com',
            ),
            'causalive.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_890_',
                'SITE_URL' => 'http://www.causalive.com',
                'DOMAIN' => 'causalive.com',
            ),
            'crestorinjuryattorneys.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_891_',
                'SITE_URL' => 'http://www.crestorinjuryattorneys.com',
                'DOMAIN' => 'crestorinjuryattorneys.com',
            ),
            'csfmanagement.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_892_',
                'SITE_URL' => 'http://www.csfmanagement.com',
                'DOMAIN' => 'csfmanagement.com',
            ),
            'customcateringbyplum.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_893_',
                'SITE_URL' => 'http://www.customcateringbyplum.com',
                'DOMAIN' => 'customcateringbyplum.com',
            ),
            'allisonveltz.com' =>
            array(
                'TABLE_SCHEMA' => 'causaliv_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_894_',
                'SITE_URL' => 'http://www.allisonveltz.com',
                'DOMAIN' => 'allisonveltz.com',
            ),
            'cdcwilmingtonde.com' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_1_83037_',
                'SITE_URL' => 'http://www.cdcwilmingtonde.com',
                'DOMAIN' => 'cdcwilmingtonde.com',
            ),
            'triangleblvd.tv' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_1_83038_',
                'SITE_URL' => 'http://www.triangleblvd.tv',
                'DOMAIN' => 'triangleblvd.tv',
            ),
            'tablaecstasy.com' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_1_83039_',
                'SITE_URL' => 'http://www.tablaecstasy.com',
                'DOMAIN' => 'tablaecstasy.com',
            ),
            'nametag.tv' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_1_83040_',
                'SITE_URL' => 'http://www.nametag.tv',
                'DOMAIN' => 'nametag.tv',
            ),
            'rmjdwcn.info' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_1_83041_',
                'SITE_URL' => 'http://www.rmjdwcn.info',
                'DOMAIN' => 'rmjdwcn.info',
            ),
            'ns1.cdcwilmingtonde.com' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_190_',
                'SITE_URL' => 'http://ns1.cdcwilmingtonde.com',
                'DOMAIN' => 'ns1.cdcwilmingtonde.com',
            ),
            'azurebestpractices.net' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_191_',
                'SITE_URL' => 'http://www.azurebestpractices.net',
                'DOMAIN' => 'azurebestpractices.net',
            ),
            'azurecomponents.net' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_192_',
                'SITE_URL' => 'http://azurecomponents.net',
                'DOMAIN' => 'azurecomponents.net',
            ),
            'azureprogramming.net' =>
            array(
                'TABLE_SCHEMA' => 'cdcwilmi_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_193_',
                'SITE_URL' => 'http://azureprogramming.net',
                'DOMAIN' => 'azureprogramming.net',
            ),
            'ncttechnologies.com' =>
            array(
                'TABLE_SCHEMA' => 'celebite_wp466',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.ncttechnologies.com',
                'DOMAIN' => 'ncttechnologies.com',
            ),
            'cgefc.org' =>
            array(
                'TABLE_SCHEMA' => 'celebite_tkp',
                'TABLE_PREFIX' => 'wp_1_82092_',
                'SITE_URL' => 'http://www.cgefc.org',
                'DOMAIN' => 'cgefc.org',
            ),
            'chaverim.org' =>
            array(
                'TABLE_SCHEMA' => 'celebite_tkp',
                'TABLE_PREFIX' => 'wp_1_82093_',
                'SITE_URL' => 'http://www.chaverim.org',
                'DOMAIN' => 'chaverim.org',
            ),
            'christmasinapril-stl.org' =>
            array(
                'TABLE_SCHEMA' => 'celebite_tkp',
                'TABLE_PREFIX' => 'wp_1_82094_',
                'SITE_URL' => 'http://www.christmasinapril-stl.org',
                'DOMAIN' => 'christmasinapril-stl.org',
            ),
            'commodorescraftedboats.com' =>
            array(
                'TABLE_SCHEMA' => 'celebite_tkp',
                'TABLE_PREFIX' => 'wp_1_82095_',
                'SITE_URL' => 'http://www.commodorescraftedboats.com',
                'DOMAIN' => 'commodorescraftedboats.com',
            ),
            'powertoolsinfo.com' =>
            array(
                'TABLE_SCHEMA' => 'celebite_wp932',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://powertoolsinfo.com',
                'DOMAIN' => 'powertoolsinfo.com',
            ),
            'zunepower.com' =>
            array(
                'TABLE_SCHEMA' => 'celebite_wp636',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://zunepower.com',
                'DOMAIN' => 'zunepower.com',
            ),
            'deafartsnetwork.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_1_84408_',
                'SITE_URL' => 'http://www.deafartsnetwork.com',
                'DOMAIN' => 'deafartsnetwork.com',
            ),
            'clockworkbpm.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_1_84407_',
                'SITE_URL' => 'http://www.clockworkbpm.com',
                'DOMAIN' => 'clockworkbpm.com',
            ),
            'facepals.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_1_84409_',
                'SITE_URL' => 'http://www.facepals.com',
                'DOMAIN' => 'facepals.com',
            ),
            'harneycountyhistory.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_1_84410_',
                'SITE_URL' => 'http://www.harneycountyhistory.com',
                'DOMAIN' => 'harneycountyhistory.com',
            ),
            'gardnernewsnow.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_172_',
                'SITE_URL' => 'http://www.gardnernewsnow.com',
                'DOMAIN' => 'gardnernewsnow.com',
            ),
            'galvestonpiratesc.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_173_',
                'SITE_URL' => 'http://www.galvestonpiratesc.com',
                'DOMAIN' => 'galvestonpiratesc.com',
            ),
            'glenscottallen.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_174_',
                'SITE_URL' => 'http://www.glenscottallen.com',
                'DOMAIN' => 'glenscottallen.com',
            ),
            'gohawaii-activities.com' =>
            array(
                'TABLE_SCHEMA' => 'charlott_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_175_',
                'SITE_URL' => 'http://www.gohawaii-activities.com',
                'DOMAIN' => 'gohawaii-activities.com',
            ),
            'chetek.info' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_1_83697_',
                'SITE_URL' => 'http://www.chetek.info',
                'DOMAIN' => 'chetek.info',
            ),
            'billberger.org' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_1_83698_',
                'SITE_URL' => 'http://www.billberger.org',
                'DOMAIN' => 'billberger.org',
            ),
            'lesclefsdorireland.com' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_1_83699_',
                'SITE_URL' => 'http://www.lesclefsdorireland.com',
                'DOMAIN' => 'lesclefsdorireland.com',
            ),
            '1800vendingtriple.com' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_1_83700_',
                'SITE_URL' => 'http://www.1800vendingtriple.com',
                'DOMAIN' => '1800vendingtriple.com',
            ),
            'blogolepsy.com' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_1_83701_',
                'SITE_URL' => 'http://www.blogolepsy.com',
                'DOMAIN' => 'blogolepsy.com',
            ),
            'energee.me' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_150_',
                'SITE_URL' => 'http://www.energee.me',
                'DOMAIN' => 'energee.me',
            ),
            'ericvick.com' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_167_',
                'SITE_URL' => 'http://www.ericvick.com',
                'DOMAIN' => 'ericvick.com',
            ),
            'forensicanth.com' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_168_',
                'SITE_URL' => 'http://www.forensicanth.com',
                'DOMAIN' => 'forensicanth.com',
            ),
            'fuckluxury.com' =>
            array(
                'TABLE_SCHEMA' => 'chetek_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_169_',
                'SITE_URL' => 'http://www.fuckluxury.com',
                'DOMAIN' => 'fuckluxury.com',
            ),
            'chocolategala.com' =>
            array(
                'TABLE_SCHEMA' => 'chocolat_tkp',
                'TABLE_PREFIX' => 'wp_1_83807_',
                'SITE_URL' => 'http://www.chocolategala.com',
                'DOMAIN' => 'chocolategala.com',
            ),
            'cognitiveinfiltration.info' =>
            array(
                'TABLE_SCHEMA' => 'chocolat_tkp',
                'TABLE_PREFIX' => 'wp_1_83808_',
                'SITE_URL' => 'http://www.cognitiveinfiltration.info',
                'DOMAIN' => 'cognitiveinfiltration.info',
            ),
            'cricketingquiz.com' =>
            array(
                'TABLE_SCHEMA' => 'chocolat_tkp',
                'TABLE_PREFIX' => 'wp_1_83809_',
                'SITE_URL' => 'http://www.cricketingquiz.com',
                'DOMAIN' => 'cricketingquiz.com',
            ),
            'hawaiivoice.org' =>
            array(
                'TABLE_SCHEMA' => 'chocolat_tkp',
                'TABLE_PREFIX' => 'wp_1_83810_',
                'SITE_URL' => 'http://www.hawaiivoice.org',
                'DOMAIN' => 'hawaiivoice.org',
            ),
            'laughandbericher.com' =>
            array(
                'TABLE_SCHEMA' => 'chocolat_tkp',
                'TABLE_PREFIX' => 'wp_1_83811_',
                'SITE_URL' => 'http://www.laughandbericher.com',
                'DOMAIN' => 'laughandbericher.com',
            ),
            'chrishanaka.com' =>
            array(
                'TABLE_SCHEMA' => 'chrishan_tkp',
                'TABLE_PREFIX' => 'wp_1_2174_',
                'SITE_URL' => 'http://www.chrishanaka.com',
                'DOMAIN' => 'chrishanaka.com',
            ),
            'christianmusicsinger.info' =>
            array(
                'TABLE_SCHEMA' => 'chrishan_tkp',
                'TABLE_PREFIX' => 'wp_1_2175_',
                'SITE_URL' => 'http://www.christianmusicsinger.info',
                'DOMAIN' => 'christianmusicsinger.info',
            ),
            'chrisweachock.com' =>
            array(
                'TABLE_SCHEMA' => 'chrishan_tkp',
                'TABLE_PREFIX' => 'wp_1_2176_',
                'SITE_URL' => 'http://www.chrisweachock.com',
                'DOMAIN' => 'chrisweachock.com',
            ),
            'chuloproductora.com' =>
            array(
                'TABLE_SCHEMA' => 'chrishan_tkp',
                'TABLE_PREFIX' => 'wp_1_2177_',
                'SITE_URL' => 'http://www.chuloproductora.com',
                'DOMAIN' => 'chuloproductora.com',
            ),
            'chushi123.info' =>
            array(
                'TABLE_SCHEMA' => 'chrishan_tkp',
                'TABLE_PREFIX' => 'wp_1_2178_',
                'SITE_URL' => 'http://www.chushi123.info',
                'DOMAIN' => 'chushi123.info',
            ),
            'plowboymansion.com' =>
            array(
                'TABLE_SCHEMA' => 'cicsearc_tkp',
                'TABLE_PREFIX' => 'wpK_138_',
                'SITE_URL' => 'http://www.plowboymansion.com',
                'DOMAIN' => 'plowboymansion.com',
            ),
            'cicsearch.com' =>
            array(
                'TABLE_SCHEMA' => 'cicsearc_tkp',
                'TABLE_PREFIX' => 'wp_1_83320_',
                'SITE_URL' => 'http://www.cicsearch.com',
                'DOMAIN' => 'cicsearch.com',
            ),
            'claptonvenicehouse.com' =>
            array(
                'TABLE_SCHEMA' => 'cicsearc_tkp',
                'TABLE_PREFIX' => 'wp_1_83321_',
                'SITE_URL' => 'http://www.claptonvenicehouse.com',
                'DOMAIN' => 'claptonvenicehouse.com',
            ),
            'customwordpresssitedesign.com' =>
            array(
                'TABLE_SCHEMA' => 'cicsearc_tkp',
                'TABLE_PREFIX' => 'wp_1_83322_',
                'SITE_URL' => 'http://www.customwordpresssitedesign.com',
                'DOMAIN' => 'customwordpresssitedesign.com',
            ),
            'dailynews-news.com' =>
            array(
                'TABLE_SCHEMA' => 'cicsearc_tkp',
                'TABLE_PREFIX' => 'wp_1_83323_',
                'SITE_URL' => 'http://www.dailynews-news.com',
                'DOMAIN' => 'dailynews-news.com',
            ),
            'digital-culture-industry.com' =>
            array(
                'TABLE_SCHEMA' => 'cicsearc_tkp',
                'TABLE_PREFIX' => 'wp_1_83324_',
                'SITE_URL' => 'http://www.digital-culture-industry.com',
                'DOMAIN' => 'digital-culture-industry.com',
            ),
            'cincinnatibengalsticketsnfl.com' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_tkp',
                'TABLE_PREFIX' => 'wp_1_83190_',
                'SITE_URL' => 'http://www.cincinnatibengalsticketsnfl.com',
                'DOMAIN' => 'cincinnatibengalsticketsnfl.com',
            ),
            'clevelandindiansticketsmlb.com' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_tkp',
                'TABLE_PREFIX' => 'wp_1_83191_',
                'SITE_URL' => 'http://www.clevelandindiansticketsmlb.com',
                'DOMAIN' => 'clevelandindiansticketsmlb.com',
            ),
            'coloradorockiesticketsmlb.com' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_tkp',
                'TABLE_PREFIX' => 'wp_1_83192_',
                'SITE_URL' => 'http://www.coloradorockiesticketsmlb.com',
                'DOMAIN' => 'coloradorockiesticketsmlb.com',
            ),
            'cruhopeforhaiti.com' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_tkp',
                'TABLE_PREFIX' => 'wp_1_83193_',
                'SITE_URL' => 'http://www.cruhopeforhaiti.com',
                'DOMAIN' => 'cruhopeforhaiti.com',
            ),
            'cubiczirconias.net' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_tkp',
                'TABLE_PREFIX' => 'wp_1_83194_',
                'SITE_URL' => 'http://www.cubiczirconias.net',
                'DOMAIN' => 'cubiczirconias.net',
            ),
            'tppdallas.org' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_wp578',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://tppdallas.org',
                'DOMAIN' => 'tppdallas.org',
            ),
            'mtadamsacademy.com' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_wp772',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://mtadamsacademy.com',
                'DOMAIN' => 'mtadamsacademy.com',
            ),
            'wildwoodcrestmotelbeaurivage.info' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_wp357',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://wildwoodcrestmotelbeaurivage.info',
                'DOMAIN' => 'wildwoodcrestmotelbeaurivage.info',
            ),
            'deweysamericangrill.com' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_wp272',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://deweysamericangrill.com',
                'DOMAIN' => 'deweysamericangrill.com',
            ),
            'awsboard.com' =>
            array(
                'TABLE_SCHEMA' => 'cincinna_wp839',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://awsboard.com',
                'DOMAIN' => 'awsboard.com',
            ),
            'dogualmanya.info' =>
            array(
                'TABLE_SCHEMA' => 'cleaning_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_141_',
                'SITE_URL' => 'http://www.dogualmanya.info',
                'DOMAIN' => 'dogualmanya.info',
            ),
            'djantoinequa.com' =>
            array(
                'TABLE_SCHEMA' => 'cleaning_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_147_',
                'SITE_URL' => 'http://www.djantoinequa.com',
                'DOMAIN' => 'djantoinequa.com',
            ),
            'e-lat.org' =>
            array(
                'TABLE_SCHEMA' => 'cleaning_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_157_',
                'SITE_URL' => 'http://www.e-lat.org',
                'DOMAIN' => 'e-lat.org',
            ),
            'emu-solutions.com' =>
            array(
                'TABLE_SCHEMA' => 'cleaning_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_158_',
                'SITE_URL' => 'http://www.emu-solutions.com',
                'DOMAIN' => 'emu-solutions.com',
            ),
            'eastafricametaproject.org' =>
            array(
                'TABLE_SCHEMA' => 'cleaning_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_160_',
                'SITE_URL' => 'http://www.eastafricametaproject.org',
                'DOMAIN' => 'eastafricametaproject.org',
            ),
            'click2usjobs.org' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_1_82759_',
                'SITE_URL' => 'http://www.click2usjobs.org',
                'DOMAIN' => 'click2usjobs.org',
            ),
            'coffeebeansx.com' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_1_82760_',
                'SITE_URL' => 'http://www.coffeebeansx.com',
                'DOMAIN' => 'coffeebeansx.com',
            ),
            'sportchix.com' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_1_82761_',
                'SITE_URL' => 'http://www.sportchix.com',
                'DOMAIN' => 'sportchix.com',
            ),
            'click2naturalization.com' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_1_82762_',
                'SITE_URL' => 'http://www.click2naturalization.com',
                'DOMAIN' => 'click2naturalization.com',
            ),
            'click2usjob.com' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_1_82763_',
                'SITE_URL' => 'http://www.click2usjob.com',
                'DOMAIN' => 'click2usjob.com',
            ),
            'detroitpistonsjerseys.info' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_144_',
                'SITE_URL' => 'http://www.detroitpistonsjerseys.info',
                'DOMAIN' => 'detroitpistonsjerseys.info',
            ),
            'daemoniq.org' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_154_',
                'SITE_URL' => 'http://www.daemoniq.org',
                'DOMAIN' => 'daemoniq.org',
            ),
            'digitalharvestdomains.com' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_155_',
                'SITE_URL' => 'http://www.digitalharvestdomains.com',
                'DOMAIN' => 'digitalharvestdomains.com',
            ),
            'discoverkalamazoo.org' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_156_',
                'SITE_URL' => 'http://www.discoverkalamazoo.org',
                'DOMAIN' => 'discoverkalamazoo.org',
            ),
            'diamondselectonline.com' =>
            array(
                'TABLE_SCHEMA' => 'click2us_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_165_',
                'SITE_URL' => 'http://www.diamondselectonline.com',
                'DOMAIN' => 'diamondselectonline.com',
            ),
            'clubinite.com' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_1_83518_',
                'SITE_URL' => 'http://www.clubinite.com',
                'DOMAIN' => 'clubinite.com',
            ),
            'comeoutandwin.com' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_1_83519_',
                'SITE_URL' => 'http://www.comeoutandwin.com',
                'DOMAIN' => 'comeoutandwin.com',
            ),
            'cozychocolate.com' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_wp57',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.cozychocolate.com',
                'DOMAIN' => 'cozychocolate.com',
            ),
            'daftmanic.com' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_1_83521_',
                'SITE_URL' => 'http://www.daftmanic.com',
                'DOMAIN' => 'daftmanic.com',
            ),
            'dawnsdinnerparty.com' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_1_83523_',
                'SITE_URL' => 'http://www.dawnsdinnerparty.com',
                'DOMAIN' => 'dawnsdinnerparty.com',
            ),
            'clintondalecommunitytheater.org' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_151_',
                'SITE_URL' => 'http://www.clintondalecommunitytheater.org',
                'DOMAIN' => 'clintondalecommunitytheater.org',
            ),
            'conect2012.org' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_152_',
                'SITE_URL' => 'http://www.conect2012.org',
                'DOMAIN' => 'conect2012.org',
            ),
            'coopamare.com' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_153_',
                'SITE_URL' => 'http://www.coopamare.com',
                'DOMAIN' => 'coopamare.com',
            ),
            'coamg.org' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_159_',
                'SITE_URL' => 'http://www.coamg.org',
                'DOMAIN' => 'coamg.org',
            ),
            'cropdusterband.com' =>
            array(
                'TABLE_SCHEMA' => 'clubinit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_164_',
                'SITE_URL' => 'http://www.cropdusterband.com',
                'DOMAIN' => 'cropdusterband.com',
            ),
            'cmyers84online.info' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_1_2586_',
                'SITE_URL' => 'http://www.cmyers84online.info',
                'DOMAIN' => 'cmyers84online.info',
            ),
            'corporateinvestmentbank.com' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_1_2587_',
                'SITE_URL' => 'http://www.corporateinvestmentbank.com',
                'DOMAIN' => 'corporateinvestmentbank.com',
            ),
            'cvbc-yukon.org' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_1_2588_',
                'SITE_URL' => 'http://www.cvbc-yukon.org',
                'DOMAIN' => 'cvbc-yukon.org',
            ),
            'danaball.org' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_1_2589_',
                'SITE_URL' => 'http://www.danaball.org',
                'DOMAIN' => 'danaball.org',
            ),
            'calfirelookouts.org' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_138_',
                'SITE_URL' => 'http://www.calfirelookouts.org',
                'DOMAIN' => 'calfirelookouts.org',
            ),
            'chinarainbowaward.org' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_143_',
                'SITE_URL' => 'http://www.chinarainbowaward.org',
                'DOMAIN' => 'chinarainbowaward.org',
            ),
            'celticcoin.com' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_148_',
                'SITE_URL' => 'http://www.celticcoin.com',
                'DOMAIN' => 'celticcoin.com',
            ),
            'chinascienceandbelief.org' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_149_',
                'SITE_URL' => 'http://www.chinascienceandbelief.org',
                'DOMAIN' => 'chinascienceandbelief.org',
            ),
            'ciaochao.me' =>
            array(
                'TABLE_SCHEMA' => 'cmyers84_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_163_',
                'SITE_URL' => 'http://www.ciaochao.me',
                'DOMAIN' => 'ciaochao.me',
            ),
            'coffeehousegypsies.com' =>
            array(
                'TABLE_SCHEMA' => 'coffeeho_tkp',
                'TABLE_PREFIX' => 'wp_1_82663_',
                'SITE_URL' => 'http://www.coffeehousegypsies.com',
                'DOMAIN' => 'coffeehousegypsies.com',
            ),
            'competingforcustomers.com' =>
            array(
                'TABLE_SCHEMA' => 'coffeeho_tkp',
                'TABLE_PREFIX' => 'wp_1_82664_',
                'SITE_URL' => 'http://www.competingforcustomers.com',
                'DOMAIN' => 'competingforcustomers.com',
            ),
            'elkhartcountyrealtor.com' =>
            array(
                'TABLE_SCHEMA' => 'coffeeho_tkp',
                'TABLE_PREFIX' => 'wp_1_82667_',
                'SITE_URL' => 'http://www.elkhartcountyrealtor.com',
                'DOMAIN' => 'elkhartcountyrealtor.com',
            ),
            '3peasartlounge.com' =>
            array(
                'TABLE_SCHEMA' => 'coffeeho_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_136_',
                'SITE_URL' => 'http://www.3peasartlounge.com',
                'DOMAIN' => '3peasartlounge.com',
            ),
            '90n.us' =>
            array(
                'TABLE_SCHEMA' => 'coffeeho_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_145_',
                'SITE_URL' => 'http://www.90n.us',
                'DOMAIN' => '90n.us',
            ),
            '515mag.net' =>
            array(
                'TABLE_SCHEMA' => 'coffeeho_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_98_',
                'SITE_URL' => 'http://www.515mag.net',
                'DOMAIN' => '515mag.net',
            ),
            'yardandgarden.us' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1337_',
                'SITE_URL' => 'http://www.yardandgarden.us',
                'DOMAIN' => 'yardandgarden.us',
            ),
            'coldstudio.us' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_981_',
                'SITE_URL' => 'http://www.coldstudio.us',
                'DOMAIN' => 'coldstudio.us',
            ),
            'lorenamonsalve.com' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_982_',
                'SITE_URL' => 'http://www.lorenamonsalve.com',
                'DOMAIN' => 'lorenamonsalve.com',
            ),
            'massagem-tantrica.com' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_983_',
                'SITE_URL' => 'http://www.massagem-tantrica.com',
                'DOMAIN' => 'massagem-tantrica.com',
            ),
            'neighborbeenyc.com' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_984_',
                'SITE_URL' => 'http://www.neighborbeenyc.com',
                'DOMAIN' => 'neighborbeenyc.com',
            ),
            'montibellomakeover.com' =>
            array(
                'TABLE_SCHEMA' => 'coldstud_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_985_',
                'SITE_URL' => 'http://www.montibellomakeover.com',
                'DOMAIN' => 'montibellomakeover.com',
            ),
            'southocautogroup.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_southo',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://southocautogroup.com',
                'DOMAIN' => 'southocautogroup.com',
            ),
            'coloradocigarandbrewfest.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_1_84327_',
                'SITE_URL' => 'http://www.coloradocigarandbrewfest.com',
                'DOMAIN' => 'coloradocigarandbrewfest.com',
            ),
            'ecuaexperienceproject2011.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_1_84328_',
                'SITE_URL' => 'http://www.ecuaexperienceproject2011.com',
                'DOMAIN' => 'ecuaexperienceproject2011.com',
            ),
            'givebackgetmore.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_1_84329_',
                'SITE_URL' => 'http://www.givebackgetmore.com',
                'DOMAIN' => 'givebackgetmore.com',
            ),
            'gvsextips.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_1_84330_',
                'SITE_URL' => 'http://www.gvsextips.com',
                'DOMAIN' => 'gvsextips.com',
            ),
            'insanity-workout.biz' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_1_84331_',
                'SITE_URL' => 'http://www.insanity-workout.biz',
                'DOMAIN' => 'insanity-workout.biz',
            ),
            'africanplanit.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_116_',
                'SITE_URL' => 'http://www.africanplanit.com',
                'DOMAIN' => 'africanplanit.com',
            ),
            '1710dodge.info' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_117_',
                'SITE_URL' => 'http://www.1710dodge.info',
                'DOMAIN' => '1710dodge.info',
            ),
            'chicago-real-estate.org' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_132_',
                'SITE_URL' => 'http://www.chicago-real-estate.org',
                'DOMAIN' => 'chicago-real-estate.org',
            ),
            'curiositysavedthehuman.info' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_133_',
                'SITE_URL' => 'http://www.curiositysavedthehuman.info',
                'DOMAIN' => 'curiositysavedthehuman.info',
            ),
            '0731lady.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_134_',
                'SITE_URL' => 'http://www.0731lady.com',
                'DOMAIN' => '0731lady.com',
            ),
            'zhaoliying.net' =>
            array(
                'TABLE_SCHEMA' => 'colorado_wp225',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://zhaoliying.net',
                'DOMAIN' => 'zhaoliying.net',
            ),
            'hotelparigiresidencepalais.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_wp375',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://hotelparigiresidencepalais.com',
                'DOMAIN' => 'hotelparigiresidencepalais.com',
            ),
            'jserraicelions.org' =>
            array(
                'TABLE_SCHEMA' => 'colorado_wp471',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://jserraicelions.org',
                'DOMAIN' => 'jserraicelions.org',
            ),
            'ifhnos.com' =>
            array(
                'TABLE_SCHEMA' => 'colorado_wp765',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ifhnos.com',
                'DOMAIN' => 'ifhnos.com',
            ),
            'constrewood.com' =>
            array(
                'TABLE_SCHEMA' => 'constrew_tkp',
                'TABLE_PREFIX' => 'wp_1_82956_',
                'SITE_URL' => 'http://www.constrewood.com',
                'DOMAIN' => 'constrewood.com',
            ),
            'dcye.org' =>
            array(
                'TABLE_SCHEMA' => 'constrew_tkp',
                'TABLE_PREFIX' => 'wp_1_82957_',
                'SITE_URL' => 'http://www.dcye.org',
                'DOMAIN' => 'dcye.org',
            ),
            'efthollywood.com' =>
            array(
                'TABLE_SCHEMA' => 'constrew_tkp',
                'TABLE_PREFIX' => 'wp_1_82958_',
                'SITE_URL' => 'http://www.efthollywood.com',
                'DOMAIN' => 'efthollywood.com',
            ),
            'einsteingroup.net' =>
            array(
                'TABLE_SCHEMA' => 'constrew_tkp',
                'TABLE_PREFIX' => 'wp_1_82959_',
                'SITE_URL' => 'http://www.einsteingroup.net',
                'DOMAIN' => 'einsteingroup.net',
            ),
            'ekc2000.com' =>
            array(
                'TABLE_SCHEMA' => 'constrew_tkp',
                'TABLE_PREFIX' => 'wp_1_82960_',
                'SITE_URL' => 'http://www.ekc2000.com',
                'DOMAIN' => 'ekc2000.com',
            ),
            'rmwebsec.com' =>
            array(
                'TABLE_SCHEMA' => 'constrew_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_126_',
                'SITE_URL' => 'http://www.rmwebsec.com',
                'DOMAIN' => 'rmwebsec.com',
            ),
            'roat.biz' =>
            array(
                'TABLE_SCHEMA' => 'constrew_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_127_',
                'SITE_URL' => 'http://www.roat.biz',
                'DOMAIN' => 'roat.biz',
            ),
            'rocknriga.com' =>
            array(
                'TABLE_SCHEMA' => 'constrew_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_128_',
                'SITE_URL' => 'http://www.rocknriga.com',
                'DOMAIN' => 'rocknriga.com',
            ),
            'mail.rsangus.com' =>
            array(
                'TABLE_SCHEMA' => 'constrew_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_129_',
                'SITE_URL' => 'http://mail.rsangus.com',
                'DOMAIN' => 'mail.rsangus.com',
            ),
            'dontvoteformydad.com' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_dontvot',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://dontvoteformydad.com',
                'DOMAIN' => 'dontvoteformydad.com',
            ),
            'melodicvibe.com' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_melodic',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://melodicvibe.com',
                'DOMAIN' => 'melodicvibe.com',
            ),
            'dezlab.us' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_115_',
                'SITE_URL' => 'http://www.dezlab.us',
                'DOMAIN' => 'dezlab.us',
            ),
            'cowtownart.com' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_129_',
                'SITE_URL' => 'http://www.cowtownart.com',
                'DOMAIN' => 'cowtownart.com',
            ),
            'daretoloveprojecthaiti.org' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_130_',
                'SITE_URL' => 'http://www.daretoloveprojecthaiti.org',
                'DOMAIN' => 'daretoloveprojecthaiti.org',
            ),
            'aldahlawi.net' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_131_',
                'SITE_URL' => 'http://www.aldahlawi.net',
                'DOMAIN' => 'aldahlawi.net',
            ),
            'cornerstonedesigns.us' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_wp286',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://cornerstonedesigns.us',
                'DOMAIN' => 'cornerstonedesigns.us',
            ),
            'konstantindesign.com' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_wp468',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://konstantindesign.com',
                'DOMAIN' => 'konstantindesign.com',
            ),
            'vuolux.com' =>
            array(
                'TABLE_SCHEMA' => 'cornerst_wp925',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://vuolux.com',
                'DOMAIN' => 'vuolux.com',
            ),
            'cornsnowclassic.com' =>
            array(
                'TABLE_SCHEMA' => 'cornsnow_tkp',
                'TABLE_PREFIX' => 'wp_1_82858_',
                'SITE_URL' => 'http://www.cornsnowclassic.com',
                'DOMAIN' => 'cornsnowclassic.com',
            ),
            'czcac.com' =>
            array(
                'TABLE_SCHEMA' => 'cornsnow_tkp',
                'TABLE_PREFIX' => 'wp_1_82859_',
                'SITE_URL' => 'http://www.czcac.com',
                'DOMAIN' => 'czcac.com',
            ),
            'danaeatonministry.com' =>
            array(
                'TABLE_SCHEMA' => 'cornsnow_tkp',
                'TABLE_PREFIX' => 'wp_1_82860_',
                'SITE_URL' => 'http://www.danaeatonministry.com',
                'DOMAIN' => 'danaeatonministry.com',
            ),
            'denton-designs.com' =>
            array(
                'TABLE_SCHEMA' => 'cornsnow_tkp',
                'TABLE_PREFIX' => 'wp_1_82861_',
                'SITE_URL' => 'http://www.denton-designs.com',
                'DOMAIN' => 'denton-designs.com',
            ),
            'designcompanyc2.com' =>
            array(
                'TABLE_SCHEMA' => 'cornsnow_tkp',
                'TABLE_PREFIX' => 'wp_1_82862_',
                'SITE_URL' => 'http://www.designcompanyc2.com',
                'DOMAIN' => 'designcompanyc2.com',
            ),
            'dsprinting.net' =>
            array(
                'TABLE_SCHEMA' => 'cornsnow_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_327_',
                'SITE_URL' => 'http://dsprinting.net',
                'DOMAIN' => 'dsprinting.net',
            ),
            'dvcags.org' =>
            array(
                'TABLE_SCHEMA' => 'cornsnow_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_328_',
                'SITE_URL' => 'http://dvcags.org',
                'DOMAIN' => 'dvcags.org',
            ),
            'emailofrussiangirls.com' =>
            array(
                'TABLE_SCHEMA' => 'cornsnow_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_329_',
                'SITE_URL' => 'http://emailofrussiangirls.com',
                'DOMAIN' => 'emailofrussiangirls.com',
            ),
            'emailmarketingmike.com' =>
            array(
                'TABLE_SCHEMA' => 'cornsnow_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_330_',
                'SITE_URL' => 'http://www.emailmarketingmike.com',
                'DOMAIN' => 'emailmarketingmike.com',
            ),
            'cqhengxiong.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_1_81588_',
                'SITE_URL' => 'http://www.cqhengxiong.com',
                'DOMAIN' => 'cqhengxiong.com',
            ),
            'cqhmxh.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_1_81589_',
                'SITE_URL' => 'http://www.cqhmxh.com',
                'DOMAIN' => 'cqhmxh.com',
            ),
            'css-soluciones.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_1_81590_',
                'SITE_URL' => 'http://www.css-soluciones.com',
                'DOMAIN' => 'css-soluciones.com',
            ),
            'danielbassmusic.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_1_81591_',
                'SITE_URL' => 'http://www.danielbassmusic.com',
                'DOMAIN' => 'danielbassmusic.com',
            ),
            'days-letter.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_1_81592_',
                'SITE_URL' => 'http://www.days-letter.com',
                'DOMAIN' => 'days-letter.com',
            ),
            'cps3lourdeshospital.org' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_121_',
                'SITE_URL' => 'http://www.cps3lourdeshospital.org',
                'DOMAIN' => 'cps3lourdeshospital.org',
            ),
            'cheapfoodsaver.us' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_125_',
                'SITE_URL' => 'http://www.cheapfoodsaver.us',
                'DOMAIN' => 'cheapfoodsaver.us',
            ),
            'cursotenis.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_126_',
                'SITE_URL' => 'http://www.cursotenis.com',
                'DOMAIN' => 'cursotenis.com',
            ),
            'disbennettonline.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_127_',
                'SITE_URL' => 'http://www.disbennettonline.com',
                'DOMAIN' => 'disbennettonline.com',
            ),
            'diabloofftherecord.com' =>
            array(
                'TABLE_SCHEMA' => 'cqhengxi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1461_',
                'SITE_URL' => 'http://www.diabloofftherecord.com',
                'DOMAIN' => 'diabloofftherecord.com',
            ),
            'painfreeinfo.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1317_',
                'SITE_URL' => 'http://www.painfreeinfo.com',
                'DOMAIN' => 'painfreeinfo.com',
            ),
            'pregnantaftermiscarriage.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1318_',
                'SITE_URL' => 'http://www.pregnantaftermiscarriage.com',
                'DOMAIN' => 'pregnantaftermiscarriage.com',
            ),
            'rehabercise.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1319_',
                'SITE_URL' => 'http://www.rehabercise.com',
                'DOMAIN' => 'rehabercise.com',
            ),
            'promisepreschool.org' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1320_',
                'SITE_URL' => 'http://www.promisepreschool.org',
                'DOMAIN' => 'promisepreschool.org',
            ),
            'santacruzboardsports.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1321_',
                'SITE_URL' => 'http://www.santacruzboardsports.com',
                'DOMAIN' => 'santacruzboardsports.com',
            ),
            'credit-turbo.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_959_',
                'SITE_URL' => 'http://www.credit-turbo.com',
                'DOMAIN' => 'credit-turbo.com',
            ),
            'wirelessgalicia.org' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_960_',
                'SITE_URL' => 'http://www.wirelessgalicia.org',
                'DOMAIN' => 'wirelessgalicia.org',
            ),
            'cyclr.me' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_961_',
                'SITE_URL' => 'http://www.cyclr.me',
                'DOMAIN' => 'cyclr.me',
            ),
            'buildinghealthycommunities2011.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_962_',
                'SITE_URL' => 'http://www.buildinghealthycommunities2011.com',
                'DOMAIN' => 'buildinghealthycommunities2011.com',
            ),
            'pumpitupcyfair.com' =>
            array(
                'TABLE_SCHEMA' => 'credittu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_963_',
                'SITE_URL' => 'http://www.pumpitupcyfair.com',
                'DOMAIN' => 'pumpitupcyfair.com',
            ),
            'curtthompson.org' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_1_83907_',
                'SITE_URL' => 'http://www.curtthompson.org',
                'DOMAIN' => 'curtthompson.org',
            ),
            '1stchoiceinspect.com' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_1_83908_',
                'SITE_URL' => 'http://www.1stchoiceinspect.com',
                'DOMAIN' => '1stchoiceinspect.com',
            ),
            'leftrightdesigns.com' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_1_83909_',
                'SITE_URL' => 'http://www.leftrightdesigns.com',
                'DOMAIN' => 'leftrightdesigns.com',
            ),
            'artshopgallery.org' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_1_83910_',
                'SITE_URL' => 'http://www.artshopgallery.org',
                'DOMAIN' => 'artshopgallery.org',
            ),
            'jwtheblueprinttv.com' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_1_83911_',
                'SITE_URL' => 'http://www.jwtheblueprinttv.com',
                'DOMAIN' => 'jwtheblueprinttv.com',
            ),
            'amtraktrainsportationvideocontest.com' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_103_',
                'SITE_URL' => 'http://www.amtraktrainsportationvideocontest.com',
                'DOMAIN' => 'amtraktrainsportationvideocontest.com',
            ),
            'availableroomsjungfrau.com' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_113_',
                'SITE_URL' => 'http://www.availableroomsjungfrau.com',
                'DOMAIN' => 'availableroomsjungfrau.com',
            ),
            'ad2westmichigan.org' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_122_',
                'SITE_URL' => 'http://www.ad2westmichigan.org',
                'DOMAIN' => 'ad2westmichigan.org',
            ),
            'dudewheresmycar.me' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_128_',
                'SITE_URL' => 'http://www.dudewheresmycar.me',
                'DOMAIN' => 'dudewheresmycar.me',
            ),
            'camels-sale.com' =>
            array(
                'TABLE_SCHEMA' => 'curtthom_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_139_',
                'SITE_URL' => 'http://www.camels-sale.com',
                'DOMAIN' => 'camels-sale.com',
            ),
            'montevideofm.com' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wpK_140_',
                'SITE_URL' => 'http://www.montevideofm.com',
                'DOMAIN' => 'montevideofm.com',
            ),
            'dealus.us' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_1_81593_',
                'SITE_URL' => 'http://www.dealus.us',
                'DOMAIN' => 'dealus.us',
            ),
            'dedhamcurves.com' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_1_81594_',
                'SITE_URL' => 'http://www.dedhamcurves.com',
                'DOMAIN' => 'dedhamcurves.com',
            ),
            'derekwoodske.com' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_1_81595_',
                'SITE_URL' => 'http://www.derekwoodske.com',
                'DOMAIN' => 'derekwoodske.com',
            ),
            'designer-d1.com' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_1_81596_',
                'SITE_URL' => 'http://www.designer-d1.com',
                'DOMAIN' => 'designer-d1.com',
            ),
            'dgfly8.com' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_1_81597_',
                'SITE_URL' => 'http://www.dgfly8.com',
                'DOMAIN' => 'dgfly8.com',
            ),
            'altavoces.info' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_108_',
                'SITE_URL' => 'http://www.altavoces.info',
                'DOMAIN' => 'altavoces.info',
            ),
            'arizonansforabrighterfuture.com' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_118_',
                'SITE_URL' => 'http://www.arizonansforabrighterfuture.com',
                'DOMAIN' => 'arizonansforabrighterfuture.com',
            ),
            '91ddy.com' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_123_',
                'SITE_URL' => 'http://www.91ddy.com',
                'DOMAIN' => '91ddy.com',
            ),
            'abovegroundcontainment.net' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_124_',
                'SITE_URL' => 'http://www.abovegroundcontainment.net',
                'DOMAIN' => 'abovegroundcontainment.net',
            ),
            'achmmpdx.org' =>
            array(
                'TABLE_SCHEMA' => 'dealus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_135_',
                'SITE_URL' => 'http://www.achmmpdx.org',
                'DOMAIN' => 'achmmpdx.org',
            ),
            'dearfrance.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_1_83728_',
                'SITE_URL' => 'http://www.dearfrance.com',
                'DOMAIN' => 'dearfrance.com',
            ),
            'e107livethemes.org' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_1_83729_',
                'SITE_URL' => 'http://www.e107livethemes.org',
                'DOMAIN' => 'e107livethemes.org',
            ),
            'eltkafol.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_wp733',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://eltkafol.com',
                'DOMAIN' => 'eltkafol.com',
            ),
            'floridaka.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_1_83731_',
                'SITE_URL' => 'http://www.floridaka.com',
                'DOMAIN' => 'floridaka.com',
            ),
            'golf0411.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_1_83732_',
                'SITE_URL' => 'http://www.golf0411.com',
                'DOMAIN' => 'golf0411.com',
            ),
            'njspokes.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_wp168',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://njspokes.com',
                'DOMAIN' => 'njspokes.com',
            ),
            'qualitysplitac.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_wp107',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://qualitysplitac.com',
                'DOMAIN' => 'qualitysplitac.com',
            ),
            'cheaperchevy.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_109_',
                'SITE_URL' => 'http://www.cheaperchevy.com',
                'DOMAIN' => 'cheaperchevy.com',
            ),
            'bpcavt.org' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_119_',
                'SITE_URL' => 'http://www.bpcavt.org',
                'DOMAIN' => 'bpcavt.org',
            ),
            'canoncitycolorado.org' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_120_',
                'SITE_URL' => 'http://www.canoncitycolorado.org',
                'DOMAIN' => 'canoncitycolorado.org',
            ),
            'charpo-alberta.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1460_',
                'SITE_URL' => 'http://www.charpo-alberta.com',
                'DOMAIN' => 'charpo-alberta.com',
            ),
            'ccp2010.org' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_162_',
                'SITE_URL' => 'http://www.ccp2010.org',
                'DOMAIN' => 'ccp2010.org',
            ),
            'ravinair.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_wp130',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ravinair.com',
                'DOMAIN' => 'ravinair.com',
            ),
            'nmgtour.info' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_wp162',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://nmgtour.info',
                'DOMAIN' => 'nmgtour.info',
            ),
            'tweetamerica.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_wp221',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://tweetamerica.com',
                'DOMAIN' => 'tweetamerica.com',
            ),
            'wrightwoodradio.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_wp307',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://wrightwoodradio.com',
                'DOMAIN' => 'wrightwoodradio.com',
            ),
            'danielpicon.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_wp479',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://danielpicon.com',
                'DOMAIN' => 'danielpicon.com',
            ),
            'qigong4health.info' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_wp6',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://qigong4health.info',
                'DOMAIN' => 'qigong4health.info',
            ),
            'startupbritain-done-better.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_wp612',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://startupbritain-done-better.com',
                'DOMAIN' => 'startupbritain-done-better.com',
            ),
            'sakal-services.com' =>
            array(
                'TABLE_SCHEMA' => 'dearfran_wp615',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://sakal-services.com',
                'DOMAIN' => 'sakal-services.com',
            ),
            'dekobokobar.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_1_83140_',
                'SITE_URL' => 'http://www.dekobokobar.com',
                'DOMAIN' => 'dekobokobar.com',
            ),
            'acilimfendershanesi.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_1_83141_',
                'SITE_URL' => 'http://www.acilimfendershanesi.com',
                'DOMAIN' => 'acilimfendershanesi.com',
            ),
            'calgary-schools.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_1_83142_',
                'SITE_URL' => 'http://www.calgary-schools.com',
                'DOMAIN' => 'calgary-schools.com',
            ),
            'killtheword.info' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_1_83143_',
                'SITE_URL' => 'http://www.killtheword.info',
                'DOMAIN' => 'killtheword.info',
            ),
            'justmobileprograms.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_1_83144_',
                'SITE_URL' => 'http://www.justmobileprograms.com',
                'DOMAIN' => 'justmobileprograms.com',
            ),
            '0356bentu.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_27_',
                'SITE_URL' => 'http://www.0356bentu.com',
                'DOMAIN' => '0356bentu.com',
            ),
            'ttcradio.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_28_',
                'SITE_URL' => 'http://www.ttcradio.com',
                'DOMAIN' => 'ttcradio.com',
            ),
            'meipimatic.org' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_29_',
                'SITE_URL' => 'http://www.meipimatic.org',
                'DOMAIN' => 'meipimatic.org',
            ),
            'statefairproject.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_30_',
                'SITE_URL' => 'http://www.statefairproject.com',
                'DOMAIN' => 'statefairproject.com',
            ),
            'neogenix.us' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_31_',
                'SITE_URL' => 'http://www.neogenix.us',
                'DOMAIN' => 'neogenix.us',
            ),
            'thenewtrover.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_32_',
                'SITE_URL' => 'http://www.thenewtrover.com',
                'DOMAIN' => 'thenewtrover.com',
            ),
            'thecurrentsband.info' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_104_',
                'SITE_URL' => 'http://www.thecurrentsband.info',
                'DOMAIN' => 'thecurrentsband.info',
            ),
            'smartpocket.info' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_105_',
                'SITE_URL' => 'http://www.smartpocket.info',
                'DOMAIN' => 'smartpocket.info',
            ),
            'stsamuel.org' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_106_',
                'SITE_URL' => 'http://www.stsamuel.org',
                'DOMAIN' => 'stsamuel.org',
            ),
            'renhuan.org' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_90_',
                'SITE_URL' => 'http://www.renhuan.org',
                'DOMAIN' => 'renhuan.org',
            ),
            'murgemusic.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_92_',
                'SITE_URL' => 'http://www.murgemusic.com',
                'DOMAIN' => 'murgemusic.com',
            ),
            'sarasotafloridawebdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_wp144',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.sarasotafloridawebdesign.com',
                'DOMAIN' => 'sarasotafloridawebdesign.com',
            ),
            'sacramentomarineproducts.info' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_wp350',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.sacramentomarineproducts.info',
                'DOMAIN' => 'sacramentomarineproducts.info',
            ),
            'rigrunner.net' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_wp445',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.rigrunner.net',
                'DOMAIN' => 'rigrunner.net',
            ),
            'sacc-sf.com' =>
            array(
                'TABLE_SCHEMA' => 'dekoboko_wp904',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.sacc-sf.com',
                'DOMAIN' => 'sacc-sf.com',
            ),
            'dexterfactorystores.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_1_81126_',
                'SITE_URL' => 'http://www.dexterfactorystores.com',
                'DOMAIN' => 'dexterfactorystores.com',
            ),
            'diariosminimos.info' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_1_81127_',
                'SITE_URL' => 'http://www.diariosminimos.info',
                'DOMAIN' => 'diariosminimos.info',
            ),
            'donphotonews.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_1_81128_',
                'SITE_URL' => 'http://www.donphotonews.com',
                'DOMAIN' => 'donphotonews.com',
            ),
            'duncanheron.name' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_1_81129_',
                'SITE_URL' => 'http://www.duncanheron.name',
                'DOMAIN' => 'duncanheron.name',
            ),
            'eaglerocksystems.net' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_1_81130_',
                'SITE_URL' => 'http://www.eaglerocksystems.net',
                'DOMAIN' => 'eaglerocksystems.net',
            ),
            'hgq168.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_101_',
                'SITE_URL' => 'http://www.hgq168.com',
                'DOMAIN' => 'hgq168.com',
            ),
            'upennifc.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_107_',
                'SITE_URL' => 'http://www.upennifc.com',
                'DOMAIN' => 'upennifc.com',
            ),
            'alejandrorivasfoto.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_110_',
                'SITE_URL' => 'http://www.alejandrorivasfoto.com',
                'DOMAIN' => 'alejandrorivasfoto.com',
            ),
            'eternalunlimited.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_95_',
                'SITE_URL' => 'http://www.eternalunlimited.com',
                'DOMAIN' => 'eternalunlimited.com',
            ),
            'ekoaffaren.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_wp148',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ekoaffaren.com',
                'DOMAIN' => 'ekoaffaren.com',
            ),
            'ancheilibriparlano.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_wp194',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ancheilibriparlano.com',
                'DOMAIN' => 'ancheilibriparlano.com',
            ),
            'jumpsnap.info' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_wp413',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://jumpsnap.info',
                'DOMAIN' => 'jumpsnap.info',
            ),
            'petrosandscan.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_wp536',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://petrosandscan.com',
                'DOMAIN' => 'petrosandscan.com',
            ),
            'bregiannos.com' =>
            array(
                'TABLE_SCHEMA' => 'dexterfa_wp930',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://bregiannos.com',
                'DOMAIN' => 'bregiannos.com',
            ),
            'dlphomeschoolers.info' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_1_83022_',
                'SITE_URL' => 'http://www.dlphomeschoolers.info',
                'DOMAIN' => 'dlphomeschoolers.info',
            ),
            'htssports.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_1_83023_',
                'SITE_URL' => 'http://www.htssports.com',
                'DOMAIN' => 'htssports.com',
            ),
            'kickstarter.us' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_1_83024_',
                'SITE_URL' => 'http://www.kickstarter.us',
                'DOMAIN' => 'kickstarter.us',
            ),
            'krumovgrad.info' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_1_83025_',
                'SITE_URL' => 'http://www.krumovgrad.info',
                'DOMAIN' => 'krumovgrad.info',
            ),
            'legodethsquad.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_1_83026_',
                'SITE_URL' => 'http://www.legodethsquad.com',
                'DOMAIN' => 'legodethsquad.com',
            ),
            'somalireal.net' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_178_',
                'SITE_URL' => 'http://www.somalireal.net',
                'DOMAIN' => 'somalireal.net',
            ),
            'szul.info' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_179_',
                'SITE_URL' => 'http://szul.info',
                'DOMAIN' => 'szul.info',
            ),
            'tiwayoilturkey.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_180_',
                'SITE_URL' => 'http://tiwayoilturkey.com',
                'DOMAIN' => 'tiwayoilturkey.com',
            ),
            'xptv.mobi' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_181_',
                'SITE_URL' => 'http://www.xptv.mobi',
                'DOMAIN' => 'xptv.mobi',
            ),
            'thefamilyplayground.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1441_',
                'SITE_URL' => 'http://www.thefamilyplayground.com',
                'DOMAIN' => 'thefamilyplayground.com',
            ),
            'autostry.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1442_',
                'SITE_URL' => 'http://www.autostry.com',
                'DOMAIN' => 'autostry.com',
            ),
            'ukghdscheap.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1443_',
                'SITE_URL' => 'http://www.ukghdscheap.com',
                'DOMAIN' => 'ukghdscheap.com',
            ),
            'europeanpitchpoint.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1444_',
                'SITE_URL' => 'http://www.europeanpitchpoint.com',
                'DOMAIN' => 'europeanpitchpoint.com',
            ),
            'deltafinance.biz' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1445_',
                'SITE_URL' => 'http://www.deltafinance.biz',
                'DOMAIN' => 'deltafinance.biz',
            ),
            '321debtconsolidationnews.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_599_',
                'SITE_URL' => 'http://www.321debtconsolidationnews.com',
                'DOMAIN' => '321debtconsolidationnews.com',
            ),
            'hanamicron.net' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_655_',
                'SITE_URL' => 'http://www.hanamicron.net',
                'DOMAIN' => 'hanamicron.net',
            ),
            'eegga.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_656_',
                'SITE_URL' => 'http://www.eegga.com',
                'DOMAIN' => 'eegga.com',
            ),
            'knoxvillevoice.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_657_',
                'SITE_URL' => 'http://www.knoxvillevoice.com',
                'DOMAIN' => 'knoxvillevoice.com',
            ),
            'homedecorok.com' =>
            array(
                'TABLE_SCHEMA' => 'dlphomes_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_672_',
                'SITE_URL' => 'http://www.homedecorok.com',
                'DOMAIN' => 'homedecorok.com',
            ),
            'tutorialicious.info' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1436_',
                'SITE_URL' => 'http://www.tutorialicious.info',
                'DOMAIN' => 'tutorialicious.info',
            ),
            'allabout-weightloss.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1437_',
                'SITE_URL' => 'http://www.allabout-weightloss.com',
                'DOMAIN' => 'allabout-weightloss.com',
            ),
            'livecricketsite.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1438_',
                'SITE_URL' => 'http://www.livecricketsite.com',
                'DOMAIN' => 'livecricketsite.com',
            ),
            'healthy-body-exercises.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1439_',
                'SITE_URL' => 'http://www.healthy-body-exercises.com',
                'DOMAIN' => 'healthy-body-exercises.com',
            ),
            'western-light.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1440_',
                'SITE_URL' => 'http://www.western-light.com',
                'DOMAIN' => 'western-light.com',
            ),
            'dli-generics.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_653_',
                'SITE_URL' => 'http://www.dli-generics.com',
                'DOMAIN' => 'dli-generics.com',
            ),
            'learningstylesuk.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_665_',
                'SITE_URL' => 'http://www.learningstylesuk.com',
                'DOMAIN' => 'learningstylesuk.com',
            ),
            'pittsburghalt.net' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_668_',
                'SITE_URL' => 'http://www.pittsburghalt.net',
                'DOMAIN' => 'pittsburghalt.net',
            ),
            'birthdaygift.us' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_670_',
                'SITE_URL' => 'http://www.birthdaygift.us',
                'DOMAIN' => 'birthdaygift.us',
            ),
            'plentyrestaurant.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_671_',
                'SITE_URL' => 'http://www.plentyrestaurant.com',
                'DOMAIN' => 'plentyrestaurant.com',
            ),
            'dotdotstudios.com' =>
            array(
                'TABLE_SCHEMA' => 'dotdotst_wp68',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://dotdotstudios.com',
                'DOMAIN' => 'dotdotstudios.com',
            ),
            'dretsemergents.net' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_1_83952_',
                'SITE_URL' => 'http://www.dretsemergents.net',
                'DOMAIN' => 'dretsemergents.net',
            ),
            'aftalaska.org' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_wp867',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://aftalaska.org',
                'DOMAIN' => 'aftalaska.org',
            ),
            'southsidetechprep.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_1_83954_',
                'SITE_URL' => 'http://www.southsidetechprep.com',
                'DOMAIN' => 'southsidetechprep.com',
            ),
            'bestofascoistanbul2011.org' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_1_83955_',
                'SITE_URL' => 'http://www.bestofascoistanbul2011.org',
                'DOMAIN' => 'bestofascoistanbul2011.org',
            ),
            'hotelreviewsinvancouver.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_1_83956_',
                'SITE_URL' => 'http://www.hotelreviewsinvancouver.com',
                'DOMAIN' => 'hotelreviewsinvancouver.com',
            ),
            'clementinescafe.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_717_',
                'SITE_URL' => 'http://www.clementinescafe.com',
                'DOMAIN' => 'clementinescafe.com',
            ),
            'adelekamp.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_718_',
                'SITE_URL' => 'http://www.adelekamp.com',
                'DOMAIN' => 'adelekamp.com',
            ),
            'emergency-response-systems.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_719_',
                'SITE_URL' => 'http://www.emergency-response-systems.com',
                'DOMAIN' => 'emergency-response-systems.com',
            ),
            'offshoreprimebrokerage.org' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_720_',
                'SITE_URL' => 'http://www.offshoreprimebrokerage.org',
                'DOMAIN' => 'offshoreprimebrokerage.org',
            ),
            'garyjonesbusinessenglish.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_721_',
                'SITE_URL' => 'http://www.garyjonesbusinessenglish.com',
                'DOMAIN' => 'garyjonesbusinessenglish.com',
            ),
            'ballpitplus.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1432_',
                'SITE_URL' => 'http://www.ballpitplus.com',
                'DOMAIN' => 'ballpitplus.com',
            ),
            'heroineaddict.us' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1433_',
                'SITE_URL' => 'http://www.heroineaddict.us',
                'DOMAIN' => 'heroineaddict.us',
            ),
            'knightbikesandskateboards.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1434_',
                'SITE_URL' => 'http://www.knightbikesandskateboards.com',
                'DOMAIN' => 'knightbikesandskateboards.com',
            ),
            'outdoor-lighting-for-sale.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1435_',
                'SITE_URL' => 'http://www.outdoor-lighting-for-sale.com',
                'DOMAIN' => 'outdoor-lighting-for-sale.com',
            ),
            'lawnprognv.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_652_',
                'SITE_URL' => 'http://www.lawnprognv.com',
                'DOMAIN' => 'lawnprognv.com',
            ),
            'emaxmarketing.net' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_654_',
                'SITE_URL' => 'http://www.emaxmarketing.net',
                'DOMAIN' => 'emaxmarketing.net',
            ),
            'nymatlaw.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_659_',
                'SITE_URL' => 'http://www.nymatlaw.com',
                'DOMAIN' => 'nymatlaw.com',
            ),
            'teaguefarms.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_667_',
                'SITE_URL' => 'http://www.teaguefarms.com',
                'DOMAIN' => 'teaguefarms.com',
            ),
            'yourmotorizedworld.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_676_',
                'SITE_URL' => 'http://www.yourmotorizedworld.com',
                'DOMAIN' => 'yourmotorizedworld.com',
            ),
            'jewishportal.info' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_wp155',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://jewishportal.info',
                'DOMAIN' => 'jewishportal.info',
            ),
            'bestwesterniridaresort.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_wp294',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://bestwesterniridaresort.com',
                'DOMAIN' => 'bestwesterniridaresort.com',
            ),
            'isyourteenatrisk.org' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_wp599',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://isyourteenatrisk.org',
                'DOMAIN' => 'isyourteenatrisk.org',
            ),
            'plazaofrichmond.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_wp791',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://plazaofrichmond.com',
                'DOMAIN' => 'plazaofrichmond.com',
            ),
            'findingjacob.com' =>
            array(
                'TABLE_SCHEMA' => 'dretseme_wp839',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://findingjacob.com',
                'DOMAIN' => 'findingjacob.com',
            ),
            'miraclemotorsports.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1307_',
                'SITE_URL' => 'http://www.miraclemotorsports.com',
                'DOMAIN' => 'miraclemotorsports.com',
            ),
            'morphisat.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1308_',
                'SITE_URL' => 'http://www.morphisat.com',
                'DOMAIN' => 'morphisat.com',
            ),
            'mypinkmotorcyclehelmets.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1309_',
                'SITE_URL' => 'http://www.mypinkmotorcyclehelmets.com',
                'DOMAIN' => 'mypinkmotorcyclehelmets.com',
            ),
            'neo-vitamin.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1310_',
                'SITE_URL' => 'http://www.neo-vitamin.com',
                'DOMAIN' => 'neo-vitamin.com',
            ),
            'duoyi.org' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_949_',
                'SITE_URL' => 'http://www.duoyi.org',
                'DOMAIN' => 'duoyi.org',
            ),
            'sosein-visualstory.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_951_',
                'SITE_URL' => 'http://www.sosein-visualstory.com',
                'DOMAIN' => 'sosein-visualstory.com',
            ),
            'rsu-ids2012.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_952_',
                'SITE_URL' => 'http://www.rsu-ids2012.com',
                'DOMAIN' => 'rsu-ids2012.com',
            ),
            'jhwyzs.com' =>
            array(
                'TABLE_SCHEMA' => 'duoyiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_953_',
                'SITE_URL' => 'http://www.jhwyzs.com',
                'DOMAIN' => 'jhwyzs.com',
            ),
            'educationsafetyconference.org' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_1_83957_',
                'SITE_URL' => 'http://www.educationsafetyconference.org',
                'DOMAIN' => 'educationsafetyconference.org',
            ),
            'bostickquailhunting.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_1_83958_',
                'SITE_URL' => 'http://www.bostickquailhunting.com',
                'DOMAIN' => 'bostickquailhunting.com',
            ),
            'nsjc.info' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_1_83959_',
                'SITE_URL' => 'http://www.nsjc.info',
                'DOMAIN' => 'nsjc.info',
            ),
            'bostickturkeyhunting.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_1_83960_',
                'SITE_URL' => 'http://www.bostickturkeyhunting.com',
                'DOMAIN' => 'bostickturkeyhunting.com',
            ),
            'bostickdeerhunting.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_1_83961_',
                'SITE_URL' => 'http://www.bostickdeerhunting.com',
                'DOMAIN' => 'bostickdeerhunting.com',
            ),
            'yeast-infection-facts.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1428_',
                'SITE_URL' => 'http://www.yeast-infection-facts.com',
                'DOMAIN' => 'yeast-infection-facts.com',
            ),
            'yogamart.net' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1429_',
                'SITE_URL' => 'http://www.yogamart.net',
                'DOMAIN' => 'yogamart.net',
            ),
            'beautysalony.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1430_',
                'SITE_URL' => 'http://www.beautysalony.com',
                'DOMAIN' => 'beautysalony.com',
            ),
            'lowridernews.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_651_',
                'SITE_URL' => 'http://www.lowridernews.com',
                'DOMAIN' => 'lowridernews.com',
            ),
            'fitforherswokc.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_662_',
                'SITE_URL' => 'http://www.fitforherswokc.com',
                'DOMAIN' => 'fitforherswokc.com',
            ),
            'hacksim.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_663_',
                'SITE_URL' => 'http://www.hacksim.com',
                'DOMAIN' => 'hacksim.com',
            ),
            'longestwalk2011.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_664_',
                'SITE_URL' => 'http://www.longestwalk2011.com',
                'DOMAIN' => 'longestwalk2011.com',
            ),
            'engineblox.com' =>
            array(
                'TABLE_SCHEMA' => 'educatio_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_674_',
                'SITE_URL' => 'http://www.engineblox.com',
                'DOMAIN' => 'engineblox.com',
            ),
            'eshosting.info' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_1_84347_',
                'SITE_URL' => 'http://www.eshosting.info',
                'DOMAIN' => 'eshosting.info',
            ),
            'guasas.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_1_84348_',
                'SITE_URL' => 'http://www.guasas.com',
                'DOMAIN' => 'guasas.com',
            ),
            'improvementle.us' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_1_84349_',
                'SITE_URL' => 'http://www.improvementle.us',
                'DOMAIN' => 'improvementle.us',
            ),
            'jausaholdings.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_1_84350_',
                'SITE_URL' => 'http://www.jausaholdings.com',
                'DOMAIN' => 'jausaholdings.com',
            ),
            'landmarkpropsales.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_1_84351_',
                'SITE_URL' => 'http://www.landmarkpropsales.com',
                'DOMAIN' => 'landmarkpropsales.com',
            ),
            'simplicitysake.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1416_',
                'SITE_URL' => 'http://www.simplicitysake.com',
                'DOMAIN' => 'simplicitysake.com',
            ),
            'theswornin.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1417_',
                'SITE_URL' => 'http://www.theswornin.com',
                'DOMAIN' => 'theswornin.com',
            ),
            'golftipsweb.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_574_',
                'SITE_URL' => 'http://golftipsweb.com',
                'DOMAIN' => 'golftipsweb.com',
            ),
            'flatsinmelbourne.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_629_',
                'SITE_URL' => 'http://www.flatsinmelbourne.com',
                'DOMAIN' => 'flatsinmelbourne.com',
            ),
            'azurerealestateagents.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_639_',
                'SITE_URL' => 'http://www.azurerealestateagents.com',
                'DOMAIN' => 'azurerealestateagents.com',
            ),
            'baltimore-injury-lawyer-attorney-law-firm.com' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_640_',
                'SITE_URL' => 'http://www.baltimore-injury-lawyer-attorney-law-firm.com',
                'DOMAIN' => 'baltimore-injury-lawyer-attorney-law-firm.com',
            ),
            'alltherightwords.biz' =>
            array(
                'TABLE_SCHEMA' => 'eshostin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_645_',
                'SITE_URL' => 'http://www.alltherightwords.biz',
                'DOMAIN' => 'alltherightwords.biz',
            ),
            'extraneouskickassery.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_1_83210_',
                'SITE_URL' => 'http://www.extraneouskickassery.com',
                'DOMAIN' => 'extraneouskickassery.com',
            ),
            'extremefuturistfest.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_1_83211_',
                'SITE_URL' => 'http://www.extremefuturistfest.com',
                'DOMAIN' => 'extremefuturistfest.com',
            ),
            'fauxchenaux.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_wp789',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.fauxchenaux.com',
                'DOMAIN' => 'fauxchenaux.com',
            ),
            'filmnavigator.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_1_83213_',
                'SITE_URL' => 'http://www.filmnavigator.com',
                'DOMAIN' => 'filmnavigator.com',
            ),
            'finland80dagar.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_1_83214_',
                'SITE_URL' => 'http://www.finland80dagar.com',
                'DOMAIN' => 'finland80dagar.com',
            ),
            'somanabolicmusclemaximizerreviewz.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_wp289',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://somanabolicmusclemaximizerreviewz.com',
                'DOMAIN' => 'somanabolicmusclemaximizerreviewz.com',
            ),
            'acsbrightspotscampaign.org' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_wp411',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://acsbrightspotscampaign.org',
                'DOMAIN' => 'acsbrightspotscampaign.org',
            ),
            'presidiotravelodge.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_wp1',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://presidiotravelodge.com',
                'DOMAIN' => 'presidiotravelodge.com',
            ),
            'ieaaa.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_wp52',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ieaaa.com',
                'DOMAIN' => 'ieaaa.com',
            ),
            'hunqing.biz' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_wp450',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://hunqing.biz',
                'DOMAIN' => 'hunqing.biz',
            ),
            'boatingandsailingaccessories.net' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1411_',
                'SITE_URL' => 'http://www.boatingandsailingaccessories.net',
                'DOMAIN' => 'boatingandsailingaccessories.net',
            ),
            'didzisdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1412_',
                'SITE_URL' => 'http://www.didzisdesign.com',
                'DOMAIN' => 'didzisdesign.com',
            ),
            'atlantisstudiosripoff.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1413_',
                'SITE_URL' => 'http://www.atlantisstudiosripoff.com',
                'DOMAIN' => 'atlantisstudiosripoff.com',
            ),
            'bjstudent.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1414_',
                'SITE_URL' => 'http://www.bjstudent.com',
                'DOMAIN' => 'bjstudent.com',
            ),
            'azzeitouna.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1415_',
                'SITE_URL' => 'http://www.azzeitouna.com',
                'DOMAIN' => 'azzeitouna.com',
            ),
            'world-series-of-poker-1996.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_608_',
                'SITE_URL' => 'http://www.world-series-of-poker-1996.com',
                'DOMAIN' => 'world-series-of-poker-1996.com',
            ),
            'followmenyc.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_615_',
                'SITE_URL' => 'http://www.followmenyc.com',
                'DOMAIN' => 'followmenyc.com',
            ),
            'nyinternationalantiquesshow.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_630_',
                'SITE_URL' => 'http://www.nyinternationalantiquesshow.com',
                'DOMAIN' => 'nyinternationalantiquesshow.com',
            ),
            'capexperience.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_631_',
                'SITE_URL' => 'http://www.capexperience.com',
                'DOMAIN' => 'capexperience.com',
            ),
            'tarheel-webdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'extraneo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_641_',
                'SITE_URL' => 'http://www.tarheel-webdesign.com',
                'DOMAIN' => 'tarheel-webdesign.com',
            ),
            'fattireclassic.org' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_1_81125_',
                'SITE_URL' => 'http://www.fattireclassic.org',
                'DOMAIN' => 'fattireclassic.org',
            ),
            'fazendodiscipulos.us' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_1_81158_',
                'SITE_URL' => 'http://www.fazendodiscipulos.us',
                'DOMAIN' => 'fazendodiscipulos.us',
            ),
            'feldmanadvertising.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_1_81159_',
                'SITE_URL' => 'http://www.feldmanadvertising.com',
                'DOMAIN' => 'feldmanadvertising.com',
            ),
            'financeclubillinois.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_1_81160_',
                'SITE_URL' => 'http://www.financeclubillinois.com',
                'DOMAIN' => 'financeclubillinois.com',
            ),
            'finnishingtouch.net' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_1_81161_',
                'SITE_URL' => 'http://www.finnishingtouch.net',
                'DOMAIN' => 'finnishingtouch.net',
            ),
            'sellvoip.net' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1402_',
                'SITE_URL' => 'http://www.sellvoip.net',
                'DOMAIN' => 'sellvoip.net',
            ),
            'wealth-online.net' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1404_',
                'SITE_URL' => 'http://www.wealth-online.net',
                'DOMAIN' => 'wealth-online.net',
            ),
            'goodautotransport.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_614_',
                'SITE_URL' => 'http://www.goodautotransport.com',
                'DOMAIN' => 'goodautotransport.com',
            ),
            'susifineart.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_623_',
                'SITE_URL' => 'http://www.susifineart.com',
                'DOMAIN' => 'susifineart.com',
            ),
            'golf-bluesky.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_636_',
                'SITE_URL' => 'http://www.golf-bluesky.com',
                'DOMAIN' => 'golf-bluesky.com',
            ),
            'shopearthessentials.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_637_',
                'SITE_URL' => 'http://www.shopearthessentials.com',
                'DOMAIN' => 'shopearthessentials.com',
            ),
            'sunwisesolarus.com' =>
            array(
                'TABLE_SCHEMA' => 'fattirec_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_644_',
                'SITE_URL' => 'http://www.sunwisesolarus.com',
                'DOMAIN' => 'sunwisesolarus.com',
            ),
            'fcfsinc.com' =>
            array(
                'TABLE_SCHEMA' => 'fcfsinc_tkp',
                'TABLE_PREFIX' => 'wp_1_82961_',
                'SITE_URL' => 'http://www.fcfsinc.com',
                'DOMAIN' => 'fcfsinc.com',
            ),
            'forestconflict.com' =>
            array(
                'TABLE_SCHEMA' => 'fcfsinc_tkp',
                'TABLE_PREFIX' => 'wp_1_82962_',
                'SITE_URL' => 'http://www.forestconflict.com',
                'DOMAIN' => 'forestconflict.com',
            ),
            'gomonza.com' =>
            array(
                'TABLE_SCHEMA' => 'fcfsinc_tkp',
                'TABLE_PREFIX' => 'wp_1_82963_',
                'SITE_URL' => 'http://www.gomonza.com',
                'DOMAIN' => 'gomonza.com',
            ),
            'hprivreinvestment.org' =>
            array(
                'TABLE_SCHEMA' => 'fcfsinc_tkp',
                'TABLE_PREFIX' => 'wp_1_82964_',
                'SITE_URL' => 'http://www.hprivreinvestment.org',
                'DOMAIN' => 'hprivreinvestment.org',
            ),
            'irelandlinecruisers.com' =>
            array(
                'TABLE_SCHEMA' => 'fcfsinc_tkp',
                'TABLE_PREFIX' => 'wp_1_82965_',
                'SITE_URL' => 'http://www.irelandlinecruisers.com',
                'DOMAIN' => 'irelandlinecruisers.com',
            ),
            'russ-t.com' =>
            array(
                'TABLE_SCHEMA' => 'fcfsinc_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_130_',
                'SITE_URL' => 'http://www.russ-t.com',
                'DOMAIN' => 'russ-t.com',
            ),
            'saleuggshop.us' =>
            array(
                'TABLE_SCHEMA' => 'fcfsinc_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_131_',
                'SITE_URL' => 'http://www.saleuggshop.us',
                'DOMAIN' => 'saleuggshop.us',
            ),
            'mail.shadowsurfing.com' =>
            array(
                'TABLE_SCHEMA' => 'fcfsinc_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_132_',
                'SITE_URL' => 'http://mail.shadowsurfing.com',
                'DOMAIN' => 'mail.shadowsurfing.com',
            ),
            'silopicudifm.com' =>
            array(
                'TABLE_SCHEMA' => 'fcfsinc_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_133_',
                'SITE_URL' => 'http://www.silopicudifm.com',
                'DOMAIN' => 'silopicudifm.com',
            ),
            'ficciframes.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_1_84449_',
                'SITE_URL' => 'http://www.ficciframes.com',
                'DOMAIN' => 'ficciframes.com',
            ),
            'groupmasterysupport.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_1_84450_',
                'SITE_URL' => 'http://www.groupmasterysupport.com',
                'DOMAIN' => 'groupmasterysupport.com',
            ),
            'grsdh1.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_1_84457_',
                'SITE_URL' => 'http://www.grsdh1.com',
                'DOMAIN' => 'grsdh1.com',
            ),
            'hngsc.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_1_84458_',
                'SITE_URL' => 'http://www.hngsc.com',
                'DOMAIN' => 'hngsc.com',
            ),
            'lemonadewedding.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_1_84459_',
                'SITE_URL' => 'http://www.lemonadewedding.com',
                'DOMAIN' => 'lemonadewedding.com',
            ),
            'xn-----uldfbofebqo6f7eta.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_wp336',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://xn-----uldfbofebqo6f7eta.com',
                'DOMAIN' => 'xn-----uldfbofebqo6f7eta.com',
            ),
            'west-houston-dentist.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1397_',
                'SITE_URL' => 'http://www.west-houston-dentist.com',
                'DOMAIN' => 'west-houston-dentist.com',
            ),
            'hopohosting.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1398_',
                'SITE_URL' => 'http://www.hopohosting.com',
                'DOMAIN' => 'hopohosting.com',
            ),
            'labsconf.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1399_',
                'SITE_URL' => 'http://www.labsconf.com',
                'DOMAIN' => 'labsconf.com',
            ),
            'beatrizargimon.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1400_',
                'SITE_URL' => 'http://www.beatrizargimon.com',
                'DOMAIN' => 'beatrizargimon.com',
            ),
            'outlaw-performance.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1401_',
                'SITE_URL' => 'http://www.outlaw-performance.com',
                'DOMAIN' => 'outlaw-performance.com',
            ),
            'imagereel.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_601_',
                'SITE_URL' => 'http://www.imagereel.com',
                'DOMAIN' => 'imagereel.com',
            ),
            'home-drugtesting.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_612_',
                'SITE_URL' => 'http://www.home-drugtesting.com',
                'DOMAIN' => 'home-drugtesting.com',
            ),
            'motormapusa.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_621_',
                'SITE_URL' => 'http://www.motormapusa.com',
                'DOMAIN' => 'motormapusa.com',
            ),
            'gabaroundtable.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_634_',
                'SITE_URL' => 'http://www.gabaroundtable.com',
                'DOMAIN' => 'gabaroundtable.com',
            ),
            'iigonline.org' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_635_',
                'SITE_URL' => 'http://www.iigonline.org',
                'DOMAIN' => 'iigonline.org',
            ),
            'lakecountryfarmersmarket.org' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_wp533',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://lakecountryfarmersmarket.org',
                'DOMAIN' => 'lakecountryfarmersmarket.org',
            ),
            'trinityballny.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_wp598',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://trinityballny.com',
                'DOMAIN' => 'trinityballny.com',
            ),
            'alltempeng.com' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_wp886',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://alltempeng.com',
                'DOMAIN' => 'alltempeng.com',
            ),
            'vshs.org' =>
            array(
                'TABLE_SCHEMA' => 'ficcifra_wp927',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://vshs.org',
                'DOMAIN' => 'vshs.org',
            ),
            'fivezorz.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_1_2239_',
                'SITE_URL' => 'http://www.fivezorz.com',
                'DOMAIN' => 'fivezorz.com',
            ),
            'fjkrobturns100.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_1_2240_',
                'SITE_URL' => 'http://www.fjkrobturns100.com',
                'DOMAIN' => 'fjkrobturns100.com',
            ),
            'focusdetective.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_1_2241_',
                'SITE_URL' => 'http://www.focusdetective.com',
                'DOMAIN' => 'focusdetective.com',
            ),
            'fogwe.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_1_2242_',
                'SITE_URL' => 'http://www.fogwe.com',
                'DOMAIN' => 'fogwe.com',
            ),
            'fotografoescort.info' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_1_2243_',
                'SITE_URL' => 'http://www.fotografoescort.info',
                'DOMAIN' => 'fotografoescort.info',
            ),
            'thenicestrealtors.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1391_',
                'SITE_URL' => 'http://www.thenicestrealtors.com',
                'DOMAIN' => 'thenicestrealtors.com',
            ),
            'washington-dc-injury-lawyer-attorney-law-firm.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1393_',
                'SITE_URL' => 'http://www.washington-dc-injury-lawyer-attorney-law-firm.com',
                'DOMAIN' => 'washington-dc-injury-lawyer-attorney-law-firm.com',
            ),
            'weddingsitesweb.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1394_',
                'SITE_URL' => 'http://www.weddingsitesweb.com',
                'DOMAIN' => 'weddingsitesweb.com',
            ),
            'uk-ghdscheap.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1395_',
                'SITE_URL' => 'http://www.uk-ghdscheap.com',
                'DOMAIN' => 'uk-ghdscheap.com',
            ),
            'thehomeopathicdoctor.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1396_',
                'SITE_URL' => 'http://www.thehomeopathicdoctor.com',
                'DOMAIN' => 'thehomeopathicdoctor.com',
            ),
            'congosoccer.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_610_',
                'SITE_URL' => 'http://www.congosoccer.com',
                'DOMAIN' => 'congosoccer.com',
            ),
            'master-handyman.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_619_',
                'SITE_URL' => 'http://www.master-handyman.com',
                'DOMAIN' => 'master-handyman.com',
            ),
            'banishedthefilm.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_632_',
                'SITE_URL' => 'http://www.banishedthefilm.com',
                'DOMAIN' => 'banishedthefilm.com',
            ),
            'leap-and-shedding.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_633_',
                'SITE_URL' => 'http://www.leap-and-shedding.com',
                'DOMAIN' => 'leap-and-shedding.com',
            ),
            'englishtownfire.com' =>
            array(
                'TABLE_SCHEMA' => 'fivezorz_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_643_',
                'SITE_URL' => 'http://www.englishtownfire.com',
                'DOMAIN' => 'englishtownfire.com',
            ),
            'flightstouk.us' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_1_2605_',
                'SITE_URL' => 'http://www.flightstouk.us',
                'DOMAIN' => 'flightstouk.us',
            ),
            'freedomsflagship.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_1_2606_',
                'SITE_URL' => 'http://www.freedomsflagship.com',
                'DOMAIN' => 'freedomsflagship.com',
            ),
            'gi77op.org' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_1_2607_',
                'SITE_URL' => 'http://www.gi77op.org',
                'DOMAIN' => 'gi77op.org',
            ),
            'gkmalumni.org' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_1_2608_',
                'SITE_URL' => 'http://www.gkmalumni.org',
                'DOMAIN' => 'gkmalumni.org',
            ),
            'gnado.org' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_1_2609_',
                'SITE_URL' => 'http://www.gnado.org',
                'DOMAIN' => 'gnado.org',
            ),
            'leoceballosrealty.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1386_',
                'SITE_URL' => 'http://www.leoceballosrealty.com',
                'DOMAIN' => 'leoceballosrealty.com',
            ),
            'lilivideos.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1387_',
                'SITE_URL' => 'http://www.lilivideos.com',
                'DOMAIN' => 'lilivideos.com',
            ),
            'studio3bphotography.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1388_',
                'SITE_URL' => 'http://www.studio3bphotography.com',
                'DOMAIN' => 'studio3bphotography.com',
            ),
            'talkweed.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1389_',
                'SITE_URL' => 'http://www.talkweed.com',
                'DOMAIN' => 'talkweed.com',
            ),
            'thenaturalwellness.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1390_',
                'SITE_URL' => 'http://www.thenaturalwellness.com',
                'DOMAIN' => 'thenaturalwellness.com',
            ),
            'emoticonstudio.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_598_',
                'SITE_URL' => 'http://www.emoticonstudio.com',
                'DOMAIN' => 'emoticonstudio.com',
            ),
            'coachbagsstore.info' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_606_',
                'SITE_URL' => 'http://www.coachbagsstore.info',
                'DOMAIN' => 'coachbagsstore.info',
            ),
            'sbvsa.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_622_',
                'SITE_URL' => 'http://www.sbvsa.com',
                'DOMAIN' => 'sbvsa.com',
            ),
            'sarawear.com' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_627_',
                'SITE_URL' => 'http://www.sarawear.com',
                'DOMAIN' => 'sarawear.com',
            ),
            'southwestfutsalchampionship.info' =>
            array(
                'TABLE_SCHEMA' => 'flights2_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_628_',
                'SITE_URL' => 'http://www.southwestfutsalchampionship.info',
                'DOMAIN' => 'southwestfutsalchampionship.info',
            ),
            'forexinst.net' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_1_82538_',
                'SITE_URL' => 'http://www.forexinst.net',
                'DOMAIN' => 'forexinst.net',
            ),
            'descubriendoelgris.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_1_82540_',
                'SITE_URL' => 'http://www.descubriendoelgris.com',
                'DOMAIN' => 'descubriendoelgris.com',
            ),
            'tiger8888.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_1_82541_',
                'SITE_URL' => 'http://www.tiger8888.com',
                'DOMAIN' => 'tiger8888.com',
            ),
            'vipsstores.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_1_82542_',
                'SITE_URL' => 'http://www.vipsstores.com',
                'DOMAIN' => 'vipsstores.com',
            ),
            'proactivesportsmassage.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_572_',
                'SITE_URL' => 'http://www.proactivesportsmassage.com',
                'DOMAIN' => 'proactivesportsmassage.com',
            ),
            'tornadostock.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_597_',
                'SITE_URL' => 'http://www.tornadostock.com',
                'DOMAIN' => 'tornadostock.com',
            ),
            'carrollogos.com' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_604_',
                'SITE_URL' => 'http://www.carrollogos.com',
                'DOMAIN' => 'carrollogos.com',
            ),
            'libertyunchained.us' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_607_',
                'SITE_URL' => 'http://www.libertyunchained.us',
                'DOMAIN' => 'libertyunchained.us',
            ),
            'lostinthecrowd.org' =>
            array(
                'TABLE_SCHEMA' => 'forexins_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_616_',
                'SITE_URL' => 'http://www.lostinthecrowd.org',
                'DOMAIN' => 'lostinthecrowd.org',
            ),
            'compatibili.me' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1161_',
                'SITE_URL' => 'http://www.compatibili.me',
                'DOMAIN' => 'compatibili.me',
            ),
            'chatyourmouth.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1185_',
                'SITE_URL' => 'http://www.chatyourmouth.com',
                'DOMAIN' => 'chatyourmouth.com',
            ),
            'clbgrls.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1186_',
                'SITE_URL' => 'http://www.clbgrls.com',
                'DOMAIN' => 'clbgrls.com',
            ),
            'companerismoensuamor.org' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1187_',
                'SITE_URL' => 'http://www.companerismoensuamor.org',
                'DOMAIN' => 'companerismoensuamor.org',
            ),
            'francinelemay.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_885_',
                'SITE_URL' => 'http://www.francinelemay.com',
                'DOMAIN' => 'francinelemay.com',
            ),
            'standinghealthacupuncture.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_886_',
                'SITE_URL' => 'http://www.standinghealthacupuncture.com',
                'DOMAIN' => 'standinghealthacupuncture.com',
            ),
            'makesomethingtoday.me' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_887_',
                'SITE_URL' => 'http://www.makesomethingtoday.me',
                'DOMAIN' => 'makesomethingtoday.me',
            ),
            'reconinspectionsllc.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_888_',
                'SITE_URL' => 'http://www.reconinspectionsllc.com',
                'DOMAIN' => 'reconinspectionsllc.com',
            ),
            'amayavillazan.com' =>
            array(
                'TABLE_SCHEMA' => 'francine_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_889_',
                'SITE_URL' => 'http://www.amayavillazan.com',
                'DOMAIN' => 'amayavillazan.com',
            ),
            'fw5k.org' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_1_81331_',
                'SITE_URL' => 'http://www.fw5k.org',
                'DOMAIN' => 'fw5k.org',
            ),
            'futurebuilders.us' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_1_81569_',
                'SITE_URL' => 'http://www.futurebuilders.us',
                'DOMAIN' => 'futurebuilders.us',
            ),
            'freejavascript.net' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_1_81621_',
                'SITE_URL' => 'http://www.freejavascript.net',
                'DOMAIN' => 'freejavascript.net',
            ),
            'freelancekelvinchng.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_1_81622_',
                'SITE_URL' => 'http://www.freelancekelvinchng.com',
                'DOMAIN' => 'freelancekelvinchng.com',
            ),
            'gameship.org' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_1_81623_',
                'SITE_URL' => 'http://www.gameship.org',
                'DOMAIN' => 'gameship.org',
            ),
            '1-credit-card-debt.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1374_',
                'SITE_URL' => 'http://www.1-credit-card-debt.com',
                'DOMAIN' => '1-credit-card-debt.com',
            ),
            'carrollogos.org' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1375_',
                'SITE_URL' => 'http://www.carrollogos.org',
                'DOMAIN' => 'carrollogos.org',
            ),
            'cleanupalbanynow.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1376_',
                'SITE_URL' => 'http://www.cleanupalbanynow.com',
                'DOMAIN' => 'cleanupalbanynow.com',
            ),
            'sanddollarsports.net' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_581_',
                'SITE_URL' => 'http://www.sanddollarsports.net',
                'DOMAIN' => 'sanddollarsports.net',
            ),
            'proposal-puzzles.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_591_',
                'SITE_URL' => 'http://www.proposal-puzzles.com',
                'DOMAIN' => 'proposal-puzzles.com',
            ),
            'telescopesonlineguide.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_593_',
                'SITE_URL' => 'http://www.telescopesonlineguide.com',
                'DOMAIN' => 'telescopesonlineguide.com',
            ),
            'thealibiinn.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_600_',
                'SITE_URL' => 'http://www.thealibiinn.com',
                'DOMAIN' => 'thealibiinn.com',
            ),
            'top100cosmetic.com' =>
            array(
                'TABLE_SCHEMA' => 'freejava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_613_',
                'SITE_URL' => 'http://www.top100cosmetic.com',
                'DOMAIN' => 'top100cosmetic.com',
            ),
            'from4to5.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_1_82668_',
                'SITE_URL' => 'http://www.from4to5.com',
                'DOMAIN' => 'from4to5.com',
            ),
            'hzppt.net' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_1_82669_',
                'SITE_URL' => 'http://www.hzppt.net',
                'DOMAIN' => 'hzppt.net',
            ),
            'kapisanan.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_1_82670_',
                'SITE_URL' => 'http://www.kapisanan.com',
                'DOMAIN' => 'kapisanan.com',
            ),
            'kategross.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_1_82671_',
                'SITE_URL' => 'http://www.kategross.com',
                'DOMAIN' => 'kategross.com',
            ),
            'killedcartoons.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_1_82672_',
                'SITE_URL' => 'http://www.killedcartoons.com',
                'DOMAIN' => 'killedcartoons.com',
            ),
            'angelsofsoccer.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1369_',
                'SITE_URL' => 'http://www.angelsofsoccer.com',
                'DOMAIN' => 'angelsofsoccer.com',
            ),
            'r-wildhorseranch.info' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1370_',
                'SITE_URL' => 'http://www.r-wildhorseranch.info',
                'DOMAIN' => 'r-wildhorseranch.info',
            ),
            'nyfbags.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1371_',
                'SITE_URL' => 'http://www.nyfbags.com',
                'DOMAIN' => 'nyfbags.com',
            ),
            'rc-sports.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1372_',
                'SITE_URL' => 'http://www.rc-sports.com',
                'DOMAIN' => 'rc-sports.com',
            ),
            'bulk-sms-business.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1373_',
                'SITE_URL' => 'http://www.bulk-sms-business.com',
                'DOMAIN' => 'bulk-sms-business.com',
            ),
            'constructoracorwell.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_578_',
                'SITE_URL' => 'http://www.constructoracorwell.com',
                'DOMAIN' => 'constructoracorwell.com',
            ),
            'debteliminationspro.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_579_',
                'SITE_URL' => 'http://www.debteliminationspro.com',
                'DOMAIN' => 'debteliminationspro.com',
            ),
            'kresztesztek.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_586_',
                'SITE_URL' => 'http://www.kresztesztek.com',
                'DOMAIN' => 'kresztesztek.com',
            ),
            'shopplayandlearn.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_588_',
                'SITE_URL' => 'http://www.shopplayandlearn.com',
                'DOMAIN' => 'shopplayandlearn.com',
            ),
            'realestatemasterymembership.com' =>
            array(
                'TABLE_SCHEMA' => 'from4to5_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_590_',
                'SITE_URL' => 'http://www.realestatemasterymembership.com',
                'DOMAIN' => 'realestatemasterymembership.com',
            ),
            'gadzooksdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_1_81165_',
                'SITE_URL' => 'http://www.gadzooksdesign.com',
                'DOMAIN' => 'gadzooksdesign.com',
            ),
            'gangbangsquadx.info' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_1_81166_',
                'SITE_URL' => 'http://www.gangbangsquadx.info',
                'DOMAIN' => 'gangbangsquadx.info',
            ),
            'gekiji.info' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_1_81167_',
                'SITE_URL' => 'http://www.gekiji.info',
                'DOMAIN' => 'gekiji.info',
            ),
            'geoffreyricketts.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_1_81168_',
                'SITE_URL' => 'http://www.geoffreyricketts.com',
                'DOMAIN' => 'geoffreyricketts.com',
            ),
            'giftitemsetc.net' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_1_81169_',
                'SITE_URL' => 'http://www.giftitemsetc.net',
                'DOMAIN' => 'giftitemsetc.net',
            ),
            'aldanasheatair.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1359_',
                'SITE_URL' => 'http://www.aldanasheatair.com',
                'DOMAIN' => 'aldanasheatair.com',
            ),
            'bfcoopermusic.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1362_',
                'SITE_URL' => 'http://www.bfcoopermusic.com',
                'DOMAIN' => 'bfcoopermusic.com',
            ),
            'clickbankcashmall.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1363_',
                'SITE_URL' => 'http://www.clickbankcashmall.com',
                'DOMAIN' => 'clickbankcashmall.com',
            ),
            'steveschalchlin.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_580_',
                'SITE_URL' => 'http://www.steveschalchlin.com',
                'DOMAIN' => 'steveschalchlin.com',
            ),
            'haus-kaufen24.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_589_',
                'SITE_URL' => 'http://www.haus-kaufen24.com',
                'DOMAIN' => 'haus-kaufen24.com',
            ),
            'idhouston.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_592_',
                'SITE_URL' => 'http://www.idhouston.com',
                'DOMAIN' => 'idhouston.com',
            ),
            'freedavidthorne.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_594_',
                'SITE_URL' => 'http://www.freedavidthorne.com',
                'DOMAIN' => 'freedavidthorne.com',
            ),
            'anywhereflower.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_609_',
                'SITE_URL' => 'http://www.anywhereflower.com',
                'DOMAIN' => 'anywhereflower.com',
            ),
            'rocksolidbackgroundscreening.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_wp278',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://rocksolidbackgroundscreening.com',
                'DOMAIN' => 'rocksolidbackgroundscreening.com',
            ),
            'csfirefestival.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_wp370',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://csfirefestival.com',
                'DOMAIN' => 'csfirefestival.com',
            ),
            'marvspirational.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_wp620',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://marvspirational.com',
                'DOMAIN' => 'marvspirational.com',
            ),
            'hosteriadeanita.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_wp69',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://hosteriadeanita.com',
                'DOMAIN' => 'hosteriadeanita.com',
            ),
            '365tustin.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_wp779',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://365tustin.com',
                'DOMAIN' => '365tustin.com',
            ),
            'nickismyhomeboy.com' =>
            array(
                'TABLE_SCHEMA' => 'gadzooks_wp867',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://nickismyhomeboy.com',
                'DOMAIN' => 'nickismyhomeboy.com',
            ),
            'gayfuckingblog.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_1_2254_',
                'SITE_URL' => 'http://www.gayfuckingblog.com',
                'DOMAIN' => 'gayfuckingblog.com',
            ),
            'txqw.net' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1063_',
                'SITE_URL' => 'http://www.txqw.net',
                'DOMAIN' => 'txqw.net',
            ),
            'generadordecodigo.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_1_2256_',
                'SITE_URL' => 'http://www.generadordecodigo.com',
                'DOMAIN' => 'generadordecodigo.com',
            ),
            'getminecraft.net' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_1_2257_',
                'SITE_URL' => 'http://www.getminecraft.net',
                'DOMAIN' => 'getminecraft.net',
            ),
            'gihanidost.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_1_2258_',
                'SITE_URL' => 'http://www.gihanidost.com',
                'DOMAIN' => 'gihanidost.com',
            ),
            'gamblingthrills.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1364_',
                'SITE_URL' => 'http://www.gamblingthrills.com',
                'DOMAIN' => 'gamblingthrills.com',
            ),
            'everykindofinsurance.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1365_',
                'SITE_URL' => 'http://www.everykindofinsurance.com',
                'DOMAIN' => 'everykindofinsurance.com',
            ),
            'trussvillesoccer.org' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1366_',
                'SITE_URL' => 'http://www.trussvillesoccer.org',
                'DOMAIN' => 'trussvillesoccer.org',
            ),
            'garagedoctorz.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1367_',
                'SITE_URL' => 'http://www.garagedoctorz.com',
                'DOMAIN' => 'garagedoctorz.com',
            ),
            '800wg.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1368_',
                'SITE_URL' => 'http://www.800wg.com',
                'DOMAIN' => '800wg.com',
            ),
            'oneactdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_584_',
                'SITE_URL' => 'http://www.oneactdesign.com',
                'DOMAIN' => 'oneactdesign.com',
            ),
            'denmadesigns.us' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_585_',
                'SITE_URL' => 'http://www.denmadesigns.us',
                'DOMAIN' => 'denmadesigns.us',
            ),
            'lifestyle-enhancement.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_595_',
                'SITE_URL' => 'http://www.lifestyle-enhancement.com',
                'DOMAIN' => 'lifestyle-enhancement.com',
            ),
            'hirkan.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_596_',
                'SITE_URL' => 'http://www.hirkan.com',
                'DOMAIN' => 'hirkan.com',
            ),
            'beachvacation-sandiego.com' =>
            array(
                'TABLE_SCHEMA' => 'gayfucki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_603_',
                'SITE_URL' => 'http://www.beachvacation-sandiego.com',
                'DOMAIN' => 'beachvacation-sandiego.com',
            ),
            'gemladies-dc.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_1_81620_',
                'SITE_URL' => 'http://www.gemladies-dc.com',
                'DOMAIN' => 'gemladies-dc.com',
            ),
            'gemladies-michigan.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_1_81624_',
                'SITE_URL' => 'http://www.gemladies-michigan.com',
                'DOMAIN' => 'gemladies-michigan.com',
            ),
            'gencsoylem.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_1_81625_',
                'SITE_URL' => 'http://www.gencsoylem.com',
                'DOMAIN' => 'gencsoylem.com',
            ),
            'geniusquotes.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_1_81626_',
                'SITE_URL' => 'http://www.geniusquotes.com',
                'DOMAIN' => 'geniusquotes.com',
            ),
            'gisbornehome.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_1_81627_',
                'SITE_URL' => 'http://www.gisbornehome.com',
                'DOMAIN' => 'gisbornehome.com',
            ),
            'nomoredeductible.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_545_',
                'SITE_URL' => 'http://www.nomoredeductible.com',
                'DOMAIN' => 'nomoredeductible.com',
            ),
            'prepnewsreport.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_554_',
                'SITE_URL' => 'http://www.prepnewsreport.com',
                'DOMAIN' => 'prepnewsreport.com',
            ),
            'morristowns.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_557_',
                'SITE_URL' => 'http://www.morristowns.com',
                'DOMAIN' => 'morristowns.com',
            ),
            'pqcarpets.com' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_558_',
                'SITE_URL' => 'http://www.pqcarpets.com',
                'DOMAIN' => 'pqcarpets.com',
            ),
            'mi-ann.org' =>
            array(
                'TABLE_SCHEMA' => 'gemladie_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_565_',
                'SITE_URL' => 'http://www.mi-ann.org',
                'DOMAIN' => 'mi-ann.org',
            ),
            'getkronk.info' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_1_83533_',
                'SITE_URL' => 'http://www.getkronk.info',
                'DOMAIN' => 'getkronk.info',
            ),
            'greenconceptslandcare.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_1_83534_',
                'SITE_URL' => 'http://www.greenconceptslandcare.com',
                'DOMAIN' => 'greenconceptslandcare.com',
            ),
            'grupa-rea-zg.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_1_83535_',
                'SITE_URL' => 'http://www.grupa-rea-zg.com',
                'DOMAIN' => 'grupa-rea-zg.com',
            ),
            'id-uri-fete.info' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_1_83536_',
                'SITE_URL' => 'http://www.id-uri-fete.info',
                'DOMAIN' => 'id-uri-fete.info',
            ),
            'il-logico.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_1_83537_',
                'SITE_URL' => 'http://www.il-logico.com',
                'DOMAIN' => 'il-logico.com',
            ),
            'personalfinancedirectory.info' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1354_',
                'SITE_URL' => 'http://www.personalfinancedirectory.info',
                'DOMAIN' => 'personalfinancedirectory.info',
            ),
            'netshottest.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1355_',
                'SITE_URL' => 'http://www.netshottest.com',
                'DOMAIN' => 'netshottest.com',
            ),
            'inetprogramming.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1356_',
                'SITE_URL' => 'http://www.inetprogramming.com',
                'DOMAIN' => 'inetprogramming.com',
            ),
            'casinoslosangeles.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1357_',
                'SITE_URL' => 'http://www.casinoslosangeles.com',
                'DOMAIN' => 'casinoslosangeles.com',
            ),
            'dy-law.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1358_',
                'SITE_URL' => 'http://www.dy-law.com',
                'DOMAIN' => 'dy-law.com',
            ),
            'cinemaeyehonors2010.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_573_',
                'SITE_URL' => 'http://www.cinemaeyehonors2010.com',
                'DOMAIN' => 'cinemaeyehonors2010.com',
            ),
            '30minutefinance.com' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_575_',
                'SITE_URL' => 'http://www.30minutefinance.com',
                'DOMAIN' => '30minutefinance.com',
            ),
            'biminnesota.org' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_576_',
                'SITE_URL' => 'http://www.biminnesota.org',
                'DOMAIN' => 'biminnesota.org',
            ),
            'lmtmemorial.org' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_577_',
                'SITE_URL' => 'http://www.lmtmemorial.org',
                'DOMAIN' => 'lmtmemorial.org',
            ),
            'jennanthony.info' =>
            array(
                'TABLE_SCHEMA' => 'getkronk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_582_',
                'SITE_URL' => 'http://www.jennanthony.info',
                'DOMAIN' => 'jennanthony.info',
            ),
            'ghotir.us' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_1_84292_',
                'SITE_URL' => 'http://www.ghotir.us',
                'DOMAIN' => 'ghotir.us',
            ),
            'loyolapolytechnic.org' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_1_84293_',
                'SITE_URL' => 'http://www.loyolapolytechnic.org',
                'DOMAIN' => 'loyolapolytechnic.org',
            ),
            'notedabove.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_1_84294_',
                'SITE_URL' => 'http://www.notedabove.com',
                'DOMAIN' => 'notedabove.com',
            ),
            'panningfamilyband.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_1_84295_',
                'SITE_URL' => 'http://www.panningfamilyband.com',
                'DOMAIN' => 'panningfamilyband.com',
            ),
            'rfbradley.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_1_84296_',
                'SITE_URL' => 'http://www.rfbradley.com',
                'DOMAIN' => 'rfbradley.com',
            ),
            'lessthanthreecomics.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1075_',
                'SITE_URL' => 'http://www.lessthanthreecomics.com',
                'DOMAIN' => 'lessthanthreecomics.com',
            ),
            'golfclubcomponents.info' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1092_',
                'SITE_URL' => 'http://www.golfclubcomponents.info',
                'DOMAIN' => 'golfclubcomponents.info',
            ),
            'percyspersonals.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_541_',
                'SITE_URL' => 'http://www.percyspersonals.com',
                'DOMAIN' => 'percyspersonals.com',
            ),
            'rochestercost.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_546_',
                'SITE_URL' => 'http://www.rochestercost.com',
                'DOMAIN' => 'rochestercost.com',
            ),
            'nq12.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_550_',
                'SITE_URL' => 'http://www.nq12.com',
                'DOMAIN' => 'nq12.com',
            ),
            'signtecs.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_560_',
                'SITE_URL' => 'http://www.signtecs.com',
                'DOMAIN' => 'signtecs.com',
            ),
            'tahoewelcome.com' =>
            array(
                'TABLE_SCHEMA' => 'ghotir_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_566_',
                'SITE_URL' => 'http://www.tahoewelcome.com',
                'DOMAIN' => 'tahoewelcome.com',
            ),
            'gokhankizil.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_1_81628_',
                'SITE_URL' => 'http://www.gokhankizil.com',
                'DOMAIN' => 'gokhankizil.com',
            ),
            'goodmusicfm.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_1_81629_',
                'SITE_URL' => 'http://www.goodmusicfm.com',
                'DOMAIN' => 'goodmusicfm.com',
            ),
            'gpev.info' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_1_81630_',
                'SITE_URL' => 'http://www.gpev.info',
                'DOMAIN' => 'gpev.info',
            ),
            'greatoaksschool.org' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_1_81631_',
                'SITE_URL' => 'http://www.greatoaksschool.org',
                'DOMAIN' => 'greatoaksschool.org',
            ),
            'gucenerji.net' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_1_81632_',
                'SITE_URL' => 'http://www.gucenerji.net',
                'DOMAIN' => 'gucenerji.net',
            ),
            'tomsfitness.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1089_',
                'SITE_URL' => 'http://www.tomsfitness.com',
                'DOMAIN' => 'tomsfitness.com',
            ),
            'meridianhomes-sc.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_536_',
                'SITE_URL' => 'http://www.meridianhomes-sc.com',
                'DOMAIN' => 'meridianhomes-sc.com',
            ),
            'naturalgroupcr.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_538_',
                'SITE_URL' => 'http://www.naturalgroupcr.com',
                'DOMAIN' => 'naturalgroupcr.com',
            ),
            'plusfortyone.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_540_',
                'SITE_URL' => 'http://www.plusfortyone.com',
                'DOMAIN' => 'plusfortyone.com',
            ),
            'noevalleyacupuncture.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_543_',
                'SITE_URL' => 'http://www.noevalleyacupuncture.com',
                'DOMAIN' => 'noevalleyacupuncture.com',
            ),
            'padreislandtv.com' =>
            array(
                'TABLE_SCHEMA' => 'gokhanki_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_544_',
                'SITE_URL' => 'http://www.padreislandtv.com',
                'DOMAIN' => 'padreislandtv.com',
            ),
            'gomegadega.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_1_83802_',
                'SITE_URL' => 'http://www.gomegadega.com',
                'DOMAIN' => 'gomegadega.com',
            ),
            'patriotmark.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_1_83803_',
                'SITE_URL' => 'http://www.patriotmark.com',
                'DOMAIN' => 'patriotmark.com',
            ),
            'minjae.me' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_1_83804_',
                'SITE_URL' => 'http://www.minjae.me',
                'DOMAIN' => 'minjae.me',
            ),
            'sfchefsunite.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_wp974',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.sfchefsunite.com',
                'DOMAIN' => 'sfchefsunite.com',
            ),
            'mathpedia.info' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_1_83806_',
                'SITE_URL' => 'http://www.mathpedia.info',
                'DOMAIN' => 'mathpedia.info',
            ),
            'houndsummitteam.org' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1467_',
                'SITE_URL' => 'http://www.houndsummitteam.org',
                'DOMAIN' => 'houndsummitteam.org',
            ),
            'tele8mercedes.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_91_',
                'SITE_URL' => 'http://www.tele8mercedes.com',
                'DOMAIN' => 'tele8mercedes.com',
            ),
            'thehivegalleryslc.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_92_',
                'SITE_URL' => 'http://www.thehivegalleryslc.com',
                'DOMAIN' => 'thehivegalleryslc.com',
            ),
            'bariatricsurgery.biz' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_93_',
                'SITE_URL' => 'http://www.bariatricsurgery.biz',
                'DOMAIN' => 'bariatricsurgery.biz',
            ),
            'chowderman.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1468_',
                'SITE_URL' => 'http://www.chowderman.com',
                'DOMAIN' => 'chowderman.com',
            ),
            'videojunky.net' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1090_',
                'SITE_URL' => 'http://www.videojunky.net',
                'DOMAIN' => 'videojunky.net',
            ),
            'g4-ranch.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1091_',
                'SITE_URL' => 'http://www.g4-ranch.com',
                'DOMAIN' => 'g4-ranch.com',
            ),
            'spiritoflifefoundation.org' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_542_',
                'SITE_URL' => 'http://www.spiritoflifefoundation.org',
                'DOMAIN' => 'spiritoflifefoundation.org',
            ),
            'mynewslab.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_547_',
                'SITE_URL' => 'http://www.mynewslab.com',
                'DOMAIN' => 'mynewslab.com',
            ),
            'rocknrollz.net' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_548_',
                'SITE_URL' => 'http://www.rocknrollz.net',
                'DOMAIN' => 'rocknrollz.net',
            ),
            'niangsou.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_551_',
                'SITE_URL' => 'http://www.niangsou.com',
                'DOMAIN' => 'niangsou.com',
            ),
            'misswaynecounty.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_552_',
                'SITE_URL' => 'http://www.misswaynecounty.com',
                'DOMAIN' => 'misswaynecounty.com',
            ),
            'rentapropertyinpaphos.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_wp257',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://rentapropertyinpaphos.com',
                'DOMAIN' => 'rentapropertyinpaphos.com',
            ),
            'nprasath.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_wp419',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://nprasath.com',
                'DOMAIN' => 'nprasath.com',
            ),
            'kcforcongress.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_wp516',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://kcforcongress.com',
                'DOMAIN' => 'kcforcongress.com',
            ),
            'booksbyrustand.com' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_wp871',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://booksbyrustand.com',
                'DOMAIN' => 'booksbyrustand.com',
            ),
            'oysterbaychamber.org' =>
            array(
                'TABLE_SCHEMA' => 'gomegade_wp92',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://oysterbaychamber.org',
                'DOMAIN' => 'oysterbaychamber.org',
            ),
            'amygotliffe.org' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1153_',
                'SITE_URL' => 'http://www.amygotliffe.org',
                'DOMAIN' => 'amygotliffe.org',
            ),
            'announceitevents.com' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1154_',
                'SITE_URL' => 'http://www.announceitevents.com',
                'DOMAIN' => 'announceitevents.com',
            ),
            'aramazd.net' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1155_',
                'SITE_URL' => 'http://www.aramazd.net',
                'DOMAIN' => 'aramazd.net',
            ),
            'britneyes.com' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1156_',
                'SITE_URL' => 'http://www.britneyes.com',
                'DOMAIN' => 'britneyes.com',
            ),
            'goodbuildings.info' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_846_',
                'SITE_URL' => 'http://www.goodbuildings.info',
                'DOMAIN' => 'goodbuildings.info',
            ),
            'mianmoman.com' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_847_',
                'SITE_URL' => 'http://www.mianmoman.com',
                'DOMAIN' => 'mianmoman.com',
            ),
            'petlossgriefguide.com' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_848_',
                'SITE_URL' => 'http://www.petlossgriefguide.com',
                'DOMAIN' => 'petlossgriefguide.com',
            ),
            'svgcl.org' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_849_',
                'SITE_URL' => 'http://www.svgcl.org',
                'DOMAIN' => 'svgcl.org',
            ),
            'srsla.org' =>
            array(
                'TABLE_SCHEMA' => 'goodbuil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_850_',
                'SITE_URL' => 'http://www.srsla.org',
                'DOMAIN' => 'srsla.org',
            ),
            'guesthouse-agatebeach.com' =>
            array(
                'TABLE_SCHEMA' => 'guesthou_wp109',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.guesthouse-agatebeach.com',
                'DOMAIN' => 'guesthouse-agatebeach.com',
            ),
            'handbagsbyyou.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_1_83842_',
                'SITE_URL' => 'http://www.handbagsbyyou.com',
                'DOMAIN' => 'handbagsbyyou.com',
            ),
            'tinagivensprose.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_1_83843_',
                'SITE_URL' => 'http://www.tinagivensprose.com',
                'DOMAIN' => 'tinagivensprose.com',
            ),
            'magwooddrystack.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_1_83844_',
                'SITE_URL' => 'http://www.magwooddrystack.com',
                'DOMAIN' => 'magwooddrystack.com',
            ),
            'lesshype.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_1_83845_',
                'SITE_URL' => 'http://www.lesshype.com',
                'DOMAIN' => 'lesshype.com',
            ),
            'kosherpower.org' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_1_83846_',
                'SITE_URL' => 'http://www.kosherpower.org',
                'DOMAIN' => 'kosherpower.org',
            ),
            'gps2doublecad.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_57_',
                'SITE_URL' => 'http://www.gps2doublecad.com',
                'DOMAIN' => 'gps2doublecad.com',
            ),
            'mmsoline.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_58_',
                'SITE_URL' => 'http://www.mmsoline.com',
                'DOMAIN' => 'mmsoline.com',
            ),
            'southjerseycenter.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_59_',
                'SITE_URL' => 'http://www.southjerseycenter.com',
                'DOMAIN' => 'southjerseycenter.com',
            ),
            'camerawhore.me' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_60_',
                'SITE_URL' => 'http://www.camerawhore.me',
                'DOMAIN' => 'camerawhore.me',
            ),
            'sucompraweb.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_61_',
                'SITE_URL' => 'http://www.sucompraweb.com',
                'DOMAIN' => 'sucompraweb.com',
            ),
            'vickipeeki.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_62_',
                'SITE_URL' => 'http://www.vickipeeki.com',
                'DOMAIN' => 'vickipeeki.com',
            ),
            'weaco.org' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1088_',
                'SITE_URL' => 'http://www.weaco.org',
                'DOMAIN' => 'weaco.org',
            ),
            'mariatmejia.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_534_',
                'SITE_URL' => 'http://www.mariatmejia.com',
                'DOMAIN' => 'mariatmejia.com',
            ),
            'matthewgreen.us' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_535_',
                'SITE_URL' => 'http://www.matthewgreen.us',
                'DOMAIN' => 'matthewgreen.us',
            ),
            'pompapetriyasos.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_564_',
                'SITE_URL' => 'http://www.pompapetriyasos.com',
                'DOMAIN' => 'pompapetriyasos.com',
            ),
            'mcas2003.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_569_',
                'SITE_URL' => 'http://www.mcas2003.com',
                'DOMAIN' => 'mcas2003.com',
            ),
            'soireesbearquebec.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_wp318',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://soireesbearquebec.com',
                'DOMAIN' => 'soireesbearquebec.com',
            ),
            'wichitaeconomicdevelopment.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_wp386',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://wichitaeconomicdevelopment.com',
                'DOMAIN' => 'wichitaeconomicdevelopment.com',
            ),
            't32013.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_wp535',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://t32013.com',
                'DOMAIN' => 't32013.com',
            ),
            'newrock1049x.info' =>
            array(
                'TABLE_SCHEMA' => 'handbags_wp648',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://newrock1049x.info',
                'DOMAIN' => 'newrock1049x.info',
            ),
            'gsmartit.com' =>
            array(
                'TABLE_SCHEMA' => 'handbags_wp765',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://gsmartit.com',
                'DOMAIN' => 'gsmartit.com',
            ),
            'hauntedlives.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_1_81173_',
                'SITE_URL' => 'http://www.hauntedlives.com',
                'DOMAIN' => 'hauntedlives.com',
            ),
            'hdfilmizletr.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_1_81174_',
                'SITE_URL' => 'http://www.hdfilmizletr.com',
                'DOMAIN' => 'hdfilmizletr.com',
            ),
            'heartheatingair.org' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_1_81175_',
                'SITE_URL' => 'http://www.heartheatingair.org',
                'DOMAIN' => 'heartheatingair.org',
            ),
            'holderbeddinginc.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_1_81176_',
                'SITE_URL' => 'http://www.holderbeddinginc.com',
                'DOMAIN' => 'holderbeddinginc.com',
            ),
            'homesforrentdurhamnc.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_1_81177_',
                'SITE_URL' => 'http://www.homesforrentdurhamnc.com',
                'DOMAIN' => 'homesforrentdurhamnc.com',
            ),
            'stud8b.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1086_',
                'SITE_URL' => 'http://www.stud8b.com',
                'DOMAIN' => 'stud8b.com',
            ),
            'ldspw.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_522_',
                'SITE_URL' => 'http://www.ldspw.com',
                'DOMAIN' => 'ldspw.com',
            ),
            'lesjeuneschangentlafrique.org' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_523_',
                'SITE_URL' => 'http://www.lesjeuneschangentlafrique.org',
                'DOMAIN' => 'lesjeuneschangentlafrique.org',
            ),
            'kumordzi.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_529_',
                'SITE_URL' => 'http://www.kumordzi.com',
                'DOMAIN' => 'kumordzi.com',
            ),
            'labourbroadheath.org' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_530_',
                'SITE_URL' => 'http://www.labourbroadheath.org',
                'DOMAIN' => 'labourbroadheath.org',
            ),
            'pietvonline.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_559_',
                'SITE_URL' => 'http://www.pietvonline.com',
                'DOMAIN' => 'pietvonline.com',
            ),
            'amargosasolar.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_wp633',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://amargosasolar.com',
                'DOMAIN' => 'amargosasolar.com',
            ),
            'besmard.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_wp717',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://besmard.com',
                'DOMAIN' => 'besmard.com',
            ),
            'metaloxides.net' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_wp844',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://metaloxides.net',
                'DOMAIN' => 'metaloxides.net',
            ),
            'hmghmy.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_wp895',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://hmghmy.com',
                'DOMAIN' => 'hmghmy.com',
            ),
            'betseyvillepursesheaven.com' =>
            array(
                'TABLE_SCHEMA' => 'hauntedl_wp983',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://betseyvillepursesheaven.com',
                'DOMAIN' => 'betseyvillepursesheaven.com',
            ),
            'digitalounge.net' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1299_',
                'SITE_URL' => 'http://www.digitalounge.net',
                'DOMAIN' => 'digitalounge.net',
            ),
            'mp4online.info' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1300_',
                'SITE_URL' => 'http://www.mp4online.info',
                'DOMAIN' => 'mp4online.info',
            ),
            'mountnmemories.net' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1301_',
                'SITE_URL' => 'http://www.mountnmemories.net',
                'DOMAIN' => 'mountnmemories.net',
            ),
            'myluckydoglottery.com' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1302_',
                'SITE_URL' => 'http://www.myluckydoglottery.com',
                'DOMAIN' => 'myluckydoglottery.com',
            ),
            'hazenprojects.com' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_939_',
                'SITE_URL' => 'http://www.hazenprojects.com',
                'DOMAIN' => 'hazenprojects.com',
            ),
            'ocdaryarestaurant.com' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_941_',
                'SITE_URL' => 'http://www.ocdaryarestaurant.com',
                'DOMAIN' => 'ocdaryarestaurant.com',
            ),
            '365harvest.com' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_942_',
                'SITE_URL' => 'http://www.365harvest.com',
                'DOMAIN' => '365harvest.com',
            ),
            'garrett-hedlund.info' =>
            array(
                'TABLE_SCHEMA' => 'hazenpro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_943_',
                'SITE_URL' => 'http://www.garrett-hedlund.info',
                'DOMAIN' => 'garrett-hedlund.info',
            ),
            'trackfourdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1087_',
                'SITE_URL' => 'http://www.trackfourdesign.com',
                'DOMAIN' => 'trackfourdesign.com',
            ),
            'mamatemplate.com' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_481_',
                'SITE_URL' => 'http://www.mamatemplate.com',
                'DOMAIN' => 'mamatemplate.com',
            ),
            'mahaon.us' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_525_',
                'SITE_URL' => 'http://www.mahaon.us',
                'DOMAIN' => 'mahaon.us',
            ),
            'lucid-links.com' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_526_',
                'SITE_URL' => 'http://www.lucid-links.com',
                'DOMAIN' => 'lucid-links.com',
            ),
            'lifeforcefamilies.com' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_531_',
                'SITE_URL' => 'http://www.lifeforcefamilies.com',
                'DOMAIN' => 'lifeforcefamilies.com',
            ),
            'manchestersoccersd.com' =>
            array(
                'TABLE_SCHEMA' => 'hefanews_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_533_',
                'SITE_URL' => 'http://www.manchestersoccersd.com',
                'DOMAIN' => 'manchestersoccersd.com',
            ),
            'mymeow-wear.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_mymeow',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://mymeow-wear.com',
                'DOMAIN' => 'mymeow-wear.com',
            ),
            'mymoviechurch.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_mymovie',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://mymoviechurch.com',
                'DOMAIN' => 'mymoviechurch.com',
            ),
            'hentaisuki.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_1_82600_',
                'SITE_URL' => 'http://www.hentaisuki.com',
                'DOMAIN' => 'hentaisuki.com',
            ),
            'joseph-duck.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_1_82601_',
                'SITE_URL' => 'http://www.joseph-duck.com',
                'DOMAIN' => 'joseph-duck.com',
            ),
            'lafayettegrapevine.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_1_82602_',
                'SITE_URL' => 'http://www.lafayettegrapevine.com',
                'DOMAIN' => 'lafayettegrapevine.com',
            ),
            'lernerandtunstall.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_1_82603_',
                'SITE_URL' => 'http://www.lernerandtunstall.com',
                'DOMAIN' => 'lernerandtunstall.com',
            ),
            'lubbockgrapevine.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_1_82604_',
                'SITE_URL' => 'http://www.lubbockgrapevine.com',
                'DOMAIN' => 'lubbockgrapevine.com',
            ),
            'anelagerqvist.net' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_10_',
                'SITE_URL' => 'http://www.anelagerqvist.net',
                'DOMAIN' => 'anelagerqvist.net',
            ),
            'angloarabictranslation.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_11_',
                'SITE_URL' => 'http://angloarabictranslation.com',
                'DOMAIN' => 'angloarabictranslation.com',
            ),
            'allthingsmexico.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_9_',
                'SITE_URL' => 'http://www.allthingsmexico.com',
                'DOMAIN' => 'allthingsmexico.com',
            ),
            'itsmyweddingsite.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1084_',
                'SITE_URL' => 'http://www.itsmyweddingsite.com',
                'DOMAIN' => 'itsmyweddingsite.com',
            ),
            'que-es-la-diabetes.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1085_',
                'SITE_URL' => 'http://www.que-es-la-diabetes.com',
                'DOMAIN' => 'que-es-la-diabetes.com',
            ),
            'kimberlygrant.net' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_511_',
                'SITE_URL' => 'http://www.kimberlygrant.net',
                'DOMAIN' => 'kimberlygrant.net',
            ),
            'kcconvoyofhope.org' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_515_',
                'SITE_URL' => 'http://www.kcconvoyofhope.org',
                'DOMAIN' => 'kcconvoyofhope.org',
            ),
            'kolbodagarden.info' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_518_',
                'SITE_URL' => 'http://www.kolbodagarden.info',
                'DOMAIN' => 'kolbodagarden.info',
            ),
            'koleurzmusic.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_519_',
                'SITE_URL' => 'http://www.koleurzmusic.com',
                'DOMAIN' => 'koleurzmusic.com',
            ),
            'kidsrkidscarrollton.com' =>
            array(
                'TABLE_SCHEMA' => 'hentaisu_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_528_',
                'SITE_URL' => 'http://www.kidsrkidscarrollton.com',
                'DOMAIN' => 'kidsrkidscarrollton.com',
            ),
            'hnchangsheng.com' =>
            array(
                'TABLE_SCHEMA' => 'hnchangs_tkp',
                'TABLE_PREFIX' => 'wp_1_2285_',
                'SITE_URL' => 'http://www.hnchangsheng.com',
                'DOMAIN' => 'hnchangsheng.com',
            ),
            'hncykj.com' =>
            array(
                'TABLE_SCHEMA' => 'hnchangs_tkp',
                'TABLE_PREFIX' => 'wp_1_2286_',
                'SITE_URL' => 'http://www.hncykj.com',
                'DOMAIN' => 'hncykj.com',
            ),
            'hopebowerselc.com' =>
            array(
                'TABLE_SCHEMA' => 'hnchangs_tkp',
                'TABLE_PREFIX' => 'wp_1_2287_',
                'SITE_URL' => 'http://www.hopebowerselc.com',
                'DOMAIN' => 'hopebowerselc.com',
            ),
            'hopsense.com' =>
            array(
                'TABLE_SCHEMA' => 'hnchangs_tkp',
                'TABLE_PREFIX' => 'wp_1_2288_',
                'SITE_URL' => 'http://www.hopsense.com',
                'DOMAIN' => 'hopsense.com',
            ),
            'hostalcholula.net' =>
            array(
                'TABLE_SCHEMA' => 'hnchangs_tkp',
                'TABLE_PREFIX' => 'wp_1_2289_',
                'SITE_URL' => 'http://www.hostalcholula.net',
                'DOMAIN' => 'hostalcholula.net',
            ),
            'how-does-neurofeedback-work.com' =>
            array(
                'TABLE_SCHEMA' => 'homesfor_tkp',
                'TABLE_PREFIX' => 'wp_1_81136_',
                'SITE_URL' => 'http://www.how-does-neurofeedback-work.com',
                'DOMAIN' => 'how-does-neurofeedback-work.com',
            ),
            'homesforsaleindurhamnc.com' =>
            array(
                'TABLE_SCHEMA' => 'homesfor_tkp',
                'TABLE_PREFIX' => 'wp_1_81178_',
                'SITE_URL' => 'http://www.homesforsaleindurhamnc.com',
                'DOMAIN' => 'homesforsaleindurhamnc.com',
            ),
            'hoosiersforwineshipping.com' =>
            array(
                'TABLE_SCHEMA' => 'homesfor_tkp',
                'TABLE_PREFIX' => 'wp_1_81179_',
                'SITE_URL' => 'http://www.hoosiersforwineshipping.com',
                'DOMAIN' => 'hoosiersforwineshipping.com',
            ),
            'housesforrentindurhamnc.com' =>
            array(
                'TABLE_SCHEMA' => 'homesfor_tkp',
                'TABLE_PREFIX' => 'wp_1_81180_',
                'SITE_URL' => 'http://www.housesforrentindurhamnc.com',
                'DOMAIN' => 'housesforrentindurhamnc.com',
            ),
            'housesforsaleindurhamnc.com' =>
            array(
                'TABLE_SCHEMA' => 'homesfor_tkp',
                'TABLE_PREFIX' => 'wp_1_81181_',
                'SITE_URL' => 'http://www.housesforsaleindurhamnc.com',
                'DOMAIN' => 'housesforsaleindurhamnc.com',
            ),
            'allthedumps.com' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1176_',
                'SITE_URL' => 'http://www.allthedumps.com',
                'DOMAIN' => 'allthedumps.com',
            ),
            'annualreport2010-11.com' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1177_',
                'SITE_URL' => 'http://www.annualreport2010-11.com',
                'DOMAIN' => 'annualreport2010-11.com',
            ),
            'artemroz.com' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1179_',
                'SITE_URL' => 'http://www.artemroz.com',
                'DOMAIN' => 'artemroz.com',
            ),
            'arkofrefugetabernacle.org' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1180_',
                'SITE_URL' => 'http://www.arkofrefugetabernacle.org',
                'DOMAIN' => 'arkofrefugetabernacle.org',
            ),
            'honkytonkhighway.info' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_870_',
                'SITE_URL' => 'http://www.honkytonkhighway.info',
                'DOMAIN' => 'honkytonkhighway.info',
            ),
            'pumps.ws' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_871_',
                'SITE_URL' => 'http://www.pumps.ws',
                'DOMAIN' => 'pumps.ws',
            ),
            'sissweets.net' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_872_',
                'SITE_URL' => 'http://www.sissweets.net',
                'DOMAIN' => 'sissweets.net',
            ),
            'obamaonisrael.org' =>
            array(
                'TABLE_SCHEMA' => 'honkyton_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_874_',
                'SITE_URL' => 'http://www.obamaonisrael.org',
                'DOMAIN' => 'obamaonisrael.org',
            ),
            'infostyling.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_1_81638_',
                'SITE_URL' => 'http://www.infostyling.com',
                'DOMAIN' => 'infostyling.com',
            ),
            'hot-website.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_1_81639_',
                'SITE_URL' => 'http://www.hot-website.com',
                'DOMAIN' => 'hot-website.com',
            ),
            'hqkht.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_1_81640_',
                'SITE_URL' => 'http://www.hqkht.com',
                'DOMAIN' => 'hqkht.com',
            ),
            'hszycom.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_1_81641_',
                'SITE_URL' => 'http://www.hszycom.com',
                'DOMAIN' => 'hszycom.com',
            ),
            'huadongcs.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_1_81642_',
                'SITE_URL' => 'http://www.huadongcs.com',
                'DOMAIN' => 'huadongcs.com',
            ),
            'locatesalesjobs.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1080_',
                'SITE_URL' => 'http://www.locatesalesjobs.com',
                'DOMAIN' => 'locatesalesjobs.com',
            ),
            'fyshoes.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1081_',
                'SITE_URL' => 'http://www.fyshoes.com',
                'DOMAIN' => 'fyshoes.com',
            ),
            'isdvf.org' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_496_',
                'SITE_URL' => 'http://www.isdvf.org',
                'DOMAIN' => 'isdvf.org',
            ),
            'intersport-travel.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_504_',
                'SITE_URL' => 'http://www.intersport-travel.com',
                'DOMAIN' => 'intersport-travel.com',
            ),
            'ipswichtownfootballclub.com' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_505_',
                'SITE_URL' => 'http://www.ipswichtownfootballclub.com',
                'DOMAIN' => 'ipswichtownfootballclub.com',
            ),
            'immigrationcz.org' =>
            array(
                'TABLE_SCHEMA' => 'hooverva_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_521_',
                'SITE_URL' => 'http://www.immigrationcz.org',
                'DOMAIN' => 'immigrationcz.org',
            ),
            'hosttopost.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_1_82533_',
                'SITE_URL' => 'http://www.hosttopost.com',
                'DOMAIN' => 'hosttopost.com',
            ),
            'maisonebene.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_1_82534_',
                'SITE_URL' => 'http://www.maisonebene.com',
                'DOMAIN' => 'maisonebene.com',
            ),
            '1001house.net' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_1_82535_',
                'SITE_URL' => 'http://www.1001house.net',
                'DOMAIN' => '1001house.net',
            ),
            'juhbseventos.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_1_82536_',
                'SITE_URL' => 'http://www.juhbseventos.com',
                'DOMAIN' => 'juhbseventos.com',
            ),
            'yytoon.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_1_82537_',
                'SITE_URL' => 'http://www.yytoon.com',
                'DOMAIN' => 'yytoon.com',
            ),
            '997kjox.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_5_',
                'SITE_URL' => 'http://997kjox.com',
                'DOMAIN' => '997kjox.com',
            ),
            'hnczsm.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1082_',
                'SITE_URL' => 'http://www.hnczsm.com',
                'DOMAIN' => 'hnczsm.com',
            ),
            'funjet-travel.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1083_',
                'SITE_URL' => 'http://www.funjet-travel.com',
                'DOMAIN' => 'funjet-travel.com',
            ),
            'istanbulescortmisra.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_507_',
                'SITE_URL' => 'http://www.istanbulescortmisra.com',
                'DOMAIN' => 'istanbulescortmisra.com',
            ),
            'jadn.tv' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_513_',
                'SITE_URL' => 'http://www.jadn.tv',
                'DOMAIN' => 'jadn.tv',
            ),
            'juttaneumann-newyork.info' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_516_',
                'SITE_URL' => 'http://www.juttaneumann-newyork.info',
                'DOMAIN' => 'juttaneumann-newyork.info',
            ),
            'ispairportlimo.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_527_',
                'SITE_URL' => 'http://www.ispairportlimo.com',
                'DOMAIN' => 'ispairportlimo.com',
            ),
            'jewsgonemeshuga.com' =>
            array(
                'TABLE_SCHEMA' => 'hosttopo_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_532_',
                'SITE_URL' => 'http://www.jewsgonemeshuga.com',
                'DOMAIN' => 'jewsgonemeshuga.com',
            ),
            'huellasdeuncion.net' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_1_81643_',
                'SITE_URL' => 'http://www.huellasdeuncion.net',
                'DOMAIN' => 'huellasdeuncion.net',
            ),
            'iclltda.com' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_1_81644_',
                'SITE_URL' => 'http://www.iclltda.com',
                'DOMAIN' => 'iclltda.com',
            ),
            'icutleather.com' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_1_81645_',
                'SITE_URL' => 'http://www.icutleather.com',
                'DOMAIN' => 'icutleather.com',
            ),
            'inglesa.us' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_1_81646_',
                'SITE_URL' => 'http://www.inglesa.us',
                'DOMAIN' => 'inglesa.us',
            ),
            'cricketwife.com' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1078_',
                'SITE_URL' => 'http://www.cricketwife.com',
                'DOMAIN' => 'cricketwife.com',
            ),
            'firstdieting.com' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1079_',
                'SITE_URL' => 'http://www.firstdieting.com',
                'DOMAIN' => 'firstdieting.com',
            ),
            'iacampsandretreats.org' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_498_',
                'SITE_URL' => 'http://www.iacampsandretreats.org',
                'DOMAIN' => 'iacampsandretreats.org',
            ),
            'hxedu365.org' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_500_',
                'SITE_URL' => 'http://www.hxedu365.org',
                'DOMAIN' => 'hxedu365.org',
            ),
            'iloveoldorchardbeach.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_wp326',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://iloveoldorchardbeach.com',
                'DOMAIN' => 'iloveoldorchardbeach.com',
            ),
            'ilmta15.org' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_510_',
                'SITE_URL' => 'http://www.ilmta15.org',
                'DOMAIN' => 'ilmta15.org',
            ),
            'icfmanagement.com' =>
            array(
                'TABLE_SCHEMA' => 'huellasd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_520_',
                'SITE_URL' => 'http://www.icfmanagement.com',
                'DOMAIN' => 'icfmanagement.com',
            ),
            'huntingtononthemove.org' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_1_83852_',
                'SITE_URL' => 'http://www.huntingtononthemove.org',
                'DOMAIN' => 'huntingtononthemove.org',
            ),
            'giangrandigourmet.net' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_wp352',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.giangrandigourmet.net',
                'DOMAIN' => 'giangrandigourmet.net',
            ),
            'e-hospitalbeds.com' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1076_',
                'SITE_URL' => 'http://www.e-hospitalbeds.com',
                'DOMAIN' => 'e-hospitalbeds.com',
            ),
            'best-food-recipes.net' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1077_',
                'SITE_URL' => 'http://www.best-food-recipes.net',
                'DOMAIN' => 'best-food-recipes.net',
            ),
            'hicooking.com' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_506_',
                'SITE_URL' => 'http://www.hicooking.com',
                'DOMAIN' => 'hicooking.com',
            ),
            'homeownerswakeup.com' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_508_',
                'SITE_URL' => 'http://www.homeownerswakeup.com',
                'DOMAIN' => 'homeownerswakeup.com',
            ),
            'holistichealthcareinsurance.com' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_514_',
                'SITE_URL' => 'http://www.holistichealthcareinsurance.com',
                'DOMAIN' => 'holistichealthcareinsurance.com',
            ),
            'ht360.net' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_517_',
                'SITE_URL' => 'http://www.ht360.net',
                'DOMAIN' => 'ht360.net',
            ),
            'heizilanqiu.com' =>
            array(
                'TABLE_SCHEMA' => 'huntingt_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_524_',
                'SITE_URL' => 'http://www.heizilanqiu.com',
                'DOMAIN' => 'heizilanqiu.com',
            ),
            'ieveisk.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_1_82518_',
                'SITE_URL' => 'http://www.ieveisk.com',
                'DOMAIN' => 'ieveisk.com',
            ),
            'greenlandpaddle.info' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_1_82519_',
                'SITE_URL' => 'http://www.greenlandpaddle.info',
                'DOMAIN' => 'greenlandpaddle.info',
            ),
            'quipscafe.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_1_82520_',
                'SITE_URL' => 'http://www.quipscafe.com',
                'DOMAIN' => 'quipscafe.com',
            ),
            'hazelcontacts.net' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_1_82521_',
                'SITE_URL' => 'http://www.hazelcontacts.net',
                'DOMAIN' => 'hazelcontacts.net',
            ),
            'reofundinggroup.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_1_82522_',
                'SITE_URL' => 'http://www.reofundinggroup.com',
                'DOMAIN' => 'reofundinggroup.com',
            ),
            'consumerhealthnet.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1073_',
                'SITE_URL' => 'http://www.consumerhealthnet.com',
                'DOMAIN' => 'consumerhealthnet.com',
            ),
            'golf-for-fun.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_497_',
                'SITE_URL' => 'http://www.golf-for-fun.com',
                'DOMAIN' => 'golf-for-fun.com',
            ),
            'goodwinfamilydentistry.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_509_',
                'SITE_URL' => 'http://www.goodwinfamilydentistry.com',
                'DOMAIN' => 'goodwinfamilydentistry.com',
            ),
            'nicoleluxuryestates.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_549_',
                'SITE_URL' => 'http://www.nicoleluxuryestates.com',
                'DOMAIN' => 'nicoleluxuryestates.com',
            ),
            'oohlytv.com' =>
            array(
                'TABLE_SCHEMA' => 'ieveisk_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_563_',
                'SITE_URL' => 'http://www.oohlytv.com',
                'DOMAIN' => 'oohlytv.com',
            ),
            'igreigra.com' =>
            array(
                'TABLE_SCHEMA' => 'igreigra_tkp',
                'TABLE_PREFIX' => 'wp_1_2295_',
                'SITE_URL' => 'http://www.igreigra.com',
                'DOMAIN' => 'igreigra.com',
            ),
            'ihopsouthbay.org' =>
            array(
                'TABLE_SCHEMA' => 'igreigra_tkp',
                'TABLE_PREFIX' => 'wp_1_2296_',
                'SITE_URL' => 'http://www.ihopsouthbay.org',
                'DOMAIN' => 'ihopsouthbay.org',
            ),
            'iisjobs.com' =>
            array(
                'TABLE_SCHEMA' => 'igreigra_tkp',
                'TABLE_PREFIX' => 'wp_1_2297_',
                'SITE_URL' => 'http://www.iisjobs.com',
                'DOMAIN' => 'iisjobs.com',
            ),
            'ilink-united.com' =>
            array(
                'TABLE_SCHEMA' => 'igreigra_tkp',
                'TABLE_PREFIX' => 'wp_1_2298_',
                'SITE_URL' => 'http://www.ilink-united.com',
                'DOMAIN' => 'ilink-united.com',
            ),
            'imljp.info' =>
            array(
                'TABLE_SCHEMA' => 'igreigra_tkp',
                'TABLE_PREFIX' => 'wp_1_2299_',
                'SITE_URL' => 'http://www.imljp.info',
                'DOMAIN' => 'imljp.info',
            ),
            'ihateyourfans.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_1_83767_',
                'SITE_URL' => 'http://www.ihateyourfans.com',
                'DOMAIN' => 'ihateyourfans.com',
            ),
            'stopcolumbia.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_1_83768_',
                'SITE_URL' => 'http://www.stopcolumbia.com',
                'DOMAIN' => 'stopcolumbia.com',
            ),
            'tristatefca.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_1_83769_',
                'SITE_URL' => 'http://www.tristatefca.com',
                'DOMAIN' => 'tristatefca.com',
            ),
            'fepaseppanama2011.org' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_1_83770_',
                'SITE_URL' => 'http://www.fepaseppanama2011.org',
                'DOMAIN' => 'fepaseppanama2011.org',
            ),
            'cpswatchlegal.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_1_83771_',
                'SITE_URL' => 'http://www.cpswatchlegal.com',
                'DOMAIN' => 'cpswatchlegal.com',
            ),
            'egyptdotnow.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_48_',
                'SITE_URL' => 'http://www.egyptdotnow.com',
                'DOMAIN' => 'egyptdotnow.com',
            ),
            'martinearle.me' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_49_',
                'SITE_URL' => 'http://www.martinearle.me',
                'DOMAIN' => 'martinearle.me',
            ),
            'xn--vck1fsa3852buxc48hh1wp62b7g0b.net' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_50_',
                'SITE_URL' => 'http://www.xn--vck1fsa3852buxc48hh1wp62b7g0b.net',
                'DOMAIN' => 'xn--vck1fsa3852buxc48hh1wp62b7g0b.net',
            ),
            'ifoundtheperfect.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_51_',
                'SITE_URL' => 'http://www.ifoundtheperfect.com',
                'DOMAIN' => 'ifoundtheperfect.com',
            ),
            'agadir-airport.info' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_52_',
                'SITE_URL' => 'http://www.agadir-airport.info',
                'DOMAIN' => 'agadir-airport.info',
            ),
            'bilgininsesi.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_53_',
                'SITE_URL' => 'http://www.bilgininsesi.com',
                'DOMAIN' => 'bilgininsesi.com',
            ),
            'humanobeing.org' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_wp267',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://humanobeing.org',
                'DOMAIN' => 'humanobeing.org',
            ),
            'e-renda.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_702_',
                'SITE_URL' => 'http://www.e-renda.com',
                'DOMAIN' => 'e-renda.com',
            ),
            'agu-eco-racing.info' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_703_',
                'SITE_URL' => 'http://www.agu-eco-racing.info',
                'DOMAIN' => 'agu-eco-racing.info',
            ),
            'executivecorrespondence.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_704_',
                'SITE_URL' => 'http://www.executivecorrespondence.com',
                'DOMAIN' => 'executivecorrespondence.com',
            ),
            'whitehouseclaremi.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_706_',
                'SITE_URL' => 'http://www.whitehouseclaremi.com',
                'DOMAIN' => 'whitehouseclaremi.com',
            ),
            'realestatehollywoodbeach.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1071_',
                'SITE_URL' => 'http://www.realestatehollywoodbeach.com',
                'DOMAIN' => 'realestatehollywoodbeach.com',
            ),
            'springcreekoaksnews.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1072_',
                'SITE_URL' => 'http://www.springcreekoaksnews.com',
                'DOMAIN' => 'springcreekoaksnews.com',
            ),
            'fotobrio.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_484_',
                'SITE_URL' => 'http://www.fotobrio.com',
                'DOMAIN' => 'fotobrio.com',
            ),
            'exclusiveconsultants.net' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_490_',
                'SITE_URL' => 'http://www.exclusiveconsultants.net',
                'DOMAIN' => 'exclusiveconsultants.net',
            ),
            'fellowshipregionalchurch.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_492_',
                'SITE_URL' => 'http://www.fellowshipregionalchurch.com',
                'DOMAIN' => 'fellowshipregionalchurch.com',
            ),
            'evannalynchbr.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_503_',
                'SITE_URL' => 'http://evannalynchbr.com/',
                'DOMAIN' => 'evannalynchbr.com',
            ),
            'newbornbabychildphotographer.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_562_',
                'SITE_URL' => 'http://www.newbornbabychildphotographer.com',
                'DOMAIN' => 'newbornbabychildphotographer.com',
            ),
            'memeorlame.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_wp113',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://memeorlame.com',
                'DOMAIN' => 'memeorlame.com',
            ),
            'ikecharlton0.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_wp17',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ikecharlton0.com',
                'DOMAIN' => 'ikecharlton0.com',
            ),
            'gunsdicebutter.com' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_wp559',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://gunsdicebutter.com',
                'DOMAIN' => 'gunsdicebutter.com',
            ),
            'cnaconline.org' =>
            array(
                'TABLE_SCHEMA' => 'ihateyou_wp568',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://cnaconline.org',
                'DOMAIN' => 'cnaconline.org',
            ),
            '618oakton-1.info' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1172_',
                'SITE_URL' => 'http://www.618oakton-1.info',
                'DOMAIN' => '618oakton-1.info',
            ),
            '60daysick.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1173_',
                'SITE_URL' => 'http://www.60daysick.com',
                'DOMAIN' => '60daysick.com',
            ),
            'agrorematescebollati.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1174_',
                'SITE_URL' => 'http://www.agrorematescebollati.com',
                'DOMAIN' => 'agrorematescebollati.com',
            ),
            'abbeyhultonweather.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1175_',
                'SITE_URL' => 'http://www.abbeyhultonweather.com',
                'DOMAIN' => 'abbeyhultonweather.com',
            ),
            'ilikeprogramming.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_865_',
                'SITE_URL' => 'http://www.ilikeprogramming.com',
                'DOMAIN' => 'ilikeprogramming.com',
            ),
            'justicefactorxlegal.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_866_',
                'SITE_URL' => 'http://www.justicefactorxlegal.com',
                'DOMAIN' => 'justicefactorxlegal.com',
            ),
            'fsbenchmarks.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_867_',
                'SITE_URL' => 'http://www.fsbenchmarks.com',
                'DOMAIN' => 'fsbenchmarks.com',
            ),
            'zimnak.info' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_868_',
                'SITE_URL' => 'http://www.zimnak.info',
                'DOMAIN' => 'zimnak.info',
            ),
            'buckminsterfullerlive.com' =>
            array(
                'TABLE_SCHEMA' => 'ilikepro_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_869_',
                'SITE_URL' => 'http://www.buckminsterfullerlive.com',
                'DOMAIN' => 'buckminsterfullerlive.com',
            ),
            'imgcamp.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_1_2620_',
                'SITE_URL' => 'http://www.imgcamp.com',
                'DOMAIN' => 'imgcamp.com',
            ),
            'internet4info.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_1_2621_',
                'SITE_URL' => 'http://www.internet4info.com',
                'DOMAIN' => 'internet4info.com',
            ),
            'intimaxy-store.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_1_2622_',
                'SITE_URL' => 'http://www.intimaxy-store.com',
                'DOMAIN' => 'intimaxy-store.com',
            ),
            'itiswhatitismusic.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_1_2623_',
                'SITE_URL' => 'http://www.itiswhatitismusic.com',
                'DOMAIN' => 'itiswhatitismusic.com',
            ),
            'ivyhny.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_1_2624_',
                'SITE_URL' => 'http://www.ivyhny.com',
                'DOMAIN' => 'ivyhny.com',
            ),
            'mountknowledge.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1069_',
                'SITE_URL' => 'http://www.mountknowledge.com',
                'DOMAIN' => 'mountknowledge.com',
            ),
            'electricavenuesf.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_489_',
                'SITE_URL' => 'http://www.electricavenuesf.com',
                'DOMAIN' => 'electricavenuesf.com',
            ),
            'long-islandhomeinspection.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_491_',
                'SITE_URL' => 'http://www.long-islandhomeinspection.com',
                'DOMAIN' => 'long-islandhomeinspection.com',
            ),
            'mcdialogue.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_493_',
                'SITE_URL' => 'http://www.mcdialogue.com',
                'DOMAIN' => 'mcdialogue.com',
            ),
            'eatmoreprotein.com' =>
            array(
                'TABLE_SCHEMA' => 'imgcamp_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_494_',
                'SITE_URL' => 'http://www.eatmoreprotein.com',
                'DOMAIN' => 'eatmoreprotein.com',
            ),
            'indiezyne.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_1_83932_',
                'SITE_URL' => 'http://www.indiezyne.com',
                'DOMAIN' => 'indiezyne.com',
            ),
            'savorthesound.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_1_83933_',
                'SITE_URL' => 'http://www.savorthesound.com',
                'DOMAIN' => 'savorthesound.com',
            ),
            'tacomasouthrotary.org' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_1_83934_',
                'SITE_URL' => 'http://www.tacomasouthrotary.org',
                'DOMAIN' => 'tacomasouthrotary.org',
            ),
            'ankylosworld.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_1_83935_',
                'SITE_URL' => 'http://www.ankylosworld.com',
                'DOMAIN' => 'ankylosworld.com',
            ),
            'aegisworldfund.org' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_1_83936_',
                'SITE_URL' => 'http://www.aegisworldfund.org',
                'DOMAIN' => 'aegisworldfund.org',
            ),
            'readrecipes.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1070_',
                'SITE_URL' => 'http://www.readrecipes.com',
                'DOMAIN' => 'readrecipes.com',
            ),
            'emhimn.org' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_482_',
                'SITE_URL' => 'http://www.emhimn.org',
                'DOMAIN' => 'emhimn.org',
            ),
            'energia-spa.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_485_',
                'SITE_URL' => 'http://www.energia-spa.com',
                'DOMAIN' => 'energia-spa.com',
            ),
            'eticarett.net' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_486_',
                'SITE_URL' => 'http://www.eticarett.net',
                'DOMAIN' => 'eticarett.net',
            ),
            'escapadesafaris.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_495_',
                'SITE_URL' => 'http://www.escapadesafaris.com',
                'DOMAIN' => 'escapadesafaris.com',
            ),
            'eugenescottishfestival2012.com' =>
            array(
                'TABLE_SCHEMA' => 'indiezyn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_499_',
                'SITE_URL' => 'http://www.eugenescottishfestival2012.com',
                'DOMAIN' => 'eugenescottishfestival2012.com',
            ),
            'insidedisneyworld.com' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_1_83441_',
                'SITE_URL' => 'http://www.insidedisneyworld.com',
                'DOMAIN' => 'insidedisneyworld.com',
            ),
            'oasistradingco.com' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_1_83442_',
                'SITE_URL' => 'http://www.oasistradingco.com',
                'DOMAIN' => 'oasistradingco.com',
            ),
            'scott-thomas.com' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_1_83443_',
                'SITE_URL' => 'http://www.scott-thomas.com',
                'DOMAIN' => 'scott-thomas.com',
            ),
            'alpacabase.com' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_1_83444_',
                'SITE_URL' => 'http://www.alpacabase.com',
                'DOMAIN' => 'alpacabase.com',
            ),
            'alwayshorny.net' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_1_83445_',
                'SITE_URL' => 'http://www.alwayshorny.net',
                'DOMAIN' => 'alwayshorny.net',
            ),
            'rottingcorpse.net' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1064_',
                'SITE_URL' => 'http://www.rottingcorpse.net',
                'DOMAIN' => 'rottingcorpse.net',
            ),
            'eatologies.com' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_470_',
                'SITE_URL' => 'http://www.eatologies.com',
                'DOMAIN' => 'eatologies.com',
            ),
            'exposedacnereviews.org' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_471_',
                'SITE_URL' => 'http://www.exposedacnereviews.org',
                'DOMAIN' => 'exposedacnereviews.org',
            ),
            'facemerk.com' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_472_',
                'SITE_URL' => 'http://www.facemerk.com',
                'DOMAIN' => 'facemerk.com',
            ),
            'femsa.us' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_473_',
                'SITE_URL' => 'http://www.femsa.us',
                'DOMAIN' => 'femsa.us',
            ),
            'mortgagefocus.org' =>
            array(
                'TABLE_SCHEMA' => 'insidedi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_555_',
                'SITE_URL' => 'http://www.mortgagefocus.org',
                'DOMAIN' => 'mortgagefocus.org',
            ),
            'insoccer.org' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_1_83742_',
                'SITE_URL' => 'http://www.insoccer.org',
                'DOMAIN' => 'insoccer.org',
            ),
            'busselton-accommodation.com' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_1_83743_',
                'SITE_URL' => 'http://www.busselton-accommodation.com',
                'DOMAIN' => 'busselton-accommodation.com',
            ),
            'victorypoker.biz' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_1_83744_',
                'SITE_URL' => 'http://www.victorypoker.biz',
                'DOMAIN' => 'victorypoker.biz',
            ),
            'golfshoescloseouts.net' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_1_83745_',
                'SITE_URL' => 'http://www.golfshoescloseouts.net',
                'DOMAIN' => 'golfshoescloseouts.net',
            ),
            'glandcs.com' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_1_83746_',
                'SITE_URL' => 'http://www.glandcs.com',
                'DOMAIN' => 'glandcs.com',
            ),
            'londonseomarketing.com' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_537_',
                'SITE_URL' => 'http://www.londonseomarketing.com',
                'DOMAIN' => 'londonseomarketing.com',
            ),
            'richardkainmarketing.com' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_556_',
                'SITE_URL' => 'http://www.richardkainmarketing.com',
                'DOMAIN' => 'richardkainmarketing.com',
            ),
            'powerbiblechurch.org' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_561_',
                'SITE_URL' => 'http://www.powerbiblechurch.org',
                'DOMAIN' => 'powerbiblechurch.org',
            ),
            'radianthomeloans.com' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_567_',
                'SITE_URL' => 'http://www.radianthomeloans.com',
                'DOMAIN' => 'radianthomeloans.com',
            ),
            'rockthemansion2012.com' =>
            array(
                'TABLE_SCHEMA' => 'insoccer_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_571_',
                'SITE_URL' => 'http://www.rockthemansion2012.com',
                'DOMAIN' => 'rockthemansion2012.com',
            ),
            'inspirationofahollywooditboy.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_1_81486_',
                'SITE_URL' => 'http://www.inspirationofahollywooditboy.com',
                'DOMAIN' => 'inspirationofahollywooditboy.com',
            ),
            'lyricvillerecords.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_1_81487_',
                'SITE_URL' => 'http://www.lyricvillerecords.com',
                'DOMAIN' => 'lyricvillerecords.com',
            ),
            'meijia-music.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_1_81488_',
                'SITE_URL' => 'http://www.meijia-music.com',
                'DOMAIN' => 'meijia-music.com',
            ),
            'neuralstudios.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1059_',
                'SITE_URL' => 'http://www.neuralstudios.com',
                'DOMAIN' => 'neuralstudios.com',
            ),
            'realfitnessresults.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1060_',
                'SITE_URL' => 'http://www.realfitnessresults.com',
                'DOMAIN' => 'realfitnessresults.com',
            ),
            'bombedoutradio.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_435_',
                'SITE_URL' => 'http://www.bombedoutradio.com',
                'DOMAIN' => 'bombedoutradio.com',
            ),
            'c8social.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_441_',
                'SITE_URL' => 'http://www.c8social.com',
                'DOMAIN' => 'c8social.com',
            ),
            'cleanol.mobi' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_451_',
                'SITE_URL' => 'http://www.cleanol.mobi',
                'DOMAIN' => 'cleanol.mobi',
            ),
            'brainevolution.org' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_458_',
                'SITE_URL' => 'http://www.brainevolution.org',
                'DOMAIN' => 'brainevolution.org',
            ),
            'sinfullywholesomearganoil.com' =>
            array(
                'TABLE_SCHEMA' => 'inspirat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_462_',
                'SITE_URL' => 'http://www.sinfullywholesomearganoil.com',
                'DOMAIN' => 'sinfullywholesomearganoil.com',
            ),
            'thepurpleelephantantiques.com' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1061_',
                'SITE_URL' => 'http://www.thepurpleelephantantiques.com',
                'DOMAIN' => 'thepurpleelephantantiques.com',
            ),
            'avtarinc.com' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1062_',
                'SITE_URL' => 'http://www.avtarinc.com',
                'DOMAIN' => 'avtarinc.com',
            ),
            'deitchwoodwindworkshop.com' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_445_',
                'SITE_URL' => 'http://www.deitchwoodwindworkshop.com',
                'DOMAIN' => 'deitchwoodwindworkshop.com',
            ),
            'fujairahmediaacademy.com' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_452_',
                'SITE_URL' => 'http://www.fujairahmediaacademy.com',
                'DOMAIN' => 'fujairahmediaacademy.com',
            ),
            'financialleadership.net' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_456_',
                'SITE_URL' => 'http://www.financialleadership.net',
                'DOMAIN' => 'financialleadership.net',
            ),
            'cnohaiti.org' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_459_',
                'SITE_URL' => 'http://www.cnohaiti.org',
                'DOMAIN' => 'cnohaiti.org',
            ),
            'shopquench.com' =>
            array(
                'TABLE_SCHEMA' => 'internat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_465_',
                'SITE_URL' => 'http://www.shopquench.com',
                'DOMAIN' => 'shopquench.com',
            ),
            'internationalmissions.org' =>
            array(
                'TABLE_SCHEMA' => 'internat_wp360',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.internationalmissions.org',
                'DOMAIN' => 'internationalmissions.org',
            ),
            'jeffersondanceparty.info' =>
            array(
                'TABLE_SCHEMA' => 'internat_wp468',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.jeffersondanceparty.info',
                'DOMAIN' => 'jeffersondanceparty.info',
            ),
            'it-hurts.com' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_1_82790_',
                'SITE_URL' => 'http://www.it-hurts.com',
                'DOMAIN' => 'it-hurts.com',
            ),
            'jessicachristine.com' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_1_82791_',
                'SITE_URL' => 'http://www.jessicachristine.com',
                'DOMAIN' => 'jessicachristine.com',
            ),
            'juliajj.com' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_1_82792_',
                'SITE_URL' => 'http://www.juliajj.com',
                'DOMAIN' => 'juliajj.com',
            ),
            'karenlyn.com' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_1_82793_',
                'SITE_URL' => 'http://www.karenlyn.com',
                'DOMAIN' => 'karenlyn.com',
            ),
            'greenwichconstruct.com' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1055_',
                'SITE_URL' => 'http://www.greenwichconstruct.com',
                'DOMAIN' => 'greenwichconstruct.com',
            ),
            'flowercompanyinc.com' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1056_',
                'SITE_URL' => 'http://www.flowercompanyinc.com',
                'DOMAIN' => 'flowercompanyinc.com',
            ),
            'wearethefuture.us' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_436_',
                'SITE_URL' => 'http://www.wearethefuture.us',
                'DOMAIN' => 'wearethefuture.us',
            ),
            'thewunder.org' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_438_',
                'SITE_URL' => 'http://www.thewunder.org',
                'DOMAIN' => 'thewunder.org',
            ),
            'thewomaninthechild.info' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_439_',
                'SITE_URL' => 'http://www.thewomaninthechild.info',
                'DOMAIN' => 'thewomaninthechild.info',
            ),
            'verizonwirelessstores.net' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_440_',
                'SITE_URL' => 'http://www.verizonwirelessstores.net',
                'DOMAIN' => 'verizonwirelessstores.net',
            ),
            'occupyfairhope.com' =>
            array(
                'TABLE_SCHEMA' => 'internet_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_460_',
                'SITE_URL' => 'http://www.occupyfairhope.com',
                'DOMAIN' => 'occupyfairhope.com',
            ),
            'acculinario.com' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wpK_142_',
                'SITE_URL' => 'http://www.acculinario.com',
                'DOMAIN' => 'acculinario.com',
            ),
            'invertebrate.ws' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_1_83738_',
                'SITE_URL' => 'http://www.invertebrate.ws',
                'DOMAIN' => 'invertebrate.ws',
            ),
            'bodabolivia.com' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_1_83739_',
                'SITE_URL' => 'http://www.bodabolivia.com',
                'DOMAIN' => 'bodabolivia.com',
            ),
            'bodaecuador.com' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_1_83740_',
                'SITE_URL' => 'http://www.bodaecuador.com',
                'DOMAIN' => 'bodaecuador.com',
            ),
            'your-pets-health-care.com' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1047_',
                'SITE_URL' => 'http://www.your-pets-health-care.com',
                'DOMAIN' => 'your-pets-health-care.com',
            ),
            'rainfireprotection.com' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1459_',
                'SITE_URL' => 'http://www.rainfireprotection.com',
                'DOMAIN' => 'rainfireprotection.com',
            ),
            'portervilleevents.com' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_392_',
                'SITE_URL' => 'http://www.portervilleevents.com',
                'DOMAIN' => 'portervilleevents.com',
            ),
            'poklibpcc.org' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_418_',
                'SITE_URL' => 'http://www.poklibpcc.org',
                'DOMAIN' => 'poklibpcc.org',
            ),
            'perfectpartyplannerfl.com' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_422_',
                'SITE_URL' => 'http://www.perfectpartyplannerfl.com',
                'DOMAIN' => 'perfectpartyplannerfl.com',
            ),
            'moodyafbchapel.org' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_453_',
                'SITE_URL' => 'http://www.moodyafbchapel.org',
                'DOMAIN' => 'moodyafbchapel.org',
            ),
            'moodychapel.org' =>
            array(
                'TABLE_SCHEMA' => 'inverteb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_454_',
                'SITE_URL' => 'http://www.moodychapel.org',
                'DOMAIN' => 'moodychapel.org',
            ),
            'israelisreal.net' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_1_83411_',
                'SITE_URL' => 'http://www.israelisreal.net',
                'DOMAIN' => 'israelisreal.net',
            ),
            'randyswanson.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_1_83412_',
                'SITE_URL' => 'http://www.randyswanson.com',
                'DOMAIN' => 'randyswanson.com',
            ),
            'irisproductions.org' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_1_83413_',
                'SITE_URL' => 'http://www.irisproductions.org',
                'DOMAIN' => 'irisproductions.org',
            ),
            'idabnk.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_1_83414_',
                'SITE_URL' => 'http://www.idabnk.com',
                'DOMAIN' => 'idabnk.com',
            ),
            'soho-businessclub.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_1_83415_',
                'SITE_URL' => 'http://www.soho-businessclub.com',
                'DOMAIN' => 'soho-businessclub.com',
            ),
            'horsetackforyou.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1057_',
                'SITE_URL' => 'http://www.horsetackforyou.com',
                'DOMAIN' => 'horsetackforyou.com',
            ),
            'mp3indirek.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1058_',
                'SITE_URL' => 'http://www.mp3indirek.com',
                'DOMAIN' => 'mp3indirek.com',
            ),
            'yhmap1129.info' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_415_',
                'SITE_URL' => 'http://www.yhmap1129.info',
                'DOMAIN' => 'yhmap1129.info',
            ),
            'optika-sasa.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_442_',
                'SITE_URL' => 'http://www.optika-sasa.com',
                'DOMAIN' => 'optika-sasa.com',
            ),
            'adventistwomensblog.org' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_443_',
                'SITE_URL' => 'http://www.adventistwomensblog.org',
                'DOMAIN' => 'adventistwomensblog.org',
            ),
            'ynsee.com' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_447_',
                'SITE_URL' => 'http://www.ynsee.com',
                'DOMAIN' => 'ynsee.com',
            ),
            'alexdavis.us' =>
            array(
                'TABLE_SCHEMA' => 'israelis_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_457_',
                'SITE_URL' => 'http://www.alexdavis.us',
                'DOMAIN' => 'alexdavis.us',
            ),
            'itaohuahui.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_1_83071_',
                'SITE_URL' => 'http://www.itaohuahui.com',
                'DOMAIN' => 'itaohuahui.com',
            ),
            'ji-nan-ban-zheng.info' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_1_83072_',
                'SITE_URL' => 'http://www.ji-nan-ban-zheng.info',
                'DOMAIN' => 'ji-nan-ban-zheng.info',
            ),
            'juan-sebastian-ramirez.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_1_83073_',
                'SITE_URL' => 'http://www.juan-sebastian-ramirez.com',
                'DOMAIN' => 'juan-sebastian-ramirez.com',
            ),
            'lalecheraverano.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_1_83074_',
                'SITE_URL' => 'http://www.lalecheraverano.com',
                'DOMAIN' => 'lalecheraverano.com',
            ),
            'ledjiancai.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_1_83075_',
                'SITE_URL' => 'http://www.ledjiancai.com',
                'DOMAIN' => 'ledjiancai.com',
            ),
            'eqxdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1054_',
                'SITE_URL' => 'http://www.eqxdesign.com',
                'DOMAIN' => 'eqxdesign.com',
            ),
            'trabajosyexamenes.net' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_430_',
                'SITE_URL' => 'http://www.trabajosyexamenes.net',
                'DOMAIN' => 'trabajosyexamenes.net',
            ),
            'thenoahsphere.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_431_',
                'SITE_URL' => 'http://www.thenoahsphere.com',
                'DOMAIN' => 'thenoahsphere.com',
            ),
            'thesinghs.biz' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_437_',
                'SITE_URL' => 'http://www.thesinghs.biz',
                'DOMAIN' => 'thesinghs.biz',
            ),
            'web-hero.net' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_446_',
                'SITE_URL' => 'http://www.web-hero.net',
                'DOMAIN' => 'web-hero.net',
            ),
            'ntierdata.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_464_',
                'SITE_URL' => 'http://www.ntierdata.com',
                'DOMAIN' => 'ntierdata.com',
            ),
            'vanguardoutdoors.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_wp229',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://vanguardoutdoors.com',
                'DOMAIN' => 'vanguardoutdoors.com',
            ),
            'nickfederoff.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_wp31',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://nickfederoff.com',
                'DOMAIN' => 'nickfederoff.com',
            ),
            'freshsouth.org' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_wp587',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://freshsouth.org',
                'DOMAIN' => 'freshsouth.org',
            ),
            'fritz-andre.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_wp615',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://fritz-andre.com',
                'DOMAIN' => 'fritz-andre.com',
            ),
            'fundamentalflooring.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_wp63',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://fundamentalflooring.com',
                'DOMAIN' => 'fundamentalflooring.com',
            ),
            'townofheathsprings.org' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_wp640',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://townofheathsprings.org',
                'DOMAIN' => 'townofheathsprings.org',
            ),
            'fiberglass-pools.us' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_wp653',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://fiberglass-pools.us',
                'DOMAIN' => 'fiberglass-pools.us',
            ),
            'freekickbowling.com' =>
            array(
                'TABLE_SCHEMA' => 'itaohuah_wp874',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://freekickbowling.com',
                'DOMAIN' => 'freekickbowling.com',
            ),
            'iwhm2011.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_1_83682_',
                'SITE_URL' => 'http://www.iwhm2011.com',
                'DOMAIN' => 'iwhm2011.com',
            ),
            'cambridgecommunityofrealtors.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_1_83683_',
                'SITE_URL' => 'http://www.cambridgecommunityofrealtors.com',
                'DOMAIN' => 'cambridgecommunityofrealtors.com',
            ),
            'glovesbyvenus.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_1_83684_',
                'SITE_URL' => 'http://www.glovesbyvenus.com',
                'DOMAIN' => 'glovesbyvenus.com',
            ),
            'chaletbrossard.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_1_83685_',
                'SITE_URL' => 'http://www.chaletbrossard.com',
                'DOMAIN' => 'chaletbrossard.com',
            ),
            '020good.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_1_83686_',
                'SITE_URL' => 'http://www.020good.com',
                'DOMAIN' => '020good.com',
            ),
            'comebackin.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_42_',
                'SITE_URL' => 'http://www.comebackin.com',
                'DOMAIN' => 'comebackin.com',
            ),
            'savannahervin.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_43_',
                'SITE_URL' => 'http://www.savannahervin.com',
                'DOMAIN' => 'savannahervin.com',
            ),
            'fishingcanadablog.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_44_',
                'SITE_URL' => 'http://www.fishingcanadablog.com',
                'DOMAIN' => 'fishingcanadablog.com',
            ),
            'teabeans.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_45_',
                'SITE_URL' => 'http://www.teabeans.com',
                'DOMAIN' => 'teabeans.com',
            ),
            '1000plus.me' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_46_',
                'SITE_URL' => 'http://www.1000plus.me',
                'DOMAIN' => '1000plus.me',
            ),
            'questionsandcocktails.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_47_',
                'SITE_URL' => 'http://www.questionsandcocktails.com',
                'DOMAIN' => 'questionsandcocktails.com',
            ),
            'terroirstudio.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_wp59',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://terroirstudio.com',
                'DOMAIN' => 'terroirstudio.com',
            ),
            'aguilasdemexico.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1052_',
                'SITE_URL' => 'http://www.aguilasdemexico.com',
                'DOMAIN' => 'aguilasdemexico.com',
            ),
            'lifeandlivingcoaching.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1053_',
                'SITE_URL' => 'http://www.lifeandlivingcoaching.com',
                'DOMAIN' => 'lifeandlivingcoaching.com',
            ),
            'xn--eckvc2a7gj5g6832ahd9e.net' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_427_',
                'SITE_URL' => 'http://www.xn--eckvc2a7gj5g6832ahd9e.net',
                'DOMAIN' => 'xn--eckvc2a7gj5g6832ahd9e.net',
            ),
            'venezuelaspeaks.org' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_429_',
                'SITE_URL' => 'http://www.venezuelaspeaks.org',
                'DOMAIN' => 'venezuelaspeaks.org',
            ),
            'victoryforchangeparty.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_432_',
                'SITE_URL' => 'http://www.victoryforchangeparty.com',
                'DOMAIN' => 'victoryforchangeparty.com',
            ),
            'zahrady-fotogalerie.info' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_433_',
                'SITE_URL' => 'http://www.zahrady-fotogalerie.info',
                'DOMAIN' => 'zahrady-fotogalerie.info',
            ),
            'zen1th.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_434_',
                'SITE_URL' => 'http://www.zen1th.com',
                'DOMAIN' => 'zen1th.com',
            ),
            'kennethorbeckdo.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_wp41',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://kennethorbeckdo.com',
                'DOMAIN' => 'kennethorbeckdo.com',
            ),
            'drmorisset.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_wp772',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://drmorisset.com',
                'DOMAIN' => 'drmorisset.com',
            ),
            'boothafricabusiness12.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_wp841',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://boothafricabusiness12.com',
                'DOMAIN' => 'boothafricabusiness12.com',
            ),
            'cleantechtospacetech.com' =>
            array(
                'TABLE_SCHEMA' => 'iwhm2011_wp867',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://cleantechtospacetech.com',
                'DOMAIN' => 'cleantechtospacetech.com',
            ),
            'baryochai.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_1_81619_',
                'SITE_URL' => 'http://www.baryochai.com',
                'DOMAIN' => 'baryochai.com',
            ),
            'izolli.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_1_82826_',
                'SITE_URL' => 'http://www.izolli.com',
                'DOMAIN' => 'izolli.com',
            ),
            'beretaybeauty.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_1_82827_',
                'SITE_URL' => 'http://www.beretaybeauty.com',
                'DOMAIN' => 'beretaybeauty.com',
            ),
            'booom.mobi' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_1_82828_',
                'SITE_URL' => 'http://www.booom.mobi',
                'DOMAIN' => 'booom.mobi',
            ),
            'bcforms.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_17_',
                'SITE_URL' => 'http://bcforms.com',
                'DOMAIN' => 'bcforms.com',
            ),
            'bike-gear.ws' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_20_',
                'SITE_URL' => 'http://bike-gear.ws',
                'DOMAIN' => 'bike-gear.ws',
            ),
            'blackdragonarts.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_21_',
                'SITE_URL' => 'http://blackdragonarts.com',
                'DOMAIN' => 'blackdragonarts.com',
            ),
            'bigmessmedia.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_4_',
                'SITE_URL' => 'http://www.bigmessmedia.com',
                'DOMAIN' => 'bigmessmedia.com',
            ),
            'ritzandrevelry.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_410_',
                'SITE_URL' => 'http://www.ritzandrevelry.com',
                'DOMAIN' => 'ritzandrevelry.com',
            ),
            'roofingdistributor.biz' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_416_',
                'SITE_URL' => 'http://www.roofingdistributor.biz',
                'DOMAIN' => 'roofingdistributor.biz',
            ),
            'recyclobin.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_419_',
                'SITE_URL' => 'http://www.recyclobin.com',
                'DOMAIN' => 'recyclobin.com',
            ),
            'new-bmw-335d-sedan.info' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_455_',
                'SITE_URL' => 'http://www.new-bmw-335d-sedan.info',
                'DOMAIN' => 'new-bmw-335d-sedan.info',
            ),
            'njsafes.com' =>
            array(
                'TABLE_SCHEMA' => 'izollico_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_463_',
                'SITE_URL' => 'http://www.njsafes.com',
                'DOMAIN' => 'njsafes.com',
            ),
            'china-ecommerce-logistics.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1157_',
                'SITE_URL' => 'http://www.china-ecommerce-logistics.com',
                'DOMAIN' => 'china-ecommerce-logistics.com',
            ),
            'crossfit-rio.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1163_',
                'SITE_URL' => 'http://www.crossfit-rio.com',
                'DOMAIN' => 'crossfit-rio.com',
            ),
            'jabirds.org' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_851_',
                'SITE_URL' => 'http://www.jabirds.org',
                'DOMAIN' => 'jabirds.org',
            ),
            'gpcounselorsforchange.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_852_',
                'SITE_URL' => 'http://www.gpcounselorsforchange.com',
                'DOMAIN' => 'gpcounselorsforchange.com',
            ),
            'richardtaylorfor50thdistrictjudge.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_853_',
                'SITE_URL' => 'http://www.richardtaylorfor50thdistrictjudge.com',
                'DOMAIN' => 'richardtaylorfor50thdistrictjudge.com',
            ),
            'createmyfirstwebsite.com' =>
            array(
                'TABLE_SCHEMA' => 'jabirdso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_855_',
                'SITE_URL' => 'http://www.createmyfirstwebsite.com',
                'DOMAIN' => 'createmyfirstwebsite.com',
            ),
            'jailnbailutd.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_jailnbu',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://jailnbailutd.com',
                'DOMAIN' => 'jailnbailutd.com',
            ),
            'mongolhuraldai.org' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_mongolh',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://mongolhuraldai.org',
                'DOMAIN' => 'mongolhuraldai.org',
            ),
            'a1bargainprice.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1050_',
                'SITE_URL' => 'http://www.a1bargainprice.com',
                'DOMAIN' => 'a1bargainprice.com',
            ),
            'hqsoftconsult.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1051_',
                'SITE_URL' => 'http://www.hqsoftconsult.com',
                'DOMAIN' => 'hqsoftconsult.com',
            ),
            'uwclubhockey.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_400_',
                'SITE_URL' => 'http://www.uwclubhockey.com',
                'DOMAIN' => 'uwclubhockey.com',
            ),
            'tacotrail.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_411_',
                'SITE_URL' => 'http://www.tacotrail.com',
                'DOMAIN' => 'tacotrail.com',
            ),
            'thejudassyndrome.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_421_',
                'SITE_URL' => 'http://www.thejudassyndrome.com',
                'DOMAIN' => 'thejudassyndrome.com',
            ),
            'towelbathrobe.net' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_424_',
                'SITE_URL' => 'http://www.towelbathrobe.net',
                'DOMAIN' => 'towelbathrobe.net',
            ),
            'uscpfa-wr.org' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_428_',
                'SITE_URL' => 'http://www.uscpfa-wr.org',
                'DOMAIN' => 'uscpfa-wr.org',
            ),
            'middleware101.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_wp454',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://middleware101.com',
                'DOMAIN' => 'middleware101.com',
            ),
            'miskimina.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_wp489',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://miskimina.com',
                'DOMAIN' => 'miskimina.com',
            ),
            'motobarn.com' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_wp650',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://motobarn.com',
                'DOMAIN' => 'motobarn.com',
            ),
            'mnjhl.org' =>
            array(
                'TABLE_SCHEMA' => 'jailnbai_wp82',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://mnjhl.org',
                'DOMAIN' => 'mnjhl.org',
            ),
            'jeremiahstanleyphotography.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_1_83877_',
                'SITE_URL' => 'http://www.jeremiahstanleyphotography.com',
                'DOMAIN' => 'jeremiahstanleyphotography.com',
            ),
            'limitedsprints.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_1_83878_',
                'SITE_URL' => 'http://www.limitedsprints.com',
                'DOMAIN' => 'limitedsprints.com',
            ),
            'yhteisjarjesto.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_wp453',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.yhteisjarjesto.com',
                'DOMAIN' => 'yhteisjarjesto.com',
            ),
            'lkcc.info' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_1_83880_',
                'SITE_URL' => 'http://www.lkcc.info',
                'DOMAIN' => 'lkcc.info',
            ),
            'phoneleadsuccess.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_1_83881_',
                'SITE_URL' => 'http://www.phoneleadsuccess.com',
                'DOMAIN' => 'phoneleadsuccess.com',
            ),
            'markvasakat.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_707_',
                'SITE_URL' => 'http://www.markvasakat.com',
                'DOMAIN' => 'markvasakat.com',
            ),
            'reflectionsoflifephotography.org' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_708_',
                'SITE_URL' => 'http://www.reflectionsoflifephotography.org',
                'DOMAIN' => 'reflectionsoflifephotography.org',
            ),
            'jussphotos.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_709_',
                'SITE_URL' => 'http://www.jussphotos.com',
                'DOMAIN' => 'jussphotos.com',
            ),
            'alisapeek.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_710_',
                'SITE_URL' => 'http://www.alisapeek.com',
                'DOMAIN' => 'alisapeek.com',
            ),
            'matthewbraney.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_711_',
                'SITE_URL' => 'http://www.matthewbraney.com',
                'DOMAIN' => 'matthewbraney.com',
            ),
            'elections2010.info' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1049_',
                'SITE_URL' => 'http://www.elections2010.info',
                'DOMAIN' => 'elections2010.info',
            ),
            'stellasavestheday.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_414_',
                'SITE_URL' => 'http://www.stellasavestheday.com',
                'DOMAIN' => 'stellasavestheday.com',
            ),
            'suclumuyum.org' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_420_',
                'SITE_URL' => 'http://www.suclumuyum.org',
                'DOMAIN' => 'suclumuyum.org',
            ),
            'scsgschools.org' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_423_',
                'SITE_URL' => 'http://www.scsgschools.org',
                'DOMAIN' => 'scsgschools.org',
            ),
            'sissycollective.org' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_425_',
                'SITE_URL' => 'http://www.sissycollective.org',
                'DOMAIN' => 'sissycollective.org',
            ),
            'soularch.info' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_426_',
                'SITE_URL' => 'http://www.soularch.info',
                'DOMAIN' => 'soularch.info',
            ),
            'rebeccasilver.net' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_wp100',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://rebeccasilver.net',
                'DOMAIN' => 'rebeccasilver.net',
            ),
            'dreamlistradio.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_wp222',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://dreamlistradio.com',
                'DOMAIN' => 'dreamlistradio.com',
            ),
            'ctconnections.org' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_wp63',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ctconnections.org',
                'DOMAIN' => 'ctconnections.org',
            ),
            'livingstonesound.info' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_wp928',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://livingstonesound.info',
                'DOMAIN' => 'livingstonesound.info',
            ),
            'mabln.com' =>
            array(
                'TABLE_SCHEMA' => 'jeremiah_wp93',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://mabln.com',
                'DOMAIN' => 'mabln.com',
            ),
            'jestermg.com' =>
            array(
                'TABLE_SCHEMA' => 'jestermg_tkp',
                'TABLE_PREFIX' => 'wp_1_82966_',
                'SITE_URL' => 'http://www.jestermg.com',
                'DOMAIN' => 'jestermg.com',
            ),
            'jhnartestudio.com' =>
            array(
                'TABLE_SCHEMA' => 'jestermg_tkp',
                'TABLE_PREFIX' => 'wp_1_82967_',
                'SITE_URL' => 'http://www.jhnartestudio.com',
                'DOMAIN' => 'jhnartestudio.com',
            ),
            'jls-group.com' =>
            array(
                'TABLE_SCHEMA' => 'jestermg_tkp',
                'TABLE_PREFIX' => 'wp_1_82968_',
                'SITE_URL' => 'http://www.jls-group.com',
                'DOMAIN' => 'jls-group.com',
            ),
            'jobsinbernalillocounty.com' =>
            array(
                'TABLE_SCHEMA' => 'jestermg_tkp',
                'TABLE_PREFIX' => 'wp_1_82969_',
                'SITE_URL' => 'http://www.jobsinbernalillocounty.com',
                'DOMAIN' => 'jobsinbernalillocounty.com',
            ),
            'jobsinbronxcounty.com' =>
            array(
                'TABLE_SCHEMA' => 'jestermg_tkp',
                'TABLE_PREFIX' => 'wp_1_82970_',
                'SITE_URL' => 'http://www.jobsinbronxcounty.com',
                'DOMAIN' => 'jobsinbronxcounty.com',
            ),
            'sleep123beds.com' =>
            array(
                'TABLE_SCHEMA' => 'jestermg_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_134_',
                'SITE_URL' => 'http://sleep123beds.com',
                'DOMAIN' => 'sleep123beds.com',
            ),
            'smashinglaptops.org' =>
            array(
                'TABLE_SCHEMA' => 'jestermg_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_135_',
                'SITE_URL' => 'http://smashinglaptops.org',
                'DOMAIN' => 'smashinglaptops.org',
            ),
            'something-dope.com' =>
            array(
                'TABLE_SCHEMA' => 'jestermg_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_136_',
                'SITE_URL' => 'http://www.something-dope.com',
                'DOMAIN' => 'something-dope.com',
            ),
            'sunfoodcashews.com' =>
            array(
                'TABLE_SCHEMA' => 'jestermg_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_137_',
                'SITE_URL' => 'http://sunfoodcashews.com',
                'DOMAIN' => 'sunfoodcashews.com',
            ),
            'jezam.biz' =>
            array(
                'TABLE_SCHEMA' => 'jezam_tkp',
                'TABLE_PREFIX' => 'wp_pkp1_1729_',
                'SITE_URL' => 'http://www.jezam.biz',
                'DOMAIN' => 'jezam.biz',
            ),
            'jobsineriecounty.com' =>
            array(
                'TABLE_SCHEMA' => 'jobsiner_tkp',
                'TABLE_PREFIX' => 'wp_1_82971_',
                'SITE_URL' => 'http://www.jobsineriecounty.com',
                'DOMAIN' => 'jobsineriecounty.com',
            ),
            'jobsinessexcounty.com' =>
            array(
                'TABLE_SCHEMA' => 'jobsiner_tkp',
                'TABLE_PREFIX' => 'wp_1_82972_',
                'SITE_URL' => 'http://www.jobsinessexcounty.com',
                'DOMAIN' => 'jobsinessexcounty.com',
            ),
            'jobsinqueenscounty.com' =>
            array(
                'TABLE_SCHEMA' => 'jobsiner_tkp',
                'TABLE_PREFIX' => 'wp_1_82973_',
                'SITE_URL' => 'http://www.jobsinqueenscounty.com',
                'DOMAIN' => 'jobsinqueenscounty.com',
            ),
            'kaddishlifeline.org' =>
            array(
                'TABLE_SCHEMA' => 'jobsiner_tkp',
                'TABLE_PREFIX' => 'wp_1_82974_',
                'SITE_URL' => 'http://www.kaddishlifeline.org',
                'DOMAIN' => 'kaddishlifeline.org',
            ),
            'kineticpark.com' =>
            array(
                'TABLE_SCHEMA' => 'jobsiner_tkp',
                'TABLE_PREFIX' => 'wp_1_82975_',
                'SITE_URL' => 'http://www.kineticpark.com',
                'DOMAIN' => 'kineticpark.com',
            ),
            'swimscore.com' =>
            array(
                'TABLE_SCHEMA' => 'jobsiner_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_138_',
                'SITE_URL' => 'http://www.swimscore.com',
                'DOMAIN' => 'swimscore.com',
            ),
            'takeitlikeahusband.com' =>
            array(
                'TABLE_SCHEMA' => 'jobsiner_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_139_',
                'SITE_URL' => 'http://www.takeitlikeahusband.com',
                'DOMAIN' => 'takeitlikeahusband.com',
            ),
            'tateuri.biz' =>
            array(
                'TABLE_SCHEMA' => 'jobsiner_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_140_',
                'SITE_URL' => 'http://tateuri.biz',
                'DOMAIN' => 'tateuri.biz',
            ),
            'tensasprogress.com' =>
            array(
                'TABLE_SCHEMA' => 'jobsiner_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_141_',
                'SITE_URL' => 'http://www.tensasprogress.com',
                'DOMAIN' => 'tensasprogress.com',
            ),
            'justlikethieves.com' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_1_81137_',
                'SITE_URL' => 'http://www.justlikethieves.com',
                'DOMAIN' => 'justlikethieves.com',
            ),
            'jumpcannon.com' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_1_81187_',
                'SITE_URL' => 'http://www.jumpcannon.com',
                'DOMAIN' => 'jumpcannon.com',
            ),
            'karendavid.info' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_1_81188_',
                'SITE_URL' => 'http://www.karendavid.info',
                'DOMAIN' => 'karendavid.info',
            ),
            'karmanos-crittenton.org' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_1_81189_',
                'SITE_URL' => 'http://www.karmanos-crittenton.org',
                'DOMAIN' => 'karmanos-crittenton.org',
            ),
            'kevincome.com' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_1_81190_',
                'SITE_URL' => 'http://www.kevincome.com',
                'DOMAIN' => 'kevincome.com',
            ),
            'cheap-health-insurance-quote.com' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1045_',
                'SITE_URL' => 'http://www.cheap-health-insurance-quote.com',
                'DOMAIN' => 'cheap-health-insurance-quote.com',
            ),
            'tempehpf.com' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1046_',
                'SITE_URL' => 'http://www.tempehpf.com',
                'DOMAIN' => 'tempehpf.com',
            ),
            'ofmap1067.info' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_395_',
                'SITE_URL' => 'http://www.ofmap1067.info',
                'DOMAIN' => 'ofmap1067.info',
            ),
            'nonprofitpathfinder.org' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_407_',
                'SITE_URL' => 'http://www.nonprofitpathfinder.org',
                'DOMAIN' => 'nonprofitpathfinder.org',
            ),
            'osuntrc.org' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_408_',
                'SITE_URL' => 'http://www.osuntrc.org',
                'DOMAIN' => 'osuntrc.org',
            ),
            'ohiocea.biz' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_413_',
                'SITE_URL' => 'http://www.ohiocea.biz',
                'DOMAIN' => 'ohiocea.biz',
            ),
            'pattinson-online.com' =>
            array(
                'TABLE_SCHEMA' => 'jumpcann_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_417_',
                'SITE_URL' => 'http://www.pattinson-online.com',
                'DOMAIN' => 'pattinson-online.com',
            ),
            'thatonefilmblog.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1146_',
                'SITE_URL' => 'http://www.thatonefilmblog.com',
                'DOMAIN' => 'thatonefilmblog.com',
            ),
            '5libertyroad.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1147_',
                'SITE_URL' => 'http://www.5libertyroad.com',
                'DOMAIN' => '5libertyroad.com',
            ),
            'guardingheartsalliance.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1148_',
                'SITE_URL' => 'http://www.guardingheartsalliance.com',
                'DOMAIN' => 'guardingheartsalliance.com',
            ),
            'dewiyuhana.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1149_',
                'SITE_URL' => 'http://www.dewiyuhana.com',
                'DOMAIN' => 'dewiyuhana.com',
            ),
            'kamaymacollege.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_924_',
                'SITE_URL' => 'http://www.kamaymacollege.com',
                'DOMAIN' => 'kamaymacollege.com',
            ),
            'mykickasslife.me' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_925_',
                'SITE_URL' => 'http://www.mykickasslife.me',
                'DOMAIN' => 'mykickasslife.me',
            ),
            'marcelvreuls.com' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_926_',
                'SITE_URL' => 'http://www.marcelvreuls.com',
                'DOMAIN' => 'marcelvreuls.com',
            ),
            'zgwne.info' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_927_',
                'SITE_URL' => 'http://www.zgwne.info',
                'DOMAIN' => 'zgwne.info',
            ),
            'unitedafricancongress.org' =>
            array(
                'TABLE_SCHEMA' => 'kamaymac_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_928_',
                'SITE_URL' => 'http://www.unitedafricancongress.org',
                'DOMAIN' => 'unitedafricancongress.org',
            ),
            'katehennessystudio.com' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_1_83135_',
                'SITE_URL' => 'http://www.katehennessystudio.com',
                'DOMAIN' => 'katehennessystudio.com',
            ),
            'euroediting.com' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_1_83136_',
                'SITE_URL' => 'http://www.euroediting.com',
                'DOMAIN' => 'euroediting.com',
            ),
            'sellingdcin47days.com' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_1_83137_',
                'SITE_URL' => 'http://www.sellingdcin47days.com',
                'DOMAIN' => 'sellingdcin47days.com',
            ),
            'aquariumfilterbags.com' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_1_83138_',
                'SITE_URL' => 'http://www.aquariumfilterbags.com',
                'DOMAIN' => 'aquariumfilterbags.com',
            ),
            'puckethotels.com' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_270_',
                'SITE_URL' => 'http://puckethotels.com',
                'DOMAIN' => 'puckethotels.com',
            ),
            'pycca.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_271_',
                'SITE_URL' => 'http://www.pycca.info',
                'DOMAIN' => 'pycca.info',
            ),
            'ricoaviation.us' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_wp737',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.ricoaviation.us',
                'DOMAIN' => 'ricoaviation.us',
            ),
            'davidortegaphotography.com' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1041_',
                'SITE_URL' => 'http://www.davidortegaphotography.com',
                'DOMAIN' => 'davidortegaphotography.com',
            ),
            'baycolattorneys.com' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1042_',
                'SITE_URL' => 'http://www.baycolattorneys.com',
                'DOMAIN' => 'baycolattorneys.com',
            ),
            'nflatlantafalconsjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_398_',
                'SITE_URL' => 'http://www.nflatlantafalconsjerseysusa.info',
                'DOMAIN' => 'nflatlantafalconsjerseysusa.info',
            ),
            'nfldallascowboysjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_401_',
                'SITE_URL' => 'http://www.nfldallascowboysjerseysusa.info',
                'DOMAIN' => 'nfldallascowboysjerseysusa.info',
            ),
            'new-toyota-tacoma-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_402_',
                'SITE_URL' => 'http://www.new-toyota-tacoma-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-tacoma-rockland-county-ny.info',
            ),
            'new-toyota-venza-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_404_',
                'SITE_URL' => 'http://www.new-toyota-venza-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-venza-rockland-county-ny.info',
            ),
            'new-toyota-tundra-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'katehenn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_409_',
                'SITE_URL' => 'http://www.new-toyota-tundra-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-tundra-rockland-county-ny.info',
            ),
            'baycollawyer.com' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1043_',
                'SITE_URL' => 'http://www.baycollawyer.com',
                'DOMAIN' => 'baycollawyer.com',
            ),
            'binocularsonlineguide.com' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1044_',
                'SITE_URL' => 'http://www.binocularsonlineguide.com',
                'DOMAIN' => 'binocularsonlineguide.com',
            ),
            'nfltennesseetitansjerseysus.info' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1458_',
                'SITE_URL' => 'http://www.nfltennesseetitansjerseysus.info',
                'DOMAIN' => 'nfltennesseetitansjerseysus.info',
            ),
            'nfldetroitlionsjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_399_',
                'SITE_URL' => 'http://www.nfldetroitlionsjerseysusa.info',
                'DOMAIN' => 'nfldetroitlionsjerseysusa.info',
            ),
            'nflmiamidolphinsjerseysus.info' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_405_',
                'SITE_URL' => 'http://www.nflmiamidolphinsjerseysus.info',
                'DOMAIN' => 'nflmiamidolphinsjerseysus.info',
            ),
            'nflpittsburghsteelersjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_406_',
                'SITE_URL' => 'http://www.nflpittsburghsteelersjerseysusa.info',
                'DOMAIN' => 'nflpittsburghsteelersjerseysusa.info',
            ),
            'nflnewyorkjetsjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_412_',
                'SITE_URL' => 'http://www.nflnewyorkjetsjerseysusa.info',
                'DOMAIN' => 'nflnewyorkjetsjerseysusa.info',
            ),
            'maverickcountyprecinct1.com' =>
            array(
                'TABLE_SCHEMA' => 'kbdlhoop_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_461_',
                'SITE_URL' => 'http://www.maverickcountyprecinct1.com',
                'DOMAIN' => 'maverickcountyprecinct1.com',
            ),
            'kcsportschat.com' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_1_83543_',
                'SITE_URL' => 'http://www.kcsportschat.com',
                'DOMAIN' => 'kcsportschat.com',
            ),
            'leatherneckaircorp.com' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_1_83544_',
                'SITE_URL' => 'http://www.leatherneckaircorp.com',
                'DOMAIN' => 'leatherneckaircorp.com',
            ),
            'm2seward.com' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_1_83545_',
                'SITE_URL' => 'http://www.m2seward.com',
                'DOMAIN' => 'm2seward.com',
            ),
            'maddhowardcounty.org' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_1_83546_',
                'SITE_URL' => 'http://www.maddhowardcounty.org',
                'DOMAIN' => 'maddhowardcounty.org',
            ),
            'magicpoetrybus.com' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_1_83547_',
                'SITE_URL' => 'http://www.magicpoetrybus.com',
                'DOMAIN' => 'magicpoetrybus.com',
            ),
            'accomplisheddesigns.com' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1040_',
                'SITE_URL' => 'http://www.accomplisheddesigns.com',
                'DOMAIN' => 'accomplisheddesigns.com',
            ),
            'new-toyota-matrix-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_382_',
                'SITE_URL' => 'http://www.new-toyota-matrix-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-matrix-rockland-county-ny.info',
            ),
            'new-toyota-sequoia-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_390_',
                'SITE_URL' => 'http://www.new-toyota-sequoia-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-sequoia-rockland-county-ny.info',
            ),
            'new-toyota-prius-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_391_',
                'SITE_URL' => 'http://www.new-toyota-prius-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-prius-rockland-county-ny.info',
            ),
            'new-toyota-sienna-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_403_',
                'SITE_URL' => 'http://www.new-toyota-sienna-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-sienna-rockland-county-ny.info',
            ),
            'masschems.com' =>
            array(
                'TABLE_SCHEMA' => 'kcsports_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_449_',
                'SITE_URL' => 'http://www.masschems.com',
                'DOMAIN' => 'masschems.com',
            ),
            'kinfolks.biz' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_1_82886_',
                'SITE_URL' => 'http://www.kinfolks.biz',
                'DOMAIN' => 'kinfolks.biz',
            ),
            'krphe.org' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_1_82887_',
                'SITE_URL' => 'http://www.krphe.org',
                'DOMAIN' => 'krphe.org',
            ),
            'kurdcivil.com' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_1_82888_',
                'SITE_URL' => 'http://www.kurdcivil.com',
                'DOMAIN' => 'kurdcivil.com',
            ),
            'kutfuras.org' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_1_82889_',
                'SITE_URL' => 'http://www.kutfuras.org',
                'DOMAIN' => 'kutfuras.org',
            ),
            'leavealegacy.net' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_1_82890_',
                'SITE_URL' => 'http://www.leavealegacy.net',
                'DOMAIN' => 'leavealegacy.net',
            ),
            'i-love-psp.com' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_70_',
                'SITE_URL' => 'http://www.i-love-psp.com',
                'DOMAIN' => 'i-love-psp.com',
            ),
            'ibeinspired.com' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_71_',
                'SITE_URL' => 'http://www.ibeinspired.com',
                'DOMAIN' => 'ibeinspired.com',
            ),
            'idafladen.com' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_72_',
                'SITE_URL' => 'http://www.idafladen.com',
                'DOMAIN' => 'idafladen.com',
            ),
            'injury-lawyers-ny.com' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1038_',
                'SITE_URL' => 'http://www.injury-lawyers-ny.com',
                'DOMAIN' => 'injury-lawyers-ny.com',
            ),
            'outlawsaloon.org' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1039_',
                'SITE_URL' => 'http://www.outlawsaloon.org',
                'DOMAIN' => 'outlawsaloon.org',
            ),
            'new-toyota-fj-cruiser-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_393_',
                'SITE_URL' => 'http://www.new-toyota-fj-cruiser-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-fj-cruiser-rockland-county-ny.info',
            ),
            'new-toyota-camry-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_394_',
                'SITE_URL' => 'http://www.new-toyota-camry-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-camry-rockland-county-ny.info',
            ),
            'new-toyota-land-cruiser-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_396_',
                'SITE_URL' => 'http://www.new-toyota-land-cruiser-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-land-cruiser-rockland-county-ny.info',
            ),
            'new-toyota-corolla-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_397_',
                'SITE_URL' => 'http://www.new-toyota-corolla-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-corolla-rockland-county-ny.info',
            ),
            'new-toyota-highlander-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'kinfolks_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_468_',
                'SITE_URL' => 'http://www.new-toyota-highlander-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-highlander-rockland-county-ny.info',
            ),
            'lapinefiddlecamp.org' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_1_84332_',
                'SITE_URL' => 'http://www.lapinefiddlecamp.org',
                'DOMAIN' => 'lapinefiddlecamp.org',
            ),
            'miitonline.com' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_1_84333_',
                'SITE_URL' => 'http://www.miitonline.com',
                'DOMAIN' => 'miitonline.com',
            ),
            'mymegaphonemusic.com' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_1_84334_',
                'SITE_URL' => 'http://www.mymegaphonemusic.com',
                'DOMAIN' => 'mymegaphonemusic.com',
            ),
            'nevadayeswecan.info' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_1_84335_',
                'SITE_URL' => 'http://www.nevadayeswecan.info',
                'DOMAIN' => 'nevadayeswecan.info',
            ),
            'newportmenus.com' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_1_84336_',
                'SITE_URL' => 'http://www.newportmenus.com',
                'DOMAIN' => 'newportmenus.com',
            ),
            'fatherhoodonline.com' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1036_',
                'SITE_URL' => 'http://www.fatherhoodonline.com',
                'DOMAIN' => 'fatherhoodonline.com',
            ),
            'hostingandbeyond.com' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1037_',
                'SITE_URL' => 'http://www.hostingandbeyond.com',
                'DOMAIN' => 'hostingandbeyond.com',
            ),
            'nbadallasmavericks.info' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_376_',
                'SITE_URL' => 'http://www.nbadallasmavericks.info',
                'DOMAIN' => 'nbadallasmavericks.info',
            ),
            'nba-san-antonio-spurs-jerseys.info' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_378_',
                'SITE_URL' => 'http://www.nba-san-antonio-spurs-jerseys.info',
                'DOMAIN' => 'nba-san-antonio-spurs-jerseys.info',
            ),
            'nba-san-antonio-spurs.info' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_383_',
                'SITE_URL' => 'http://www.nba-san-antonio-spurs.info',
                'DOMAIN' => 'nba-san-antonio-spurs.info',
            ),
            'mykaef.org' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_387_',
                'SITE_URL' => 'http://www.mykaef.org',
                'DOMAIN' => 'mykaef.org',
            ),
            'new-toyota-avalon-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'lapinefi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_389_',
                'SITE_URL' => 'http://www.new-toyota-avalon-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-avalon-rockland-county-ny.info',
            ),
            'letskeepitbetweenus.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_1_84453_',
                'SITE_URL' => 'http://www.letskeepitbetweenus.com',
                'DOMAIN' => 'letskeepitbetweenus.com',
            ),
            'lijibo.org' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_1_84460_',
                'SITE_URL' => 'http://www.lijibo.org',
                'DOMAIN' => 'lijibo.org',
            ),
            'myafya.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_wp252',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.myafya.com',
                'DOMAIN' => 'myafya.com',
            ),
            'portoseguro360.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_1_84462_',
                'SITE_URL' => 'http://www.portoseguro360.com',
                'DOMAIN' => 'portoseguro360.com',
            ),
            'ren666.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_1_84463_',
                'SITE_URL' => 'http://www.ren666.com',
                'DOMAIN' => 'ren666.com',
            ),
            'healthyrecoveryformula.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1034_',
                'SITE_URL' => 'http://www.healthyrecoveryformula.com',
                'DOMAIN' => 'healthyrecoveryformula.com',
            ),
            'lockportwebdesign.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1035_',
                'SITE_URL' => 'http://www.lockportwebdesign.com',
                'DOMAIN' => 'lockportwebdesign.com',
            ),
            'soundforus.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_380_',
                'SITE_URL' => 'http://www.soundforus.com',
                'DOMAIN' => 'soundforus.com',
            ),
            'montrealtweetup.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_384_',
                'SITE_URL' => 'http://www.montrealtweetup.com',
                'DOMAIN' => 'montrealtweetup.com',
            ),
            'sugaranni.info' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_385_',
                'SITE_URL' => 'http://www.sugaranni.info',
                'DOMAIN' => 'sugaranni.info',
            ),
            'startingoutsideways.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_388_',
                'SITE_URL' => 'http://www.startingoutsideways.com',
                'DOMAIN' => 'startingoutsideways.com',
            ),
            'luxuryshoreproperties.biz' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_448_',
                'SITE_URL' => 'http://www.luxuryshoreproperties.biz',
                'DOMAIN' => 'luxuryshoreproperties.biz',
            ),
            'kscjbasements.com' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_wp192',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://kscjbasements.com',
                'DOMAIN' => 'kscjbasements.com',
            ),
            'chicasguapas.org' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_wp371',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://chicasguapas.org',
                'DOMAIN' => 'chicasguapas.org',
            ),
            'kg-env.org' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_wp432',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://kg-env.org',
                'DOMAIN' => 'kg-env.org',
            ),
            'metrogreen.us' =>
            array(
                'TABLE_SCHEMA' => 'letskeep_wp946',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://metrogreen.us',
                'DOMAIN' => 'metrogreen.us',
            ),
            'lighthousechurchofgod.org' =>
            array(
                'TABLE_SCHEMA' => 'lighthou_tkp',
                'TABLE_PREFIX' => 'wp_1_81673_',
                'SITE_URL' => 'http://www.lighthousechurchofgod.org',
                'DOMAIN' => 'lighthousechurchofgod.org',
            ),
            'lightningfest2010.com' =>
            array(
                'TABLE_SCHEMA' => 'lighthou_tkp',
                'TABLE_PREFIX' => 'wp_1_81674_',
                'SITE_URL' => 'http://www.lightningfest2010.com',
                'DOMAIN' => 'lightningfest2010.com',
            ),
            'llandovery.biz' =>
            array(
                'TABLE_SCHEMA' => 'lighthou_tkp',
                'TABLE_PREFIX' => 'wp_1_81675_',
                'SITE_URL' => 'http://www.llandovery.biz',
                'DOMAIN' => 'llandovery.biz',
            ),
            'loan-sh.com' =>
            array(
                'TABLE_SCHEMA' => 'lighthou_tkp',
                'TABLE_PREFIX' => 'wp_1_81676_',
                'SITE_URL' => 'http://www.loan-sh.com',
                'DOMAIN' => 'loan-sh.com',
            ),
            'machixian.net' =>
            array(
                'TABLE_SCHEMA' => 'lighthou_tkp',
                'TABLE_PREFIX' => 'wp_1_81677_',
                'SITE_URL' => 'http://www.machixian.net',
                'DOMAIN' => 'machixian.net',
            ),
            'refinance-internet.com' =>
            array(
                'TABLE_SCHEMA' => 'lighthou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1031_',
                'SITE_URL' => 'http://www.refinance-internet.com',
                'DOMAIN' => 'refinance-internet.com',
            ),
            'marxismovivo.org' =>
            array(
                'TABLE_SCHEMA' => 'lighthou_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1032_',
                'SITE_URL' => 'http://www.marxismovivo.org',
                'DOMAIN' => 'marxismovivo.org',
            ),
            'liviparadise.com' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_1_82528_',
                'SITE_URL' => 'http://www.liviparadise.com',
                'DOMAIN' => 'liviparadise.com',
            ),
            'naraywal.com' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_1_82529_',
                'SITE_URL' => 'http://www.naraywal.com',
                'DOMAIN' => 'naraywal.com',
            ),
            'heshoucha.com' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_1_82530_',
                'SITE_URL' => 'http://www.heshoucha.com',
                'DOMAIN' => 'heshoucha.com',
            ),
            'miniwork.net' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_1_82531_',
                'SITE_URL' => 'http://www.miniwork.net',
                'DOMAIN' => 'miniwork.net',
            ),
            'cansonicmediapro.org' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_1_82532_',
                'SITE_URL' => 'http://www.cansonicmediapro.org',
                'DOMAIN' => 'cansonicmediapro.org',
            ),
            'pecmi.org' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1033_',
                'SITE_URL' => 'http://www.pecmi.org',
                'DOMAIN' => 'pecmi.org',
            ),
            'simplysocialmedia.me' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_375_',
                'SITE_URL' => 'http://www.simplysocialmedia.me',
                'DOMAIN' => 'simplysocialmedia.me',
            ),
            'swamptours.info' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_377_',
                'SITE_URL' => 'http://www.swamptours.info',
                'DOMAIN' => 'swamptours.info',
            ),
            'shopcrankcountydaredevils.com' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_381_',
                'SITE_URL' => 'http://www.shopcrankcountydaredevils.com',
                'DOMAIN' => 'shopcrankcountydaredevils.com',
            ),
            'stephenaubrey.net' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_386_',
                'SITE_URL' => 'http://www.stephenaubrey.net',
                'DOMAIN' => 'stephenaubrey.net',
            ),
            'syscooklahoma.com' =>
            array(
                'TABLE_SCHEMA' => 'livipara_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_467_',
                'SITE_URL' => 'http://www.syscooklahoma.com',
                'DOMAIN' => 'syscooklahoma.com',
            ),
            'lousponderosa.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_1_83076_',
                'SITE_URL' => 'http://www.lousponderosa.com',
                'DOMAIN' => 'lousponderosa.com',
            ),
            'mariajlovestory.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_1_83077_',
                'SITE_URL' => 'http://www.mariajlovestory.com',
                'DOMAIN' => 'mariajlovestory.com',
            ),
            'mattfoley.us' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_1_83078_',
                'SITE_URL' => 'http://www.mattfoley.us',
                'DOMAIN' => 'mattfoley.us',
            ),
            'meaghanokeefe.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_1_83079_',
                'SITE_URL' => 'http://www.meaghanokeefe.com',
                'DOMAIN' => 'meaghanokeefe.com',
            ),
            'melechmizrahi.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_1_83080_',
                'SITE_URL' => 'http://www.melechmizrahi.com',
                'DOMAIN' => 'melechmizrahi.com',
            ),
            'nwphoenixrealestateguide.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1029_',
                'SITE_URL' => 'http://www.nwphoenixrealestateguide.com',
                'DOMAIN' => 'nwphoenixrealestateguide.com',
            ),
            'real-home-based-business-opportunity.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1030_',
                'SITE_URL' => 'http://www.real-home-based-business-opportunity.com',
                'DOMAIN' => 'real-home-based-business-opportunity.com',
            ),
            'save-our-sperm.org' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_370_',
                'SITE_URL' => 'http://www.save-our-sperm.org',
                'DOMAIN' => 'save-our-sperm.org',
            ),
            'saaccnh.org' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_371_',
                'SITE_URL' => 'http://www.saaccnh.org',
                'DOMAIN' => 'saaccnh.org',
            ),
            'roaringlambspublishing.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_373_',
                'SITE_URL' => 'http://www.roaringlambspublishing.com',
                'DOMAIN' => 'roaringlambspublishing.com',
            ),
            'shaikabdulhameed.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_374_',
                'SITE_URL' => 'http://www.shaikabdulhameed.com',
                'DOMAIN' => 'shaikabdulhameed.com',
            ),
            'shavetheadmirals.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_379_',
                'SITE_URL' => 'http://www.shavetheadmirals.com',
                'DOMAIN' => 'shavetheadmirals.com',
            ),
            'tnconservationeasementlaw.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_wp179',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://tnconservationeasementlaw.com',
                'DOMAIN' => 'tnconservationeasementlaw.com',
            ),
            'hemp-protein.org' =>
            array(
                'TABLE_SCHEMA' => 'louspond_wp600',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://hemp-protein.org',
                'DOMAIN' => 'hemp-protein.org',
            ),
            'gameforgilrs.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_wp707',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://gameforgilrs.com',
                'DOMAIN' => 'gameforgilrs.com',
            ),
            'theindianstore.org' =>
            array(
                'TABLE_SCHEMA' => 'louspond_wp74',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://theindianstore.org',
                'DOMAIN' => 'theindianstore.org',
            ),
            'kolmarden.org' =>
            array(
                'TABLE_SCHEMA' => 'louspond_wp804',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://kolmarden.org',
                'DOMAIN' => 'kolmarden.org',
            ),
            'artdebutant-sf.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_wp840',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://artdebutant-sf.com',
                'DOMAIN' => 'artdebutant-sf.com',
            ),
            'ericasegovia.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_wp885',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ericasegovia.com',
                'DOMAIN' => 'ericasegovia.com',
            ),
            'g4cfoundation.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_wp937',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://g4cfoundation.com',
                'DOMAIN' => 'g4cfoundation.com',
            ),
            'garratygroup.com' =>
            array(
                'TABLE_SCHEMA' => 'louspond_wp99',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://garratygroup.com',
                'DOMAIN' => 'garratygroup.com',
            ),
            'manthasoft.com' =>
            array(
                'TABLE_SCHEMA' => 'manthaso_tkp',
                'TABLE_PREFIX' => 'wp_1_2635_',
                'SITE_URL' => 'http://www.manthasoft.com',
                'DOMAIN' => 'manthasoft.com',
            ),
            'mapadefiesta.com' =>
            array(
                'TABLE_SCHEMA' => 'manthaso_tkp',
                'TABLE_PREFIX' => 'wp_1_2636_',
                'SITE_URL' => 'http://www.mapadefiesta.com',
                'DOMAIN' => 'mapadefiesta.com',
            ),
            'mardynli.com' =>
            array(
                'TABLE_SCHEMA' => 'manthaso_tkp',
                'TABLE_PREFIX' => 'wp_1_2637_',
                'SITE_URL' => 'http://www.mardynli.com',
                'DOMAIN' => 'mardynli.com',
            ),
            'mariposahotelinnonline.com' =>
            array(
                'TABLE_SCHEMA' => 'manthaso_tkp',
                'TABLE_PREFIX' => 'wp_1_2638_',
                'SITE_URL' => 'http://www.mariposahotelinnonline.com',
                'DOMAIN' => 'mariposahotelinnonline.com',
            ),
            'maximefotografie.com' =>
            array(
                'TABLE_SCHEMA' => 'manthaso_tkp',
                'TABLE_PREFIX' => 'wp_1_2639_',
                'SITE_URL' => 'http://www.maximefotografie.com',
                'DOMAIN' => 'maximefotografie.com',
            ),
            'childsplaykennels.com' =>
            array(
                'TABLE_SCHEMA' => 'manthaso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1025_',
                'SITE_URL' => 'http://www.childsplaykennels.com',
                'DOMAIN' => 'childsplaykennels.com',
            ),
            'doctorgaber.com' =>
            array(
                'TABLE_SCHEMA' => 'manthaso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1026_',
                'SITE_URL' => 'http://www.doctorgaber.com',
                'DOMAIN' => 'doctorgaber.com',
            ),
            'newsouthernmedia.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1312_',
                'SITE_URL' => 'http://www.newsouthernmedia.com',
                'DOMAIN' => 'newsouthernmedia.com',
            ),
            'northspringsbasketball.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1313_',
                'SITE_URL' => 'http://www.northspringsbasketball.com',
                'DOMAIN' => 'northspringsbasketball.com',
            ),
            'number1homebusiness.net' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1314_',
                'SITE_URL' => 'http://www.number1homebusiness.net',
                'DOMAIN' => 'number1homebusiness.net',
            ),
            'ozpokertour.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1315_',
                'SITE_URL' => 'http://www.ozpokertour.com',
                'DOMAIN' => 'ozpokertour.com',
            ),
            'pacific-coastwellness.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1316_',
                'SITE_URL' => 'http://www.pacific-coastwellness.com',
                'DOMAIN' => 'pacific-coastwellness.com',
            ),
            'margaritapizzabar.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_954_',
                'SITE_URL' => 'http://www.margaritapizzabar.com',
                'DOMAIN' => 'margaritapizzabar.com',
            ),
            'hsprtpl.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_955_',
                'SITE_URL' => 'http://www.hsprtpl.com',
                'DOMAIN' => 'hsprtpl.com',
            ),
            'liberalatheistvegetarian.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_956_',
                'SITE_URL' => 'http://www.liberalatheistvegetarian.com',
                'DOMAIN' => 'liberalatheistvegetarian.com',
            ),
            'xiphiasgladius.org' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_957_',
                'SITE_URL' => 'http://www.xiphiasgladius.org',
                'DOMAIN' => 'xiphiasgladius.org',
            ),
            'tweetupkzoo.com' =>
            array(
                'TABLE_SCHEMA' => 'margarit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_958_',
                'SITE_URL' => 'http://www.tweetupkzoo.com',
                'DOMAIN' => 'tweetupkzoo.com',
            ),
            'maryloubarrett.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_1_82896_',
                'SITE_URL' => 'http://www.maryloubarrett.com',
                'DOMAIN' => 'maryloubarrett.com',
            ),
            'modaluxe.net' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_1_82897_',
                'SITE_URL' => 'http://www.modaluxe.net',
                'DOMAIN' => 'modaluxe.net',
            ),
            'morandizone.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_1_82898_',
                'SITE_URL' => 'http://www.morandizone.com',
                'DOMAIN' => 'morandizone.com',
            ),
            'mtb-equipment.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_1_82899_',
                'SITE_URL' => 'http://www.mtb-equipment.com',
                'DOMAIN' => 'mtb-equipment.com',
            ),
            'naturesdesigns.biz' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_1_82900_',
                'SITE_URL' => 'http://www.naturesdesigns.biz',
                'DOMAIN' => 'naturesdesigns.biz',
            ),
            'ns2.maryloubarrett.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_78_',
                'SITE_URL' => 'http://ns2.maryloubarrett.com',
                'DOMAIN' => 'ns2.maryloubarrett.com',
            ),
            'kickbackcommunications.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_79_',
                'SITE_URL' => 'http://www.kickbackcommunications.com',
                'DOMAIN' => 'kickbackcommunications.com',
            ),
            'klivekravenbeats.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_80_',
                'SITE_URL' => 'http://klivekravenbeats.com',
                'DOMAIN' => 'klivekravenbeats.com',
            ),
            'lawofattractionsuccessstories.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1027_',
                'SITE_URL' => 'http://www.lawofattractionsuccessstories.com',
                'DOMAIN' => 'lawofattractionsuccessstories.com',
            ),
            'southeastohioadvertising.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1028_',
                'SITE_URL' => 'http://www.southeastohioadvertising.com',
                'DOMAIN' => 'southeastohioadvertising.com',
            ),
            'original-individuals.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_366_',
                'SITE_URL' => 'http://www.original-individuals.com',
                'DOMAIN' => 'original-individuals.com',
            ),
            'osoconalas.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_367_',
                'SITE_URL' => 'http://www.osoconalas.com',
                'DOMAIN' => 'osoconalas.com',
            ),
            'patientcompliancehypnosis.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_368_',
                'SITE_URL' => 'http://www.patientcompliancehypnosis.com',
                'DOMAIN' => 'patientcompliancehypnosis.com',
            ),
            'radiofolcloreportugues.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_369_',
                'SITE_URL' => 'http://www.radiofolcloreportugues.com',
                'DOMAIN' => 'radiofolcloreportugues.com',
            ),
            're-energizenow.com' =>
            array(
                'TABLE_SCHEMA' => 'maryloub_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_372_',
                'SITE_URL' => 'http://www.re-energizenow.com',
                'DOMAIN' => 're-energizenow.com',
            ),
            'masproductionsny.com' =>
            array(
                'TABLE_SCHEMA' => 'masprodu_tkp',
                'TABLE_PREFIX' => 'wp_1_82513_',
                'SITE_URL' => 'http://www.masproductionsny.com',
                'DOMAIN' => 'masproductionsny.com',
            ),
            'orlandofantasy.com' =>
            array(
                'TABLE_SCHEMA' => 'masprodu_tkp',
                'TABLE_PREFIX' => 'wp_1_82514_',
                'SITE_URL' => 'http://www.orlandofantasy.com',
                'DOMAIN' => 'orlandofantasy.com',
            ),
            'outcomesbuilders.com' =>
            array(
                'TABLE_SCHEMA' => 'masprodu_tkp',
                'TABLE_PREFIX' => 'wp_1_82515_',
                'SITE_URL' => 'http://www.outcomesbuilders.com',
                'DOMAIN' => 'outcomesbuilders.com',
            ),
            'whybecommon.com' =>
            array(
                'TABLE_SCHEMA' => 'masprodu_tkp',
                'TABLE_PREFIX' => 'wp_1_82516_',
                'SITE_URL' => 'http://www.whybecommon.com',
                'DOMAIN' => 'whybecommon.com',
            ),
            '1stchoiceleasing.com' =>
            array(
                'TABLE_SCHEMA' => 'masprodu_tkp',
                'TABLE_PREFIX' => 'wp_1_82517_',
                'SITE_URL' => 'http://www.1stchoiceleasing.com',
                'DOMAIN' => '1stchoiceleasing.com',
            ),
            'mcdavidnissanparts.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_1_2745_',
                'SITE_URL' => 'http://www.mcdavidnissanparts.com',
                'DOMAIN' => 'mcdavidnissanparts.com',
            ),
            'mdjjysb.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_1_2746_',
                'SITE_URL' => 'http://www.mdjjysb.com',
                'DOMAIN' => 'mdjjysb.com',
            ),
            'melody-musicschool.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_1_2747_',
                'SITE_URL' => 'http://www.melody-musicschool.com',
                'DOMAIN' => 'melody-musicschool.com',
            ),
            'merderbir.org' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_1_2748_',
                'SITE_URL' => 'http://www.merderbir.org',
                'DOMAIN' => 'merderbir.org',
            ),
            'mi-swingers.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_1_2749_',
                'SITE_URL' => 'http://www.mi-swingers.com',
                'DOMAIN' => 'mi-swingers.com',
            ),
            'accident-lawyers-new-york.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1023_',
                'SITE_URL' => 'http://www.accident-lawyers-new-york.com',
                'DOMAIN' => 'accident-lawyers-new-york.com',
            ),
            'adaboatyard.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1024_',
                'SITE_URL' => 'http://www.adaboatyard.com',
                'DOMAIN' => 'adaboatyard.com',
            ),
            'lionjewelry-rickcamerondesign.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_340_',
                'SITE_URL' => 'http://www.lionjewelry-rickcamerondesign.com',
                'DOMAIN' => 'lionjewelry-rickcamerondesign.com',
            ),
            'internetoglasavanje.info' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_344_',
                'SITE_URL' => 'http://www.internetoglasavanje.info',
                'DOMAIN' => 'internetoglasavanje.info',
            ),
            'pantherjewelry-rickcamerondesign.com' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_350_',
                'SITE_URL' => 'http://www.pantherjewelry-rickcamerondesign.com',
                'DOMAIN' => 'pantherjewelry-rickcamerondesign.com',
            ),
            'beppumasjid.org' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_351_',
                'SITE_URL' => 'http://www.beppumasjid.org',
                'DOMAIN' => 'beppumasjid.org',
            ),
            'outsourcejob.info' =>
            array(
                'TABLE_SCHEMA' => 'mcdavidn_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_352_',
                'SITE_URL' => 'http://www.outsourcejob.info',
                'DOMAIN' => 'outsourcejob.info',
            ),
            'meida.biz' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_1_81199_',
                'SITE_URL' => 'http://www.meida.biz',
                'DOMAIN' => 'meida.biz',
            ),
            'metropolemarket.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_1_81200_',
                'SITE_URL' => 'http://www.metropolemarket.com',
                'DOMAIN' => 'metropolemarket.com',
            ),
            'millstream-adult-ed.org' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_1_81201_',
                'SITE_URL' => 'http://www.millstream-adult-ed.org',
                'DOMAIN' => 'millstream-adult-ed.org',
            ),
            'mlmexecutiveleadpro.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_1_81202_',
                'SITE_URL' => 'http://www.mlmexecutiveleadpro.com',
                'DOMAIN' => 'mlmexecutiveleadpro.com',
            ),
            'mmtco.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_1_81203_',
                'SITE_URL' => 'http://www.mmtco.com',
                'DOMAIN' => 'mmtco.com',
            ),
            'discounthotelcanada.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1021_',
                'SITE_URL' => 'http://www.discounthotelcanada.com',
                'DOMAIN' => 'discounthotelcanada.com',
            ),
            '825credit.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1022_',
                'SITE_URL' => 'http://www.825credit.com',
                'DOMAIN' => '825credit.com',
            ),
            'noimpactmandoc.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1457_',
                'SITE_URL' => 'http://www.noimpactmandoc.com',
                'DOMAIN' => 'noimpactmandoc.com',
            ),
            'creativelyendeavored.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_333_',
                'SITE_URL' => 'http://www.creativelyendeavored.com',
                'DOMAIN' => 'creativelyendeavored.com',
            ),
            'millionsmarchharlem.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_339_',
                'SITE_URL' => 'http://www.millionsmarchharlem.com',
                'DOMAIN' => 'millionsmarchharlem.com',
            ),
            'wcsafford.org' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_347_',
                'SITE_URL' => 'http://www.wcsafford.org',
                'DOMAIN' => 'wcsafford.org',
            ),
            'eaglejewelry-rickcamerondesign.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_348_',
                'SITE_URL' => 'http://www.eaglejewelry-rickcamerondesign.com',
                'DOMAIN' => 'eaglejewelry-rickcamerondesign.com',
            ),
            'mongoriverrun.net' =>
            array(
                'TABLE_SCHEMA' => 'meida_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_355_',
                'SITE_URL' => 'http://www.mongoriverrun.net',
                'DOMAIN' => 'mongoriverrun.net',
            ),
            'summerinfantdayandnightvideomonitor.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_wp181',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://summerinfantdayandnightvideomonitor.com',
                'DOMAIN' => 'summerinfantdayandnightvideomonitor.com',
            ),
            'scheucenter.org' =>
            array(
                'TABLE_SCHEMA' => 'meida_wp597',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://scheucenter.org',
                'DOMAIN' => 'scheucenter.org',
            ),
            'samsbd.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_wp648',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://samsbd.com',
                'DOMAIN' => 'samsbd.com',
            ),
            'gimellifisto.net' =>
            array(
                'TABLE_SCHEMA' => 'meida_wp667',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://gimellifisto.net',
                'DOMAIN' => 'gimellifisto.net',
            ),
            'bitmultimedia.com' =>
            array(
                'TABLE_SCHEMA' => 'meida_wp925',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://bitmultimedia.com',
                'DOMAIN' => 'bitmultimedia.com',
            ),
            'milwaukeecricket.org' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_1_83792_',
                'SITE_URL' => 'http://www.milwaukeecricket.org',
                'DOMAIN' => 'milwaukeecricket.org',
            ),
            'seedr.us' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_1_83793_',
                'SITE_URL' => 'http://www.seedr.us',
                'DOMAIN' => 'seedr.us',
            ),
            'sociedadcolombianamastozoologia.org' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_1_83794_',
                'SITE_URL' => 'http://www.sociedadcolombianamastozoologia.org',
                'DOMAIN' => 'sociedadcolombianamastozoologia.org',
            ),
            'lowcountrytalent.com' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_1_83795_',
                'SITE_URL' => 'http://www.lowcountrytalent.com',
                'DOMAIN' => 'lowcountrytalent.com',
            ),
            'hispanicvoterproject.org' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_1_83796_',
                'SITE_URL' => 'http://www.hispanicvoterproject.org',
                'DOMAIN' => 'hispanicvoterproject.org',
            ),
            'colossusfinance.com' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1020_',
                'SITE_URL' => 'http://www.colossusfinance.com',
                'DOMAIN' => 'colossusfinance.com',
            ),
            'hiddengrove.info' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_318_',
                'SITE_URL' => 'http://www.hiddengrove.info',
                'DOMAIN' => 'hiddengrove.info',
            ),
            'villo.me' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_345_',
                'SITE_URL' => 'http://villo.me',
                'DOMAIN' => 'villo.me',
            ),
            'egyptjobsearch.com' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_349_',
                'SITE_URL' => 'http://www.egyptjobsearch.com',
                'DOMAIN' => 'egyptjobsearch.com',
            ),
            'pdv2.com' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_353_',
                'SITE_URL' => 'http://www.pdv2.com',
                'DOMAIN' => 'pdv2.com',
            ),
            'sufiotetahi.org' =>
            array(
                'TABLE_SCHEMA' => 'milwauke_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_358_',
                'SITE_URL' => 'http://www.sufiotetahi.org',
                'DOMAIN' => 'sufiotetahi.org',
            ),
            'mommiescornerlive.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_1_2640_',
                'SITE_URL' => 'http://www.mommiescornerlive.com',
                'DOMAIN' => 'mommiescornerlive.com',
            ),
            'myneek.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_1_2641_',
                'SITE_URL' => 'http://www.myneek.com',
                'DOMAIN' => 'myneek.com',
            ),
            'neysanat.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_1_2642_',
                'SITE_URL' => 'http://www.neysanat.com',
                'DOMAIN' => 'neysanat.com',
            ),
            'oasisdeamorva.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_1_2643_',
                'SITE_URL' => 'http://www.oasisdeamorva.com',
                'DOMAIN' => 'oasisdeamorva.com',
            ),
            'octoviakuries.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_1_2644_',
                'SITE_URL' => 'http://www.octoviakuries.com',
                'DOMAIN' => 'octoviakuries.com',
            ),
            'brendabuckley.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1233_',
                'SITE_URL' => 'http://www.brendabuckley.com',
                'DOMAIN' => 'brendabuckley.com',
            ),
            'brucebeaton.biz' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1234_',
                'SITE_URL' => 'http://www.brucebeaton.biz',
                'DOMAIN' => 'brucebeaton.biz',
            ),
            'carlsaganbarranca.org' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1235_',
                'SITE_URL' => 'http://www.carlsaganbarranca.org',
                'DOMAIN' => 'carlsaganbarranca.org',
            ),
            'pezavalpalmas.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_260_',
                'SITE_URL' => 'http://www.pezavalpalmas.com',
                'DOMAIN' => 'pezavalpalmas.com',
            ),
            'theartoftransplant.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_268_',
                'SITE_URL' => 'http://www.theartoftransplant.com',
                'DOMAIN' => 'theartoftransplant.com',
            ),
            'resultspropertiestenerife.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_274_',
                'SITE_URL' => 'http://www.resultspropertiestenerife.com',
                'DOMAIN' => 'resultspropertiestenerife.com',
            ),
            'whateverendeavor.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_275_',
                'SITE_URL' => 'http://www.whateverendeavor.com',
                'DOMAIN' => 'whateverendeavor.com',
            ),
            'uasnews.net' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_997_',
                'SITE_URL' => 'http://www.uasnews.net',
                'DOMAIN' => 'uasnews.net',
            ),
            'waterqualityday.com' =>
            array(
                'TABLE_SCHEMA' => 'mommiesc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_998_',
                'SITE_URL' => 'http://www.waterqualityday.com',
                'DOMAIN' => 'waterqualityday.com',
            ),
            'moolarb.com' =>
            array(
                'TABLE_SCHEMA' => 'moolarbc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_880_',
                'SITE_URL' => 'http://www.moolarb.com',
                'DOMAIN' => 'moolarb.com',
            ),
            'scholarshipz.info' =>
            array(
                'TABLE_SCHEMA' => 'moolarbc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_881_',
                'SITE_URL' => 'http://www.scholarshipz.info',
                'DOMAIN' => 'scholarshipz.info',
            ),
            'seattlemetropolitanfashionweek.com' =>
            array(
                'TABLE_SCHEMA' => 'moolarbc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_882_',
                'SITE_URL' => 'http://www.seattlemetropolitanfashionweek.com',
                'DOMAIN' => 'seattlemetropolitanfashionweek.com',
            ),
            'basilbernstein7.com' =>
            array(
                'TABLE_SCHEMA' => 'moolarbc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_883_',
                'SITE_URL' => 'http://www.basilbernstein7.com',
                'DOMAIN' => 'basilbernstein7.com',
            ),
            'whydidnti.org' =>
            array(
                'TABLE_SCHEMA' => 'moolarbc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_884_',
                'SITE_URL' => 'http://www.whydidnti.org',
                'DOMAIN' => 'whydidnti.org',
            ),
            'multiculturalyouth.org' =>
            array(
                'TABLE_SCHEMA' => 'multicul_tkp',
                'TABLE_PREFIX' => 'wp_1_83250_',
                'SITE_URL' => 'http://www.multiculturalyouth.org',
                'DOMAIN' => 'multiculturalyouth.org',
            ),
            'musicforjapan.org' =>
            array(
                'TABLE_SCHEMA' => 'multicul_tkp',
                'TABLE_PREFIX' => 'wp_1_83251_',
                'SITE_URL' => 'http://www.musicforjapan.org',
                'DOMAIN' => 'musicforjapan.org',
            ),
            'mvnoworld.net' =>
            array(
                'TABLE_SCHEMA' => 'multicul_tkp',
                'TABLE_PREFIX' => 'wp_1_83252_',
                'SITE_URL' => 'http://www.mvnoworld.net',
                'DOMAIN' => 'mvnoworld.net',
            ),
            'newjerseybenefitsblog.com' =>
            array(
                'TABLE_SCHEMA' => 'multicul_tkp',
                'TABLE_PREFIX' => 'wp_1_83253_',
                'SITE_URL' => 'http://www.newjerseybenefitsblog.com',
                'DOMAIN' => 'newjerseybenefitsblog.com',
            ),
            'newyorkcommercialfitnessequipment.com' =>
            array(
                'TABLE_SCHEMA' => 'multicul_tkp',
                'TABLE_PREFIX' => 'wp_1_83254_',
                'SITE_URL' => 'http://www.newyorkcommercialfitnessequipment.com',
                'DOMAIN' => 'newyorkcommercialfitnessequipment.com',
            ),
            'tntrap.com' =>
            array(
                'TABLE_SCHEMA' => 'multicul_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_993_',
                'SITE_URL' => 'http://www.tntrap.com',
                'DOMAIN' => 'tntrap.com',
            ),
            'twdnc.com' =>
            array(
                'TABLE_SCHEMA' => 'multicul_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_994_',
                'SITE_URL' => 'http://www.twdnc.com',
                'DOMAIN' => 'twdnc.com',
            ),
            'myalignhr.com' =>
            array(
                'TABLE_SCHEMA' => 'myalignh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_697_',
                'SITE_URL' => 'http://www.myalignhr.com',
                'DOMAIN' => 'myalignhr.com',
            ),
            'mydragonfly.info' =>
            array(
                'TABLE_SCHEMA' => 'myalignh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_698_',
                'SITE_URL' => 'http://www.mydragonfly.info',
                'DOMAIN' => 'mydragonfly.info',
            ),
            'mykvutza.com' =>
            array(
                'TABLE_SCHEMA' => 'myalignh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_699_',
                'SITE_URL' => 'http://www.mykvutza.com',
                'DOMAIN' => 'mykvutza.com',
            ),
            'naero.org' =>
            array(
                'TABLE_SCHEMA' => 'myalignh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_700_',
                'SITE_URL' => 'http://www.naero.org',
                'DOMAIN' => 'naero.org',
            ),
            'narutoroom.info' =>
            array(
                'TABLE_SCHEMA' => 'myalignh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_701_',
                'SITE_URL' => 'http://www.narutoroom.info',
                'DOMAIN' => 'narutoroom.info',
            ),
            'mybrownbag.org' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_1_84317_',
                'SITE_URL' => 'http://www.mybrownbag.org',
                'DOMAIN' => 'mybrownbag.org',
            ),
            'sweetsweetsweet.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_1_84318_',
                'SITE_URL' => 'http://www.sweetsweetsweet.com',
                'DOMAIN' => 'sweetsweetsweet.com',
            ),
            'topdogranch.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_1_84319_',
                'SITE_URL' => 'http://www.topdogranch.com',
                'DOMAIN' => 'topdogranch.com',
            ),
            'water-tech.org' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_1_84320_',
                'SITE_URL' => 'http://www.water-tech.org',
                'DOMAIN' => 'water-tech.org',
            ),
            'acplp.org' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_1_84321_',
                'SITE_URL' => 'http://www.acplp.org',
                'DOMAIN' => 'acplp.org',
            ),
            'apocalyptomedia.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1227_',
                'SITE_URL' => 'http://www.apocalyptomedia.com',
                'DOMAIN' => 'apocalyptomedia.com',
            ),
            'aromasensual.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1228_',
                'SITE_URL' => 'http://www.aromasensual.com',
                'DOMAIN' => 'aromasensual.com',
            ),
            'chequeboutique.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1229_',
                'SITE_URL' => 'http://www.chequeboutique.com',
                'DOMAIN' => 'chequeboutique.com',
            ),
            'curvesneedham.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1230_',
                'SITE_URL' => 'http://www.curvesneedham.com',
                'DOMAIN' => 'curvesneedham.com',
            ),
            'dotclassik.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1231_',
                'SITE_URL' => 'http://www.dotclassik.com',
                'DOMAIN' => 'dotclassik.com',
            ),
            '8thstreetdesigndistrict.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_258_',
                'SITE_URL' => 'http://www.8thstreetdesigndistrict.com',
                'DOMAIN' => '8thstreetdesigndistrict.com',
            ),
            'jellisphotography.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_259_',
                'SITE_URL' => 'http://www.jellisphotography.com',
                'DOMAIN' => 'jellisphotography.com',
            ),
            'savannahsinfonietta.org' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_267_',
                'SITE_URL' => 'http://www.savannahsinfonietta.org',
                'DOMAIN' => 'savannahsinfonietta.org',
            ),
            'lookoutshuttersinc.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_278_',
                'SITE_URL' => 'http://www.lookoutshuttersinc.com',
                'DOMAIN' => 'lookoutshuttersinc.com',
            ),
            'safemoveoregon.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_279_',
                'SITE_URL' => 'http://www.safemoveoregon.com',
                'DOMAIN' => 'safemoveoregon.com',
            ),
            'xoopsers.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_995_',
                'SITE_URL' => 'http://www.xoopsers.com',
                'DOMAIN' => 'xoopsers.com',
            ),
            'therrsfeedsound.com' =>
            array(
                'TABLE_SCHEMA' => 'mybrownb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_996_',
                'SITE_URL' => 'http://www.therrsfeedsound.com',
                'DOMAIN' => 'therrsfeedsound.com',
            ),
            'nashvilledjservice.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_1_83027_',
                'SITE_URL' => 'http://www.nashvilledjservice.com',
                'DOMAIN' => 'nashvilledjservice.com',
            ),
            'newyorkprivateplacement.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_wp113',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.newyorkprivateplacement.com',
                'DOMAIN' => 'newyorkprivateplacement.com',
            ),
            'pierreshughes.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_1_83029_',
                'SITE_URL' => 'http://www.pierreshughes.com',
                'DOMAIN' => 'pierreshughes.com',
            ),
            'rmjd.info' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_1_83030_',
                'SITE_URL' => 'http://www.rmjd.info',
                'DOMAIN' => 'rmjd.info',
            ),
            'swordsandhorses.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_wp962',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.swordsandhorses.com',
                'DOMAIN' => 'swordsandhorses.com',
            ),
            '3g-brasil.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_182_',
                'SITE_URL' => 'http://3g-brasil.com',
                'DOMAIN' => '3g-brasil.com',
            ),
            'aarons-bar-mitzvah.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_183_',
                'SITE_URL' => 'http://www.aarons-bar-mitzvah.com',
                'DOMAIN' => 'aarons-bar-mitzvah.com',
            ),
            'accessabilitymodifications.us' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_184_',
                'SITE_URL' => 'http://www.accessabilitymodifications.us',
                'DOMAIN' => 'accessabilitymodifications.us',
            ),
            'waterfowlersbootcamp.org' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_wp441',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://waterfowlersbootcamp.org',
                'DOMAIN' => 'waterfowlersbootcamp.org',
            ),
            'ocalabridal.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_wp659',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ocalabridal.com',
                'DOMAIN' => 'ocalabridal.com',
            ),
            'indiansong-radio.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1101_',
                'SITE_URL' => 'http://www.indiansong-radio.com',
                'DOMAIN' => 'indiansong-radio.com',
            ),
            'phillycommunitycinema.org' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1102_',
                'SITE_URL' => 'http://www.phillycommunitycinema.org',
                'DOMAIN' => 'phillycommunitycinema.org',
            ),
            'buymonclersite.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1103_',
                'SITE_URL' => 'http://www.buymonclersite.com',
                'DOMAIN' => 'buymonclersite.com',
            ),
            'geoose.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1104_',
                'SITE_URL' => 'http://www.geoose.com',
                'DOMAIN' => 'geoose.com',
            ),
            'cdmstudycentre.org' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_249_',
                'SITE_URL' => 'http://cdmstudycentre.org',
                'DOMAIN' => 'cdmstudycentre.org',
            ),
            'kidsandmoney.tv' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_wp209',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://kidsandmoney.tv',
                'DOMAIN' => 'kidsandmoney.tv',
            ),
            'infrawebs-eu.org' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_251_',
                'SITE_URL' => 'http://www.infrawebs-eu.org',
                'DOMAIN' => 'infrawebs-eu.org',
            ),
            'archifind.net' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_252_',
                'SITE_URL' => 'http://www.archifind.net',
                'DOMAIN' => 'archifind.net',
            ),
            'upshots.biz' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_255_',
                'SITE_URL' => 'http://www.upshots.biz',
                'DOMAIN' => 'upshots.biz',
            ),
            'weswatsonweb.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_991_',
                'SITE_URL' => 'http://www.weswatsonweb.com',
                'DOMAIN' => 'weswatsonweb.com',
            ),
            'thestudiosky.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_992_',
                'SITE_URL' => 'http://www.thestudiosky.com',
                'DOMAIN' => 'thestudiosky.com',
            ),
            'vipperla.com' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_wp692',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://vipperla.com',
                'DOMAIN' => 'vipperla.com',
            ),
            'roboticstrends2012.org' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_wp825',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://roboticstrends2012.org',
                'DOMAIN' => 'roboticstrends2012.org',
            ),
            'politeia-station.net' =>
            array(
                'TABLE_SCHEMA' => 'nashvill_wp940',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://politeia-station.net',
                'DOMAIN' => 'politeia-station.net',
            ),
            'natlainsafe.info' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_1_83081_',
                'SITE_URL' => 'http://www.natlainsafe.info',
                'DOMAIN' => 'natlainsafe.info',
            ),
            'nerconline.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_1_83082_',
                'SITE_URL' => 'http://www.nerconline.com',
                'DOMAIN' => 'nerconline.com',
            ),
            'notonmydime.net' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_1_83083_',
                'SITE_URL' => 'http://www.notonmydime.net',
                'DOMAIN' => 'notonmydime.net',
            ),
            'phpmytools.org' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_1_83084_',
                'SITE_URL' => 'http://www.phpmytools.org',
                'DOMAIN' => 'phpmytools.org',
            ),
            'picturesofthefreedomtower.biz' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_1_83085_',
                'SITE_URL' => 'http://www.picturesofthefreedomtower.biz',
                'DOMAIN' => 'picturesofthefreedomtower.biz',
            ),
            'handlvmade.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1095_',
                'SITE_URL' => 'http://www.handlvmade.com',
                'DOMAIN' => 'handlvmade.com',
            ),
            'mycreditcardpayment.net' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1098_',
                'SITE_URL' => 'http://www.mycreditcardpayment.net',
                'DOMAIN' => 'mycreditcardpayment.net',
            ),
            'tactologic.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1100_',
                'SITE_URL' => 'http://www.tactologic.com',
                'DOMAIN' => 'tactologic.com',
            ),
            'konsep.mobi' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1111_',
                'SITE_URL' => 'http://www.konsep.mobi',
                'DOMAIN' => 'konsep.mobi',
            ),
            'mu-md.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1113_',
                'SITE_URL' => 'http://www.mu-md.com',
                'DOMAIN' => 'mu-md.com',
            ),
            'netbeans-serbia.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1114_',
                'SITE_URL' => 'http://www.netbeans-serbia.com',
                'DOMAIN' => 'netbeans-serbia.com',
            ),
            'jobdestruction.info' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_247_',
                'SITE_URL' => 'http://www.jobdestruction.info',
                'DOMAIN' => 'jobdestruction.info',
            ),
            'thedamesite.net' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_248_',
                'SITE_URL' => 'http://www.thedamesite.net',
                'DOMAIN' => 'thedamesite.net',
            ),
            'kreszvizsga.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_253_',
                'SITE_URL' => 'http://www.kreszvizsga.com',
                'DOMAIN' => 'kreszvizsga.com',
            ),
            'startwhereyouareprovt.org' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_254_',
                'SITE_URL' => 'http://www.startwhereyouareprovt.org',
                'DOMAIN' => 'startwhereyouareprovt.org',
            ),
            'nxdqk.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_256_',
                'SITE_URL' => 'http://www.nxdqk.com',
                'DOMAIN' => 'nxdqk.com',
            ),
            'sudanmap.org' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_755_',
                'SITE_URL' => 'http://www.sudanmap.org',
                'DOMAIN' => 'sudanmap.org',
            ),
            'takeahikefitness.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_756_',
                'SITE_URL' => 'http://www.takeahikefitness.com',
                'DOMAIN' => 'takeahikefitness.com',
            ),
            's-swuw.org' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_764_',
                'SITE_URL' => 'http://www.s-swuw.org',
                'DOMAIN' => 's-swuw.org',
            ),
            'purpleminibikemedia.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_765_',
                'SITE_URL' => 'http://www.purpleminibikemedia.com',
                'DOMAIN' => 'purpleminibikemedia.com',
            ),
            'austrianaccommodation.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_766_',
                'SITE_URL' => 'http://www.austrianaccommodation.com',
                'DOMAIN' => 'austrianaccommodation.com',
            ),
            's-choirmusic.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_wp159',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://s-choirmusic.com',
                'DOMAIN' => 's-choirmusic.com',
            ),
            'whymarriagemattersalabama.org' =>
            array(
                'TABLE_SCHEMA' => 'natlains_wp319',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://whymarriagemattersalabama.org',
                'DOMAIN' => 'whymarriagemattersalabama.org',
            ),
            'oliviawilde.ws' =>
            array(
                'TABLE_SCHEMA' => 'natlains_wp391',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://oliviawilde.ws',
                'DOMAIN' => 'oliviawilde.ws',
            ),
            'imprint123.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_wp410',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://imprint123.com',
                'DOMAIN' => 'imprint123.com',
            ),
            'macosaudio.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_wp413',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://macosaudio.com',
                'DOMAIN' => 'macosaudio.com',
            ),
            'jewishnightlifeusa.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_wp647',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://jewishnightlifeusa.com',
                'DOMAIN' => 'jewishnightlifeusa.com',
            ),
            'johnireland.info' =>
            array(
                'TABLE_SCHEMA' => 'natlains_wp684',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://johnireland.info',
                'DOMAIN' => 'johnireland.info',
            ),
            'jamesrcomer.com' =>
            array(
                'TABLE_SCHEMA' => 'natlains_wp792',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://jamesrcomer.com',
                'DOMAIN' => 'jamesrcomer.com',
            ),
            'ncaafootball12.net' =>
            array(
                'TABLE_SCHEMA' => 'ncaafoot_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_702_',
                'SITE_URL' => 'http://www.ncaafootball12.net',
                'DOMAIN' => 'ncaafootball12.net',
            ),
            'networkinglinuxbook.com' =>
            array(
                'TABLE_SCHEMA' => 'ncaafoot_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_703_',
                'SITE_URL' => 'http://www.networkinglinuxbook.com',
                'DOMAIN' => 'networkinglinuxbook.com',
            ),
            'new-toyota-yaris-rockland-county-ny.info' =>
            array(
                'TABLE_SCHEMA' => 'ncaafoot_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_704_',
                'SITE_URL' => 'http://www.new-toyota-yaris-rockland-county-ny.info',
                'DOMAIN' => 'new-toyota-yaris-rockland-county-ny.info',
            ),
            'nflarizonacardinalsjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'ncaafoot_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_705_',
                'SITE_URL' => 'http://www.nflarizonacardinalsjerseysusa.info',
                'DOMAIN' => 'nflarizonacardinalsjerseysusa.info',
            ),
            'nflkansascitychiefsjerseysusa.info' =>
            array(
                'TABLE_SCHEMA' => 'ncaafoot_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_706_',
                'SITE_URL' => 'http://www.nflkansascitychiefsjerseysusa.info',
                'DOMAIN' => 'nflkansascitychiefsjerseysusa.info',
            ),
            'nicholsct.org' =>
            array(
                'TABLE_SCHEMA' => 'nicholsc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_707_',
                'SITE_URL' => 'http://www.nicholsct.org',
                'DOMAIN' => 'nicholsct.org',
            ),
            'nflnewyorkgiantsjerseysus.info' =>
            array(
                'TABLE_SCHEMA' => 'nicholsc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_708_',
                'SITE_URL' => 'http://www.nflnewyorkgiantsjerseysus.info',
                'DOMAIN' => 'nflnewyorkgiantsjerseysus.info',
            ),
            'niezgodafitness.com' =>
            array(
                'TABLE_SCHEMA' => 'nicholsc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_709_',
                'SITE_URL' => 'http://www.niezgodafitness.com',
                'DOMAIN' => 'niezgodafitness.com',
            ),
            'nihul-atarim.com' =>
            array(
                'TABLE_SCHEMA' => 'nicholsc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_710_',
                'SITE_URL' => 'http://www.nihul-atarim.com',
                'DOMAIN' => 'nihul-atarim.com',
            ),
            'nicolecorbin.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_1_82794_',
                'SITE_URL' => 'http://www.nicolecorbin.com',
                'DOMAIN' => 'nicolecorbin.com',
            ),
            'philipgibson.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_1_82795_',
                'SITE_URL' => 'http://www.philipgibson.com',
                'DOMAIN' => 'philipgibson.com',
            ),
            'pomaikaii.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_1_82796_',
                'SITE_URL' => 'http://www.pomaikaii.com',
                'DOMAIN' => 'pomaikaii.com',
            ),
            'turlockautomall.net' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_1_82797_',
                'SITE_URL' => 'http://www.turlockautomall.net',
                'DOMAIN' => 'turlockautomall.net',
            ),
            'xxxbluezone.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_1_82798_',
                'SITE_URL' => 'http://www.xxxbluezone.com',
                'DOMAIN' => 'xxxbluezone.com',
            ),
            'andrewwmullins.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_102_',
                'SITE_URL' => 'http://www.andrewwmullins.com',
                'DOMAIN' => 'andrewwmullins.com',
            ),
            'thompsonpeakretreat.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_112_',
                'SITE_URL' => 'http://www.thompsonpeakretreat.com',
                'DOMAIN' => 'thompsonpeakretreat.com',
            ),
            'opresume.net' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_114_',
                'SITE_URL' => 'http://www.opresume.net',
                'DOMAIN' => 'opresume.net',
            ),
            'redhillchina.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_161_',
                'SITE_URL' => 'http://www.redhillchina.com',
                'DOMAIN' => 'redhillchina.com',
            ),
            'studiomaxim.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_753_',
                'SITE_URL' => 'http://www.studiomaxim.com',
                'DOMAIN' => 'studiomaxim.com',
            ),
            'strictlyswingak.com' =>
            array(
                'TABLE_SCHEMA' => 'nicoleco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_754_',
                'SITE_URL' => 'http://www.strictlyswingak.com',
                'DOMAIN' => 'strictlyswingak.com',
            ),
            'nicoledeanincomecpr.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_1_83812_',
                'SITE_URL' => 'http://www.nicoledeanincomecpr.com',
                'DOMAIN' => 'nicoledeanincomecpr.com',
            ),
            'distritalpsdviseu.info' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_1_83813_',
                'SITE_URL' => 'http://www.distritalpsdviseu.info',
                'DOMAIN' => 'distritalpsdviseu.info',
            ),
            'deschutesswcd.org' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_1_83814_',
                'SITE_URL' => 'http://www.deschutesswcd.org',
                'DOMAIN' => 'deschutesswcd.org',
            ),
            'mitchelldesignstudios.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_1_83815_',
                'SITE_URL' => 'http://www.mitchelldesignstudios.com',
                'DOMAIN' => 'mitchelldesignstudios.com',
            ),
            'apart-movie.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_1_83816_',
                'SITE_URL' => 'http://www.apart-movie.com',
                'DOMAIN' => 'apart-movie.com',
            ),
            'dnfjiejinqi.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_84_',
                'SITE_URL' => 'http://www.dnfjiejinqi.com',
                'DOMAIN' => 'dnfjiejinqi.com',
            ),
            'socialmarketing4travel.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_85_',
                'SITE_URL' => 'http://www.socialmarketing4travel.com',
                'DOMAIN' => 'socialmarketing4travel.com',
            ),
            'sbdist.org' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_86_',
                'SITE_URL' => 'http://www.sbdist.org',
                'DOMAIN' => 'sbdist.org',
            ),
            'selenamcarthur.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_87_',
                'SITE_URL' => 'http://www.selenamcarthur.com',
                'DOMAIN' => 'selenamcarthur.com',
            ),
            'ranchosports.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_88_',
                'SITE_URL' => 'http://www.ranchosports.com',
                'DOMAIN' => 'ranchosports.com',
            ),
            'challwamar.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_89_',
                'SITE_URL' => 'http://www.challwamar.com',
                'DOMAIN' => 'challwamar.com',
            ),
            'yellowticket2.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_111_',
                'SITE_URL' => 'http://www.yellowticket2.com',
                'DOMAIN' => 'yellowticket2.com',
            ),
            'thelifestylehunter.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_752_',
                'SITE_URL' => 'http://www.thelifestylehunter.com',
                'DOMAIN' => 'thelifestylehunter.com',
            ),
            'itvexploration.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_86_',
                'SITE_URL' => 'http://www.itvexploration.com',
                'DOMAIN' => 'itvexploration.com',
            ),
            'ecoscreener.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_91_',
                'SITE_URL' => 'http://www.ecoscreener.com',
                'DOMAIN' => 'ecoscreener.com',
            ),
            'manakkudiyan.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_93_',
                'SITE_URL' => 'http://www.manakkudiyan.com',
                'DOMAIN' => 'manakkudiyan.com',
            ),
            'r3ma.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_99_',
                'SITE_URL' => 'http://www.r3ma.com',
                'DOMAIN' => 'r3ma.com',
            ),
            'contractorjobsite.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_wp478',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://contractorjobsite.com',
                'DOMAIN' => 'contractorjobsite.com',
            ),
            'the-adjustment.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_wp726',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://the-adjustment.com',
                'DOMAIN' => 'the-adjustment.com',
            ),
            'stepup2green.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_wp802',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://stepup2green.com',
                'DOMAIN' => 'stepup2green.com',
            ),
            'glovesinc.net' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_wp876',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://glovesinc.net',
                'DOMAIN' => 'glovesinc.net',
            ),
            'ourpreciouswords.com' =>
            array(
                'TABLE_SCHEMA' => 'nicolede_wp904',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ourpreciouswords.com',
                'DOMAIN' => 'ourpreciouswords.com',
            ),
            'niknyce.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_1_2750_',
                'SITE_URL' => 'http://www.niknyce.com',
                'DOMAIN' => 'niknyce.com',
            ),
            'notaryprocedures.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_1_2751_',
                'SITE_URL' => 'http://www.notaryprocedures.com',
                'DOMAIN' => 'notaryprocedures.com',
            ),
            'oblivion-lb.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_1_2752_',
                'SITE_URL' => 'http://www.oblivion-lb.com',
                'DOMAIN' => 'oblivion-lb.com',
            ),
            'palawanhotels.us' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_1_2753_',
                'SITE_URL' => 'http://www.palawanhotels.us',
                'DOMAIN' => 'palawanhotels.us',
            ),
            'paschertshirt.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_1_2754_',
                'SITE_URL' => 'http://www.paschertshirt.com',
                'DOMAIN' => 'paschertshirt.com',
            ),
            'st-louis-auto-accidents-lawyers.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_750_',
                'SITE_URL' => 'http://www.st-louis-auto-accidents-lawyers.com',
                'DOMAIN' => 'st-louis-auto-accidents-lawyers.com',
            ),
            'steadfastbags.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_751_',
                'SITE_URL' => 'http://www.steadfastbags.com',
                'DOMAIN' => 'steadfastbags.com',
            ),
            'inet-oll.info' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_78_',
                'SITE_URL' => 'http://www.inet-oll.info',
                'DOMAIN' => 'inet-oll.info',
            ),
            'horsebookdepot.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_79_',
                'SITE_URL' => 'http://www.horsebookdepot.com',
                'DOMAIN' => 'horsebookdepot.com',
            ),
            'slavonski-brod.info' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_82_',
                'SITE_URL' => 'http://www.slavonski-brod.info',
                'DOMAIN' => 'slavonski-brod.info',
            ),
            'naturalawakeningsswva.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_96_',
                'SITE_URL' => 'http://www.naturalawakeningsswva.com',
                'DOMAIN' => 'naturalawakeningsswva.com',
            ),
            'mvhsfreelancer.com' =>
            array(
                'TABLE_SCHEMA' => 'niknyce_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_97_',
                'SITE_URL' => 'http://www.mvhsfreelancer.com',
                'DOMAIN' => 'mvhsfreelancer.com',
            ),
            'ninoslatinossanos.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_1_81213_',
                'SITE_URL' => 'http://www.ninoslatinossanos.com',
                'DOMAIN' => 'ninoslatinossanos.com',
            ),
            'notearsforthecreatures.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_1_81214_',
                'SITE_URL' => 'http://www.notearsforthecreatures.com',
                'DOMAIN' => 'notearsforthecreatures.com',
            ),
            'objectagent.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_1_81215_',
                'SITE_URL' => 'http://www.objectagent.com',
                'DOMAIN' => 'objectagent.com',
            ),
            'onidentitytheftat.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_1_81216_',
                'SITE_URL' => 'http://www.onidentitytheftat.com',
                'DOMAIN' => 'onidentitytheftat.com',
            ),
            'our-yachts.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_1_81217_',
                'SITE_URL' => 'http://www.our-yachts.com',
                'DOMAIN' => 'our-yachts.com',
            ),
            'spokenbyzurek.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_748_',
                'SITE_URL' => 'http://www.spokenbyzurek.com',
                'DOMAIN' => 'spokenbyzurek.com',
            ),
            'st-ives.biz' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_749_',
                'SITE_URL' => 'http://www.st-ives.biz',
                'DOMAIN' => 'st-ives.biz',
            ),
            'gaillourgroup.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_75_',
                'SITE_URL' => 'http://www.gaillourgroup.com',
                'DOMAIN' => 'gaillourgroup.com',
            ),
            'prepare4rain.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_83_',
                'SITE_URL' => 'http://www.prepare4rain.com',
                'DOMAIN' => 'prepare4rain.com',
            ),
            'chattanoogacrash.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_87_',
                'SITE_URL' => 'http://www.chattanoogacrash.com',
                'DOMAIN' => 'chattanoogacrash.com',
            ),
            'hidrobiologia.info' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_88_',
                'SITE_URL' => 'http://www.hidrobiologia.info',
                'DOMAIN' => 'hidrobiologia.info',
            ),
            'uniteamasia.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_94_',
                'SITE_URL' => 'http://www.uniteamasia.com',
                'DOMAIN' => 'uniteamasia.com',
            ),
            'kaylin-boehme.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_wp152',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://kaylin-boehme.com',
                'DOMAIN' => 'kaylin-boehme.com',
            ),
            'shandsrehab.org' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_wp515',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://shandsrehab.org',
                'DOMAIN' => 'shandsrehab.org',
            ),
            'ifara.tv' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_wp616',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ifara.tv',
                'DOMAIN' => 'ifara.tv',
            ),
            'abczdravlja.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_wp685',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://abczdravlja.com',
                'DOMAIN' => 'abczdravlja.com',
            ),
            'pneumablogs.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_wp710',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://pneumablogs.com',
                'DOMAIN' => 'pneumablogs.com',
            ),
            'postwarmonologues.com' =>
            array(
                'TABLE_SCHEMA' => 'ninoslat_wp844',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://postwarmonologues.com',
                'DOMAIN' => 'postwarmonologues.com',
            ),
            'nmllyq.com' =>
            array(
                'TABLE_SCHEMA' => 'nmllyqco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_712_',
                'SITE_URL' => 'http://www.nmllyq.com',
                'DOMAIN' => 'nmllyq.com',
            ),
            'nirman.me' =>
            array(
                'TABLE_SCHEMA' => 'nmllyqco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_713_',
                'SITE_URL' => 'http://www.nirman.me',
                'DOMAIN' => 'nirman.me',
            ),
            'nysignaturefilms.com' =>
            array(
                'TABLE_SCHEMA' => 'nmllyqco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_714_',
                'SITE_URL' => 'http://www.nysignaturefilms.com',
                'DOMAIN' => 'nysignaturefilms.com',
            ),
            'occupy837.com' =>
            array(
                'TABLE_SCHEMA' => 'nmllyqco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_715_',
                'SITE_URL' => 'http://www.occupy837.com',
                'DOMAIN' => 'occupy837.com',
            ),
            'xb179.com' =>
            array(
                'TABLE_SCHEMA' => 'nmllyqco_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_760_',
                'SITE_URL' => 'http://www.xb179.com',
                'DOMAIN' => 'xb179.com',
            ),
            'nordif.org' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_1_83100_',
                'SITE_URL' => 'http://www.nordif.org',
                'DOMAIN' => 'nordif.org',
            ),
            'prokidsdc.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_1_83101_',
                'SITE_URL' => 'http://www.prokidsdc.com',
                'DOMAIN' => 'prokidsdc.com',
            ),
            'steph101.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_1_83102_',
                'SITE_URL' => 'http://www.steph101.com',
                'DOMAIN' => 'steph101.com',
            ),
            'paulkingforcongress.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_1_83103_',
                'SITE_URL' => 'http://www.paulkingforcongress.com',
                'DOMAIN' => 'paulkingforcongress.com',
            ),
            'goalkitsap.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_1_83104_',
                'SITE_URL' => 'http://www.goalkitsap.com',
                'DOMAIN' => 'goalkitsap.com',
            ),
            'medicalmarijuanaindustrygroup.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_242_',
                'SITE_URL' => 'http://medicalmarijuanaindustrygroup.com',
                'DOMAIN' => 'medicalmarijuanaindustrygroup.com',
            ),
            'metromailadvertising.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_243_',
                'SITE_URL' => 'http://metromailadvertising.com',
                'DOMAIN' => 'metromailadvertising.com',
            ),
            'miaomiao003.info' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_244_',
                'SITE_URL' => 'http://miaomiao003.info',
                'DOMAIN' => 'miaomiao003.info',
            ),
            'mkchildcare.info' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_245_',
                'SITE_URL' => 'http://mkchildcare.info',
                'DOMAIN' => 'mkchildcare.info',
            ),
            'elevationlive.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1214_',
                'SITE_URL' => 'http://www.elevationlive.com',
                'DOMAIN' => 'elevationlive.com',
            ),
            'fredplimley.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1215_',
                'SITE_URL' => 'http://www.fredplimley.com',
                'DOMAIN' => 'fredplimley.com',
            ),
            'gymguyunderwear.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1217_',
                'SITE_URL' => 'http://www.gymguyunderwear.com',
                'DOMAIN' => 'gymguyunderwear.com',
            ),
            'pamravenscroft.org' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_822_',
                'SITE_URL' => 'http://www.pamravenscroft.org',
                'DOMAIN' => 'pamravenscroft.org',
            ),
            'zeitgeist-jalisco.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_823_',
                'SITE_URL' => 'http://www.zeitgeist-jalisco.com',
                'DOMAIN' => 'zeitgeist-jalisco.com',
            ),
            'cetskip.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_824_',
                'SITE_URL' => 'http://www.cetskip.com',
                'DOMAIN' => 'cetskip.com',
            ),
            'enchiladaenduro.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_825_',
                'SITE_URL' => 'http://www.enchiladaenduro.com',
                'DOMAIN' => 'enchiladaenduro.com',
            ),
            'andreverdenskrig.com' =>
            array(
                'TABLE_SCHEMA' => 'nordif_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_826_',
                'SITE_URL' => 'http://www.andreverdenskrig.com',
                'DOMAIN' => 'andreverdenskrig.com',
            ),
            'norentemas.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_1_82769_',
                'SITE_URL' => 'http://www.norentemas.com',
                'DOMAIN' => 'norentemas.com',
            ),
            'palmspringsgayrealtors.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_1_82770_',
                'SITE_URL' => 'http://www.palmspringsgayrealtors.com',
                'DOMAIN' => 'palmspringsgayrealtors.com',
            ),
            'theinterworks.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_1_82771_',
                'SITE_URL' => 'http://www.theinterworks.com',
                'DOMAIN' => 'theinterworks.com',
            ),
            'zipbusca.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_1_82772_',
                'SITE_URL' => 'http://www.zipbusca.com',
                'DOMAIN' => 'zipbusca.com',
            ),
            'electronic-appliance-repair.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_1_82773_',
                'SITE_URL' => 'http://www.electronic-appliance-repair.com',
                'DOMAIN' => 'electronic-appliance-repair.com',
            ),
            'singaporeasiaexpo.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_744_',
                'SITE_URL' => 'http://www.singaporeasiaexpo.com',
                'DOMAIN' => 'singaporeasiaexpo.com',
            ),
            'sisterforgiveme.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_745_',
                'SITE_URL' => 'http://www.sisterforgiveme.com',
                'DOMAIN' => 'sisterforgiveme.com',
            ),
            'parquesihunchen.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_77_',
                'SITE_URL' => 'http://www.parquesihunchen.com',
                'DOMAIN' => 'parquesihunchen.com',
            ),
            'usacejacksonvilleregulatory.com' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_84_',
                'SITE_URL' => 'http://www.usacejacksonvilleregulatory.com',
                'DOMAIN' => 'usacejacksonvilleregulatory.com',
            ),
            'evolutionaryvoters.us' =>
            array(
                'TABLE_SCHEMA' => 'norentem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_85_',
                'SITE_URL' => 'http://www.evolutionaryvoters.us',
                'DOMAIN' => 'evolutionaryvoters.us',
            ),
            'nsciusa.org' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_1_84352_',
                'SITE_URL' => 'http://www.nsciusa.org',
                'DOMAIN' => 'nsciusa.org',
            ),
            'proudlypowerless.com' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_1_84353_',
                'SITE_URL' => 'http://www.proudlypowerless.com',
                'DOMAIN' => 'proudlypowerless.com',
            ),
            'provenirblog.com' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_1_84354_',
                'SITE_URL' => 'http://www.provenirblog.com',
                'DOMAIN' => 'provenirblog.com',
            ),
            'serfmusicfestival.com' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_1_84355_',
                'SITE_URL' => 'http://www.serfmusicfestival.com',
                'DOMAIN' => 'serfmusicfestival.com',
            ),
            'shittycarwash.com' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_1_84356_',
                'SITE_URL' => 'http://www.shittycarwash.com',
                'DOMAIN' => 'shittycarwash.com',
            ),
            'slowjanuary.com' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_746_',
                'SITE_URL' => 'http://www.slowjanuary.com',
                'DOMAIN' => 'slowjanuary.com',
            ),
            'smokeythewellnessguy.net' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_747_',
                'SITE_URL' => 'http://www.smokeythewellnessguy.net',
                'DOMAIN' => 'smokeythewellnessguy.net',
            ),
            'queenannefb.org' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_74_',
                'SITE_URL' => 'http://www.queenannefb.org',
                'DOMAIN' => 'queenannefb.org',
            ),
            'mtsbmlyym.info' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_80_',
                'SITE_URL' => 'http://www.mtsbmlyym.info',
                'DOMAIN' => 'mtsbmlyym.info',
            ),
            'williamrowson.com' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_81_',
                'SITE_URL' => 'http://www.williamrowson.com',
                'DOMAIN' => 'williamrowson.com',
            ),
            'poslovnapolitika.com' =>
            array(
                'TABLE_SCHEMA' => 'nsciusa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_89_',
                'SITE_URL' => 'http://www.poslovnapolitika.com',
                'DOMAIN' => 'poslovnapolitika.com',
            ),
            'nshfw.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_1_84468_',
                'SITE_URL' => 'http://www.nshfw.com',
                'DOMAIN' => 'nshfw.com',
            ),
            'ockpp.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_1_84469_',
                'SITE_URL' => 'http://www.ockpp.com',
                'DOMAIN' => 'ockpp.com',
            ),
            'poetornot.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_1_84470_',
                'SITE_URL' => 'http://www.poetornot.com',
                'DOMAIN' => 'poetornot.com',
            ),
            'tampahockeyclub.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_1_84471_',
                'SITE_URL' => 'http://www.tampahockeyclub.com',
                'DOMAIN' => 'tampahockeyclub.com',
            ),
            'transformativeradio.org' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_1_84472_',
                'SITE_URL' => 'http://www.transformativeradio.org',
                'DOMAIN' => 'transformativeradio.org',
            ),
            'casino-hotel.us' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_61_',
                'SITE_URL' => 'http://www.casino-hotel.us',
                'DOMAIN' => 'casino-hotel.us',
            ),
            'digitalmark.me' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_66_',
                'SITE_URL' => 'http://www.digitalmark.me',
                'DOMAIN' => 'digitalmark.me',
            ),
            'cantuss.info' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_679_',
                'SITE_URL' => 'http://www.cantuss.info',
                'DOMAIN' => 'cantuss.info',
            ),
            '1windows8.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_72_',
                'SITE_URL' => 'http://www.1windows8.com',
                'DOMAIN' => '1windows8.com',
            ),
            'shippinghazmat.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_742_',
                'SITE_URL' => 'http://www.shippinghazmat.com',
                'DOMAIN' => 'shippinghazmat.com',
            ),
            'shoplederniercri.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_743_',
                'SITE_URL' => 'http://www.shoplederniercri.com',
                'DOMAIN' => 'shoplederniercri.com',
            ),
            'occupy-public-space.com' =>
            array(
                'TABLE_SCHEMA' => 'nshfw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_76_',
                'SITE_URL' => 'http://www.occupy-public-space.com',
                'DOMAIN' => 'occupy-public-space.com',
            ),
            'nvsys.biz' =>
            array(
                'TABLE_SCHEMA' => 'nvsys_tkp',
                'TABLE_PREFIX' => 'wp_1_83553_',
                'SITE_URL' => 'http://www.nvsys.biz',
                'DOMAIN' => 'nvsys.biz',
            ),
            'pcjavitas.info' =>
            array(
                'TABLE_SCHEMA' => 'nvsys_tkp',
                'TABLE_PREFIX' => 'wp_1_83554_',
                'SITE_URL' => 'http://www.pcjavitas.info',
                'DOMAIN' => 'pcjavitas.info',
            ),
            'pluginmotorsports.com' =>
            array(
                'TABLE_SCHEMA' => 'nvsys_tkp',
                'TABLE_PREFIX' => 'wp_1_83555_',
                'SITE_URL' => 'http://www.pluginmotorsports.com',
                'DOMAIN' => 'pluginmotorsports.com',
            ),
            'productsthailand.com' =>
            array(
                'TABLE_SCHEMA' => 'nvsys_tkp',
                'TABLE_PREFIX' => 'wp_1_83556_',
                'SITE_URL' => 'http://www.productsthailand.com',
                'DOMAIN' => 'productsthailand.com',
            ),
            'puppiesacrosscanada.com' =>
            array(
                'TABLE_SCHEMA' => 'nvsys_tkp',
                'TABLE_PREFIX' => 'wp_1_83557_',
                'SITE_URL' => 'http://www.puppiesacrosscanada.com',
                'DOMAIN' => 'puppiesacrosscanada.com',
            ),
            'deadonguns.com' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1164_',
                'SITE_URL' => 'http://www.deadonguns.com',
                'DOMAIN' => 'deadonguns.com',
            ),
            'deboraharkrader.com' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1165_',
                'SITE_URL' => 'http://www.deboraharkrader.com',
                'DOMAIN' => 'deboraharkrader.com',
            ),
            '1866999.com' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1166_',
                'SITE_URL' => 'http://www.1866999.com',
                'DOMAIN' => '1866999.com',
            ),
            'dreambigcasting.net' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1167_',
                'SITE_URL' => 'http://www.dreambigcasting.net',
                'DOMAIN' => 'dreambigcasting.net',
            ),
            'disastersolutionslc.com' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1168_',
                'SITE_URL' => 'http://www.disastersolutionslc.com',
                'DOMAIN' => 'disastersolutionslc.com',
            ),
            'oemsoftware.info' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_854_',
                'SITE_URL' => 'http://www.oemsoftware.info',
                'DOMAIN' => 'oemsoftware.info',
            ),
            'nykmc.org' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_856_',
                'SITE_URL' => 'http://www.nykmc.org',
                'DOMAIN' => 'nykmc.org',
            ),
            'hermajestyssecretplayers.org' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_857_',
                'SITE_URL' => 'http://www.hermajestyssecretplayers.org',
                'DOMAIN' => 'hermajestyssecretplayers.org',
            ),
            'speakupvictoria.com' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_858_',
                'SITE_URL' => 'http://www.speakupvictoria.com',
                'DOMAIN' => 'speakupvictoria.com',
            ),
            'furgetmenot.biz' =>
            array(
                'TABLE_SCHEMA' => 'nykmcorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_859_',
                'SITE_URL' => 'http://www.furgetmenot.biz',
                'DOMAIN' => 'furgetmenot.biz',
            ),
            'oandstrucking.net' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_1_82906_',
                'SITE_URL' => 'http://www.oandstrucking.net',
                'DOMAIN' => 'oandstrucking.net',
            ),
            'orgoneskulls.com' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_1_82907_',
                'SITE_URL' => 'http://www.orgoneskulls.com',
                'DOMAIN' => 'orgoneskulls.com',
            ),
            'wulabakan.com' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_1_82908_',
                'SITE_URL' => 'http://www.wulabakan.com',
                'DOMAIN' => 'wulabakan.com',
            ),
            'ostitto.com' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_1_82909_',
                'SITE_URL' => 'http://www.ostitto.com',
                'DOMAIN' => 'ostitto.com',
            ),
            'pedroluisonline.org' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_1_82910_',
                'SITE_URL' => 'http://www.pedroluisonline.org',
                'DOMAIN' => 'pedroluisonline.org',
            ),
            '193.235.227.112' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_86_',
                'SITE_URL' => 'http://193.235.227.112//',
                'DOMAIN' => '193.235.227.112',
            ),
            'linksystocisco.com' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_87_',
                'SITE_URL' => 'http://linksystocisco.com',
                'DOMAIN' => 'linksystocisco.com',
            ),
            'livegiveskate.com' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_88_',
                'SITE_URL' => 'http://www.livegiveskate.com',
                'DOMAIN' => 'livegiveskate.com',
            ),
            'mail.lusofolie.com' =>
            array(
                'TABLE_SCHEMA' => 'oandstru_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_89_',
                'SITE_URL' => 'http://mail.lusofolie.com',
                'DOMAIN' => 'mail.lusofolie.com',
            ),
            'parsons-table.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1290_',
                'SITE_URL' => 'http://www.parsons-table.com',
                'DOMAIN' => 'parsons-table.com',
            ),
            'propiedadesconestilo.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1291_',
                'SITE_URL' => 'http://www.propiedadesconestilo.com',
                'DOMAIN' => 'propiedadesconestilo.com',
            ),
            'qctqpus.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1292_',
                'SITE_URL' => 'http://www.qctqpus.com',
                'DOMAIN' => 'qctqpus.com',
            ),
            'rabighcip.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1293_',
                'SITE_URL' => 'http://www.rabighcip.com',
                'DOMAIN' => 'rabighcip.com',
            ),
            'oliveoakranch.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1294_',
                'SITE_URL' => 'http://www.oliveoakranch.com',
                'DOMAIN' => 'oliveoakranch.com',
            ),
            'occhiavarichairs4rent.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_929_',
                'SITE_URL' => 'http://www.occhiavarichairs4rent.com',
                'DOMAIN' => 'occhiavarichairs4rent.com',
            ),
            'orangecountyladjs.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_930_',
                'SITE_URL' => 'http://www.orangecountyladjs.com',
                'DOMAIN' => 'orangecountyladjs.com',
            ),
            'socalchiavarichairs.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_931_',
                'SITE_URL' => 'http://www.socalchiavarichairs.com',
                'DOMAIN' => 'socalchiavarichairs.com',
            ),
            'descriptiontoronto.com' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_932_',
                'SITE_URL' => 'http://www.descriptiontoronto.com',
                'DOMAIN' => 'descriptiontoronto.com',
            ),
            'ipisinternational.org' =>
            array(
                'TABLE_SCHEMA' => 'occhiava_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_933_',
                'SITE_URL' => 'http://www.ipisinternational.org',
                'DOMAIN' => 'ipisinternational.org',
            ),
            '100topbooks.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wpK_151_',
                'SITE_URL' => 'http://www.100topbooks.com',
                'DOMAIN' => '100topbooks.com',
            ),
            'abookbargain.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wpK_152_',
                'SITE_URL' => 'http://www.abookbargain.com',
                'DOMAIN' => 'abookbargain.com',
            ),
            'oogaboogatees.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_1_2645_',
                'SITE_URL' => 'http://www.oogaboogatees.com',
                'DOMAIN' => 'oogaboogatees.com',
            ),
            'pankowcompany.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_1_2646_',
                'SITE_URL' => 'http://www.pankowcompany.com',
                'DOMAIN' => 'pankowcompany.com',
            ),
            'premiumglassbeads.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_1_2647_',
                'SITE_URL' => 'http://www.premiumglassbeads.com',
                'DOMAIN' => 'premiumglassbeads.com',
            ),
            'racestorenow.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_1_2648_',
                'SITE_URL' => 'http://www.racestorenow.com',
                'DOMAIN' => 'racestorenow.com',
            ),
            'rapislife.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_1_2649_',
                'SITE_URL' => 'http://www.rapislife.com',
                'DOMAIN' => 'rapislife.com',
            ),
            'jqueryrefuge.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_65_',
                'SITE_URL' => 'http://www.jqueryrefuge.com',
                'DOMAIN' => 'jqueryrefuge.com',
            ),
            'rentalmedia.net' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_68_',
                'SITE_URL' => 'http://www.rentalmedia.net',
                'DOMAIN' => 'rentalmedia.net',
            ),
            'uweillustration2012.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_70_',
                'SITE_URL' => 'http://www.uweillustration2012.com',
                'DOMAIN' => 'uweillustration2012.com',
            ),
            'lasemainedelapub.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_71_',
                'SITE_URL' => 'http://www.lasemainedelapub.com',
                'DOMAIN' => 'lasemainedelapub.com',
            ),
            'selmannforstaterep.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_740_',
                'SITE_URL' => 'http://www.selmannforstaterep.com',
                'DOMAIN' => 'selmannforstaterep.com',
            ),
            'shamrocked2012.com' =>
            array(
                'TABLE_SCHEMA' => 'oogaboog_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_741_',
                'SITE_URL' => 'http://www.shamrocked2012.com',
                'DOMAIN' => 'shamrocked2012.com',
            ),
            'optometricexpress.net' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_50_',
                'SITE_URL' => 'http://www.optometricexpress.net',
                'DOMAIN' => 'optometricexpress.net',
            ),
            'sotmary.com' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_62_',
                'SITE_URL' => 'http://www.sotmary.com',
                'DOMAIN' => 'sotmary.com',
            ),
            'quhuizhijiatexiaoyao.com' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_63_',
                'SITE_URL' => 'http://www.quhuizhijiatexiaoyao.com',
                'DOMAIN' => 'quhuizhijiatexiaoyao.com',
            ),
            'cisallecce.com' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_64_',
                'SITE_URL' => 'http://www.cisallecce.com',
                'DOMAIN' => 'cisallecce.com',
            ),
            'fatina.net' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_67_',
                'SITE_URL' => 'http://www.fatina.net',
                'DOMAIN' => 'fatina.net',
            ),
            'sail-ssa.org' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_737_',
                'SITE_URL' => 'http://www.sail-ssa.org',
                'DOMAIN' => 'sail-ssa.org',
            ),
            'sarahpalinwillshootyouintheface.com' =>
            array(
                'TABLE_SCHEMA' => 'operafor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_738_',
                'SITE_URL' => 'http://www.sarahpalinwillshootyouintheface.com',
                'DOMAIN' => 'sarahpalinwillshootyouintheface.com',
            ),
            'operaforhumanity.org' =>
            array(
                'TABLE_SCHEMA' => 'operafor_wp268',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.operaforhumanity.org',
                'DOMAIN' => 'operaforhumanity.org',
            ),
            'p1umc.com' =>
            array(
                'TABLE_SCHEMA' => 'operafor_wp981',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.p1umc.com',
                'DOMAIN' => 'p1umc.com',
            ),
            'orderequipment.com' =>
            array(
                'TABLE_SCHEMA' => 'orderequ_tkp',
                'TABLE_PREFIX' => 'wp_1_2401_',
                'SITE_URL' => 'http://www.orderequipment.com',
                'DOMAIN' => 'orderequipment.com',
            ),
            'originaliceprincess.com' =>
            array(
                'TABLE_SCHEMA' => 'orderequ_tkp',
                'TABLE_PREFIX' => 'wp_1_2402_',
                'SITE_URL' => 'http://www.originaliceprincess.com',
                'DOMAIN' => 'originaliceprincess.com',
            ),
            'oryxinternational.net' =>
            array(
                'TABLE_SCHEMA' => 'orderequ_tkp',
                'TABLE_PREFIX' => 'wp_1_2403_',
                'SITE_URL' => 'http://www.oryxinternational.net',
                'DOMAIN' => 'oryxinternational.net',
            ),
            'osom.info' =>
            array(
                'TABLE_SCHEMA' => 'orderequ_tkp',
                'TABLE_PREFIX' => 'wp_1_2404_',
                'SITE_URL' => 'http://www.osom.info',
                'DOMAIN' => 'osom.info',
            ),
            'pageslove.com' =>
            array(
                'TABLE_SCHEMA' => 'orderequ_tkp',
                'TABLE_PREFIX' => 'wp_1_2405_',
                'SITE_URL' => 'http://www.pageslove.com',
                'DOMAIN' => 'pageslove.com',
            ),
            'phillypooltable.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_1_81218_',
                'SITE_URL' => 'http://www.phillypooltable.com',
                'DOMAIN' => 'phillypooltable.com',
            ),
            'pinecrestestate.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_1_81219_',
                'SITE_URL' => 'http://www.pinecrestestate.com',
                'DOMAIN' => 'pinecrestestate.com',
            ),
            'playtheblues.net' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_1_81220_',
                'SITE_URL' => 'http://www.playtheblues.net',
                'DOMAIN' => 'playtheblues.net',
            ),
            'plentyofish.info' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_1_81221_',
                'SITE_URL' => 'http://www.plentyofish.info',
                'DOMAIN' => 'plentyofish.info',
            ),
            'shepherdstownchalkartfestival.org' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_54_',
                'SITE_URL' => 'http://www.shepherdstownchalkartfestival.org',
                'DOMAIN' => 'shepherdstownchalkartfestival.org',
            ),
            'post342.org' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_55_',
                'SITE_URL' => 'http://www.post342.org',
                'DOMAIN' => 'post342.org',
            ),
            'yellowpandaartstudio.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_wp840',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://yellowpandaartstudio.com',
                'DOMAIN' => 'yellowpandaartstudio.com',
            ),
            'adphi-middlesex.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_60_',
                'SITE_URL' => 'http://www.adphi-middlesex.com',
                'DOMAIN' => 'adphi-middlesex.com',
            ),
            'route66restorationsinc.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_735_',
                'SITE_URL' => 'http://www.route66restorationsinc.com',
                'DOMAIN' => 'route66restorationsinc.com',
            ),
            'royalsulu.org' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_736_',
                'SITE_URL' => 'http://www.royalsulu.org',
                'DOMAIN' => 'royalsulu.org',
            ),
            'hzqqx.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_wp174',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://hzqqx.com',
                'DOMAIN' => 'hzqqx.com',
            ),
            'bluesbirdtours.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_wp684',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://bluesbirdtours.com',
                'DOMAIN' => 'bluesbirdtours.com',
            ),
            'blackboysaredelicious.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_wp739',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://blackboysaredelicious.com',
                'DOMAIN' => 'blackboysaredelicious.com',
            ),
            'carpet-cleaning-virginia.com' =>
            array(
                'TABLE_SCHEMA' => 'outtakon_wp858',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://carpet-cleaning-virginia.com',
                'DOMAIN' => 'carpet-cleaning-virginia.com',
            ),
            'koacon2013.org' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1130_',
                'SITE_URL' => 'http://www.koacon2013.org',
                'DOMAIN' => 'koacon2013.org',
            ),
            'hudsonvalleylgbtqcenter.com' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1131_',
                'SITE_URL' => 'http://www.hudsonvalleylgbtqcenter.com',
                'DOMAIN' => 'hudsonvalleylgbtqcenter.com',
            ),
            'collegegoalsundaypr.com' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1132_',
                'SITE_URL' => 'http://www.collegegoalsundaypr.com',
                'DOMAIN' => 'collegegoalsundaypr.com',
            ),
            'hotelsinleedscitycentre.net' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1133_',
                'SITE_URL' => 'http://www.hotelsinleedscitycentre.net',
                'DOMAIN' => 'hotelsinleedscitycentre.net',
            ),
            'respect-tradition.us' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1134_',
                'SITE_URL' => 'http://www.respect-tradition.us',
                'DOMAIN' => 'respect-tradition.us',
            ),
            'oxfordindiansociety.com' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_909_',
                'SITE_URL' => 'http://www.oxfordindiansociety.com',
                'DOMAIN' => 'oxfordindiansociety.com',
            ),
            'jews4change.org' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_910_',
                'SITE_URL' => 'http://www.jews4change.org',
                'DOMAIN' => 'jews4change.org',
            ),
            'arduousblog.com' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_911_',
                'SITE_URL' => 'http://www.arduousblog.com',
                'DOMAIN' => 'arduousblog.com',
            ),
            'berkeleycivilsidewalks.com' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_912_',
                'SITE_URL' => 'http://www.berkeleycivilsidewalks.com',
                'DOMAIN' => 'berkeleycivilsidewalks.com',
            ),
            'cidhm.org' =>
            array(
                'TABLE_SCHEMA' => 'oxfordin_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_913_',
                'SITE_URL' => 'http://www.cidhm.org',
                'DOMAIN' => 'cidhm.org',
            ),
            'datatranzx.com' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wpK_144_',
                'SITE_URL' => 'http://www.datatranzx.com',
                'DOMAIN' => 'datatranzx.com',
            ),
            'peoplesdaily.net' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_1_83762_',
                'SITE_URL' => 'http://www.peoplesdaily.net',
                'DOMAIN' => 'peoplesdaily.net',
            ),
            'iamnotforsale.org' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_1_83763_',
                'SITE_URL' => 'http://www.iamnotforsale.org',
                'DOMAIN' => 'iamnotforsale.org',
            ),
            '2buckscript.com' =>
            array(
                'TABLE_SCHEMA' => 'pgi-law_wp980',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://2buckscript.com',
                'DOMAIN' => '2buckscript.com',
            ),
            'hydrocell.net' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_1_83765_',
                'SITE_URL' => 'http://www.hydrocell.net',
                'DOMAIN' => 'hydrocell.net',
            ),
            'datamaskins.info' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_1_83766_',
                'SITE_URL' => 'http://www.datamaskins.info',
                'DOMAIN' => 'datamaskins.info',
            ),
            'camaemlak.com' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_47_',
                'SITE_URL' => 'http://www.camaemlak.com',
                'DOMAIN' => 'camaemlak.com',
            ),
            'ns-designer.com' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_49_',
                'SITE_URL' => 'http://www.ns-designer.com',
                'DOMAIN' => 'ns-designer.com',
            ),
            'coldsore.biz' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_56_',
                'SITE_URL' => 'http://www.coldsore.biz',
                'DOMAIN' => 'coldsore.biz',
            ),
            'genericana.com' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_57_',
                'SITE_URL' => 'http://www.genericana.com',
                'DOMAIN' => 'genericana.com',
            ),
            'fb68.info' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_59_',
                'SITE_URL' => 'http://www.fb68.info',
                'DOMAIN' => 'fb68.info',
            ),
            'roomba-review.org' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_732_',
                'SITE_URL' => 'http://www.roomba-review.org',
                'DOMAIN' => 'roomba-review.org',
            ),
            'rogermillerresortsales.com' =>
            array(
                'TABLE_SCHEMA' => 'peoplesd_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_733_',
                'SITE_URL' => 'http://www.rogermillerresortsales.com',
                'DOMAIN' => 'rogermillerresortsales.com',
            ),
            'photographicreverie.com' =>
            array(
                'TABLE_SCHEMA' => 'photogra_tkp',
                'TABLE_PREFIX' => 'wp_1_82285_',
                'SITE_URL' => 'http://www.photographicreverie.com',
                'DOMAIN' => 'photographicreverie.com',
            ),
            'qdplastics.com' =>
            array(
                'TABLE_SCHEMA' => 'photogra_tkp',
                'TABLE_PREFIX' => 'wp_1_82286_',
                'SITE_URL' => 'http://www.qdplastics.com',
                'DOMAIN' => 'qdplastics.com',
            ),
            'rainbowseptet.com' =>
            array(
                'TABLE_SCHEMA' => 'photogra_tkp',
                'TABLE_PREFIX' => 'wp_1_82287_',
                'SITE_URL' => 'http://www.rainbowseptet.com',
                'DOMAIN' => 'rainbowseptet.com',
            ),
            'rccweb.org' =>
            array(
                'TABLE_SCHEMA' => 'photogra_tkp',
                'TABLE_PREFIX' => 'wp_1_82288_',
                'SITE_URL' => 'http://www.rccweb.org',
                'DOMAIN' => 'rccweb.org',
            ),
            'rdmmedia.com' =>
            array(
                'TABLE_SCHEMA' => 'photogra_tkp',
                'TABLE_PREFIX' => 'wp_1_82289_',
                'SITE_URL' => 'http://www.rdmmedia.com',
                'DOMAIN' => 'rdmmedia.com',
            ),
            'treasurecoastmustangs.com' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1330_',
                'SITE_URL' => 'http://www.treasurecoastmustangs.com',
                'DOMAIN' => 'treasurecoastmustangs.com',
            ),
            'vacationsgetawayincentives.com' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1331_',
                'SITE_URL' => 'http://www.vacationsgetawayincentives.com',
                'DOMAIN' => 'vacationsgetawayincentives.com',
            ),
            'yankeenetwork.net' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1332_',
                'SITE_URL' => 'http://www.yankeenetwork.net',
                'DOMAIN' => 'yankeenetwork.net',
            ),
            'plantoplaymoredurham.com' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_971_',
                'SITE_URL' => 'http://www.plantoplaymoredurham.com',
                'DOMAIN' => 'plantoplaymoredurham.com',
            ),
            'fss2020.org' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_972_',
                'SITE_URL' => 'http://www.fss2020.org',
                'DOMAIN' => 'fss2020.org',
            ),
            'mauriciomessafotografia.com' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_973_',
                'SITE_URL' => 'http://www.mauriciomessafotografia.com',
                'DOMAIN' => 'mauriciomessafotografia.com',
            ),
            'distributeurdelest.com' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_974_',
                'SITE_URL' => 'http://www.distributeurdelest.com',
                'DOMAIN' => 'distributeurdelest.com',
            ),
            'lightstar.info' =>
            array(
                'TABLE_SCHEMA' => 'plantopl_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_975_',
                'SITE_URL' => 'http://www.lightstar.info',
                'DOMAIN' => 'lightstar.info',
            ),
            'aclimocab.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1151_',
                'SITE_URL' => 'http://www.aclimocab.com',
                'DOMAIN' => 'aclimocab.com',
            ),
            'alteredrootsquartet.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1152_',
                'SITE_URL' => 'http://www.alteredrootsquartet.com',
                'DOMAIN' => 'alteredrootsquartet.com',
            ),
            'poui.org' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_841_',
                'SITE_URL' => 'http://www.poui.org',
                'DOMAIN' => 'poui.org',
            ),
            'rachelibd.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_842_',
                'SITE_URL' => 'http://www.rachelibd.com',
                'DOMAIN' => 'rachelibd.com',
            ),
            'in-nell.org' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_843_',
                'SITE_URL' => 'http://www.in-nell.org',
                'DOMAIN' => 'in-nell.org',
            ),
            'assalcr.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_844_',
                'SITE_URL' => 'http://www.assalcr.com',
                'DOMAIN' => 'assalcr.com',
            ),
            'cucinamoreno.com' =>
            array(
                'TABLE_SCHEMA' => 'pouiorg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_845_',
                'SITE_URL' => 'http://www.cucinamoreno.com',
                'DOMAIN' => 'cucinamoreno.com',
            ),
            'storyteller-artist.com' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1296_',
                'SITE_URL' => 'http://www.storyteller-artist.com',
                'DOMAIN' => 'storyteller-artist.com',
            ),
            'rafalupion.com' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_934_',
                'SITE_URL' => 'http://www.rafalupion.com',
                'DOMAIN' => 'rafalupion.com',
            ),
            'boomeranked.com' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_935_',
                'SITE_URL' => 'http://www.boomeranked.com',
                'DOMAIN' => 'boomeranked.com',
            ),
            'esesme.org' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_936_',
                'SITE_URL' => 'http://www.esesme.org',
                'DOMAIN' => 'esesme.org',
            ),
            'ht-sc.org' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_937_',
                'SITE_URL' => 'http://www.ht-sc.org',
                'DOMAIN' => 'ht-sc.org',
            ),
            'graphoquebec.com' =>
            array(
                'TABLE_SCHEMA' => 'rafalupi_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_938_',
                'SITE_URL' => 'http://www.graphoquebec.com',
                'DOMAIN' => 'graphoquebec.com',
            ),
            'treat-hemorrhoids.us' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_22_',
                'SITE_URL' => 'http://www.treat-hemorrhoids.us',
                'DOMAIN' => 'treat-hemorrhoids.us',
            ),
            'imaginingpericles.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_15_',
                'SITE_URL' => 'http://www.imaginingpericles.com',
                'DOMAIN' => 'imaginingpericles.com',
            ),
            'cfnpc.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_16_',
                'SITE_URL' => 'http://www.cfnpc.org',
                'DOMAIN' => 'cfnpc.org',
            ),
            'cpcimostate.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_saexpo20_tkp1',
                'TABLE_PREFIX' => 'wpK_156_',
                'SITE_URL' => 'http://www.cpcimostate.org',
                'DOMAIN' => 'cpcimostate.org',
            ),
            'saexpo2010.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_1_83817_',
                'SITE_URL' => 'http://www.saexpo2010.com',
                'DOMAIN' => 'saexpo2010.com',
            ),
            'joiningpeoplestring.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_1_83818_',
                'SITE_URL' => 'http://www.joiningpeoplestring.com',
                'DOMAIN' => 'joiningpeoplestring.com',
            ),
            'carlosmbarros.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_1_83819_',
                'SITE_URL' => 'http://www.carlosmbarros.com',
                'DOMAIN' => 'carlosmbarros.com',
            ),
            'theipsummit.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_1_83820_',
                'SITE_URL' => 'http://www.theipsummit.com',
                'DOMAIN' => 'theipsummit.com',
            ),
            'bjhytk.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_1_83821_',
                'SITE_URL' => 'http://www.bjhytk.com',
                'DOMAIN' => 'bjhytk.com',
            ),
            'paintforthecause.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_70_',
                'SITE_URL' => 'http://www.paintforthecause.org',
                'DOMAIN' => 'paintforthecause.org',
            ),
            '4hinoc.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_71_',
                'SITE_URL' => 'http://www.4hinoc.org',
                'DOMAIN' => '4hinoc.org',
            ),
            'developnp.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_72_',
                'SITE_URL' => 'http://www.developnp.com',
                'DOMAIN' => 'developnp.com',
            ),
            'winnerschool.net' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_73_',
                'SITE_URL' => 'http://www.winnerschool.net',
                'DOMAIN' => 'winnerschool.net',
            ),
            'unetusmundos.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_74_',
                'SITE_URL' => 'http://www.unetusmundos.com',
                'DOMAIN' => 'unetusmundos.com',
            ),
            'eosiowa.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_75_',
                'SITE_URL' => 'http://www.eosiowa.com',
                'DOMAIN' => 'eosiowa.com',
            ),
            'museotextildeoaxaca.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_76_',
                'SITE_URL' => 'http://www.museotextildeoaxaca.org',
                'DOMAIN' => 'museotextildeoaxaca.org',
            ),
            'toddgrebeandcoldcountry.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_77_',
                'SITE_URL' => 'http://www.toddgrebeandcoldcountry.com',
                'DOMAIN' => 'toddgrebeandcoldcountry.com',
            ),
            'ycyc-registration.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_78_',
                'SITE_URL' => 'http://www.ycyc-registration.com',
                'DOMAIN' => 'ycyc-registration.com',
            ),
            'hbxftc.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_24_',
                'SITE_URL' => 'http://www.hbxftc.org',
                'DOMAIN' => 'hbxftc.org',
            ),
            'viviendoenotradimension.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_wp115',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://viviendoenotradimension.com',
                'DOMAIN' => 'viviendoenotradimension.com',
            ),
            'stmaryscountychildadvocacycenter.org' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_wp148',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://stmaryscountychildadvocacycenter.org',
                'DOMAIN' => 'stmaryscountychildadvocacycenter.org',
            ),
            '911medicalidus.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_wp227',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://911medicalidus.com',
                'DOMAIN' => '911medicalidus.com',
            ),
            'akroneconomicdevelopment.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_wp529',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://akroneconomicdevelopment.com',
                'DOMAIN' => 'akroneconomicdevelopment.com',
            ),
            'laviedesmutants.com' =>
            array(
                'TABLE_SCHEMA' => 'saexpo20_wp565',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://laviedesmutants.com',
                'DOMAIN' => 'laviedesmutants.com',
            ),
            'anovadiaspora.org' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1125_',
                'SITE_URL' => 'http://www.anovadiaspora.org',
                'DOMAIN' => 'anovadiaspora.org',
            ),
            'faxbrt.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1126_',
                'SITE_URL' => 'http://www.faxbrt.com',
                'DOMAIN' => 'faxbrt.com',
            ),
            'mysummerwoodhome.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1127_',
                'SITE_URL' => 'http://www.mysummerwoodhome.com',
                'DOMAIN' => 'mysummerwoodhome.com',
            ),
            'artspacesailboatbend.org' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1128_',
                'SITE_URL' => 'http://www.artspacesailboatbend.org',
                'DOMAIN' => 'artspacesailboatbend.org',
            ),
            'clubsambil.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1129_',
                'SITE_URL' => 'http://www.clubsambil.com',
                'DOMAIN' => 'clubsambil.com',
            ),
            'saldarriagacompany.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_904_',
                'SITE_URL' => 'http://www.saldarriagacompany.com',
                'DOMAIN' => 'saldarriagacompany.com',
            ),
            'thcorodeopageant.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_905_',
                'SITE_URL' => 'http://www.thcorodeopageant.com',
                'DOMAIN' => 'thcorodeopageant.com',
            ),
            'artwurst.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_906_',
                'SITE_URL' => 'http://www.artwurst.com',
                'DOMAIN' => 'artwurst.com',
            ),
            'thecheesemongerswife.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_907_',
                'SITE_URL' => 'http://www.thecheesemongerswife.com',
                'DOMAIN' => 'thecheesemongerswife.com',
            ),
            'amartransgenics.com' =>
            array(
                'TABLE_SCHEMA' => 'saldarri_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_908_',
                'SITE_URL' => 'http://www.amartransgenics.com',
                'DOMAIN' => 'amartransgenics.com',
            ),
            'namebuilder.net' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1351_',
                'SITE_URL' => 'http://www.namebuilder.net',
                'DOMAIN' => 'namebuilder.net',
            ),
            'p-q-a.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1352_',
                'SITE_URL' => 'http://www.p-q-a.com',
                'DOMAIN' => 'p-q-a.com',
            ),
            '101010runforpasigriver.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1353_',
                'SITE_URL' => 'http://www.101010runforpasigriver.com',
                'DOMAIN' => '101010runforpasigriver.com',
            ),
            'sarah-coleman.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_687_',
                'SITE_URL' => 'http://www.sarah-coleman.com',
                'DOMAIN' => 'sarah-coleman.com',
            ),
            'selecthotelnashville.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_688_',
                'SITE_URL' => 'http://www.selecthotelnashville.com',
                'DOMAIN' => 'selecthotelnashville.com',
            ),
            'shevetmezada.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_689_',
                'SITE_URL' => 'http://www.shevetmezada.com',
                'DOMAIN' => 'shevetmezada.com',
            ),
            'smizgraffiti.com' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_690_',
                'SITE_URL' => 'http://www.smizgraffiti.com',
                'DOMAIN' => 'smizgraffiti.com',
            ),
            'silente.info' =>
            array(
                'TABLE_SCHEMA' => 'sarahcol_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_691_',
                'SITE_URL' => 'http://www.silente.info',
                'DOMAIN' => 'silente.info',
            ),
            'ciaraconsidine.com' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1096_',
                'SITE_URL' => 'http://www.ciaraconsidine.com',
                'DOMAIN' => 'ciaraconsidine.com',
            ),
            'crappyholidays.net' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1097_',
                'SITE_URL' => 'http://www.crappyholidays.net',
                'DOMAIN' => 'crappyholidays.net',
            ),
            'eyelovefashion.com' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_836_',
                'SITE_URL' => 'http://www.eyelovefashion.com',
                'DOMAIN' => 'eyelovefashion.com',
            ),
            'smhsevents.org' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_837_',
                'SITE_URL' => 'http://www.smhsevents.org',
                'DOMAIN' => 'smhsevents.org',
            ),
            'dagnabbitdcp.com' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_838_',
                'SITE_URL' => 'http://www.dagnabbitdcp.com',
                'DOMAIN' => 'dagnabbitdcp.com',
            ),
            'teamgleasonexperience.org' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_839_',
                'SITE_URL' => 'http://www.teamgleasonexperience.org',
                'DOMAIN' => 'teamgleasonexperience.org',
            ),
            'b672.com' =>
            array(
                'TABLE_SCHEMA' => 'smhseven_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_840_',
                'SITE_URL' => 'http://www.b672.com',
                'DOMAIN' => 'b672.com',
            ),
            'theroflnetwork.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1140_',
                'SITE_URL' => 'http://www.theroflnetwork.com',
                'DOMAIN' => 'theroflnetwork.com',
            ),
            'nuitdesmines.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1141_',
                'SITE_URL' => 'http://www.nuitdesmines.com',
                'DOMAIN' => 'nuitdesmines.com',
            ),
            'savetheigloo.info' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1142_',
                'SITE_URL' => 'http://www.savetheigloo.info',
                'DOMAIN' => 'savetheigloo.info',
            ),
            'misanunciosprofesionales.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1143_',
                'SITE_URL' => 'http://www.misanunciosprofesionales.com',
                'DOMAIN' => 'misanunciosprofesionales.com',
            ),
            'viewvanguard.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1144_',
                'SITE_URL' => 'http://www.viewvanguard.com',
                'DOMAIN' => 'viewvanguard.com',
            ),
            'nemanjakrecelj.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1347_',
                'SITE_URL' => 'http://www.nemanjakrecelj.com',
                'DOMAIN' => 'nemanjakrecelj.com',
            ),
            'willhobbsoregon.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1348_',
                'SITE_URL' => 'http://www.willhobbsoregon.com',
                'DOMAIN' => 'willhobbsoregon.com',
            ),
            'advantagepandrcorp.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1349_',
                'SITE_URL' => 'http://www.advantagepandrcorp.com',
                'DOMAIN' => 'advantagepandrcorp.com',
            ),
            'elkmtnwy.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1350_',
                'SITE_URL' => 'http://www.elkmtnwy.com',
                'DOMAIN' => 'elkmtnwy.com',
            ),
            'soudertoncareandshare.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_919_',
                'SITE_URL' => 'http://www.soudertoncareandshare.com',
                'DOMAIN' => 'soudertoncareandshare.com',
            ),
            'yeuxdor.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_920_',
                'SITE_URL' => 'http://www.yeuxdor.com',
                'DOMAIN' => 'yeuxdor.com',
            ),
            'avntubes.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_921_',
                'SITE_URL' => 'http://www.avntubes.com',
                'DOMAIN' => 'avntubes.com',
            ),
            'curtinaiesec.org' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_922_',
                'SITE_URL' => 'http://www.curtinaiesec.org',
                'DOMAIN' => 'curtinaiesec.org',
            ),
            'festivaldetiteresrosetearanda.com' =>
            array(
                'TABLE_SCHEMA' => 'souderto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_923_',
                'SITE_URL' => 'http://www.festivaldetiteresrosetearanda.com',
                'DOMAIN' => 'festivaldetiteresrosetearanda.com',
            ),
            'businesslinc-fw.org' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1338_',
                'SITE_URL' => 'http://www.businesslinc-fw.org',
                'DOMAIN' => 'businesslinc-fw.org',
            ),
            'stanzelcommunications.com' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_986_',
                'SITE_URL' => 'http://www.stanzelcommunications.com',
                'DOMAIN' => 'stanzelcommunications.com',
            ),
            'jorgealonso.net' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_987_',
                'SITE_URL' => 'http://www.jorgealonso.net',
                'DOMAIN' => 'jorgealonso.net',
            ),
            'regenerationfestival.us' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_988_',
                'SITE_URL' => 'http://www.regenerationfestival.us',
                'DOMAIN' => 'regenerationfestival.us',
            ),
            'sckappadelta.com' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_989_',
                'SITE_URL' => 'http://www.sckappadelta.com',
                'DOMAIN' => 'sckappadelta.com',
            ),
            'themagicposition.net' =>
            array(
                'TABLE_SCHEMA' => 'stanzelc_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_990_',
                'SITE_URL' => 'http://www.themagicposition.net',
                'DOMAIN' => 'themagicposition.net',
            ),
            'starwarsworld.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_1_84302_',
                'SITE_URL' => 'http://www.starwarsworld.com',
                'DOMAIN' => 'starwarsworld.com',
            ),
            'thewritingsource.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_1_84303_',
                'SITE_URL' => 'http://www.thewritingsource.com',
                'DOMAIN' => 'thewritingsource.com',
            ),
            'unitedwaypolkcounty.org' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_1_84304_',
                'SITE_URL' => 'http://www.unitedwaypolkcounty.org',
                'DOMAIN' => 'unitedwaypolkcounty.org',
            ),
            'waxahachiedowntownmerchants.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_1_84305_',
                'SITE_URL' => 'http://www.waxahachiedowntownmerchants.com',
                'DOMAIN' => 'waxahachiedowntownmerchants.com',
            ),
            'famwellfed.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_1_84306_',
                'SITE_URL' => 'http://www.famwellfed.com',
                'DOMAIN' => 'famwellfed.com',
            ),
            'southeastfutsalchampionship.info' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_27_',
                'SITE_URL' => 'http://www.southeastfutsalchampionship.info',
                'DOMAIN' => 'southeastfutsalchampionship.info',
            ),
            'gryffynskeep.org' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_32_',
                'SITE_URL' => 'http://www.gryffynskeep.org',
                'DOMAIN' => 'gryffynskeep.org',
            ),
            'jltele1000.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_40_',
                'SITE_URL' => 'http://www.jltele1000.com',
                'DOMAIN' => 'jltele1000.com',
            ),
            'pemra.org' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_721_',
                'SITE_URL' => 'http://www.pemra.org',
                'DOMAIN' => 'pemra.org',
            ),
            'daniellenicoleadornments.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_73_',
                'SITE_URL' => 'http://www.daniellenicoleadornments.com',
                'DOMAIN' => 'daniellenicoleadornments.com',
            ),
            'chacaraparaiso.org' =>
            array(
                'TABLE_SCHEMA' => 'starwars_wp130',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://chacaraparaiso.org',
                'DOMAIN' => 'chacaraparaiso.org',
            ),
            'selectedbrandsdistributors.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_wp328',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://selectedbrandsdistributors.com',
                'DOMAIN' => 'selectedbrandsdistributors.com',
            ),
            'mv-1ofmassachusetts.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_wp495',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://mv-1ofmassachusetts.com',
                'DOMAIN' => 'mv-1ofmassachusetts.com',
            ),
            'harrywinston-engagementrings.biz' =>
            array(
                'TABLE_SCHEMA' => 'starwars_wp551',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://harrywinston-engagementrings.biz',
                'DOMAIN' => 'harrywinston-engagementrings.biz',
            ),
            'suki-usa-33.com' =>
            array(
                'TABLE_SCHEMA' => 'starwars_wp609',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://suki-usa-33.com',
                'DOMAIN' => 'suki-usa-33.com',
            ),
            'supernaturalfan.org' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_1_83777_',
                'SITE_URL' => 'http://www.supernaturalfan.org',
                'DOMAIN' => 'supernaturalfan.org',
            ),
            'rlopes.net' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_1_83778_',
                'SITE_URL' => 'http://www.rlopes.net',
                'DOMAIN' => 'rlopes.net',
            ),
            'wheel-of-chance-wheels.com' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_1_83779_',
                'SITE_URL' => 'http://www.wheel-of-chance-wheels.com',
                'DOMAIN' => 'wheel-of-chance-wheels.com',
            ),
            'jlresource.com' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_1_83780_',
                'SITE_URL' => 'http://www.jlresource.com',
                'DOMAIN' => 'jlresource.com',
            ),
            'paddleandtrail.net' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_1_83781_',
                'SITE_URL' => 'http://www.paddleandtrail.net',
                'DOMAIN' => 'paddleandtrail.net',
            ),
            'katherinemorrison.com' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_3_',
                'SITE_URL' => 'http://www.katherinemorrison.com',
                'DOMAIN' => 'katherinemorrison.com',
            ),
            'ohioaidshivstdhotline.info' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_4_',
                'SITE_URL' => 'http://www.ohioaidshivstdhotline.info',
                'DOMAIN' => 'ohioaidshivstdhotline.info',
            ),
            'victory-fellowship.org' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_5_',
                'SITE_URL' => 'http://www.victory-fellowship.org',
                'DOMAIN' => 'victory-fellowship.org',
            ),
            'ourfloridapromise.org' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_718_',
                'SITE_URL' => 'http://www.ourfloridapromise.org',
                'DOMAIN' => 'ourfloridapromise.org',
            ),
            'orangehara.com' =>
            array(
                'TABLE_SCHEMA' => 'supernat_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_719_',
                'SITE_URL' => 'http://www.orangehara.com',
                'DOMAIN' => 'orangehara.com',
            ),
            'newjeweleddogcollars.com' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1303_',
                'SITE_URL' => 'http://www.newjeweleddogcollars.com',
                'DOMAIN' => 'newjeweleddogcollars.com',
            ),
            'pellegrinosrestaurant.com' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1304_',
                'SITE_URL' => 'http://www.pellegrinosrestaurant.com',
                'DOMAIN' => 'pellegrinosrestaurant.com',
            ),
            'mfathleticclub.com' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1306_',
                'SITE_URL' => 'http://www.mfathleticclub.com',
                'DOMAIN' => 'mfathleticclub.com',
            ),
            'tecaltema.org' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_944_',
                'SITE_URL' => 'http://www.tecaltema.org',
                'DOMAIN' => 'tecaltema.org',
            ),
            'montanaclimatechange.net' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_945_',
                'SITE_URL' => 'http://www.montanaclimatechange.net',
                'DOMAIN' => 'montanaclimatechange.net',
            ),
            'lsondemand.com' =>
            array(
                'TABLE_SCHEMA' => 'tecaltem_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_946_',
                'SITE_URL' => 'http://www.lsondemand.com',
                'DOMAIN' => 'lsondemand.com',
            ),
            'temeculasandiegowinetours.com' =>
            array(
                'TABLE_SCHEMA' => 'temecula_tkp',
                'TABLE_PREFIX' => 'wp_1_83982_',
                'SITE_URL' => 'http://www.temeculasandiegowinetours.com',
                'DOMAIN' => 'temeculasandiegowinetours.com',
            ),
            'comedyinthousandoaks.com' =>
            array(
                'TABLE_SCHEMA' => 'temecula_tkp',
                'TABLE_PREFIX' => 'wp_1_83983_',
                'SITE_URL' => 'http://www.comedyinthousandoaks.com',
                'DOMAIN' => 'comedyinthousandoaks.com',
            ),
            'mymainstreetmarketingmachine.com' =>
            array(
                'TABLE_SCHEMA' => 'temecula_tkp',
                'TABLE_PREFIX' => 'wp_1_83984_',
                'SITE_URL' => 'http://www.mymainstreetmarketingmachine.com',
                'DOMAIN' => 'mymainstreetmarketingmachine.com',
            ),
            'soccerticketnews.com' =>
            array(
                'TABLE_SCHEMA' => 'temecula_tkp',
                'TABLE_PREFIX' => 'wp_1_83985_',
                'SITE_URL' => 'http://www.soccerticketnews.com',
                'DOMAIN' => 'soccerticketnews.com',
            ),
            'jmphones.com' =>
            array(
                'TABLE_SCHEMA' => 'temecula_tkp',
                'TABLE_PREFIX' => 'wp_1_83986_',
                'SITE_URL' => 'http://www.jmphones.com',
                'DOMAIN' => 'jmphones.com',
            ),
            'terrykelhawk.com' =>
            array(
                'TABLE_SCHEMA' => 'terrykel_tkp',
                'TABLE_PREFIX' => 'wp_1_83937_',
                'SITE_URL' => 'http://www.terrykelhawk.com',
                'DOMAIN' => 'terrykelhawk.com',
            ),
            'viankavanbokkem.com' =>
            array(
                'TABLE_SCHEMA' => 'terrykel_tkp',
                'TABLE_PREFIX' => 'wp_1_83938_',
                'SITE_URL' => 'http://www.viankavanbokkem.com',
                'DOMAIN' => 'viankavanbokkem.com',
            ),
            'ccustudents.com' =>
            array(
                'TABLE_SCHEMA' => 'terrykel_tkp',
                'TABLE_PREFIX' => 'wp_1_83939_',
                'SITE_URL' => 'http://www.ccustudents.com',
                'DOMAIN' => 'ccustudents.com',
            ),
            'whosyourmommyonline.com' =>
            array(
                'TABLE_SCHEMA' => 'terrykel_tkp',
                'TABLE_PREFIX' => 'wp_1_83940_',
                'SITE_URL' => 'http://www.whosyourmommyonline.com',
                'DOMAIN' => 'whosyourmommyonline.com',
            ),
            'rbc-granfondowhistler-bluewaterproject.com' =>
            array(
                'TABLE_SCHEMA' => 'terrykel_tkp',
                'TABLE_PREFIX' => 'wp_1_83941_',
                'SITE_URL' => 'http://www.rbc-granfondowhistler-bluewaterproject.com',
                'DOMAIN' => 'rbc-granfondowhistler-bluewaterproject.com',
            ),
            'tfditour.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_1_83757_',
                'SITE_URL' => 'http://www.tfditour.com',
                'DOMAIN' => 'tfditour.com',
            ),
            'xinhualou.info' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_25_',
                'SITE_URL' => 'http://xinhualou.info',
                'DOMAIN' => 'xinhualou.info',
            ),
            'nywzsh.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_26_',
                'SITE_URL' => 'http://www.nywzsh.com',
                'DOMAIN' => 'nywzsh.com',
            ),
            'hao6767.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_39_',
                'SITE_URL' => 'http://www.hao6767.com',
                'DOMAIN' => 'hao6767.com',
            ),
            'foodnotlawnslincoln.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_41_',
                'SITE_URL' => 'http://www.foodnotlawnslincoln.com',
                'DOMAIN' => 'foodnotlawnslincoln.com',
            ),
            'pacific-ecosystems-climate.org' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_43_',
                'SITE_URL' => 'http://www.pacific-ecosystems-climate.org',
                'DOMAIN' => 'pacific-ecosystems-climate.org',
            ),
            'rjwtransport.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_729_',
                'SITE_URL' => 'http://www.rjwtransport.com',
                'DOMAIN' => 'rjwtransport.com',
            ),
            'resourcedrillalert.com' =>
            array(
                'TABLE_SCHEMA' => 'tfditour_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_730_',
                'SITE_URL' => 'http://www.resourcedrillalert.com',
                'DOMAIN' => 'resourcedrillalert.com',
            ),
            'thecoachatbanbury.com' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_1_83857_',
                'SITE_URL' => 'http://www.thecoachatbanbury.com',
                'DOMAIN' => 'thecoachatbanbury.com',
            ),
            'jeremysmoak.com' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_1_83858_',
                'SITE_URL' => 'http://www.jeremysmoak.com',
                'DOMAIN' => 'jeremysmoak.com',
            ),
            '1beertokillsubscribecomments.com' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_1_83859_',
                'SITE_URL' => 'http://www.1beertokillsubscribecomments.com',
                'DOMAIN' => '1beertokillsubscribecomments.com',
            ),
            'unitedfamilyfellowship.com' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_1_83860_',
                'SITE_URL' => 'http://www.unitedfamilyfellowship.com',
                'DOMAIN' => 'unitedfamilyfellowship.com',
            ),
            'erinsaxseymour.com' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_1_83861_',
                'SITE_URL' => 'http://www.erinsaxseymour.com',
                'DOMAIN' => 'erinsaxseymour.com',
            ),
            'amitycares.org' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_29_',
                'SITE_URL' => 'http://www.amitycares.org',
                'DOMAIN' => 'amitycares.org',
            ),
            'apocalypseguild.info' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_45_',
                'SITE_URL' => 'http://www.apocalypseguild.info',
                'DOMAIN' => 'apocalypseguild.info',
            ),
            'greenprojectblog.org' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_46_',
                'SITE_URL' => 'http://www.greenprojectblog.org',
                'DOMAIN' => 'greenprojectblog.org',
            ),
            'fatsuperman.net' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_681_',
                'SITE_URL' => 'http://www.fatsuperman.net',
                'DOMAIN' => 'fatsuperman.net',
            ),
            'redobservatoriosterritoriales.com' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_727_',
                'SITE_URL' => 'http://www.redobservatoriosterritoriales.com',
                'DOMAIN' => 'redobservatoriosterritoriales.com',
            ),
            'resikla.com' =>
            array(
                'TABLE_SCHEMA' => 'thecoach_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_728_',
                'SITE_URL' => 'http://www.resikla.com',
                'DOMAIN' => 'resikla.com',
            ),
            'theneonmoon.com' =>
            array(
                'TABLE_SCHEMA' => 'theneonm_wp138',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.theneonmoon.com',
                'DOMAIN' => 'theneonmoon.com',
            ),
            'thenorthfactory.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_1_2670_',
                'SITE_URL' => 'http://www.thenorthfactory.com',
                'DOMAIN' => 'thenorthfactory.com',
            ),
            'thepadclimbing.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_1_2671_',
                'SITE_URL' => 'http://www.thepadclimbing.com',
                'DOMAIN' => 'thepadclimbing.com',
            ),
            'thequestpointgroup.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_1_2672_',
                'SITE_URL' => 'http://www.thequestpointgroup.com',
                'DOMAIN' => 'thequestpointgroup.com',
            ),
            'tisagoodlife.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_1_2673_',
                'SITE_URL' => 'http://www.tisagoodlife.com',
                'DOMAIN' => 'tisagoodlife.com',
            ),
            'tj-bz.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_1_2674_',
                'SITE_URL' => 'http://www.tj-bz.com',
                'DOMAIN' => 'tj-bz.com',
            ),
            'posiciona2.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_31_',
                'SITE_URL' => 'http://www.posiciona2.com',
                'DOMAIN' => 'posiciona2.com',
            ),
            'persimmonparkfremont.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_34_',
                'SITE_URL' => 'http://www.persimmonparkfremont.com',
                'DOMAIN' => 'persimmonparkfremont.com',
            ),
            'kristiriskforcongress.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_36_',
                'SITE_URL' => 'http://www.kristiriskforcongress.com',
                'DOMAIN' => 'kristiriskforcongress.com',
            ),
            'hagane.us' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_42_',
                'SITE_URL' => 'http://www.hagane.us',
                'DOMAIN' => 'hagane.us',
            ),
            'kusadasikentkonseyi.org' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_44_',
                'SITE_URL' => 'http://www.kusadasikentkonseyi.org',
                'DOMAIN' => 'kusadasikentkonseyi.org',
            ),
            'rcgjy.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_725_',
                'SITE_URL' => 'http://www.rcgjy.com',
                'DOMAIN' => 'rcgjy.com',
            ),
            'puntozerodesign.com' =>
            array(
                'TABLE_SCHEMA' => 'thenorth_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_726_',
                'SITE_URL' => 'http://www.puntozerodesign.com',
                'DOMAIN' => 'puntozerodesign.com',
            ),
            'cfa2012tampa.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1115_',
                'SITE_URL' => 'http://www.cfa2012tampa.com',
                'DOMAIN' => 'cfa2012tampa.com',
            ),
            'youthhonoravet.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1116_',
                'SITE_URL' => 'http://www.youthhonoravet.com',
                'DOMAIN' => 'youthhonoravet.com',
            ),
            'onyxtheater.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1117_',
                'SITE_URL' => 'http://www.onyxtheater.com',
                'DOMAIN' => 'onyxtheater.com',
            ),
            'duatmush.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1119_',
                'SITE_URL' => 'http://www.duatmush.com',
                'DOMAIN' => 'duatmush.com',
            ),
            'thetexanwhowouldbeking.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_896_',
                'SITE_URL' => 'http://www.thetexanwhowouldbeking.com',
                'DOMAIN' => 'thetexanwhowouldbeking.com',
            ),
            'thebestavailablescience.com' =>
            array(
                'TABLE_SCHEMA' => 'thetexan_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_898_',
                'SITE_URL' => 'http://thebestavailablescience.com',
                'DOMAIN' => 'thebestavailablescience.com',
            ),
            'thingstodoincavecreek.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_1_83942_',
                'SITE_URL' => 'http://www.thingstodoincavecreek.com',
                'DOMAIN' => 'thingstodoincavecreek.com',
            ),
            'feriadellibrotijuana.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_1_83943_',
                'SITE_URL' => 'http://www.feriadellibrotijuana.com',
                'DOMAIN' => 'feriadellibrotijuana.com',
            ),
            'avvocatiperniente.info' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_1_83944_',
                'SITE_URL' => 'http://www.avvocatiperniente.info',
                'DOMAIN' => 'avvocatiperniente.info',
            ),
            'thelegendsfromgil.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_1_83945_',
                'SITE_URL' => 'http://www.thelegendsfromgil.com',
                'DOMAIN' => 'thelegendsfromgil.com',
            ),
            'recooks.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_1_83946_',
                'SITE_URL' => 'http://www.recooks.com',
                'DOMAIN' => 'recooks.com',
            ),
            'djsmithaccounting.net' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_722_',
                'SITE_URL' => 'http://www.djsmithaccounting.net',
                'DOMAIN' => 'djsmithaccounting.net',
            ),
            'nationwidescooters.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_723_',
                'SITE_URL' => 'http://www.nationwidescooters.com',
                'DOMAIN' => 'nationwidescooters.com',
            ),
            'business-communication.info' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_724_',
                'SITE_URL' => 'http://www.business-communication.info',
                'DOMAIN' => 'business-communication.info',
            ),
            'cruise-ship-jobs.us' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_725_',
                'SITE_URL' => 'http://www.cruise-ship-jobs.us',
                'DOMAIN' => 'cruise-ship-jobs.us',
            ),
            'tubesdirectory.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_726_',
                'SITE_URL' => 'http://www.tubesdirectory.com',
                'DOMAIN' => 'tubesdirectory.com',
            ),
            'sebastiancomedy.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_13_',
                'SITE_URL' => 'http://www.sebastiancomedy.com',
                'DOMAIN' => 'sebastiancomedy.com',
            ),
            'ampaharmonia.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_wp128',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ampaharmonia.com',
                'DOMAIN' => 'ampaharmonia.com',
            ),
            'ecnucoast.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_14_',
                'SITE_URL' => 'http://www.ecnucoast.com',
                'DOMAIN' => 'ecnucoast.com',
            ),
            'admclubman.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_17_',
                'SITE_URL' => 'http://www.admclubman.com',
                'DOMAIN' => 'admclubman.com',
            ),
            'postleftanarchy.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_723_',
                'SITE_URL' => 'http://www.postleftanarchy.com',
                'DOMAIN' => 'postleftanarchy.com',
            ),
            'popdemosrus.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_wp109',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://popdemosrus.com',
                'DOMAIN' => 'popdemosrus.com',
            ),
            'juncal.org' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_wp136',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://juncal.org',
                'DOMAIN' => 'juncal.org',
            ),
            'ghostreveries.com' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_wp886',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://ghostreveries.com',
                'DOMAIN' => 'ghostreveries.com',
            ),
            'economics-articles.info' =>
            array(
                'TABLE_SCHEMA' => 'thingsto_wp960',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://economics-articles.info',
                'DOMAIN' => 'economics-articles.info',
            ),
            '360nw.info' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_1_83508_',
                'SITE_URL' => 'http://www.360nw.info',
                'DOMAIN' => '360nw.info',
            ),
            'adamgod.net' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_1_83509_',
                'SITE_URL' => 'http://www.adamgod.net',
                'DOMAIN' => 'adamgod.net',
            ),
            'alphaaz.org' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_1_83510_',
                'SITE_URL' => 'http://www.alphaaz.org',
                'DOMAIN' => 'alphaaz.org',
            ),
            'askcarrielee.net' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_1_83511_',
                'SITE_URL' => 'http://www.askcarrielee.net',
                'DOMAIN' => 'askcarrielee.net',
            ),
            'bigdaymaps.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_1_83512_',
                'SITE_URL' => 'http://www.bigdaymaps.com',
                'DOMAIN' => 'bigdaymaps.com',
            ),
            'desigvalencia.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1199_',
                'SITE_URL' => 'http://www.desigvalencia.com',
                'DOMAIN' => 'desigvalencia.com',
            ),
            'digitalgraphicsinform.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1200_',
                'SITE_URL' => 'http://www.digitalgraphicsinform.com',
                'DOMAIN' => 'digitalgraphicsinform.com',
            ),
            'dreadfilms.org' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1201_',
                'SITE_URL' => 'http://www.dreadfilms.org',
                'DOMAIN' => 'dreadfilms.org',
            ),
            'earlzpearlz.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1202_',
                'SITE_URL' => 'http://www.earlzpearlz.com',
                'DOMAIN' => 'earlzpearlz.com',
            ),
            'jactellconstruction.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1203_',
                'SITE_URL' => 'http://www.jactellconstruction.com',
                'DOMAIN' => 'jactellconstruction.com',
            ),
            'gamecocksocialrewards.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_807_',
                'SITE_URL' => 'http://www.gamecocksocialrewards.com',
                'DOMAIN' => 'gamecocksocialrewards.com',
            ),
            'outletshoes-christianlouboutin.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_808_',
                'SITE_URL' => 'http://www.outletshoes-christianlouboutin.com',
                'DOMAIN' => 'outletshoes-christianlouboutin.com',
            ),
            'sniblog.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_809_',
                'SITE_URL' => 'http://www.sniblog.com',
                'DOMAIN' => 'sniblog.com',
            ),
            'vmgsails.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_810_',
                'SITE_URL' => 'http://www.vmgsails.com',
                'DOMAIN' => 'vmgsails.com',
            ),
            'isspa2009.com' =>
            array(
                'TABLE_SCHEMA' => 'threesix_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_811_',
                'SITE_URL' => 'http://www.isspa2009.com',
                'DOMAIN' => 'isspa2009.com',
            ),
            'tk6688.net' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_1_2675_',
                'SITE_URL' => 'http://www.tk6688.net',
                'DOMAIN' => 'tk6688.net',
            ),
            'toadkiss.com' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_1_2676_',
                'SITE_URL' => 'http://www.toadkiss.com',
                'DOMAIN' => 'toadkiss.com',
            ),
            'tri-lakestool.com' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_1_2677_',
                'SITE_URL' => 'http://www.tri-lakestool.com',
                'DOMAIN' => 'tri-lakestool.com',
            ),
            'uqia.org' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_1_2678_',
                'SITE_URL' => 'http://www.uqia.org',
                'DOMAIN' => 'uqia.org',
            ),
            'usarcherydreamteam.org' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_1_2679_',
                'SITE_URL' => 'http://www.usarcherydreamteam.org',
                'DOMAIN' => 'usarcherydreamteam.org',
            ),
            'ppvalencia.com' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_10_',
                'SITE_URL' => 'http://www.ppvalencia.com',
                'DOMAIN' => 'ppvalencia.com',
            ),
            'retrojordansalescheap.com' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_11_',
                'SITE_URL' => 'http://www.retrojordansalescheap.com',
                'DOMAIN' => 'retrojordansalescheap.com',
            ),
            'santyesprogramadorynografista.net' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_12_',
                'SITE_URL' => 'http://www.santyesprogramadorynografista.net',
                'DOMAIN' => 'santyesprogramadorynografista.net',
            ),
            'pivotpointfilms.com' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_722_',
                'SITE_URL' => 'http://www.pivotpointfilms.com',
                'DOMAIN' => 'pivotpointfilms.com',
            ),
            'grupo-de-apoyo.org' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_wp936',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://grupo-de-apoyo.org',
                'DOMAIN' => 'grupo-de-apoyo.org',
            ),
            'inletemergencyservices.com' =>
            array(
                'TABLE_SCHEMA' => 'tk6688_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_9_',
                'SITE_URL' => 'inletemergencyservices.com',
                'DOMAIN' => 'inletemergencyservices.com',
            ),
            'tomdunlop.com' =>
            array(
                'TABLE_SCHEMA' => 'tomdunlo_tkp',
                'TABLE_PREFIX' => 'wp_1_2365_',
                'SITE_URL' => 'http://www.tomdunlop.com',
                'DOMAIN' => 'tomdunlop.com',
            ),
            'mtcjabalpur.com' =>
            array(
                'TABLE_SCHEMA' => 'tomdunlo_tkp',
                'TABLE_PREFIX' => 'wp_1_2366_',
                'SITE_URL' => 'http://www.mtcjabalpur.com',
                'DOMAIN' => 'mtcjabalpur.com',
            ),
            'mul-ift.com' =>
            array(
                'TABLE_SCHEMA' => 'tomdunlo_tkp',
                'TABLE_PREFIX' => 'wp_1_2367_',
                'SITE_URL' => 'http://www.mul-ift.com',
                'DOMAIN' => 'mul-ift.com',
            ),
            'musclerelaxing.com' =>
            array(
                'TABLE_SCHEMA' => 'tomdunlo_tkp',
                'TABLE_PREFIX' => 'wp_1_2368_',
                'SITE_URL' => 'http://www.musclerelaxing.com',
                'DOMAIN' => 'musclerelaxing.com',
            ),
            'uiclarama.com' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_1_81148_',
                'SITE_URL' => 'http://www.uiclarama.com',
                'DOMAIN' => 'uiclarama.com',
            ),
            'tsegal.net' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_1_81282_',
                'SITE_URL' => 'http://www.tsegal.net',
                'DOMAIN' => 'tsegal.net',
            ),
            'tulsaprofessionalresumes.com' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_1_81283_',
                'SITE_URL' => 'http://www.tulsaprofessionalresumes.com',
                'DOMAIN' => 'tulsaprofessionalresumes.com',
            ),
            'tywell.net' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_1_81284_',
                'SITE_URL' => 'http://www.tywell.net',
                'DOMAIN' => 'tywell.net',
            ),
            'ucrealcleaning.biz' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_1_81285_',
                'SITE_URL' => 'http://www.ucrealcleaning.biz',
                'DOMAIN' => 'ucrealcleaning.biz',
            ),
            'calvaryshreveport.net' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1019_',
                'SITE_URL' => 'http://www.calvaryshreveport.net',
                'DOMAIN' => 'calvaryshreveport.net',
            ),
            'bendcyclery.org' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_322_',
                'SITE_URL' => 'http://www.bendcyclery.org',
                'DOMAIN' => 'bendcyclery.org',
            ),
            'illustratorsforkids.com' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_324_',
                'SITE_URL' => 'http://www.illustratorsforkids.com',
                'DOMAIN' => 'illustratorsforkids.com',
            ),
            'fucopez.net' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_337_',
                'SITE_URL' => 'http://www.fucopez.net',
                'DOMAIN' => 'fucopez.net',
            ),
            'chambersburgreads.org' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_343_',
                'SITE_URL' => 'http://www.chambersburgreads.org',
                'DOMAIN' => 'chambersburgreads.org',
            ),
            'whatisproblem.com' =>
            array(
                'TABLE_SCHEMA' => 'tsegal_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_365_',
                'SITE_URL' => 'http://www.whatisproblem.com',
                'DOMAIN' => 'whatisproblem.com',
            ),
            'tumalditamadre.net' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_1_83643_',
                'SITE_URL' => 'http://www.tumalditamadre.net',
                'DOMAIN' => 'tumalditamadre.net',
            ),
            'tourphi-phi.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_1_83644_',
                'SITE_URL' => 'http://www.tourphi-phi.com',
                'DOMAIN' => 'tourphi-phi.com',
            ),
            'tourphuket-phangnga.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_1_83645_',
                'SITE_URL' => 'http://www.tourphuket-phangnga.com',
                'DOMAIN' => 'tourphuket-phangnga.com',
            ),
            'tourphuket-phiphi.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_1_83646_',
                'SITE_URL' => 'http://www.tourphuket-phiphi.com',
                'DOMAIN' => 'tourphuket-phiphi.com',
            ),
            'elgaleme.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_1_83647_',
                'SITE_URL' => 'http://www.elgaleme.com',
                'DOMAIN' => 'elgaleme.com',
            ),
            'anualhost.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1017_',
                'SITE_URL' => 'http://www.anualhost.com',
                'DOMAIN' => 'anualhost.com',
            ),
            'baltimorefitnesstraining.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1018_',
                'SITE_URL' => 'http://www.baltimorefitnesstraining.com',
                'DOMAIN' => 'baltimorefitnesstraining.com',
            ),
            'wyandottefsc.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_314_',
                'SITE_URL' => 'http://www.wyandottefsc.com',
                'DOMAIN' => 'wyandottefsc.com',
            ),
            'leminotaure.org' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_325_',
                'SITE_URL' => 'http://www.leminotaure.org',
                'DOMAIN' => 'leminotaure.org',
            ),
            'johnmuirpta.com' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_330_',
                'SITE_URL' => 'http://www.johnmuirpta.com',
                'DOMAIN' => 'johnmuirpta.com',
            ),
            'rousseau-2012.net' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_332_',
                'SITE_URL' => 'http://www.rousseau-2012.net',
                'DOMAIN' => 'rousseau-2012.net',
            ),
            'extreme-linux.org' =>
            array(
                'TABLE_SCHEMA' => 'tumaldit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_334_',
                'SITE_URL' => 'http://www.extreme-linux.org',
                'DOMAIN' => 'extreme-linux.org',
            ),
            'twilightwalkingtours.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_wp816',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.twilightwalkingtours.com',
                'DOMAIN' => 'twilightwalkingtours.com',
            ),
            'uniquelywalden.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_1_83569_',
                'SITE_URL' => 'http://www.uniquelywalden.com',
                'DOMAIN' => 'uniquelywalden.com',
            ),
            'vcadillac.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_1_83570_',
                'SITE_URL' => 'http://www.vcadillac.com',
                'DOMAIN' => 'vcadillac.com',
            ),
            'votealbright.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_1_83571_',
                'SITE_URL' => 'http://www.votealbright.com',
                'DOMAIN' => 'votealbright.com',
            ),
            'whitemagickjewelry.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_1_83572_',
                'SITE_URL' => 'http://www.whitemagickjewelry.com',
                'DOMAIN' => 'whitemagickjewelry.com',
            ),
            'beachresorts.biz' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1016_',
                'SITE_URL' => 'http://www.beachresorts.biz',
                'DOMAIN' => 'beachresorts.biz',
            ),
            'forex-now.info' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1285_',
                'SITE_URL' => 'http://www.forex-now.info',
                'DOMAIN' => 'forex-now.info',
            ),
            'forexadvice101.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1286_',
                'SITE_URL' => 'http://www.forexadvice101.com',
                'DOMAIN' => 'forexadvice101.com',
            ),
            'freedommedicalrobotics.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1287_',
                'SITE_URL' => 'http://www.freedommedicalrobotics.com',
                'DOMAIN' => 'freedommedicalrobotics.com',
            ),
            'gardenstatebhangra.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1288_',
                'SITE_URL' => 'http://www.gardenstatebhangra.com',
                'DOMAIN' => 'gardenstatebhangra.com',
            ),
            'zglf1890.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_315_',
                'SITE_URL' => 'http://www.zglf1890.com',
                'DOMAIN' => 'zglf1890.com',
            ),
            'playgroundmusic.us' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_327_',
                'SITE_URL' => 'http://www.playgroundmusic.us',
                'DOMAIN' => 'playgroundmusic.us',
            ),
            'wetestyouprofit.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_328_',
                'SITE_URL' => 'http://www.wetestyouprofit.com',
                'DOMAIN' => 'wetestyouprofit.com',
            ),
            'epicstreetchoir.com' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_331_',
                'SITE_URL' => 'http://www.epicstreetchoir.com',
                'DOMAIN' => 'epicstreetchoir.com',
            ),
            'studentemp.org' =>
            array(
                'TABLE_SCHEMA' => 'twilight_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_336_',
                'SITE_URL' => 'http://www.studentemp.org',
                'DOMAIN' => 'studentemp.org',
            ),
            '20ate.org' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_1_81005_',
                'SITE_URL' => 'http://www.20ate.org',
                'DOMAIN' => '20ate.org',
            ),
            'allianceopen.org' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_1_81006_',
                'SITE_URL' => 'http://www.allianceopen.org',
                'DOMAIN' => 'allianceopen.org',
            ),
            'ankarayenikoy.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_1_81007_',
                'SITE_URL' => 'http://www.ankarayenikoy.com',
                'DOMAIN' => 'ankarayenikoy.com',
            ),
            'keaganspub.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1209_',
                'SITE_URL' => 'http://www.keaganspub.com',
                'DOMAIN' => 'keaganspub.com',
            ),
            'leoestevez.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1210_',
                'SITE_URL' => 'http://www.leoestevez.com',
                'DOMAIN' => 'leoestevez.com',
            ),
            'levismithmusic.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1211_',
                'SITE_URL' => 'http://www.levismithmusic.com',
                'DOMAIN' => 'levismithmusic.com',
            ),
            'loathinglola.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1212_',
                'SITE_URL' => 'http://www.loathinglola.com',
                'DOMAIN' => 'loathinglola.com',
            ),
            'marketingbooster.biz' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1213_',
                'SITE_URL' => 'http://www.marketingbooster.biz',
                'DOMAIN' => 'marketingbooster.biz',
            ),
            'karagozmuzesi.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_817_',
                'SITE_URL' => 'http://www.karagozmuzesi.com',
                'DOMAIN' => 'karagozmuzesi.com',
            ),
            'lospinos50k.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_818_',
                'SITE_URL' => 'http://www.lospinos50k.com',
                'DOMAIN' => 'lospinos50k.com',
            ),
            'mansfieldoilonline.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_819_',
                'SITE_URL' => 'http://www.mansfieldoilonline.com',
                'DOMAIN' => 'mansfieldoilonline.com',
            ),
            'siegenprofessional.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_820_',
                'SITE_URL' => 'http://www.siegenprofessional.com',
                'DOMAIN' => 'siegenprofessional.com',
            ),
            'sigmanuhouston.com' =>
            array(
                'TABLE_SCHEMA' => 'twozeroa_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_821_',
                'SITE_URL' => 'http://www.sigmanuhouston.com',
                'DOMAIN' => 'sigmanuhouston.com',
            ),
            'mgphotographe.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_289_',
                'SITE_URL' => 'http://www.mgphotographe.com',
                'DOMAIN' => 'mgphotographe.com',
            ),
            'mail.18jeux.com' =>
            array(
                'TABLE_SCHEMA' => 'uawlocal_joomla',
                'TABLE_PREFIX' => 'wp_pkp1_1965_',
                'SITE_URL' => 'http://mail.18jeux.com',
                'DOMAIN' => 'mail.18jeux.com',
            ),
            'uawlocal913.com' =>
            array(
                'TABLE_SCHEMA' => 'uawlocal_wp419',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.uawlocal913.com',
                'DOMAIN' => 'uawlocal913.com',
            ),
            'shambhallafengshui.com' =>
            array(
                'TABLE_SCHEMA' => 'uawlocal_tkp',
                'TABLE_PREFIX' => 'wp_1_83397_',
                'SITE_URL' => 'http://www.shambhallafengshui.com',
                'DOMAIN' => 'shambhallafengshui.com',
            ),
            'israelisreal.org' =>
            array(
                'TABLE_SCHEMA' => 'uawlocal_wp553',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.israelisreal.org',
                'DOMAIN' => 'israelisreal.org',
            ),
            '1-87ridefind.com' =>
            array(
                'TABLE_SCHEMA' => 'uawlocal_tkp',
                'TABLE_PREFIX' => 'wp_1_83399_',
                'SITE_URL' => 'http://www.1-87ridefind.com',
                'DOMAIN' => '1-87ridefind.com',
            ),
            'licensex.com' =>
            array(
                'TABLE_SCHEMA' => 'uawlocal_tkp',
                'TABLE_PREFIX' => 'wp_1_83400_',
                'SITE_URL' => 'http://www.licensex.com',
                'DOMAIN' => 'licensex.com',
            ),
            'unionhillpizza.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_1_83160_',
                'SITE_URL' => 'http://www.unionhillpizza.com',
                'DOMAIN' => 'unionhillpizza.com',
            ),
            '086sg.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_1_83161_',
                'SITE_URL' => 'http://www.086sg.com',
                'DOMAIN' => '086sg.com',
            ),
            '21stcenturycoalminer.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_1_83162_',
                'SITE_URL' => 'http://www.21stcenturycoalminer.com',
                'DOMAIN' => '21stcenturycoalminer.com',
            ),
            '21toy.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_1_83163_',
                'SITE_URL' => 'http://www.21toy.com',
                'DOMAIN' => '21toy.com',
            ),
            '368la.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_1_83164_',
                'SITE_URL' => 'http://www.368la.com',
                'DOMAIN' => '368la.com',
            ),
            'abaima.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_21_',
                'SITE_URL' => 'http://www.abaima.com',
                'DOMAIN' => 'abaima.com',
            ),
            'accas2011.org' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_22_',
                'SITE_URL' => 'http://www.accas2011.org',
                'DOMAIN' => 'accas2011.org',
            ),
            'thechicagohubrestaurant.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_23_',
                'SITE_URL' => 'http://www.thechicagohubrestaurant.com',
                'DOMAIN' => 'thechicagohubrestaurant.com',
            ),
            'thesalesoperator.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_24_',
                'SITE_URL' => 'http://www.thesalesoperator.com',
                'DOMAIN' => 'thesalesoperator.com',
            ),
            'cheapcomputers.biz' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_25_',
                'SITE_URL' => 'http://www.cheapcomputers.biz',
                'DOMAIN' => 'cheapcomputers.biz',
            ),
            'roanokesbdc.org' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_26_',
                'SITE_URL' => 'http://www.roanokesbdc.org',
                'DOMAIN' => 'roanokesbdc.org',
            ),
            'omurraygolf.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1014_',
                'SITE_URL' => 'http://www.omurraygolf.com',
                'DOMAIN' => 'omurraygolf.com',
            ),
            'orangetiecomputers.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1015_',
                'SITE_URL' => 'http://www.orangetiecomputers.com',
                'DOMAIN' => 'orangetiecomputers.com',
            ),
            'greenherbgarden.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1280_',
                'SITE_URL' => 'http://www.greenherbgarden.com',
                'DOMAIN' => 'greenherbgarden.com',
            ),
            'greenteaandhealth.info' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1281_',
                'SITE_URL' => 'http://www.greenteaandhealth.info',
                'DOMAIN' => 'greenteaandhealth.info',
            ),
            'interui.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1282_',
                'SITE_URL' => 'http://www.interui.com',
                'DOMAIN' => 'interui.com',
            ),
            'equityhomeloanscredit.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1283_',
                'SITE_URL' => 'http://www.equityhomeloanscredit.com',
                'DOMAIN' => 'equityhomeloanscredit.com',
            ),
            'emarketingmeister.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1284_',
                'SITE_URL' => 'http://www.emarketingmeister.com',
                'DOMAIN' => 'emarketingmeister.com',
            ),
            'buildhealthyhomes.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1451_',
                'SITE_URL' => 'http://www.buildhealthyhomes.com',
                'DOMAIN' => 'buildhealthyhomes.com',
            ),
            'cebglobalhealth.org' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1453_',
                'SITE_URL' => 'http://www.cebglobalhealth.org',
                'DOMAIN' => 'cebglobalhealth.org',
            ),
            'chipmandentalproducts.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1455_',
                'SITE_URL' => 'http://www.chipmandentalproducts.com',
                'DOMAIN' => 'chipmandentalproducts.com',
            ),
            'pallatraxusa.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_311_',
                'SITE_URL' => 'http://www.pallatraxusa.com',
                'DOMAIN' => 'pallatraxusa.com',
            ),
            'delshannoncarshow.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_312_',
                'SITE_URL' => 'http://www.delshannoncarshow.com',
                'DOMAIN' => 'delshannoncarshow.com',
            ),
            'bcpba.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_323_',
                'SITE_URL' => 'http://www.bcpba.com',
                'DOMAIN' => 'bcpba.com',
            ),
            'merchantaccountusa.us' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_329_',
                'SITE_URL' => 'http://www.merchantaccountusa.us',
                'DOMAIN' => 'merchantaccountusa.us',
            ),
            'upstairsroomwithaview.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_364_',
                'SITE_URL' => 'http://www.upstairsroomwithaview.com',
                'DOMAIN' => 'upstairsroomwithaview.com',
            ),
            'thestresswhisperer.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_wp264',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.thestresswhisperer.com',
                'DOMAIN' => 'thestresswhisperer.com',
            ),
            'theoverbrookbrothersmovie.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_wp133',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.theoverbrookbrothersmovie.com',
                'DOMAIN' => 'theoverbrookbrothersmovie.com',
            ),
            'thevillageinnhotel.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_wp656',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.thevillageinnhotel.com',
                'DOMAIN' => 'thevillageinnhotel.com',
            ),
            'theoverbrookbrothersthefilm.com' =>
            array(
                'TABLE_SCHEMA' => 'unionhil_wp862',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.theoverbrookbrothersthefilm.com',
                'DOMAIN' => 'theoverbrookbrothersthefilm.com',
            ),
            'usedcartradersonline.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_1_2680_',
                'SITE_URL' => 'http://www.usedcartradersonline.com',
                'DOMAIN' => 'usedcartradersonline.com',
            ),
            'vecon.net' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_1_2681_',
                'SITE_URL' => 'http://www.vecon.net',
                'DOMAIN' => 'vecon.net',
            ),
            'voteforjenn.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_1_2682_',
                'SITE_URL' => 'http://www.voteforjenn.com',
                'DOMAIN' => 'voteforjenn.com',
            ),
            'welshrealtor.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_1_2683_',
                'SITE_URL' => 'http://www.welshrealtor.com',
                'DOMAIN' => 'welshrealtor.com',
            ),
            'wheelsformovies.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_1_2684_',
                'SITE_URL' => 'http://www.wheelsformovies.com',
                'DOMAIN' => 'wheelsformovies.com',
            ),
            'ecommercedesigns.biz' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1275_',
                'SITE_URL' => 'http://www.ecommercedesigns.biz',
                'DOMAIN' => 'ecommercedesigns.biz',
            ),
            'fitness-health-nutrition.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1276_',
                'SITE_URL' => 'http://www.fitness-health-nutrition.com',
                'DOMAIN' => 'fitness-health-nutrition.com',
            ),
            'fusefitnessplan.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1277_',
                'SITE_URL' => 'http://www.fusefitnessplan.com',
                'DOMAIN' => 'fusefitnessplan.com',
            ),
            'footfetishpictures.org' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1278_',
                'SITE_URL' => 'http://www.footfetishpictures.org',
                'DOMAIN' => 'footfetishpictures.org',
            ),
            'g2gtomyspace.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1279_',
                'SITE_URL' => 'http://www.g2gtomyspace.com',
                'DOMAIN' => 'g2gtomyspace.com',
            ),
            'auctiontwo.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1446_',
                'SITE_URL' => 'http://www.auctiontwo.com',
                'DOMAIN' => 'auctiontwo.com',
            ),
            'atlantahostexpert.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1447_',
                'SITE_URL' => 'http://www.atlantahostexpert.com',
                'DOMAIN' => 'atlantahostexpert.com',
            ),
            'xxx-cite.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1448_',
                'SITE_URL' => 'http://www.xxx-cite.com',
                'DOMAIN' => 'xxx-cite.com',
            ),
            'bookclique.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1449_',
                'SITE_URL' => 'http://www.bookclique.com',
                'DOMAIN' => 'bookclique.com',
            ),
            'brewcitybloodbath.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1450_',
                'SITE_URL' => 'http://www.brewcitybloodbath.com',
                'DOMAIN' => 'brewcitybloodbath.com',
            ),
            'noplan2006.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_316_',
                'SITE_URL' => 'http://www.noplan2006.com',
                'DOMAIN' => 'noplan2006.com',
            ),
            'cchexp.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_317_',
                'SITE_URL' => 'http://www.cchexp.com',
                'DOMAIN' => 'cchexp.com',
            ),
            'ramthestrait.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_319_',
                'SITE_URL' => 'http://www.ramthestrait.com',
                'DOMAIN' => 'ramthestrait.com',
            ),
            'streetkidscusco.org' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_320_',
                'SITE_URL' => 'http://www.streetkidscusco.org',
                'DOMAIN' => 'streetkidscusco.org',
            ),
            'belize-maya-2012.com' =>
            array(
                'TABLE_SCHEMA' => 'usedcart_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_321_',
                'SITE_URL' => 'http://www.belize-maya-2012.com',
                'DOMAIN' => 'belize-maya-2012.com',
            ),
            'videohd.ws' =>
            array(
                'TABLE_SCHEMA' => 'videohdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_692_',
                'SITE_URL' => 'http://www.videohd.ws',
                'DOMAIN' => 'videohd.ws',
            ),
            'mingyegongfutaijiclub.com' =>
            array(
                'TABLE_SCHEMA' => 'videohdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_694_',
                'SITE_URL' => 'http://www.mingyegongfutaijiclub.com',
                'DOMAIN' => 'mingyegongfutaijiclub.com',
            ),
            'my-form.biz' =>
            array(
                'TABLE_SCHEMA' => 'videohdw_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_696_',
                'SITE_URL' => 'http://www.my-form.biz',
                'DOMAIN' => 'my-form.biz',
            ),
            'tri4number1.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1135_',
                'SITE_URL' => 'http://www.tri4number1.com',
                'DOMAIN' => 'tri4number1.com',
            ),
            'abjjcphotos.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1136_',
                'SITE_URL' => 'http://www.abjjcphotos.com',
                'DOMAIN' => 'abjjcphotos.com',
            ),
            '70for70.org' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1137_',
                'SITE_URL' => 'http://www.70for70.org',
                'DOMAIN' => '70for70.org',
            ),
            'frying-bacon-naked.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1138_',
                'SITE_URL' => 'http://www.frying-bacon-naked.com',
                'DOMAIN' => 'frying-bacon-naked.com',
            ),
            'aimencryption.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1139_',
                'SITE_URL' => 'http://www.aimencryption.com',
                'DOMAIN' => 'aimencryption.com',
            ),
            'mercy25th.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1346_',
                'SITE_URL' => 'http://www.mercy25th.com',
                'DOMAIN' => 'mercy25th.com',
            ),
            'vip-kidsonline.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_914_',
                'SITE_URL' => 'http://www.vip-kidsonline.com',
                'DOMAIN' => 'vip-kidsonline.com',
            ),
            'rednegocioscaintra.com' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_915_',
                'SITE_URL' => 'http://www.rednegocioscaintra.com',
                'DOMAIN' => 'rednegocioscaintra.com',
            ),
            'thepublicstudio.org' =>
            array(
                'TABLE_SCHEMA' => 'vipkidso_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_916_',
                'SITE_URL' => 'http://www.thepublicstudio.org',
                'DOMAIN' => 'thepublicstudio.org',
            ),
            'nribusiness.com' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1120_',
                'SITE_URL' => 'http://www.nribusiness.com',
                'DOMAIN' => 'nribusiness.com',
            ),
            'vsdonline.us' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1121_',
                'SITE_URL' => 'http://www.vsdonline.us',
                'DOMAIN' => 'vsdonline.us',
            ),
            'makersofheaven.net' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1122_',
                'SITE_URL' => 'http://www.makersofheaven.net',
                'DOMAIN' => 'makersofheaven.net',
            ),
            'sherlockplayground.org' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1123_',
                'SITE_URL' => 'http://www.sherlockplayground.org',
                'DOMAIN' => 'sherlockplayground.org',
            ),
            'homeandentertaining.com' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1124_',
                'SITE_URL' => 'http://www.homeandentertaining.com',
                'DOMAIN' => 'homeandentertaining.com',
            ),
            'mictabscoob.com' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_900_',
                'SITE_URL' => 'http://www.mictabscoob.com',
                'DOMAIN' => 'mictabscoob.com',
            ),
            'visualshared.com' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_901_',
                'SITE_URL' => 'http://www.visualshared.com',
                'DOMAIN' => 'visualshared.com',
            ),
            'gphtc.org' =>
            array(
                'TABLE_SCHEMA' => 'visualsh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_902_',
                'SITE_URL' => 'http://www.gphtc.org',
                'DOMAIN' => 'gphtc.org',
            ),
            'wangjingbao.com' =>
            array(
                'TABLE_SCHEMA' => 'wangjing_tkp',
                'TABLE_PREFIX' => 'wp_1_2531_',
                'SITE_URL' => 'http://www.wangjingbao.com',
                'DOMAIN' => 'wangjingbao.com',
            ),
            'weaningbabies.info' =>
            array(
                'TABLE_SCHEMA' => 'wangjing_tkp',
                'TABLE_PREFIX' => 'wp_1_2532_',
                'SITE_URL' => 'http://www.weaningbabies.info',
                'DOMAIN' => 'weaningbabies.info',
            ),
            'wehatefacebook.com' =>
            array(
                'TABLE_SCHEMA' => 'wangjing_tkp',
                'TABLE_PREFIX' => 'wp_1_2533_',
                'SITE_URL' => 'http://www.wehatefacebook.com',
                'DOMAIN' => 'wehatefacebook.com',
            ),
            'weiygon.com' =>
            array(
                'TABLE_SCHEMA' => 'wangjing_tkp',
                'TABLE_PREFIX' => 'wp_1_2534_',
                'SITE_URL' => 'http://www.weiygon.com',
                'DOMAIN' => 'weiygon.com',
            ),
            'wikileaks13.com' =>
            array(
                'TABLE_SCHEMA' => 'wangjing_tkp',
                'TABLE_PREFIX' => 'wp_1_2535_',
                'SITE_URL' => 'http://www.wikileaks13.com',
                'DOMAIN' => 'wikileaks13.com',
            ),
            'nmcatoday.info' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_1_83882_',
                'SITE_URL' => 'http://www.nmcatoday.info',
                'DOMAIN' => 'nmcatoday.info',
            ),
            'webats.net' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_1_83883_',
                'SITE_URL' => 'http://www.webats.net',
                'DOMAIN' => 'webats.net',
            ),
            'triangolorestaurant.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_1_83884_',
                'SITE_URL' => 'http://www.triangolorestaurant.com',
                'DOMAIN' => 'triangolorestaurant.com',
            ),
            'montrealbiz.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_1_83885_',
                'SITE_URL' => 'http://www.montrealbiz.com',
                'DOMAIN' => 'montrealbiz.com',
            ),
            'wawaareavictimservices.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_1_83886_',
                'SITE_URL' => 'http://www.wawaareavictimservices.com',
                'DOMAIN' => 'wawaareavictimservices.com',
            ),
            'myguitarsherpa.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_100_',
                'SITE_URL' => 'http://www.myguitarsherpa.com',
                'DOMAIN' => 'myguitarsherpa.com',
            ),
            'nithyadesigns.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_101_',
                'SITE_URL' => 'http://www.nithyadesigns.com',
                'DOMAIN' => 'nithyadesigns.com',
            ),
            'snapstylus.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_102_',
                'SITE_URL' => 'http://www.snapstylus.com',
                'DOMAIN' => 'snapstylus.com',
            ),
            'wemonster.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_103_',
                'SITE_URL' => 'http://www.wemonster.com',
                'DOMAIN' => 'wemonster.com',
            ),
            'twosisterscandybouquets.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp3_104_',
                'SITE_URL' => 'http://www.twosisterscandybouquets.com',
                'DOMAIN' => 'twosisterscandybouquets.com',
            ),
            'vcestudent.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_wp843',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://vcestudent.com',
                'DOMAIN' => 'vcestudent.com',
            ),
            'dustinjswatson.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_wp635',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://dustinjswatson.com',
                'DOMAIN' => 'dustinjswatson.com',
            ),
            'nostrum.ws' =>
            array(
                'TABLE_SCHEMA' => 'webats_wp288',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://nostrum.ws',
                'DOMAIN' => 'nostrum.ws',
            ),
            'flagman-mmm.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_wp689',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://flagman-mmm.com',
                'DOMAIN' => 'flagman-mmm.com',
            ),
            'spellbound-wordgame.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1012_',
                'SITE_URL' => 'http://www.spellbound-wordgame.com',
                'DOMAIN' => 'spellbound-wordgame.com',
            ),
            'steelhorseauto.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1013_',
                'SITE_URL' => 'http://www.steelhorseauto.com',
                'DOMAIN' => 'steelhorseauto.com',
            ),
            'fourseasonsistanbulnews.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_265_',
                'SITE_URL' => 'http://www.fourseasonsistanbulnews.com',
                'DOMAIN' => 'fourseasonsistanbulnews.com',
            ),
            'rebelalliance.ws' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_309_',
                'SITE_URL' => 'http://www.rebelalliance.ws',
                'DOMAIN' => 'rebelalliance.ws',
            ),
            '4onthefloormusic.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_310_',
                'SITE_URL' => 'http://www.4onthefloormusic.com',
                'DOMAIN' => '4onthefloormusic.com',
            ),
            'edu-osha.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_313_',
                'SITE_URL' => 'http://www.edu-osha.com',
                'DOMAIN' => 'edu-osha.com',
            ),
            'bunjoscomedyclub.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_326_',
                'SITE_URL' => 'http://www.bunjoscomedyclub.com',
                'DOMAIN' => 'bunjoscomedyclub.com',
            ),
            'peninsulaaptcouncil.com' =>
            array(
                'TABLE_SCHEMA' => 'webats_wp45',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://peninsulaaptcouncil.com',
                'DOMAIN' => 'peninsulaaptcouncil.com',
            ),
            'youthvision2050.org' =>
            array(
                'TABLE_SCHEMA' => 'weecours_wp494',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.youthvision2050.org',
                'DOMAIN' => 'youthvision2050.org',
            ),
            'weecourseatwilliamscreek.com' =>
            array(
                'TABLE_SCHEMA' => 'weecours_wp814',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://www.weecourseatwilliamscreek.com',
                'DOMAIN' => 'weecourseatwilliamscreek.com',
            ),
            'becas.ws' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wpK_145_',
                'SITE_URL' => 'http://www.becas.ws',
                'DOMAIN' => 'becas.ws',
            ),
            'andrearakocevic.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wpK_146_',
                'SITE_URL' => 'http://www.andrearakocevic.com',
                'DOMAIN' => 'andrearakocevic.com',
            ),
            'wenyanshuhua.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_1_82543_',
                'SITE_URL' => 'http://www.wenyanshuhua.com',
                'DOMAIN' => 'wenyanshuhua.com',
            ),
            'kiss-eyes.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_1_82544_',
                'SITE_URL' => 'http://www.kiss-eyes.com',
                'DOMAIN' => 'kiss-eyes.com',
            ),
            'hiphopshowbiz.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_1_82545_',
                'SITE_URL' => 'http://www.hiphopshowbiz.com',
                'DOMAIN' => 'hiphopshowbiz.com',
            ),
            'zhengyihe.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_1_82546_',
                'SITE_URL' => 'http://www.zhengyihe.com',
                'DOMAIN' => 'zhengyihe.com',
            ),
            'kaynaknet.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_1_82547_',
                'SITE_URL' => 'http://www.kaynaknet.com',
                'DOMAIN' => 'kaynaknet.com',
            ),
            'realestatecentrum.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1010_',
                'SITE_URL' => 'http://www.realestatecentrum.com',
                'DOMAIN' => 'realestatecentrum.com',
            ),
            'savingwithrain.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1011_',
                'SITE_URL' => 'http://www.savingwithrain.com',
                'DOMAIN' => 'savingwithrain.com',
            ),
            'jobsatworthingtonschools.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1271_',
                'SITE_URL' => 'http://www.jobsatworthingtonschools.com',
                'DOMAIN' => 'jobsatworthingtonschools.com',
            ),
            'keadilansarawak.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1272_',
                'SITE_URL' => 'http://www.keadilansarawak.com',
                'DOMAIN' => 'keadilansarawak.com',
            ),
            'kelsogc.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1273_',
                'SITE_URL' => 'http://www.kelsogc.com',
                'DOMAIN' => 'kelsogc.com',
            ),
            'lucidmover.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1274_',
                'SITE_URL' => 'http://www.lucidmover.com',
                'DOMAIN' => 'lucidmover.com',
            ),
            'katie-faulkner.com' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_297_',
                'SITE_URL' => 'http://www.katie-faulkner.com',
                'DOMAIN' => 'katie-faulkner.com',
            ),
            'pozycjonowaniekatowice.info' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_299_',
                'SITE_URL' => 'http://www.pozycjonowaniekatowice.info',
                'DOMAIN' => 'pozycjonowaniekatowice.info',
            ),
            'southeastfutsalchampionship.org' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_305_',
                'SITE_URL' => 'http://www.southeastfutsalchampionship.org',
                'DOMAIN' => 'southeastfutsalchampionship.org',
            ),
            'pozycjonowanie-poznan.info' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_307_',
                'SITE_URL' => 'http://www.pozycjonowanie-poznan.info',
                'DOMAIN' => 'pozycjonowanie-poznan.info',
            ),
            'pozycjonowanie-warszawa.info' =>
            array(
                'TABLE_SCHEMA' => 'wenyansh_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_308_',
                'SITE_URL' => 'http://www.pozycjonowanie-warszawa.info',
                'DOMAIN' => 'pozycjonowanie-warszawa.info',
            ),
            'what-can-neurofeedback-help.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_1_81149_',
                'SITE_URL' => 'http://www.what-can-neurofeedback-help.com',
                'DOMAIN' => 'what-can-neurofeedback-help.com',
            ),
            'whitecristalsb.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_1_81292_',
                'SITE_URL' => 'http://www.whitecristalsb.com',
                'DOMAIN' => 'whitecristalsb.com',
            ),
            'x-servers.net' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_1_81293_',
                'SITE_URL' => 'http://www.x-servers.net',
                'DOMAIN' => 'x-servers.net',
            ),
            'wsi-solar.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_1_84422_',
                'SITE_URL' => 'http://www.wsi-solar.com',
                'DOMAIN' => 'wsi-solar.com',
            ),
            'petexpose.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1008_',
                'SITE_URL' => 'http://www.petexpose.com',
                'DOMAIN' => 'petexpose.com',
            ),
            'platformofcreativity.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1009_',
                'SITE_URL' => 'http://www.platformofcreativity.com',
                'DOMAIN' => 'platformofcreativity.com',
            ),
            'faxchicago.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1266_',
                'SITE_URL' => 'http://www.faxchicago.com',
                'DOMAIN' => 'faxchicago.com',
            ),
            'infozaradaklub.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1267_',
                'SITE_URL' => 'http://www.infozaradaklub.com',
                'DOMAIN' => 'infozaradaklub.com',
            ),
            'mchistoricalsociety.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1268_',
                'SITE_URL' => 'http://www.mchistoricalsociety.com',
                'DOMAIN' => 'mchistoricalsociety.com',
            ),
            'httpforums.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1269_',
                'SITE_URL' => 'http://www.httpforums.com',
                'DOMAIN' => 'httpforums.com',
            ),
            'hot-xxx-porn-directory.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1270_',
                'SITE_URL' => 'http://www.hot-xxx-porn-directory.com',
                'DOMAIN' => 'hot-xxx-porn-directory.com',
            ),
            'thepizzaplaceomaha.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_271_',
                'SITE_URL' => 'http://www.thepizzaplaceomaha.com',
                'DOMAIN' => 'thepizzaplaceomaha.com',
            ),
            '10footgames.net' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_288_',
                'SITE_URL' => 'http://www.10footgames.net',
                'DOMAIN' => '10footgames.net',
            ),
            'rcraiders.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_301_',
                'SITE_URL' => 'http://www.rcraiders.com',
                'DOMAIN' => 'rcraiders.com',
            ),
            'visiteastnorwich.com' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_302_',
                'SITE_URL' => 'http://www.visiteastnorwich.com',
                'DOMAIN' => 'visiteastnorwich.com',
            ),
            'sauktrail.org' =>
            array(
                'TABLE_SCHEMA' => 'westcoas_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_303_',
                'SITE_URL' => 'http://www.sauktrail.org',
                'DOMAIN' => 'sauktrail.org',
            ),
            'whhuiwenhua.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_1_2685_',
                'SITE_URL' => 'http://www.whhuiwenhua.com',
                'DOMAIN' => 'whhuiwenhua.com',
            ),
            'wjzajd.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_1_2686_',
                'SITE_URL' => 'http://www.wjzajd.com',
                'DOMAIN' => 'wjzajd.com',
            ),
            'wst263.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_1_2687_',
                'SITE_URL' => 'http://www.wst263.com',
                'DOMAIN' => 'wst263.com',
            ),
            'xgjdjj.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_1_2688_',
                'SITE_URL' => 'http://www.xgjdjj.com',
                'DOMAIN' => 'xgjdjj.com',
            ),
            'xzjozs.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_1_2689_',
                'SITE_URL' => 'http://www.xzjozs.com',
                'DOMAIN' => 'xzjozs.com',
            ),
            'smeoracle.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1006_',
                'SITE_URL' => 'http://www.smeoracle.com',
                'DOMAIN' => 'smeoracle.com',
            ),
            'spastikwax.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1007_',
                'SITE_URL' => 'http://www.spastikwax.com',
                'DOMAIN' => 'spastikwax.com',
            ),
            'laughingyogini.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1261_',
                'SITE_URL' => 'http://www.laughingyogini.com',
                'DOMAIN' => 'laughingyogini.com',
            ),
            'longbeachrehab.net' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1262_',
                'SITE_URL' => 'http://www.longbeachrehab.net',
                'DOMAIN' => 'longbeachrehab.net',
            ),
            'magiquestmb.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1263_',
                'SITE_URL' => 'http://www.magiquestmb.com',
                'DOMAIN' => 'magiquestmb.com',
            ),
            'meilleurs-vins.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1264_',
                'SITE_URL' => 'http://www.meilleurs-vins.com',
                'DOMAIN' => 'meilleurs-vins.com',
            ),
            'easyfreecodes.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1265_',
                'SITE_URL' => 'http://www.easyfreecodes.com',
                'DOMAIN' => 'easyfreecodes.com',
            ),
            'lepetitspirou.net' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_292_',
                'SITE_URL' => 'http://www.lepetitspirou.net',
                'DOMAIN' => 'lepetitspirou.net',
            ),
            'angoltanar.info' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_298_',
                'SITE_URL' => 'http://www.angoltanar.info',
                'DOMAIN' => 'angoltanar.info',
            ),
            'mediawestpost.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_300_',
                'SITE_URL' => 'http://www.mediawestpost.com',
                'DOMAIN' => 'mediawestpost.com',
            ),
            'donnertrailfruit.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_304_',
                'SITE_URL' => 'http://www.donnertrailfruit.com',
                'DOMAIN' => 'donnertrailfruit.com',
            ),
            'jeffhunterbailey.com' =>
            array(
                'TABLE_SCHEMA' => 'whhuiwen_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_306_',
                'SITE_URL' => 'http://www.jeffhunterbailey.com',
                'DOMAIN' => 'jeffhunterbailey.com',
            ),
            'auffiliates.com' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1182_',
                'SITE_URL' => 'http://www.auffiliates.com',
                'DOMAIN' => 'auffiliates.com',
            ),
            'winkingbear.com' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_875_',
                'SITE_URL' => 'http://www.winkingbear.com',
                'DOMAIN' => 'winkingbear.com',
            ),
            'ilatlon.com' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_876_',
                'SITE_URL' => 'http://www.ilatlon.com',
                'DOMAIN' => 'ilatlon.com',
            ),
            'levinrozalis.com' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_877_',
                'SITE_URL' => 'http://www.levinrozalis.com',
                'DOMAIN' => 'levinrozalis.com',
            ),
            'decaturheights.com' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_878_',
                'SITE_URL' => 'http://www.decaturheights.com',
                'DOMAIN' => 'decaturheights.com',
            ),
            'mizzoulax.org' =>
            array(
                'TABLE_SCHEMA' => 'winkingb_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_879_',
                'SITE_URL' => 'http://www.mizzoulax.org',
                'DOMAIN' => 'mizzoulax.org',
            ),
            'winnipegmen.net' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_1_82653_',
                'SITE_URL' => 'http://www.winnipegmen.net',
                'DOMAIN' => 'winnipegmen.net',
            ),
            'watch-case-central.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_1_82654_',
                'SITE_URL' => 'http://www.watch-case-central.com',
                'DOMAIN' => 'watch-case-central.com',
            ),
            '24inchrimsandtires.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_1_82655_',
                'SITE_URL' => 'http://www.24inchrimsandtires.com',
                'DOMAIN' => '24inchrimsandtires.com',
            ),
            '2kooltek.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_1_82656_',
                'SITE_URL' => 'http://www.2kooltek.com',
                'DOMAIN' => '2kooltek.com',
            ),
            'acessibilidadenapratica.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_1_82657_',
                'SITE_URL' => 'http://www.acessibilidadenapratica.com',
                'DOMAIN' => 'acessibilidadenapratica.com',
            ),
            'pmengraving.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1004_',
                'SITE_URL' => 'http://www.pmengraving.com',
                'DOMAIN' => 'pmengraving.com',
            ),
            'puertoricoisbest.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1005_',
                'SITE_URL' => 'http://www.puertoricoisbest.com',
                'DOMAIN' => 'puertoricoisbest.com',
            ),
            'flagstaffbikers.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1256_',
                'SITE_URL' => 'http://www.flagstaffbikers.com',
                'DOMAIN' => 'flagstaffbikers.com',
            ),
            'freedomtour08.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1257_',
                'SITE_URL' => 'http://www.freedomtour08.com',
                'DOMAIN' => 'freedomtour08.com',
            ),
            'ixten.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1258_',
                'SITE_URL' => 'http://www.ixten.com',
                'DOMAIN' => 'ixten.com',
            ),
            'jocuri3.net' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1259_',
                'SITE_URL' => 'http://www.jocuri3.net',
                'DOMAIN' => 'jocuri3.net',
            ),
            'lasvegasrealtyexperts.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1260_',
                'SITE_URL' => 'http://www.lasvegasrealtyexperts.com',
                'DOMAIN' => 'lasvegasrealtyexperts.com',
            ),
            'emarketingpark.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_270_',
                'SITE_URL' => 'http://www.emarketingpark.com',
                'DOMAIN' => 'emarketingpark.com',
            ),
            'volksbuehne-bochum.info' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_290_',
                'SITE_URL' => 'http://www.volksbuehne-bochum.info',
                'DOMAIN' => 'volksbuehne-bochum.info',
            ),
            'fxxgqt.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_296_',
                'SITE_URL' => 'http://www.fxxgqt.com',
                'DOMAIN' => 'fxxgqt.com',
            ),
            'nuestracasaarequipa.com' =>
            array(
                'TABLE_SCHEMA' => 'winnipeg_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_356_',
                'SITE_URL' => 'http://www.nuestracasaarequipa.com',
                'DOMAIN' => 'nuestracasaarequipa.com',
            ),
            'witlit.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_1_82523_',
                'SITE_URL' => 'http://www.witlit.com',
                'DOMAIN' => 'witlit.com',
            ),
            'ppict.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_1_82524_',
                'SITE_URL' => 'http://www.ppict.com',
                'DOMAIN' => 'ppict.com',
            ),
            'vacasquad.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_1_82525_',
                'SITE_URL' => 'http://www.vacasquad.com',
                'DOMAIN' => 'vacasquad.com',
            ),
            'location-site-web.net' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_1_82526_',
                'SITE_URL' => 'http://www.location-site-web.net',
                'DOMAIN' => 'location-site-web.net',
            ),
            'humblenarrator.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_1_82527_',
                'SITE_URL' => 'http://www.humblenarrator.com',
                'DOMAIN' => 'humblenarrator.com',
            ),
            'approachnutrition.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1244_',
                'SITE_URL' => 'http://www.approachnutrition.com',
                'DOMAIN' => 'approachnutrition.com',
            ),
            'askmyrealestateagent.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1245_',
                'SITE_URL' => 'http://www.askmyrealestateagent.com',
                'DOMAIN' => 'askmyrealestateagent.com',
            ),
            'casanoblemortgages.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1246_',
                'SITE_URL' => 'http://www.casanoblemortgages.com',
                'DOMAIN' => 'casanoblemortgages.com',
            ),
            'costumesforhaloween.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1247_',
                'SITE_URL' => 'http://www.costumesforhaloween.com',
                'DOMAIN' => 'costumesforhaloween.com',
            ),
            'votewaynenorman.net' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_257_',
                'SITE_URL' => 'http://www.votewaynenorman.net',
                'DOMAIN' => 'votewaynenorman.net',
            ),
            'jewishstlouis.info' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_264_',
                'SITE_URL' => 'http://www.jewishstlouis.info',
                'DOMAIN' => 'jewishstlouis.info',
            ),
            'noblebrothersviolin.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_281_',
                'SITE_URL' => 'http://www.noblebrothersviolin.com',
                'DOMAIN' => 'noblebrothersviolin.com',
            ),
            'getapersonaldoctor.org' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_282_',
                'SITE_URL' => 'http://www.getapersonaldoctor.org',
                'DOMAIN' => 'getapersonaldoctor.org',
            ),
            'compeces.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_291_',
                'SITE_URL' => 'http://www.compeces.com',
                'DOMAIN' => 'compeces.com',
            ),
            'mariacandler.com' =>
            array(
                'TABLE_SCHEMA' => 'witlit_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_758_',
                'SITE_URL' => 'http://www.mariacandler.com',
                'DOMAIN' => 'mariacandler.com',
            ),
            'work2world.org' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_1_83573_',
                'SITE_URL' => 'http://www.work2world.org',
                'DOMAIN' => 'work2world.org',
            ),
            'zimcodd.org' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_1_83574_',
                'SITE_URL' => 'http://www.zimcodd.org',
                'DOMAIN' => 'zimcodd.org',
            ),
            'zuckerarchitecture.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_1_83575_',
                'SITE_URL' => 'http://www.zuckerarchitecture.com',
                'DOMAIN' => 'zuckerarchitecture.com',
            ),
            'zzvs.info' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_1_83576_',
                'SITE_URL' => 'http://www.zzvs.info',
                'DOMAIN' => 'zzvs.info',
            ),
            'gsexclusive.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_1_83577_',
                'SITE_URL' => 'http://www.gsexclusive.com',
                'DOMAIN' => 'gsexclusive.com',
            ),
            'modernacupuncturestudio.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1002_',
                'SITE_URL' => 'http://www.modernacupuncturestudio.com',
                'DOMAIN' => 'modernacupuncturestudio.com',
            ),
            'mullinsdrums.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1003_',
                'SITE_URL' => 'http://www.mullinsdrums.com',
                'DOMAIN' => 'mullinsdrums.com',
            ),
            'dogpursecarriers.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1253_',
                'SITE_URL' => 'http://www.dogpursecarriers.com',
                'DOMAIN' => 'dogpursecarriers.com',
            ),
            'easttaylor.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1254_',
                'SITE_URL' => 'http://www.easttaylor.com',
                'DOMAIN' => 'easttaylor.com',
            ),
            'erikslaws.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1255_',
                'SITE_URL' => 'http://www.erikslaws.com',
                'DOMAIN' => 'erikslaws.com',
            ),
            'sudokupuzzles.biz' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_276_',
                'SITE_URL' => 'http://www.sudokupuzzles.biz',
                'DOMAIN' => 'sudokupuzzles.biz',
            ),
            'signalshow.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_285_',
                'SITE_URL' => 'http://www.signalshow.com',
                'DOMAIN' => 'signalshow.com',
            ),
            'iabg2009.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_287_',
                'SITE_URL' => 'http://www.iabg2009.com',
                'DOMAIN' => 'iabg2009.com',
            ),
            'mukorom.net' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_295_',
                'SITE_URL' => 'http://www.mukorom.net',
                'DOMAIN' => 'mukorom.net',
            ),
            'xn----6hckibbsbfgb3iya.com' =>
            array(
                'TABLE_SCHEMA' => 'work2wor_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_363_',
                'SITE_URL' => 'http://www.xn----6hckibbsbfgb3iya.com',
                'DOMAIN' => 'xn----6hckibbsbfgb3iya.com',
            ),
            'quillsvideo.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_1_83043_',
                'SITE_URL' => 'http://www.quillsvideo.com',
                'DOMAIN' => 'quillsvideo.com',
            ),
            'xenodus.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_1_83042_',
                'SITE_URL' => 'http://www.xenodus.com',
                'DOMAIN' => 'xenodus.com',
            ),
            'bodachile.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_1_83044_',
                'SITE_URL' => 'http://www.bodachile.com',
                'DOMAIN' => 'bodachile.com',
            ),
            'usguidez.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_1_83045_',
                'SITE_URL' => 'http://www.usguidez.com',
                'DOMAIN' => 'usguidez.com',
            ),
            'bebekolay.net' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_195_',
                'SITE_URL' => 'http://bebekolay.net',
                'DOMAIN' => 'bebekolay.net',
            ),
            'bullshitproduction.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_196_',
                'SITE_URL' => 'http://bullshitproduction.com',
                'DOMAIN' => 'bullshitproduction.com',
            ),
            'burgerbarnyc.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp2_197_',
                'SITE_URL' => 'http://burgerbarnyc.com',
                'DOMAIN' => 'burgerbarnyc.com',
            ),
            'earthtelevision.org' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_1242_',
                'SITE_URL' => 'http://www.earthtelevision.org',
                'DOMAIN' => 'earthtelevision.org',
            ),
            'femmes-entreprises-bretagne.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_263_',
                'SITE_URL' => 'http://www.femmes-entreprises-bretagne.com',
                'DOMAIN' => 'femmes-entreprises-bretagne.com',
            ),
            'sellandmarketyourbook.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_272_',
                'SITE_URL' => 'http://www.sellandmarketyourbook.com',
                'DOMAIN' => 'sellandmarketyourbook.com',
            ),
            'aplanet4creation.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_273_',
                'SITE_URL' => 'http://www.aplanet4creation.com',
                'DOMAIN' => 'aplanet4creation.com',
            ),
            'kalispellphoto.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_280_',
                'SITE_URL' => 'http://www.kalispellphoto.com',
                'DOMAIN' => 'kalispellphoto.com',
            ),
            'counselinginmetairie.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_286_',
                'SITE_URL' => 'http://www.counselinginmetairie.com',
                'DOMAIN' => 'counselinginmetairie.com',
            ),
            'tidbitsofmarketing.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_762_',
                'SITE_URL' => 'http://www.tidbitsofmarketing.com',
                'DOMAIN' => 'tidbitsofmarketing.com',
            ),
            'tokelau-govt.info' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_tkp',
                'TABLE_PREFIX' => 'wp_pkp4_763_',
                'SITE_URL' => 'http://www.tokelau-govt.info',
                'DOMAIN' => 'tokelau-govt.info',
            ),
            'geraldtparksmemorialfoundation.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_wp137',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://geraldtparksmemorialfoundation.com',
                'DOMAIN' => 'geraldtparksmemorialfoundation.com',
            ),
            'i-computacion.info' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_wp26',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://i-computacion.info',
                'DOMAIN' => 'i-computacion.info',
            ),
            'info-internet.info' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_wp467',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://info-internet.info',
                'DOMAIN' => 'info-internet.info',
            ),
            'salud-medicina.info' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_wp777',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://salud-medicina.info',
                'DOMAIN' => 'salud-medicina.info',
            ),
            'homsoftware.com' =>
            array(
                'TABLE_SCHEMA' => 'xenodus_wp909',
                'TABLE_PREFIX' => 'wp_',
                'SITE_URL' => 'http://homsoftware.com',
                'DOMAIN' => 'homsoftware.com',
            ),
        );

        if (is_array($reDomains) && $reDomains) {
            $reArr = array();
            foreach ($reDomains as $domain) {
                if (isset($dbAllKey[$domain])) {
                    $reArr[$domain] = $dbAllKey[$domain];
                }
            }
            return $reArr;
        } else {
            return $dbAllKey;
        }

        echo count($dbAllKey);
    }

}

// end class
?>