<?php

App::uses('AppModel', 'Model');

class Addcredit extends AppModel {

    public $name = 'Addcredit';
    public $useTable = 'users';
    var $validate = array(
        'additionalcredit' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Credit can not be left empty',
                'allowEmpty' => false,
                'required' => true
            ),
            'onlyNumber' => array(
                'rule' => 'numeric',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Credit should be in number'
            )
        )
    );

}

?>