<?php

App::uses('AppModel', 'Model');

class Linkemperor extends AppModel {

    public $name = 'Linkemperor';
    public $useTable = 'linkemperor_orders';
    var $validate = array(
        'ordername' => array(
            'rule' => 'notEmpty',
            'message' => 'Name of Blast is required',
            'allowEmpty' => false,
            'required' => true,
        ),
        'anchor1' => array(
            'rule' => 'notEmpty',
            'message' => 'First anchor is required',
            'allowEmpty' => false,
            'required' => true,
        ),
        'url1' => array(
            'rule' => 'notEmpty',
            'message' => 'First url is required',
            'allowEmpty' => false,
            'required' => true,
        )
    );

}

// end class
?>