<?php

App::uses('AppModel', 'Model');

class Submission extends AppModel {

    public $name = 'Submission';
    public $useTable = 'submissions';
    var $validate = array(
        'submissionname' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Submission Name can not be left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'startdate' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Start Date can not be left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'enddate' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'End Date can not be left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'howmanytopost' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Howmanytopost can not be left empty',
                'allowEmpty' => false,
                'required' => true
            ),
            'onlyNumber' => array(
                'rule' => 'numeric',
                'required' => true,
                'allowEmpty' => false,
                'message' => 'Howmanytopost should be in number'
            ),
            'networkLimit' => array(
                'rule' => array('checkNetworkLimit'),
                'message' => 'Howmanytopost should be in available limit'
            ),
        ),
        'posttitle' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Post Title can not be left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'postcontent' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Article to post can not be left empty',
                'allowEmpty' => false,
                'required' => true
            )
        ),
        'pr_service_id' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Network can not be left empty',
                'allowEmpty' => true,
                'required' => false
            )
        )
    );
    public $virtualFields = array(
        'clientname' => '(SELECT CONCAT(users.firstname, " ", users.lastname) FROM users WHERE Submission.user_id = users.id)',
        'statuslabel' => "SELECT CASE WHEN Submission.status = 1 THEN 'Active' WHEN Submission.status = 2 THEN 'Completed' ELSE 'Inactive' END",
        'totalsubmitted' => "SELECT COUNT(submissionreports.id) FROM submissionreports WHERE submissionreports.submission_id = Submission.id"
    );
    public $order = array('Submission.id' => 'DESC');

    public function checkNetworkLimit($check) {
        //$networkType = $this->data[$this->alias]['pr_service_id'];
        $howManyToPost = $check['howmanytopost'];
        $domainsAvailable = ClassRegistry::init('Domain')->find('count', array(
            'conditions' => "Domain.is_indexed=1 AND Domain.owner_id = '-1' AND Domain.user_id = '-1'"
        ));
        if ($howManyToPost <= $domainsAvailable) {
            return true;
        }
        return false;
    }

}

?>