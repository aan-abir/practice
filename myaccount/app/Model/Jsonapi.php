<?php
    /**
    * Description of JsonAPi
    *
    * @author Abir Aust <abirymail@gmail.com>
    */
    require_once dirname(__FILE__) . '/Response.php';
    class Jsonapi extends AppModel{
        private static $app_url = 'http://jsonwhois.com/api/v1/whois';
        private static $app_secret_key = '7afa8cd66001bb346379f52765401cf8';
        private static $jsonOpts = array();
        private static $verifyPeer = false;
        private static $socketTimeout = null;
        private static $defaultHeaders =  array( "Accept" => "application/json", "Authorization" => 'Token token=7afa8cd66001bb346379f52765401cf8');

        public static function getWhoisData($body = null) {
            $ch = curl_init();
            $url = self::$app_url;
            if (is_array($body)) {
                if (strpos($url, '?') !== false) {
                    $url .= '&';
                } else {
                    $url .= '?';
                }
                $url .= urldecode(http_build_query(self::buildHTTPCurlQuery($body)));
            }
           

            curl_setopt($ch, CURLOPT_URL, self::encodeUrl($url));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            curl_setopt($ch, CURLOPT_HTTPHEADER, self::getFormattedHeaders(self::$defaultHeaders));
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, self::$verifyPeer);
            curl_setopt($ch, CURLOPT_ENCODING, ''); // If an empty string, '', is set, a header containing all supported encoding types is sent.
            curl_setopt($ch, CURLOPT_TIMEOUT, 20); // The maximum number of seconds to allow cURL functions to execute.

            $response = curl_exec($ch);
            $error    = curl_error($ch);

            if ($error) {
                return false;
                // throw new \Exception($error);
            }
          

            // Split the full response in its headers and body
            $curl_info   = curl_getinfo($ch);
            $header_size = $curl_info['header_size'];
            $header      = substr($response, 0, $header_size);
            $body        = substr($response, $header_size);
            $httpCode    = $curl_info['http_code'];
            return new Response($httpCode, $body, $header, self::$jsonOpts);
            //return $body;
        }

        public static function getFormattedHeaders($headers)
        {
            $formattedHeaders = array();

            $combinedHeaders = array_change_key_case(array_merge((array) $headers, self::$defaultHeaders));

            foreach ($combinedHeaders as $key => $val) {
                $formattedHeaders[] = self::getHeaderString($key, $val);
            }

            if (!array_key_exists('user-agent', $combinedHeaders)) {
                $formattedHeaders[] = 'user-agent: unirest-php/2.0';
            }

            if (!array_key_exists('expect', $combinedHeaders)) {
                $formattedHeaders[] = 'expect:';
            }

            return $formattedHeaders;
        }

        public static function buildHTTPCurlQuery($data, $parent = false)
        {
            $result = array();

            if (is_object($data)) {
                $data = get_object_vars($data);
            }

            foreach ($data as $key => $value) {
                if ($parent) {
                    $new_key = sprintf('%s[%s]', $parent, $key);
                } else {
                    $new_key = $key;
                }

                if (!$value instanceof \CURLFile and (is_array($value) or is_object($value))) {
                    $result = array_merge($result, self::buildHTTPCurlQuery($value, $new_key));
                } else {
                    $result[$new_key] = $value;
                }
            }

            return $result;
        }
        private static function encodeUrl($url)
        {
            $url_parsed = parse_url($url);

            $scheme = $url_parsed['scheme'] . '://';
            $host   = $url_parsed['host'];
            $port   = (isset($url_parsed['port']) ? $url_parsed['port'] : null);
            $path   = (isset($url_parsed['path']) ? $url_parsed['path'] : null);
            $query  = (isset($url_parsed['query']) ? $url_parsed['query'] : null);

            if ($query !== null) {
                $query = '?' . http_build_query(self::getArrayFromQuerystring($query));
            }

            if ($port && $port[0] !== ':') {
                $port = ':' . $port;
            }

            $result = $scheme . $host . $port . $path . $query;
            return $result;
        }


        private static function getArrayFromQuerystring($query)
        {
            $query = preg_replace_callback('/(?:^|(?<=&))[^=[]+/', function ($match) {
                return bin2hex(urldecode($match[0]));
                }, $query);

            parse_str($query, $values);

            return array_combine(array_map('hex2bin', array_keys($values)), $values);
        }
        private static function getHeaderString($key, $val)
        {
            $key = trim(strtolower($key));
            return $key . ': ' . $val;
        }
        public static function jsonOpts($assoc = false, $depth = 512, $options = 0)
        {
            return self::$jsonOpts = array($assoc, $depth, $options);
        }

    }



?>