<?php
App::uses('AppModel', 'Model');
class Uploadfile extends AppModel {
    public $name = 'Uploadfile';
    public $useTable = 'uploaded_files';
    public $validate = array(
        'filename' => array(
            'rule' => 'notEmpty',
            'message' => 'File Name is required.',
            'allowEmpty' => false,
            'required' => true,
        )
    );
}
