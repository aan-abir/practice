<?php

App::uses('AppModel', 'Model');

class Faqcategory extends AppModel {

    public $name = 'Faqcategory';
    public $useTable = 'faqcategory';
    var $validate = array(
        'category' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Category Name can not be left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
    );
    public $virtualFields = array(
        'countFaqs' => 'SELECT COUNT(id) FROM faq WHERE faq.category_id = Faqcategory.id AND faq.status = 1'
    );

}

?>