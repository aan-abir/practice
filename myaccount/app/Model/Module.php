<?php

App::uses('AppModel', 'Model');

class Module extends AppModel
{

    public $name = 'Module';
    public $useTable = 'modules';
    public $validate = array(
        'name' => array(
            'rule' => 'notEmpty',
            'message' => 'Name is required.',
            'allowEmpty' => false,
            'required' => true,
        ),
        'title' => array(
            'rule' => 'notEmpty',
            'message' => 'Title is required.',
            'allowEmpty' => false,
            'required' => true,
        ),
        'content' => array(
            'rule' => 'notEmpty',
            'message' => 'Content is required.',
            'allowEmpty' => false,
            'required' => true,
        )
    );

    public $hasMany = array(
        'Lesson' => array('foreignKey' => 'module_id')
    );
}
