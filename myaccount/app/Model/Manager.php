<?php

App::uses('AppModel', 'Model');

class Manager extends AppModel {

    public $name = 'Manager';
    public $useTable = false;

    function getAutoDomains($type, $cont) {
        $params = array(
            'conditions' => array("Domain.owner_id = -1 AND Domain.networktype = $type"),
            'order' => ' Domain.domain ASC ',
            'recursive' => -1
        );
        $typeArr[3] = 'Premium';
        $typeArr[4] = 'Prime';
        $typeArr[5] = 'LowGrade';
        $cont->loadModel('Domain');
        $itemList = $cont->Domain->find('all', $params);
        $cont->set(compact('itemList'));
        $cont->set('title_for_layout', $typeArr[$type] . ' Domains');
        $cont->render('alldomains');
    }

    function iwactive($itemId = null, $modelName = null, $cont = null) {
        $cont->autoRender = false;
        $cont->layout = 'ajax';
        if ($itemId && $modelName && $cont) {
            $modelName = ucfirst($modelName);
            $this->useTable = ClassRegistry::init($modelName)->useTable;
            $this->id = $itemId;
            $currentStatus = $this->field('iw_active');
            if (in_array($currentStatus, array(0, 1))) {
                if ($currentStatus == 1) {
                    $newStatus = 0;
                } elseif ($currentStatus == 0) {
                    $newStatus = 1;
                }
                if ($this->saveField('iw_active', $newStatus)) {
                    $fMsg = ($newStatus ? 'Activated' : 'Deactivated') . ' successfully!';
                    $cont->Session->setFlash($fMsg, 'flash_success');
                } else {
                    $fMsg = 'Could not be ' . ($newStatus ? 'activated' : 'deactivated') . '!';
                    $cont->Session->setFlash($fMsg, 'flash_error');
                }
            }
            $this->useTable = false;
        }
    }

    function refreshindex($itemId, $cont) {
        $cont->autoRender = false;
        $cont->layout = 'ajax';
        $cont->loadModel('Domain');
        $cont->Domain->id = $itemId;
        $url = $cont->Domain->field('domain');

        $cont->loadModel('GoogePageRank');
        $admin_ip = $cont->admin_ip();
        $proxyIp = $admin_ip['ip'];
        $proxy_user_pass = $admin_ip['password'];
        $proxy_port = $admin_ip['port'];
        $indexed = $cont->GoogePageRank->checkIndex($url, $proxyIp, $proxy_user_pass, $proxy_port);
        $isIndexed = $indexed == 'Yes' ? 1 : 0;
        $updateData['is_indexed'] = $isIndexed;
        $updateData['iw_active'] = $isIndexed;
        $updateData['last_check'] = date('Y-m-d H:i:s');
        $cont->Domain->save($updateData, false);
        $fMsg = 'Successfully Refreshed! Domain is currently ';
        $fMsg .='<span class="redMarked">' . ($isIndexed ? 'Indexed' : 'Not Indexed') . '!</span>';
        $cont->Session->setFlash($fMsg, 'flash_success');
    }

}
