<?php

App::uses('AppModel', 'Model');

class Credithistory extends AppModel {

    public $name = 'Credithistory';
    public $useTable = 'credithistories';
    public $virtualFields = array(
        'servicename' => 'SELECT servicename FROM services WHERE services.id = Credithistory.service_id',
        'clientname' => '(SELECT CONCAT(users.firstname, " ", users.lastname) FROM users WHERE Credithistory.user_id = users.id)'
    );

}

?>