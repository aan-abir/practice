<?php

App::uses('AppModel', 'Model');

class Education extends AppModel {

    public $name = 'Education';
    public $useTable = 'educations';
    public $validate = array(
        'cat_id' => array(
            'rule' => 'notEmpty',
            'message' => 'Select Category'
        ),
        'title' => array(
            'rule' => 'notEmpty',
            'message' => 'Enter Title',
            'allowEmpty' => false,
            'required' => true,
        )
    );

}

// end class
?>