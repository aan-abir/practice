<?php

App::uses('AppModel', 'Model');

class Blogrolltext extends AppModel {

    public $name = 'Blogrolltext';
    public $useTable = 'blogrolltexts';
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Blogroll' => array(
            'className' => 'Blogroll',
            'foreignKey' => 'blogroll_id'
        ),
    );

}

// end class
?>