<?php
    /*
    * User has access to recording
    */
    App::uses('AppModel', 'Model');

    class Ruser extends AppModel {
        public $name = 'Ruser';
        public $useTable = 'upackages';
        public $belongsTo = array(
            'User' => array(
                'className' => 'User',
                'foreignKey' => 'user_id'
            ),
            'Package' => array(
                'className' => 'Package',
                'foreignKey' => 'package_id'
            ),
        );

    }

    // end class
?>