<?php

App::uses('AppModel', 'Model');

class Serviceprice extends AppModel {

    public $name = 'Serviceprice';
    public $useTable = 'serviceprices';
    public $validate = array(
        'unitprice' => array(
            'unique' => array(
                'rule' => array('checkUnique', array('service_id', 'package_id')),
                'message' => 'Service ID and Package ID together must be unique.',
            )
        )
    );

}

?>