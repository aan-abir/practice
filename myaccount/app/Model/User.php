<?php

App::uses('AppModel', 'Model');

class User extends AppModel {

    public $name = 'User';
    public $useTable = 'users';
    public $belongsTo = array(
        'Package' => array(
            'className' => 'Package',
            'foreignKey' => false,
            'conditions' => '(User.package_id = Package.id)'
        ),
    );
    public $virtualFields = array(
        'fullname' => 'CONCAT(User.firstname, " ", User.lastname)',
        'fullnamewptype' => "CONCAT(User.firstname, ' ', User.lastname, (SELECT (CASE WHEN type = 1 THEN ' (Free)' WHEN type = 0 THEN ' (Paid)' END) as type FROM packages  WHERE packages.id = User.package_id))",
        'packagename' => 'SELECT packagename FROM packages WHERE packages.id = User.package_id'
    );

}

?>