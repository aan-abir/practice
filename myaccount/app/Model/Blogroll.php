<?php

App::uses('AppModel', 'Model');

class Blogroll extends AppModel {

    public $name = 'Blogroll';
    public $useTable = 'blogrolls';
    public $validate = array(
        'domain_id' => array(
            'rule' => 'notEmpty',
            'message' => 'Please choose a domain',
            'allowEmpty' => false,
            'required' => true,
        )
    );
    public $virtualFields = array(
        'domain' => 'SELECT domain FROM domains WHERE domains.id = Blogroll.domain_id'
    );

}

// end class
?>