<?php

App::uses('AppModel', 'Model');

class Anchortext extends AppModel {

    public $name = 'Anchortext';
    public $useTable = 'anchortexts';
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id'
        ),
    );

}

// end class
?>