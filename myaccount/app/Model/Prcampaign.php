<?php

App::uses('AppModel', 'Model');

class Prcampaign extends AppModel {

    public $name = 'Prcampaign';
    public $useTable = 'pr_campaigns';
    
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Campaign Name can not empty',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'c_urls' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter your domain',
                'allowEmpty' => false,
                'required' => true,
            )
        )
    );

    
    /* function atleastOneKeyword(){
      $k = implode("", array_filter($this->data['Rankcampaign']['keywords']));
      return ( strlen($k) == 0 ) ? 0 : 1;
      }
      function atleastOneUrl() {
      $k = implode("", array_filter($this->data['Rankcampaign']['urls']));
      return ( strlen($k) == 0 ) ? 0 : 1;
      } */
}
