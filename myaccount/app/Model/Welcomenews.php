<?php
App::uses('AppModel', 'Model');
class Welcomenews extends AppModel {
    public $name = 'Welcomenews';
    public $useTable = 'welcomenews';
    public $validate = array(
            'title' => array(
                'rule' => 'notEmpty',
                'message' => 'Enter Event Title',
                'allowEmpty' => false,
                'required' => true,
            ),
        );
}
?>