<?php

App::uses('AppModel', 'Model');

class Rankreport extends AppModel {

    public $name = 'Rankreport';
    public $useTable = 'rankreports';

    function atleastOneEngine() {
        return ( ($this->data['Rankcampaign']['google'] == 'false') && ( $this->data['Rankcampaign']['yahoo'] == 'false' ) && ( $this->data['Rankcampaign']['bing'] == 'false' ) ) ? 0 : 1;
    }

    function process_amazon_youtube($reportIds = null) {

        $cond = array();
        if ($reportIds) {
            $cond = array(
                'conditions' => "Rankreport.id IN ($reportIds)"
            );
        }
        $allAmYou = $this->find('all', $cond);

        //pr($allAmYou);

        if ($allAmYou) {

            foreach ($allAmYou as $aAY) {

                $arr = $aAY['Rankreport'];
                $urls = (array) unserialize($arr['urls']);
                $keyword = $arr['keyword'];

                // parse position
                $res = '';

                if ($urls) {

                    $sEngine = $arr['engine'];
                    $exactMatch = $arr['exact_match'];

                    $result = array();
                    if ($sEngine == 'amazon') {
                        $result = $this->processAmazon($keyword, $urls, $exactMatch);
                    } elseif ($sEngine == 'youtube') {
                        $result = $this->processYoutube($keyword, $urls);
                    }

                    if ($result) {
                        $res = serialize($result);
                    } else {
                        foreach ($urls as $url) {
                            $url = 'http://' . str_replace(array('http://', 'https://'), '', $url);
                            // take only the highest position
                            $hpos = 0;
                            $page = 0;
                            $title = 'NA';
                            $desc = 'NA';
                            $result[$url] = array($hpos, $page, $title, $desc, $url);
                        }
                        $res = serialize($result);
                    }

                    // update rankreports table
                    $updateData['position'] = $res;
                    $updateData['update_time'] = date('Y-m-d');
                    $updateData['status'] = 'completed';

                    $this->id = $arr['id'];
                    $this->save($updateData, false);
                }
            }
        }
    }

    function parseToArray($xpath, $class) {
        //$xpathquery = "//span[@class='" . $class . "']";
        $xpathquery = "//*[@class='" . $class . "']";
        $elements = $xpath->query($xpathquery);

        if (!is_null($elements)) {
            $resultarray = array();
            foreach ($elements as $element) {
                $nodes = $element->childNodes;
                foreach ($nodes as $node) {
                    $resultarray[] = $node->nodeValue;
                }
            }
            return $resultarray;
        }
    }

    function processAmazon($keyword, $urls, $exactMatch = 0) {
        $fullArr = array();
        $finalArr = array();

        //pr($keyword);
        //pr($urls);

        $keywordEncoded = urlencode($keyword);

        $key = 1;
        for ($i = 1; $i <= 7; $i++) {
            $maxResults = 16;
            $startIndex = $i;
            $url = "http://www.amazon.com/s/ref=nb_sb_ss_i_0_10?url=search-alias%3Daps&field-keywords=$keywordEncoded&page=$startIndex";

            $div = $this->recursive_amazon($url);

            //pr($div);
            if ($div->length > 0) {
                foreach ($div as $val) {
                    $a = $val->getElementsByTagName('a');
                    //echo '<pre>';
                    $temp['rank_title'] = $a->item(0)->textContent;
                    $temp['rank_url'] = $a->item(0)->getAttribute('href');


                    if ($urls) {
                        foreach ($urls as $url) {
                            if (!isset($finalArr[$url]) || empty($finalArr[$url])) {
                                //if ((stripos(strtolower($temp['rank_title']), strtolower($keyword)) !== false) && (stripos(strtolower($temp['rank_url']), strtolower($ur)) !== false)) {
                                //if ((trim(strtolower($temp['rank_url']) == trim(strtolower($ur))))) {
                                $ur = trim(str_replace(array('http://', 'https://'), "", $url));
                                $tempRankURL = str_replace(array('http://', 'https://'), "", $url);

                                $urArr = explode('/ref=', $ur);
                                $ur = $urArr[0];

                                $fTemp[0] = $key;
                                $fTemp[1] = $i;
                                $fTemp[2] = $temp['rank_title'];
                                $fTemp[3] = '';
                                $fTemp[4] = $temp['rank_url'];

                                if ($exactMatch) {
                                    $tempRankUrlArr = explode('/ref=', $tempRankURL);
                                    $tempRankURL = $tempRankUrlArr[0];

                                    if ($tempRankURL == $ur) {
                                        $finalArr[$url] = $fTemp;
                                    }
                                } else {
                                    if (stripos($tempRankURL, $ur) !== false) {
                                        $finalArr[$url] = $fTemp;
                                    }
                                }
                            }
                        }
                    }


                    $fullArr[$key] = $temp;
                    $key++;
                    if ($key > 100) {
                        break;
                    }
                }
            } else {
                $key+= $maxResults;
            }
            //echo $i . ' ';
        }
        //pr($finalArr);
        //pr($fullArr);
        //exit;
        return $finalArr;
    }

    function recursive_amazon($url) {
        $amazonBody = $this->curl_get_contents($url);
        //echo $amazonBody = preg_replace('/^.*\n\n/s', '', $amazonBody, 1);

        $dom = new DomDocument();
        $dom->preserveWhiteSpace = false;

        @$dom->loadHTML($amazonBody);

        $xpath = new DomXpath($dom);
        $div = $xpath->query('//h3[@class="newaps"]');

        if ($div->length == 0) {
            //recursive_amazon($url);
        }
        return $div;
    }

    function processYoutube($keyword, $urls) {

        $fullArr = array();
        $finalArr = array();

        $keywordEncoded = urlencode($keyword);

        //pr($keyword);
        //pr($urls);

        $key = 1;
        for ($i = 1; $i <= 5; $i++) {

            $maxResults = 20;
            $startIndex = (($i - 1) * $maxResults) + 1;

            $url = "https://gdata.youtube.com/feeds/api/videos?q=$keywordEncoded&orderby=relevance&fields=entry(title,link[@rel='alternate'])&start-index=$startIndex&max-results=$maxResults&v=2&alt=json";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_REFERER, $url);
            $body1 = curl_exec($ch);
            $body = str_replace('$', '', $body1);
            curl_close($ch);
            $data = json_decode($body);

            if ($data) {
                foreach ($data->feed->entry as $result) {

                    $temp['rank_title'] = $result->title->t;
                    $temp['rank_url'] = $result->link[0]->href;

                    if ($urls) {
                        foreach ($urls as $ur) {
                            if (!isset($finalArr[$ur]) || empty($finalArr[$ur])) {
                                $vId = $this->parse_yturl($ur);
                                if (strpos($temp['rank_url'], $vId) !== false) {
                                    $fTemp[0] = $key;
                                    $fTemp[1] = $i;
                                    $fTemp[2] = $temp['rank_title'];
                                    $fTemp[3] = '';
                                    $fTemp[4] = $temp['rank_url'];
                                    $finalArr[$ur] = $fTemp;
                                }
                            }
                        }
                    }
                    //$fullArr[$key] = $temp;
                    $key++;
                }
            } else {
                $key+= $maxResults;
            }
        }
        //pr($finalArr);
        //pr($fullArr);
        return $finalArr;
    }

    function parse_yturl($url) {
        parse_str(parse_url($url, PHP_URL_QUERY), $yt);
        return isset($yt['v']) ? $yt['v'] : $url;
    }

    function curl_get_contents($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }

}
