<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DomainTools
 *
 * @author Royal
 */
App::uses('AppModel', 'Model');

class DomainTools extends AppModel{

    public $name = 'DomainTools';
    public $useTable = false;

    public static function getData($domain)
    {
    	$domain = "www.".self::urlToDomain($domain);
        //echo $domain; exit;
        //freeuser api
        //$url = "http://freeapi.domaintools.com/v1/".$domain."?api_username=".sfConfig::get('app_domaintools_api_username')."&api_key=".sfConfig::get('app_domaintools_api_key');

        //paid user whois api
        $url = "http://api.domaintools.com/v1/".$domain."/whois?api_username=dori&api_key=5f093-57bfc-ea296-68a6b-8823e";
        //exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);

        // Check if any error occurred
        if(curl_errno($ch) || curl_exec($ch) === false)
        {
            $data = 'Error: ' . curl_error($ch);
        }
        curl_close($ch);

        //$data = '{"response":{"registrant":"SEONitro","registration":{"created":"2006-05-01","expires":"2017-05-01","updated":"2011-08-07","registrar":"GODADDY.COM, LLC","statuses":["clientDeleteProhibited","clientRenewProhibited","clientTransferProhibited","clientUpdateProhibited"]},"name_servers":["NS1.SEONITRO.NET","NS2.SEONITRO.NET"],"whois":{"date":"2013-03-22","record":"   Registered through: GoDaddy.com, LLC (http:\/\/www.godaddy.com)\n   Domain Name: SEONITRO.COM\n      Created on: 01-May-06\n      Expires on: 01-May-17\n      Last Updated on: 07-Aug-11\n\n   Registrant:\n   SEONitro\n   3451 Dry Creek Road\n   Ione, California 95640\n   United States\n\n   Administrative Contact:\n      Friend, Dori  acccounts@seonitro.com\n      SEONitro\n      3451 Dry Creek Road\n      Ione, California 95640\n      United States\n      12092742215\n\n   Technical Contact:\n      Friend, Dori  acccounts@seonitro.com\n      SEONitro\n      3451 Dry Creek Road\n      Ione, California 95640\n      United States\n      12092742215\n\n   Domain servers in listed order:\n      NS1.SEONITRO.NET\n      NS2.SEONITRO.NET\n"}}}';

        $domain_info = json_decode($data);
        $return = self::parseData($domain_info);
        return $return;

    }

    public static function parseData($domain_info)
    {
        $arr_domain_info = array();
        $arr_domain_info['created'] = NULL;
        $arr_domain_info['expires'] = NULL;
        $arr_domain_info['name_servers'] = "";
        $arr_domain_info['error_code'] = NULL;
        $arr_domain_info['error_message'] = NULL;
        foreach($domain_info as $v)
        {

            //print_r($v);
            //echo "<br/>";
		    if(property_exists($v,'registration') && !is_array($v->registration))
		    {
	            $arr_domain_info['created'] = property_exists($v->registration, 'created') ? $v->registration->created : "";
	            $arr_domain_info['expires'] = property_exists($v->registration, 'expires') ? $v->registration->expires : "";
	            //$arr_domain_info['updated'] = property_exists($v->registration, 'updated') ? $v->registration->updated : "";
	        }

		    if(property_exists($v,'name_servers'))
		    {
	            foreach($v->name_servers as $v1)
	            {
                    //paid user
	                $arr_domain_info['name_servers'][] = $v1;
                    //free user
	                //$arr_domain_info['name_servers'][] = $v1->server;
	            }
	        }
        }

        if($arr_domain_info['created'] == NULL || $arr_domain_info['expires'] == NULL || $arr_domain_info['name_servers'] == "")
        {
            $arr_whois = self::whoisparse($domain_info);
            if($arr_domain_info['created'] == NULL && $arr_whois['created'] != "")
                $arr_domain_info['created'] = $arr_whois['created'];
            if($arr_domain_info['expires'] == NULL && $arr_whois['expires'] != "")
                $arr_domain_info['expires'] = $arr_whois['expires'];
            if($arr_domain_info['name_servers'] == "" && $arr_whois['name_servers'] != "")
                $arr_domain_info['name_servers'] = $arr_whois['name_servers'];
        }

        $arr_error = self::errorData($domain_info);
        $arr_domain_info['error_code'] = $arr_error['error_code'];
        $arr_domain_info['error_message'] = $arr_error['error_message'];

        return $arr_domain_info;
    }

	public static function errorData($domain_info)
	{
        $arr_domain_info = array();
        $arr_domain_info['error_code'] = "";
        $arr_domain_info['error_message'] = "";
        foreach($domain_info as $v)
        {
		    if(property_exists($v,'code') && !is_array($v->code))
        		$arr_domain_info['error_code'] = $v->code;
		    if(property_exists($v,'message') && !is_array($v->message))
        		$arr_domain_info['error_message'] = $v->message;
		}
		return $arr_domain_info;
	}

    public static function whoisparse($domain_info)
    {
        $arr_domain_info['created'] = NULL;
        $arr_domain_info['expires'] = NULL;
        $arr_domain_info['name_servers'] = "";

        $created_on  = "Created On";
        $expiration_date  = "Expiration Date";
        $name_server = "Name Server";
        foreach($domain_info as $k => $v)
        {
            if(property_exists($v,'whois'))
            {
                if(property_exists($v->whois,'record'))
                {
                    $whois = $v->whois->record;
                    $arr_whois = explode("\n",$whois);
                    foreach($arr_whois as $key => $whois)
                    {
                        $keyval = explode(":",$whois,2);

                        if(trim($keyval[0]) == $created_on && trim($keyval[1]) != "")
                            $arr_domain_info['created'] = self::dateFormat(trim($keyval[1]));
                        if(trim($keyval[0]) == $expiration_date && trim($keyval[1]) != "")
                            $arr_domain_info['expires'] = self::dateFormat(trim($keyval[1]));
                        if(trim($keyval[0]) == $name_server  && trim($keyval[1]) != "")
                            $arr_domain_info['name_servers'][] = trim($keyval[1]);

                        $keyval = null;
                    }
                }
            }
        
        }
        return $arr_domain_info;
    }

    public static function urlToDomain($domain)
    {
        $domain = str_replace("http://", "", $domain);
        $domain = str_replace("https://", "", $domain);
        if(strtolower(substr($domain, 0, 4)) == "www.")
        {
            $len = strlen($domain);
            $domain = trim(substr($domain, 4,($len-4)));
        }
		$arr_domain = explode("/",$domain);
		return $arr_domain[0];
    }

    public static function dateFormat($datetime){
        $arr_datetime = explode(" ", $datetime,2);
        $arr_date = explode("-", $arr_datetime[0]);

        $arr_month = array('01'=>'Jan', '02'=>'Feb', '03'=>'Mar', '04'=>'Apr', '05'=>'May', '06'=>'Jun', '07'=> 'Jul', '08' => 'Aug' , '09'=> 'Sep', '10'=>'Oct', '11'=>'Nov', '12'=>'Dec');

        $month = array_keys($arr_month, $arr_date[1]);
        
        return $arr_date[2]."-".$month[0]."-".$arr_date[0];

    }
}

?>
