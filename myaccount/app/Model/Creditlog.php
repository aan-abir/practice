<?php

App::uses('AppModel', 'Model');

class Creditlog extends AppModel {

    public $name = 'Creditlog';
    public $useTable = 'creditlogs';
    public $virtualFields = array(
        'clientname' => '(SELECT CONCAT(users.firstname, " ", users.lastname) FROM users WHERE Creditlog.assignedto = users.id)'
    );
    public $order = array("Creditlog.id" => "DESC");

}

?>