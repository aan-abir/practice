<?php

App::uses('AppModel', 'Model');

class Instruction extends AppModel {

    public $name = 'Instruction';
    public $useTable = 'instructions';
    var $validate = array(
        'pagename' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Page Action can not be left empty',
                'allowEmpty' => false,
                'required' => true,
            ),
            'already taken' => array(
                'rule' => 'isUnique',
                'message' => 'You have added this page instruction already. Go edit instead.'
            )
        )
    );

}

?>