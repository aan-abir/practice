<?php

App::uses('AppModel', 'Model');

class Bonus extends AppModel
{

    public $name = 'Bonus';
    public $useTable = 'bonuses';
    public $validate = array(
        'name' => array(
            'rule' => 'notEmpty',
            'message' => 'Name is required.',
            'allowEmpty' => false,
            'required' => true,
        )
    );

    public function getEmbededBonuses()
    {
        $parentIds = $this->find('list', array(
            'conditions' => "parent_id IS NULL",
            'fields' => 'id,id',
            'order' => 'menu_order asc'
        ));
        $allmenus = $this->find('all',array(
            'order' => 'menu_order asc'
        ));

        $items = array();
        if ($allmenus) {
            foreach ($allmenus as $menu) {
                $arr = $menu['Bonus'];
                $mId = $arr['id'];
                if (in_array($mId, $parentIds)) {
                    $items[$mId]['top'] = $arr;
                } else {
                    if (in_array($arr['parent_id'], $parentIds)) {
                        $items[$arr['parent_id']]['sub'][] = $arr;
                    }
                }
            }
        }
        return $items;
    }

}
