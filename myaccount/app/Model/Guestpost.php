<?php

App::uses('AppModel', 'Model');

class Guestpost extends AppModel {

    public $name = 'Guestpost';
    public $useTable = 'guestposts';
    var $validate = array(
        'postmarket' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please let us know your desire market',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'title' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter your article title to post',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'postdraft' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter your article to post',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        
        /*'postdraft' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please Enter your article to post',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),*/
    );
    public $virtualFields = array(
        'clientname' => '(SELECT CONCAT(users.firstname, " ", users.lastname) FROM users WHERE Guestpost.user_id = users.id)',
        'clientemail' => '(SELECT users.email FROM users WHERE Guestpost.user_id = users.id)',
        'statuslabel' => "SELECT CASE WHEN Guestpost.status = 1 THEN 'Ordered' WHEN Guestpost.status = 2 THEN 'Processing' WHEN Guestpost.status = 3 THEN 'Completed' ELSE 'Inactive' END"
    );
    public $order = array('Guestpost.id' => 'DESC');

}

?>