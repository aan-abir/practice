<?php

App::uses('AppModel', 'Model');

class Lesson extends AppModel
{

    public $name = 'Lesson';
    public $useTable = 'lessons';
    public $validate = array(
        'title' => array(
            'rule' => 'notEmpty',
            'message' => 'Title is required.',
            'allowEmpty' => false,
            'required' => true,
        ),
        'lesson_length' => array(
            'rule' => 'notEmpty',
            'message' => 'Length is required.',
            'allowEmpty' => false,
            'required' => true,
        )
    );

    public $hasMany = array(
        'Lessonfile' => array('foreignKey' => 'lesson_id'),
        'Lessoncomment' => array('foreignKey' => 'lesson_id')
    );

    public $belongsTo = array(
        'Module' => array('foreignKey' => 'module_id')
    );
}
