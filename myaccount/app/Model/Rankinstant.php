<?php

App::uses('AppModel', 'Model');

class Rankinstant extends AppModel {

    public $name = 'Rankinstant';
    public $useTable = 'rankinstants';
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );
    public $validate = array(
        'c_keywords' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter your keyword',
                'allowEmpty' => false,
                'required' => true,
            )
        ),
        'c_urls' => array(
            'notempty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter your keyword',
                'allowEmpty' => false,
                'required' => true,
            )
        )
    );

    function atleastOneEngine() {
        return ( ($this->data['Rankcampaign']['google'] == 'false') && ( $this->data['Rankcampaign']['yahoo'] == 'false' ) && ( $this->data['Rankcampaign']['bing'] == 'false' ) ) ? 0 : 1;
    }

    function process_amazon_youtube($reportIds = null) {

        $cond = array();
        if ($reportIds) {
            $cond = array(
                'conditions' => "Rankinstant.id IN ($reportIds)"
            );
        }
        $allAmYou = $this->find('all', $cond);

        pr($allAmYou);

        if ($allAmYou) {

            foreach ($allAmYou as $aAY) {

                $arr = $aAY['Rankinstant'];
                $urls = (array) unserialize($arr['url']);
                $keyword = $arr['keyword'];

                // parse position
                $res = '';

                if ($urls) {

                    $sEngine = $arr['engine'];

                    $result = array();
                    if ($sEngine == 'amazon') {
                        $result = $this->processAmazon($keyword, $urls);
                    } elseif ($sEngine == 'youtube') {
                        $result = $this->processYoutube($keyword, $urls);
                    }
                    if ($result) {
                        $res = serialize($result);

                        // update rankreports table
                        $updateData['position'] = $res;
                        $updateData['update_time'] = date('Y-m-d');
                        $updateData['status'] = 'completed';

                        $this->id = $arr['id'];
                        $this->save($updateData, false);
                    }
                }
            }
        }
        //exit;
        return true;
    }

    function parseToArray($xpath, $class) {
        //$xpathquery = "//span[@class='" . $class . "']";
        $xpathquery = "//*[@class='" . $class . "']";
        $elements = $xpath->query($xpathquery);

        if (!is_null($elements)) {
            $resultarray = array();
            foreach ($elements as $element) {
                $nodes = $element->childNodes;
                foreach ($nodes as $node) {
                    $resultarray[] = $node->nodeValue;
                }
            }
            return $resultarray;
        }
    }

    function processAmazon($keyword, $urls) {
        $fullArr = array();
        $finalArr = array();

        $keywordEncoded = urlencode($keyword);

        $key = 1;
        for ($i = 1; $i <= 7; $i++) {
            $maxResults = 16;
            $startIndex = $i;
            $url = "http://www.amazon.com/s/ref=nb_sb_ss_i_0_10?url=search-alias%3Daps&field-keywords=$keywordEncoded&page=$startIndex";
            $amazonBody = $this->curl_get_contents($url);
            //echo $amazonBody = preg_replace('/^.*\n\n/s', '', $amazonBody, 1);

            $dom = new DomDocument();
            $dom->preserveWhiteSpace = false;

            @$dom->loadHTML($amazonBody);

            $xpath = new DomXpath($dom);
            $div = $xpath->query('//h3[@class="newaps"]');

            foreach ($div as $val) {
                $a = $val->getElementsByTagName('a');
                //echo '<pre>';
                $temp['rank_title'] = $a->item(0)->textContent;
                $temp['rank_url'] = $a->item(0)->getAttribute('href');

                //pr($temp);

                if ($urls) {
                    foreach ($urls as $ur) {
                        if (!isset($finalArr[$ur]) || empty($finalArr[$ur])) {
                            if ((strpos(strtolower($temp['rank_title']), strtolower($keyword)) !== false) && (strpos(strtolower($temp['rank_url']), strtolower($ur)) !== false)) {
                                $fTemp[0] = $key;
                                $fTemp[1] = $i;
                                $fTemp[2] = $temp['rank_title'];
                                $fTemp[3] = '';
                                $fTemp[4] = $temp['rank_url'];
                                $finalArr[$ur] = $fTemp;
                            }
                        }
                    }
                }

                $fullArr[$key] = $temp;
                $key++;
                if ($key > 100) {
                    break;
                }
            }
        }
        //pr($finalArr);
        //pr($fullArr);
        return $finalArr;
        //exit;
    }

    function processYoutube($keyword, $urls) {

        $fullArr = array();
        $finalArr = array();

        $key = 1;
        for ($i = 1; $i <= 5; $i++) {

            $maxResults = 20;
            $startIndex = (($i - 1) * $maxResults) + 1;

            $url = "https://gdata.youtube.com/feeds/api/videos?q=" . urlencode($keyword) . "&orderby=relevance&fields=entry(title,link[@rel='alternate'])&start-index=$startIndex&max-results=$maxResults&v=2&alt=json";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_REFERER, $url);
            $body1 = curl_exec($ch);
            $body = str_replace('$', '', $body1);
            curl_close($ch);
            $data = json_decode($body);

            foreach ($data->feed->entry as $result) {

                $temp['rank_title'] = $result->title->t;
                $temp['rank_url'] = $result->link[0]->href;

                if ($urls) {
                    foreach ($urls as $ur) {
                        if (!isset($finalArr[$ur]) || empty($finalArr[$ur])) {
                            echo $vId = $this->parse_yturl($ur);
                            echo '<br/>';
                            echo $temp['rank_url'];
                            echo '<br/>';
                            if (strpos($temp['rank_url'], $vId) !== false) {
                                $fTemp[0] = $key;
                                $fTemp[1] = $i;
                                $fTemp[2] = $temp['rank_title'];
                                $fTemp[3] = '';
                                $fTemp[4] = $temp['rank_url'];
                                $finalArr[$ur] = $fTemp;
                                break;
                            }
                        }
                    }
                }
                //$fullArr[$key] = $temp;
                $key++;
            }
        }
        //pr($finalArr);
        //pr($fullArr);
        return $finalArr;
    }

    function parse_yturl($url) {
        parse_str(parse_url($url, PHP_URL_QUERY), $yt);
        return isset($yt['v']) ? $yt['v'] : $url;
    }

    function curl_get_contents($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($curl);
        curl_close($curl);
        return $data;
    }

}
