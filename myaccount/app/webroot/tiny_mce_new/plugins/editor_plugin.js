/**
 * editor_plugin.js
 */

(function() {
	var each = tinymce.each;
	
    tinymce.create('tinymce.plugins.LinkVine', {
        shift_down:0,
        has_spin_selected:1,
        spellCheckerMenu:null,
        
        getInfo: function() {
            return {
                longname : 'SEO Nitro editor plugin',
                author : 'Bryxen Software Inc.',
                authorurl : 'http://www.seonitronetwork.com',
                infourl : 'http://',
                version : tinymce.majorVersion + "." + tinymce.minorVersion
            };
        },

		init : function(ed, url) {
            var t = this;
            
            t.url = url;
            t.editor = ed;

            // Register commands
            ed.addCommand('mceLinkVineRewrite', function() {
                newStr = ed.selection.getContent();
                if (newStr == "") newStr = "&nbsp;Edit this text&nbsp;";
                
                newStr = "<spin><spindelimiter>{</spindelimiter><spinvariant>" + newStr + "</spinvariant><spindelimiter>|</spindelimiter>" +
                         "<spinvariant> Edit this text </spinvariant><spindelimiter>}</spindelimiter></spin> ";
                
                ed.selection.setContent( newStr );
            });
            ed.addCommand('mceLinkVineAddVariant', function() {
                
                
                el = ed.selection.getNode();
                while (el && el.tagName != "SPIN") el = el.parentNode;
                
                if (el && (last = el.getElementsByTagName("spindelimiter")) && last.length)
                {
                    newE1 = document.createElement("spindelimiter"); newE1.innerHTML = "|";
                    newE2 = document.createElement("spinvariant"  ); newE2.innerHTML = " Edit this text ";
                    
                    a = el.insertBefore(newE1, last[ last.length-1 ]);
                    b = el.insertBefore(newE2, last[ last.length-1 ]);
                }
            });
            ed.addCommand('mceLinkVineRemVariant', function() {
                el = ed.selection.getNode();
                while (el && el.tagName != "SPINVARIANT") el = el.parentNode;
                
                if (el)
                {
                    vars = el.parentNode.getElementsByTagName("SPINVARIANT");
                    saveHTML = "";
                    
                    if (vars.length == 2)
                        saveHTML = (el == vars[1]) ? vars[0].innerHTML : vars[1].innerHTML;
                    if (vars.length <= 2)
                    {
                        newE = document.createTextNode(saveHTML);
                        el = el.parentNode;
                        
                        el.parentNode.insertBefore(newE, el);
                        el.parentNode.removeChild(el);
                    } else {
                        
                        if (el == vars[0])
                        {
                            if (el.nextSibling) el.parentNode.removeChild(el.nextSibling);
                            el.parentNode.removeChild(el);
                        } else {
                            if (el.previousSibling) el.parentNode.removeChild(el.previousSibling);
                            el.parentNode.removeChild(el);
                        }
                    }
                }
            });
            
            ed.addCommand("mceLinkVineThesaurus", function () {
            	//me = ed.controlManager.get("rewrite4");
            	
            	//if (me) me.showMenu();
            });
            
            ed.addCommand("mceLinkVineAutoSpin", function () {
                me = ed.controlManager.get("rewrite4");
                
                var mytext = ed.getContent();
                if (window.confirm('This will replace all SEONitro favorites, including yours and other users\'.  Are you sure you want to continue?')) {
                    $('#LoadingMessage').get(0).style.display = 'block';
                    
                    alert(1111);
                                  
                    $.post("/post.php", { what: "ReplaceEveronesFavorites", text: mytext },
                    function(data){ 
                        ed.setContent( data );
                        
                        $('#LoadingMessage').get(0).style.display = 'none';
                    });    
                }
                
            });
            
            ed.addCommand("mceLinkVineGetSyns", function () {
                me = ed.controlManager.get("rewrite5");
                
                var mytext = $.trim(ed.selection.getContent());
                
                if (mytext == '') {
                    alert('You must select at least one word.  After selecting at least one word, click Get Synonyms');
                    return;
                }
                
                TheWord = mytext;                  
                $.post("/post.php", { what: "GetSynonyms", word: mytext },
                
                    function(data){ 
                        //ed.setContent( data );
                        $('#favs').get(0).style.display = 'block';
                        $('#favs').get(0).style.position = 'absolute';
                        $('#favs').get(0).style.top = tempY+'px';
                        $('#favs').get(0).style.left = tempX+'px';
                        
                        SelectedSynonyms = null;
                        AllSynonyms = null;
                        SelectedSynonyms = new Array();
                        AllSynonyms = new Array();
                        
                        var syns = data.split('|');
                        var mydiv = $('#synonyms').get(0);
                        mydiv.innerHTML = '';
                        for (var i = 0; i < syns.length; i++) {
                            AllSynonyms.push(syns[i]);
                            
                            var newdiv = document.createElement('div');
                            newdiv.className = 'synonym_i';
                            newdiv.id = 'syn_' + syns[i].replace(/\s+/g, '');
                            newdiv.innerHTML = '<a href="javascript: AddRemoveSynonym(\'' + syns[i] + '\')">' + syns[i] + '</a>';
                            
                            mydiv.appendChild(newdiv);
                        }
                        $('#handlers').get(0).style.display = 'block';
                        
                        AddRemoveSynonym(mytext);
                                            
                });
            });
            
            ed.addCommand("mceLinkVineMyThesaurus", function () {
                me = ed.controlManager.get("rewrite6");
                
                if (me) me.showMenu();
            });
            
            ed.addCommand("mceLinkVineSpinText", function () {
                me = ed.controlManager.get("rewrite7");
                
                var mytext = ed.getContent();
                if (window.confirm('This will take your spin variations and spin your article.  Then, it will create a .zip file for download, containing multiple newly spun articles based on the spin tags you added to your article.  After clicking Ok, you will see a download window.  Are you sure you want to continue?')) {
                    $('#LoadingMessage').get(0).style.display = 'block';
                                  
                    $.post("/post.php", { what: "SpinText", text: mytext },
                    function(data){ 
                        //ed.setContent( data );
                        document.location = '/zip/' + data + '.zip';
                        $('#LoadingMessage').get(0).style.display = 'none';
                    });    
                }
            });
            
            
            
            // Register buttons
            ed.addButton('rewrite1', {title : 'Create a rewrite tag from the current selection'    , cmd : 'mceLinkVineRewrite'   , label: "Rewrite text"    });
            ed.addButton('rewrite2', {title : 'Add a new text variation to the current rewrite tag', cmd : 'mceLinkVineAddVariant', label: "Add variation"   });
            ed.addButton('rewrite3', {title : 'Delete the current text variation'                  , cmd : 'mceLinkVineRemVariant', label: "Delete variation"});
            ed.addButton('rewrite4', {title : 'Auto-Export Spins'                                     , cmd : 'mceLinkVineAutoSpin',   label: "Auto Spin"});
            ed.addButton('rewrite5', {title : 'Get Synonyms'                                       , cmd : 'mceLinkVineGetSyns',    label: "Get Synonyms"});
            ed.addButton('rewrite6', {title : 'My Thesaurus'                                       , cmd : 'mceLinkVineMyThesaurus',label: "My Thesaurus"});
            ed.addButton('rewrite7', {title : 'Export Spins'                                          , cmd : 'mceLinkVineSpinText',   label: "Export Spins"});

            ed.onNodeChange.add(function(ed, cm, e, c, o) {
               
                if (t.has_spin_selected)
                {
                    t.has_spin_selected = 0;
                    var i, body = e;
                    while (body.nodeName != "BODY" && body.parentNode)
                        body = body.parentNode;
                        
                    spins = body.getElementsByTagName("spin");
                    vars  = body.getElementsByTagName("spinvariant");
                    
                    for (i=0; i<spins.length; i++) spins[i].className = "";
                    for (i=0; i<vars .length; i++) vars [i].className = "";
                }
                
                
                if (e.nodeName.toLowerCase() == 'spinvariant')
                {   
                    t.has_spin_selected = 1;
                    e.className = 'spinvariantselected';
                    
                    while (e.parentNode && e.nodeName.toLowerCase() != "spin")
                        e = e.parentNode;
                        
                    if (e.nodeName.toLowerCase() == "spin")
                        e.className = "spinselected";
                }
            });

/*            
            ed.onKeyDown.add(function(ed, e) {
                
                if (e.keyCode == 219 && e.shiftKey)
                {
                    ed.selection.setContent("<spin><spindelimiter class='mceNonEditable'>{</spindelimiter><spinvariant></spinvariant>"+ed.selection.getContent()+"</spinvariant><spindelimiter class='mceNonEditable>}</spindelimiter></spin>");
                    
                    if (e.cancelBubble   ) e.cancelBubble   (true);
                    if (e.stopPropagation) e.stopPropagation(true);
                }
            });
*/        

			ed.onInit.add(function() {
                var t = this;
                
                me = document.getElementById('theEditor_rewrite1'       );  if (me)   me.innerHTML = "<span class='mceButtonLabel' style='padding-left: 3px;'>Rewrite text</span>";
                me = document.getElementById('theEditor_rewrite2'       );  if (me)   me.innerHTML = "<span class='mceButtonLabel' style='padding-left: 3px;'>Add variation</span>";
                me = document.getElementById('theEditor_rewrite3'       );  if (me)   me.innerHTML = "<span class='mceButtonLabel' style='padding-left: 3px;'>Delete variation</span>";
                me = document.getElementById('theEditor_rewrite4'       );  if (me)   me.innerHTML = "<span class='mceButtonLabel' style='padding-left: 3px;'>Autospin</span>"; 
                me = document.getElementById('theEditor_rewrite5'       );  if (me)   me.innerHTML = "<span class='mceButtonLabel' style='padding-left: 3px;'>Get Synonyms</span>"; 
                me = document.getElementById('theEditor_rewrite6_action');  if (me) { me.innerHTML = "<span class='mceButtonLabel' style='padding-left: 6px;'>My Thesaurus</span>"; me.style.width = "inherit"; }
                me = document.getElementById('theEditor_rewrite7'       );  if (me)   me.innerHTML = "<span class='mceButtonLabel' style='padding-left: 3px;'>Export Spins</span>"; 
            
				if (ed.settings.content_css !== false)
					ed.dom.loadCSS(url + '/css/content.css');
			}); 
		},
         
        createControl : function(n, cm) {
            var t = this, c, ed = t.editor;
            
            if (n == 'rewrite6') {
                c = cm.createSplitButton(n, {title : 'Start Thesaurus editor', cmd : 'mceLinkVineMyThesaurus', scope : t, icon: 3, label: 'My Thesaurus'});

                c.onRenderMenu.add(function(c, m) {
                    m.add({title : "Thesaurus Options", 'class' : 'mceMenuItemTitle'}).setDisabled(1);
                    m.add({title : "My Favorites"      , 'class':'mceLinkVineItem', onclick: function() { t._ManageFavs(); }});
                    m.add({title : "Protected Words"   , 'class':'mceLinkVineItem', onclick: function() { t._ProtectedWords(); }});  
                });                
                
                t.spellCheckerMenu = c;
                
                return c;
            }
        },
        
        _ManageFavs : function() {
            $('#LoadingMessage').get(0).style.display = 'block';
            $.post("/post.php", { what: "ManageFavs" },
                function(data){ 
                    
                    $('#myfavs').get(0).innerHTML = '';  
                    AllFavs = null;
                    AllFavs = new Array();
                    
                    $('#managefavsx').get(0).style.display = 'block';
                    $('#managefavsx').get(0).style.position = 'absolute';
                    $('#managefavsx').get(0).style.top = 450+'px';
                    $('#managefavsx').get(0).style.left = tempX-600+'px';
                    
                    var groups = data.split('||');
                    for (var i = 0; i < groups.length; i++) {
                        var x = groups[i].split('|');
                        AllFavs.push(x);
                    } 
                    
                    ListFavorites();
                                            
                    if (data.trim() == '') {                           
                        
                        $('#myfavs').get(0).innerHTML = '<center>You have no favorites</center>';
                    
                        $('#LoadingMessage').get(0).style.display = 'none';
                    }
                    
                    
                    
                }
            );
        }, 
        
        _ProtectedWords : function() {
            $('#LoadingMessage').get(0).style.display = 'block';
            $.post("/post.php", { what: "ProtectedWords" },
                function(data){                    
                    
                    $('#protectedws').get(0).innerHTML = '';  
                    AllProtected = null;
                    AllProtected = new Array();
                    
                    $('#protectedw').get(0).style.display = 'block';
                    $('#protectedw').get(0).style.position = 'absolute';
                    $('#protectedw').get(0).style.top = 450+'px';
                    $('#protectedw').get(0).style.left = tempX-600+'px';
                       
                    if (data.trim() == '') {                           
                        
                        $('#protectedws').get(0).innerHTML = '<center>You have no protected words</center>';
                    
                        $('#LoadingMessage').get(0).style.display = 'none';
                        
                        return;
                    }
                    
                    var groups = data.split('|');
                    for (var i = 0; i < groups.length; i++) {
                        var x = groups[i].split('|');
                        AllProtected.push(x);
                    }
                    
                    for (var i = 0; i < AllProtected.length; i++) {
                        
                        var newdiv = document.createElement('div');
                        
                        newdiv.className = 'group';
                        newdiv.id = 'gr_' + i;
                        
                        if (i % 2 == 1) {
                            var theclass = 'td1';
                        } else { var theclass = 'td0'; }
                        
                        var myhtml = '';
                        myhtml = '<table width="" cellpadding="0" cellspacing="0"><tr>';
                        myhtml += '<td class="'+theclass+'" width="505" valign="middle">&nbsp;&nbsp;'+AllProtected[i]+'</td>';     
                        myhtml += '<td class="'+theclass+'" width="30" align="center" valign="middle"><a href="javascript: RemoveProtected('+i+');"><img src="/imgz/delete-btn.png" border="0" /></a></td>';
                        myhtml += '</tr></table>';
                        
                        newdiv.innerHTML = myhtml;
                        
                        $('#protectedws').get(0).appendChild(newdiv);    
                    }
                    
                    $('#LoadingMessage').get(0).style.display = 'none';
                }
            );
        } 
        
        

    });

    tinymce.PluginManager.add('linkvine', tinymce.plugins.LinkVine);
})();
