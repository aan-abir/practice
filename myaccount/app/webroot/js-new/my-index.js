function DoLogin() {
    $.blockUI({                           
        message: $('#login-popup'),   
        fadeIn: 200, 
        fadeOut: 200, 
        css: { 
            border: 'none',           
            background: 'none', 
            top: '150px',
            padding: '0px',                 
            opacity: .99,
            cursor: null,  
            color: '#fff' 
        }
    });
    
    document.body.style.cursor = 'arrow';
    
    $('.blockOverlay').attr('title','Click to cancel').click(function(){ DoNotLogin(); });   
    
    $('#email').get(0).value = '';
    $('#pass').get(0).value = '';
    $('#reset-email').get(0).value = '';
    
    setTimeout('SlideLogin()', 500);
}

function DoNotLogin() {
    $("#login-popup-mid").slideUp(150)
//    $("#login-popup-content").slideUp(150)
    setTimeout('$.unblockUI()', 500);  
}

function SlideLogin() {             
//    $("#login-popup-content").slideDown(150);
    $("#login-popup-mid").slideDown(150);
}

