<?php

function processRankCampaignEmail($user, $allCamp) {

    if ($user && $allCamp) {
        $allResult = array(); // store all the campaign results in this array
        $iebody = '';

        foreach ($allCamp as $camp) {
            $iebody = '';
            //pr($camp);
            //echo '<br>======================  start a active  campaing =========================<br>';
            $campData = '';
            $keywords = explode("\n", $camp['c_keywords']);
            $urls = explode("\n", $camp['c_urls']);
            //echo $camp[''].'<br>';
            //$gPos = $yPos = $bPos = array();
            foreach ($keywords as $kd) {
                // if user goes out of credit then no data will be updated here
                $noDataFound = mysql_query("SELECT * FROM `rankreports` WHERE keyword = '" . $kd . "' AND campaign_id = " . $camp['id'] . " AND status = 'completed' AND rank_date != '0000-00-00'  ");
                if (mysql_num_rows($noDataFound) == 0) {
                    continue;
                }
                // From Google
                if ($camp['google'] == 1) {
                    //$allResult[$camp['campaign_name']]['Google'] = getCampaignAllData($camp, $kd, 'google');
                    $iebody .= getCampaignAllData($camp, $kd, 'google');
                }

                // From Yahoo
                if ($camp['yahoo'] == 1) {
                    //$allResult[$camp['campaign_name']]['Yahoo'] = getCampaignAllData($camp, $kd, 'yahoo');
                    $iebody .= getCampaignAllData($camp, $kd, 'yahoo');
                }
                // From Bing
                if ($camp['bing'] == 1) {
                    //$allResult[$camp['campaign_name']]['Bing'] = getCampaignAllData($camp, $kd, 'bing');
                    $iebody .= getCampaignAllData($camp, $kd, 'bing');
                }
                // From Amazon
                if ($camp['amazon'] == 1) {
                    $iebody .= getCampaignAllData($camp, $kd, 'amazon');
                }
                // From Youtube
                if ($camp['youtube'] == 1) {
                    $iebody .= getCampaignAllData($camp, $kd, 'youtube');
                }
                //echo '<br>------  end keyword data -----------<br>';
            }
            //echo '<br>======================  end a active  campaing =========================<br>';
            // send individual emaoll to each campagin receipents
            //echo '<pre>'; print_r($allResult); 
            if ($iebody != '') {
                $prepend = '<table width="100%" border="0" style="margin-top: 10px; border: 1px solid #ccc; border-bottom: none;">
                <tr bgcolor="#f9f9f9"><td align="center" style="font-size: 15px;">
                <h2 style="color: #82A544; font-weight: bold;margin:0;">' . $camp['campaign_name'] . '</h2>
                </td></tr>
                </table>
                ';
                $iebody = $prepend . $iebody;
            }

            if ($iebody != '') {
                sendEmail($iebody, $user, $camp); // call function to send email to receiver           
            }

            /**/
            $currdate = date('Y-m-d');
            if (date('Y-m-d', strtotime($camp['send_mail'])) != $currdate) {
                //mysql_query("UPDATE  rankcampaigns SET send_mail = '$currdate' WHERE id = " . $camp['id']);
                if ($iebody != '') {
                    //sendEmail($iebody, $user, $camp); // call function to send email to receiver
                }
            }
            /**/
        } // end while campaign
        //pr($allResult);
        //*
        // generate the html email body and send it to client
    }
}

function sendEmail($data, $user = null, $camp = null) {

    $ebody = file_get_contents('/home/rankratio/public_html/login/app/webroot/rank/processRankCampaignEmail.html');

    $subject = 'Daily Ranking Update for Campaign: ' . $camp['campaign_name'];
    $body = $ebody;
    //pr($data);
    //pr($user);
    //pr($camp);
    //exit;
    // send an email to all the receipants
    $replace = array(
        '/\[headerImage\]/',
        '/\[data\]/',
        '/\[companyName\]/',
        '/\[contactName\]/',
        '/\[phone\]/',
        '/\[website\]/',
        '/\[fmsg\]/',
        '/\[date\]/'
    );

    $img = '<img src="http://rankratio.com/login/images/side-logo.png" style="display:block;" />';
    if ($user['logo']) {
        $img = '<img src="http://rankratio.com/login/branding/' . $user['logo'] . '" style="display:block;" />';
    }
    $companyName = $user['firstname'] . ' ' . $user['lastname'];
    $companyContact = $user['bemail'];
    $companyPhone = $user['phone'];
    $companyWeb = 'www.seonitro.com';
    $footerMsg = '';
    // move  uploaded file to branding dir in webroot
    if ($camp['customize_header']) {
        if ($camp['e_logo'] != '') {
            $img = '<img src="http://rankratio.com/login/branding/' . $camp['e_logo'] . '" width="298" height="67" style="display:block;" />';
        }
        $companyName = $camp['e_company'];
        $companyContact = $camp['e_contact'];
        $companyPhone = $camp['e_phone'];
        $companyWeb = $camp['e_weburl'];
        $footerMsg = nl2br($camp['e_fmsg']);
    }

    $replacement = array(
        $img,
        $data,
        $companyName,
        $companyContact,
        $companyPhone,
        $companyWeb,
        $footerMsg,
        date('M j, Y, g:i a')
    );

    if ($camp['customize_header']) {
        
    }



    $body = preg_replace($replace, $replacement, $body);

    // to other user
    $mailcontent = preg_replace('/\[email\]/', $user['email'], $body);
    //mail($user['email'], 'Your Daily Keyword Tracking Update', $mailcontent, MAIL_HEADERS);
    //mail('mainuljs@gmail.com', 'Your Daily Keyword Tracking Update', $mailcontent, MAIL_HEADERS);
    //exit;
    $toOther = trim($camp['e_email']);
    if (!$toOther) {
        $toOther = $user['email'];
        $toOther = '-';
    }
    if ($toOther) {
        $r = explode(',', $toOther);
        $ar = array_filter($r);
        $actr = array_unique($ar);
        //$actr[] = 'mainuljs@gmail.com';
        foreach ($actr as $er) {
            if (filter_var($er, FILTER_VALIDATE_EMAIL)) {
                mail($er, $subject, preg_replace('/\[email\]/', $er, $body), MAIL_HEADERS);
                //mail('hasantarif@yahoo.com', 'Your Daily Keyword Tracking Update', preg_replace('/\[email\]/', $er, $body), MAIL_HEADERS);
            }
            //$rec .= $er;
        }
    } else {
        mail('support@seonitro.com', 'Your Monthly Keyword Tracking Update', preg_replace('/\[email\]/', 'support@seonitro.com', $body), MAIL_HEADERS);
    }

    //return 1;
}

function getCampaignAllData($camp, $kd, $sEngine) {

    $allResult = array();
    // get first 7 days data
    $gogoleD = mysql_query("SELECT * FROM `rankreports` WHERE keyword = '" . $kd . "' AND campaign_id = " . $camp['id'] . " AND status = 'completed' and engine = '$sEngine' AND rank_date != '0000-00-00'  order by id desc limit 0, 7");
    while ($keywData = mysql_fetch_assoc($gogoleD)) {
        $UnPos = (array) unserialize($keywData['position']);
        foreach ($UnPos as $posUrl => $posArr) {
            $spos = $UnPos[$posUrl];
            $spos[] = $keywData['rank_date'];
            $allResult[$posUrl][$kd][] = $spos;
            //$allResult[$camp['campaign_name']]['Google'][$posUrl]['sevenDays'][$kd][$keywData['rank_date']] = $UnPos[$posUrl];
        }
    }

    //pr($allResult);
    //exit;
    // find position change in this month
    $mq = mysql_query("SELECT * FROM `rankreports` WHERE rank_date like '%" . date('Y-m-') . "%' AND keyword = '" . $kd . "' AND campaign_id = " . $camp['id'] . " AND status = 'completed' AND engine = '$sEngine' AND rank_date != '0000-00-00' order by id ASC limit 0, 1");
    $mr = mysql_fetch_assoc($mq);
    $monthPos = (array) @unserialize($mr['position']);
    if (isset($mr['position'])) {
        foreach ($monthPos as $posUrl => $posArr) {
            $sposs = $monthPos[$posUrl];
            $sposs[] = $mr['rank_date'];
            $allResult[$posUrl][$kd][] = $sposs;
        }
    }

    // find position change from start
    $mq = mysql_query("SELECT * FROM `rankreports` WHERE keyword = '" . $kd . "' AND campaign_id = " . $camp['id'] . " AND status = 'completed' AND engine = '$sEngine' AND rank_date != '0000-00-00' order by id ASC limit 0, 1");
    $mr = mysql_fetch_assoc($mq);
    $monthPos = (array) @unserialize($mr['position']);
    if (isset($mr['position'])) {
        foreach ($monthPos as $posUrl => $posArr) {
            $sposs = $monthPos[$posUrl];
            $sposs[] = $mr['rank_date'];
            $allResult[$posUrl][$kd][] = $sposs;
        }
    }

    $iebody = '';
    if ($allResult) {
        foreach ($allResult as $url => $keyword) {
            $urlTomail = $url;
            $url = 'http://' . str_replace('http://', '', $url);
            if ($url != 'http://0') {
                $iebody .= '<table width="100%" border="0" style="margin-bottom:10px; border: 1px solid #ccc;">
                        <tr bgcolor="#cccccc"><td align="center" style="font-size: 15px;color:#1C64CA;"><strong>Results From ' . ucfirst($sEngine) . '</strong></td></tr>
                        <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="2" cellpadding="2">
                        <thead>
                        <tr>
                        <th colspan="7" style="border-bottom:1px solid #ccc; background-color: #f1f1f3;">
                        <a style="font-size: 13px;color:#82A544;" href="' . $url . '">' . $urlTomail . '</a>
                        </th>
                        </tr>
                        <tr bgcolor="#f9f9f9">
                        <th style="font-size: 13px;">Keyword/phrase </th>
                        <th style="font-size: 13px;">Position</th>
                        <th style="font-size: 13px;">Day Change</th>
                        <th style="font-size: 13px;">Week Change</th>
                        <th style="font-size: 13px;">Month Change</th>
                        <th style="font-size: 13px;">Start Change</th>
                        </tr>
                        </thead>
                        <tbody>';
                $cl = 0;
                foreach ($keyword as $k => $v) {
                    //pr($v);
                    $bgcolor = ($cl % 2 == 0) ? ' bgcolor="#f2f3f4"' : ' bgcolor="#ffffff"';

                    $dc = $wc = $mc = $ac = 0;

                    $tPos = isset($v[0][0]) ? $v[0][0] : 0;
                    $yPos = isset($v[1][0]) ? $v[1][0] : $tPos;
                    $wPos = isset($v['week'][0]) ? $v['week'][0] : $yPos;
                    $mPos = $v[count($v) - 2][0];
                    $aPos = $v[count($v) - 1][0];

                    if ($yPos == 0 && $tPos > 0) {
                        $dc = '+' . abs(100 - $tPos);
                    } else if ($tPos == 0 && $yPos > 0) {
                        $dc = '-' . abs(100 - $yPos);
                    } else {
                        $dc = ($tPos < $yPos) ? '+' . abs($yPos - $tPos) : '-' . ($tPos - $yPos);
                    }
                    if ($wPos == 0 && $tPos > 0) {
                        $wc = '+' . abs(100 - $tPos);
                    } else if ($tPos == 0 && $wPos > 0) {
                        $wc = '-' . abs(100 - $wPos);
                    } else {
                        $wc = ($tPos < $wPos) ? '+' . abs($wPos - $tPos) : '-' . ($tPos - $wPos);
                    }
                    if ($mPos == 0 && $tPos > 0) {
                        $mc = '+' . abs(100 - $tPos);
                    } else if ($tPos == 0 && $mPos > 0) {
                        $mc = '-' . abs(100 - $mPos);
                    } else {
                        $mc = ($tPos < $mPos) ? '+' . abs($mPos - $tPos) : '-' . ($tPos - $mPos);
                    }
                    if ($aPos == 0 && $tPos > 0) {
                        $ac = '+' . abs(100 - $tPos);
                    } else if ($tPos == 0 && $aPos > 0) {
                        $ac = '-' . abs(100 - $aPos);
                    } else {
                        $ac = ($tPos < $aPos) ? '+' . abs($aPos - $tPos) : '-' . ($tPos - $aPos);
                    }


                    $dc = ($dc == '-0' || $dc == '0') ? 'NA' : $dc;
                    $wc = ($wc == '-0' || $wc == 0) ? 'NA' : $wc;
                    $mc = ($mc == '-0' || $mc == 0) ? 'NA' : $mc;
                    $ac = ($ac == '-0' || $ac == 0) ? 'NA' : $ac;
                    $tPos = ($tPos == '-0' || $tPos == 0) ? 'NA' : $tPos;


                    $iebody .= '<tr' . $bgcolor . '>
                            <td  style="font-size: 12px;">' . $k . '</td>
                            <td  style="font-size: 12px;text-align: center;">' . $tPos . '</td>
                            <td  style="font-size: 12px;text-align: center;">' . $dc . '</td>
                            <td  style="font-size: 12px;text-align: center;">' . $wc . '</td>
                            <td  style="font-size: 12px;text-align: center;">' . $mc . '</td>
                            <td  style="font-size: 12px;text-align: center;">' . $ac . '</td>
                            </tr>';
                }
                $iebody .= '
                        </tbody>';
                $iebody .= '
                        </table>
                        </td>
                        </tr>
                        </table>
                        ';
            }
        }
    }
    //exit;
    return $iebody;
}
