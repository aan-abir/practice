$(document).ready(function() {
    // make the left nave active
    try {
        // make selected the active nav
        selected = $(".leftParentIndex").val().split(",");
        //alert(selected);
        $li = selected[0];  // top li
        $cLi = selected[1]; // sub li selected

        // make top most li active
        $("ul#nav_side li").each(function(i, elm) {
            $(elm).find('a').removeClass('bold');
            $(elm).find('span').removeClass('blue');
        });

        $curSelLi = $("ul#nav_side").find('.top').eq($li);
        $($curSelLi).find('a:eq(0)').addClass('bold');
        //  End make top most li active

        // close all the sub nav menu
        $("ul#nav_side ul.sub").each(function(i, elm) {
            $(elm).removeClass('open'); // collasp menu
            $(elm).children('li').removeClass('current'); // remove selected submenu class
        });

        // now make the current one active
        $($curSelLi).find('ul.sub').removeClass('open').addClass('open');
        $($curSelLi).find('ul.sub li:eq(' + $cLi + ')').removeClass('current').addClass('current');

    } catch (e) {
    }
});

// A common ajax get call request
$(".callAction").live('click', function(e) {
    e.preventDefault();
    if (!confirm("Are you sure to execute the action?"))
        return false;
    _this = $(this);
    sucMsg = $(this).attr('data-smsg');
    // load state areas
    $.ajax({
        type: "get",
        url: _this.attr('href'),
        beforeSend: function() {
            //$("html").addClass('loadstate');
        },
        data: false
    }).done(function(msg) {
        $("html").addClass('loadstate');
        $("html").removeClass('loadstate');
        if (msg == '') {
            // $("#content").prepend('<div class="alert dismissible alert_green"><img height="24" width="24" src="/images/icons/small/white/alert.png"><strong>'+sucMsg+'</strong></div>');
            window.location.replace(window.location.href);
        } else {
            //alert(msg); // debug only
            $("#content").prepend('<div class="alert alert-error"><img height="24" width="24" src="/images/icons/small/white/alarm_bell.png"><strong> Could not execute the request.. Try Again</strong></div>');
        }
    });
});

// A common ajax fetch call request
$(".fetchAction").live('click', function(e) {
    e.preventDefault();
    if (!confirm("Your request will be processed shortly. Please have patience."))
        return false;
    _this = $(this);
    sucMsg = $(this).attr('data-smsg');
    // load state areas
    $.ajax({
        type: "get",
        url: _this.attr('href'),
        beforeSend: function() {
            $("html").addClass('loadstate');
        },
        data: false
    }).done(function(msg) {
        //$("html").removeClass('loadstate');
        window.location.reload();
    });
});

// A common ajax fetch dropdown
$(".fetchDropdown").live('change', function(e) {
    e.preventDefault();
    _this = $(this);
    $.ajax({
        type: "post",
        url: _this.attr('rel'),
        data: {
            'value1': _this.val()
        },
        beforeSend: function() {
            $('#horizontalLoader').show();
            $('#domainDropDownAjaxLoaded').hide();
        }
    }).done(function(msg) {
        //alert(msg);
        $('#domainDropDownAjaxLoaded').empty();
        $('#domainDropDownAjaxLoaded').html(msg);
        $('#horizontalLoader').hide();
        $('#domainDropDownAjaxLoaded').fadeIn();
    });
});


// open new popup window
$(".newwindow").live('click', function(e) {
    e.preventDefault();
    _this = $(this);
    window.open(_this.attr('href'), "SEONitro", "location=1,status=1,scrollbars=1,width=800,height=600,resizable=yes,menubar=yes,toolbar=yes");
});

