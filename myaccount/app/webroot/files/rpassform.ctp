<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
        <td class="logo">
            <div class="bodycopy">
                <h4>Dear [firstname] [lastname]</h4> 
                <p>As per your request find your password below. Please keep it in a safe place. </p>
                <p><strong>Your Access Credentials: </strong></p>
                <p>------------------------------------</p>
                <p>User Name: [username]</p>
                <p>Password: [password]</p>
                <p>Email: [email]</p>
                <p>------------------------------------</p>
            </div>
        </td>
    </tr>
</table>
