<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:12px;">
            <table width="100%" border="0" cellspacing="2" cellpadding="2" style="margin-bottom:10px;">
                <tr>
                    <td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;">
                        <div style="font-size:18px; color:#006d00;width:100%;">Congratulation! [firstname] [lastname],</div>
                        <div>
                            Your account has been created at RankRatio Dashboard! As a reminder, Your Information are as follows:
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="100" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;">User Name: [username]</td> 
                </tr>
                <tr>     
                    <td width="100"  style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;">Password: [password]</td>
                </tr>
                <tr>    
                    <td width="100"  style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;">Email: [email]</td> 
                </tr>
                <tr>
                    <td align="center" style="text-align: center;font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;">
                        <strong>Enjoy Your dashbaord - <a href="http://rankratio.com/login">Sign In</a></strong>
                    </td>
                </tr>    
            </table>
        </td>
    </tr>
</table>

