<?php

    class ClientsController extends AppController
    {

        public $uses = array('User', 'Domain', 'Activitylog');

        function beforeFilter()
        {
            if (!method_exists($this, $this->params['action'])) {
                $this->redirect(array('action' => 'dashboard'));
            }
            parent::beforeFilter();
            $this->Auth->allow('pr_corn_system');
            $this->Auth->allow('analyzedomain_cron');
            $this->Auth->allow('ld_page_rank_cron');
            $this->Auth->allow('ld_obls_link_cron');
            $this->Auth->allow('credit_needed_for_current_ranking');
            $this->Auth->allow('cleanbysubid');
            $data_url = array();
            if ($this->Auth->user('id') != 0) {
                Configure::write('debug', 2);
                Configure::write('Cache.disable', true);
            }
        }

        function isAuthorized($user)
        {
            if (isset($user['role']) && (('client' === $user['role']))) {
                return true;
            }
            return false; // The rest don't
        }

        function beforeRender()
        {

            /// get clients page instructions
            $gi = @$this->Instruction->findBypagename(strtolower($this->params['action']));
            $topInstruction = '';
            if ($gi) {
                $topInstruction = $gi['Instruction']['toptext'];
            }
            $this->set(compact("topInstruction"));
            parent::beforeRender();
        }

        //3f0361396a47bf032d746e5b87be871c55fabc2d
        // log out users
        function logout()
        {
            $this->Session->setFlash('You\'ve successfully logged out.', 'default', null, 'bad');
            $this->redirect($this->Auth->logout());
        }

        // check if a user can edit, delete a record. I.e IS that belongs to him or not
        /*
        *  $model = User for example
        * $searchid = searching ID in table that belongs to this looged user
        *  $relationaKey = logged in user ID field name in that table
        */
        public function haveAuthToDoAction($model = '', $searchid = '', $relationalKey = '')
        {
            $cond = $model . '.' . $relationalKey . ' = ' . $this->Auth->user('id') . ' AND ' . $model . '.id = ' . $searchid;
            return $this->{$model}->find('count', array('recursive' => -1, 'conditions' => array($cond)));
        }

        function new_dashboard()
        {
            $userId = $this->Auth->user('id');

            $this->loadModel('Campaign');
            $this->loadModel('Anchortext');
            $this->loadModel('Events');
            $this->loadModel('Welcomenews');


            $wmsg = $this->Welcomenews->find('first', array('order' => 'id desc'));
            $this->set('wmsg', $wmsg);


            // show 3 month activity past, current and next month
            $activity = $this->Events->find('all', array(
                'conditions' => array(" status = 1 "),
                'order' => 'modified DESC'
            ));


            /* title: 'Lunch',
            start: new Date(y, m, d, 12, 0),
            end: new Date(y, m, d, 14, 0),
            allDay: false,
            color: '#9FC569'*/

            // assing the calendar events
            $i = 1;
            $calData = '';
            foreach ($activity as $events) {
                $color = $events['Events']['color'] != '' ? $events['Events']['color'] : '#fffff';
                $t = $events['Events']['title'];
                $start = $events['Events']['starttime'];
                $end = $events['Events']['endtime'];
                $calData .= '{';
                $calData .= "title:'$t',start:'$start',description: '" . nl2br($events['Events']['description']) . "', end:'$end','color':'$color'" . ($events['Events']['url'] != '' ? ",'url':'" . $events['Events']['url'] . "'" : '');
                $calData .= '},';
                $i++;
            }
            $calData = trim($calData, ',');
            //pr($calData);
            $this->set(compact('calData'));
            $this->set('navIndex', '0,0');
            $this->set('title_for_layout', 'dashboard');
        }

        function tools()
        {
            $userId = $this->Auth->user('id');

            $this->loadModel('Campaign');
            $this->loadModel('Anchortext');

            $this->Campaign->bindModel(array(
                'belongsTo' => array(
                    'User' => array('foreignKey' => 'user_id'),
                ),
                'hasMany' => array(
                    'Anchortext' => array('foreignKey' => 'campaign_id'),
                )
            ));
            $siteAllCamps = $this->Campaign->find('all', array(
                'conditions' => array("Campaign.user_id = $userId AND Campaign.custom = 0"),
                'recursive' => 1
            ));
            $this->set(compact('siteAllCamps'));

            $serviceList = $this->getMyPlanInfo();

            $finalCircleArr = array();
            if ($serviceList) {
                foreach ($serviceList as $key => $service) {
                    $serviceId = $service['Service']['id'];
                    $serviceLimit = isset($service['Service']['servicelimit']) ? $service['Service']['servicelimit'] : 0;
                    $extraUnit = isset($service['Service']['extra_unit']) ? $service['Service']['extra_unit'] : 0;
                    $serviceLimit = $serviceLimit + $extraUnit;
                    $serviceUsed = isset($service['Service']['service_used']) ? $service['Service']['service_used'] : 0;
                    $finalCircleArr[$serviceId]['service_used'] = $serviceUsed;
                    $finalCircleArr[$serviceId]['service_limit'] = $serviceLimit;
                    $finalCircleArr[$serviceId]['percentage_used'] = @(100 - ($serviceUsed / $serviceLimit) * 100);
                    $finalCircleArr[$serviceId]['service_id'] = $serviceId;
                }
            }

            $this->set(compact('finalCircleArr'));

            // show 3 month activity past, current and next month
            $activity = $this->Activitylog->find('all', array(
                'fields' => "job,created,activitytype",
                'conditions' => array("showincalendar = '1' AND user_id = " . $this->Auth->user('id')),
                'order' => 'created ASC, activitytype ASC'
            ));
            $colorCode = array(
                'historicalranking' => '#ED7A53',
                'socialcontent' => '#0e830e',
                'profile' => '#1baa47',
                'historicalranking' => '#b40b8a',
                'sitewide' => '#0e830e',
                'campaign' => '#c1ba05',
                'manaul sites' => '#a06508',
                'mashup' => '#1baa47',
                'linkemperor' => '#cb5c02',
                'mysites' => '#1baa47',
                'bazookablast' => '#cb5c02',
                'guestpost' => '#085c0e',
                'automation' => '#1baa47',
                'socialcontent' => '#09aea0',
                'rankcampaign' => '#1baa47',
            );

            $links = array(
                'automation' => 'managesubmission',
                'profile' => 'editprofile',
                'sitewide' => 'manageblogroll',
                'campaign' => 'managecampaign',
                'manaul sites' => 'manualnetworks',
                'mashup' => 'viewmashup',
                'linkemperor' => 'linkemperorinprogress',
                'mysites' => 'mysites',
                'bazookablast' => 'bazookainprogress',
                'guestpost' => 'assignedguestpost',
                'socialcontent' => 'managemashup',
                'rankcampaign' => 'rankresults',
                'historicalranking' => 'historicalranking',
            );

            $sameTypeOfJob = array();
            // assing the calendar events
            foreach ($activity as $log) {
                $start = substr($log['Activitylog']['created'], 0, 10);
                $title = trim($log['Activitylog']['job']);
                $color = @$colorCode[$log['Activitylog']['activitytype']];
                $url = @$links[$log['Activitylog']['activitytype']];
                if (@key_exists($title, $sameTypeOfJob[$start])) {
                    $sameTypeOfJob[$start][$title]['count']++;
                } else {
                    $sameTypeOfJob[$start][$title] = array('color' => $color, 'count' => 1, 'url' => $url);
                }
            }

            // assing the calendar events
            $i = 1;
            $calData = '';
            foreach ($sameTypeOfJob as $date => $events) {
                $ev = (array)$events;
                foreach ($ev as $title => $countColor) {
                    $color = $countColor['color'];
                    $t = $title . ' (' . $countColor['count'] . ')';
                    $calData .= '{';
                    $calData .= "title:'$t',start:'$date','color':'$color'" . ($countColor['url'] != '' ? ",'url':'" . Router::url($countColor['url']) . "'" : '');
                    $calData .= '},';
                    $i++;
                }
            }
            $calData = trim($calData, ',');
            //pr($calData);
            $this->set(compact('calData'));

            $this->set('navIndex', '0,0');
            $this->set('title_for_layout', 'dashboard');
        }

        /*
        *  Edit a Clients profile
        *
        */

        function editprofile()
        {
            if (!empty($this->data)) {
                $uid = $this->Auth->user('id');
                $this->User->id = $uid;
                if (trim($this->data['UserEx']['password']) != '') {
                    $this->request->data['User']['password'] = AuthComponent::password($this->data['UserEx']['password']);
                    $this->request->data['User']['ccode'] = $this->data['UserEx']['password'];
                }
                $this->User->save($this->data['User'], false);
                // set msg flash
                $eMsg = 'Your Profile has been updated';
                $this->Session->setFlash($eMsg, 'flash_success');
                // save this activity to log
                $this->setActivity($this->Auth->user('id'), $showincalendar = '0', $generator = 'user', $activitytype = 'profile', $job = $eMsg);
                $this->redirect($this->params['action']);
            }
            $data = $this->User->find('first', array('conditions' => array("User.id = " . $this->Auth->user('id'))));
            $this->data = $data;
            // $this->set('data', $data);
            //$this->set('navIndex', '0,0');
            $this->set('title_for_layout', 'My Profile');
        }

        // company branding information
        function brands()
        {
            $extRes = array('jpg', 'jpeg', 'gif', 'png');
            $this->User->id = $this->Auth->user('id');
            if (!empty($this->data['User'])) {
                $ins = $this->User->findById($this->Auth->user('id'));
                if ($this->data['User']['logo']['name'] != '') {
                    if ($this->data['User']['logo']['error'] == 0) {
                        if (in_array($this->fileExt($this->data['User']['logo']['name']), $extRes)) {
                            $folder = WWW_ROOT . 'branding';
                            $fileName = time() . '_' . stripslashes(preg_replace("~\s+~", "_", str_replace("'", "", $this->data['User']['logo']['name'])));
                            $ufile = $folder . '/' . $fileName;

                            $this->ImageResizer->resizeImage($this->data['User']['logo']['tmp_name'], array(
                                'output' => $ufile,
                                'ignoreSmallImages' => true,
                                'maxHeight' => 300,
                                'maxWidth' => 300
                            ));
                            //@move_uploaded_file($this->data['User']['logo']['tmp_name'], $ufile);
                            $unfile = $folder . '/' . $ins['User']['logo'];
                            @unlink($unfile);
                            $this->request->data['User']['logo'] = $fileName;
                        } // end if in_array
                        else {
                            $eMsg = 'File Not Supported';
                            $this->Session->setFlash($eMsg, 'flash_error');
                            $this->redirect($this->params['action']);
                        }
                    }
                } // end picure upload
                else {
                    $this->request->data['User']['logo'] = $ins['User']['logo'];
                }
                // delete old logo file
                if (isset($this->data['User']['delOld'])) {
                    $unfile = WWW_ROOT . 'branding/' . $ins['User']['logo'];
                    @unlink($unfile);
                    $this->request->data['User']['logo'] = '';
                }
                //pr($this->data['User']); exit;
                $this->User->save($this->data['User']);
                $eMsg = 'Branding information has been updated successfully';
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->redirect($this->params['action']);
            }
            $data = $this->User->findById($this->Auth->user('id'));
            $this->data = $data;
            $this->set('data', $data);
            $this->set('title_for_layout', 'Edit Branding Information');
        }

        function addcampaign()
        {

            $this->set('navIndex', '1,0');
            $this->set('title_for_layout', 'New Campaign');

            $this->loadModel('Campaign');

            $serviceId = 12;  // Campaign
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from your extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {
                $unitPrice = floatval($userServiceCredit['unit_price']);

                $this->loadModel('Anchortext');

                if ($this->request->is('post')) {
                    $this->request->data['Campaign']['user_id'] = $this->Auth->user('id');
                    $this->Campaign->set($this->data['Campaign']);
                    //pr($this->data); exit;
                    if ($this->Campaign->validates()) {
                        if ($this->Campaign->save($this->data['Campaign'], false)) {
                            $campaignId = $this->Campaign->getLastInsertId();
                            // save all the campaings settins
                            $rawData = $this->data['Campaign']['settings'];
                            $this->save_anchor_texts($rawData, $campaignId);

                            $updateAccount['user_id'] = $userId;
                            $updateAccount['service_id'] = $serviceId;
                            $updateAccount['amount'] = $unitPrice;
                            $updateAccount['remarks'] = 'Credit deducted for Campaign.';

                            $this->updateAccountDebit($updateAccount);

                            if (!$userServiceCredit['eligible'] && $userServiceCredit['extra_credit_eligible']) {
                                $this->User->id = $userId;
                                $newExtraCredit = ($userServiceCredit['extra_credit'] - $unitPrice);
                                $this->User->saveField('extra_credit', $newExtraCredit);
                            }
                            // set msg flash
                            $eMsg = 'Your Campaign has been created successfully';
                            $this->Session->setFlash($eMsg, 'flash_success');
                            // save this activity to log
                            $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'campaign', $job = 'added new campaign');
                            $this->redirect($this->params['action']);
                        }// end if
                    } else {
                        $this->generateError('Campaign');
                    }
                }
            }
        }

        function save_anchor_texts($rawData, $campaignId)
        {
            $anchorJsonArr = array();
            $this->loadModel('DclCampaign');

            foreach ($rawData as $k => $campsetting) {
                $tDensity = trim($campsetting['targetDensity']);
                $aText = trim($campsetting['anchortexts']);
                $aNoFollow = (isset($campsetting['noFollow']) && in_array($campsetting['noFollow'], array('1', 'on'))) ? 1 : 0;
                $aTextArr = (array)explode(",", $aText);
                $uText = trim($campsetting['internalPageUrl']);
                //$uText = preg_replace('/https?\:\/\//i', '', $uText);
                //$uText = preg_replace('/www\./i', '', $uText);
                $uTextArr = (array)explode(",", $uText);
                $tDensityEach = $tDensity / count($aTextArr);
                // discard empty row
                if (($tDensity == '') || ($aText == '')) {
                    continue;
                }
                // all multiple anchor text with commas
                // for ( $ii = 0; $ii < count($aTextArr); $ii++ ) {
                $this->Anchortext->create();
                $d = array(
                    '',
                    'user_id' => $this->Auth->user('id'),
                    'campaign_id' => $campaignId,
                    'targetDensity' => $tDensity, // $tDensityEach,
                    'anchortexts' => $aText, // $aTextArr[$ii],
                    'internalPageUrl' => $uText, // ( isset($uTextArr[$ii]) ? $uTextArr[$ii] : '') ,
                    'nofollow' => $aNoFollow,
                    'addtime' => date('Y-m-d G:i:s', time()),
                );
                $temp[0] = $aText;
                $temp[1] = $uText;
                $temp[2] = $aNoFollow;
                $temp[3] = $tDensity;
                $anchorJsonArr[] = $temp;
                $this->Anchortext->save($d, false);
            }

            $updateDclCampaign['id'] = $campaignId;
            $updateDclCampaign['anchors'] = json_encode($anchorJsonArr);
            $this->DclCampaign->save($updateDclCampaign, false);
        }

        // manage all the campaings
        function managecampaign()
        {

            $this->loadModel('Campaign');
            $this->loadModel('Anchortext');

            $this->Campaign->bindModel(array(
                'belongsTo' => array(
                    'User' => array('foreignKey' => 'user_id'),
                )
            ));

            $this->Campaign->bindModel(array(
                'hasMany' => array(
                    'Anchortext' => array('foreignKey' => 'campaign_id'),
                )
            ));

            $allCamps = $this->Campaign->find('all', array(
                'conditions' => array("Campaign.user_id = " . $this->Auth->user('id') . " AND Campaign.custom = 0"),
                'recursive' => 1,
                'order' => 'Campaign.id DESC'
            ));
            $this->set(compact('allCamps'));

            //pr($allCamps);
            $this->set('navIndex', '1,1');
            $this->set('title_for_layout', 'Manage Campaign');
        }

        // edit a campaing
        function editcampaign($cId = null)
        {
            if (!$cId) {
                $this->redirect(array('action' => 'managecampaign'));
            }
            $this->loadModel('Campaign');
            $this->loadModel('Anchortext');
            if (isset($this->data['Campaign']['campignname'])) {
                //pr($_POST);
                $cId = $this->data['Campaign']['id'];
                $this->Campaign->id = $cId;
                $this->Campaign->set($this->data['Campaign']);
                if ($this->Campaign->validates()) {
                    if ($this->Campaign->save($this->data['Campaign'], false)) {
                        $this->Anchortext->deleteall(array('Anchortext.campaign_id' => $cId), false);

                        $rawData = $this->data['Campaign']['settings'];
                        $this->save_anchor_texts($rawData, $cId);

                        if (isset($this->data['Custom']['instantly']) && $this->data['Custom']['instantly'] == 1) {
                            //@$this->updateCampaignInstantly($cId);
                        }
                        //pr($response);
                        //exit;
                        // delete all other recored for this campaign
                        // pr( $needToDelete);
                        // set msg flash
                        $eMsg = 'Your Campaign has been updated successfully';
                        $this->Session->setFlash($eMsg, 'flash_success');
                        $rDirect = BASEURL . 'clients/editcampaign/' . $cId;
                        // save this activity to log
                        $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'campaign', $job = 'edited campaign');

                        $this->redirect($rDirect);
                    } else {
                        // set msg flash
                        $eMsg = 'Could not Update Campaign';
                        $this->Session->setFlash($eMsg, 'flash_error');
                        $rDirect = BASEURL . 'clients/editcampaign/' . $cId;
                        $this->redirect($rDirect);
                    }
                } else {
                    $this->generateError('Campaign');
                }
            }


            $this->Campaign->bindModel(array(
                'belongsTo' => array(
                    'User' => array('foreignKey' => 'user_id'),
                )
            ));
            $this->Campaign->bindModel(array(
                'hasMany' => array(
                    'Anchortext' => array('foreignKey' => 'campaign_id', 'order' => ' targetDensity DESC'),
                )
            ));

            $data = $this->Campaign->findById($cId);
            $this->data = $data;


            //pr($data);
            $this->set('data', $data);
            $this->set('cId', $cId);
            $this->set('navIndex', '1,1');
            $this->set('title_for_layout', 'Edit Campaign');
        }

        // delete a anchor text row from a campaign
        function delAnchorText($aid = null)
        {
            if (empty($aid))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Anchortext');
            if ($this->Anchortext->delete($aid, false)) {
                // save this activity to log
                $this->setActivity($this->Auth->user('id'), $showincalendar = '0', $generator = 'user', $activitytype = 'campaign', $job = 'deleted campaign anchor id - ' . $aid);
                $this->Session->setFlash('Anchor Text has been deleted!', 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit;
        }

        // delete a campaign
        function deletecampaign($id)
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Campaign');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Campaign', $id, 'user_id')) {
                echo 'error';
                exit;
            }
            $this->loadModel('Anchortext');

            $this->Campaign->id = $id;
            $userId = $this->Auth->user('id');
            $oData = $this->Campaign->read();
            if ($this->Campaign->delete($id, false)) {
                $userCredits = $this->Usercredit->find('first', array('conditions' => "Usercredit.user_id = $userId AND Usercredit.service_id = 12"));
                if ($userCredits) {
                    $serviceUsed = $userCredits['Usercredit']['service_used'] - 1;
                    if ($serviceUsed >= 0) {
                        $this->Usercredit->id = $userCredits['Usercredit']['id'];
                        $this->Usercredit->saveField('service_used', $serviceUsed);
                    }
                }
                // delete all the anchor recored under this campaign
                $this->Anchortext->deleteall(array('Anchortext.campaign_id' => $id), false);
                $eMsg = 'Campaign with all records has been deleted successfully!';
                // save this activity to log
                $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'campaign', $job = 'deleted campaign  - ' . $oData['Campaign']['campignname']);
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        function viewcampaign($cId = null)
        {
            if (empty($cId)) {
                $rDirect = BASEURL . '/clients/addcampaign/';
                $this->redirect($rDirect);
            }

            $this->loadModel('Campaign');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Campaign', $cId, 'user_id')) {
                $rDirect = BASEURL . 'clients/addcampaign/';
                return $this->redirect($rDirect);
            }

            $this->loadModel('Trackeddomain');

            $this->set('cId', $cId);
            $userId = $this->Auth->user('id');

            $this->loadModel('Anchortext');
            $this->loadModel('Blogroll');

            $this->Campaign->bindModel(array(
                'hasMany' => array(
                    'Anchortext' => array('foreignKey' => 'campaign_id', 'order' => ' targetDensity DESC'),
                )
            ));

            $campaignInfo = $this->Campaign->find('first', array(
                'conditions' => array("Campaign.user_id = $userId AND Campaign.id = $cId AND Campaign.custom = 0"),
                'recursive' => 1
            ));

            if ($campaignInfo) {

                $anchorList = $this->getAnchorListByCid($cId);
                $this->set(compact('campaignInfo'));
                $this->set(compact('anchorList'));

                $blogrollList = $this->Blogroll->find('all', array(
                    'conditions' => array("Blogroll.campaign_id = $cId"),
                    'recursive' => -1
                ));

                $blogrollResults = array();
                $blogrollDomainIdArr = array();
                $blogrollFinalResults = array();
                if ($blogrollList) {
                    foreach ($blogrollList as $bRoll) {
                        $blogrollDomainIdArr[] = $bRoll['Blogroll']['domain_id'];
                    }

                    $blogrollDomainIdArr = array_unique($blogrollDomainIdArr);

                    foreach ($blogrollDomainIdArr as $key => $bId) {
                        $blogrollFinalResults[$key]['in_sidebar'] = 0;
                        $blogrollFinalResults[$key]['in_footer'] = 0;
                        foreach ($blogrollList as $bRollList) {
                            if ($bRollList['Blogroll']['domain_id'] == $bId) {
                                $blogrollFinalResults[$key]['Blogroll'] = $bRollList['Blogroll'];
                                if ($bRollList['Blogroll']['setting_option'] == 1) {
                                    $blogrollFinalResults[$key]['in_sidebar'] += 1;
                                }
                                if ($bRollList['Blogroll']['setting_option'] == 2) {
                                    $blogrollFinalResults[$key]['in_footer'] += 1;
                                }
                            }
                        }
                    }
                }
                //pr($blogrollFinalResults); exit;
                $this->set(compact('blogrollFinalResults'));

                $campaignResults = $this->Trackeddomain->find('all', array(
                    'conditions' => array("Trackeddomain.campaign_id = $cId "),
                    'recursive' => -1
                ));

                $this->set(compact('campaignResults'));
            } else {
                $this->redirect(array('action' => 'managecampaign'));
            }

            $this->set('navIndex', '1,1');
            $this->set('title_for_layout', 'View Campaign Details');
        }

        function viewkeyword($cId, $kw)
        {
            if (empty($cId)) {
                $rDirect = BASEURL . '/clients/addcampaign/';
                $this->redirect($rDirect);
            }
            $this->loadModel('Campaign');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Campaign', $cId, 'user_id')) {
                $rDirect = BASEURL . '/clients/addcampaign/';
                $this->redirect($rDirect);
            }

            $this->loadModel('Blogroll');
            $this->loadModel('Trackeddomain');

            $userId = $this->Auth->user('id');
            $kw = base64_decode($kw);
            $this->set(compact('kw'));

            $campaignInfo = $this->Campaign->find('first', array(
                'conditions' => array("Campaign.user_id = $userId AND Campaign.id = $cId AND Campaign.custom = 0"),
                'recursive' => -1
            ));

            $this->set(compact('campaignInfo'));

            $campaignBlogrolls = $this->Blogroll->find('list', array(
                'fields' => 'domain_id',
                'conditions' => array("Blogroll.campaign_id = $cId"),
                'recursive' => -1
            ));

            $blogrollDomainString = '0';
            if ($campaignBlogrolls) {
                $blogrollDomainString = implode(',', array_unique($campaignBlogrolls));
            }

            $blogrollResults = $this->Trackeddomain->find('all', array(
                'conditions' => "Trackeddomain.campaign_id = -1 AND Trackeddomain.domain_id IN($blogrollDomainString)"
            ));

            if ($blogrollResults) {
                foreach ($blogrollResults as $k => $cR) {
                    $pingerResult = unserialize($cR['Trackeddomain']['results']);
                    $keywordFound1 = 0;
                    $keywordFound2 = 0;
                    if (isset($pingerResult['sidebar_html'])) {
                        $fCount1 = 0;
                        foreach ($pingerResult['sidebar_html'] as $anchorSet) {
                            $fCount1 = substr_count(strip_tags($anchorSet), $kw);
                            if ($fCount1 > $keywordFound1) {
                                $keywordFound1 = $fCount1;
                            }
                        }
                    }
                    if (isset($pingerResult['footer_html'])) {
                        $fCount2 = 0;
                        foreach ($pingerResult['footer_html'] as $anchorSet) {
                            $fCount2 = substr_count(strip_tags($anchorSet), $kw);
                            if ($fCount2 > $keywordFound2) {
                                $keywordFound2 = $fCount2;
                            }
                        }
                    }

                    $blogrollResults[$k]['Trackeddomain']['keyword_in_sidebar'] = $keywordFound1;
                    $blogrollResults[$k]['Trackeddomain']['keyword_in_footer'] = $keywordFound2;
                }
            }

            $this->paginate = array(
                'conditions' => array("Trackeddomain.campaign_id = $cId "),
                'limit' => 10,
                'recursive' => -1
            );

            $campaignResults = $this->paginate('Trackeddomain');

            if ($campaignResults) {
                foreach ($campaignResults as $k => $cR) {
                    $pingerResult = unserialize($cR['Trackeddomain']['results']);
                    if (count($pingerResult)) {
                        $keywordFound = 0;
                        foreach ($pingerResult as $anchorSet) {
                            $fCount = 0;
                            foreach ($anchorSet as $anchorLink) {
                                if (strip_tags($anchorLink) == $kw) {
                                    $fCount++;
                                }
                            }
                            if ($fCount > $keywordFound) {
                                $keywordFound = $fCount;
                            }
                            $campaignResults[$k]['Trackeddomain']['keyword_in_content'] = $keywordFound;
                        }
                    }
                }
            }

            $this->set(compact('blogrollResults'));
            $this->set(compact('campaignResults'));

            $this->set('cId', $cId);
            $this->set('navIndex', '1,1');
            $this->set('title_for_layout', 'View Keyword Details');
        }

        function addblogroll()
        {
            $this->set('title_for_layout', 'New Sitewide Link');

            $serviceId = 9;
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            $this->loadModel('Campaign');
            $this->loadModel('Anchortext');
            $this->loadModel('Setting');
            $this->loadModel('Blogroll');


            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from your extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {
                $unitPrice = floatval($userServiceCredit['unit_price']);

                $domainIds = $this->Domain->find('list', array(
                    'conditions' => 'is_indexed = 1',
                    'fields' => 'id,id'
                ));

                $domainIds = array_values($domainIds);

                if ($this->request->is('post')) {

                    $domain_id = $domainIds[rand(0, count($domainIds) - 2)];
                    $this->request->data['Blogroll']['domain_id'] = $domain_id;
                    $setting_option = $this->data['Blogroll']['setting_option'];
                    $spotName = 'sidebar';
                    if ($setting_option == 1) {
                        $spotName = 'sidebar';
                    } elseif ($setting_option == 2) {
                        $spotName = 'footer';
                    }
                    if ($this->data['Custom']['networktype'] == '2') {
                        $settingsInfo = $this->Setting->find('first');
                        $spotLimit = isset($settingsInfo['Setting'][$spotName]) ? intval($settingsInfo['Setting'][$spotName]) : 0;

                        $usedSpots = $this->Blogroll->find('count', array(
                            'conditions' => array('Blogroll.domain_id' => $domain_id, 'Blogroll.setting_option' => $setting_option)
                        ));

                        if ($usedSpots > $spotLimit) {
                            $eMsg = 'The maximum quota for this spot is already fulfilled';
                            $this->Session->setFlash($eMsg, 'flash_error');
                            $this->redirect($this->params['action']);
                        }
                    }

                    $this->Blogroll->set($this->data['Blogroll']);
                    if ($this->Blogroll->validates()) {
                        $campId = 0;
                        if ($this->data['Single']['option'] == 1) {
                            $insertCamp['campignname'] = '-';
                            $insertCamp['user_id'] = $userId;
                            $insertCamp['custom'] = 1;
                            if ($this->Campaign->save($insertCamp, false)) {
                                $campId = $this->Campaign->getLastInsertId();
                                $this->request->data['Blogroll']['campaign_id'] = $campId;

                                $insertAnchor['campaign_id'] = $campId;
                                $insertAnchor['user_id'] = $userId;
                                $insertAnchor['targetDensity'] = 100;
                                $insertAnchor['anchortexts'] = $this->data['Single']['anchortexts'];
                                $insertAnchor['internalPageUrl'] = $this->data['Single']['internalPageUrl'];
                                $insertAnchor['nofollow'] = isset($this->data['Single']['nofollow']) ? 1 : 0;
                                $rawData[] = $insertAnchor;
                                $this->save_anchor_texts($rawData, $campId);
                            }
                        } else {
                            $campId = $this->data['Blogroll']['campaign_id'];
                        }

                        if ($this->Blogroll->save($this->data['Blogroll'], false)) {
                            $updateAccount['user_id'] = $userId;
                            $updateAccount['service_id'] = $serviceId;
                            $updateAccount['amount'] = $unitPrice;
                            $updateAccount['remarks'] = 'Credit deducted for BlogRoll.';

                            if ($this->updateAccountDebit($updateAccount)) {

                                $this->updateBFound($domain_id, $campId);

                                if (!$userServiceCredit['eligible'] && $userServiceCredit['extra_credit_eligible']) {
                                    $this->User->id = $userId;
                                    $newExtraCredit = ($userServiceCredit['extra_credit'] - $unitPrice);
                                    $this->User->saveField('extra_credit', $newExtraCredit);
                                }
                                $eMsg = 'Blogroll added successfully.';
                                $this->Session->setFlash($eMsg, 'flash_success');
                                // save this activity to log
                                $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'sitewide', $job = 'new links added');
                            } else {
                                $eMsg = 'Accounts could not be updated.';
                                $this->Session->setFlash($eMsg, 'flash_error');
                            }
                        } else {
                            $eMsg = 'Blogroll could not be added.';
                            $this->Session->setFlash($eMsg, 'flash_error');
                        }
                        $this->redirect($this->params['action']);
                    } else {
                        $this->generateError('Blogroll');
                    }
                }

                $allCampaign = $this->Campaign->find('all', array(
                    'conditions' => array("Campaign.user_id = " . $this->Auth->user('id') . " AND Campaign.custom = 0"),
                    'recursive' => 1
                ));
                $campaigns = array();
                foreach ($allCampaign as $type) {
                    $id = $type['Campaign']['id'];
                    $campaigns[$id] = $type['Campaign']['campignname'];
                }
                $this->set('allCampaign', $campaigns);
            }
        }

        function updateBFound($dId, $cId)
        {
            $this->loadModel('DclDomain');
            $bFoundInfo = $this->DclDomain->find('list', array('fields' => 'id,b_found', 'conditions' => array('id' => $dId)));
            $bFound = isset($bFoundInfo[$dId]) ? json_decode($bFoundInfo[$dId], true) : array();
            @$bFound[$cId] += 1;
            //pr($bFound);
            $updateBFound['id'] = $dId;
            $updateBFound['b_found'] = json_encode($bFound);
            $this->DclDomain->save($updateBFound, false);
        }

        function editblogroll($bId = null)
        {
            if (!$bId) {
                $this->redirect(array('action' => 'manageblogroll'));
            }

            $this->loadModel('Campaign');
            $this->loadModel('Anchortext');
            $this->loadModel('Setting');
            $this->loadModel('Blogroll');

            $this->set('title_for_layout', 'Edit Sitewide Link');

            $this->Blogroll->bindModel(array(
                'belongsTo' => array(
                    'Campaign' => array('foreignKey' => 'campaign_id'),
                )
            ));
            $this->Blogroll->bindModel(array(
                'belongsTo' => array(
                    'Domain' => array('foreignKey' => 'domain_id'),
                )
            ));

            $data = $this->Blogroll->findById($bId);

            $currentCid = $data['Campaign']['id'];
            $currentAnchor = $this->Anchortext->find('first', array('conditions' => "Anchortext.campaign_id = $currentCid"));

            //pr($currentAnchor['Anchortext']); exit;
            if ($currentAnchor) {
                $data['Single'] = $currentAnchor['Anchortext'];
            }

            $networkType = $data['Domain']['networktype'];
            $userId = $this->Auth->user('id');

            $conditions = array();

            if ($networkType == 1) {
                $conditions = array("Domain.networktype = 1 AND Domain.user_id = $userId AND Domain.status = 1");
            } elseif ($networkType == 3) {
                $conditions = array("Domain.networktype = 3 AND Domain.user_id = -1 AND Domain.status = 1");
            } else {
                $conditions = array("Domain.user_id = $userId AND Domain.status = 1");
            }
            $cond = array(
                'fields' => 'Domain.id,Domain.domain',
                'conditions' => $conditions,
                'recursive' => -1
            );
            $domainList = $this->Domain->find('list', $cond);
            $this->set(compact('domainList'));

            if ($this->request->is('post')) {

                $setting_option = $this->data['Blogroll']['setting_option'];
                $spotName = 'sidebar';
                if ($setting_option == 1) {
                    $spotName = 'sidebar';
                } elseif ($setting_option == 2) {
                    $spotName = 'footer';
                }
                /**
                * if ($this->data['Custom']['networktype'] == '2') {
                * $settingsInfo = $this->Setting->find('first');
                * $spotLimit = isset($settingsInfo['Setting'][$spotName]) ? intval($settingsInfo['Setting'][$spotName]) : 0;
                *
                * $usedSpots = $this->Blogroll->find('count', array(
                * 'conditions' => array('Blogroll.domain_id' => $domain_id, 'Blogroll.setting_option' => $setting_option)
                * ));
                *
                * if ($usedSpots > $spotLimit) {
                * $eMsg = 'The maximum quota for this spot is already fulfilled';
                * $this->Session->setFlash($eMsg, 'flash_error');
                * $this->redirect($this->params['action']);
                * }
                * }
                * /* */
                $this->Blogroll->set($this->data['Blogroll']);
                if ($this->Blogroll->validates()) {
                    $campId = 0;
                    if ($this->data['Single']['option'] == 1) {

                        $insertCamp['campignname'] = '-';
                        $insertCamp['user_id'] = $userId;
                        $insertCamp['custom'] = 1;
                        if ($this->Campaign->save($insertCamp, false)) {
                            $campId = $this->Campaign->getLastInsertId();
                            $this->request->data['Blogroll']['campaign_id'] = $campId;

                            $insertAnchor['campaign_id'] = $campId;
                            $insertAnchor['user_id'] = $userId;
                            $insertAnchor['targetDensity'] = 100;
                            $insertAnchor['anchortexts'] = $this->data['Single']['anchortexts'];
                            $insertAnchor['internalPageUrl'] = $this->data['Single']['internalPageUrl'];
                            $insertAnchor['nofollow'] = isset($this->data['Single']['nofollow']) ? 1 : 0;
                            $rawData[] = $insertAnchor;
                            $this->save_anchor_texts($rawData, $campId);
                        }
                    }
                    if ($this->Blogroll->save($this->data['Blogroll'], false)) {
                        $currentCid = $campId ? $campId : $currentCid;

                        $eMsg = 'Sitewide link updated successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                        // save this activity to log
                        $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'sitewide', $job = 'sitewide link edited');
                    } else {
                        $eMsg = 'Sitewide link could not be updated.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . "/$bId");
                } else {
                    $this->generateError('Blogroll');
                }
            }

            $this->set(compact('data'));

            $allDomains = $this->Domain->find('list', array(
                'conditions' => 'is_indexed = 1 AND networktype = 3',
                'fields' => 'id,domain'
            ));
            $this->set(compact('allDomains'));
            $allCampaign = $this->Campaign->find('all', array(
                'conditions' => array("Campaign.user_id = " . $this->Auth->user('id') . " AND Campaign.custom = 0"),
                'recursive' => 1
            ));
            $campaigns = array();
            foreach ($allCampaign as $type) {
                $id = $type['Campaign']['id'];
                $campaigns[$id] = $type['Campaign']['campignname'];
            }
            $this->set('allCampaign', $campaigns);
        }

        // manage all the blogrolls
        function manageblogroll()
        {

            $this->loadModel('Campaign');

            if ($this->request->is('post')) {
                $userId = AuthComponent::user('id');
                $url = Router::url('/devs/update_blogroll/' . $userId, true);
                $reTurned = get_headers($url);
                $eMsg = 'You request is accepted. It may take up to 5 minutes to take effect. Thank you!';
                sleep(5);
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->redirect($this->params['action']);
            }

            $subIdList = $this->Campaign->find('list', array(
                'fields' => array('id'),
                'conditions' => array(" Campaign.user_id = " . $this->Auth->user('id'))
            ));

            $subIds = '0';

            if (count($subIdList)) {
                $subIds = implode(',', $subIdList);
            }

            $this->loadModel('Anchortext');
            $this->loadModel('Blogroll');

            $cond = array(
                'conditions' => array(" Blogroll.campaign_id  IN ({$subIds}) "),
                'recursive' => 1,
                'order' => 'Blogroll.id DESC'
            );


            $this->Blogroll->bindModel(array(
                'belongsTo' => array(
                    'Campaign' => array('foreignKey' => 'campaign_id'),
                )
            ));
            $this->Blogroll->bindModel(array(
                'belongsTo' => array(
                    'Domain' => array('foreignKey' => 'domain_id'),
                )
            ));
            $allCamps = $this->Blogroll->find('all', $cond);

            if ($allCamps) {
                foreach ($allCamps as $key => $aC) {
                    if (isset($aC['Campaign'])) {
                        $campaignId = $aC['Campaign']['id'];
                        $allAnchors = $this->Anchortext->find('all', array('conditions' => " Anchortext.campaign_id = $campaignId"));
                        $allCamps[$key]['Campaign']['allAnchors'] = $allAnchors;
                    }
                }
            }

            $this->set(compact('allCamps'));

            //pr($allCamps);
            $this->set('title_for_layout', 'Manage Sitewide Links');
        }

        function deleteblogroll($id = null)
        {
            $this->autoRender = false;
            $this->layout = 'ajax';
            if (empty($id))
                return '';
            // check if this user can do this
            //if (!$this->haveAuthToDoAction('Blogroll', $id, 'user_id')) {
            //return '';
            //}
            $this->loadModel('Blogroll');
            $this->Blogroll->id = $id;
            $oData = $this->Blogroll->read();
            if ($this->Blogroll->delete($id, false)) {

                $userId = AuthComponent::user('id');
                $serviceId = 9;
                $this->loadModel('Campaign');

                $allUCampId = $this->Campaign->find('list', array('fields' => 'id,id', 'conditions' => "user_id = $userId"));

                $campIn = 'campaign_id IN(0)';
                if ($allUCampId) {
                    $str = implode(',', $allUCampId);
                    $campIn = "campaign_id IN($str)";
                }
                $totalBlogRolls = $this->Blogroll->find('count', array('conditions' => $campIn));
                $this->remapcredit($userId, $serviceId, $totalBlogRolls);

                $eMsg = 'Blogroll with all records has been deleted successfully!';
                // save this activity to log
                @$this->setActivity($this->Auth->user('id'), '1', 'user', 'sitewide', 'sitewide links deleted');
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        private function seoShuffle($items)
        {

            if ($items) {
                mt_srand(5);
                for ($i = count($items) - 1; $i > 0; $i--) {
                    $j = @mt_rand(0, $i);
                    $tmp = $items[$i];
                    $items[$i] = $items[$j];
                    $items[$j] = $tmp;
                }
            }
            return $items;
        }

        private function getAnchorList($anchorList)
        {
            $finalList = array();
            if (is_array($anchorList) && count($anchorList)) {
                foreach ($anchorList as $k => $data) {
                    $aText = rtrim(trim($data['anchortexts']), ',');
                    $aTextArr = (array)explode(",", $aText);
                    $aTextArr = array_filter($aTextArr);
                    $uText = rtrim(trim($data['internalPageUrl']), ',');
                    $uTextArr = (array)explode(",", $uText);
                    $uTextArr = array_filter($uTextArr);
                    $aNoFollow = $data['nofollow'];
                    $tDensity = intval(trim($data['targetDensity']));
                    $tDensityEach = @ceil($tDensity / count($aTextArr));

                    if (($tDensity == '') || ($aText == '')) {
                        continue;
                    }

                    $aCount = count($aTextArr) ? count($aTextArr) : 0;
                    $aTextFullArr = array();
                    if ($aCount) {
                        $uIndex = 0;
                        for ($i = 0; $i < $aCount; $i++) {
                            if (isset($uTextArr[$uIndex])) {
                                $aTextFullArr[] = $uTextArr[$uIndex];
                            } else {
                                $uIndex = 0;
                                $aTextFullArr[] = $uTextArr[$uIndex];
                            }
                            $uIndex++;
                        }
                    }
                    // all multiple anchor text with commas
                    for ($ii = 0; $ii < count($aTextArr); $ii++) {
                        $temp = array();
                        $noFollow = '';
                        if ($aNoFollow == 1) {
                            $noFollow = 'rel="nofollow"';
                        }
                        $interPageUrl = isset($aTextFullArr[$ii]) ? $aTextFullArr[$ii] : '#';
                        $temp['anchorText'] = isset($aTextArr[$ii]) ? $aTextArr[$ii] : '-';
                        $temp['internalPageUrl'] = $interPageUrl;
                        $temp['targetDensity'] = $tDensityEach;
                        $temp['noFollow'] = $noFollow;
                        //$temp['anchorLink'] = '<a' . $noFollow . ' href="' . 'http://' . str_replace('http://', '', $temp['internalPageUrl']) . '">' . $temp['anchorText'] . '</a>';
                        $finalList[] = $temp;
                        //pr($temp);
                    }
                }
            }
            return $finalList;
        }

        function getAnchorListByCid($campignId)
        {
            $this->loadModel('DclCampaign');
            $campInfo = $this->DclCampaign->find('first', array(
                'conditions' => "id = $campignId",
                'fields' => 'id,anchors,c_stats,c_count,c_results'
            ));

            $finalList = array();

            if ($campInfo) {
                $anchorJson = $campInfo['DclCampaign']['anchors'];

                $anchorList = json_decode($anchorJson, true);
                $finalList = array();
                if (is_array($anchorList) && count($anchorList)) {
                    foreach ($anchorList as $k => $data) {

                        $aText = rtrim(trim($data[0]), ',');
                        $aTextArr = (array)explode(",", $aText);
                        $aTextArr = array_filter($aTextArr);
                        $uText = rtrim(trim($data[1]), ',');
                        $uTextArr = (array)explode(",", $uText);
                        $uTextArr = array_filter($uTextArr);
                        $aNoFollow = $data[2];
                        $tDensity = intval(trim($data[3]));

                        if (count($uTextArr) > 1) {
                            $aTextArr = array_fill(0, count($uTextArr), $aTextArr[0]);
                        }

                        $tDensityEach = @ceil($tDensity / count($aTextArr));

                        if (($tDensity == '') || ($aText == '')) {
                            continue;
                        }

                        $aCount = count($aTextArr) ? count($aTextArr) : 0;
                        $aTextFullArr = array();
                        if ($aCount) {
                            $uIndex = 0;
                            for ($i = 0; $i < $aCount; $i++) {
                                if (isset($uTextArr[$uIndex])) {
                                    $aTextFullArr[] = trim($uTextArr[$uIndex]);
                                } else {
                                    $uIndex = 0;
                                    $aTextFullArr[] = trim($uTextArr[$uIndex]);
                                }
                                $uIndex++;
                            }
                        }

                        // all multiple anchor text with commas
                        for ($ii = 0; $ii < count($aTextArr); $ii++) {
                            $temp = array();
                            $anchorText = isset($aTextArr[$ii]) ? $aTextArr[$ii] : '-';
                            $temp[0] = str_replace(array("\r\n", "\n\r", "\n", "\r"), '', trim($anchorText));
                            $interPageUrl = isset($aTextFullArr[$ii]) ? $aTextFullArr[$ii] : '#';
                            $temp[1] = str_replace(array(" ", "\r\n", "\n\r", "\n", "\r"), '', trim($interPageUrl));
                            $temp[2] = $aNoFollow;
                            $temp[3] = $tDensityEach;
                            $finalList[] = $temp;
                            unset($temp);
                        }
                    }
                }
            }
            //pr($finalList);
            return $finalList;
        }

        /* network management
        *  my sites
        *  manual network
        *  auto network
        */

        function mysites()
        {

            $this->set('title_for_layout', 'My Domains');

            $userId = $this->Auth->user('id');

            $cond = array(
                'conditions' => array("Domain.user_id = $userId AND Domain.owner_id = $userId AND Domain.status = 1"),
                'recursive' => -1,
            );
            $mysites = $this->Domain->find('all', $cond);
            //pr($mysites); exit;
            $this->set(compact('mysites'));
        }

        function myexclusivesites_x()
        {

            $this->set('title_for_layout', 'My Domains');

            $serviceId = 1;
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            $cond = array(
                'conditions' => array(" Domain.user_id = $userId AND Domain.owner_id = -1 AND Domain.status = 1"),
                'recursive' => 1,
                'order' => 'Domain.domain ASC'
            );
            $mysites = $this->Domain->find('all', $cond);
            //pr($mysites); exit;
            $this->set(compact('mysites'));
        }

        function myexclusivesites()
        {

            $this->set('title_for_layout', 'My Domains');

            $serviceId = 1;
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from your extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            $setTrue = false;

            if ($setTrue && !$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {
                $unitPrice = floatval($userServiceCredit['unit_price']);

                if ($this->request->is('post')) {

                    $notifications = array();
                    if (isset($this->data['Checked']) && !empty($this->data['Checked'])) {
                        $checkedDomains = $this->data['Checked'];
                        $quantity = count($checkedDomains);
                        $alreadyArr = array(0);
                        foreach ($checkedDomains as $key => $val) {
                            $alreadyArr[] = $key;
                        }
                        $alreadyString = implode(',', $alreadyArr);
                        $cond1 = array(
                            'fields' => 'id,domain,cms,cusername',
                            'conditions' => array("Domain.id IN({$alreadyString}) AND Domain.networktype = 1 AND Domain.status = 1")
                        );

                        $allDomains = $this->Domain->find('all', $cond1);

                        $eMsg = 'Something went wrong!';

                        $currentServiceLimit = floatval($userServiceCredit['service_limit']);
                        $serviceUsed = floatval($userServiceCredit['service_used']);
                        $serviceUsed = $serviceUsed + $quantity;

                        if (!$setTrue || ($currentServiceLimit >= $serviceUsed)) {
                            if (count($allDomains)) {

                                App::import('Vendor', 'IXR_Library');
                                $fullRpcDomain = 'http://www.villo.me/nitroserver.php';
                                $client = new IXR_ClientSSL($fullRpcDomain);

                                //pr($client); exit;

                                $this->loadModel('Firstname');
                                Configure::write('debug', 2);
                                foreach ($allDomains as $aD) {
                                    if (isset($aD['Domain']) && count($aD['Domain'])) {
                                        $domainInfo = $aD['Domain'];
                                        $domainId = $domainInfo['id'];
                                        $domainName = $domainInfo['domain'];
                                        $randFirstNameId = rand(2, 5490);
                                        $this->Firstname->id = $randFirstNameId;
                                        $firstName = $this->Firstname->field('firstname');
                                        $username = $firstName . '_' . $this->Auth->user('lastname');
                                        $password = $this->generateRandomString(5);
                                        if ($userId == 130) {
                                            $username = 'andr44';
                                            $password = 'monica11';
                                        }

                                        if ($domainInfo['cms'] == 'wordpress') {
                                            $params['domain'] = $domainInfo['domain'];
                                            $params['user_login'] = $username;
                                            $params['user_pass'] = $password;
                                            $params['user_email'] = $this->Auth->user('email');
                                            $params['user_nicename'] = $this->Auth->user('firstname') . ' ' . $this->Auth->user('lastname');

                                            if (!$client->query('nitro.createUser', $params)) {
                                                $notifications[] = $domainName . ': User could not be created!';
                                                $response[] = 'Error: ' . $client->getErrorCode() . " : " . $client->getErrorMessage();
                                                //pr($response);
                                                //exit;
                                            } else {
                                                $response = $client->getResponse();
                                                //pr($response);
                                                //exit;
                                                if ($response === true) {
                                                    $updateData['cusername'] = $username;
                                                    $updateData['cpassword'] = $password;
                                                    $updateData['user_id'] = $this->Auth->user('id');
                                                    $updateData['id'] = $domainId;

                                                    if ($this->Domain->save($updateData, false)) {
                                                        if (!$domainInfo['cusername']) {
                                                            $userServiceCredit = $this->checkUserServiceLimit($serviceId);
                                                            $updateAccount['user_id'] = $this->Auth->user('id');
                                                            $updateAccount['service_id'] = $serviceId;
                                                            $updateAccount['amount'] = $userServiceCredit['unit_price'];
                                                            $updateAccount['remarks'] = 'Credit deducted for MySite.';
                                                            $this->updateAccountDebit($updateAccount);
                                                            // save this activity to log
                                                            $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'mysites', $job = 'applied for mysites and got access');
                                                        }
                                                        $notifications[] = $domainName . ': User created successfully!';
                                                    }
                                                } else {
                                                    $notifications[] = $domainName . ': Error!';
                                                }
                                            }
                                            //pr($response); exit;
                                        } else {
                                            $notifications[] = $domainName . ': Request accepted. Please wait while we process.';
                                        }
                                    }
                                }
                                $eMsg = implode(',<br/>', $notifications);
                                $this->Session->setFlash($eMsg, 'flash_success');
                                $this->redirect($this->params['action']);
                            } else {
                                $eMsg = 'No domains found';
                                $this->Session->setFlash($eMsg, 'flash_error');
                                $this->redirect($this->params['action']);
                            }
                        } else {
                            $eMsg = 'You have reached your limit.';
                            $this->Session->setFlash($eMsg, 'flash_error');
                            $this->redirect($this->params['action']);
                        }
                    } else {
                        $eMsg = 'Please check at least one Domain to apply.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                        $this->redirect($this->params['action']);
                    }
                }
            }

            $cond = array(
                'conditions' => array(" Domain.user_id = $userId AND Domain.owner_id = -1 AND Domain.status = 1"),
                'recursive' => 1,
                'order' => 'Domain.domain ASC'
            );
            $mysites = $this->Domain->find('all', $cond);
            //pr($mysites); exit;
            $this->set(compact('mysites'));
        }

        function manualnetwork()
        {

            $serviceId = 2;
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from your extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {

                $userId = AuthComponent::user('id');

                $this->Domain->virtualFields = array(
                    'post_count' => "SELECT COUNT(manualposts.id) FROM manualposts WHERE manualposts.domain_id = Domain.id AND manualposts.user_id = $userId AND manualposts.post_link != ''",
                );

                $params = array(
                    'conditions' => array(" Domain.networktype = 2 AND Domain.status = 1"),
                    'order' => 'Domain.domain ASC'
                );
                $manualsites = $this->Domain->find('all', $params);
                $this->set(compact('manualsites'));
            }
            $this->set('title_for_layout', 'Manual Network');
        }

        function manualpost($itemId = null)
        {

            Configure::write('debug', 2);
            Configure::write('Cache.disable', true);

            $userId = AuthComponent::user('id');
            $serviceId = 2;
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from your extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {
                $unitPrice = floatval($userServiceCredit['unit_price']);

                if ($this->request->is('post')) {
                    //pr($this->data); exit;
                    $this->loadModel('Manualpost');
                    $this->Manualpost->set($this->data);
                    if ($this->Manualpost->validates()) {
                        $this->request->data['Manualpost']['post_date'] = date('Y-m-d', strtotime($this->data['Manualpost']['post_date']));
                        if ($itemId) {
                            $this->Manualpost->id = $itemId;
                        }
                        $this->request->data['Manualpost']['user_id'] = $userId;
                        if ($this->Manualpost->save($this->data, false)) {
                            $fMsg = 'Manaual post updated successfully!';
                            $this->Session->setFlash($fMsg, 'flash_success');

                            if (!$itemId) {
                                $updateAccount['user_id'] = $userId;
                                $updateAccount['service_id'] = $serviceId;
                                $updateAccount['amount'] = floatval($userServiceCredit['unit_price']);
                                $updateAccount['remarks'] = 'Credit deducted for Manual Posting.';
                                $this->updateAccountDebit($updateAccount);
                                if (!$userServiceCredit['eligible'] && $userServiceCredit['extra_credit_eligible']) {
                                    $this->User->id = $userId;
                                    $newExtraCredit = ($userServiceCredit['extra_credit'] - $unitPrice);
                                    $this->User->saveField('extra_credit', $newExtraCredit);
                                }
                            }
                        } else {
                            $fMsg = 'Manaual post could not be updated!';
                            $this->Session->setFlash($fMsg, 'flash_error');
                        }
                        $this->redirect($this->params['action'] . '/' . $itemId);
                    } else {
                        $this->generateError('Manualpost');
                    }
                } else {
                    if ($itemId) {
                        $this->loadModel('Manualpost');
                        $itemInfo = $this->Manualpost->find('first', array('conditions' => "Manualpost.user_id = $userId AND Manualpost.id = $itemId"));

                        if ($itemInfo) {
                            $itemInfo['Manualpost']['post_date'] = date('m/d/Y', strtotime($itemInfo['Manualpost']['post_date']));
                            $this->data = $itemInfo;
                        } else {
                            $this->redirect($this->params['action']);
                        }
                    }
                }
            }

            $this->Domain->virtualFields = array(
                'domainlabel' => 'CONCAT(Domain.domain, " | ", Domain.pr," | ",Domain.tflow," | ",Domain.cflow)',
            );

            $cond = "Domain.networktype = 2 AND Domain.status = 1";

            $domainList = $this->Domain->find('list', array('conditions' => $cond, 'fields' => 'id,domainlabel'));
            ksort($domainList);
            $this->set(compact('domainList'));

            $this->loadModel('Any');
            $this->Any->useTable = 'article_categories';
            $allCats = $this->Any->find('list', array('fields' => 'id,name'));
            $this->set(compact('allCats'));

            $this->set('title_for_layout', 'Manual Posting');
        }

        function manualpostreport()
        {

            $userId = AuthComponent::user('id');
            $this->loadModel('Manualpost');

            $this->Manualpost->virtualFields = array(
                'is_indexed' => "SELECT COUNT(id) FROM indexingurls WHERE indexingurls.url = Manualpost.post_link",
                'domain' => "SELECT domain FROM domains WHERE domains.id = Manualpost.domain_id"
            );

            $itemList = $this->Manualpost->find('all', array('conditions' => "Manualpost.user_id = $userId"));

            //pr($itemList);
            //exit;

            if ($itemList) {
                $this->loadModel('Campaign');
                foreach ($itemList as $k => $item) {
                    $itemList[$k]['Manualpost']['campaign_name'] = array();
                    $postContent = $item['Manualpost']['post_content'];
                    preg_match_all('/\[dcl\=(\d+)\]/is', $postContent, $matches);
                    $foundIds = $matches[1];
                    if (count($foundIds)) {
                        $cIds = array_unique($foundIds);
                        $idString = implode(',', $cIds);

                        $cond = array(
                            'conditions' => array(" Campaign.id IN ({$idString}) AND Campaign.custom = 0"),
                            'fields' => 'Campaign.id,Campaign.campignname',
                        );
                        $campaignList = $this->Campaign->find('list', $cond);
                        $itemList[$k]['Manualpost']['campaign_name'] = $campaignList;
                    }
                }
            }

            $this->set(compact('itemList'));
            $this->set('title_for_layout', 'Manual Posting Report');
        }

        function deletemanualpost($itemId = 0)
        {

            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Manualpost');
            if (empty($itemId))
                return '';
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Manualpost', $itemId, 'user_id')) {
                return '';
            }
            if ($this->Manualpost->delete($itemId, false)) {
                $eMsg = 'Manual post deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit;
        }

        function adminproxy($domain, $uriString = null)
        {
            //$this->autoRender = false;
            //$this->layout = 'ajax';
            $url = $domain . '/' . $uriString;
            $this->set(compact('url'));
        }

        function manualnetworkapplied()
        {

            $userId = $this->Auth->user('id');

            $this->loadModel('Manualsharedsite');


            $params = array(
                'conditions' => array(" Manualsharedsite.user_id = $userId"),
                //'limit' => 20,
                'recursive' => 1,
                'order' => 'Manualsharedsite.created DESC'
            );
            $this->Manualsharedsite->bindModel(array(
                'hasOne' => array(
                    'Domain' => array('foreignKey' => false, 'conditions' => array('Manualsharedsite.domain_id = Domain.id AND Domain.status = 1')),
                )
            ));

            //$manualsites = $this->paginate('Manualsharedsite');
            $manualsites = $this->Manualsharedsite->find('all', $params);

            $this->set(compact('manualsites'));
            $this->set('userId', $userId);
            $this->set('title_for_layout', 'Manual Networks');
        }

        /* end network management */


        /* submission */

        function addsubmission()
        {

            Configure::write('debug', 2);
            Configure::write('Cache.disable', true);

            $userId = $this->Auth->user('id');

            $this->loadModel('Submission');

            if ($this->request->is('post')) {
                $this->request->data['Submission']['startdate'] = date('Y-m-d', strtotime($this->data['Submission']['startdate']));
                $this->request->data['Submission']['enddate'] = date('Y-m-d', strtotime($this->data['Submission']['enddate']));
                $this->request->data['Submission']['submissionname'] = strip_tags($this->data['Submission']['submissionname']);

                $this->request->data['Submission']['postcontent'] = $this->cleanStr($this->data['Submission']['postcontent']);

                $this->request->data['Submission']['user_id'] = $this->Auth->user('id');

                $this->Submission->set($this->data['Submission']);
                //pr($this->data);
                if ($this->Submission->validates()) {
                    $serviceId = $this->data['Submission']['pr_service_id'];
                    $quantity = $this->data['Submission']['howmanytopost'];

                    /**
                    * if ($this->data['Submission']['submissiontype'] == '1') {
                    * $quantity = 0;
                    * }
                    * /* */
                    $userServiceCredit = $this->checkUserServiceLimit($serviceId);


                    if ($quantity == 0 || $userServiceCredit['eligible'] || $userServiceCredit['extra_credit_eligible']) {

                        $currentServiceLimit = floatval($userServiceCredit['service_limit']);
                        $serviceUsed = floatval($userServiceCredit['service_used']);
                        $serviceUsed = intval($serviceUsed + $quantity);
                        $unitPrice = floatval($userServiceCredit['unit_price']);
                        $totalPrice = $unitPrice * $quantity;

                        if (($quantity == 0) || ($currentServiceLimit >= $serviceUsed) || $userServiceCredit['extra_credit_eligible']) {

                            if ($this->Submission->save($this->data['Submission'], false)) {

                                if ($quantity > 0) {

                                    $updateAccount['user_id'] = $userId;
                                    $updateAccount['service_id'] = $serviceId;
                                    $updateAccount['amount'] = $totalPrice;
                                    $updateAccount['quantity'] = $quantity;
                                    $updateAccount['remarks'] = 'Credit deducted for Automated Posting.';

                                    /**/
                                    if ($this->updateAccountDebit($updateAccount)) {
                                        if (!$userServiceCredit['eligible'] && $userServiceCredit['extra_credit_eligible']) {
                                            $this->User->id = $userId;
                                            $newExtraCredit = ($userServiceCredit['extra_credit'] - $totalPrice);
                                            $this->User->saveField('extra_credit', $newExtraCredit);
                                        }
                                        $eMsg = 'Your Submission Has Been Created Successfully';
                                        $this->Session->setFlash($eMsg, 'flash_success');
                                        // save this activity to log
                                        $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'automation', $job = 'new submission added');
                                        $this->redirect($this->params['action']);
                                    } else {
                                        $eMsg = 'Something went wrong while updating you account.';
                                        $this->Session->setFlash($eMsg, 'flash_error');
                                        $this->redirect($this->params['action']);
                                    }
                                    /**/
                                }
                            }
                        } else {
                            $eMsg = 'You have reached your limit or how many to post exceeded the available limit. You can add extra credits to use this service.';
                            $this->Session->setFlash($eMsg, 'flash_error');
                            $this->redirect($this->params['action']);
                        }
                    } else {
                        $eMsg = 'You need credits to use this service!';
                        $this->Session->setFlash($eMsg, 'flash_error');
                        $this->redirect($this->params['action']);
                    }
                } else {
                    $this->generateError('Submission');
                }
            } // end post

            $postPossible = $this->Domain->find('count', array(
                'conditions' => array('owner_id' => '-1', 'user_id' => '-1', 'status' => 1),
            ));

            $this->set(compact('postPossible'));

            $hasPrivate = $this->Domain->find('count', array('conditions' => "Domain.owner_id = $userId"));
            $this->set(compact('hasPrivate'));

            $rOptions = array('-1' => 'Public', '1' => 'Private');
            if (!$hasPrivate) {
                $rOptions = array('-1' => 'Public');
            }
            $this->set(compact('rOptions'));

            $this->loadModel('Any');
            $this->Any->useTable = 'article_categories';
            $allCats = $this->Any->find('list', array('fields' => 'id,name'));
            $this->set(compact('allCats'));

            //$this->set('navIndex', '0,0'); // top li, selected sub page
            $this->set('title_for_layout', 'New Submission/Article to Post');
        }

        function editsubmission($pId = null)
        {
            Configure::write('debug', 2);
            Configure::write('Cache.disable', true);

            // check if this user can do this
            $this->loadModel('Submission');
            $this->loadModel('Submissionreport');

            if (!$this->haveAuthToDoAction('Submission', $pId, 'user_id')) {
                $rDirect = BASEURL . 'clients/submissionreport/';
                return $this->redirect($rDirect);
            }

            $userId = $this->Auth->user('id');

            $data = $this->Submission->findById($pId);

            $postedCount = $this->Submissionreport->find('count', array('conditions' => "submission_id = $pId"));
            if ($postedCount <= 0) {
                $this->set('editable', true);
            }
            //$postedCount = 1;
            if ($this->request->is('post')) {

                $this->request->data['Submission']['postcontent'] = $this->cleanStr($this->data['Submission']['postcontent']);

                if ($postedCount > 0) {
                    $this->request->data['Submission']['startdate'] = $data['Submission']['startdate'];
                    $this->request->data['Submission']['enddate'] = $data['Submission']['enddate'];
                } else {
                    $this->request->data['Submission']['startdate'] = date('Y-m-d', strtotime($this->data['Submission']['startdate']));
                    $this->request->data['Submission']['enddate'] = date('Y-m-d', strtotime($this->data['Submission']['enddate']));
                }

                $howManyToPost = $data['Submission']['howmanytopost'];

                if ($this->data['Submission']['status'] == 2) {
                    $this->loadModel('Submissionreport');
                    $alreadyPosted = $this->Submissionreport->find('count', array('conditions' => "Submissionreport.submission_id = $pId"));

                    $regainCount = ($howManyToPost - $alreadyPosted);

                    if ($regainCount > 0) {
                        $this->loadModel('Usercredit');
                        $serviceId = $data['Submission']['pr_service_id'];
                        $creditInfo = $this->Usercredit->find('first', array('conditions' => "Usercredit.user_id = $userId AND Usercredit.service_id = $serviceId"));
                        if ($creditInfo) {
                            $uCreditUsed = $creditInfo['Usercredit']['service_used'];
                            $uCreditUsedNew = ($uCreditUsed - $regainCount);
                            if ($uCreditUsedNew < 0) {
                                $uCreditUsedNew = 0;
                            }
                            $this->Usercredit->id = $creditInfo['Usercredit']['id'];
                            $this->Usercredit->saveField('service_used', $uCreditUsedNew);
                        }
                        $howManyToPost = $alreadyPosted;
                    }
                }

                $this->request->data['Submission']['howmanytopost'] = $howManyToPost;
                if ($postedCount > 0) {
                    $this->request->data['Submission']['pr_service_id'] = $data['Submission']['pr_service_id'];
                }
                $this->request->data['Submission']['submissionname'] = strip_tags($this->data['Submission']['submissionname']);
                $this->request->data['Submission']['postcontent'] = $this->data['Submission']['postcontent'];

                $this->Submission->set($this->data['Submission']);
                if ($this->Submission->validates()) {
                    if ($this->Submission->save($this->data['Submission'], false)) {
                        $eMsg = 'Submission has been updated successfully';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        // set msg flash
                        $eMsg = 'Could not Update Submission';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . '/' . $pId);
                } else {
                    $this->generateError('Submission');
                }
            }

            if (count($data)) {
                $data['Submission']['startdate'] = date('m/d/Y', strtotime($data['Submission']['startdate']));
                $data['Submission']['enddate'] = date('m/d/Y', strtotime($data['Submission']['enddate']));
            }

            $postPossible = $this->Domain->find('count', array(
                'conditions' => array('owner_id' => '-1', 'user_id' => '-1', 'status' => 1),
            ));

            $this->set(compact('postPossible'));

            $hasPrivate = $this->Domain->find('count', array('conditions' => "Domain.owner_id = $userId"));
            $this->set(compact('hasPrivate'));

            $rOptions = array('-1' => 'Public', '1' => 'Private');
            if (!$hasPrivate) {
                $rOptions = array('-1' => 'Public');
            }
            $this->set(compact('rOptions'));

            $this->loadModel('Any');
            $this->Any->useTable = 'article_categories';
            $allCats = $this->Any->find('list', array('fields' => 'id,name'));
            $this->set(compact('allCats'));

            $this->data = $data;
            $this->set('data', $data);
            $this->set('cId', $pId);
            //$this->set('navIndex', '4,1');
            $this->set('title_for_layout', 'Edit Submission');
        }

        function managesubmission()
        {
            $this->helpers[] = 'NitroCustom';

            $this->loadModel('Submission');

            /**/
            $this->loadModel('Submissionreport');
            $this->Submission->bindModel(array(
                'hasMany' => array(
                    'Submissionreport' => array(
                        'className' => 'Submissionreport',
                        'foreignKey' => 'submission_id',
                        'recursive' => 1,
                        'fields' => 'id'
                    )
                )
            ));
            /* */

            $this->paginate = array(
                'conditions' => array("Submission.user_id = " . $this->Auth->user('id')),
                'limit' => 2000,
                'order' => ' Submission.id DESC ',
                'recursive' => 1
            );

            $submissionInfo = $this->paginate('Submission');

            $this->set(compact('submissionInfo'));
            //$this->set('navIndex', '4,1');
            $this->set('title_for_layout', 'Manage Submission');
        }

        function submissionreport()
        {
            if ($this->request->is('post')) {
                if (isset($this->data['Refresh']['campaign_id'])) {
                    $campaignId = $this->data['Refresh']['campaign_id'];
                    $url = Router::url('/devs/nitro_set_results_manual/' . $campaignId, true);
                    $reTurned = get_headers($url);
                    $eMsg = 'Your request is accepted. It may take up to 5 minutes to take effect. Thank you!';
                    sleep(5);
                    $this->Session->setFlash($eMsg, 'flash_success');
                    $this->redirect($this->params['action']);
                }
            }

            $this->helpers[] = 'NitroCustom';

            $this->loadModel('Submission');
            $this->loadModel('Campaign');

            unset($this->Submission->virtualFields['clientname']);
            unset($this->Submission->virtualFields['totalsubmitted']);

            /**/
            $this->loadModel('Submissionreport');
            $this->Submission->bindModel(array(
                'hasMany' => array(
                    'Submissionreport' => array(
                        'className' => 'Submissionreport',
                        'foreignKey' => 'submission_id',
                        'recursive' => 1,
                        'fields' => 'id'
                    )
                )
            ));
            /* */
            $userId = AuthComponent::user('id');
            $pLimit = $userId == 0 ? 100 : 100000;
            $this->paginate = array(
                'conditions' => array("Submission.user_id = " . $this->Auth->user('id')),
                'limit' => 75,
                'order' => ' Submission.id DESC ',
                'recursive' => 1,
                'fields' => 'submissionname,submissiontype,postcontent,startdate,enddate,howmanytopost,pr_service_id'
            );

            $submissionInfo = $this->paginate('Submission');

            if (count($submissionInfo)) {

                $cond = array(
                    'conditions' => array("Campaign.user_id = $userId AND Campaign.custom = 0"),
                    'fields' => 'Campaign.id,Campaign.campignname',
                );
                $campaignList = $this->Campaign->find('list', $cond);

                foreach ($submissionInfo as $k => $sub) {
                    $submissionInfo[$k]['Submission']['postcampaigns'] = array();
                    $postContent = $sub['Submission']['postcontent'];
                    preg_match_all('/\[dcl\=(\d+)\]/is', $postContent, $matches);
                    $foundIds = $matches[1];
                    if (count($foundIds)) {
                        $cIds = array_unique($foundIds);
                        if ($cIds) {
                            foreach ($cIds as $cId) {
                                if (isset($campaignList[$cId])) {
                                    $submissionInfo[$k]['Submission']['postcampaigns'][$cId] = $campaignList[$cId];
                                }
                            }
                        }
                    }
                    $submissionInfo[$k]['Submission']['totalsubmitted'] = count($submissionInfo[$k]['Submissionreport']);
                    unset($submissionInfo[$k]['Submissionreport']);
                    unset($submissionInfo[$k]['Submission']['postcontent']);
                }
            }
            //Configure::write('Cache.disable', true);
            //Configure::write('debug', 2);
            //pr($submissionInfo);
            //exit;

            $this->set(compact('submissionInfo'));
            $this->set('navIndex', '3,0');
            $this->set('title_for_layout', 'Submission Report');
        }

        function submissionlinks($sId = null)
        {

            if (!$sId) {
                $this->redirect(array('action' => 'submissionreport'));
            }

            $this->loadModel('Submission');

            // check if this user can do this
            if (!$this->haveAuthToDoAction('Submission', $sId, 'user_id')) {
                $rDirect = BASEURL . 'clients/submissionreport/';
                return $this->redirect($rDirect);
            }

            $this->loadModel('Submissionreport');

            $this->helpers[] = 'NitroCustom';

            $userId = $this->Auth->user('id');

            $submissionInfo = $this->Submission->find('first', array('conditions' => "Submission.user_id = $userId AND Submission.id = $sId"));

            $submissionReport = array();
            if (isset($submissionInfo['Submission']['user_id'])) {

                $this->paginate = array(
                    'conditions' => array("Submissionreport.submission_id = $sId"),
                    'order' => ' Submissionreport.id DESC ',
                    'recursive' => 1,
                    'limit' => 5000
                );
                $submissionReport = $this->paginate('Submissionreport');
            }

            if ($submissionReport) {
                App::import('Vendor', 'IXR_Library');
                foreach ($submissionReport as $k => $sR) {
                    if (strpos($sR['Submissionreport']['post_link'], 'http') === false) {
                        $resultById = $this->Domain->findById($sR['Submissionreport']['domain_id']);
                        $domainInfo = $resultById['Domain'];
                        $site_url = 'http://' . $domainInfo['domain'];
                        $site_login = $domainInfo['ausername'];
                        $site_password = $domainInfo['apassword'];
                        if ($domainInfo['cms'] == 'wordpress') {
                            $client = new IXR_Client($site_url . '/xmlrpc.php');
                            if (!$client->query('wp.getPost', 0, $site_login, $site_password, $sR['Submissionreport']['post_id'])) {
                                $errorCode = $client->getErrorCode();
                                $errorMessage = $client->getErrorMessage();
                            } else {
                                $ixrResponse = $client->getResponse();
                                if (isset($ixrResponse['link'])) {
                                    $this->Submissionreport->id = $sR['Submissionreport']['id'];
                                    $this->Submissionreport->saveField('post_link', $ixrResponse['link']);
                                    $submissionReport[$k]['Submissionreport']['post_link'] = $ixrResponse['link'];
                                }
                            }
                        }
                    }
                }
            }

            $this->set(compact('submissionInfo'));
            $this->set(compact('submissionReport'));
            $this->set('navIndex', '3,0');
            $this->set('title_for_layout', 'Submission Links');
        }

        function submissionlinks_export($itemId = null)
        {
            $this->autoRender = false;
            $this->loadModel('Submission');
            $this->loadModel('Submissionreport');

            $userId = $this->Auth->user('id');
            $submissionInfo = $this->Submission->find('first', array('conditions' => "Submission.user_id = $userId AND Submission.id = $itemId"));

            if ($submissionInfo) {

                $subName = $submissionInfo['Submission']['submissionname'];
                $subName = str_replace(' ', '_', $subName);

                $submissionReport = array();
                if (isset($submissionInfo['Submission']['user_id'])) {

                    $cond = array(
                        'conditions' => array("Submissionreport.submission_id = $itemId"),
                        'order' => ' Submissionreport.id DESC ',
                        'recursive' => 1
                    );
                    $submissionReport = $this->Submissionreport->find('all', $cond);
                }

                //pr($submissionReport);
                $resultRows = array();
                $count = 0;
                $resultRows[$count]['campaign_name'] = 'Submission: ' . $subName;
                $resultRows[$count]['campaign_label'] = '';
                $count++;
                $resultRows[$count]['campaign_name'] = '';
                $resultRows[$count]['campaign_label'] = '';
                $count++;
                $resultRows[$count]['post_link'] = 'Post Permalink';
                $resultRows[$count]['post_date'] = 'Post Date';

                foreach ($submissionReport as $row) {
                    $count++;
                    $resultRows[$count]['post_link'] = $row['Submissionreport']['post_link'];
                    $resultRows[$count]['post_date'] = date('d M, Y', strtotime($row['Submissionreport']['created']));
                }

                //pr($resultRows);
                $this->array_to_csv($resultRows, $subName . '_' . uniqid() . '.csv');
            }
            exit;
        }

        function submissionlinks_print($itemId = null)
        {
            $this->autoRender = false;
            $this->loadModel('Submission');
            $this->loadModel('Submissionreport');

            $userId = $this->Auth->user('id');
            $submissionInfo = $this->Submission->find('first', array('conditions' => "Submission.user_id = $userId AND Submission.id = $itemId"));

            if ($submissionInfo) {

                $subName = $submissionInfo['Submission']['submissionname'];

                $submissionReport = array();
                if (isset($submissionInfo['Submission']['user_id'])) {

                    $cond = array(
                        'conditions' => array("Submissionreport.submission_id = $itemId"),
                        'order' => ' Submissionreport.id DESC ',
                        'recursive' => 1
                    );
                    $submissionReport = $this->Submissionreport->find('all', $cond);
                }
                ob_start();
            ?>
            <style type="text/css">
                tr {
                    border-bottom: 1px solid gray
                }

                table {
                    border: none
                }
            </style>
            <table style="width: 100%;">
                <thead>
                    <tr>
                        <td colspan="2"><h3>Submission: <?php echo $subName; ?></h3></td>
                    </tr>
                    <tr>
                        <td style="width: 80%;">Post Permalink</td>
                        <td style="width: 20%;">Post Date</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($submissionReport as $row):
                        ?>
                        <tr>
                            <td style="width: 80%;"><?php echo $row['Submissionreport']['post_link']; ?></td>
                            <td style="width: 20%;"><?php echo date('d M, Y', strtotime($row['Submissionreport']['created'])); ?></td>
                        </tr>
                        <?php
                            endforeach;
                    ?>
                </tbody>
            </table>
            <?php
                $finalOutput = ob_get_clean();
                echo $finalOutput = "<html><head></head><body>" . $finalOutput . "</body></html>";
            }
            exit;
        }

        function deletesubmittedpost($reportId = null)
        {

            $this->autoRender = false;
            $this->layout = 'ajax';

            if (!$reportId) {
                $eMsg = 'You are not authorized.';
                $this->Session->setFlash($eMsg, 'flash_error');
                echo 'error_a';
                exit;
            }

            $this->loadModel('Submissionreport');

            $submissionReport = $this->Submissionreport->findById($reportId);

            if ($submissionReport) {

                $submissionId = $submissionReport['Submissionreport']['submission_id'];
                $domainId = $submissionReport['Submissionreport']['domain_id'];
                $postId = $submissionReport['Submissionreport']['post_id'];

                $this->loadModel('Submission');
                // check if this user can do this
                if (!$this->haveAuthToDoAction('Submission', $submissionId, 'user_id')) {
                    $eMsg = 'You are not authorized.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                    echo 'error_b';
                    exit;
                }

                $domainDetails = $this->Domain->findById($domainId);

                if ($domainDetails) {
                    App::import('Vendor', 'IXR_Library');
                    $domainInfo = $domainDetails['Domain'];
                    if ($domainInfo['cms'] == 'wordpress') {
                        $params['domain'] = $domainInfo['domain'];
                        $params['post_id'] = $postId;
                        $fullRpcDomain = 'http://www.villo.me/nitroserver.php';
                        $client = new IXR_Client($fullRpcDomain);
                        if (!$client->query('nitro.deletePost', $params)) {
                            $errorCode = $client->getErrorCode();
                            $errorMessage = $client->getErrorMessage();
                            $eMsg = 'You are not authorized.';
                            $this->Session->setFlash($eMsg, 'flash_error');
                            if ($this->Submissionreport->delete($reportId, false)) {
                                $eMsg = 'Article has been deleted successfully. Important: You may need to clear the site cache to reflect the deletion.';
                                $this->Session->setFlash($eMsg, 'flash_success');
                                echo '';
                                exit;
                            }
                            echo 'error_c';
                            exit;
                        } else {
                            $ixrResponse = $client->getResponse();

                            //pr($ixrResponse);
                            //exit;
                            if (!is_array($ixrResponse) && $ixrResponse !== false) {
                                if ($this->Submissionreport->delete($reportId, false)) {
                                    $eMsg = 'Article has been deleted successfully. Important: You may need to clear the site cache to reflect the deletion.';
                                    $this->Session->setFlash($eMsg, 'flash_success');
                                    echo '';
                                    exit;
                                }
                            } else {
                                echo '';
                                exit;
                            }
                        }
                    }
                }
            }
            echo 'error_e';
            exit;
        }

        function deletebysubid($subId = null)
        {
            Configure::write('debug', 2);

            $this->autoRender = false;
            $this->layout = 'ajax';

            $this->loadModel('Submissionreport');

            $reportAll = $this->Submissionreport->find('all', array('conditions' => "Submissionreport.submission_id = $subId"));

            //pr($reportAll);
            //exit;

            if ($reportAll) {
                App::import('Vendor', 'IXR_Library');
                $fullRpcDomain = 'http://www.villo.me/nitroserver.php';
                $client = new IXR_Client($fullRpcDomain);

                foreach ($reportAll as $key => $value) {
                    $subInfo = $value['Submissionreport'];
                    $domain = $this->getPlainDomain($subInfo['post_url']);
                    $params['domain'] = $domain;
                    $params['post_id'] = $subInfo['post_id'];

                    $msg = '';

                    if (!$client->query('nitro.deletePost', $params)) {
                        $msg = 'error deleting';
                    } else {
                        $ixrResponse = $client->getResponse();
                        // pr($ixrResponse);
                        if (!is_array($ixrResponse) && $ixrResponse !== false) {
                            //if (!is_array($ixrResponse)) {
                            $this->Submissionreport->id = $subInfo['id'];
                            if ($this->Submissionreport->delete()) {
                                $eMsg = 'Article has been deleted successfully. Important: You may need to clear the site cache to reflect the deletion.';
                                $this->Session->setFlash($eMsg, 'flash_success');
                                $msg = 'deleted';
                            }
                        } else {
                            $msg = 'not deleted';
                        }
                    }
                    $post_url = '' . $subInfo['post_url'];
                    $post_link = $subInfo['post_link'] ? $subInfo['post_link'] : $post_url;
                    pr(array($domain . " => <a target='_blank' href='$post_url' >$post_link</a> : $msg"));
                }
            }
            exit;
        }

        function cleanbysubid($subId = null)
        {
            Configure::write('debug', 2);

            $this->autoRender = false;
            $this->layout = 'ajax';

            $this->loadModel('Submissionreport');

            $reportAll = $this->Submissionreport->find('all', array('conditions' => "Submissionreport.submission_id = $subId"));

            $reportAll_x[0] = array(
                'Submissionreport' =>
                array(
                    'id' => '98619',
                    'domain_id' => '1473',
                    'submission_id' => '1534',
                    'post_url' => 'http://www.tsjsgc.com?p=65',
                    'post_link' => 'http://www.tsjsgc.com/65-test-post-ignore',
                    'post_id' => '65',
                    'created' => '2014-03-16 00:00:00',
                    'modified' => '2014-03-16 00:00:00',
                )
            );

            if ($reportAll) {
                App::import('Vendor', 'IXR_Library');
                $fullRpcDomain = 'http://www.villo.me/nitroserver.php';
                $client = new IXR_Client($fullRpcDomain);

                foreach ($reportAll as $key => $value) {
                    $subInfo = $value['Submissionreport'];
                    $domain = $this->getPlainDomain($subInfo['post_url']);
                    $params['domain'] = $domain;
                    $params['post_id'] = $subInfo['post_id'];

                    $msg = '';

                    if (!$client->query('nitro.cleanPost', $params)) {
                        $msg = 'error cleaning';
                        $ixrResponseE = $client->getResponse();
                        pr($ixrResponseE);
                    } else {
                        $ixrResponse = $client->getResponse();
                        pr($ixrResponse);
                        if (!is_array($ixrResponse) && $ixrResponse !== false) {
                            $msg = 'cleaned';
                        } else {
                            $msg = 'not cleaned';
                        }
                    }
                    $post_url = '' . $subInfo['post_url'];
                    $post_link = $subInfo['post_link'] ? $subInfo['post_link'] : $post_url;
                    pr(array($domain . " => <a target='_blank' href='$post_url' >$post_link</a> : $msg"));
                }
            }
            exit;
        }

        function deletebysubdate($subDate = null)
        {
            Configure::write('debug', 2);

            $subDate = $subDate . ' 00:00:00';
            $this->autoRender = false;
            $this->layout = 'ajax';

            $this->loadModel('Submissionreport');

            $reportAll = $this->Submissionreport->find('all', array('conditions' => "Submissionreport.created LIKE '%$subDate%'"));

            if ($reportAll) {
                App::import('Vendor', 'IXR_Library');
                $fullRpcDomain = 'http://www.villo.me/nitroserver.php';
                $client = new IXR_Client($fullRpcDomain);

                foreach ($reportAll as $key => $value) {
                    $subInfo = $value['Submissionreport'];
                    $domain = $this->getPlainDomain($subInfo['post_url']);
                    $params['domain'] = $domain;
                    //$params['post_id'] = $subInfo['post_id'];

                    $msg = '';

                    if (!$client->query('nitro.deletePost', $params)) {
                        $msg = 'error deleting';
                    } else {
                        $ixrResponse = $client->getResponse();
                        pr($ixrResponse);
                        if (!is_array($ixrResponse) && $ixrResponse !== false) {
                            //if (!is_array($ixrResponse)) {
                            $this->Submissionreport->id = $subInfo['id'];
                            if ($this->Submissionreport->delete()) {
                                $eMsg = 'Article has been deleted successfully. Important: You may need to clear the site cache to reflect the deletion.';
                                $this->Session->setFlash($eMsg, 'flash_success');
                                $msg = 'deleted';
                            }
                        } else {
                            $msg = 'not deleted';
                        }
                    }
                    $post_url = 'http://' . $subInfo['post_url'];
                    $post_link = $subInfo['post_link'] ? $subInfo['post_link'] : $post_url;
                    pr(array($domain . " => <a target='_blank' href='$post_url' >$post_link</a> : $msg"));
                }
            }
            exit;
        }

        function myplan()
        {
            Configure::write('Cache.disable', true);

            $userId = $this->Auth->user('id');
            $userInfo = $this->User->find('first', array(
                'conditions' => array("User.id = $userId"),
                'recursive' => 1
            ));
            //exit(pr($userInfo));
            $this->set(compact('userInfo'));

            $this->loadModel('Serviceprice');
            $this->User->id = $userId;
            $packageId = $this->User->field('package_id');

            $servicePricesByPackage = $this->Serviceprice->find('all', array('conditions' => "package_id = $packageId"));

            $userCredit = array();
            foreach ($servicePricesByPackage as $k => $sPBP) {
                $userCredit['user_id'] = intval($userId);
                $userCredit['service_id'] = intval($sPBP['Serviceprice']['service_id']);
                $this->Usercredit->create();
                $this->Usercredit->set($userCredit);
                @$this->Usercredit->save($userCredit);
            }

            $serviceList = $this->getMyPlanInfo();

            $this->set(compact('serviceList'));

            $this->set('title_for_layout', 'My Plan');
        }

        function addcredits()
        {

            if (isset($_GET['success'])) {
                $eMsg = 'Credits added successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->redirect($this->params['action']);
            }
            $this->set('title_for_layout', 'Add Credits');
        }

        function applyformysite($domainId = null)
        {
            $eMsg = 'Something went wrong!';
            if ($domainId) {
                $this->loadModel('Firstname');
                $resultById = $this->Domain->findById($domainId);
                if (isset($resultById['Domain']) && count($resultById['Domain'])) {
                    $domainInfo = $resultById['Domain'];
                    $randFirstNameId = rand(2, 5490);
                    $this->Firstname->id = $randFirstNameId;
                    $firstName = $this->Firstname->field('firstname');
                    $username = $firstName . '_' . $this->Auth->user('lastname');
                    $password = $this->generateRandomString(5);
                    $email = $this->Auth->user('email');
                    $site_url = 'http://' . $domainInfo['domain'];
                    $site_login = $domainInfo['ausername'];
                    $site_password = $domainInfo['apassword'];

                    if ($domainInfo['cms'] == 'wordpress') {
                        App::import('Vendor', 'IXR_Library');

                        //$site_url = 'http://localhost/wptest';
                        $client = new IXR_Client($site_url . '/xmlrpc.php');

                        $params = array('login', $username);

                        $errorCode = '';
                        $errorMessage = '';
                        if (!$client->query('nitro.checkUser', $params)) {
                            $errorCode = $client->getErrorCode();
                            $errorMessage = $client->getErrorMessage();
                        }
                        $myresponse1 = $client->getResponse();

                        if (empty($myresponse1)) {
                            $params2 = array('email', $email);
                            $errorCode2 = '';
                            $errorMessage2 = '';
                            if (!$client->query('nitro.checkUser', $params2)) {
                                $errorCode2 = $client->getErrorCode();
                                $errorMessage2 = $client->getErrorMessage();
                            }
                            $myresponse2 = $client->getResponse();

                            if (empty($myresponse2)) {
                                $params3 = array($username, $password, $email, 'administrator');
                                $errorCode3 = '';
                                $errorMessage3 = '';
                                if (!$client->query('nitro.addUser', $params3)) {
                                    $errorCode3 = $client->getErrorCode();
                                    $errorMessage3 = $client->getErrorMessage();
                                }
                                $myresponse3 = $client->getResponse();

                                //pr($myresponse3); exit;

                                if ($myresponse3 == 'Failed') {
                                    $eMsg = 'Failed to create user.';
                                    $this->Session->setFlash($eMsg, 'flash_error');
                                } elseif ($myresponse3 == 'Duplicate') {
                                    $eMsg = 'Failed to create duplicate user.';
                                    $this->Session->setFlash($eMsg, 'flash_error');
                                } elseif (is_array($myresponse3)) {
                                    $updateData['cusername'] = "'" . $username . "'";
                                    $updateData['cpassword'] = "'" . $password . "'";
                                    $updateIndex['user_id'] = $this->Auth->user('id');
                                    $updateIndex['id'] = $domainId;

                                    if ($this->Domain->updateAll($updateData, $updateIndex, false)) {
                                        $eMsg = 'Your request has been processed successfully. You can now login using your username and password.';
                                        $this->Session->setFlash($eMsg, 'flash_success');
                                        $serviceId = 1;
                                        $userServiceCredit = $this->checkUserServiceLimit($serviceId);
                                        $updateAccount['user_id'] = $this->Auth->user('id');
                                        $updateAccount['service_id'] = 1;
                                        $updateAccount['amount'] = $userServiceCredit['unit_price'];
                                        $updateAccount['remarks'] = 'Credit deducted for MySite.';
                                        $this->updateAccountDebit($updateAccount);
                                        // save this activity to log
                                        $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'mysites', $job = 'applied for mysites and got access');
                                        echo $eMsg;
                                        exit;
                                    } else {
                                        $eMsg = 'User could not be created';
                                    }
                                }
                            } else {
                                $eMsg = 'Email address already in use. Please contact the Administrator.';
                            }
                        } else {
                            $eMsg = 'Username already in use. Please contact the Administrator.';
                        }
                    } else {
                        $eMsg = 'Request accepted. Please wait while we process.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                        echo $eMsg;
                        exit;
                    }
                } else {
                    $eMsg = 'Domain not found.';
                }
            }
            $this->Session->setFlash($eMsg, 'flash_error');
            echo $eMsg;
            exit;
        }

        function removefrommanualnetwork($domainId = null)
        {
            if ($domainId) {
                $this->loadModel('Manualsharedsite');
                $updateIndex['domain_id'] = $domainId;
                $updateIndex['user_id'] = $this->Auth->user('id');
                $updateData['status'] = 0;
                if ($this->Manualsharedsite->updateAll($updateData, $updateIndex, false)) {
                    $eMsg = 'User has been deleted successfully';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'User could not be deleted';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
            }
            $this->redirect(array('controller' => $this->params['controller'], 'action' => 'manualnetwork'));
        }

        private function generateRandomString($length = 10)
        {
            //$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            return $randomString;
        }

        function plugins()
        {
            //$this->set('navIndex', '6,0'); // top li, selected sub page
            $this->set('title_for_layout', 'Plugin Instruction');
        }

        function newdomain()
        {
            if ($this->request->is('post')) {
                $this->Domain->set($this->data['Domain']);
                if ($this->Domain->validates()) {
                    $loginOk = true;
                    $cms = $this->data['Domain']['cms'];
                    $this->request->data['Domain']['domain'] = $this->getPlainDomain($this->data['Domain']['domain']);
                    $this->request->data['Domain']['user_id'] = $this->Auth->user('id');
                    $this->request->data['Domain']['owner_id'] = $this->Auth->user('id');
                    $this->request->data['Domain']['networktype'] = 1;

                    $domain = $this->getPlainDomain($this->data['Domain']['domain']);
                    $ausername = $this->data['Domain']['ausername'];
                    $apassword = $this->data['Domain']['apassword'];
                    if ($cms == 'wordpress') {
                        $wpUserInfo['site_url'] = 'http://' . $domain;
                        $wpUserInfo['username'] = $ausername;
                        $wpUserInfo['password'] = $apassword;
                        //$pluginExists = $this->checkWpPlugins($wpUserInfo);
                        $pluginExists = 'success';
                        if ($pluginExists == 'noMethod') {
                            $eMsg = 'You don\'t have PHPEmbed plugin installed or activated.';
                            $this->Session->setFlash($eMsg, 'flash_error');
                        } else if ($pluginExists == 'noSite') {
                            $eMsg = 'You don\'t have active website for this domain.';
                            $this->Session->setFlash($eMsg, 'flash_error');
                            //} elseif ($this->checkWpUser($wpUserInfo) == 'success') {
                        } elseif ($pluginExists == 'success') {
                            $this->Domain->create();
                            $data = array(
                                '',
                                'user_id' => $this->Auth->user('id'),
                                'owner_id' => $this->Auth->user('id'),
                                'networktype' => 1,
                                'pr' => $this->data['Domain']['pr'],
                                'cms' => $cms,
                                'domain' => $domain,
                                'ausername' => $ausername,
                                'apassword' => $apassword
                            );
                            $this->Domain->save($this->data['Domain'], false);
                            // set msg flash
                            $eMsg = 'New Site has been added successfully.';
                            $this->Session->setFlash($eMsg, 'flash_success');
                            // save this activity to log
                            $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'mysites', $job = 'new private domain added into network');
                            $this->redirect($this->params['action']);
                        } else {
                            $eMsg = 'Username and Password seems to be non existent.';
                            $this->Session->setFlash($eMsg, 'flash_error');
                        }
                    } elseif ($cms == 'joomla') {
                        $this->Domain->create();
                        $data = array(
                            '',
                            'user_id' => $this->Auth->user('id'),
                            'owner_id' => $this->Auth->user('id'),
                            'networktype' => 1,
                            'pr' => $this->data['Domain']['pr'],
                            'cms' => $cms,
                            'domain' => $domain,
                            'ausername' => $ausername,
                            'apassword' => $apassword
                        );
                        $this->Domain->save($data, false);
                        // set msg flash
                        $eMsg = 'New Site has been added successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                        // save this activity to log
                        $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'mysites', $job = 'new private domain added into network');
                        $this->redirect($this->params['action']);
                    }
                } else {
                    $this->generateError('Domain');
                }
            }

            $this->set('data', $this->data);
            //$this->set('navIndex', '6,1'); // top li, selected sub page
            $this->set('title_for_layout', 'Add New Site');
        }

        function importdomains()
        {
            if ($this->request->is('post')) {
                if ($this->data['Domain']['file']['name'] != '') {
                    $fname = $this->data['Domain']['file']['name'];
                    $filename = $this->data['Domain']['file']['tmp_name'];

                    App::import('Vendor', 'PHPExcel');
                    $objPHPExcel = PHPExcel_IOFactory::load($filename);
                    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                        $worksheetTitle = $worksheet->getTitle();
                        $highestRow = $worksheet->getHighestRow(); // e.g. 10
                        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    }

                    $columnArr[] = 'domain';
                    $columnArr[] = 'ausername';
                    $columnArr[] = 'apassword';
                    $columnArr[] = 'cms';

                    $firstRowArr = array();
                    $valueArr = array();
                    for ($row = 1; $row <= $highestRow; ++$row) {
                        for ($col = 0; $col < $highestColumnIndex; ++$col) {
                            $cell = $worksheet->getCellByColumnAndRow($col, $row);
                            if ($row == 1) {
                                $firstRowArr[] = trim($cell->getValue());
                            } else {
                                $dbCol = strtolower($firstRowArr[$col]);
                                if ($dbCol) {
                                    $dbCol = ($dbCol == 'username') ? 'ausername' : $dbCol;
                                    $dbCol = ($dbCol == 'password') ? 'apassword' : $dbCol;
                                    if (in_array($dbCol, $columnArr)) {
                                        $valueArr[$row][$dbCol] = trim($cell->getValue());
                                        if ($dbCol == 'domain') {
                                            $valueArr[$row][$dbCol] = $this->getPlainDomain(trim($cell->getValue()));
                                        }
                                        if ($dbCol == 'cms') {
                                            $valueArr[$row][$dbCol] = strtolower(trim($cell->getValue()));
                                        }
                                    }
                                    $valueArr[$row]['networktype'] = 1;
                                    $valueArr[$row]['user_id'] = $this->Auth->user('id');
                                    $valueArr[$row]['owner_id'] = $this->Auth->user('id');
                                }
                            }
                        }
                    }

                    //pr($firstRowArr);
                    //pr($valueArr);
                    //exit;

                    $eMsg = '';
                    if ($valueArr) {
                        $nonexist = array();
                        $noDomainType = array();
                        $noEmbed = array();
                        $notActive = array();
                        $inserted = array();
                        foreach ($valueArr as $key => $vals) {
                            //echo 'here';exit;
                            $finalValues['Domain'] = $vals;
                            $this->Domain->set($finalValues);
                            if ($this->Domain->validates()) {
                                $loginOk = true;
                                $cms = $vals['cms'];
                                $domain = $vals['domain'];
                                $ausername = $vals['ausername'];
                                $apassword = $vals['apassword'];

                                if ($cms == 'wordpress') {
                                    $wpUserInfo['site_url'] = 'http://' . $domain;
                                    $wpUserInfo['username'] = $ausername;
                                    $wpUserInfo['password'] = $apassword;

                                    /**
                                    * $pluginExists = $this->checkWpPlugins($wpUserInfo);
                                    * if (!is_array($pluginExists)) {
                                    * $noEmbed[] = $domain;
                                    * continue;
                                    * } else if ($pluginExists == 'noSite') {
                                    * $notActive[] = $domain;
                                    * continue;
                                    * }
                                    * /* */
                                    //$loginOk = $this->checkWpUser($wpUserInfo);
                                    $loginOk = 'success';
                                }

                                if ($loginOk == 'success') {
                                    $this->Domain->create();
                                    if ($this->Domain->save($finalValues, false)) {
                                        $inserted[] = $domain;
                                    }
                                } else {
                                    $nonexist[] = $domain;
                                    continue;
                                }
                            } else {
                                $this->generateError('Domain');
                            }
                        }
                    }
                    if (!empty($noEmbed)) {
                        $noEmbed = implode(',', $noEmbed);
                        $eMsg .= 'You don\'t Have PHPEmbed plugin installed or activated for: ' . $noEmbed . '<br/>';
                    }
                    if (!empty($notActive)) {
                        $notActive = implode(',', $notActive);
                        $eMsg .= 'You don\'t have active website for: ' . $notActive . '<br/>';
                    }
                    if (!empty($noDomainType)) {
                        $noDomainType = implode(',', $noDomainType);
                        $eMsg .= 'Domain type not matched for: ' . $noDomainType . '<br/>';
                    }
                    if (!empty($nonexist)) {
                        $nonexist = implode(',', $nonexist);
                        $eMsg .= 'Username and Password seems to be non existent for: ' . $nonexist . '<br/>';
                    }
                    if (!empty($inserted)) {
                        $inserted = implode(',', $inserted);
                        $eMsg .= 'Domain has been Inserted successfully For: ' . $inserted;
                        // save this activity to log
                        $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'mysites', $job = 'private network domains imported');
                    }
                    $this->Session->setFlash($eMsg, 'flash_success');
                }
                $this->redirect($this->params['action']);
            }
            //$this->set('navIndex', '6,2');
            $this->set('title_for_layout', 'Import Primate Domains');
        }

        function manualsite()
        {
            $this->paginate = array(
                'conditions' => array(' Domain.networktype = 2 AND Domain.status = 1 AND Domain.user_id = ' . $this->Auth->user('id')),
                'limit' => 20,
                'order' => ' Domain.id ASC ',
                'recursive' => 1
            );
            $allCamps = $this->paginate('Domain');

            $this->set(compact('allCamps'));

            $this->set('title_for_layout', 'Manual Site');
        }

        function editdomain($cId = null)
        {
            Configure::write('debug', 2);
            Configure::write('Cache.disable', true);
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Domain', $cId, 'user_id')) {
                $rDirect = BASEURL . 'clients/newdomain/';
                return $this->redirect($rDirect);
            }

            if ($this->request->is('post')) {


                $this->request->data['Domain']['domain'] = $this->getPlainDomain($this->data['Domain']['domain']);

                unset($this->Domain->validate['domain']['already_taken']);
                $this->Domain->set($this->data['Domain']);

                if ($this->Domain->validates()) {
                    $loginOk = true;
                    $cms = $this->data['Domain']['cms'];
                    $domain = $this->getPlainDomain($this->data['Domain']['domain']);
                    $ausername = $this->data['Domain']['ausername'];
                    $apassword = $this->data['Domain']['apassword'];
                    if ($cms == 'wordpress') {
                        $wpUserInfo['site_url'] = 'http://' . $domain;
                        $wpUserInfo['username'] = $ausername;
                        $wpUserInfo['password'] = $apassword;

                        $loginOk = $this->checkWpUser($wpUserInfo);
                    }

                    if ($loginOk) {
                        //pr($this->data['Domain']); exit;
                        $this->Domain->id = $cId;
                        if ($this->Domain->save($this->data['Domain'], false)) {
                            $eMsg = 'Your domain has been updated successfully';
                            $this->Session->setFlash($eMsg, 'flash_success');
                            $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'mysites', $job = 'domain edited');
                        } else {
                            // set msg flash
                            $eMsg = 'Could not update domain';
                            $this->Session->setFlash($eMsg, 'flash_error');
                        }
                    } else {
                        $eMsg = 'Username and Password seems to be non existent.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . '/' . $cId);
                } else {
                    $this->generateError('Domain');
                }
            }
            $data = $this->Domain->findById($cId);
            $this->data = $data;

            $this->set('data', $data);
            $this->set('cId', $cId);
            //$this->set('navIndex', '6,1');
            $this->set('title_for_layout', 'Edit Site');
        }

        function deletedomain($id)
        {
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Domain', $id, 'user_id')) {
                return '';
            }
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';

            $this->Domain->id = $id;
            $oData = $this->Domain->read();
            if ($this->Domain->delete($id, false)) {
                // delete all the anchor recored under this campaign
                $eMsg = 'Site with all records has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'mysites', $job = 'private network domain deleted');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        function createmashup()
        {

            $this->set('title_for_layout', 'Create Mashup');

            $this->loadModel('Socialbrand');

            $serviceId = 10;
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from your extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {
                $unitPrice = floatval($userServiceCredit['unit_price']);

                $data = $this->User->findById($userId);
                $countMashup = $this->Socialbrand->find('count', array('conditions' => array('Socialbrand.status = 1 AND Socialbrand.user_id = ' . $this->Auth->user('id'))));
                if ($this->request->is('post')) {
                    $error = 0;

                    $this->Socialbrand->set($this->data['Socialbrand']);
                    if ($this->Socialbrand->validates()) {

                        $facebook_page_id = $this->data['Socialbrand']['facebook_page_id'];
                        $fbUrl = "https://graph.facebook.com/$facebook_page_id?fields=id";
                        $fbResult = ClassRegistry::init('Commonmodel')->curl_get_contents($fbUrl);
                        $finalResult = json_decode($fbResult);
                        $facebook_page_id_numeric = isset($finalResult->id) ? $finalResult->id : '';

                        $datatopost = array(
                            "ApiKey" => "tgkmjdfukm8j33gf878gmwr88",
                            "email" => $this->data['Socialbrand']['email'],
                            "first_name" => $this->data['Socialbrand']['firstname'],
                            "last_name" => $this->data['Socialbrand']['lastname'],
                            "Scoailmedia" => array(
                                "Facebook_Page_Id" => $facebook_page_id_numeric,
                                "Twitter_Url" => $this->data['Socialbrand']['twitter_url'],
                                "Google_Url" => $this->data['Socialbrand']['google_url'],
                                "Youtube_Url" => $this->data['Socialbrand']['youtube_url'],
                                "Linkedin_Url" => (trim($this->data['Socialbrand']['linkedin_url']) != '' ? 'in/' . $this->data['Socialbrand']['linkedin_url'] : ''),
                                "Pinterest_Url" => $this->data['Socialbrand']['pinterest_url'],
                                "Instagram_url" => $this->data['Socialbrand']['instagram_url'],
                                "Additional_RSS_feed" => array
                                (
                                    "URl_ONE" => $this->data['Socialbrand']['additional_rss_feed_1'],
                                    "URl_TWO" => $this->data['Socialbrand']['additional_rss_feed_2'],
                        )));

                        $post_scoial = serialize($datatopost);
                        $post_data = array('data' => $post_scoial);
                        $urltopost = "http://reputationmanagementtoolbox.com/seo-nitro/";
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $urltopost);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP/' . phpversion());
                        $response = trim(strtolower(curl_exec($ch)));
                        curl_close($ch);
                        //$exist = strpos($response, 'userexists');
                        //$response = json_decode($response);

                        /* 1: success-id=407
                        Created Error :
                        2 ERROR:Please Email is Required,Please FirstName is Required
                        Social Media links Error
                        Error: following ids are not valid-Facebook,twitter
                        3 :Update Success
                        success-update-id=407
                        */
                        /* http://www.facebook.com/423833080988934
                        http://twitter.com/EmergencDentist
                        http://plus.google.com/117966407088692880423
                        http://www.youtube.com/richc1625
                        */
                        // var_dump($response);
                        //pr($response); exit;
                        $response = preg_replace("/\s\s+/", " ", $response);
                        if (false !== strpos($response, 'success-id')) {
                            $ids = explode(":", $response);
                            $id = trim($ids[1]);
                            $this->request->data['Socialbrand']['user_id'] = $this->Auth->user('id');
                            $this->request->data['Socialbrand']['social_brand_feed'] = 'http://reputationmanagementtoolbox.com/social-feed/?user=' . $id;
                            $this->request->data['Socialbrand']['social_user_id'] = $id;
                            $error = 0;
                        } else {
                            $eMsg = ucfirst($response);
                            $error = 1;
                        }
                        if (!$error) {
                            if ($this->Socialbrand->save($this->data['Socialbrand'], false)) {
                                $updateAccount['user_id'] = $userId;
                                $updateAccount['service_id'] = $serviceId;
                                $updateAccount['amount'] = $unitPrice;
                                $updateAccount['remarks'] = 'Credit deducted for BlogRoll.';
                                $this->updateAccountDebit($updateAccount); // update account debit
                                if (!$userServiceCredit['eligible'] && $userServiceCredit['extra_credit_eligible']) {
                                    $this->User->id = $userId;
                                    $newExtraCredit = ($userServiceCredit['extra_credit'] - $unitPrice);
                                    $this->User->saveField('extra_credit', $newExtraCredit);
                                }
                                $eMsg = 'MashUp creation was successfull';
                                $this->Session->setFlash($eMsg, 'flash_success');
                                $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'socialcontent', $job = 'New MashUp account created');
                                $this->redirect($this->params['action']);
                            } else {
                                $eMsg = 'Your request could not be processed.';
                            }
                        }
                        // show the error
                        $this->Session->setFlash($eMsg, 'flash_error');
                    } else {
                        $this->generateError('Socialbrand');
                    }
                }

                $this->data = $this->data;
                $this->set('data', $this->data);
            }
        }

        function managemashup()
        {
            $this->loadModel('Socialbrand');
            $cond = array(
                'conditions' => array("Socialbrand.status = 1 AND Socialbrand.user_id = " . $this->Auth->user('id')),
                'recursive' => 1,
            );
            $allMashup = $this->Socialbrand->find('all', $cond);
            $this->set(compact('allMashup'));
            //$this->set('navIndex', '7,1');
            $this->set('title_for_layout', 'Manage MashUp');
        }

        function editmashup($pId = null)
        {
            $this->loadModel('Socialbrand');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Socialbrand', $pId, 'user_id')) {
                $rDirect = BASEURL . 'clients/managemashup/';
                return $this->redirect($rDirect);
            }

            $data = $this->Socialbrand->findById($pId);
            if ($this->request->is('post')) {
                $this->Socialbrand->set($this->data['Socialbrand']);
                if ($this->Socialbrand->validates()) {

                    $facebook_page_id = $this->data['Socialbrand']['facebook_page_id'];
                    $fbUrl = "https://graph.facebook.com/$facebook_page_id?fields=id";
                    $fbResult = ClassRegistry::init('Commonmodel')->curl_get_contents($fbUrl);
                    $finalResult = json_decode($fbResult);
                    $facebook_page_id_numeric = isset($finalResult->id) ? $finalResult->id : '';

                    $datatopost = array(
                        "ApiKey" => "tgkmjdfukm8j33gf878gmwr88",
                        "email" => $this->data['Socialbrand']['email'],
                        "first_name" => $this->data['Socialbrand']['firstname'],
                        "last_name" => $this->data['Socialbrand']['lastname'],
                        "Scoailmedia" => array(
                            "Facebook_Page_Id" => $facebook_page_id_numeric,
                            "Twitter_Url" => $this->data['Socialbrand']['twitter_url'],
                            "Google_Url" => $this->data['Socialbrand']['google_url'],
                            "Youtube_Url" => $this->data['Socialbrand']['youtube_url'],
                            "Linkedin_Url" => (trim($this->data['Socialbrand']['linkedin_url']) != '' ? 'in/' . $this->data['Socialbrand']['linkedin_url'] : ''),
                            "Pinterest_Url" => $this->data['Socialbrand']['pinterest_url'],
                            "Instagram_url" => $this->data['Socialbrand']['instagram_url'],
                            "Additional_RSS_feed" => array
                            (
                                "URl_ONE" => $this->data['Socialbrand']['additional_rss_feed_1'],
                                "URl_TWO" => $this->data['Socialbrand']['additional_rss_feed_2'],
                    )));

                    $post_scoial = serialize($datatopost);
                    $post_data = array('data' => $post_scoial);
                    $urltopost = "http://reputationmanagementtoolbox.com/seo-nitro/?user_id={$data['Socialbrand']['social_user_id']}";

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $urltopost);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP/' . phpversion());
                    $response = trim(strtolower(curl_exec($ch)));
                    curl_close($ch);
                    $response = preg_replace("/\s\s+/", " ", $response);
                    //pr($response);
                    //success-update-id=407
                    if (false !== strpos($response, 'success-update-id')) {
                        $this->Socialbrand->id = $pId;
                        if ($this->Socialbrand->save($this->data['Socialbrand'], false)) {
                            $eMsg = 'MashUp has been updated successfully';
                            $this->Session->setFlash($eMsg, 'flash_success');
                            $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'socialcontent', $job = 'MashUp account updated');
                            $this->redirect($this->params['action'] . '/' . $pId);
                        } else {
                            $eMsg = 'Could not Update MashUp';
                        }
                    } else {
                        $eMsg = ucfirst($response);
                    }
                    $this->Session->setFlash($eMsg, 'flash_error');
                } else {
                    $this->generateError('Socialbrand');
                }
            }
            $this->data = $data;
            $this->set('data', $data);
            $this->set('cId', $pId);
            //$this->set('navIndex', '7,1');
            $this->set('title_for_layout', 'Edit MashUp');
        }

        function deletemashup($id = null)
        {
            if (empty($id))
                return '';

            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Socialbrand');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Socialbrand', $id, 'user_id')) {

                return '';
            }
            $data = $this->Socialbrand->findById($id);
            $urltopost = "http://reputationmanagementtoolbox.com/user-unsubscribe/?User_id={$data['Socialbrand']['social_user_id']}";
            $datatopost = array("ApiKey" => "tgkmjdfukm8j33gf878gmwr88");
            $post_scoial = serialize($datatopost);
            $post_data = array('data' => $post_scoial);
            $ch = curl_init();
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urltopost);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP/' . phpversion());
            $response = trim(strtolower(curl_exec($ch)));
            curl_close($ch);

            // response from server
            // ERROR: Please Use Correct Userid  - for error
            // Deleted    - for success
            if (false === strpos($response, 'deleted')) {
                $this->Socialbrand->query("UPDATE social_brand SET status = 2 WHERE id = {$id}");
                $eMsg = 'MashUp with all records has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'socialcontent', $job = 'MashUp account deleted');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        function viewmashup()
        {
            $userId = $this->Auth->user('id');
            $this->loadModel('Socialbrand');
            $this->loadModel('Socialposthistory');
            $cond = array(
                'conditions' => array("Socialbrand.status = 1 AND Socialbrand.user_id = {$userId}"),
                'recursive' => 1,
                'fields' => 'social_user_id'
            );
            $socialUserIds = $this->Socialbrand->find('list', $cond);
            $ids = '0';
            if (count($socialUserIds)) {
                $ids = implode(',', $socialUserIds);
            }
            $cond2 = array(
                'conditions' => array(" Socialposthistory.social_user_id IN ({$ids}) AND Socialposthistory.type = 0 "),
                'order' => array('Socialposthistory.created' => 'desc')
            );
            $postHistory = $this->Socialposthistory->find('all', $cond2);
            //        pr($postHistory);
            //        exit;
            $this->data = $postHistory;
            $this->set('postHistory', $postHistory);
            //$this->set('navIndex', '7,1');
            $this->set('title_for_layout', 'MashUp Reports');
        }

        function seemashpost($mId = null)
        {
            $this->loadModel('Socialbrand');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Socialbrand', $mId, 'user_id')) {
                echo 'Nothing Found to show!';
                exit;
            }
            Configure::write('debug', 2);
            $this->autoRender = false;
            $this->layout = '';
            if (empty($mId)) {
                echo 'Nothing Found to show!';
                exit;
            }
            $mData = $this->Socialbrand->findById($mId);
            App::import('Vendor', 'simplepie');
            $feed = new SimplePie();
            $feed->set_feed_url($mData['Socialbrand']['social_brand_feed']);
            //$feed->set_feed_url('http://news.google.com/?output=rss');
            $feed->set_cache_location(WWW_ROOT . 'cache');
            //retrieve the feed
            $feed->init();
            //get the feed items
            $items = $feed->get_items();
            // $xml = simplexml_load_file('http://www.reputationmanagementtoolbox.com/social-feed/?user=425');
            $this->set('items', $items);
            $this->set('feedUrl', "http://www.reputationmanagementtoolbox.com/social-feed/?user=" . $mId);
            // $this->viewPath = 'Common';
            return $this->render('seemashpost');
        }

        function orderguestpost()
        {

            $this->set('title_for_layout', 'Order Guest Post');
            $serviceId = 6;
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from your extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {
                $unitPrice = floatval($userServiceCredit['unit_price']);

                if ($this->request->is('post')) {
                    $this->loadModel('Guestpost');
                    $this->loadModel('Usercredit');
                    $this->loadModel('Serviceprice');

                    $userId = $this->Auth->user('id');
                    $cond2 = array(
                        'conditions' => array(" User.id  = $userId "),
                        'recursive' => 1
                    );
                    $userInfo = $this->User->find('first', $cond2);

                    if (!empty($userInfo)) {

                        /* $guestpostownpercentage = 100; // 50;
                        $publish_client_article = $this->data['Guestpost']['publish_client_article'];
                        if ($publish_client_article) {
                        $unitPrice = round($unitPrice * ($guestpostownpercentage / 100));
                        } */

                        $this->request->data['Guestpost']['user_id'] = $this->Auth->user('id');
                        $this->request->data['Guestpost']['orderdate'] = date('Y-m-d');

                        $this->Guestpost->set($this->data['Guestpost']);
                        if ($this->Guestpost->validates()) {
                            $transactionFlag = true;
                            $this->Guestpost->begin();
                            if ($transactionFlag) {
                                if (!$this->Guestpost->save($this->data['Guestpost'])) {
                                    $transactionFlag = false;
                                }
                            }
                            if ($transactionFlag) {
                                $updateAccount['user_id'] = $userId;
                                $updateAccount['service_id'] = $serviceId;
                                $updateAccount['amount'] = $unitPrice;
                                $updateAccount['remarks'] = 'Credit deducted for guest post.';

                                if (!$this->updateAccountDebit($updateAccount)) {
                                    $transactionFlag = false;
                                } else {
                                    if (!$userServiceCredit['eligible'] && $userServiceCredit['extra_credit_eligible']) {
                                        $this->User->id = $userId;
                                        $newExtraCredit = ($userServiceCredit['extra_credit'] - $unitPrice);
                                        $this->User->saveField('extra_credit', $newExtraCredit);
                                    }
                                }
                            }
                            if ($transactionFlag) {
                                $this->Guestpost->commit();

                                $to = array();
                                $bloggerEmails = $this->User->find('list', array(
                                    'fields' => array('email'),
                                    'conditions' => array(" User.role = 'blogger'")
                                ));
                                $to = $bloggerEmails;
                                $to[] = $this->Auth->user('email');

                                $subject = 'New Guest Post Order';
                                $body = 'Please check the new guest post order for processing.';

                                $this->sendEmail($to, $subject, $body);

                                $eMsg = 'Your Guest Post order has been submitted successfully.';
                                $this->Session->setFlash($eMsg, 'flash_success');
                                $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'guestpost', $job = 'New Guest post order placed');
                                $this->redirect($this->params['action']);
                            } else {
                                $this->Guestpost->rollback();
                                $eMsg = 'Your Guest Post coulde not be submitted.';
                                $this->Session->setFlash($eMsg, 'flash_error');
                            }
                            $this->redirect($this->params['action']);
                        } else {
                            $this->generateError('Guestpost');
                        }
                    }
                    //$this->redirect($this->params['action']);
                }
            }
            $this->data = $this->data;
            //$this->set('data',$this->data);
        }

        function editguestpost($postId = null)
        {
            $this->loadModel('Guestpost');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Guestpost', $postId, 'user_id')) {
                $rDirect = BASEURL . 'clients/orderguestpost/';
                return $this->redirect($rDirect);
            }
            if ($this->request->is('post')) {

                $this->Guestpost->set($this->data['Guestpost']);
                if ($this->Guestpost->validates()) {
                    if ($this->Guestpost->save($this->data['Guestpost'], false)) {

                        $eMsg = 'Guest Post updated successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                        // set activity log
                        $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'guestpost', $job = 'Guest post updated');
                        $this->redirect($this->params['action'] . '/' . $postId);
                    } else {
                        $eMsg = 'Guest Post could not updated.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                } else {
                    $this->generateError('Guestpost');
                }
            }

            $data = $this->Guestpost->find('first', array('conditions' => "id = $postId AND status = 1"));

            if (empty($data)) {
                $this->redirect(array('controller' => $this->params['controller'], 'action' => 'manageguestpost'));
            }
            $this->data = $data;
            $this->set('data', $data);
            $this->set('title_for_layout', 'Edit Your Guest Post');
        }

        function manageguestpost()
        {
            $this->loadModel('Guestpost');

            $userId = $this->Auth->user('id');

            $cond = array(
                'conditions' => array(" Guestpost.user_id  =  $userId AND Guestpost.status NOT IN (2,3)"),
                'recursive' => 1,
            );

            $guestPosts = $this->Guestpost->find('all', $cond);
            $this->set(compact('guestPosts'));
            //$this->set('navIndex', '9,1');
            $this->set('title_for_layout', 'Manage Guest Post');
        }

        function assignedguestpost()
        {
            $this->loadModel('Guestpost');
            $userId = $this->Auth->user('id');
            $cond = array(
                'conditions' => array(" Guestpost.user_id  =  $userId AND Guestpost.status IN (2)"),
                'recursive' => 1,
            );
            $guestPosts = $this->Guestpost->find('all', $cond);
            $this->set(compact('guestPosts'));
            $this->set('title_for_layout', 'In Process Guest Post');
        }

        function completedguestpost()
        {
            $this->loadModel('Guestpost');

            $userId = $this->Auth->user('id');

            $cond = array(
                'conditions' => array(" Guestpost.user_id  =  $userId AND Guestpost.status IN (3)"),
                'recursive' => 1,
            );

            $guestPosts = $this->Guestpost->find('all', $cond);
            $this->set(compact('guestPosts'));
            //$this->set('navIndex', '9,2');
            $this->set('title_for_layout', 'Completed Guest Post');
        }

        function deleteguestpost($id)
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Guestpost');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Guestpost', $id, 'user_id')) {
                return '';
            }

            //$this->Guestpost->id = $id;
            //$oData = $this->Guestpost->read();
            if ($this->Guestpost->delete($id, false)) {
                $eMsg = 'Guest Post deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                // set activity log
                $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'guestpost', $job = 'Guest post deleted');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        /*
        * Link Emperor
        *
        */

        function linkemperor()
        {

            $this->set('title_for_layout', 'Link Emperor');

            $serviceId = 13;  //     Linkemperor id = 13, Bazooka Blast = 11
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from you extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }


            $unitPrice = floatval($userServiceCredit['unit_price']);

            App::import('Vendor', 'LinkemperorVendor');
            $linkEmp = new LinkemperorVendor('76d4c635242a15890a4022ca695ef9f7c17fc195');

            if ($this->request->is('post')) {
                //pr($this->data['Linkemperor']['vendor']);exit;
                $this->loadModel('Linkemperor');

                $this->Linkemperor->set($this->data['Linkemperor']);
                if ($this->Linkemperor->validates()) {

                    $callback_url = "http://pageoneengine.com/myaccount/postUrl_linkEmperor.php";
                    $custom = $this->Auth->user('id'); /* can be any custom identifier to identify this purchase */;
                    $how_pay = "credits"; /* can be cash or credits */

                    $url1 = 'http://' . str_replace('http://', '', trim($this->data['Linkemperor']['url1']));
                    $anchor1 = trim($this->data['Linkemperor']['anchor1']);
                    $url2 = 'http://' . str_replace('http://', '', trim($this->data['Linkemperor']['url2']));
                    $anchor2 = trim($this->data['Linkemperor']['anchor2']);

                    $services = $srvcs = array();
                    $quantity = 0;
                    $orders = array();
                    if (($anchor1 != '') && filter_var($url1, FILTER_VALIDATE_URL)) {
                        $orders[] = array('url' => $url1, 'anchor_text' => $anchor1, 'title' => trim($anchor1));
                    }
                    if (($anchor2 != '') && filter_var($url2, FILTER_VALIDATE_URL)) {
                        $orders[] = array('url' => $url2, 'anchor_text' => $anchor2, 'title' => trim($anchor2));
                    }
                    foreach ($this->data['Linkemperor']['vendor'] as $k => $v) {
                        if ($v) {
                            $srvcs[$k] = $v;
                            $services[] = array('service_id' => $k, 'quantity' => $v);
                            $quantity += intval($v);
                        }
                    }

                    // make the json for LE
                    $reqestArr = array();
                    foreach ($orders as $o) {
                        foreach ($services as $s) {
                            $r['targets'] = array('target' => $o);
                            $r['services'] = array('service' => $s);
                            $reqestArr['request'][] = $r;
                        }
                    }
                    $r = array(
                        array(
                            'order' => array(
                                'how_pay' => 'credits',
                                'callback_url' => $callback_url,
                                'custom' => $custom, //* name this request.  referenced by other requests below.
                                'requests' => $reqestArr
                            )
                        )
                    );

                    $requests = json_encode($r);

                    //pr($requests);exit;
                    /* if ( $this->Auth->user('id') == 2 ) {
                    pr($requests);
                    exit;

                    } */

                    //Configure::write('debug', 2);

                    if (!empty($orders) && !empty($services)) {
                        $userServiceCredit = $this->checkUserServiceLimit($serviceId);
                        if ($userServiceCredit['eligible'] || $userServiceCredit['extra_credit_eligible']) {
                            $currentServiceLimit = floatval($userServiceCredit['service_limit']);
                            $serviceUsed = floatval($userServiceCredit['service_used']);
                            $serviceUsed = $serviceUsed + $quantity;
                            $unitPrice = floatval($userServiceCredit['unit_price']);
                            $totalPrice = $unitPrice * $quantity;

                            if ($currentServiceLimit >= $serviceUsed || $totalPrice <= $userServiceCredit['extra_credit']) {
                                $finalOrder = $linkEmp->linkemperor_purchase($requests);
                                /* if ($userId == 2) {
                                pr($finalOrder);
                                exit;
                                } */

                                $insertArr = array();
                                if (isset($finalOrder['id'])) {
                                    $insertArr['user_id'] = $this->Auth->user('id');
                                    $insertArr['ordername'] = trim($this->data['Linkemperor']['ordername']);
                                    $insertArr['order_id'] = $finalOrder['id'];
                                    $insertArr['orders'] = serialize($orders);
                                    $insertArr['services'] = serialize($srvcs);
                                    $insertArr['custom'] = $custom;
                                    $insertArr['status'] = $finalOrder['status'];

                                    if ($this->Linkemperor->save($insertArr, false)) {

                                        $updateAccount['user_id'] = $userId;
                                        $updateAccount['service_id'] = $serviceId;
                                        $updateAccount['quantity'] = $quantity;
                                        $updateAccount['amount'] = $totalPrice;
                                        $updateAccount['remarks'] = 'Credit deducted for Linkemperor services.';

                                        if ($this->updateAccountDebit($updateAccount)) {
                                            if ($currentServiceLimit < $serviceUsed && $totalPrice <= $userServiceCredit['extra_credit']) {
                                                $this->User->id = $userId;
                                                $newExtraCredit = ($userServiceCredit['extra_credit'] - $totalPrice);
                                                $this->User->saveField('extra_credit', $newExtraCredit);
                                            }
                                            $eMsg = 'Order posted successfully.';
                                            $this->Session->setFlash($eMsg, 'flash_success');
                                            // set activity log
                                            $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'linkemperor', $job = 'Link emperor order placed');
                                        } else {
                                            $eMsg = 'Something went wrong while updating you account.';
                                            $this->Session->setFlash($eMsg, 'flash_error');
                                            $this->redirect($this->params['action']);
                                        }
                                        //pr($finalOrder);
                                        //exit;
                                    } else {
                                        $eMsg = 'Order record could not be saved.';
                                        $this->Session->setFlash($eMsg, 'flash_error');
                                    }
                                } else {
                                    $eMsg = 'Order could not be posted.';
                                    $this->Session->setFlash($eMsg, 'flash_error');
                                }
                            } else {
                                $eMsg = 'You have reached your limit.';
                                $this->Session->setFlash($eMsg, 'flash_error');
                            }
                        } else {
                            $eMsg = 'You need credits to use this service!';
                            $this->Session->setFlash($eMsg, 'flash_error');
                        }
                    } else {
                        $eMsg = 'Please make sure you have picked at least one vendor. or The Link Emperor service is busy now! We could not process your request. Please try again later!';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action']);
                } else {
                    $this->generateError('Linkemperor');
                }
            }

            $vendors = $linkEmp->linkemperor_services();

            $vendors = is_array($vendors) ? $vendors : array();
            //pr($vendors);
            //exit;

            $this->loadModel('Bazooka');
            $bazookaCats = array();
            $bazookaCatsAll = array();
            if ($vendors) {
                foreach ($vendors as $lS) {
                    $bazookaCats[] = trim((string)$lS['name']);
                }
                $bazookaCats = array_unique(array_filter($bazookaCats));
                //pr($bazookaCats);
                if ($bazookaCats) {
                    foreach ($bazookaCats as $key => $bC) {

                        $insertCat['name'] = trim($bC);
                        $this->Bazooka->set($insertCat);
                        if ($this->Bazooka->validates()) {
                            $this->Bazooka->create();
                            $this->Bazooka->save($insertCat);
                        }

                        foreach ($vendors as $lSinner) {
                            $catName = strtolower(trim($bC));
                            if ($catName == (string)strtolower(trim($lSinner['name']))) {
                                $bazookaCatsAll[$catName][] = (array)$lSinner;
                            }
                        }
                    }
                }
            }

            $bazookaCatNames = $this->Bazooka->find('list', array('fields' => 'id,name', 'conditions' => "Bazooka.status = 1"));
            if ($bazookaCatNames) {
                foreach ($bazookaCatNames as $key => $name) {
                    $bazookaCatNames[$key] = strtolower(trim($name));
                }
            }

            if ($bazookaCatNames) {
                if ($bazookaCatsAll) {
                    $this->loadModel('Bazookaservice');
                    foreach ($bazookaCatsAll as $key => $bC) {
                        if (in_array($key, $bazookaCatNames)) {

                            foreach ($bC as $b) {
                                $insertData['name'] = $b['vendor'];
                                $insertData['service_id'] = $b['id'];
                                $insertData['cat_id'] = @array_search($key, $bazookaCatNames);

                                $this->Bazookaservice->set($insertData);
                                if ($this->Bazookaservice->validates()) {
                                    $this->Bazookaservice->create();
                                    $this->Bazookaservice->save($insertData);
                                }
                            }
                        }
                    }
                }
            }
            $this->set('vendors', $vendors);
        }

        function linkemperorinprogress()
        {
            $this->loadModel('Linkemperor');
            $this->loadModel('Bazookaservice');
            $this->loadModel('Linkemperorresults');

            $userId = $this->Auth->user('id');
            //$orderList = $this->Linkemperor->find('all', array('conditions' => "Linkemperor.status = 'In Progress' AND Linkemperor.order_type = 0 AND Linkemperor.user_id = $userId"));

            $orderList = $this->Linkemperor->find('all', array(
                'conditions' => "Linkemperor.order_type = 0 AND Linkemperor.user_id = $userId",
                'order' => 'Linkemperor.id DESC'
            ));

            //print_r($orderList);

            if ($orderList) {

                foreach ($orderList as $k => $order) {
                    $r = $this->Linkemperorresults->find('all', array('conditions' => "Linkemperorresults.order_id = " . $order['Linkemperor']['order_id']));
                    $res = array();
                    if ($r) {
                        foreach ($r as $kk => $rs) {
                            $res[] = $rs['Linkemperorresults'];
                        }
                    }
                    $orderList[$k]['Linkemperor']['results'] = $res;
                }
            }


            $serviceList = $this->Bazookaservice->find('list', array('fields' => 'service_id,name'));
            $this->set(compact('orderList'));
            $this->set(compact('serviceList'));

            //Configure::write('debug', 2);
            //pr($orderList);
            //exit;

            $this->set('title_for_layout', 'Link Emperor In Progress Campaigns');
        }

        function linkemperorcompleted()
        {
            $this->loadModel('Linkemperor');
            $this->loadModel('Bazookaservice');
            $this->loadModel('Linkemperorresults');
            $userId = $this->Auth->user('id');
            $orderList = $this->Linkemperor->find('all', array('conditions' => "Linkemperor.status = 'Completed' AND Linkemperor.order_type = 0 AND Linkemperor.user_id = $userId"));
            $serviceList = $this->Bazookaservice->find('list', array('fields' => 'service_id,name'));

            $this->set(compact('orderList'));
            $this->set(compact('serviceList'));
            $this->set('title_for_layout', 'Link Emperor Completed Campaigns');
        }

        function linkempdetails($oId = null, $sId = null)
        {
            Configure::write('debug', 0);
            $this->autoRender = false;
            $this->layout = '';
            if (empty($oId) || empty($sId)) {
                echo 'Nothing Found to show!';
                exit;
            }
            $this->loadModel('Linkemperorresults');
            $detail = $this->Linkemperorresults->find('first', array('conditions' => array(" order_id = $oId and service_id = $sId ")));
            $this->set(compact('detail'));
            //$this->viewPath = 'Common';
            return $this->render('linkempdetails');
        }

        /*
        * Bazooka Blast
        *
        */

        function bazookablast()
        {

            $this->set('title_for_layout', 'Bazooka Blast');

            $serviceId = 11;
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from you extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {
                $unitPrice = floatval($userServiceCredit['unit_price']);

                if ($this->request->is('post')) {
                    $this->loadModel('Linkemperor');

                    $this->Linkemperor->set($this->data['Linkemperor']);
                    if ($this->Linkemperor->validates()) {
                        //pr($this->data);
                        $callback_url = "http://pageoneengine.com/myaccount/postUrl_linkEmperor.php";
                        $custom = $this->Auth->user('id'); /* can be any custom identifier to identify this purchase */;
                        $how_pay = "credits"; /* can be cash or credits */

                        $url1 = 'http://' . str_replace('http://', '', trim($this->data['Linkemperor']['url1']));
                        $anchor1 = trim($this->data['Linkemperor']['anchor1']);
                        $orders = array();
                        if (($anchor1 != '') && filter_var($url1, FILTER_VALIDATE_URL)) {
                            $orders[] = array('url' => $url1, 'anchor_text' => $anchor1, 'title' => trim($this->data['Linkemperor']['ordername']));
                        }

                        App::import('Vendor', 'LinkemperorVendor');
                        $linkEmp = new LinkemperorVendor('76d4c635242a15890a4022ca695ef9f7c17fc195');
                        $vendors = $linkEmp->linkemperor_services();

                        $bazookaCats = array();
                        $bazookaCatsAll = array();
                        if ($vendors) {
                            foreach ($vendors as $lS) {
                                $bazookaCats[] = trim((string)$lS['name']);
                            }
                            $bazookaCats = array_unique(array_filter($bazookaCats));
                            if ($bazookaCats) {
                                foreach ($bazookaCats as $key => $bC) {
                                    foreach ($vendors as $lSinner) {
                                        $catName = strtolower(trim($bC));
                                        if ($catName == (string)strtolower(trim($lSinner['name']))) {
                                            $bazookaCatsAll[$catName][] = (array)$lSinner;
                                        }
                                    }
                                }
                            }
                        }

                        $this->loadModel('Bazooka');
                        $bazookaCatNames = $this->Bazooka->find('list', array('fields' => 'id,name', 'conditions' => "Bazooka.status = 1"));
                        if ($bazookaCatNames) {
                            foreach ($bazookaCatNames as $key => $name) {
                                $bazookaCatNames[$key] = strtolower(trim($name));
                            }
                        }

                        $services = $srvs = array();
                        if ($bazookaCatNames) {
                            if ($bazookaCatsAll) {
                                $this->loadModel('Bazookaservice');
                                foreach ($bazookaCatsAll as $key => $bC) {
                                    if (in_array($key, $bazookaCatNames)) {
                                        if (count($bC)) {
                                            $rId = rand(0, count($bC) - 1);
                                            $service_Id = $bC[$rId]['id'];
                                            $srvs[$service_Id] = 1;
                                            $services[] = array('service_id' => $service_Id, 'quantity' => 1);
                                        }
                                    }
                                }
                            }
                        }

                        /* if ($userId == 2) {
                        pr($orders);
                        } */

                        // make the json for LE
                        $reqestArr = array();
                        foreach ($orders as $o) {
                            foreach ($services as $s) {
                                $r['targets'] = array('target' => $o);
                                $r['services'] = array('service' => $s);
                                $reqestArr['request'][] = $r;
                            }
                        }
                        $r = array(
                            array(
                                'order' => array(
                                    'how_pay' => 'credits',
                                    'callback_url' => $callback_url,
                                    'custom' => $custom, //* name this request.  referenced by other requests below.
                                    'requests' => $reqestArr
                                )
                            )
                        );

                        $requests = json_encode($r);

                        /* if ($userId == 2) {
                        pr($requests);
                        } */

                        if (!empty($orders) && !empty($services)) {
                            $finalOrder = $linkEmp->linkemperor_purchase($requests);

                            /* if ($userId == 2) {
                            pr($finalOrder);
                            exit;
                            } */

                            $insertArr = array();
                            if (isset($finalOrder['id'])) {
                                $insertArr['user_id'] = $this->Auth->user('id');
                                $insertArr['ordername'] = trim($this->data['Linkemperor']['ordername']);
                                $insertArr['order_type'] = 1;
                                $insertArr['order_id'] = $finalOrder['id'];
                                $insertArr['orders'] = serialize($orders);
                                $insertArr['services'] = serialize($srvs);
                                $insertArr['custom'] = $custom;
                                $insertArr['status'] = $finalOrder['status'];

                                if ($this->Linkemperor->save($insertArr, false)) {

                                    $updateAccount['user_id'] = $userId;
                                    $updateAccount['service_id'] = $serviceId;
                                    $updateAccount['amount'] = $unitPrice;
                                    $updateAccount['remarks'] = 'Credit deducted for Bazooka Blast Services.';

                                    if ($this->updateAccountDebit($updateAccount)) {
                                        if (!$userServiceCredit['eligible'] && $userServiceCredit['extra_credit_eligible']) {
                                            $this->User->id = $userId;
                                            $newExtraCredit = ($userServiceCredit['extra_credit'] - $unitPrice);
                                            $this->User->saveField('extra_credit', $newExtraCredit);
                                        }
                                        $eMsg = 'Order posted successfully.';
                                        $this->Session->setFlash($eMsg, 'flash_success');
                                        // set activity log
                                        $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'bazookablast', $job = 'Bazooka order placed');
                                    } else {
                                        $eMsg = 'Something went wrong while updating you account.';
                                        $this->Session->setFlash($eMsg, 'flash_error');
                                        $this->redirect($this->params['action']);
                                    }
                                } else {
                                    $eMsg = 'Order record could not be saved.';
                                    $this->Session->setFlash($eMsg, 'flash_error');
                                }
                            } else {
                                $eMsg = 'Order could not be posted.';
                                $this->Session->setFlash($eMsg, 'flash_error');
                            }
                        } else {
                            $eMsg = 'The The Link Emperor service is busy now! We could not process your request. Please try again later. Really sorry for inconvenience!';
                            $this->Session->setFlash($eMsg, 'flash_error');
                        }
                    } else {
                        $this->generateError('Linkemperor');
                    }
                }
            }
        }

        function bazookainprogress()
        {
            $this->loadModel('Linkemperor');
            $this->loadModel('Bazookaservice');
            $this->loadModel('Linkemperorresults');
            $userId = $this->Auth->user('id');
            // $orderList = $this->Linkemperor->find('all', array('conditions' => "Linkemperor.status = 'In Progress' AND Linkemperor.order_type = 1 AND Linkemperor.user_id = $userId"));
            $orderList = $this->Linkemperor->find('all', array(
                'conditions' => "Linkemperor.order_type = 1 AND Linkemperor.user_id = $userId",
                'order' => 'Linkemperor.id DESC'
            ));

            if ($orderList) {

                foreach ($orderList as $k => $order) {
                    $r = $this->Linkemperorresults->find('all', array('conditions' => "Linkemperorresults.order_id = " . $order['Linkemperor']['order_id']));
                    $res = array();
                    if ($r) {
                        foreach ($r as $kk => $rs) {
                            $res[] = $rs['Linkemperorresults'];
                        }
                    }
                    $orderList[$k]['Linkemperor']['results'] = $res;
                }
            }
            $serviceList = $this->Bazookaservice->find('list', array('fields' => 'service_id,name'));
            $this->set(compact('orderList'));
            $this->set(compact('serviceList'));
            $this->set('title_for_layout', 'Bazooka Blast In Progress Campaigns');
        }

        function bazookacompleted()
        {
            $this->loadModel('Linkemperor');
            $this->loadModel('Bazookaservice');
            $userId = $this->Auth->user('id');
            $orderList = $this->Linkemperor->find('all', array('conditions' => "Linkemperor.status = 'Completed' AND Linkemperor.order_type = 1 AND Linkemperor.user_id = $userId"));
            $serviceList = $this->Bazookaservice->find('list', array('fields' => 'service_id,name'));

            $this->set(compact('orderList'));
            $this->set(compact('serviceList'));
            $this->set('title_for_layout', 'Bazooka Blast Completed Campaigns');
        }

        /*
        * Domain Dropdown Option
        *
        */

        function domaindropdown()
        {
            //$this->autoRender = false;
            $this->layout = 'ajax';
            if ($this->request->is('post')) {
                $networkType = $this->data['value1'];
                $userId = $this->Auth->user('id');

                $domainList = array();
                $conditions = array();

                if ($networkType == 1) {
                    $conditions = array("Domain.networktype = 1 AND Domain.user_id = $userId AND Domain.status = 1");
                } elseif ($networkType == 3) {
                    $conditions = array("Domain.networktype = 3 AND Domain.user_id = -1 AND Domain.status = 1");
                } elseif ($networkType == 4) {
                    $conditions = array("Domain.user_id = $userId AND Domain.status = 1");
                } else {
                    $conditions = array("Domain.networktype = 99999");
                }
                $cond = array(
                    'fields' => 'Domain.id,Domain.domain',
                    'conditions' => $conditions,
                    'recursive' => -1,
                    'order' => 'Domain.domain ASC'
                );
                $domainList = $this->Domain->find('list', $cond);
                $this->set(compact('domainList'));
            }
        }

        function historicalranking()
        {

            if ($this->request->is('post')) {
                if ($this->data['HRanking']['link_name']) {

                    $isUrl = $this->data['HRanking']['is_url'];

                    $domainName = str_replace('%23', '.', urldecode($this->data['HRanking']['link_name']));
                    $domainName = str_replace('#', '.', $domainName);
                    $se = trim($this->data['HRanking']['se']);

                    if ($isUrl) {
                        $url = 'http://' . $se . '.api.semrush.com/?action=report&type=url_organic&key=53943933b40028e9b9a33a8cdedc24f7&display_limit=10&export=api&export_columns=Ph,Po,Nq,Cp,Co,Tr,Tc,Nr,Td&url=' . $domainName;
                    } else {
                        $url = 'http://' . $se . '.api.semrush.com/?action=report&type=domain_organic&key=53943933b40028e9b9a33a8cdedc24f7&domain=' . $domainName . '&export=api&export_columns=Ph,Po,Nq,Cp,Ur,Tr,Tc,Co,Nr';
                    }
                    //exit;
                    //&display_limit=%LIMIT%&display_offset=%OFFSET%&export=api&export_columns=%EXPORT_COLUMNS%;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_REFERER, 'http://www.seonitronetwork.com');
                    $result = curl_exec($ch);
                    curl_close($ch);

                    $this->set(compact('domainName'));
                    $this->set(compact('result'));
                }
                // set activity log
                $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'historicalranking', $job = 'Used historical ranking tools');
            }

            $searchEngines = array(
                'us' => 'Google.com',
                'uk' => 'Google.co.uk',
                'ca' => 'Google.ca',
                'ru' => 'Google.ru',
                'de' => 'Google.de',
                'fr' => 'Google.fr',
                'es' => 'Google.es',
                'it' => 'Google.it',
                'br' => 'Google.com.br',
                'au' => 'Google.com.au',
                'us.bing' => 'Bing.com'
            );
            $this->set(compact('searchEngines'));

            $this->set('navIndex', '2,2');
            $this->set('title_for_layout', 'Historical Ranking');
        }

        /*
        * FAQ page
        */

        function faq($cat_id = null)
        {
            $this->loadModel('Faq');
            $this->loadModel('Faqcategory');

            if ($cat_id) {
                $faqAll = $this->Faq->find('all', array(
                    'conditions' => array("Faq.category_id = {$cat_id} AND Faq.status = 1"),
                    'recursive' => 1
                ));
                $this->set(compact('faqAll'));
                $this->set('params', $cat_id);

                $faqCat = $this->Faqcategory->find('first', array(
                    'conditions' => array("Faqcategory.id = {$cat_id}"),
                    'limit' => 1
                ));
                $cat_name = $faqCat['Faqcategory']['category'];
                $this->set('cat_name', $cat_name);
            } else {
                $faqCat = $this->Faqcategory->find('first', array(
                    'conditions' => array("Faqcategory.status = 1"),
                    'limit' => 1
                ));
                $cat_id = $faqCat['Faqcategory']['id'];
                $faqAll = $this->Faq->find('all', array(
                    'conditions' => array("Faq.category_id = {$cat_id} AND Faq.status = 1"),
                    'recursive' => 1
                ));
                $this->set(compact('faqAll'));
                //$this->set('params', $cat_id);
            }

            $faqAllCategories = $this->Faqcategory->find('all', array(
                'conditions' => array("Faqcategory.status = 1"),
                'recursive' => 1
            ));
            $this->set(compact('faqAllCategories'));
            //            echo count($faqAllCategories);
            //            pr($faqAllCategories); exit;
            $this->set('title_for_layout', 'Frequently Asked Questions');
        }

        /*
        * Education Page
        */

        function education()
        {
            $this->loadModel('Education');
            // find education for this user group
            /* $this->Education->bindModel(array(
            'belongsTo' => array(
            'Category' => array('foreignKey' => 'cat_id'),
            )
            )); */

            $uPid = $this->Auth->user("package_id");
            $education = (array)$this->Education->find('all', array(
                'conditions' => array("Education.packages like '%,$uPid,%' "),
                'order' => 'cat_id ASC',
                //'fields' => 'id,cat_id,title,modified'
            ));
            $sortedData = array();
            foreach ($education as $k => $v) {
                $cId = $v['Education']['cat_id'];
                $path = $this->getcatparentlist($cId);
                //echo $path;
                $sortedData[$path][] = $v['Education'];
            }
            ksort($sortedData);
            $this->set(compact('sortedData'));
            //pr($sortedData);
            /* $outputA = ' <div class="accordion" id="accordion1">';
            $output = '';
            foreach ($education as $k => $v) {

            $title = strip_tags($v['Education']['title']);
            $body = $v['Education']['body'];
            $body = preg_replace($replace, $replacement, $body);

            // page header set
            $output .= ' <div class="marginT10"></div> <div class="page-header">
            <h4>';
            $output .= $title;
            $output .= '</h4></div>';

            }
            $outputA .= '</div>';
            $this->set(compact('output')); */
            $this->set('title_for_layout', 'Education');
        }

        function etopics($id = null)
        {
            $this->loadModel('Media');
            $this->loadModel('Education');
            $this->Media->useTable = 'medias';
            $media = (array)$this->Media->find('all');
            $replace = $replacement = array();
            foreach ($media as $k => $v) {
                if ($v['Media']['type'] == 'image') {
                    $pt = '<a title="' . $v['Media']['title'] . '" class="fancybox tip" href="/media/' . $v['Media']['source'] . '">' . $v['Media']['title'] . '</a>';
                    $et = '<a style="display:block;" title="' . $v['Media']['title'] . '" class="fancybox tip" href="/media/' . $v['Media']['source'] . '">';
                    $et .= '<img style="max-width:250px;"  alt="Preview" src="/media/' . $v['Media']['source'] . '" />';
                    $et .= '</a>';
                }

                if ($v['Media']['type'] == 'video') {
                    $pt = '<a class="openModalDialog" title="Preview. Double click on Video for full screen" href="http://pageoneengine.com/myaccount/clients/videopreview/' . $v['Media']['source'] . '">' . $v['Media']['title'] . '</a>';

                    $src = 'http://pageoneengine.com/myaccount/media/' . $v['Media']['source'];
                    $vtype = 'video/mp4';
                    $et = '<div class="flowplayer" data-swf="http://pageoneengine.com/myaccount/js/flowplayer543/flowplayer.swf" data-ratio="0.4167">
                    <video controls = "controls" width = "100%" height = "340">
                    <source src = "' . $src . '" type = "' . $vtype . '">
                    </video>
                    </div>';
                }

                if ($v['Media']['type'] == 'files') {
                    $pt = '<a class="fancypdf" title="Read It!" href="/media/' . $v['Media']['source'] . '">' . $v['Media']['title'] . '</a>';
                    //echo  '<a class="fancypdf" title="Preview"  href="/media/' . $arr['Media']['source'].'">preview</a>';

                    $src = 'http://pageoneengine.com/myaccount/media/' . $v['Media']['source'];
                    //$vtype = 'video/mp4';
                    $et = '<a title="Download It!" href="' . $src . '" target="_blank">' . $v['Media']['title'] . '</a>';
                }


                if ($v['Media']['type'] == 'link') {
                    $pt = '<a title="' . $v['Media']['title'] . '" class="fancybox-media" href="' . trim($v['Media']['source']) . '">preview</a>';
                    // $et = '<iframe width="640" height="230" id="fancybox-frame'.time().'" name="fancybox-frame'.time().'" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" scrolling="auto" src="'. str_replace('http:','',trim($v['Media']['source'])).'?autoplay=0&amp;autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1"></iframe>';
                    $et = '<iframe width="95%" height="340" id="fancybox-frame' . time() . '" frameborder="0" vspace="0" hspace="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" scrolling="auto" src="' . str_replace('http:', '', trim($v['Media']['source'])) . '"></iframe>';
                }
                $replace[] = '/\[popup_' . $v['Media']['id'] . '\]/';
                $replacement[] = $pt;
                $replace[] = '/\[embed_' . $v['Media']['id'] . '\]/';
                $replacement[] = $et;
            }
            $this->set(compact('replace', 'replacement'));
            $education = (array)$this->Education->find('first', array(
                'conditions' => array("Education.id = $id"),
                'order' => 'cat_id ASC',
            ));
            $this->set(compact('education'));


            // find related topics
            $uPid = $this->Auth->user("package_id");
            $catId = $education['Education']['cat_id'];
            $education = (array)$this->Education->find('all', array(
                'conditions' => array(" Education.cat_id = $catId AND Education.packages like '%,$uPid,%' "),
                'fields' => 'id,cat_id,title,modified'
            ));
            $sortedData = array();
            foreach ($education as $k => $v) {
                $cId = $v['Education']['cat_id'];
                $path = $this->getcatparentlist($cId);
                $sortedData[$path][] = $v['Education'];
            }
            ksort($sortedData);
            $this->set(compact('sortedData'));
            // pr($sortedData);


            $this->set('title_for_layout', 'Education');
        }

        // end education

        function resources()
        {
            $this->loadModel('Ruser');
            $this->loadModel('Recording');
            $cId = $this->Auth->user('id');
            $getPack = $this->Ruser->find('all', array(
                'conditions' => array("Ruser.user_id = $cId"),
                'recursive' => 1,
            ));
            $user = array();
            if (count($getPack) > 0) {
                foreach ($getPack as $k => $U) {
                    $user[$U['Ruser']['package_id']] = $U['Package']['packagename'];
                }
            }
            //pr($user);
            $this->set(compact('user'));
            $this->set('title_for_layout', 'Resources');
        }

        function rday($pid = null)
        {

            $this->loadModel('Ruser');
            // check if this user can access this resources
            $cId = $this->Auth->user('id');
            $getPack = $this->Ruser->find('all', array(
                'conditions' => array("Ruser.user_id = $cId"),
                'recursive' => 1,
            ));
            $allowed = array();
            if (count($getPack) > 0) {
                foreach ($getPack as $k => $U) {
                    array_push($allowed, $U['Ruser']['package_id']);
                }
            }
            if (!in_array($pid, $allowed)) {
                return $this->redirect(array('action' => 'resources'));
            }


            //$p = array('21' => 'SEORockstars Recordings', '22' => 'SEORockstar Recordings Downsell', '23' => 'SEORockstar Recordings OTO');
            $this->loadModel('Recording');
            $ryear = (($pid == 21) || ($pid == 22)) ? '(2014)' : '(2012,2013)';
            $allRecording = $this->Recording->find('all', array('conditions' => array("Recording.ryear in $ryear "), 'order' => 'day_number asc', 'recursive' => 1));

            $days = array();
            foreach ($allRecording as $k => $v) {
                $days[$v['Recording']['ryear']][$v['Recording']['day_number']][] = $v['Recording'];
            }

            $this->set('pid', $pid);
            $this->set(compact('days'));
            $this->set(compact('allRecording'));
            $this->set('title_for_layout', 'Watch, Read SEORockstars Resources');
        }

        function vresource($pId = null, $rId = null)
        {

            $this->loadModel('Media');
            $this->loadModel('Education');
            $this->Media->useTable = 'medias';
            $media = (array)$this->Media->find('all');
            $replace = $replacement = array();
            foreach ($media as $k => $v) {
                if ($v['Media']['type'] == 'image') {
                    $pt = '<a title="' . $v['Media']['title'] . '" class="fancybox tip" href="/media/' . $v['Media']['source'] . '">' . $v['Media']['title'] . '</a>';
                    $et = '<a style="display:block;" title="' . $v['Media']['title'] . '" class="fancybox tip" href="/media/' . $v['Media']['source'] . '">';
                    $et .= '<img style="max-width:250px;"  alt="Preview" src="/media/' . $v['Media']['source'] . '" />';
                    $et .= '</a>';
                }

                if ($v['Media']['type'] == 'video') {
                    $pt = '<a class="openModalDialog" title="Preview. Double click on Video for full screen" href="http://pageoneengine.com/myaccount/clients/videopreview/' . $v['Media']['source'] . '">' . $v['Media']['title'] . '</a>';

                    $src = 'http://pageoneengine.com/myaccount/media/' . $v['Media']['source'];
                    $vtype = 'video/mp4';
                    $et = '<div class="flowplayer" data-swf="http://pageoneengine.com/myaccount/js/flowplayer543/flowplayer.swf" data-ratio="0.4167">
                    <video controls = "controls" width = "100%" height = "340">
                    <source src = "' . $src . '" type = "' . $vtype . '">
                    </video>
                    </div>';
                }

                if ($v['Media']['type'] == 'files') {
                    $pt = '<a class="fancypdf" title="Read It!" href="/media/' . $v['Media']['source'] . '">' . $v['Media']['title'] . '</a>';
                    //echo  '<a class="fancypdf" title="Preview"  href="/media/' . $arr['Media']['source'].'">preview</a>';

                    $src = 'http://pageoneengine.com/myaccount/media/' . $v['Media']['source'];
                    //$vtype = 'video/mp4';
                    $et = '<a title="Download It!" href="' . $src . '" target="_blank">' . $v['Media']['title'] . '</a>';
                }
                // media type is link
                if ($v['Media']['type'] == 'link') {
                    $pt = '<a title="' . $v['Media']['title'] . '" class="fancybox-media" href="' . trim($v['Media']['source']) . '">' . $v['Media']['title'] . '</a>';
                    // $et = '<iframe width="640" height="230" id="fancybox-frame'.time().'" name="fancybox-frame'.time().'" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" scrolling="auto" src="'. str_replace('http:','',trim($v['Media']['source'])).'?autoplay=0&amp;autohide=1&amp;fs=1&amp;rel=0&amp;hd=1&amp;wmode=opaque&amp;enablejsapi=1"></iframe>';

                    if (preg_match('/youtube/', $v['Media']['source'])) {
                        $srcc = explode("=", trim($v['Media']['source']));
                        $yts = '//www.youtube.com/embed/' . $srcc[1];
                        $et = '<iframe width="95%" height="500" src="' . $yts . '" frameborder="0" allowfullscreen></iframe>';
                    } else {
                        $et = '<iframe width="95%" height="340" id="fancybox-frame' . time() . '" frameborder="0" vspace="0" hspace="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" scrolling="auto" src="' . str_replace('http:', '', trim($v['Media']['source'])) . '"></iframe>';
                    }
                }
                $replace[] = '/\[popup_' . $v['Media']['id'] . '\]/';
                $replacement[] = $pt;
                $replace[] = '/\[embed_' . $v['Media']['id'] . '\]/';
                $replacement[] = $et;
            }
            $this->set(compact('replace', 'replacement'));


            $this->loadModel('Ruser');
            // check if this user can access this resources
            $cId = $this->Auth->user('id');
            $getPack = $this->Ruser->find('all', array(
                'conditions' => array("Ruser.user_id = $cId"),
                'recursive' => 1,
            ));
            $allowed = array();
            if (count($getPack) > 0) {
                foreach ($getPack as $k => $U) {
                    array_push($allowed, $U['Ruser']['package_id']);
                }
            }
            if (!in_array($pId, $allowed)) {
                return $this->redirect(array('action' => 'resources'));
            }
            // $pack = array('21' => 'SEORockstars Recordings', '22' => 'SEORockstar NOTES', '23' => 'SEORockstars 2012/13 Recordings');
            $this->loadModel('Recording');
            $data = $this->Recording->find('first', array('conditions' => array("Recording.id = $rId "), 'recursive' => 1));
            $allowed = array('2014' => array(21, 23), '2012' => array(22), '2013' => array(22));
            if ((($pId == 21) || ($pId == 22)) && $data['Recording']['ryear'] != 2014) {
                return $this->redirect(array('action' => 'resources'));
            }
            if ((($pId == 23)) && !in_array($data['Recording']['ryear'], array(2013, 2012))) {
                return $this->redirect(array('action' => 'resources'));
            }
            $data = $this->Recording->find('first', array('conditions' => array("Recording.id = $rId "), 'recursive' => 1));

            $this->set('pId', $pId);
            $this->set(compact('data'));
            $this->set('title_for_layout', $data['Recording']['title']);
        }

        protected function getcatparentlist($id = null)
        {
            $this->loadModel('Category');
            $path = (array)$this->Category->getPath($id);
            $i = 1;
            $p = '';
            if (count($path)) {
                foreach ($path as $k => $ph) {
                    $p .= $ph['Category']['name'] . ($i != count($path) ? " &rArr; " : "");
                    $i++;
                }
            }
            return $p;
        }

        /*
        * Tour page
        */

        function tour()
        {

            //$this->set('navIndex', '0,0');
            $this->set('title_for_layout', 'Tour');
        }

        function support()
        {

            //$this->set('navIndex', '0,0');
            $this->set('title_for_layout', 'Support Desk');
        }

        function domainalldetails($id = null)
        {
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Domain', $id, 'user_id')) {
                $rDirect = BASEURL . 'clients/adddomain/';
                return $this->redirect($rDirect);
            }
            $domain = $this->Domain->findById($id);
            $this->set(compact('domain'));
            //$this->set('navIndex', '0,0');
            $this->set('title_for_layout', 'Everything on this Domain');
        }

        function updateCampaignInstantly($cId = null)
        {
            if ($cId) {
                $cId = intval($cId);
            } else {
                $cId = 0;
            }
            $post_data = array();
            //$urltopost = "http://tracker.seonitro.com/generateCampaignResults.php";
            $urltopost = "http://pageoneengine.com/myaccount/devs/nitro_set_results_manual/$cId";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urltopost);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP/' . phpversion());
            curl_exec($ch);
            //$response = trim(strtolower(curl_exec($ch)));
            curl_close($ch);
        }

        private function manageservices()
        {

            $userId = $this->Auth->user('id');
            $userExtraCredit = $this->getUserExtraCredit($userId);
            $this->set(compact('userExtraCredit'));

            $userCredits = $this->Usercredit->find('all', array('conditions' => "Usercredit.user_id = $userId"));
            $this->set(compact('userCredits'));

            if ($this->request->is('post')) {
                //pr($_POST);
                //exit;
                $totalExtraCreditRequired = $this->data['Extra']['total_extra_credit_required'];

                if ($userExtraCredit <= $totalExtraCreditRequired) {
                    $actualExtraCreditNeeded = number_format(floatval($totalExtraCreditRequired - $userExtraCredit), 2);
                    $eMsg = "You need another $actualExtraCreditNeeded credit to avail these service changes!";
                    $this->Session->setFlash($eMsg, 'flash_error');
                    $this->redirect($this->params['action']);
                } else {
                    $actualExtraCreditAfterDeducted = floatval($userExtraCredit - $totalExtraCreditRequired);

                    $this->User->id = $userId;
                    if ($this->User->saveField('extra_credit', $actualExtraCreditAfterDeducted)) {
                        foreach ($userCredits as $uC) {
                            $serviceId = $uC['Usercredit']['service_id'];
                            $currentServiceLimit = $uC['Usercredit']['service_limit'];
                            $extraUnitPosted = $this->data['Extra']['extra_unit'][$serviceId];
                            if ($extraUnitPosted > 0) {
                                $newCurrentServiceLimit = intval($currentServiceLimit + $extraUnitPosted);
                                $this->Usercredit->id = $uC['Usercredit']['id'];
                                $this->Usercredit->saveField('current_service_limit', $newCurrentServiceLimit);
                            }
                        }
                        $eMsg = "Your request has been processd successfully!";
                        $this->Session->setFlash($eMsg, 'flash_success');
                        $this->redirect(array('action' => 'myplan'));
                    }
                }
            }
        }

        private function checkUserServiceLimit($serviceId, $quantity = 1) {

            Configure::write('Cache.disable', true);

            $userId = $this->Auth->user('id');

            $this->loadModel('Service');
            $this->loadModel('Serviceprice');
            $this->loadModel('Usercredit');

            $reArr = array();

            $this->User->id = $userId;
            $userExtraCredit = $this->User->field('extra_credit');
            $pacakgeId = $this->User->field('package_id');

            $reArr['service_id'] = $serviceId;
            $reArr['package_id'] = $pacakgeId;
            $reArr['extra_credit'] = $userExtraCredit;
            $reArr['extra_credit_eligible'] = false;
            $reArr['eligible'] = false;

            $serviceLimitInfo = $this->Serviceprice->find('first', array('conditions' => "Serviceprice.package_id = $pacakgeId AND Serviceprice.service_id = $serviceId"));

            $serviceLimit = 0;
            if ($serviceLimitInfo) {
                $serviceLimit = intval($serviceLimitInfo['Serviceprice']['servicelimit']);
            }
            $reArr['service_limit'] = $serviceLimit;

            $this->Service->id = $serviceId;
            $unitPrice = $this->Service->field('default_unit_price');
            $reArr['unit_price'] = $unitPrice;

            $cond = array(
                'conditions' => "Usercredit.user_id = $userId AND Usercredit.service_id = $serviceId"
            );
            $userServiceCredit = $this->Usercredit->find('first', $cond);
            $totalPrice = $unitPrice * $quantity;

            if (isset($userServiceCredit['Usercredit'])) {
                $extraUnit = intval($userServiceCredit['Usercredit']['extra_unit']);
                $serviceLimit = $serviceLimit + $extraUnit;
                $reArr['service_limit'] = $serviceLimit;

                $serviceUsed = intval($userServiceCredit['Usercredit']['service_used']);
                $reArr['service_used'] = $serviceUsed;
                $reArr['eligible_for'] = ($serviceLimit - $serviceUsed);
                $serviceUsed = ($serviceUsed + $quantity);
                if ($serviceUsed <= $serviceLimit) {
                    $reArr['eligible'] = true;
                } elseif ($userExtraCredit >= $totalPrice) {
                    $reArr['extra_credit_eligible'] = true;
                }
            } elseif ($userExtraCredit >= $totalPrice) {
                $reArr['extra_credit_eligible'] = true;
            }
            return $reArr;
        }

        function historydebited()
        {
            $userId = $this->Auth->user('id');
            $this->loadModel('Credithistory');
            $this->paginate = array(
                'conditions' => array(" Credithistory.user_id = $userId AND Credithistory.type = 'debit'"),
                'limit' => 1000,
                'recursive' => 1,
                'order' => 'Credithistory.id DESC'
            );
            $debitHistories = $this->paginate('Credithistory');
            $this->set(compact('debitHistories'));
            $this->set('title_for_layout', 'User Debit Histories');
        }

        function historycredited()
        {
            $userId = $this->Auth->user('id');
            $this->loadModel('Credithistory');
            $this->paginate = array(
                'conditions' => array(" Credithistory.user_id = $userId AND Credithistory.type = 'credit'"),
                'limit' => 10,
                'recursive' => 1,
                'order' => 'Credithistory.id DESC'
            );
            $creditHistories = $this->paginate('Credithistory');
            $this->set(compact('creditHistories'));
            $this->set('title_for_layout', 'User Credit Histories');
        }

        private function getMyPlanInfo()
        {

            $userId = $this->Auth->user('id');
            $this->loadModel('Service');
            $serviceList = $this->Service->find('all', array('order' => 'id'));

            $this->loadModel('Serviceprice');

            $this->User->id = $userId;
            $packageId = $this->User->field('package_id');

            $servicePrices = $this->Serviceprice->find('all', array('conditions' => "Serviceprice.package_id = $packageId"));

            //pr($servicePrices);

            $this->loadModel('Usercredit');
            $userCredits = $this->Usercredit->find('all', array('conditions' => "Usercredit.user_id = $userId AND Usercredit.status = 1"));

            if ($serviceList) {
                foreach ($serviceList as $key => $service) {
                    if ($servicePrices) {
                        foreach ($servicePrices as $price) {
                            if ($price['Serviceprice']['service_id'] == $service['Service']['id']) {
                                $serviceList[$key]['Service']['servicelimit'] = $price['Serviceprice']['servicelimit'];
                            }
                        }
                        if ($userCredits) {
                            foreach ($userCredits as $credit) {
                                if ($credit['Usercredit']['service_id'] == $service['Service']['id']) {
                                    $serviceList[$key]['Service']['service_used'] = $credit['Usercredit']['service_used'];
                                    $serviceList[$key]['Service']['extra_unit'] = $credit['Usercredit']['extra_unit'];
                                }
                            }
                        }
                    }
                }
            }

            return $serviceList;
        }

        //=================== gorilla tools =======================

        function newrankcampaign()
        {
            // get all the locales
            Configure::write('Cache.disable', true);
            $this->loadModel('Locale');
            $this->loadModel('Rankcampaign');

            $this->loadModel('Commonmodel');
            $this->loadModel('Rankreport');
            $this->loadModel('Authoritylab');
            $this->loadModel('Setting');

            $setting_info = $this->Setting->query('SELECT * FROM settings WHERE id = 1');
            $instant_price = @$setting_info[0]['settings']['instant_price'];
            $keyword_price = @$setting_info[0]['settings']['keyword_price'];
            $instant_price_for_client = @$setting_info[0]['settings']['instant_price_for_client'];

            $userId = $this->Auth->user('id');

            $serviceId = 14;
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {

                if ($this->request->is('post')) {

                    //pr($this->data);exit;
                    /** For Logo start * */
                    $extRes = array('jpg', 'jpeg', 'gif', 'png');
                    if ($this->data['image']['e_logo']['name'] != '') {
                        if ($this->data['image']['e_logo']['error'] == 0) {
                            if (in_array($this->fileExt($this->data['image']['e_logo']['name']), $extRes)) {
                                $folder = WWW_ROOT . 'branding';
                                $fileName = time() . '_' . stripslashes(preg_replace("~\s+~", "_", str_replace("'", "", $this->data['image']['e_logo']['name'])));
                                $ufile = $folder . '/' . $fileName;

                                $this->ImageResizer->resizeImage($this->data['image']['e_logo']['tmp_name'], array(
                                    'output' => $ufile,
                                    'ignoreSmallImages' => true,
                                    'maxHeight' => 300,
                                    'maxWidth' => 300
                                ));
                                //@move_uploaded_file($this->data['User']['logo']['tmp_name'], $ufile);
                                //$unfile = $folder . '/' . $ins['Rankcampaign']['e_logo'];
                                //@unlink($unfile);
                                $this->request->data['Rankcampaign']['e_logo'] = $fileName;
                            } // end if in_array
                            else {
                                $eMsg = 'File Not Supported';
                                $this->Session->setFlash($eMsg, 'flash_error');
                                $this->redirect($this->params['action']);
                            }
                        }
                    } // end picure upload
                    //pr($_POST); exit;
                    /** For Logo end * */
                    $this->request->data['Rankcampaign']['last_update'] = date('Y-m-d G:i:s');
                    $this->request->data['Rankcampaign']['user_id'] = $userId;
                    $this->Rankcampaign->set($this->data['Rankcampaign']);
                    //pr($this->data); die;
                    if ($this->Rankcampaign->validates()) {
                        $urls = preg_replace("~http:\/\/~", "", $this->data['Rankcampaign']['c_urls']);
                        $this->request->data['Rankcampaign']['c_urls'] = $urls;

                        $kds = explode("\n", trim($this->data['Rankcampaign']['c_keywords']));
                        $keywords = array();
                        foreach ($kds as $k) {
                            $ky = trim($k);
                            $ky = preg_replace("/\s\s+/", " ", $ky);
                            $ky = preg_replace("/[^A-Za-z0-9 ]/", "", $ky);
                            $keywords[] = $ky;

                            if (strlen($ky) >= 1) { //previously it was 1
                                // remove duplicates
                                if (!in_array($ky, $keywords))
                                    $keywords[] = $ky;
                            }
                        }

                        $uniqueKeywords = implode("\n", $keywords);
                        $this->request->data['Rankcampaign']['c_keywords'] = $uniqueKeywords;

                        $curls = explode("\n", trim($this->data['Rankcampaign']['c_urls']));
                        $urls = array();
                        foreach ($curls as $u) {
                            $ul1 = trim($u);
                            //$ul = str_replace("http://", "", $ul);
                            //$ul = str_replace("www.", "", $ul);
                            //$ul1 = explode("/", $ul);

                            if (!in_array(str_replace("www.", "", $ul1), $urls)) // remove duplicates
                                $urls[] = $ul1;
                        }
                        $uniqueUrls = implode("\n", $urls);
                        $this->request->data['Rankcampaign']['c_urls'] = $uniqueUrls;
                        //pr($this->data['Rankcampaign']);
                        if ($this->Rankcampaign->save($this->data['Rankcampaign'], false)) {

                            //for new instant logic start

                            if ($this->data['Rankcampaign']['instant_check'] == 1) {
                                // keywords
                                $kds = explode("\n", trim($this->data['Rankcampaign']['c_keywords']));

                                $keywords = array();
                                $kc = 1;
                                foreach ($kds as $k) {
                                    $ky = trim($k);
                                    $ky = preg_replace("/\s\s+/", " ", $ky);
                                    $ky = preg_replace("/[^A-Za-z0-9 ]/", "", $ky);
                                    if (strlen($ky) >= 1) {  ////previously it was 3
                                        // remove duplicates
                                        if (!in_array($ky, $keywords))
                                            $keywords[] = $ky;
                                    }
                                    if ($kc == 20)
                                        break;
                                    $kc++;
                                }
                                $curls = explode("\n", trim($this->data['Rankcampaign']['c_urls']));
                                $urls = array();
                                foreach ($curls as $u) {
                                    $ul1 = trim($u);
                                    //$ul = str_replace("http://", "", $ul);
                                    //$ul1 = explode("/", $ul);
                                    if (!in_array($ul1, $urls)) // remove duplicates
                                        $urls[] = $ul1;
                                }
                                $surls = serialize($urls); // for save to db
                                $se = 0;
                                if ($this->data['Rankcampaign']['google'] == 1) {
                                    $se++;
                                }
                                if ($this->data['Rankcampaign']['yahoo'] == 1) {
                                    $se++;
                                }
                                if ($this->data['Rankcampaign']['bing'] == 1) {
                                    $se++;
                                }

                                $totalAuthorityLabRequestNeed = count($keywords) * $se;
                                $havePerHourLimitAtAuthorityLab = 1;
                                //for instant deduct dori balance
                                $deduct_dori_balance = count($keywords) * $se * ($instant_price + $keyword_price / 30);
                                $this->Setting->query("UPDATE  `settings`  set `gorilla_credit` = `gorilla_credit` - $deduct_dori_balance WHERE id = 1");

                                /**/
                                if ($this->data['Rankcampaign']['amazon'] == 1) {
                                    $se++;
                                }
                                if ($this->data['Rankcampaign']['youtube'] == 1) {
                                    $se++;
                                }
                                /* */

                                $currentServiceLimit = floatval($userServiceCredit['service_limit']);
                                $serviceUsed = floatval($userServiceCredit['service_used']);
                                $serviceUsed = $serviceUsed + $totalAuthorityLabRequestNeed;
                                $unitPrice = floatval($userServiceCredit['unit_price']);
                                $totalPrice = $unitPrice * $totalAuthorityLabRequestNeed + count($keywords) * $se * ($instant_price_for_client);

                                if ($currentServiceLimit >= $serviceUsed || $userServiceCredit['extra_credit_eligible']) {

                                    $getLimit = $this->Commonmodel->curl_get_contents('https://api.authoritylabs.com/account/211.json?auth_token=zpN0Q1ZzCDxty3p3nnVk');

                                    $authorityLabLimit = json_decode($getLimit, true);
                                    if (is_array($authorityLabLimit)) {
                                        $postCount = $authorityLabLimit['user']['current_post_count'];
                                        if (($postCount + $totalAuthorityLabRequestNeed) >= 1000) {
                                            $havePerHourLimitAtAuthorityLab = 0;
                                        }
                                    }

                                    if ($havePerHourLimitAtAuthorityLab) { // authoritylab post count per hour is under 1000
                                        // database insertion for this keywords, urls etc
                                        //id 	user_id 	keyword 	url 	engine 	locale 	callbackurl 	position 	result 	google 	status 	yahoo 	bing 	updatetime
                                        $callback_url = "http://tracker.seonitro.com/rank/processRankCampaignCallback.php";
                                        $auth_token = "zpN0Q1ZzCDxty3p3nnVk";
                                        $cnt = 1;
                                        //$instantResId = array();
                                        $rid = $this->Auth->user('id') . '_' . time();

                                        // send email update to the user
                                        $emailoptions = array();
                                        $emailupdate = 0;
                                        if ($this->data['Rankcampaign']['email_update'] == 1) {
                                            $emailupdate = 1;
                                            //$img = '<img src="http://www.gorilla-tools.com/wp-content/uploads/2013/03/logo.png' . '" style="display:block">';

                                            $emailoptions['e_email'] = trim($this->data['Rankcampaign']['e_email']);
                                            $emailoptions['e_company'] = $this->data['Rankcampaign']['e_company'];
                                            $emailoptions['e_contact'] = $this->data['Rankcampaign']['e_contact'];
                                            $emailoptions['e_phone'] = $this->data['Rankcampaign']['e_phone'];
                                            $emailoptions['e_weburl'] = $this->data['Rankcampaign']['e_weburl'];
                                            $emailoptions['e_fmsg'] = $this->data['Rankcampaign']['e_fmsg'];
                                            $emailoptions['time'] = date('M j, Y, g:i a');
                                            //$emailoptions['img'] = $img;
                                        }
                                        $emailoption = serialize($emailoptions);

                                        $amYouIds = array();
                                        foreach ($keywords as $kd) {
                                            $kds = $kd;

                                            $insertData = array(
                                                'id' => '',
                                                'user_id' => $this->Auth->user('id'),
                                                'campaign_id' => $this->Rankcampaign->getLastInsertID(),
                                                'keyword' => $kd,
                                                'urls' => $surls,
                                                'engine' => 'google',
                                                'locale' => $this->data['Rankcampaign']['glocale'],
                                                'callback_url' => '',
                                                'position' => '',
                                                'update_time' => date('Y-m-d'),
                                                'status' => 'inprocess',
                                                'r_id' => $rid,
                                                'email_update' => $emailupdate,
                                                'email_option' => $emailoption,
                                                'e_send_status' => 0,
                                                'rank_date' => date('Y-m-d G:i:s'),
                                                'created' => date('Y-m-d G:i:s'),
                                                'modified' => date('Y-m-d G:i:s')
                                            );

                                            //pr($row); exit;
                                            if ($this->data['Rankcampaign']['google'] == 1) {
                                                $insertData['engine'] = 'google';
                                                $insertData['locale'] = $this->data['Rankcampaign']['glocale'];

                                                $this->Rankreport->create();
                                                $this->Rankreport->save($insertData, false);
                                                //pr($insertData); exit;
                                                $id = $this->Rankreport->id ? $this->Rankreport->id : $this->Rankreport->getLastInsertID();
                                                //array_push($instantResId, $this->Rankinstant->getLastInsertID());
                                                // set immediate post queue to authoritylab
                                                $gr = $this->Authoritylab->priorityPartnerKeyword("$kds", $auth_token, "google", $this->data['Rankcampaign']['glocale'], "false", $callback_url . '?id=' . $id);
                                            }
                                            if ($this->data['Rankcampaign']['yahoo'] == 1) {
                                                $insertData['engine'] = 'yahoo';
                                                $insertData['locale'] = $this->data['Rankcampaign']['ylocale'];

                                                $this->Rankreport->create();
                                                $this->Rankreport->save($insertData, false);
                                                $id = $this->Rankreport->id ? $this->Rankreport->id : $this->Rankreport->getLastInsertID();
                                                // array_push($instantResId, $this->Rankinstant->getLastInsertID());
                                                // set immediate post queue to authoritylab
                                                $yr = $this->Authoritylab->priorityPartnerKeyword("$kds", $auth_token, "yahoo", $this->data['Rankcampaign']['ylocale'], "false", $callback_url . '?id=' . $id);
                                            }

                                            if ($this->data['Rankcampaign']['bing'] == 1) {
                                                $insertData['engine'] = 'bing';
                                                $insertData['locale'] = $this->data['Rankcampaign']['blocale'];

                                                $this->Rankreport->create();
                                                $this->Rankreport->save($insertData, false);
                                                $id = $this->Rankreport->id ? $this->Rankreport->id : $this->Rankreport->getLastInsertID();
                                                // array_push($instantResId, $this->Rankinstant->getLastInsertID());
                                                // set immediate post queue to authoritylab
                                                $br = $this->Authoritylab->priorityPartnerKeyword("$kds", $auth_token, "bing", $this->data['Rankcampaign']['blocale'], "false", $callback_url . '?id=' . $id);
                                            }

                                            if ($this->data['Rankcampaign']['amazon'] == 1) {
                                                $insertData['engine'] = 'amazon';

                                                $this->Rankreport->create();
                                                $this->Rankreport->save($insertData, false);
                                                $amYouIds[] = $this->Rankreport->id ? $this->Rankreport->id : $this->Rankreport->getLastInsertID();
                                            }

                                            if ($this->data['Rankcampaign']['youtube'] == 1) {
                                                $insertData['engine'] = 'youtube';

                                                $this->Rankreport->create();
                                                $this->Rankreport->save($insertData, false);
                                                $amYouIds[] = $this->Rankreport->id ? $this->Rankreport->id : $this->Rankreport->getLastInsertID();
                                            }
                                            $cnt++;

                                            //pr($instantResId);
                                            $_SESSION['r_id'] = $rid;

                                            $updateAccount['user_id'] = $userId;
                                            $updateAccount['service_id'] = $serviceId;
                                            $updateAccount['amount'] = $totalPrice;
                                            $updateAccount['quantity'] = $totalAuthorityLabRequestNeed;
                                            $updateAccount['remarks'] = 'Credit deducted for Current Ranking(instant).';

                                            if ($this->updateAccountDebit($updateAccount)) {
                                                if (!$userServiceCredit['eligible'] && $userServiceCredit['extra_credit_eligible']) {
                                                    $this->User->id = $userId;
                                                    $newExtraCredit = ($userServiceCredit['extra_credit'] - $totalPrice);
                                                    $this->User->saveField('extra_credit', $newExtraCredit);
                                                }
                                                //$eMsg = 'Instant Ranking request accepted. Please wait for processing.';
                                                //$this->Session->setFlash($eMsg, 'flash_success');
                                                //$this->redirect(array('action' => 'rankresults'));
                                                //$this->redirect(array('action' => 'instantrankresults'));
                                            } else {
                                                $eMsg = 'Something went wrong while updating you account.';
                                                $this->Session->setFlash($eMsg, 'flash_error');
                                                $this->redirect($this->params['action']);
                                            }
                                        }
                                        if ($amYouIds) {
                                            $amYouString = implode(',', $amYouIds);
                                            $this->Rankreport->process_amazon_youtube($amYouString);
                                        }
                                    } else {
                                        $this->set('results', array());
                                        $this->set('urls', array());
                                    }
                                } else {
                                    $eMsg = 'You don\'t have enough credits for all the keywords.';
                                    $this->Session->setFlash($eMsg, 'flash_error');
                                    $this->redirect($this->params['action']);
                                }
                            }
                            //for new instant logic end
                            // set msg flash
                            $eMsg = 'Your Rank Campaign has been created successfully. We are currently fetching your rankings now. Please wait while we process.';
                            $this->Session->setFlash($eMsg, 'flash_success');
                            $this->redirect($this->params['action']);
                        } else {
                            $eMsg = 'Data could not be saved.';
                            $this->Session->setFlash($eMsg, 'flash_error');
                            $this->redirect($this->params['action']);
                        }
                    } else {
                        $this->generateError('Rankcampaign');
                    }
                } // is post
            }
            $locale = $this->Locale->find('all', array('conditions' => null, 'order' => 'desc ASC'));
            $glocale = $ylocale = $blocale = array();
            foreach ($locale as $lcl) {
                if ($lcl['Locale']['engine'] == 'google') {
                    $glocale[$lcl['Locale']['locale']] = $lcl['Locale']['desc'];
                }
                if ($lcl['Locale']['engine'] == 'yahoo') {
                    $ylocale[$lcl['Locale']['locale']] = $lcl['Locale']['desc'];
                }
                if ($lcl['Locale']['engine'] == 'bing') {
                    $blocale[$lcl['Locale']['locale']] = $lcl['Locale']['desc'];
                }
            }
            //pr($glocale);
            $this->set('glocale', $glocale);
            $this->set('ylocale', $ylocale);
            $this->set('blocale', $blocale);

            $this->data = $this->data;
            $this->set('data', $this->data);


            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Create New Rank Campaign');
        }

        // get instant result

        function credit_needed_for_current_ranking()
        {
            $credit_info = $this->credit_needed();
            $credit_needed = number_format($credit_info['credit_needed'], 2);
            $gorilla_credit = number_format($credit_info['gorilla_credit'], 2);

            if ($credit_needed > $gorilla_credit) {
                $add_balance = $credit_needed - $gorilla_credit;
                $subject = 'Current Ranking Credit Report for ' . date('M Y', time());
                //$body = 'Month: ' . date('M Y', time());
                $body = 'Dear Admin';
                $body .= '<br/>Your current working balance is - $' . $gorilla_credit . ' USD';
                $body .= '<br/>To cover all the campaign results system needs - $' . $credit_needed . ' USD';
                $body .= '<br/>Please adjust your balance by adding money ( ' . $credit_needed . ' - ' . $gorilla_credit . ' = ' . number_format($add_balance, 2) . ') to run the system smoothly.';
                $body .= '<br/>To do so please go to your account - <a href="http://pageoneengine.com/myaccount/" target="_blank">http://pageoneengine.com/myaccount/</a>';

                $this->sendEmail(array('abir@seonitro.com', 'dori@seonitro.com', 'darren@seonitro.com', 'julie@seonitro.com'), $subject, $body);
            }
            exit;
        }

        function editrankcampaign($cId = null)
        {

            Configure::write('Cache.disable', true);
            $this->loadModel('Rankcampaign');

            if (!$this->haveAuthToDoAction('Rankcampaign', $cId, 'user_id') || $cId == null) {
                $this->redirect(array('action' => 'rankresults'));
            }

            // get all the locales
            $this->loadModel('Locale');

            $userId = $this->Auth->user('id');

            if ($this->request->is('post')) {

                /** For Logo start * */
                $extRes = array('jpg', 'jpeg', 'gif', 'png');
                if ($this->data['image']['e_logo']['name'] != '') {
                    if ($this->data['image']['e_logo']['error'] == 0) {
                        if (in_array($this->fileExt($this->data['image']['e_logo']['name']), $extRes)) {
                            $folder = WWW_ROOT . 'branding';
                            $fileName = time() . '_' . stripslashes(preg_replace("~\s+~", "_", str_replace("'", "", $this->data['image']['e_logo']['name'])));
                            $ufile = $folder . '/' . $fileName;

                            $this->ImageResizer->resizeImage($this->data['image']['e_logo']['tmp_name'], array(
                                'output' => $ufile,
                                'ignoreSmallImages' => true,
                                'maxHeight' => 300,
                                'maxWidth' => 300
                            ));
                            //@move_uploaded_file($this->data['User']['logo']['tmp_name'], $ufile);
                            //$unfile = $folder . '/' . $ins['Rankcampaign']['e_logo'];
                            //@unlink($unfile);
                            $this->request->data['Rankcampaign']['e_logo'] = $fileName;
                        } // end if in_array
                        else {
                            $eMsg = 'File Not Supported';
                            $this->Session->setFlash($eMsg, 'flash_error');
                            $this->redirect($this->params['action'] . "/$cId");
                        }
                    }
                } // end picure upload
                //pr($_POST); exit;
                /** For Logo end * */
                $this->request->data['Rankcampaign']['id'] = $cId;
                $this->request->data['Rankcampaign']['last_update'] = date('Y-m-d G:i:s');
                $this->request->data['Rankcampaign']['user_id'] = $userId;
                $this->Rankcampaign->set($this->data['Rankcampaign']);
                //pr($this->data); die;
                //pr($this->data);
                //exit;
                if ($this->Rankcampaign->validates()) {
                    $urls = preg_replace("~http:\/\/~", "", $this->data['Rankcampaign']['c_urls']);
                    $this->request->data['Rankcampaign']['c_urls'] = $urls;

                    $kds = explode("\n", trim($this->data['Rankcampaign']['c_keywords']));
                    $keywords = array();
                    foreach ($kds as $k) {
                        $ky = trim($k);
                        $ky = preg_replace("/\s\s+/", " ", $ky);
                        //$ky = preg_replace("/[^A-Za-z0-9 ]/", "", $ky);
                        if (strlen($ky) >= 3) {
                            // remove duplicates
                            if (!in_array($ky, $keywords))
                                $keywords[] = $ky;
                        }
                    }
                    $uniqueKeywords = implode("\n", $keywords);
                    $this->request->data['Rankcampaign']['c_keywords'] = $uniqueKeywords;

                    $curls = explode("\n", trim($this->data['Rankcampaign']['c_urls']));
                    $urls = array();
                    foreach ($curls as $u) {
                        $ul1 = trim($u);
                        //$ul = str_replace("http://", "", $ul);
                        //$ul = str_replace("www.", "", $ul);
                        //$ul1 = explode("/", $ul);

                        if (!in_array(str_replace("www.", "", $ul1), $urls)) // remove duplicates
                            $urls[] = $ul1;
                    }
                    $uniqueUrls = implode("\n", $urls);
                    $this->request->data['Rankcampaign']['c_urls'] = $uniqueUrls;
                    //pr($this->data['Rankcampaign']);
                    if ($this->Rankcampaign->save($this->data['Rankcampaign'], false)) {
                        // set msg flash
                        $eMsg = 'Your Rank Campaign has been updated successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                        $this->redirect($this->params['action'] . "/$cId");
                    }// end if
                } else {
                    $this->generateError('Rankcampaign');
                }
            } // is post

            $locale = $this->Locale->find('all', array('conditions' => null, 'order' => 'desc ASC'));
            $glocale = $ylocale = $blocale = array();
            foreach ($locale as $lcl) {
                if ($lcl['Locale']['engine'] == 'google') {
                    $glocale[$lcl['Locale']['locale']] = $lcl['Locale']['desc'];
                }
                if ($lcl['Locale']['engine'] == 'yahoo') {
                    $ylocale[$lcl['Locale']['locale']] = $lcl['Locale']['desc'];
                }
                if ($lcl['Locale']['engine'] == 'bing') {
                    $blocale[$lcl['Locale']['locale']] = $lcl['Locale']['desc'];
                }
            }
            //pr($glocale);
            $this->set('glocale', $glocale);
            $this->set('ylocale', $ylocale);
            $this->set('blocale', $blocale);


            $data = $this->Rankcampaign->findById($cId);
            $this->data = $data;

            $this->set('data', $this->data);
            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Edit Rank Campaign');
        }

        // delete a rank campaign
        function deleterankcampaign($id)
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Rankcampaign');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Rankcampaign', $id, 'user_id')) {
                echo 'error';
                exit;
            }

            $this->Rankcampaign->id = $id;
            $userId = $this->Auth->user('id');
            $oData = $this->Rankcampaign->read();
            if ($this->Rankcampaign->delete($id, false)) {

                $this->loadModel('Rankreport');
                // delete all the anchor recored under this campaign
                $this->Rankreport->deleteall(array('Rankreport.campaign_id' => $id), false);
                $eMsg = 'Rank Campaign with all records has been deleted successfully!';
                // save this activity to log
                $this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'rankcampaign', $job = 'deleted rank campaign  - ' . $oData['Rankcampaign']['campaign_name']);
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        // Current ranking activation change
        function activation_change($id, $status)
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Rankcampaign');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Rankcampaign', $id, 'user_id')) {
                echo 'error';
                exit;
            }
            //$status == 1 ? '0' : '1';
            if ($status == 1) {
                $status = 0;
                $eMsg = 'This campaign has been successfully inactivated!';
            } else {
                $eMsg = 'This campaign has been successfully activated!';
                $status = 1;
            }
            $this->Rankcampaign->query("UPDATE `rankcampaigns` SET `status` = '$status' WHERE `id` =$id");
            $this->Session->setFlash($eMsg, 'flash_success');
        }

        /*
        function instantranking() {
        $userId = $this->Auth->user('id');
        $this->User->id = $userId;
        $userdata = $this->User->read();
        $this->set(compact('userdata'));

        $this->loadModel('Locale');
        $this->loadModel('Rankinstant');
        $this->loadModel('Authoritylab');
        $this->loadModel('Commonmodel');

        $locale = $this->Locale->find('all', array('conditions' => null, 'order' => 'desc ASC'));
        $glocale = $ylocale = $blocale = array();
        foreach ($locale as $lcl) {
        if ($lcl['Locale']['engine'] == 'google') {
        $glocale[$lcl['Locale']['locale']] = $lcl['Locale']['desc'];
        }
        if ($lcl['Locale']['engine'] == 'yahoo') {
        $ylocale[$lcl['Locale']['locale']] = $lcl['Locale']['desc'];
        }
        if ($lcl['Locale']['engine'] == 'bing') {
        $blocale[$lcl['Locale']['locale']] = $lcl['Locale']['desc'];
        }
        }
        //pr($glocale);
        $this->set('glocale', $glocale);
        $this->set('ylocale', $ylocale);
        $this->set('blocale', $blocale);

        $urls = $results = array();
        if ($this->request->is("post")) {

        //pr($this->data);
        //exit;
        $this->Rankinstant->set($this->data['Rankinstant']);
        if ($this->Rankinstant->validates()) {
        // keywords
        $kds = explode("\n", trim($this->data['Rankinstant']['c_keywords']));
        $keywords = array();
        $kc = 1;
        foreach ($kds as $k) {
        $ky = trim($k);
        $ky = preg_replace("/\s\s+/", " ", $ky);
        $ky = preg_replace("/[^A-Za-z0-9 ]/", "", $ky);
        if (strlen($ky) >= 3) {
        // remove duplicates
        if (!in_array($ky, $keywords))
        $keywords[] = $ky;
        }
        if ($kc == 20)
        break;
        $kc++;
        }
        $curls = explode("\n", trim($this->data['Rankinstant']['c_urls']));
        $urls = array();
        foreach ($curls as $u) {
        $ul1 = trim($u);
        //$ul = str_replace("http://", "", $ul);
        //$ul1 = explode("/", $ul);
        if (!in_array($ul1, $urls)) // remove duplicates
        $urls[] = $ul1;
        }
        $surls = serialize($urls); // for save to db
        $se = 0;
        if ($this->data['Rankinstant']['google'] == 1) {
        $se++;
        }
        if ($this->data['Rankinstant']['yahoo'] == 1) {
        $se++;
        }
        if ($this->data['Rankinstant']['bing'] == 1) {
        $se++;
        }
        if ($this->data['Rankinstant']['amazon'] == 1) {
        $se++;
        }
        if ($this->data['Rankinstant']['youtube'] == 1) {
        $se++;
        }

        $totalAuthorityLabRequestNeed = count($keywords) * $se;
        $havePerHourLimitAtAuthorityLab = 1;

        $getLimit = $this->Commonmodel->curl_get_contents('https://api.authoritylabs.com/account/211.json?auth_token=zpN0Q1ZzCDxty3p3nnVk');

        $authorityLabLimit = json_decode($getLimit, true);
        if (is_array($authorityLabLimit)) {
        $postCount = $authorityLabLimit['user']['current_post_count'];
        if (($postCount + $totalAuthorityLabRequestNeed) >= 1000) {
        $havePerHourLimitAtAuthorityLab = 0;
        }
        }

        if ($havePerHourLimitAtAuthorityLab) { // authoritylab post count per hour is under 1000
        // database insertion for this keywords, urls etc
        //id 	user_id 	keyword 	url 	engine 	locale 	callbackurl 	position 	result 	google 	status 	yahoo 	bing 	updatetime
        $callback_url = "http://tracker.seonitro.com/rank/processRankInstant.php";
        $auth_token = "zpN0Q1ZzCDxty3p3nnVk";
        $cnt = 1;
        //$instantResId = array();
        $rid = $this->Auth->user('id') . '_' . time();

        // send email update to the user
        $emailoptions = array();
        $emailupdate = 0;
        if ($this->data['Rankinstant']['email_update'] == 1) {
        $emailupdate = 1;
        //$img = '<img src="http://www.gorilla-tools.com/wp-content/uploads/2013/03/logo.png' . '" style="display:block">';

        $emailoptions['e_email'] = trim($this->data['Rankinstant']['e_email']);
        $emailoptions['e_company'] = $this->data['Rankinstant']['e_company'];
        $emailoptions['e_contact'] = $this->data['Rankinstant']['e_contact'];
        $emailoptions['e_phone'] = $this->data['Rankinstant']['e_phone'];
        $emailoptions['e_weburl'] = $this->data['Rankinstant']['e_weburl'];
        $emailoptions['e_fmsg'] = $this->data['Rankinstant']['e_fmsg'];
        $emailoptions['time'] = date('M j, Y, g:i a');
        //$emailoptions['img'] = $img;
        }
        $emailoption = serialize($emailoptions);

        $amYouIds = array();
        foreach ($keywords as $kd) {
        $kds = $kd;

        $insertData = array(
        'id' => '',
        'user_id' => $this->Auth->user('id'),
        'keyword' => $kd,
        'url' => $surls,
        'engine' => 'google',
        'locale' => $this->data['Rankinstant']['glocale'],
        'callback_url' => '',
        'position' => '',
        'update_time' => date('Y-m-d'),
        'status' => 'inprocess',
        'r_id' => $rid,
        'email_update' => $emailupdate,
        'email_option' => $emailoption,
        'e_send_status' => 0
        );

        $serviceId = 15;
        $userServiceCredit = $this->checkUserServiceLimit($serviceId);

        if ($userServiceCredit['eligible'] || $userServiceCredit['extra_credit_eligible']) {

        $currentServiceLimit = floatval($userServiceCredit['service_limit']);
        $serviceUsed = floatval($userServiceCredit['service_used']);
        $serviceUsed = $serviceUsed + $totalAuthorityLabRequestNeed;
        $unitPrice = floatval($userServiceCredit['unit_price']);
        $totalPrice = $unitPrice * $totalAuthorityLabRequestNeed;

        if ($currentServiceLimit >= $serviceUsed || $userServiceCredit['extra_credit_eligible']) {

        if ($this->data['Rankinstant']['google'] == 1) {
        $insertData['engine'] = 'google';
        $insertData['locale'] = $this->data['Rankinstant']['glocale'];

        $this->Rankinstant->create();
        $this->Rankinstant->save($insertData, false);
        $id = $this->Rankinstant->id ? $this->Rankinstant->id : $this->Rankinstant->getLastInsertID();
        //array_push($instantResId, $this->Rankinstant->getLastInsertID());
        // set immediate post queue to authoritylab
        $gr = $this->Authoritylab->priorityPartnerKeyword("$kds", $auth_token, "google", $this->data['Rankinstant']['glocale'], "false", $callback_url . '?id=' . $id);
        }
        if ($this->data['Rankinstant']['yahoo'] == 1) {
        $insertData['engine'] = 'yahoo';
        $insertData['locale'] = $this->data['Rankinstant']['ylocale'];

        $this->Rankinstant->create();
        $this->Rankinstant->save($insertData, false);
        $id = $this->Rankinstant->id ? $this->Rankinstant->id : $this->Rankinstant->getLastInsertID();
        // array_push($instantResId, $this->Rankinstant->getLastInsertID());
        // set immediate post queue to authoritylab
        $yr = $this->Authoritylab->priorityPartnerKeyword("$kds", $auth_token, "yahoo", $this->data['Rankinstant']['ylocale'], "false", $callback_url . '?id=' . $id);
        }

        if ($this->data['Rankinstant']['bing'] == 1) {
        $insertData['engine'] = 'bing';
        $insertData['locale'] = $this->data['Rankinstant']['blocale'];

        $this->Rankinstant->create();
        $this->Rankinstant->save($insertData, false);
        $id = $this->Rankinstant->id ? $this->Rankinstant->id : $this->Rankinstant->getLastInsertID();
        // array_push($instantResId, $this->Rankinstant->getLastInsertID());
        // set immediate post queue to authoritylab
        $br = $this->Authoritylab->priorityPartnerKeyword("$kds", $auth_token, "bing", $this->data['Rankinstant']['blocale'], "false", $callback_url . '?id=' . $id);
        }

        if ($this->data['Rankinstant']['amazon'] == 1) {
        $insertData['engine'] = 'amazon';

        $this->Rankinstant->create();
        $this->Rankinstant->save($insertData, false);
        $amYouIds[] = $this->Rankinstant->id ? $this->Rankinstant->id : $this->Rankinstant->getLastInsertID();
        }

        if ($this->data['Rankinstant']['youtube'] == 1) {
        $insertData['engine'] = 'youtube';

        $this->Rankinstant->create();
        $this->Rankinstant->save($insertData, false);
        $amYouIds[] = $this->Rankinstant->id ? $this->Rankinstant->id : $this->Rankinstant->getLastInsertID();
        }
        $cnt++;
        }
        if ($amYouIds) {
        $amYouString = implode(',', $amYouIds);
        $this->Rankinstant->process_amazon_youtube($amYouString);
        }
        //pr($instantResId);
        $_SESSION['r_id'] = $rid;

        $updateAccount['user_id'] = $userId;
        $updateAccount['service_id'] = $serviceId;
        $updateAccount['amount'] = $totalPrice;
        $updateAccount['quantity'] = $totalAuthorityLabRequestNeed;
        $updateAccount['remarks'] = 'Credit deducted for Current Ranking.';

        if ($this->updateAccountDebit($updateAccount)) {
        if (!$userServiceCredit['eligible'] && $userServiceCredit['extra_credit_eligible']) {
        $this->User->id = $userId;
        $newExtraCredit = ($userServiceCredit['extra_credit'] - $totalPrice);
        $this->User->saveField('extra_credit', $newExtraCredit);
        }
        $eMsg = 'Instant Ranking request accepted. Please wait for processing.';
        $this->Session->setFlash($eMsg, 'flash_success');
        $this->redirect(array('action' => 'instantrankresults'));
        } else {
        $eMsg = 'Something went wrong while updating you account.';
        $this->Session->setFlash($eMsg, 'flash_error');
        $this->redirect($this->params['action']);
        }
        } else {
        $eMsg = 'You have reached your limit.';
        $this->Session->setFlash($eMsg, 'flash_error');
        $this->redirect($this->params['action']);
        }
        }
        } else {
        $this->set('results', array());
        $this->set('urls', array());
        }
        } else {
        $this->generateError('Rankinstant');
        }
        } // end if post

        $this->set('navIndex', '2,0');
        $this->set('title_for_layout', 'Instant Keyword Track Report');
        }
        */

        // show all the instant results
        function instantrankresults()
        {
            $this->loadModel('Rankinstant');
            $instantRanks = $this->Rankinstant->find('all', array('recursive' => -1,
                'conditions' => array('Rankinstant.user_id = ' . $this->Auth->user('id')),
                'order' => ' id desc ',
            ));
            //pr($instantRanks);
            //exit;
            $this->set(compact('instantRanks'));

            //$this->User->id = $this->Auth->user('id');
            //$userdata = $this->User->read();

            if (isset($_SESSION['rid'])) {
                if (!empty($_SESSION['rid'])) {
                    $inprocess = $this->Rankinstant->find('count', array('recursive' => -1,
                        'conditions' => array("status = 'inprocess'"),
                    ));
                    if ($inprocess) {
                        $this->set('refreshpage', 'yes');
                    } else {
                        $this->set('refreshpage', 'no');
                        $_SESSION['rid'] = null;
                        unset($_SESSION['rid']);
                    }
                }
            }

            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Instant Keyword Tracking Reports');
        }

        // all result of existing campaigns
        function rankresults()
        {
            $this->loadModel('Rankcampaign');
            $this->paginate = array(
                'conditions' => array('Rankcampaign.user_id' => $this->Auth->user('id')), //array of conditions
                'recursive' => 1,
                'limit' => 200,
                'order' => 'Rankcampaign.created'
            );

            $rankCampaigns = $this->paginate('Rankcampaign');

            //pr($rankCampaigns);
            //exit;
            $this->set(compact('rankCampaigns'));

            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Regular Campaign Reporting  ');
        }

        function viewrankresult($campId = 0)
        {

            App::uses('Rankcampaign', 'Model');
            $Rankcampaign = new Rankcampaign();

            $allResult = array();

            //get campaing details
            $param = array(
                'conditions' => array(
                    'Rankcampaign.id' => $campId
                ),
                'recursive' => -1,
            );

            $camp = $Rankcampaign->find('first', $param);
            if ($camp) {
                $keywords = explode("\n", $camp['Rankcampaign']['c_keywords']);
                $urls = explode("\n", $camp['Rankcampaign']['c_urls']);

                //pr($keywords);
                //pr($urls);

                $allResult = array();
                $kdArr = array();
                $fromdate = date("Y-m-d", strtotime("-1 month"));
                $todate = date("Y-m-d");

                App::uses('Authoritylab', 'Model');
                $Authoritylab = new Authoritylab();

                if ($camp['Rankcampaign']['google'] == 1) {
                    $chunkResult = $Authoritylab->getSerachEngineResult($campId, $keywords, 'google', $urls);
                    $allResult['Google']['EngineResult'] = $chunkResult['EngineResult'];
                    $allResult['Google']['GraphResult'] = $chunkResult['GraphResult'];
                }
                if ($camp['Rankcampaign']['yahoo'] == 1) {
                    $chunkResult = $Authoritylab->getSerachEngineResult($campId, $keywords, 'yahoo', $urls);
                    $allResult['Yahoo']['EngineResult'] = $chunkResult['EngineResult'];
                    $allResult['Yahoo']['GraphResult'] = $chunkResult['GraphResult'];
                }
                if ($camp['Rankcampaign']['bing'] == 1) {
                    $chunkResult = $Authoritylab->getSerachEngineResult($campId, $keywords, 'bing', $urls);
                    $allResult['Bing']['EngineResult'] = $chunkResult['EngineResult'];
                    $allResult['Bing']['GraphResult'] = $chunkResult['GraphResult'];
                }
                if ($camp['Rankcampaign']['amazon'] == 1) {
                    $chunkResult = $Authoritylab->getSerachEngineResult($campId, $keywords, 'amazon', $urls);
                    $allResult['Amazon']['EngineResult'] = $chunkResult['EngineResult'];
                    $allResult['Amazon']['GraphResult'] = $chunkResult['GraphResult'];
                }
                if ($camp['Rankcampaign']['youtube'] == 1) {
                    $chunkResult = $Authoritylab->getSerachEngineResult($campId, $keywords, 'youtube', $urls);
                    $allResult['Youtube']['EngineResult'] = $chunkResult['EngineResult'];
                    $allResult['Youtube']['GraphResult'] = $chunkResult['GraphResult'];
                }
            }
            $this->set('report', $allResult);
            //pr($allResult);
            //exit;
            $this->set('camp', $camp);
            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Rank Campaign Report');
        }

        function rankresultsmore($cId, $sEngine, $url, $keyword)
        {
            if ($cId && $sEngine && $url && $keyword) {

                $this->loadModel('Rankcampaign');
                $this->Rankcampaign->id = $cId;

                $campaignInfo = $this->Rankcampaign->find('first', array('fields' => 'id,campaign_name', 'conditions' => "Rankcampaign.id = $cId"));
                $this->set(compact('campaignInfo'));

                $keyword = base64_decode($keyword);
                $url = base64_decode($url);
                $url = trim('http://' . str_replace(array('http://', 'https://'), '', $url));

                $this->loadModel('Rankreport');
                $con = array(
                    'conditions' => "Rankreport.campaign_id=$cId AND Rankreport.engine = '$sEngine' AND Rankreport.keyword = '$keyword'",
                    'limit' => 30,
                    'order' => 'Rankreport.rank_date DESC'
                );
                $keywordReport = $this->Rankreport->find('all', $con);

                $rankKeyword = array();
                if ($keywordReport) {
                    foreach ($keywordReport as $key => $rK) {
                        $position = unserialize($rK['Rankreport']['position']);
                        if ($position) {
                            foreach ($position as $key => $pos) {
                                if ($url == trim($key) || (stripos($url, $key) !== false) || (stripos($key, $url) != false)) {
                                    //if ($key == $url) {
                                    if (isset($pos[4]) && $pos[2] != '') {
                                        $temp['rank_url'] = $pos[4];
                                        $temp['rank_page'] = $pos[1];
                                        $temp['rank_position'] = $pos[0];
                                        $temp['rank_title'] = $pos[2];
                                        $temp['rank_description'] = $pos[3];
                                        $temp['rank_date'] = isset($rK['Rankreport']['rank_date']) ? $rK['Rankreport']['rank_date'] : '';
                                        $rankKeyword[] = $temp;
                                    }
                                }
                            }
                        }
                    }
                }
                $this->set(compact('keyword'));
                $this->set(compact('rankKeyword'));
            } else {
                $this->redirect(array('action' => 'rankresults'));
            }
        }

        function ranktrends($id = null, $engine = '', $keyword = '', $url = '')
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = '';
            App::uses('Rankreport', 'Model');
            $modelRankreport = new Rankreport();
            $results = $modelRankreport->find('all', array(
                'fields' => ' position, rank_date ',
                'conditions' => array(" Rankreport.rank_date != '0000-00-00' AND Rankreport.keyword = '" . $keyword . "' AND engine = '$engine' AND Rankreport.campaign_id = " . $id),
                'recursive' => -1,
                'order' => 'Rankreport.id DESC',
                'limit' => 30)
            );
            //pr($results);
            $url = base64_decode($url);
            //$url = 'http://' . str_replace(array('http://', 'https://'), '', $url);
            $this->set(compact('results'));
            $this->set('k', $keyword);
            $this->set('engine', $engine);
            $this->set('url', $url);
            return $this->render('ranktrends');
        }

        function articlepreview()
        {
            $this->layout = 'ajax';
            if ($this->request->is('post')) {
                $article['title'] = $this->request->data['post_title'];
                $article['content'] = $this->request->data['post_content'];

                $this->set(compact('article'));
            }
        }

        /*     * *************************** FOR PAGE RANK MODULE START ****************** */

        function pr_campaign_create()
        {

            $this->loadModel('Prcampaign');
            $this->loadModel('Pruserdomain');


            $userId = $this->Auth->user('id');

            $serviceId = 19;  // Domain Digger
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from you extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {
                $unitPrice = floatval($userServiceCredit['unit_price']);

                if ($this->request->is('post')) {
                    $this->request->data['Prcampaign']['name'] = $this->request->data['Prcampaign']['name'];
                    $this->request->data['Prcampaign']['created_at'] = date('Y-m-d G:i:s');
                    $this->request->data['Prcampaign']['user_id'] = $userId;
                    $this->Prcampaign->set($this->data['Prcampaign']);
                    //pr($this->data); die;
                    if ($this->Prcampaign->validates()) {
                        $urls = preg_replace("~http:\/\/~", "", $this->data['Prcampaign']['c_urls']);
                        $this->request->data['Prcampaign']['c_urls'] = $urls;
                        $curls = explode("\n", trim($this->data['Prcampaign']['c_urls']));
                        //pr($curls); exit;
                        $this->request->data['Prcampaign']['no_of_domain'] = count($curls);
                        if ($this->Prcampaign->save($this->data['Prcampaign'], false)) {
                            $this->request->data['Pruserdomain']['user_id'] = $userId;
                            $this->request->data['Pruserdomain']['pr_campaigns_id'] = $this->Prcampaign->getLastInsertId();

                            // $this->loadModel('GoogePageRank');
                            //  $this->loadModel('Majesticseo');
                            //  $this->loadModel('Jsonapi');


                            $urls = array();
                            $admin_ip = $this->admin_ip();
                            $domain_ar = array();
                            foreach ($curls as $u) {
                                $ul = trim($u);
                                $ul = str_replace("http://", "", $ul);
                                //$ul = str_replace("www.", "", $ul);
                                $ul1 = explode("/", $ul);

                                //if (!in_array(str_replace("www.", "", $ul1[0]), $urls)) // remove duplicates
                                if (!in_array($ul1[0], $urls)) // remove duplicates
                                    $urls[] = $ul1[0];
                                if (!in_array($ul1[0], $domain_ar)) {
                                    $domain_ar[] = $ul1[0];
                                    $proxyIp = $admin_ip['ip'];
                                    $proxy_user_pass = $admin_ip['password'];
                                    $pu = $admin_ip['username'] . ':' . $admin_ip['password'];
                                    $proxy_port = $admin_ip['port'];

                                    //$d = serialize(array($ul1[0]));
                                    //$prIndex = @array_shift(unserialize(file_get_contents("http://alltest.seonitro.com/get_pr_index.php?doms=" . $d)));
                                    $page_rank = 'NULL';// isset($prIndex[0]) ? $prIndex[0] : 'NULL';;
                                    $indexed = 'NULL';// isset($prIndex[1]) ? $prIndex[1] : 'NULL';
                                    $created_date = 'NULL';
                                    $expires_date = 'NULL';
                                    $all_name_server = 'NULL';
                                    $age = 'NULL';

                                    /*$majesticseo = @$this->Majesticseo->GetIndexItemInfo($ul1[0]);
                                    $whoisProperties = @$this->Jsonapi->getWhoisData(array("domain" => str_replace("www.", "", $ul1[0])));
                                    if (isset($whoisProperties->code) && ($whoisProperties->code != 200)) {
                                    $this->request->data['Pruserdomain']['created'] = $created_date;
                                    $this->request->data['Pruserdomain']['expires'] = $expires_date;
                                    $this->request->data['Pruserdomain']['age'] = $age;
                                    $this->request->data['Pruserdomain']['name_server'] = $all_name_server;
                                    } else {
                                    ///*** For all name server start
                                    $created_date = @date('Y-m-d', strtotime($whoisProperties->body->created_on));
                                    $age = @date('Y', strtotime($whoisProperties->body->created_on));
                                    $expires_date = @date('Y-m-d', strtotime($whoisProperties->body->expires_on));
                                    $dns1 = @$whoisProperties->body->nameservers[0]->name;
                                    $dns2 = @$whoisProperties->body->nameservers[1]->name;
                                    $all_name_server = $dns1 . ',' . $dns2;
                                    $this->request->data['Pruserdomain']['created'] = $created_date;
                                    $this->request->data['Pruserdomain']['expires'] = $expires_date;
                                    $this->request->data['Pruserdomain']['name_server'] = $all_name_server;
                                    $this->request->data['Pruserdomain']['age'] = $age;
                                    }*/





                                    /** *********** Semoz Rank start ************** */
                                    /* App::import('Vendor', 'seomoz_vendor/seomoz');
                                    $obj = new seomoz();
                                    $seomozrank = $obj->getData($ul1[0]);
                                    $seomozrank = $seomozrank['seomozrank'];

                                    //*** *********** Semoz Rank end ************** 
                                    // Update PR data instantly end

                                    $this->request->data['Pruserdomain']['domain'] = $ul1[0];
                                    $this->request->data['Pruserdomain']['page_rank'] = $page_rank;

                                    $this->request->data['Pruserdomain']['seomoz_rank'] = $seomozrank;
                                    $this->request->data['Pruserdomain']['ext_back_links'] = $majesticseo['row'][4];
                                    $this->request->data['Pruserdomain']['indexed'] = $indexed;
                                    $this->request->data['Pruserdomain']['citation_flow'] = $majesticseo['row'][33];
                                    $this->request->data['Pruserdomain']['trust_flow'] = $majesticseo['row'][34];*/
                                    $this->request->data['Pruserdomain']['created_at'] = date('Y-m-d G:i:s');
                                    $this->request->data['Pruserdomain']['updated_at'] = date('Y-m-d G:i:s');
                                    $this->request->data['Pruserdomain']['domain'] = $ul1[0];
                                    $this->Pruserdomain->create();
                                    $this->Pruserdomain->save($this->data['Pruserdomain']);
                                }
                            }

                            //pr($this->params);
                            $eMsg = 'Your Page Rank Campaign has been created successfully. Information will be updated soon';
                            $this->Session->setFlash($eMsg, 'flash_success');
                            //echo '<script>document.location.href = "pr_campaign_list";</script>';
                            $this->redirect(array('action' => 'pr_campaign_create'));
                        }// end if
                    } else {
                        $this->generateError('Prcampaign');
                    }
                } // is post


                $this->data = $this->data;
                $this->set('data', $this->data);
            } // not eligible

            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Create New Page Rank Campaign');
        }

        function pr_campaign_edit($cId = null)
        {
            $this->loadModel('Prcampaign');
            $this->loadModel('Pruserdomain');
            if (!$this->haveAuthToDoAction('Prcampaign', $cId, 'user_id') || $cId == null) {
                $this->redirect(array('action' => 'pr_campaign_list'));
            }

            $userId = $this->Auth->user('id');

            if ($this->request->is('post')) {
                $this->request->data['Prcampaign']['id'] = $cId;
                $this->request->data['Prcampaign']['created_at'] = date('Y-m-d G:i:s');
                $this->request->data['Prcampaign']['user_id'] = $userId;
                $this->Prcampaign->set($this->data['Prcampaign']);
                //pr($this->data); die;
                if ($this->Prcampaign->validates()) {
                    $urls = preg_replace("~http:\/\/~", "", $this->data['Prcampaign']['c_urls']);
                    $this->request->data['Prcampaign']['c_urls'] = $urls;
                    $curls = explode("\n", trim($this->data['Prcampaign']['c_urls']));

                    $this->request->data['Prcampaign']['no_of_domain'] = count($curls);
                    if ($this->Prcampaign->save($this->data['Prcampaign'], false)) {
                        $this->request->data['Pruserdomain']['user_id'] = $userId;
                        $this->request->data['Pruserdomain']['pr_campaigns_id'] = $cId;
                        $this->request->data['Pruserdomain']['created_at'] = date('Y-m-d G:i:s');

                        $this->loadModel('GoogePageRank');
                        $this->loadModel('Majesticseo');
                        $admin_ip = $this->admin_ip();
                        $urls = array();

                        $this->Pruserdomain->deleteall(array('Pruserdomain.pr_campaigns_id' => $cId), false);

                        $domain_ar = array();
                        foreach ($curls as $u) {
                            $ul = trim($u);
                            $ul = str_replace("http://", "", $ul);
                            //$ul = str_replace("www.", "", $ul);
                            $ul1 = explode("/", $ul);

                            //if (!in_array(str_replace("www.", "", $ul1[0]), $urls)) // remove duplicates
                            if (!in_array($ul1[0], $urls)) // remove duplicates
                                $urls[] = $ul1[0];

                            // for edit start
                            if (!in_array($ul1[0], $domain_ar)) {
                                $domain_ar[] = $ul1[0];


                                /*$proxyIp = $admin_ip['ip'];
                                $proxy_user_pass = $admin_ip['password'];
                                $proxy_port = $admin_ip['port'];
                                $page_rank = $this->GoogePageRank->getPageRank($url = $ul1[0], $proxyIp, $proxy_user_pass, $proxy_port);
                                $indexed = $this->GoogePageRank->checkIndex($url = $ul1[0], $proxyIp, $proxy_user_pass, $proxy_port);

                                $majesticseo = $this->Majesticseo->GetIndexItemInfo($ul1[0]);*/

                                /*App::import('Vendor', 'seomoz_vendor/seomoz');
                                $obj = new seomoz();
                                $seomozrank = $obj->getData($ul1[0]);
                                $seomozrank = $seomozrank['seomozrank'];

                                $this->request->data['Pruserdomain']['domain'] = $ul1[0];
                                $this->request->data['Pruserdomain']['page_rank'] = $page_rank;

                                $this->request->data['Pruserdomain']['seomoz_rank'] = $seomozrank;
                                $this->request->data['Pruserdomain']['ext_back_links'] = $majesticseo['row'][4];
                                $this->request->data['Pruserdomain']['indexed'] = $indexed;
                                $this->request->data['Pruserdomain']['citation_flow'] = $majesticseo['row'][33];
                                $this->request->data['Pruserdomain']['trust_flow'] = $majesticseo['row'][34];
                                $this->request->data['Pruserdomain']['created_at'] = date('Y-m-d G:i:s');
                                $this->request->data['Pruserdomain']['updated_at'] = date('Y-m-d G:i:s');*/
                                // for edit end

                                $this->request->data['Pruserdomain']['domain'] = $ul1[0];
                                $this->Pruserdomain->create();
                                $this->Pruserdomain->save($this->data['Pruserdomain']);
                            }
                        }

                        $eMsg = 'Your Page Rank Campaign has been updated successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                        echo '<script>document.location.href = "";</script>';
                        exit;
                        //$this->redirect($this->params['action'] . "/$cId");
                    }// end if
                } else {
                    $this->generateError('Prcampaign');
                }
            } // is post

            $this->data = $this->Prcampaign->findById($cId);
            // pr($this->all_domain($cId));exit;
            $this->request->data['Prcampaign']['c_urls'] = implode("\n", $this->all_domain($cId));
            $this->set('data', $this->data);

            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Update New Page Rank Campaign');
        }

        function pr_campaign_list()
        {
            $this->loadModel('Prcampaign');
            $this->paginate = array(
                'conditions' => array('Prcampaign.user_id' => $this->Auth->user('id')), //array of conditions
                'order' => 'Prcampaign.id DESC',
                'recursive' => 1,
                'limit' => 200
            );

            $page_rank_campaign_list = $this->paginate('Prcampaign');

            $this->set(compact('page_rank_campaign_list'));

            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Page Rank Campaign List');
        }

        function pr_campaign_view($cId = null)
        {
            $this->loadModel('Prcampaign');
            $this->loadModel('Pruserdomain');

            if (!$this->haveAuthToDoAction('Prcampaign', $cId, 'user_id') || $cId == null) {
                $this->redirect(array('action' => 'pr_campaign_list'));
            }

            $this->paginate = array(
                'conditions' => array('Pruserdomain.pr_campaigns_id' => $cId), //array of conditions
                'recursive' => 1,
                'limit' => 200
            );

            $page_rank_campaign_domain_list = $this->paginate('Pruserdomain');

            $this->paginate = array(
                'conditions' => array('Pruserdomain.pr_campaigns_id' => $cId), //array of conditions
                'fields' => 'domain,page_rank,indexed,created,expires,name_server,seomoz_rank,citation_flow,trust_flow,ext_back_links,created_at,updated_at',
                'recursive' => 1,
                'limit' => 200
            );

            $_SESSION['csv_data'] = $this->paginate('Pruserdomain');

            $this->set(compact('page_rank_campaign_domain_list'));

            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'View Page Rank Campaign');
        }

        function pr_campaign_delete($id)
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Prcampaign');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Prcampaign', $id, 'user_id')) {
                echo 'error';
                exit;
            }

            $this->Prcampaign->id = $id;
            #$userId = $this->Auth->user('id');
            $oData = $this->Prcampaign->read();
            if ($this->Prcampaign->delete($id, false)) {

                $this->loadModel('Pruserdomain');
                // delete all the anchor recored under this campaign
                $this->Pruserdomain->deleteall(array('Pruserdomain.pr_campaigns_id' => $id), false);
                $eMsg = 'Page rank campaign has been deleted successfully with their domain!';
                // save this activity to log
                //$this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'rankcampaign', $job = 'deleted rank campaign  - ' . $oData['Rankcampaign']['campaign_name']);
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        function pr_campaign_delete_domain($id)
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Prcampaign');
            $this->loadModel('Pruserdomain');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Pruserdomain', $id, 'user_id')) {
                echo 'error';
                exit;
            }

            $this->Pruserdomain->id = $id;
            #$userId = $this->Auth->user('id');
            $oData = $this->Pruserdomain->read();

            $this->Prcampaign->id = $oData['Pruserdomain']['pr_campaigns_id'];
            $Prcampaign_data = $this->Prcampaign->read();

            /*         * **** Reduce number of domain count and remove this domain from c_urls column start ***** */
            $this->request->data['Prcampaign']['id'] = $oData['Pruserdomain']['pr_campaigns_id'];
            $this->request->data['Prcampaign']['no_of_domain'] = $Prcampaign_data['Prcampaign']['no_of_domain'] - 1;
            $this->request->data['Prcampaign']['c_urls'] = str_replace($oData['Pruserdomain']['domain'], "", $Prcampaign_data['Prcampaign']['c_urls']);
            $this->Prcampaign->save($this->data['Prcampaign'], false);
            /*         * **** Reduce number of domain count column start ***** */

            if ($this->Pruserdomain->delete($id, false)) {
                $eMsg = 'Page rank campaign domain has been deleted successfully!';
                // save this activity to log
                //$this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'rankcampaign', $job = 'deleted rank campaign  - ' . $oData['Rankcampaign']['campaign_name']);
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        function pr_user_proxy_create()
        {

            $this->loadModel('Pruserproxy');

            $userId = $this->Auth->user('id');

            if ($this->request->is('post')) {
                $this->request->data['Pruserproxy']['created_at'] = date('Y-m-d G:i:s');
                $this->request->data['Pruserproxy']['user_id'] = $userId;
                $this->Pruserproxy->set($this->data['Pruserproxy']);
                //pr($this->data); die;
                if ($this->Pruserproxy->validates()) {
                    if ($this->Pruserproxy->save($this->data['Pruserproxy'], false)) {
                        $eMsg = 'Your Page Rank User Proxy has been created successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                        $this->redirect($this->params['action']);
                    }// end if
                } else {
                    $this->generateError('Pruserproxy');
                }
            } // is post


            $this->data = $this->data;
            $this->set('data', $this->data);


            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Create New User Proxy');
        }

        function pr_user_proxy_list()
        {
            $this->loadModel('Pruserproxy');
            $this->paginate = array(
                'conditions' => array('Pruserproxy.user_id' => $this->Auth->user('id')), //array of conditions
                'recursive' => 1,
                'limit' => 200
            );

            $page_rank_user_proxy = $this->paginate('Pruserproxy');

            $this->set(compact('page_rank_user_proxy'));

            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Page Rank User Proxy List');
        }

        function pr_user_proxy_delete($id)
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Pruserproxy');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Pruserproxy', $id, 'user_id')) {
                echo 'error';
                exit;
            }

            $this->Pruserproxy->id = $id;
            $userId = $this->Auth->user('id');
            $oData = $this->Pruserproxy->read();
            if ($this->Pruserproxy->delete($id, false)) {

                $eMsg = 'Page rank user proxy has been deleted successfully!';
                // save this activity to log
                //$this->setActivity($this->Auth->user('id'), $showincalendar = '1', $generator = 'user', $activitytype = 'rankcampaign', $job = 'deleted rank campaign  - ' . $oData['Rankcampaign']['campaign_name']);
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        function pr_user_proxy_edit($cId)
        {

            $this->loadModel('Pruserproxy');
            if (!$this->haveAuthToDoAction('Pruserproxy', $cId, 'user_id') || $cId == null) {
                $this->redirect(array('action' => 'pr_user_proxy_list'));
            }
            $userId = $this->Auth->user('id');

            if ($this->request->is('post')) {
                $this->request->data['Pruserproxy']['id'] = $cId;
                $this->request->data['Pruserproxy']['created_at'] = date('Y-m-d G:i:s');
                $this->request->data['Pruserproxy']['user_id'] = $userId;
                $this->Pruserproxy->set($this->data['Pruserproxy']);
                //pr($this->data); die;
                if ($this->Pruserproxy->validates()) {
                    if ($this->Pruserproxy->save($this->data['Pruserproxy'], false)) {
                        $eMsg = 'Your Page Rank User Proxy has been updated successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                        $this->redirect($this->params['action'] . "/" . $cId);
                    }// end if
                } else {
                    $this->generateError('Pruserproxy');
                }
            } // is post

            $data = $this->Pruserproxy->findById($cId);
            $this->data = $data;

            $this->set('data', $this->data);


            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Update New Page Rank User Proxy');
        }

        function pr_instant_check()
        {
            $per_day_limit = 100; // $this->pr_instant_check_limit_per_day();
            $this->loadModel('Prcampaign');
            $this->loadModel('Pruserdomain');
            $this->loadModel('Prinstantcheck');
            $this->loadModel('Jsonapi');


            $userId = $this->Auth->user('id');
            $serviceId = 19;  // Domain Digger
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from you extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {
                $unitPrice = floatval($userServiceCredit['unit_price']);

                $this->loadModel('Pruserproxy');
                $this->paginate = array(
                    'conditions' => array('Pruserproxy.user_id' => $this->Auth->user('id')), //array of conditions
                    'recursive' => 1,
                    'limit' => 200
                );
                $page_rank_user_proxy = $this->paginate('Pruserproxy');

                $this->set(compact('page_rank_user_proxy'));
                $this->set('data', $this->data);
                /*         * ****** After post instant check domain start ****** */
                if ($this->request->is('post')) {

                    $this->Prcampaign->set($this->data['Prcampaign']);

                    $user_proxy = $this->data['Prcampaign']['user_proxy'];
                    $this->set(compact('user_proxy'));

                    $this->Prcampaign->unbindValidation('remove', array('name'), true);

                    /** MY own proxy start */
                    if ($this->data['Prcampaign']['user_proxy'] == 1) {
                        $count = count($this->data['ip_address']);
                        $unitPrice = 0.5;

                        unset($page_rank_user_proxy);
                        $check = 0;
                        for ($i = 0; $i < $count; $i++) {
                            if ($this->data['ip_address'][$i] != "" && $this->data['password'][$i] != "" && $this->data['port'][$i] != "") {
                                $check = 1;
                                $array_proxyIp[] = $page_rank_user_proxy[$i]['Pruserproxy']['ip_address'] = $this->data['ip_address'][$i];
                                //$proxy_user_pass =  $ip_info['username'];
                                $page_rank_user_proxy[$i]['Pruserproxy']['username'] = $this->data['username'][$i];
                                $array_proxy_user_pass[$i] = $page_rank_user_proxy[$i]['Pruserproxy']['password'] = $this->data['password'][$i];
                                $array_proxy_port[$i] = $page_rank_user_proxy[$i]['Pruserproxy']['port'] = $this->data['port'][$i];
                            }
                        }
                        if ($check == 0) {
                            $this->Prcampaign->validationErrors['must_proxy_ip'] = array("Please insert minimum one proxy ip information.");
                        }
                    }

                    $this->set(compact('page_rank_user_proxy'));
                    $this->set('data', $this->data);
                    /** MY own proxy end */
                    // For domain limit start
                    $current_day = date('Y-m-d');
                    $total_domain_today = $this->Domain->query("SELECT * FROM `pr_instant_check` WHERE `created_at` > '$current_day'");
                    $total_domain_today = count($total_domain_today);
                    $urls = preg_replace("~http:\/\/~", "", $this->data['Prcampaign']['c_urls']);
                    $this->request->data['Prcampaign']['c_urls'] = $urls;
                    $curls = explode("\n", trim($this->data['Prcampaign']['c_urls']));
                    $total_post_domain = count($curls);

                    $total_domain = $total_domain_today + $total_post_domain;
                    if ($total_domain > $per_day_limit) {
                        $remain_domain = $per_day_limit - $total_domain_today;
                        $rmsg = " Per day limit is " . $per_day_limit . ' domains for instant checking.';
                        if ($remain_domain > 0) {
                            $rmsg = ' You have only ' . $remain_domain . ' domains are left today for instant checking. So please reduce your domain.';
                        }
                        $this->Prcampaign->validationErrors['bb'] = array("You have exceed your today's instant check limit!" . $rmsg);
                    }
                    // For domain limit end

                    if ($this->Prcampaign->validates()) {


                        $urls = array();
                        $all_domain = array();
                        $this->loadModel('GoogePageRank');
                        $this->loadModel('DomainTools');
                        $this->loadModel('Majesticseo');
                        //$this->loadModel('robowhois');
                        /*$credit = $this->robowhois->credit();
                        if ($credit < 500) {
                        $subject = 'Increase your Robwhois Credit';
                        $body = 'Hi, <br/>Please increase your Robwhois Credit. Current credit is ' . $credit;
                        $this->sendEmail(array('abir@seonitro.com', 'julie@seonitro.com', 'dori@seonitro.com'), $subject, $body);
                        }*/


                        $admin_ip = $this->admin_ip();
                        $inc = 0;
                        $domain_ar = array();
                        foreach ($curls as $index => $u) {
                            $ul = trim($u);
                            $ul = str_replace("http://", "", $ul);
                            $ul = str_replace("www.", "", $ul);
                            $ul1 = explode("/", $ul);

                            if (!in_array($ul1[0], $urls)) { // remove duplicates
                                //if (!in_array(str_replace("www.", "", $ul1[0]), $urls)) { // remove duplicates
                                $urls[] = $ul1[0];
                                if (!in_array($ul1[0], $domain_ar)) {
                                    $domain_ar[] = $ul1[0];

                                    /*                             * **** For multiple ip start ***** */
                                    if ($this->data['Prcampaign']['user_proxy'] == 0) {
                                        $proxyIp = $admin_ip['ip'];
                                        $proxy_user_pass = $admin_ip['password'];
                                        $proxy_port = $admin_ip['port'];
                                    } else {
                                        if (!isset($array_proxyIp[$index]))
                                            $index = 0;
                                        $proxyIp = array($array_proxyIp[$index]);
                                        $proxy_user_pass = $array_proxy_user_pass[$index];
                                        $proxy_port = $array_proxy_port[$index];
                                    }
                                    /*                             * **** For multiple ip end ***** */
                                    // pr($_POST);
                                    //$page_rank = $this->GoogePageRank->getPageRank($url = $ul1[0], $proxyIp, $proxy_user_pass, $proxy_port);
                                    //$indexed = $this->GoogePageRank->checkIndex($url = $ul1[0], $proxyIp, $proxy_user_pass, $proxy_port);
                                    $d = serialize(array($ul1[0]));
                                    $prIndex = @array_shift(unserialize(file_get_contents("http://alltest.seonitro.com/get_pr_index.php?doms=" . $d)));
                                    $page_rank = $prIndex[0];
                                    $indexed = $prIndex[1];
                                    $created_date = 'na';
                                    $expires_date = 'na';
                                    $all_name_server = 'na';
                                    $age = 'na';

                                    $majesticseo = @$this->Majesticseo->GetIndexItemInfo($ul1[0]);
                                    $whoisProperties = @$this->Jsonapi->getWhoisData(array("domain" => str_replace("www.", "", $ul1[0])));
                                    //pr($whoisProperties);
                                    if (isset($whoisProperties->code) && ($whoisProperties->code != 200)) {
                                        $this->request->data['Prinstantcheck']['created'] = $created_date;
                                        $this->request->data['Prinstantcheck']['expires'] = $expires_date;
                                        $this->request->data['Prinstantcheck']['age'] = $age;
                                        $this->request->data['Prinstantcheck']['name_server'] = $all_name_server;
                                    } else {
                                        /*                                 * * For all name server start * */
                                        $created_date = @date('Y-m-d', strtotime($whoisProperties->body->created_on));
                                        $age = @date('Y', strtotime($whoisProperties->body->created_on));
                                        $expires_date = @date('Y-m-d', strtotime($whoisProperties->body->expires_on));
                                        $dns1 = @$whoisProperties->body->nameservers[0]->name;
                                        $dns2 = @$whoisProperties->body->nameservers[1]->name;
                                        $all_name_server = $dns1 . ',' . $dns2;
                                        $this->request->data['Prinstantcheck']['created'] = $created_date;
                                        $this->request->data['Prinstantcheck']['expires'] = $expires_date;
                                        $this->request->data['Prinstantcheck']['name_server'] = $all_name_server;
                                        $this->request->data['Prinstantcheck']['age'] = $age;
                                    }


                                    /*                             * * For all name server end * */

                                    /*                             * ************ Semoz Rank start ************** */
                                    App::import('Vendor', 'seomoz_vendor/seomoz');
                                    $obj = new seomoz();
                                    $seomozrank = $obj->getData($ul1[0]);
                                    $seomozrank = $seomozrank['seomozrank'];

                                    /*                             * ************ Semoz Rank end ************** */
                                    $all_domain[$inc]['Pruserdomain'] = array(
                                        'domains' => $ul1[0],
                                        'page_rank' => $page_rank,
                                        'indexed' => $indexed,
                                        'created' => $created_date,
                                        'expires' => $expires_date,
                                        'name_server' => $all_name_server,
                                        'age' => $age,
                                        //'age' => $this->robowhois->date_difference($whoisProperties->response->properties->created_on, date('Y-m-d')),
                                        'seomoz_rank' => $seomozrank,
                                        'citation_flow' => $majesticseo['row'][33],
                                        'trust_flow' => $majesticseo['row'][34],
                                        'ext_back_links' => $majesticseo['row'][4],
                                        //'proxy' => $proxyIp,
                                        'created_at' => date('d M Y'),
                                        'updated_at' => date('d M Y')
                                    );
                                    $inc++;
                                    // Save instant value in database start
                                    $this->request->data['Prinstantcheck']['domain'] = $ul1[0];
                                    $this->request->data['Prinstantcheck']['user_id'] = $userId;
                                    $this->request->data['Prinstantcheck']['page_rank'] = $page_rank;
                                    $this->request->data['Prinstantcheck']['seomoz_rank'] = $seomozrank;
                                    $this->request->data['Prinstantcheck']['ext_back_links'] = $majesticseo['row'][4];
                                    $this->request->data['Prinstantcheck']['indexed'] = $indexed;
                                    $this->request->data['Prinstantcheck']['citation_flow'] = $majesticseo['row'][33];
                                    $this->request->data['Prinstantcheck']['trust_flow'] = $majesticseo['row'][34];
                                    //$this->request->data['Prinstantcheck']['proxy'] = $proxyIp;
                                    $this->request->data['Prinstantcheck']['created_at'] = date('Y-m-d G:i:s');
                                    $this->Prinstantcheck->create();
                                    if ($this->Prinstantcheck->save($this->data['Prinstantcheck'], false)) {
                                        $updateAccount['user_id'] = $userId;
                                        $updateAccount['service_id'] = $serviceId;
                                        $updateAccount['amount'] = $unitPrice;
                                        $updateAccount['quantity'] = 1;
                                        $updateAccount['remarks'] = 'Credit deducted for pr instant check.';

                                        if ($this->updateAccountDebit($updateAccount)) {
                                            if (!$userServiceCredit['eligible'] && $userServiceCredit['extra_credit_eligible']) {
                                                $this->User->id = $userId;
                                                $newExtraCredit = ($userServiceCredit['extra_credit'] - $unitPrice);
                                                $this->User->saveField('extra_credit', $newExtraCredit);
                                            }
                                        }
                                    }
                                    // Save instant value in database end

                                    $this->set(compact('all_domain'));
                                }
                            }
                        }
                        $_SESSION['csv_data'] = $all_domain;
                    } else {
                        $this->generateError('Prcampaign');
                    }
                } // is post
                /*         * ****** After post instant check domain end ****** */
            } //not eligible
            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', 'Page Rank Instant Check');
        }

        function pr_instant_check_list()
        {
            $this->loadModel('Prinstantcheck');
            $this->paginate = array(
                'conditions' => array('Prinstantcheck.user_id' => $this->Auth->user('id')), //array of conditions
                'fields' => 'domain,page_rank,indexed,created,expires,name_server,seomoz_rank,citation_flow,trust_flow,ext_back_links,created_at,updated_at',
                'order' => 'Prinstantcheck.id DESC',
                'recursive' => 1,
                'limit' => 200
            );

            $_SESSION['csv_data'] = $this->paginate('Prinstantcheck');
            //pr($_SESSION['csv_data']); exit;
            $this->paginate = array(
                'conditions' => array('Prinstantcheck.user_id' => $this->Auth->user('id')), //array of conditions
                //'fields' => 'domain,page_rank,indexed,created,expires,name_server,seomoz_rank,citation_flow,trust_flow,ext_back_links,proxy,created_at,updated_at',
                'order' => 'Prinstantcheck.id DESC',
                'recursive' => 1,
                'limit' => 200
            );
            $instant_check_list = $this->paginate('Prinstantcheck');

            $this->set(compact('instant_check_list'));
        }

        function pr_instant_check_delete($id)
        {
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Prinstantcheck');
            // check if this user can do this
            if (!$this->haveAuthToDoAction('Prinstantcheck', $id, 'user_id') || $id == null) {
                $this->redirect(array('action' => 'pr_instant_check_list'));
            }
            $this->Prinstantcheck->id = $id;

            if ($this->Prinstantcheck->delete($id, false)) {
                $eMsg = 'Page rank instant check domain has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }






        // run the cron to update domain information, it will run once a week or so and add to pr_domain_history table
        function pr_corn_system()  {

            
            $this->autoRender = false;
            $this->loadModel('Pruserdomain');
            $this->loadModel('Pruserdomaincorn');
            $this->loadModel('GoogePageRank');
            $this->loadModel('Majesticseo');
            $this->loadModel('Jsonapi');
               

            // get all the domain which needs update or updated $days day ago
            $days = 15;
            $dd = date('Y-m-d', strtotime("+$days day"));
            $alldomain = $this->Pruserdomain->find('all', array(
                'conditions' => array(" (Pruserdomain.created = 'waiting update') OR (Pruserdomain.updated_at < $dd ) "),
                'order' => array("Pruserdomain.domain" => "ASC"),
                'recursive' => 1
            ));

            $usersDontHaveCredit = array();
            foreach ($alldomain as $row) {
                $domain = $row['Pruserdomain']['domain'];
                /** * Previous data will be corn data ** */
                $insert['Pruserdomaincorn']['domain_id'] = $row['Pruserdomain']['id'];
                $insert['Pruserdomaincorn']['user_id'] = $row['Pruserdomain']['user_id'];
                $insert['Pruserdomaincorn']['domain'] = $row['Pruserdomain']['domain'];
                $insert['Pruserdomaincorn']['page_rank'] = $row['Pruserdomain']['page_rank'];
                $insert['Pruserdomaincorn']['indexed'] = $row['Pruserdomain']['indexed'];
                $insert['Pruserdomaincorn']['created'] = $row['Pruserdomain']['created'];
                $insert['Pruserdomaincorn']['expires'] = $row['Pruserdomain']['expires'];
                $insert['Pruserdomaincorn']['age'] = $row['Pruserdomain']['age'];
                $insert['Pruserdomaincorn']['name_server'] = $row['Pruserdomain']['name_server'];

                $insert['Pruserdomaincorn']['seomoz_rank'] = $row['Pruserdomain']['seomoz_rank'];
                $insert['Pruserdomaincorn']['citation_flow'] = $row['Pruserdomain']['citation_flow'];
                $insert['Pruserdomaincorn']['trust_flow'] = $row['Pruserdomain']['trust_flow'];
                $insert['Pruserdomaincorn']['ext_back_links'] = $row['Pruserdomain']['ext_back_links'];
                $insert['Pruserdomaincorn']['last_checked'] = $row['Pruserdomain']['last_checked'];
                $insert['Pruserdomaincorn']['status'] = $row['Pruserdomain']['status'];
                $insert['Pruserdomaincorn']['script_run'] = $row['Pruserdomain']['script_run'];
                $insert['Pruserdomaincorn']['pr_campaigns_id'] = $row['Pruserdomain']['pr_campaigns_id'];
                $insert['Pruserdomaincorn']['proxy'] = $row['Pruserdomain']['proxy'];
                $insert['Pruserdomaincorn']['reminder_status'] = $row['Pruserdomain']['reminder_status'];
                $insert['Pruserdomaincorn']['created_at'] = $row['Pruserdomain']['created_at'];
                $insert['Pruserdomaincorn']['updated_at'] = $row['Pruserdomain']['updated_at'];

                // do not insert into history table if data not updated yet
                if ( ($row['Pruserdomain']['trust_flow'] != 'waiting update' ) && ($row['Pruserdomain']['seomoz_rank'] != 'waiting update') ) {
                    $this->Pruserdomaincorn->create();
                    $this->Pruserdomaincorn->save($insert['Pruserdomaincorn']);
                }




                $d = serialize(array($domain));
                $prIndex = @array_shift(unserialize(file_get_contents("http://alltest.seonitro.com/get_pr_index.php?doms=" . $d)));
                $page_rank = isset($prIndex[0]) ? $prIndex[0] : 'NULL';
                $indexed =   isset($prIndex[1]) ? $prIndex[1] : 'NULL';

                $majesticseo = $this->Majesticseo->GetIndexItemInfo($domain);
                $whoisProperties = @$this->Jsonapi->getWhoisData(array("domain" => str_replace("www.", "", 'www.'.$domain)));
                pr($whoisProperties);
                if (isset($whoisProperties->code) && ($whoisProperties->code != 200)) {
                    $update['Pruserdomain']['created'] = $row['Pruserdomain']['created'];
                    $update['Pruserdomain']['expires'] = $row['Pruserdomain']['expires'];
                    $update['Pruserdomain']['age'] = $row['Pruserdomain']['age'];
                    $update['Pruserdomain']['name_server'] = $row['Pruserdomain']['name_server'];
                } else {
                    ///*** For all name server start
                    $created_date = @date('Y-m-d', strtotime($whoisProperties->body->created_on));
                    $age = @date('Y', strtotime($whoisProperties->body->created_on));
                    $expires_date = @date('Y-m-d', strtotime($whoisProperties->body->expires_on));
                    $dns1 = @$whoisProperties->body->nameservers[0]->name;
                    $dns2 = @$whoisProperties->body->nameservers[1]->name;
                    $all_name_server = $dns1 . ',' . $dns2;
                    $update['Pruserdomain']['created'] = $created_date;
                    $update['Pruserdomain']['expires'] = $expires_date;
                    $update['Pruserdomain']['name_server'] = $all_name_server;
                    $update['Pruserdomain']['age'] = $age;
                }           


                /* for robowhois corn
                $expires_date = date('Y-m-d',strtotime($row['Pruserdomain']['expires']));
                $expires_date_before_date = date("Y-m-d", strtotime($expires_date) - 24*3600*$days);

                if(date('Y-m-d') > $expires_date_before_date)
                {
                $whoisProperties = $this->robowhois->whoisProperties($domain);
                $name_server = array();
                $all_name_server = "";
                $name_server = $whoisProperties->response->properties->nameservers;

                foreach ($name_server as $key => $nmserver) {
                $all_name_server .= $nmserver->name . '<br/>';
                }

                $update['Pruserdomain']['created'] = date("d M Y", strtotime($whoisProperties->response->properties->created_on));
                $update['Pruserdomain']['expires'] = date("d M Y", strtotime($whoisProperties->response->properties->expires_on));
                $update['Pruserdomain']['name_server'] = $all_name_server;
                $update['Pruserdomain']['age'] = $this->robowhois->date_difference($whoisProperties->response->properties->created_on, date('Y-m-d'));
                }
                */

                /*             * ************ Semoz Rank start ************** */
                App::import('Vendor', 'seomoz_vendor/seomoz');
                $obj = new seomoz();
                $seomozrank = $obj->getData($domain);
                $seomozrank = $seomozrank['seomozrank'];
                /*             * ************ Semoz Rank end ************** */
                $update['Pruserdomain']['id'] = $row['Pruserdomain']['id'];
                $update['Pruserdomain']['page_rank'] = $page_rank;
                $update['Pruserdomain']['seomoz_rank'] = $seomozrank;
                $update['Pruserdomain']['ext_back_links'] = $majesticseo['row'][4];
                $update['Pruserdomain']['indexed'] = $indexed;
                $update['Pruserdomain']['citation_flow'] = $majesticseo['row'][33];
                $update['Pruserdomain']['trust_flow'] = $majesticseo['row'][34];
                $update['Pruserdomain']['script_run'] = date('Y-m-d G:i:s');
                //$update['Pruserdomain']['proxy'] = $proxyIp;
                $update['Pruserdomain']['updated_at'] = date('Y-m-d G:i:s');
                $this->Pruserdomain->save($update['Pruserdomain']);
            }

            exit('success');

        } // end function

        function pr_domain_history($cId)
        {
            $this->loadModel('Pruserdomain');
            $this->loadModel('Pruserdomaincorn');

            if (!$this->haveAuthToDoAction('Pruserdomain', $cId, 'user_id') || $cId == null) {
                $this->redirect(array('action' => 'pr_campaign_list'));
            }

            $this->paginate = array(
                'conditions' => array('Pruserdomain.id' => $cId), //array of conditions
                'fields' => 'domain,page_rank,indexed,created,expires,name_server,seomoz_rank,citation_flow,trust_flow,ext_back_links,created_at,updated_at',
                'recursive' => 1,
                'limit' => 200
            );

            $single_domain = $this->single_domain($cId);
            //pr($single_domain); exit;

            $alldomaincorn = $this->Pruserdomaincorn->find('all', array(
                //'conditions' => array("Pruserdomaincorn.domain_id = " . $cId),
                'conditions' => array(" Pruserdomaincorn.pr_campaigns_id = " . $single_domain['pr_campaigns_id'] . " AND Pruserdomaincorn.domain = '" . $single_domain['domain'] . "'"),
                'fields' => 'domain,page_rank,indexed,created,expires,name_server,seomoz_rank,citation_flow,trust_flow,ext_back_links,created_at,updated_at',
                'order' => array("Pruserdomaincorn.id" => "desc"),
                'recursive' => 1,
                'limit' => 10
            ));

            $this->set(compact('alldomaincorn'));

            $Pruserdomain = $this->paginate('Pruserdomain');
            $_SESSION['csv_data'] = array_merge($alldomaincorn, $Pruserdomain);

            $domain = $Pruserdomain[0]['Pruserdomain']['domain'];
            //pr($Pruserdomain);
            $this->set(compact('Pruserdomain'));


            $this->set('navIndex', '2,0');
            $this->set('title_for_layout', $domain . ' Domain History List');
        }

        /*     * *********** For Csv file start ******* */

        function report_csv()
        {
            $this->autoRender = false;
            $this->loadModel('Pruserdomain');
            $alldomain = $_SESSION['csv_data'];

            $domain = array();
            $count = 0;
            $domain[$count]['domain'] = 'Domain Name';
            $domain[$count]['page_rank'] = 'Page Rank';
            $domain[$count]['indexed'] = 'Indexed';
            $domain[$count]['created'] = 'Created';
            $domain[$count]['expires'] = 'Eexpires';
            $domain[$count]['name_server'] = 'Name server';
            $domain[$count]['seomoz_rank'] = 'Seomoz Rank';
            $domain[$count]['citation_flow'] = 'Citation Flow';
            $domain[$count]['trust_flow'] = 'Trust Flow';
            $domain[$count]['ext_back_links'] = 'Ext Back Links';
            //$domain[$count]['proxy'] = 'Proxy';
            $domain[$count]['created_at'] = 'Created Date';
            $domain[$count]['updated_at'] = 'Updated Date';


            foreach ($alldomain as $row) {
                $count++;
                if (isset($row['Pruserdomain']))
                    $domain[$count] = $row['Pruserdomain'];
                else if (isset($row['Pruserdomaincorn']))
                    $domain[$count] = $row['Pruserdomaincorn'];
                    else if (isset($row['Prinstantcheck']))
                        $domain[$count] = $row['Prinstantcheck'];
            }
            $this->array_to_csv($domain, 'domain_csv_report' . '.csv');
            exit;
        }

        function test()
        { // for test
            pr($this->single_domain(50));
            exit;
        }

        /*     * *************************** FOR PAGE RANK MODULE END ****************** */

        /* start Press Release
        *  https://www.prreach.com/
        *
        */

        function orderpressrelease()
        {
            $this->set('title_for_layout', 'Order Press Release');
            $serviceId = 12; // press relase
            $userId = $this->Auth->user('id');
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);
            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from your extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            $this->loadModel('Pressrelease');

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $this->render('not_eligible');
            } else {
                $unitPrice = floatval($userServiceCredit['unit_price']);

                if ($this->request->is('post')) {


                    $this->Pressrelease->set($this->data['Pressrelease']);
                    if ($this->Pressrelease->validates()) {

                        $this->request->data['Pressrelease']['user_id'] = $userId;

                        $fileFieldsArr[] = 'image_1_file';
                        $fileFieldsArr[] = 'image_2_file';
                        $fileFieldsArr[] = 'image_3_file';
                        $fileFieldsArr[] = 'image_4_file';
                        $fileFieldsArr[] = 'attachment_1_file';
                        $fileFieldsArr[] = 'attachment_2_file';
                        $fileFieldsArr[] = 'attachment_3_file';
                        $fileFieldsArr[] = 'attachment_4_file';

                        foreach ($fileFieldsArr as $fileField) {
                            if (!empty($this->data['Pressrelease'][$fileField]['name'])) {
                                $file = pathinfo($this->data['Pressrelease'][$fileField]['name']);
                                $tmp_file = new File($this->data['Pressrelease'][$fileField]['tmp_name']);
                                $finalFileName = $file['filename'] . '_' . $this->generateRandomString(3) . '.' . $file['extension'];
                                $finalFileName = str_replace(array(' ', '-'), '_', $finalFileName);
                                if ($tmp_file->copy('press_release' . DS . $finalFileName)) {
                                    $this->request->data['Pressrelease'][$fileField] = $finalFileName;
                                }
                            } else {
                                $this->request->data['Pressrelease'][$fileField] = '';
                            }
                        }
                        $this->request->data['Pressrelease']['release_date'] = date('Y-m-d', strtotime($this->data['Pressrelease']['release_date']));

                        if ($this->Pressrelease->save($this->data['Pressrelease'], false)) {
                            $eMsg = 'Your Press Release request has been accepted. Please wait while we process.';
                            $this->Session->setFlash($eMsg, 'flash_success');
                        } else {
                            $eMsg = 'Request could not be processed.';
                            $this->Session->setFlash($eMsg, 'flash_success');
                        }
                        $this->redirect($this->params['action']);
                    } else {
                        $this->generateError('Pressrelease');
                    }
                    //echo 'form posted';
                }
            }
        }

        function editpressrelease($itemId)
        {
            $this->set('title_for_layout', 'Edit Press Release');
            $userId = $this->Auth->user('id');

            $this->loadModel('Pressrelease');
            if (!$this->haveAuthToDoAction('Pressrelease', $itemId, 'user_id')) {
                $this->redirect(array('action' => 'pressreleases'));
            }

            $cond = array(
                'conditions' => "Pressrelease.id = $itemId AND Pressrelease.user_id = $userId"
            );

            $pressReleaseInfo = $this->Pressrelease->find('first', $cond);

            $pressReleaseInfo['Pressrelease']['release_date'] = date('m/d/Y', strtotime($pressReleaseInfo['Pressrelease']['release_date']));

            if ($this->request->is('post')) {
                $this->Pressrelease->set($this->data['Pressrelease']);
                if ($this->Pressrelease->validates()) {
                    $fileFieldsArr[] = 'image_1_file';
                    $fileFieldsArr[] = 'image_2_file';
                    $fileFieldsArr[] = 'image_3_file';
                    $fileFieldsArr[] = 'image_4_file';
                    $fileFieldsArr[] = 'attachment_1_file';
                    $fileFieldsArr[] = 'attachment_2_file';
                    $fileFieldsArr[] = 'attachment_3_file';
                    $fileFieldsArr[] = 'attachment_4_file';

                    foreach ($fileFieldsArr as $fileField) {
                        if (!empty($this->data['Pressrelease'][$fileField]['name'])) {
                            $file = pathinfo($this->data['Pressrelease'][$fileField]['name']);
                            $tmp_file = new File($this->data['Pressrelease'][$fileField]['tmp_name']);
                            $finalFileName = $file['filename'] . '_' . $this->generateRandomString(3) . '.' . $file['extension'];
                            $finalFileName = str_replace(array(' ', '-'), '_', $finalFileName);
                            if ($tmp_file->copy('press_release' . DS . $finalFileName)) {
                                $this->request->data['Pressrelease'][$fileField] = $finalFileName;
                            }
                        } else {
                            $this->request->data['Pressrelease'][$fileField] = $pressReleaseInfo['Pressrelease'][$fileField];
                        }
                    }
                    $this->request->data['Pressrelease']['release_date'] = date('Y-m-d', strtotime($this->data['Pressrelease']['release_date']));

                    $this->Pressrelease->id = $itemId;
                    if ($this->Pressrelease->save($this->data['Pressrelease'], false)) {
                        $eMsg = 'Your Press Release has been updated successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'Request could not be updated.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    }
                    $this->redirect($this->params['action'] . '/' . $itemId);
                } else {
                    $this->generateError('Pressrelease');
                }
            } else {
                $this->data = $pressReleaseInfo;
            }
            $this->set('data', $pressReleaseInfo);
            $this->set('itemId', $itemId);
        }

        function viewpressrelease($itemId)
        {
            Configure::write('Cache.disable', true);
            $this->set('title_for_layout', 'View Press Release');
            $userId = $this->Auth->user('id');

            $this->loadModel('Pressrelease');
            if (!$this->haveAuthToDoAction('Pressrelease', $itemId, 'user_id')) {
                $this->redirect(array('action' => 'pressreleases'));
            }

            $cond = array(
                'conditions' => "Pressrelease.id = $itemId AND Pressrelease.user_id = $userId"
            );

            $pressReleaseInfo = $this->Pressrelease->find('first', $cond);
            $this->set(compact('pressReleaseInfo'));
        }

        function pressreleases()
        {
            Configure::write('Cache.disable', true);
            $this->loadModel('Pressrelease');
            $userId = $this->Auth->user('id');

            $pressReleaseList = $this->Pressrelease->find('all', array('conditions' => "Pressrelease.user_id=$userId"));
            $this->set(compact('pressReleaseList'));
        }

        public function downloadFile($folderName, $fileName)
        {
            $this->autoRender = false;
            $this->response->file(APP . 'webroot/' . $folderName . '/' . $fileName, array('download' => true, 'name' => 'Current File'));
        }

        //Link density work shyam by shyam
        function ldreport()
        {
            //Configure::write('debug', 2);
            //Configure::write('Cache.disable', true);
            $this->loadModel('Ldreport');
            if ($this->request->is('post')) {
                $GetTopBackLinksAnalysisResUnitsCost = $this->data['Ldreportcron']['GetTopBackLinksAnalysisResUnitsCost'];
                $domain = $this->getPlainDomain($this->data['Ldreportcron']['domain']);
                //pr($_POST); exit;
                $this->loadModel('Majesticseo');
                //echo $domain; exit;
                $GetBackLinksHistory = $this->Majesticseo->GetBackLinksHistory($domain);
                //pr($GetBackLinksHistory); exit;

                $majesticseo = $this->Majesticseo->GetIndexItemInfo($domain);
                $update['Ldreport']['user_id'] = $this->Auth->user('id');
                $update['Ldreport']['domain'] = $domain;
                $update['Ldreport']['ItemNum'] = $majesticseo['row'][0];
                $update['Ldreport']['ResultCode'] = $majesticseo['row'][2];
                $update['Ldreport']['Status'] = $majesticseo['row'][3];
                $update['Ldreport']['ExtBackLinks'] = $majesticseo['row'][4];
                $update['Ldreport']['RefDomains'] = $majesticseo['row'][5];
                $update['Ldreport']['AnalysisResUnitsCost'] = $majesticseo['row'][6];
                $update['Ldreport']['ACRank'] = $majesticseo['row'][7];
                $update['Ldreport']['ItemType'] = $majesticseo['row'][8];
                $update['Ldreport']['IndexedURLs'] = $majesticseo['row'][9];
                //$update['Ldreport']['GetTopBackLinksAnalysisResUnitsCost'] = $majesticseo['row'][10];
                $update['Ldreport']['GetTopBackLinksAnalysisResUnitsCost'] = $GetTopBackLinksAnalysisResUnitsCost;
                $update['Ldreport']['DownloadBacklinksAnalysisResUnitsCost'] = $majesticseo['row'][11];
                $update['Ldreport']['RefIPs'] = $majesticseo['row'][12];
                $update['Ldreport']['RefSubNets'] = $majesticseo['row'][13];
                $update['Ldreport']['RefDomainsEDU'] = $majesticseo['row'][14];
                $update['Ldreport']['ExtBackLinksEDU'] = $majesticseo['row'][15];
                $update['Ldreport']['RefDomainsGOV'] = $majesticseo['row'][16];
                $update['Ldreport']['ExtBackLinksGOV'] = $majesticseo['row'][17];
                $update['Ldreport']['RefDomainsEDU_Exact'] = $majesticseo['row'][18];
                $update['Ldreport']['ExtBackLinksEDU_Exact'] = $majesticseo['row'][19];
                $update['Ldreport']['RefDomainsGOV_Exact'] = $majesticseo['row'][20];
                $update['Ldreport']['ExtBackLinksGOV_Exact'] = $majesticseo['row'][21];
                $update['Ldreport']['CrawledFlag'] = $majesticseo['row'][22];
                $update['Ldreport']['LastCrawlDate'] = $majesticseo['row'][23];
                $update['Ldreport']['LastCrawlResult'] = $majesticseo['row'][24];
                $update['Ldreport']['RedirectFlag'] = $majesticseo['row'][25];
                $update['Ldreport']['FinalRedirectResult'] = $majesticseo['row'][26];
                $update['Ldreport']['OutDomainsExternal'] = $majesticseo['row'][27];
                $update['Ldreport']['OutLinksExternal'] = $majesticseo['row'][28];
                $update['Ldreport']['OutLinksInternal'] = $majesticseo['row'][29];
                $update['Ldreport']['LastSeen'] = $majesticseo['row'][30];
                $update['Ldreport']['Title'] = $majesticseo['row'][31];
                $update['Ldreport']['RedirectTo'] = $majesticseo['row'][32];
                $update['Ldreport']['CitationFlow'] = $majesticseo['row'][33];
                $update['Ldreport']['TrustFlow'] = $majesticseo['row'][34];
                $update['Ldreport']['TrustMetric'] = $majesticseo['row'][35];


                foreach ($GetBackLinksHistory as $index => $value) {

                    $update['Ldreport'][$index] = $GetBackLinksHistory[$index][0];
                }
                $this->Ldreport->save($update['Ldreport']);

                // go to details page

                $_SESSION['session_id'] = $this->Ldreport->getLastInsertId();
                $_SESSION['session_domain'] = $domain;
                $this->redirect(array('action' => 'ld_link_ratio'));

                //pr($majesticseo);exit;
                $eMsg = 'Searching completed Successfully.';
                $this->Session->setFlash($eMsg, 'flash_success');
            }
            $this->paginate = array(
                'conditions' => array('Ldreport.user_id' => $this->Auth->user('id')),
                'order' => 'Ldreport.domain ASC, Ldreport.id DESC',
                //'group' => 'Ldreport.domain ASC',
                'recursive' => 1,
                //'limit' => 200
            );
            $allDomains = $this->paginate('Ldreport');
            //pr($allDomains);exit;
            $this->set(compact('allDomains'));
            $this->set('title_for_layout', ' Domain History List');
        }

        function ld_report_list()
        {
            $this->redirect(array('action' => 'ldreport'));
        }

        function analyzedomain($domain, $id)
        {
            $_SESSION['session_id'] = $id;
            $_SESSION['session_domain'] = $domain;
            $this->redirect(array('action' => 'ld_link_ratio'));
        }

        function ld_link_ratio()
        {
            if (!isset($_SESSION['session_id']) && !isset($_SESSION['session_domain'])) {
                $this->redirect(array('action' => 'ldreport'));
            }
            $id = $_SESSION['session_id'];
            $domain = $_SESSION['session_domain'];
            Configure::write('debug', 0);
            Configure::write('Cache.disable', true);
            $this->loadModel('Ldreport');
            $this->loadModel('Ldreportcron');
            $data = $this->Ldreport->findById($id);
            $_SESSION['external_link'] = "`SourceURL` NOT LIKE '%$domain%' AND `ldreport_id` = $id";
            $_SESSION['internal_link'] = "`SourceURL`  LIKE '%$domain%' AND `ldreport_id` = $id";

            $this->data = $data;
            $this->set('data', $this->data);

            $this->set('title_for_layout', ' Domain History List');
        }

        function ld_anchor_metrics()
        {
            if (!isset($_SESSION['session_id'])) {
                $this->redirect(array('action' => 'ldreport'));
            }
            $id = $_SESSION['session_id'];
            $this->loadModel('Ldreport');
            $this->loadModel('Ldreportcron');
            $data = $this->Ldreport->findById($id);
            $this->data = $data;
            $this->set('data', $this->data);
            $this->set('title_for_layout', 'Anchor Metrics');
        }

        function ld_linktype_metrics()
        {
            if (!isset($_SESSION['session_id'])) {
                $this->redirect(array('action' => 'ldreport'));
            }
            $id = $_SESSION['session_id'];
            $this->loadModel('Ldreport');
            $this->loadModel('Ldreportcron');
            $data = $this->Ldreport->findById($id);
            $this->data = $data;
            $this->set('data', $this->data);
            $this->set('title_for_layout', 'Linktype Metrics');
        }

        function ld_power_link_metrics()
        {
            //Configure::write('debug', 2);
            //Configure::write('Cache.disable', true);
            //exit;
            if (!isset($_SESSION['session_id'])) {
                $this->redirect(array('action' => 'ldreport'));
            }
            $id = $_SESSION['session_id'];
            $this->loadModel('Ldreport');
            $this->loadModel('Ldreportcron');
            $data = $this->Ldreport->findById($id);
            //pr($data); exit;
            $this->data = $data;
            $this->set('data', $this->data);
            $this->set('title_for_layout', 'Power Link Matrics');
        }

        function ld_filter()
        {
            if (!isset($_SESSION['session_id']) && !isset($_SESSION['session_domain'])) {
                $this->redirect(array('action' => 'ldreport'));
            }
            $id = $_SESSION['session_id'];
            $domain = $_SESSION['session_domain'];
            $this->loadModel('Ldreport');
            $this->loadModel('Ldreportcron');
            if ($this->request->is('post')) {
                //pr($_POST);
                $pr = $this->data['Ldreportcron']['pr'];
                $index = $this->data['Ldreportcron']['index'];
                $LinkType = $this->data['Ldreportcron']['LinkType'];
                $FlagNoFollow = $this->data['Ldreportcron']['FlagNoFollow'];
                $con_array = array();
                if ($pr != '') {
                    $con_array[] = "pr = '$pr'";
                }
                if ($index != '') {
                    $con_array[] = "`index` = '$index'";
                }
                if ($LinkType != '') {
                    $con_array[] = "LinkType = '$LinkType'";
                }
                if ($FlagNoFollow != '') {
                    $con_array[] = "FlagNoFollow = '$FlagNoFollow'";
                }

                $con = implode(' AND ', $con_array);
                if ($con != "")
                    $con = ' AND ' . $con;
                //echo $con; exit;
                $this->set('data', $this->data);
                $_SESSION['external_link'] = "`SourceURL` NOT LIKE '%$domain%' AND `ldreport_id` = $id $con";
                $_SESSION['internal_link'] = "`SourceURL`  LIKE '%$domain%' AND `ldreport_id` = $id $con";

                $this->set('ld_filter', 1);
            }

            $all_pr = $this->Ldreportcron->find('list', array('fields' => 'pr, pr', 'conditions' => 'pr != "" AND ldreport_id = ' . $id, 'group' => 'pr', 'order' => 'pr ASC'));
            $all_pr = array('' => 'Select PR') + $all_pr;
            $this->set('all_pr', $all_pr);
            $all_LinkType = $this->Ldreportcron->find('list', array('fields' => 'LinkType,LinkType', 'conditions' => 'LinkType != "" AND ldreport_id = ' . $id, 'group' => 'LinkType', 'order' => 'LinkType DESC', 'limit' => 3));
            $all_LinkType = array('' => 'Select LinkType') + $all_LinkType;
            //array_unshift($all_LinkType, "Select LinkType");
            $this->set('all_LinkType', $all_LinkType);
            //$data = $this->Ldreport->findById($id);
            //$this->data = $data;
            //$this->set('data', $this->data);
            $this->set('title_for_layout', 'Linkdensity Filter');
        }

        function ldreport_delete($id)
        {
            //echo $id; exit;
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel('Ldreport');
            $this->loadModel('Ldreportcron');
            // check if this user can do this

            if (!$this->haveAuthToDoAction('Ldreport', $id, 'user_id') || $id == null) {
                $this->redirect(array('action' => 'ldreport'));
            }
            $this->Ldreport->id = $id;

            if ($this->Ldreport->delete($id, false)) {

                $this->Ldreportcron->deleteall(array('Ldreportcron.ldreport_id' => $id), false);

                $eMsg = 'This link density report has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        
        // run by cron job....it is running 
        
        function analyzedomain_cron()
        {
            $this->loadModel('Ldreport');
            $this->loadModel('Ldreportcron');
            $this->loadModel('Majesticseo');
            $attributes = '@attributes';
            $alldomain = $this->Ldreport->find('all', array(
                'conditions' => "cron_status = 0",
                'fields' => "id,domain,GetTopBackLinksAnalysisResUnitsCost",
                'order' => array("Ldreport.id" => "asc"),
                'limit' => 10,
                'recursive' => 1
            ));

            if (!empty($alldomain)) {
                foreach ($alldomain as $row_val) {

                    $cut_credit = $row_val['Ldreport']['GetTopBackLinksAnalysisResUnitsCost'];
                    //$cut_credit = 2; //echo $cut_credit;exit;

                    $update['Ldreportcron']['ldreport_id'] = $row_val['Ldreport']['id'];
                    $GetBackLinksHistory = $this->Majesticseo->GetBackLinkData($row_val['Ldreport']['domain'], $cut_credit);
                    $GetBackLinksHistory = json_decode(json_encode($GetBackLinksHistory));


                    $all_attr = $GetBackLinksHistory->$attributes;

                    if (!empty($all_attr)) {
                        foreach ($all_attr as $index => $history) {
                            if ($index != 'Headers')
                                $update_ldreport['Ldreport'][$index] = $history;
                            else
                                $Headers = $history;
                        }
                    }
                    $update_ldreport['Ldreport']['id'] = $row_val['Ldreport']['id'];
                    $update_ldreport['Ldreport']['cron_status'] = 1;
                    $this->Ldreport->save($update_ldreport);
                    $Headers = explode("|", $Headers);
                    if (!empty($GetBackLinksHistory->Row)) {
                        foreach ($GetBackLinksHistory->Row as $single_row) {
                            $Headers_info = explode("|", $single_row);
                            foreach ($Headers_info as $in => $row_header) {
                                if (isset($Headers[$in]) && isset($Headers_info[$in]) && $Headers[$in] != "" && $Headers_info[$in] != "") {
                                    $update['Ldreportcron'][$Headers[$in]] = $Headers_info[$in];
                                }
                            }
                            if ($update['Ldreportcron']['LinkType'] == 'Dead' || strlen($update['Ldreportcron']['SourceURL']) > 200) {
                                $update['Ldreportcron']['index'] = 0;
                                $update['Ldreportcron']['pr'] = 0;
                            }
                            //pr($update);exit;
                            $this->Ldreportcron->create();
                            $this->Ldreportcron->save($update);
                        }
                    }

                    //pr($update);
                    // $this->Ldreportcron->save($update,false);
                }
            }
            exit('Successfully executed.');
        }

        function ld_page_rank_cron()
        {
            //Configure::write('debug', 2);
            Configure::write('Cache.disable', true);
            $this->loadModel('Ldreportcron');
            $this->loadModel('GoogePageRank');
            $admin_ip = $this->admin_ip();

            $alldomain = $this->Ldreportcron->find('all', array(
                'conditions' => "pr = ''",
                'fields' => "id,ldreport_id,SourceURL",
                'order' => array("Ldreportcron.id" => "asc"),
                'limit' => 180,
                'recursive' => 1
            ));

            foreach ($alldomain as $row) {
                //pr($domain);
                $id = $row['Ldreportcron']['id'];
                $ldreport_id = $row['Ldreportcron']['ldreport_id'];
                $domain = $row['Ldreportcron']['SourceURL'];

                $sql = "SELECT * FROM `ldreportcron` WHERE `SourceURL` = '$domain' AND `ldreport_id` = $ldreport_id AND `id` < $id LIMIT 1";
                $find_pr_from_previous_id = $this->Ldreportcron->query($sql);

                if (empty($find_pr_from_previous_id)) {
                    //echo $id.'<br/>';
                    $proxyIp = $admin_ip['ip'];
                    $proxy_user_pass = $admin_ip['password'];
                    $proxy_port = $admin_ip['port'];
                    //$page_rank = $this->GoogePageRank->getPageRank($domain, $proxyIp, $proxy_user_pass, $proxy_port);
                    //if ($page_rank == 'noproxy') $page_rank = 0;
                    //$indexed = $this->GoogePageRank->checkIndex($domain, $proxyIp, $proxy_user_pass, $proxy_port);

                    $domain_ar[] = $domain;
                    $d = serialize($domain_ar);
                    $prIndex = @array_shift(unserialize(file_get_contents("http://alltest.seonitro.com/get_pr_index.php?doms=" . $d)));

                    $page_rank = isset($prIndex[0]) ? $prIndex[0] : 0;
                    $indexed = isset($prIndex[1]) ? $prIndex[1] : 0;

                    $sql = "UPDATE `ldreportcron` SET `index` = '$indexed', `pr` = '$page_rank' WHERE `SourceURL` = '$domain' AND `ldreport_id` = $ldreport_id";
                    $this->Ldreportcron->query($sql);
                }
            }

            exit('Success.');
        }

        function domains_ajax($con = '')
        {
            if ($con == "") {
                $_SESSION['con'] = ' ldreport_id = ' . $_SESSION['session_id'];
            } else if ($con == 1) {
                $_SESSION['con'] = $_SESSION['external_link'];
            } else if ($con == 2) {
                $_SESSION['con'] = $_SESSION['internal_link'];
            }
            $this->loadModel('Domain');
            $this->loadModel('Ldreportcron');
            //Configure::write('debug', 2);
            //Configure::write('Cache.disable', true);
            $this->autoRender = false;
            $this->layout = '';
            //Configure::write('debug', 0);
            /* Array of database columns which should be read and sent back to DataTables. Use a space where
            * you want to insert a non-database field (for example a counter or static image)
            */
            $aColumns = array('`ldreportcron`.`AnchorText`', '`ldreportcron`.`SourceURL`', '`ldreportcron`.`pr`', '`ldreportcron`.`FlagNoFollow`', '`ldreportcron`.`SourceCitationFlow`', '`ldreportcron`.`FlagNoFollow`');

            /*
            * Paging
            */
            $sLimit = "";
            if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
                $sLimit = "LIMIT " . intval($_GET['iDisplayStart']) . ", " .
                intval($_GET['iDisplayLength']);
            }
            /*
            * Ordering
            */
            $sOrder = "";
            if (isset($_GET['iSortCol_0'])) {
                $sOrder = "ORDER BY  ";
                for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                    if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                        $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] .
                        ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                    }
                }

                $sOrder = substr_replace($sOrder, "", -2);
                if ($sOrder == "ORDER BY") {
                    $sOrder = "";
                }
            } else {
                $sOrder = " ORDER BY  `ldreportcron`.`id` ASC";
            }
            /*
            * Filtering
            * NOTE this does not match the built-in DataTables filtering which does it
            * word by word on any field. It's possible to do here, but concerned about efficiency
            * on very large tables, and MySQL's regex functionality is very limited
            */
            $sWhere = "";
            if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
                $sWhere = "WHERE (";
                for ($i = 0; $i < count($aColumns); $i++) {
                    $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
                }
                $sWhere = substr_replace($sWhere, "", -3);
                $sWhere .= ')';
            }

            /* Individual column filtering */
            for ($i = 0; $i < count($aColumns); $i++) {
                if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                    if ($sWhere == "") {
                        $sWhere = "WHERE ";
                    } else {
                        $sWhere .= " AND ";
                    }
                    $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch_' . $i] . "%' ";
                }
            }


            //$cols = "`ldreportcron`.`id`, `ldreportcron`.`rootdomain_id`, `ldreportcron`.`masteraccess_id`, `ldreportcron`.`domain`, `ldreportcron`.`expires_on`, `ldreportcron`.`created`, `ldreportcron`.`dns`, `ldreportcron`.`regstatus`, `ldreportcron`.`privacy`, `ldreportcron`.`pr`, `ldreportcron`.`is_indexed`, `ldreportcron`.`cflow`, `ldreportcron`.`tflow`, `ldreportcron`.`ljpr`, `ldreportcron`.`gdix`, `ldreportcron`.`seomoz_rank`, `ldreportcron`.`ext_backlinks`, `ldreportcron`.`title`, `ldreportcron`.`markets`, `ldreportcron`.`topic`, `ldreportcron`.`tagline`, `ldreportcron`.`cms`, `ldreportcron`.`ausername`, `ldreportcron`.`apassword`, `ldreportcron`.`cusername`, `ldreportcron`.`httpcode`, `ldreportcron`.`httpstatus`, `ldreportcron`.`livestatus`, `ldreportcron`.`status`, `ldreportcron`.`modified`, `Rootdomain`.`id`, `Rootdomain`.`masteraccess_id`, `Rootdomain`.`domain`, `Rootdomain`.`ip`, `Rootdomain`.`expires_on`, `Rootdomain`.`created`, `Rootdomain`.`dns`, `Rootdomain`.`regstatus`, `Rootdomain`.`privacy`, `Rootdomain`.`pr`, `Rootdomain`.`is_indexed`, `Rootdomain`.`cflow`, `Rootdomain`.`tflow`, `Rootdomain`.`ljpr`, `Rootdomain`.`gdix`, `Rootdomain`.`seomoz_rank`, `Rootdomain`.`ext_backlinks`, `Rootdomain`.`title`, `Rootdomain`.`markets`, `Rootdomain`.`topic`, `Rootdomain`.`tagline`, `Rootdomain`.`cms`, `Rootdomain`.`ausername`, `Rootdomain`.`apassword`, `Rootdomain`.`cusername`, `Rootdomain`.`ftppass`, `Rootdomain`.`httpcode`, `Rootdomain`.`httpstatus`, `Rootdomain`.`livestatus`, `Rootdomain`.`status`, `Rootdomain`.`modified`, `Masteraccess`.`id`, `Masteraccess`.`Registrant`, `Masteraccess`.`Registrar`, `Masteraccess`.`Login`, `Masteraccess`.`Password`, `Masteraccess`.`PIN`, `Masteraccess`.`DBP`, `Masteraccess`.`notes`, `Masteraccess`.`modified`";
            if ($sWhere == "") {
                $sWhere = " WHERE {$_SESSION['con']} ";
                //$sWhere = " WHERE `rootdomain_id` > 0 ";
            } else {
                $sWhere .= " AND {$_SESSION['con']} ";
                //$sWhere .= " AND `rootdomain_id` > 0 ";
            }
            $q = "SELECT SQL_CALC_FOUND_ROWS * FROM `ldreportcron`  $sWhere $sOrder $sLimit"; // $sWhere $sOrder $sLimit  WHERE 1 = 1 ORDER BY `ldreportcron`.`domain` ASC LIMIT 100
            //echo $q; exit;
            $res = $this->Ldreportcron->query($q);
            //pr($res); exit;
            /* Data set length after filtering */
            $resC = $this->Ldreportcron->query("SELECT FOUND_ROWS()");
            $iFilteredTotal = $resC[0][0]['FOUND_ROWS()'];
            /* Total data set length */
            $iTotal = $this->Ldreportcron->find('count', array('conditions' => null));

            /*
            * Output
            */
            $output = array(
                //"sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
            );

            foreach ($res as $r) {
                $row = array();

                $d = 'http://' . str_replace('http://', '', $r['ldreportcron']['SourceURL']);

                // $stats = 'Seo Stats for this Domain <br>---------------------------------------------<br> Google Page Rank - ' . intval($r['ldreportcron']['pr']) . ' <br> Citation Flow - ' . intval($r['ldreportcron']['cflow']) . ' <br> Trust Flow - ' . intval($r['ldreportcron']['tflow']) . ' <br> SeoMoz Rank - ' . intval($r['ldreportcron']['seomoz_rank']) . ' <br> External Back Links -  ' . intval($r['ldreportcron']['ext_backlinks']);


                $row[] = $r['ldreportcron']['AnchorText'];
                $row[] = '<a href="' . $d . '"  target="_blank">' . strtolower($r['ldreportcron']['SourceURL']) . '</a>';
                $row[] = date("d M Y", strtotime($r['ldreportcron']['FirstIndexedDate']));
                if ($r['ldreportcron']['index'] == '') {
                    $row[] = '<img src="' . BASEURL . 'app/webroot/images/loaders/circular/017.gif" alt="" />';
                } else {
                    $row[] = $r['ldreportcron']['index'] == '1' ? 'Yes' : 'No';
                }

                if ($r['ldreportcron']['pr'] == '') {
                    $row[] = '<img src="' . BASEURL . 'app/webroot/images/loaders/circular/017.gif" alt="" />';
                } else {
                    $row[] = $r['ldreportcron']['pr'];
                }
                $row[] = $r['ldreportcron']['LinkType'];
                $row[] = $r['ldreportcron']['FlagNoFollow'] == '1' ? 'No' : 'Yes'; //$r['ldreportcron']['FlagNoFollow'];
                $row[] = $r['ldreportcron']['SourceCitationFlow'];
                $row[] = $r['ldreportcron']['SourceTrustFlow'];


                $output['aaData'][] = $row;
            }

            //pr($output);
            echo json_encode($output);
            exit;
        }

        function exportdomainstatus()
        {
            $this->loadModel('Ldreportcron');
            $this->autoRender = false;
            $this->layout = 'ajax';
            App::import('Vendor', 'PHPExcel');

            // Create new PHPExcel object
            $objPHPExcel = new PHPExcel();

            // Set document properties
            $objPHPExcel->getProperties()->setCreator("SeoNitro Dashboard")
            ->setLastModifiedBy("SeoNitro Dashboard")
            ->setTitle("Domain Status")
            ->setSubject("Domain Status")
            ->setDescription("Domain Status with Status Code. Which Need to be fixed ASAP");

            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'SourceURL')
            ->setCellValue('B1', 'Anchor Text')
            ->setCellValue('C1', 'First Seen')
            ->setCellValue('D1', 'Indexed')
            ->setCellValue('E1', 'Page Rank')
            ->setCellValue('F1', 'LinkType')
            ->setCellValue('G1', 'Do Follow')
            ->setCellValue('H1', 'Citation Flow')
            ->setCellValue('I1', 'Trust Flow');

            $params = array(
                'conditions' => array("{$_SESSION['con']}"),
                'order' => ' Ldreportcron.id ASC ',
                //'limit' => 10,
                'recursive' => 1,
            );

            $allDomains = $this->Ldreportcron->find('all', $params);
            $inc = 2;
            foreach ($allDomains as $k => $d) {
                $SourceURL = $d['Ldreportcron']['SourceURL'];
                $AnchorText = $d['Ldreportcron']['AnchorText'];
                $FirstIndexedDate = $d['Ldreportcron']['FirstIndexedDate'];
                $index = $d['Ldreportcron']['index'] == '1' ? 'Yes' : 'No';
                $pr = $d['Ldreportcron']['pr'];
                $LinkType = $d['Ldreportcron']['LinkType'];
                $FlagNoFollow = $d['Ldreportcron']['FlagNoFollow'] == '1' ? 'No' : 'Yes';
                $SourceCitationFlow = $d['Ldreportcron']['SourceCitationFlow'];
                $SourceTrustFlow = $d['Ldreportcron']['SourceTrustFlow'];
                //$pass = $d['Domain']['apassword'];

                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $inc, $SourceURL)
                ->setCellValue('B' . $inc, $AnchorText)
                ->setCellValue('C' . $inc, $FirstIndexedDate)
                ->setCellValue('D' . $inc, $index)
                ->setCellValue('E' . $inc, $pr)
                ->setCellValue('F' . $inc, $LinkType)
                ->setCellValue('G' . $inc, $FlagNoFollow)
                ->setCellValue('H' . $inc, $SourceCitationFlow)
                ->setCellValue('I' . $inc, $SourceTrustFlow);
                $inc++;
            }

            // Rename worksheet
            $objPHPExcel->getActiveSheet()->setTitle('Domain Stauts');
            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);
            // Redirect output to a client’s web browser (Excel2007)
            $fileName = 'Ld_report_' . date('Y_m_d_h_i_s') . '.xlsx';
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $fileName . '"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');


            exit;
        }

        function ld_obls_link_cron()
        {
            set_time_limit(20000000000);
            //$url = 'http://www.junglejumps.com/';
            $this->loadModel('Ldreport');
            $alldomain = $this->Ldreport->find('all', array(
                'conditions' => "obls_link = 0",
                'fields' => "id,domain",
                'order' => array("Ldreport.id" => "desc"),
                'limit' => 2,
                'recursive' => 1
            ));

            if (!empty($alldomain)) {
                foreach ($alldomain as $row_val) {
                    $cnt = 0;
                    $id = $row_val['Ldreport']['id'];
                    $url = "http://www." . trim($row_val['Ldreport']['domain']) . "/";
                    $this->loadModel('simple_html_dom_node');
                    $html = $this->simple_html_dom_node->file_get_html($url);
                    $href_arr = array();

                    foreach ($html->find('a') as $element) {


                        $href = $element->href;
                        if (strstr($href, $url)) {
                            $href_arr[] = $href;
                        } else if (!strstr($href, 'http') && (!strstr($href, 'javascript') && (!strstr($href, '#')))) {
                            $href = $url . $href;
                            $href_arr[] = $href;
                        }
                    }
                    $href_arr = array_unique($href_arr);
                    if (count($href_arr) < 30) {
                        foreach ($href_arr as $link) {
                            $cnt += $this->ld_single_obls_link($link);
                            if ($cnt > 500)
                                break;
                        }
                    } else {
                        $cnt = 500;
                    }
                    //echo $cnt;
                    if ($cnt > 0)
                        $this->Ldreport->query("UPDATE `ldreport` SET `obls_link` = '$cnt' WHERE `id` = $id");
                }
            }
            exit('Success');
        }

        function ld_single_obls_link($url)
        {
            set_time_limit(20000000000);
            $this->loadModel('simple_html_dom_node');
            //$url = 'http://www.cricinfo.com/';
            $html = $this->simple_html_dom_node->file_get_html($url);
            $cnt = 0;
            foreach ($html->find('a') as $element) {


                $href = $element->href;

                //*
                if (preg_match('/(ftp|http(s)?:\/\/)(www\.)?(.)*[\.](.)*$/i', $href) && !strstr($href, $url)) {
                    $obl = "yes";
                    $cnt++;
                }
            }
            return $cnt;
            // echo  $cnt; exit;
        }


        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        function linkindexing()
        {
            //Configure::write('debug', 2);
            Configure::write('Cache.disable', true);
            $this->loadModel('Linkindexing');
            $userId = $this->Auth->user('id');
            if ($this->request->is('post')) {
                $this->Linkindexing->set($this->data['Linkindexing']);
                if ($this->Linkindexing->validates()) {
                    $batchData = trim($this->data['Linkindexing']['urls']);
                    $batchName = time() . '_RR_' . preg_replace("/\s/", "_", trim($this->data['Linkindexing']['title']));
                    $upStatus = $this->Linkindexing->uploadLinks($batchData, $batchName);
                    if (isset($upStatus->LinksAdded)) { // { LinksAdded: 10, LinksRejected: 1, RejectedLinks: ["REJECTEDURL"], BatchId: "GUID" }
                        $this->request->data['Linkindexing']['user_id'] = $userId;
                        $this->request->data['Linkindexing']['batch_name'] = $batchName;
                        $this->request->data['Linkindexing']['batch_id'] = $upStatus->BatchId;
                        if ($this->Linkindexing->save($this->data['Linkindexing'], false)) {
                            $eMsg = 'Your links has been sent to be indexed.';
                            $this->Session->setFlash($eMsg, 'flash_success');
                            $this->redirect($this->params['action']);

                        } else {
                            $eMsg = 'we are sorry, this could not be done right now, try later!';
                            $this->Session->setFlash($eMsg, 'flash_error');
                            $this->redirect($this->params['action']);
                        }
                    }
                } else {
                    $this->generateError('Linkindexing');
                }
            }
            $this->set('title_for_layout', 'Indexing Campaign');
        }

        function indexingreport()
        {
            //Configure::write('debug', 2);
            Configure::write('Cache.disable', true);
            $this->loadModel('Linkindexing');
            $userId = $this->Auth->user('id');

            $indexingReport = $this->Linkindexing->find('all', array('conditions' => "Linkindexing.user_id = $userId"));
            $this->set(compact('indexingReport'));
            $this->set('title_for_layout', 'Indexing Campaign Report');
        }

        function onehourindexing($id = 0, $batch_id = null)
        {
            Configure::write('Cache.disable', true);
            Configure::write('debug', 2);
            $this->autoRender = false;
            $this->layout = '';
            if (empty($id)) {
                echo 'Nothing Found to show!';
                exit;
            }
            $this->loadModel('Linkindexing');
            $this->Linkindexing->id = $id;
            $iR = $this->Linkindexing->find('first', array('conditions' => "Linkindexing.id = $id"));
            $this->set(compact('iR'));
            $batch_id = trim($iR['Linkindexing']['batch_id']);
            $batchData = $this->Linkindexing->getBatch($batch_id);
            $this->set(compact('batchData'));
            $this->set('title_for_layout', 'Indexing Report: URLS');
            return $this->render('onehourindexing');
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        function article($itemId = null)
        {
            $this->loadModel('Article');
            $userId = AuthComponent::user('id');
            if ($this->request->is('post')) {
                $this->request->data['Article']['user_id'] = $userId;
                //pr($this->data);exit;
                $this->Article->set($this->data['Article']);
                if ($this->Article->validates()) {
                    if ($itemId) {
                        $this->Article->id = $itemId;
                    }
                    if ($this->Article->save($this->data['Article'], false)) {
                        $fMsg = 'Content updated successfully.';
                        $this->Session->setFlash($fMsg, 'flash_success');
                    } else {
                        $fMsg = 'Content could not be updated.';
                        $this->Session->setFlash($fMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . '/' . $itemId);
                } else {
                    $this->generateError('Article');
                }
            } else {
                if ($itemId) {
                    $itemInfo = $this->Article->find('first', array('conditions' => array('id' => $itemId, 'user_id' => $userId)));
                    if ($itemInfo) {
                        $this->data = $itemInfo;
                    }
                }
            }

            $this->loadModel('Any');
            $this->Any->useTable = 'article_categories';
            $allCats = $this->Any->find('list', array('fields' => 'id,name'));
            $this->set(compact('allCats'));
            $this->set('title_for_layout', ($itemId ? 'Update' : 'Add') . ' article');
        }

        function articles()
        {
            $userId = AuthComponent::user('id');
            $this->loadModel('Article');
            $itemList = $this->Article->find('all', array('conditions' => "user_id = $userId AND requested NOT IN(1,2)", 'order' => 'id DESC'));
            $this->set(compact('itemList'));
            $this->set('title_for_layout', 'Manage Articles');
        }

        function order_article($itemId = null)
        {

            $this->loadModel('Article');

            $userId = AuthComponent::user('id');
            $serviceId = 17;
            $userServiceCredit = $this->checkUserServiceLimit($serviceId);

            if ($userServiceCredit['extra_credit_eligible']) {
                $extraCreditCharging = 'You have reached your allocated limit. You will be charged from your extra credit. You have: $' . $userServiceCredit['extra_credit'];
                $this->set(compact('extraCreditCharging'));
            }

            if (!$userServiceCredit['eligible'] && !$userServiceCredit['extra_credit_eligible']) {
                $notEligible = $this->render('not_eligible');
                $this->set(compact('notEligible'));
            } else {
                $unitPrice = floatval($userServiceCredit['unit_price']);

                $this->Article->validate['art_purpose'] = array(
                    'not_empty' => array(
                        'rule' => 'notEmpty',
                        'message' => 'Article purpose is required.',
                        'allowEmpty' => false,
                        'required' => true,
                    ),
                );
                $this->Article->validate['keywords'] = array(
                    'not_empty' => array(
                        'rule' => 'notEmpty',
                        'message' => 'Keywords are required.',
                        'allowEmpty' => false,
                        'required' => true,
                    ),
                );
                $userId = AuthComponent::user('id');
                if ($this->request->is('post')) {
                    unset($this->Article->validate['body']);
                    $this->request->data['Article']['user_id'] = $userId;
                    $this->Article->set($this->data['Article']);
                    if ($this->Article->validates()) {
                        $this->request->data['Article']['user_id'] = $userId;
                        $this->request->data['Article']['requested'] = 1;
                        //$this->request->data['Article']['request_date'] = date('Y-m-d H:i:s');
                        if ($itemId) {
                            $this->Article->id = $itemId;
                        }
                        if ($this->Article->save($this->data['Article'], false)) {
                            $fMsg = 'Request saved successfully.';
                            $this->Session->setFlash($fMsg, 'flash_success');

                            $updateAccount['user_id'] = $userId;
                            $updateAccount['service_id'] = $serviceId;
                            $updateAccount['amount'] = floatval($userServiceCredit['unit_price']);
                            $updateAccount['remarks'] = 'Credit deducted for Manual Posting.';
                            $this->updateAccountDebit($updateAccount);
                            if (!$userServiceCredit['eligible'] && $userServiceCredit['extra_credit_eligible']) {
                                $this->User->id = $userId;
                                $newExtraCredit = ($userServiceCredit['extra_credit'] - $unitPrice);
                                $this->User->saveField('extra_credit', $newExtraCredit);
                            }
                        } else {
                            $fMsg = 'Request could not be saved.';
                            $this->Session->setFlash($fMsg, 'flash_error');
                        }
                        //pr($this->data); exit;
                        $this->redirect($this->params['action']);
                    } else {
                        $this->generateError('Article');
                    }
                } else {
                    if ($itemId) {
                        $itemInfo = $this->Article->find('first', array('conditions' => array('id' => $itemId)));
                        if ($itemInfo) {
                            $this->data = $itemInfo;
                        }
                    }
                }
            }

            $itemList = $this->Article->find('all', array('conditions' => array('requested !=' => 0, 'body' => null), 'order' => 'id DESC'));
            $this->set(compact('itemList'));

            $this->loadModel('Any');
            $this->Any->useTable = 'categs';
            $allCategs = $this->Any->find('list', array('fields' => 'id,name', 'order' => 'name ASC'));
            $this->Any->useTable = null;

            $aLanguage = array('en' => 'English (US)', 'uk' => 'English (UK)', 'fr' => 'French', 'de' => 'German', 'es' => 'Spanish');
            $this->set(compact('aLanguage'));

            $this->set(compact('allCategs'));

            $this->set('title_for_layout', ($itemId ? 'Updating Order' : 'Order Article'));
        }

        function delitem($itemId = null, $modelName = null)
        {
            $this->loadModel('Any');
            $this->Any->delitem($itemId, $modelName, $this);
        }

        function confirm_iw_order($itemId = null)
        {
            $this->loadModel('Article');
            $this->Article->confirm_iw_order($itemId, $this);
        }

        function approve_article($itemId = null)
        {
            $this->loadModel('Article');
            $this->Article->approve_article($itemId, $this);
        }

        function new_modules($itemId)
        {
            $this->loadModel('Module');
            $this->loadModel('Lesson');
            $this->loadModel('Lessonfile');
            $this->loadModel('Lessoncomment');

            $this->Module->recursive = 2;

            $item = $this->Module->find('first', array(
                'conditions' => "Module.id = $itemId"
            ));

            $tLength = 0;
            if ($item['Lesson']) {
                foreach ($item['Lesson'] as $it) {
                    $tLength += $it['lesson_length'];
                }

            }
            $this->set(compact('item', 'tLength'));
            $this->set('title_for_layout', 'Modules');
        }

        function new_view_lesson($itemId)
        {
            $this->loadModel('Module');
            $this->loadModel('Lesson');
            $this->loadModel('Lessonfile');
            $this->loadModel('Lessoncomment');

            $this->Lesson->recursive = 1;
            $item = $this->Lesson->find('first', array(
                'conditions' => array('Lesson.id' => $itemId)
            ));

            $this->set(compact('item'));
            $this->set('title_for_layout', $item['Lesson']['title']);

            // find all playlist of this module

            $this->Lesson->unbindModel(
                array(
                    'hasMany' => array('Lessonfile', 'Lessoncomment'),
                    'belongsTo' => array('Module')
                )
            );

            $allLesson = $this->Lesson->find('all', array(
                'conditions' => array('Lesson.module_id' => $item['Lesson']['module_id']),
                'fields' => 'id,title'
            ));
            //pr($allLesson);
            $this->set(compact('allLesson'));


        }

        function new_view_page($itemId)
        {
            $this->loadModel('Menu');

            $item = $this->Menu->find('first', array(
                'conditions' => array('Menu.id' => $itemId)
            ));

            $this->set(compact('item'));
        }

        function new_view_bonus($itemId)
        {
            $this->loadModel('Bonus');

            $item = $this->Bonus->find('first', array(
                'conditions' => array('Bonus.id' => $itemId)
            ));

            $this->set(compact('item'));
        }

        public function new_downloadFile($fileName)
        {
            $fileName = base64_decode($fileName);
            $this->autoRender = false;
            $this->response->file(APP . 'webroot/' . $fileName, array('download' => true, 'name' => 'Current File'));
        }

        function community()
        {
            $this->layout = 'no-sidebar';
            $this->set('title_for_layout', "Community");
        }

    }
