<?php

    class SadminsController extends AppController
    {

        public $uses = array('User', 'Sadmin', 'Register', 'Campaign', 'Anchortext', 'Domain', 'Blogroll', 'Blogrolltext', 'Trackeddomain', 'Package', 'Credit', 'Mysite', 'Creditlog', 'Instruction', 'Setting');

        function beforeFilter()
        {
            Configure::write('debug', 2);
            $this->params['action'];
            if (!method_exists($this, $this->params['action'])) {
                $this->redirect('dashboard');
            }
            AuthComponent::$sessionKey = 'Auth.Usera';
            parent::beforeFilter();
        }

        function isAuthorized($user)
        {
            if (isset($user['role']) && 'admin' === $user['role']) {
                return true; //Admin can access every action
            } else {
                $this->redirect('/users/login');
            }
            return false; // The rest don't
        }

        function beforeRender()
        {
            App::uses('CakeNumber', 'Utility');
            parent::beforeRender();
            $siteCountAllCamp = $this->Campaign->find('all', array(
                'conditions' => array(""),
                'recursive' => 1
            ));
            $siteAllCountUser = $this->User->find('all', array(
                'conditions' => array("User.role = 'client' "),
                'recursive' => 1
            ));

            $this->set('siteAllCountUser', count($siteAllCountUser));
            $this->set('siteCountAllCamp', count($siteCountAllCamp));
        }

        // check if a user can edit, delete a record. I.e IS that belongs to him or not
        public function haveAuthToDoAction($model = '', $searchid = '', $relationalKey = '')
        {
            $cond = $model . '.' . $relationalKey . ' = ' . $this->Auth->user('id') . ' AND ' . $model . '.id = ' . $searchid;
            return $this->{$model}->find('count', array('recursive' => -1, 'conditions' => array($cond)));
        }

        // log out users
        function logout()
        {
            AuthComponent::$sessionKey = 'Auth.Usera';
            //$this->Session->setFlash('You\'ve successfully logged out.', 'flash_success');
            $this->redirect($this->Auth->logout());
        }

        function sendwelcomeemail()
        {
            $this->autoRender = false;
            $this->layout = '';
            die();
            // send an email to the user that his account has heen created
            $replace = array(
                '/\[firstname\]/',
                '/\[lastname\]/',
                '/\[username\]/',
                '/\[password\]/',
                '/\[email\]/',
            );
            $body = file_get_contents(WWW_ROOT . 'files/adduser.ctp');

            $users = $this->User->find('all', array('conditions' => array("User.role = 'client'")));
            foreach ($users as $k => $data) {
                $replacement = array(
                    $data['User']['firstname'],
                    $data['User']['lastname'],
                    $data['User']['username'],
                    $data['User']['ccode'],
                    $data['User']['email'],
                );
                $ebody = preg_replace($replace, $replacement, $body);
                echo 'sending email to - ' . $data['User']['email'] . ' ' . $data['User']['username'] . ' ' . $data['User']['ccode'] . '<br>';
                $this->sendEmail($data['User']['email'], 'Welcome to RankRatio Dashboard', $ebody);
                sleep(1);
            }
            $this->set('title_for_layout', 'sendwelcome');
        }

        // dashboard reports
        function dashboard()
        {
            // Total Active Domains
            $tD = $this->Domain->find('count', array('conditions' => '`status` = 1'));
            $this->set('tD', $tD);

            // Total Inactive Domains
            $tiD = $this->Domain->find('count', array('conditions' => " (`status` != 1 OR `httpcode` != '200' ) "));
            $this->set('pD', $tiD);

            // problem domain
            //$pD = $this->Domain->find('count', array('conditions' => "`httpcode` != '200'"));
            //$this->set('pD', $pD);

            $days = 15;
            $cond = " user_id = -1 AND  expires_on > NOW() AND expires_on <= '" . date('Y-m-d', strtotime("+15 day")) . "'";
            if ($this->request->is('post')) {
                $days = intval($this->data['Domain']['days']);
                $cond = " user_id = -1 AND  expires_on > NOW() AND expires_on <= '" . date('Y-m-d', strtotime("+$days day")) . "'";
                if ($days == 0) {
                    $cond = " user_id = -1 AND  expires_on <= '" . date('Y-m-d', strtotime("+$days day")) . "' ";
                }
            }
            $params = array(
                'conditions' => array($cond),
                'order' => ' Domain.id ASC ',
                'recursive' => 1,
            );
            //echo $cond; exit;
            $allDomain = $this->Domain->find('all', $params);
            $this->set(compact('allDomain'));
            $this->set('days', $days);
            $this->set('title_for_layout', 'dashboard');
        }

        function services()
        {
            $this->uses[] = 'Service';
            $allServices = $this->Service->find('all');
            $this->set(compact('allServices'));

            $this->set('title_for_layout', 'Manage Services');
        }

        function addservice()
        {
            if ($this->request->is('post')) {
                //pr($_POST); exit;
                $this->uses[] = 'Service';
                $this->Service->set($this->data['Service']);
                if ($this->Service->validates()) {
                    if ($this->Service->save($this->data['Service'], false)) {
                        $eMsg = 'New Service has been added successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'New Service could not be added.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action']);
                } else {
                    $this->generateError('Service');
                }
            }

            $this->set('title_for_layout', 'Add Service');
        }

        function editservice($sId = null)
        {
            $this->uses[] = 'Service';
            if ($this->request->is('post')) {
                $this->Service->set($this->data['Service']);
                if ($this->Service->validates()) {
                    if ($this->Service->save($this->data['Service'], false)) {
                        $eMsg = 'Service has been updated uccessfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'Could not Update Service.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . '/' . $sId);
                } else {
                    $this->generateError('Service');
                }
            }

            $data = $this->Service->findById($sId);

            $this->data = $data;
            $this->set('data', $data);
            $this->set('title_for_layout', 'Edit Service');
        }

        function packages()
        {
            $allPackages = $this->Package->find('all');
            $this->set(compact('allPackages'));

            $this->set('title_for_layout', 'Manage Packages');
        }

        function addpackage()
        {
            if ($this->request->is('post')) {
                $this->Package->set($this->data['Package']);
                if ($this->Package->validates()) {
                    $transactionFlag = true;
                    $packageId = 0;
                    $this->Package->begin();

                    if ($transactionFlag) {
                        if (!$this->Package->save($this->data['Package'])) {
                            $transactionFlag = false;
                        } else {
                            $packageId = $this->Package->getLastInsertId();
                        }
                    }
                    if ($transactionFlag) {
                        if (isset($this->data['Serviceprice']['unitprice'])) {
                            $this->uses[] = 'Serviceprice';
                            $servicePrice = array();
                            $count = 0;
                            foreach ($this->data['Serviceprice']['unitprice'] as $k => $price) {
                                $servicePrice[$count]['service_id'] = intval($k);
                                $servicePrice[$count]['package_id'] = intval($packageId);
                                //$servicePrice[$count]['unitprice'] = floatval($price);
                                $servicePrice[$count]['servicelimit'] = intval($this->data['Serviceprice']['servicelimit'][$k]);
                                //$servicePrice[$count]['status'] = $this->data['Serviceprice']['status'][$k];
                                $count++;
                            }
                            if (!$this->Serviceprice->saveAll($servicePrice)) {
                                $transactionFlag = false;
                            }
                        }
                    }
                    if ($transactionFlag) {
                        $this->Package->commit();
                        $eMsg = 'New Package has been added successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'New Package could not be added.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action']);
                } else {
                    $this->generateError('Package');
                }
            }

            $this->uses[] = 'Service';
            $cond = array(
                'conditions' => 'Service.status = 1'
            );
            $serviceList = $this->Service->find('all', $cond);

            $this->set(compact('serviceList'));

            $this->set('title_for_layout', 'Add Package');
        }

        function editpackage($pId = null)
        {
            if ($this->request->is('post')) {

                $this->Package->set($this->data['Package']);
                if ($this->Package->validates()) {
                    $transactionFlag = true;
                    $this->Package->begin();

                    if ($transactionFlag) {
                        if (!$this->Package->save($this->data['Package'])) {
                            $transactionFlag = false;
                        }
                    }
                    if ($transactionFlag) {
                        if (isset($this->data['Serviceprice']['unitprice'])) {
                            $this->uses[] = 'Serviceprice';
                            $servicePrice = array();
                            $count = 0;
                            foreach ($this->data['Serviceprice']['unitprice'] as $k => $price) {
                                $servicePrice[$count]['service_id'] = intval($k);
                                $servicePrice[$count]['package_id'] = intval($pId);
                                //$servicePrice[$count]['unitprice'] = floatval($price);
                                $servicePrice[$count]['servicelimit'] = intval($this->data['Serviceprice']['servicelimit'][$k]);
                                //$servicePrice[$count]['status'] = $this->data['Serviceprice']['status'][$k];
                                $count++;
                            }
                            if (!$this->Serviceprice->deleteAll(array('Serviceprice.package_id' => $pId))) {
                                $transactionFlag = false;
                            }
                            if (!$this->Serviceprice->saveAll($servicePrice)) {
                                $transactionFlag = false;
                            }
                        }
                    }
                    if ($transactionFlag) {
                        $this->Package->commit();
                        $eMsg = 'Package has been updated uccessfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'Could not Update Package.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . '/' . $pId);
                } else {
                    $this->generateError('Package');
                }
            }

            $this->uses[] = 'Service';
            $cond2 = array(
                'conditions' => 'Service.status = 1'
            );
            $serviceList = $this->Service->find('all', $cond2);

            //pr($serviceList);
            //exit;

            $this->set(compact('serviceList'));

            $this->uses[] = 'Serviceprice';
            $cond3 = array(
                'conditions' => "Serviceprice.package_id = $pId"
            );
            $servicePrices = $this->Serviceprice->find('all', $cond3);

            $currentPrices = array();
            foreach ($serviceList as $k => $service) {
                $serviceId = $service['Service']['id'];
                $defaultUnitPrice = $service['Service']['default_unit_price'];
                foreach ($servicePrices as $sP) {
                    if ($sP['Serviceprice']['service_id'] == $serviceId) {
                        $currentPrices['unitprice'][$serviceId] = $defaultUnitPrice;
                        $currentPrices['servicelimit'][$serviceId] = $sP['Serviceprice']['servicelimit'];
                        $currentPrices['status'][$serviceId] = $sP['Serviceprice']['status'];
                    }
                }
            }

            $data = $this->Package->findById($pId);

            $data['Serviceprice'] = $currentPrices;
            //pr($data);; exit;
            $this->data = $data;
            $this->set('data', $data);
            $this->set('cId', $pId);
            $this->set('title_for_layout', 'Edit Package');
        }

        function newclient()
        {
            if ($this->request->is('post')) {

                $givenPass = $this->data['User']['password'];
                $this->request->data['User']['password'] = (trim($this->data['User']['password']) != '') ? AuthComponent::password($this->data['User']['password']) : '';
                $this->request->data['User']['next_package_id'] = $packageId = $this->data['User']['package_id'];
                $this->request->data['User']['package_renew_date'] = date('Y-m-d');
                $this->request->data['User']['package_apply_date'] = date('Y-m-d');
                $this->request->data['User']['lastsignin'] = date('Y-m-d G:i:s');
                $this->request->data['User']['status'] = '1';
                $this->request->data['User']['verified'] = '1';
                $this->request->data['User']['ccode'] = trim($givenPass);
                $this->request->data['User']['created_by'] = 'Admin';
                $this->Register->set($this->data['User']);

                if ($this->Register->validates()) {
                    if ($this->User->save($this->data['User'], false)) {
                        $userId = $this->User->getLastInsertId();
                        $this->uses[] = 'Serviceprice';
                        $servicePricesByPackage = $this->Serviceprice->find('all', array('conditions' => "package_id = $packageId"));

                        $userCredit = array();
                        $count = 0;
                        foreach ($servicePricesByPackage as $k => $sPBP) {
                            $userCredit[$count]['user_id'] = intval($userId);
                            $userCredit[$count]['service_id'] = intval($sPBP['Serviceprice']['service_id']);
                            $count++;
                        }
                        if ($userCredit) {
                            $this->uses[] = 'Usercredit';
                            $this->Usercredit->set($userCredit);
                            if ($this->Usercredit->saveAll($userCredit)) {
                                //$this->User->saveField('currentcredit', $totalPrice);
                                $eMsg = 'New Client has been added successfully.';
                                $this->Session->setFlash($eMsg, 'flash_success');

                                // send an email to the user that his account has heen created
                                $replace = array(
                                    '/\[firstname\]/',
                                    '/\[lastname\]/',
                                    '/\[username\]/',
                                    '/\[password\]/',
                                    '/\[email\]/',
                                );
                                $body = file_get_contents(WWW_ROOT . 'files/adduser.ctp');
                                $replacement = array(
                                    $this->data['User']['firstname'],
                                    $this->data['User']['lastname'],
                                    $this->data['User']['username'],
                                    trim($givenPass),
                                    $this->data['User']['email'],
                                );
                                $ebody = preg_replace($replace, $replacement, $body);
                                $this->sendEmail($this->data['User']['email'], 'Welcome to RankRatio Dashboard', $ebody);
                            } else {
                                $this->User->deleteAll(array('User.id' => $userId), false);
                                $eMsg = 'Client Credit info could not be added.';
                                $this->Session->setFlash($eMsg, 'flash_error');
                            }
                        } else {
                            $eMsg = 'New Client has been added successfully.';
                            $this->Session->setFlash($eMsg, 'flash_success');
                        }
                    } else {
                        $eMsg = 'Could not save user data';
                        $this->Session->setFlash($eMsg, 'flash_error');
                        //$errors = $this->User->validationErrors;
                        // pr($errors);
                    }
                    $this->redirect($this->params['action']);
                } else {
                    $this->request->data['User']['password'] = $givenPass;
                    $this->generateError('Register');
                }
            }

            // Find all the packages
            //$packages = array('Paid' => array(), 'Free' => array());
            $packages = $this->Package->find('list', array('fields' => 'id,packagename'));
            //pr($package);
            $this->set('packageList', $packages);
            $this->set('title_for_layout', 'Add New Client');
        }

        function manageclients()
        {
            $allUser = $this->User->find('all', array(
                'conditions' => array("User.role = 'client' "),
                'order' => array('User.firstname ASC'),
                'recursive' => 1,
            ));
            $this->set(compact('allUser'));

            $this->set('title_for_layout', 'Manage Clients');
        }

        function monthlyrenewals()
        {
            $this->loadModel('Monthlyrenewal');

            $this->Monthlyrenewal->bindModel(array(
                'hasOne' => array(
                    'User' => array('foreignKey' => false, 'conditions' => "Monthlyrenewal.user_id = User.id"),
                )
            ));

            $allUsers = $this->Monthlyrenewal->find('all', array('conditions' => "Monthlyrenewal.status=0", 'order' => 'Monthlyrenewal.renew_date DESC'));

            //pr($allUsers);
            //exit;

            $this->set(compact('allUsers'));

            $this->set('title_for_layout', 'Monthly Renewal');
        }

        function renewal_action($action, $itemId)
        {

            if (empty($action) || empty($itemId)) {
                return '';
            }
            $this->autoRender = false;
            $this->layout = 'ajax';

            $status = 99999;
            if ($action == 'paid') {
                $status = 1;
            } elseif ($action == 'unpaid') {
                $status = 0;
            }

            if ($status == 0 || $status == 1) {
                $this->loadModel('Monthlyrenewal');
                $this->Monthlyrenewal->id = $itemId;
                if ($this->Monthlyrenewal->saveField('status', $status)) {
                    $eMsg = 'Status updated successfully!';
                    $this->Session->setFlash($eMsg, 'flash_success');
                    echo '';
                } else {
                    echo 'error';
                }
            } else {
                echo 'error';
            }
            exit;
        }

        function viewclient($id)
        {
            $data = $this->User->find('first', array(
                'conditions' => array("User.id = " . $id . " AND User.role = 'client' "),
                'recursive' => 1
            ));
            $this->set(compact('data'));
            $this->set('title_for_layout', 'View Client');
        }

        function editclient($uId = null)
        {
            if ($this->request->is('post')) {
                if ($this->User->validates()) {
                    if (trim($this->data['UserEx']['password']) != '') {
                        $this->request->data['User']['password'] = AuthComponent::password($this->data['UserEx']['password']);
                    }
                    $this->request->data['User']['next_package_id'] = $this->data['User']['package_id'];
                    $this->request->data['User']['package_id'] = $this->data['User']['package_id'];
                    $this->request->data['User']['package_apply_date'] = date('Y-m-d');
                    if ($this->User->save($this->data['User'], false)) {

                        $this->uses[] = 'Usercredit';
                        //$priceHas = $this->Usercredit->find('count', array('conditions' => "Usercredit.user_id = $uId"));

                        $this->uses[] = 'Serviceprice';
                        $packageId = $this->data['User']['package_id'];
                        $servicePricesByPackage = $this->Serviceprice->find('all', array('conditions' => "package_id = $packageId"));

                        $userCredit = array();
                        foreach ($servicePricesByPackage as $k => $sPBP) {
                            $userCredit['user_id'] = intval($uId);
                            $userCredit['service_id'] = intval($sPBP['Serviceprice']['service_id']);
                            $this->Usercredit->set($userCredit);
                            $this->Usercredit->save($userCredit);
                        }

                        $eMsg = 'Client information has been updated successfully';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        // set msg flash
                        $eMsg = 'Could not update Client information';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . '/' . $uId);
                } else {
                    $this->generateError('User');
                }
            }
            // Find all the packages
            $packages = array('Paid' => array(), 'Free' => array());
            $package = $this->Package->find('all');
            if (count($package) > 0) {
                foreach ($package as $p) {
                    if ($p['Package']['type'] == 1) {
                        $packages[$p['Package']['id']] = $p['Package']['packagename'];
                    } else {
                        $packages[$p['Package']['id']] = $p['Package']['packagename'];
                    }
                }
            }

            $this->set('packages', $packages);

            $packageList = $this->Package->find('list', array('fields' => 'id,packagename'));

            $this->set('packageList', $packageList);

            $data = $this->User->findById($uId);
            $this->data = $data;

            $this->set('data', $data);
            $this->set('cId', $uId);
            $this->set('title_for_layout', 'Edit Client');
        }

        function deleteclient($id)
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';

            $this->User->id = $id;
            $oData = $this->User->read();
            if ($this->User->delete($id, false)) {
                //$this->Mysite->deleteall(array('Mysite.user_id' => $id), false);
                // delete all the anchor recored under this campaign
                $eMsg = 'Client with all records has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        function newdomain()
        {
            if ($this->request->is('post')) {
                $this->Domain->set($this->data['Domain']);
                if ($this->Domain->validates()) {
                    $loginOk = 'success';
                    $user_id = -1;
                    if ($this->data['Domain']['networktype'] == 1) {
                        $user_id = $this->data['Domain']['user_id'];
                    }
                    $this->request->data['domain'] = $this->getPlainDomain($this->data['Domain']['domain']);
                    $this->request->data['user_id'] = $user_id;
                    $this->request->data['adddate'] = date('Y-m-d G:i:s', time());
                    $this->request->data['expires_on'] = date('Y-m-d', strtotime($this->data['Domain']['expires_on']));

                    if ($this->Domain->save($this->data['Domain'], false)) {
                        // set msg flash
                        $eMsg = 'New Domain has been added successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'New Domain could not be created.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action']);
                } else {
                    $this->generateError('Domain');
                }
            }
            $this->User->bindModel(array(
                'hasMany' => array(
                    'Domain' => array('foreignKey' => 'user_id', 'conditions' => "Domain.owner_id = -1"),
                )
            ));

            $this->uses[] = 'Serviceprice';
            $this->User->bindModel(array(
                'belongsTo' => array(
                    'Serviceprice' => array('foreignKey' => false, 'conditions' => 'Serviceprice.package_id = User.package_id AND Serviceprice.service_id = 1'),
                )
            ));

            $clientList = $this->User->find('all', array('conditions' => "User.role = 'client'", 'order' => 'User.firstname ASC'));

            $users = array();

            if ($clientList) {
                $users[-1] = 'SEONitro';
                foreach ($clientList as $key => $client) {
                    $serviceLimit = $client['Serviceprice']['servicelimit'];
                    if ($serviceLimit > 0) {
                        $siteAssigned = count($client['Domain']);
                        $siteDue = $serviceLimit - $siteAssigned;
                        $siteDue = $siteDue >= 0 ? $siteDue : 0;
                        $uId = $client['User']['id'];
                        $users[$uId] = $client['User']['fullname'] . " (Due MySite: $siteDue out of $serviceLimit)";
                    }
                }
            }
            $this->set(compact('users'));

            $this->set('title_for_layout', 'Add New Domain');
        }

        /* ---------------------------------------------
        *
        *  Edit Domain
        */

        function editdomain($cId = null)
        {

            // if action is present in referer url then it seems same action send the request to keep the previous one intact
            //echo strpos($this->request->referer(), $this->params['action']);
            if (strpos($this->request->referer(), $this->params['action']) === false) {
                $_SESSION['refrr'] = $this->request->referer();
            }
            // pr($_SESSION);

            $data = $this->Domain->findById($cId);

            if ($this->request->is('post')) {
                $this->Domain->set($this->data['Domain']);
                $this->request->data['Domain']['domain'] = $this->getPlainDomain($this->data['Domain']['domain']);
                $this->request->data['expires_on'] = date('Y-m-d', strtotime($this->data['Domain']['expires_on']));

                if ($this->Domain->validates()) {
                    $loginOk = true;

                    $user_id = -1;
                    if ($this->data['Domain']['networktype'] == 1) {
                        $user_id = $this->data['Domain']['user_id'];
                    }

                    if ($data['Domain']['user_id'] != $user_id) {
                        $this->request->data['Domain']['cusername'] = '';
                        $this->request->data['Domain']['cpassword'] = '';
                    }
                    $this->request->data['Domain']['user_id'] = $user_id;
                    $this->Domain->save($this->data['Domain'], false);
                    $eMsg = 'Domain has been updated successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                    $this->redirect(isset($_SESSION['refrr']) ? $_SESSION['refrr'] : $this->params['action']);
                } else {
                    $this->generateError('Domain');
                }
            }

            $this->data = $data;

            $this->User->bindModel(array(
                'hasMany' => array(
                    'Domain' => array('foreignKey' => 'user_id', 'conditions' => "Domain.owner_id = -1"),
                )
            ));

            $this->uses[] = 'Serviceprice';
            $this->User->bindModel(array(
                'belongsTo' => array(
                    'Serviceprice' => array('foreignKey' => false, 'conditions' => 'Serviceprice.package_id = User.package_id AND Serviceprice.service_id = 1'),
                )
            ));

            $clientList = $this->User->find('all', array('conditions' => "User.role = 'client'", 'order' => 'User.firstname ASC'));

            $users = array();

            if ($clientList) {
                $users[-1] = 'SEONitro';
                foreach ($clientList as $key => $client) {
                    $serviceLimit = $client['Serviceprice']['servicelimit'];
                    if ($serviceLimit > 0) {
                        $siteAssigned = count($client['Domain']);
                        $siteDue = $serviceLimit - $siteAssigned;
                        $siteDue = $siteDue >= 0 ? $siteDue : 0;
                        $uId = $client['User']['id'];
                        $users[$uId] = $client['User']['fullname'] . " (Due MySite: $siteDue out of $serviceLimit)";
                    }
                }
            }
            $this->set(compact('users'));
            $this->set('data', $data);
            $this->set('cId', $cId);
            $this->set('title_for_layout', 'Edit Site');
        }

        function deletedomain($id)
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';

            $this->Domain->id = $id;
            $oData = $this->Domain->read();
            if ($this->Domain->delete($id, false)) {
                // delete all the anchor recored under this campaign
                $eMsg = 'Domain with all records has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        // put a domain under maintanance mode or live mode
        function statuschange($id = null, $status = 'ok')
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->Domain->id = $id;
            $dt = array(
                'livestatus' => 'problematic',
                'status' => 0
            );
            if ($status == 'ok') {
                $dt = array(
                    'livestatus' => 'ok',
                    'status' => 1, //  no post or no action will be working if stauts is 0
                    'httpcode' => '200',
                    'httpstatus' => 'OK'
                );
            }
            if ($this->Domain->save($dt, false)) {
                $eMsg = "Domain has been marked as " . strtoupper($status);
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        // 1 = mysites, 2 = manula sites, 3 = premium sites, 4 = prime sites, 5 = lowgrade sites, 6 = guest blog
        function managemanualsites()
        {
            $params = array(
                'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 2'),
                'order' => ' Domain.domain ASC ',
                'recursive' => 1
            );
            $allDomains = $this->Domain->find('all', $params);
            $this->set(compact('allDomains'));
            $this->set('title_for_layout', 'Manage Manual Sites');
        }

        function manageautosites()
        {
            $params = array(
                'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 3'),
                'order' => ' Domain.domain ASC ',
                'recursive' => 1
            );
            $allDomains = $this->Domain->find('all', $params);
            $this->set(compact('allDomains'));
            $this->set('title_for_layout', 'Manage Automation Sites');
        }

        // automated premium sites, id = 3 (pr 3)
        function managepremium()
        {
            $params = array(
                'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 3'),
                'order' => ' Domain.domain ASC ',
                'recursive' => 1
            );
            $allDomains = $this->Domain->find('all', $params);
            $this->set(compact('allDomains'));
            $this->set('title_for_layout', 'Manage Premium Automation Sites');
        }

        // automated premium sites, id = 4 (pr 2/1)
        function manageprime()
        {
            $params = array(
                'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 4'),
                'order' => ' Domain.domain ASC ',
                'recursive' => 1
            );
            $allDomains = $this->Domain->find('all', $params);
            $this->set(compact('allDomains'));
            $this->set('title_for_layout', 'Manage Prime Automation Sites');
        }

        // automated premium sites, id = 5 (pr 0)
        function managelowgrade()
        {
            $params = array(
                'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 5'),
                'order' => ' Domain.domain ASC ',
                'recursive' => 1
            );
            $allDomains = $this->Domain->find('all', $params);
            $this->set(compact('allDomains'));
            $this->set('title_for_layout', 'Manage LowGrade Automation Sites');
        }

        /*
        // Guest Blog sites (pr >= 4)
        function manageguestsites() {
        $params = array(
        'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 6'),
        'order' => ' Domain.domain ASC ',
        'recursive' => 1
        );
        $allDomains = $this->Domain->find('all', $params);
        $this->set(compact('allDomains'));

        $this->set('title_for_layout', 'Manage Guest Blogging Sites');
        }

        */

        // All types of domains
        function managedomains()
        {
            /*
            $this->loadModel('Commonmodel');
            $this->Commonmodel->useTable = 'dmclass';
            $params = array(
            'conditions' => null, // array(' Domain.owner_id = -1'),
            'order' => ' Domain.domain ASC ',
            'recursive' => 1,
            //'limit' => 50,
            //'fields' => 'Domain.'
            );
            $this->Domain->bindModel(array(
            'belongsTo' => array(
            'User' => array('foreignKey' => 'user_id', 'conditions' => array("User.role = 'client'"), 'fields' => 'User.firstname,User.lastname,User.id'),
            'Commonmodel' => array('foreignKey' => 'networktype', 'fields' => 'Commonmodel.name,Commonmodel.id'),
            )
            ));
            $allDomains = $this->Domain->find('all', $params);
            //pr($allDomains); exit;
            $this->set(compact(array('allDomains')));
            */
            $this->set('title_for_layout', 'Manage All Domains');
        }

        // database ajax
        function getdomainsajax()
        {
            $this->autoRender = false;
            $this->layout = '';
            Configure::write('debug', 2);
            /* Array of database columns which should be read and sent back to DataTables. Use a space where
            * you want to insert a non-database field (for example a counter or static image)
            */
            $aColumns = array('`Domain`.`domain`', '`User`.`firstname`', '`Domainclass`.`name`', '`Domain`.`pr`', '`Domain`.`cflow`', '`Domain`.`tflow`', '`Domain`.`ljpr`', '`Domain`.`seomoz_rank`', '`Domain`.`ext_backlinks`', '`Domain`.`expires_on`', '`Domain`.`cms`');

            /*
            * Paging
            */
            $sLimit = "";
            if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
                $sLimit = "LIMIT " . intval($_GET['iDisplayStart']) . ", " .
                intval($_GET['iDisplayLength']);
            }
            /*
            * Ordering
            */
            $sOrder = "";
            if (isset($_GET['iSortCol_0'])) {
                $sOrder = "ORDER BY  ";
                for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                    if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                        $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] .
                        ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                    }
                }

                $sOrder = substr_replace($sOrder, "", -2);
                if ($sOrder == "ORDER BY") {
                    $sOrder = "";
                }
            } else {
                $sOrder = " ORDER BY  `Domain`.`domain` ASC";
            }
            /*
            * Filtering
            * NOTE this does not match the built-in DataTables filtering which does it
            * word by word on any field. It's possible to do here, but concerned about efficiency
            * on very large tables, and MySQL's regex functionality is very limited
            */
            $sWhere = "";
            if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
                $sWhere = "WHERE (";
                for ($i = 0; $i < count($aColumns); $i++) {
                    $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
                }
                $sWhere = substr_replace($sWhere, "", -3);
                $sWhere .= ')';
            }

            /* Individual column filtering */
            for ($i = 0; $i < count($aColumns); $i++) {
                if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                    if ($sWhere == "") {
                        $sWhere = "WHERE ";
                    } else {
                        $sWhere .= " AND ";
                    }
                    $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch_' . $i] . "%' ";
                }
            }


            $cols = "`Domain`.`id`, `Domain`.`domain`, `Domain`.`expires_on`, `Domain`.`created`, `Domain`.`pr`, `Domain`.`cflow`, `Domain`.`tflow`, `Domain`.`ljpr`,  `Domain`.`seomoz_rank`, `Domain`.`ext_backlinks`, `Domain`.`cms`, `Domain`.`ausername`, `Domain`.`apassword`, `Domain`.`cusername`, `Domain`.`status`, `User`.`firstname`, `User`.`lastname`, `User`.`id`, `Domainclass`.`name`,`Domainclass`.`id`";
            $q = "SELECT SQL_CALC_FOUND_ROWS $cols FROM `domains` AS `Domain` LEFT JOIN `users` AS `User` ON (`Domain`.`user_id` = `User`.`id`) LEFT JOIN `dmclass` AS `Domainclass` ON (`Domain`.`networktype` = `Domainclass`.`id`) $sWhere $sOrder $sLimit"; // $sWhere $sOrder $sLimit  WHERE 1 = 1 ORDER BY `Domain`.`domain` ASC LIMIT 100


            $res = $this->Domain->query($q);
            /* Data set length after filtering */
            $resC = $this->Domain->query("SELECT FOUND_ROWS()");
            $iFilteredTotal = $resC[0][0]['FOUND_ROWS()'];
            /* Total data set length */
            $iTotal = $this->Domain->find('count', array('conditions' => null));

            /*
            * Output
            */
            $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
            );
            $color = array(
                '1' => '#ED7A53',
                '2' => '#0e830e',
                '3' => '#1baa47',
                '4' => '#b40b8a',
                '5' => '#a06508',
                '6' => '#09aea0',
                '7' => '#AAAAAA',
            );
            foreach ($res as $r) {
                $clor = $r['Domainclass']['id']; //$color[$r['Domainclass']['id']];
                $row = array();
                $d = 'http://' . str_replace('http://', '', $r['Domain']['domain']);
                $row[] = '<a href="' . $d . '"  target="_blank">' . strtolower($r['Domain']['domain']) . '</a>';

                $row[] = ($r['User']['firstname'] != '' ? '<span style="color:#b40b8a;">' . $r['User']['firstname'] . ' ' . $r['User']['lastname'] . '</span>' : 'SeoNitro');
                $row[] = '<span style="color:' . $clor . ';">' . $r['Domainclass']['name'] . '</span>';
                $row[] = $r['Domain']['pr'];
                $row[] = $r['Domain']['cflow'];
                $row[] = $r['Domain']['tflow'];
                $row[] = $r['Domain']['ljpr'];
                $row[] = $r['Domain']['seomoz_rank'];
                $row[] = $r['Domain']['ext_backlinks'];
                $row[] = $r['Domain']['cms'] . '<br>' . $r['Domain']['ausername'] . '<br>' . $r['Domain']['apassword'];

                $row[] = '<div class="controls" style="text-align:left !important;">
                <a href="' . Router::url('detailsdomain/' . $r['Domain']['id']) . '" title="See Details about this Domain" class="tip">&rArr;details</a> <br>
                <!--a href="' . Router::url('editdomain/' . $r['Domain']['id']) . '" title="Edit This Domain?" class="tip">
                &rArr;edit</a> <br>
                <a href="' . Router::url('deletedomain/' . $r['Domain']['id']) . ' " title="Remove This Domain?" class="tip callAction">&rArr;delete</a>
                <br-->
                <a target="_blank" href="http://core.seoplushosting.com:2080/nodeworx/siteworx?action=siteworxLoginCommit&login_domain=' . strtolower($r['Domain']['domain']) . '" title="Go SiteWorx Account" class="tip">&rArr; Go SiteWorx</a>
                </div>';
                $output['aaData'][] = $row;
            }

            //pr($output);
            echo json_encode($output);
            exit;
        }

        // end function
        // Move domain from one network to another
        function movedomains()
        {

            $nArray = array('1' => 'MySites', '2' => 'Manual Networks', '3' => 'Premium', '4' => 'Prime', '5' => 'Low Grade', '6' => 'Guest Blog');

            if ($this->request->is('post')) {
                $networkType = intval($this->data['Moveto']['networktype']);
                if ($networkType) {
                    if (count($this->data['Moveto']['ids'])) {
                        $ids = implode(",", array_keys($this->data['Moveto']['ids']));
                        $q = 'Domain.id IN (' . $ids . ')';
                        //echo $networkType.' '. $q;
                        if ($this->Domain->updateAll(
                            array('Domain.networktype' => $networkType, 'Domain.user_id' => '-1'), array($q)
                            )
                        ) {
                            $eMsg = 'Selected Domain has been Moved to ' . $nArray[$networkType];
                            $this->Session->setFlash($eMsg, 'flash_success');
                        }
                    } else {
                        $eMsg = ' You did not select domains to move. ';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                } else {
                    $eMsg = ' You did not select a network where domains to move';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
            }

            $this->loadModel('Commonmodel');
            $this->Commonmodel->useTable = 'dmclass';
            $params = array(
                'conditions' => array(" Domain.owner_id = -1 "),
                'order' => ' Domain.domain ASC ',
                'recursive' => 1,
            );
            $this->Domain->bindModel(array(
                'belongsTo' => array(
                    'Commonmodel' => array('foreignKey' => 'networktype', 'fields' => 'Commonmodel.name,Commonmodel.id'),
                )
            ));
            $allDomains = $this->Domain->find('all', $params);
            //pr($allDomains);
            $this->set(compact(array('allDomains')));
            $this->set('title_for_layout', 'Moving Domain inter network');
        }

        // MySites
        function managemysites()
        {
            $params = array(
                'conditions' => array(' Domain.networktype = 1 AND owner_id = -1'),
                //'limit' => 20,
                'order' => ' Domain.id ASC ',
                'recursive' => 1
            );

            $this->Domain->bindModel(array(
                'hasOne' => array(
                    'User' => array('foreignKey' => false, 'conditions' => array('Domain.user_id = User.id')),
                )
            ));
            $allDomains = $this->Domain->find('all', $params);
            $this->set(compact('allDomains'));

            $this->set('title_for_layout', 'Manage My Sites');
        }

        // MySites
        function manageclprvsites()
        {
            $params = array(
                'conditions' => array(' Domain.networktype = 1 AND owner_id > 1'),
                //'limit' => 20,
                'order' => ' Domain.id ASC ',
                'recursive' => 1
            );

            $this->Domain->bindModel(array(
                'hasOne' => array(
                    'User' => array('foreignKey' => false, 'conditions' => array('Domain.user_id = User.id')),
                )
            ));
            $allDomains = $this->Domain->find('all', $params);
            $this->set(compact('allDomains'));

            $this->set('title_for_layout', 'Manage Client Private Sites');
        }

        function assignedmysites()
        {

            $allDomains = $this->paginate('Domain');

            $this->set(compact('allDomains'));

            $this->Domain->bindModel(array(
                'hasOne' => array(
                    'User' => array('foreignKey' => false, 'conditions' => "Domain.user_id = User.id", 'fields' => 'id,fullname'),
                )
            ));


            $clientList = $this->Domain->find('all', array('conditions' => "Domain.owner_id = -1 AND Domain.user_id != -1 AND Domain.networktype = 1"));

            //pr($clientList);exit;

            $this->set(compact('clientList'));
            $this->set('title_for_layout', 'Manage Assigned Mysites');
        }

        function assignedclients()
        {

            $allDomains = $this->paginate('Domain');

            $this->set(compact('allDomains'));

            $this->User->bindModel(array(
                'hasMany' => array(
                    'Domain' => array('foreignKey' => 'user_id', 'conditions' => "Domain.owner_id = -1"),
                )
            ));

            $this->uses[] = 'Serviceprice';
            $this->User->bindModel(array(
                'belongsTo' => array(
                    'Serviceprice' => array('foreignKey' => false, 'conditions' => 'Serviceprice.package_id = User.package_id AND Serviceprice.service_id = 1'),
                )
            ));

            $clientList = $this->User->find('all', array('conditions' => "User.role = 'client'"));

            $this->set(compact('clientList'));
            $this->set('title_for_layout', 'Manage Assigned Mysites');
        }

        function clientmysites($userId = null)
        {

            if ($this->request->is('post')) {
                $checkedDomains = $this->data['Checked'];
                $updateArr = array();
                if ($checkedDomains) {
                    $count = 0;
                    foreach ($checkedDomains as $key => $val) {
                        $updateArr[$count]['id'] = $key;
                        $updateArr[$count]['user_id'] = -1;
                        $count++;
                    }
                    $fieldsArr = array(
                        'fieldList' => array(
                            'id', 'user_id'
                    ));
                    if ($this->Domain->saveAll($updateArr, $fieldsArr)) {
                        $eMsg = 'Domains unassigned successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'Domains could not be unassigned.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                } else {
                    $eMsg = 'Please check at least one Domain to unassign.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action'] . "/$userId");
            }

            $allDomains = $this->Domain->find('all', array(
                'conditions' => array("Domain.owner_id = -1 AND Domain.user_id = $userId AND Domain.networktype = 1"),
                'order' => ' Domain.id ASC ',
                'recursive' => 1
            ));


            $this->set(compact('allDomains'));

            $userInfo = $this->User->findById($userId);

            $this->set(compact('userInfo'));


            $this->set('title_for_layout', 'Manage Assigned Mysites');
        }

        function freemysites()
        {

            if ($this->request->is('post')) {
                $checkedDomains = @$this->data['Checked']; // could be all unselected
                $userId = $this->data['Domain']['user_id'];
                $updateArr = array();
                if ($checkedDomains && $userId) {
                    $count = 0;
                    foreach ($checkedDomains as $key => $val) {
                        $updateArr[$count]['id'] = $key;
                        $updateArr[$count]['user_id'] = $userId;
                        $count++;
                    }

                    $this->User->id = $userId;
                    $packageId = $this->User->field('package_id');

                    $alreadyAssigned = $this->Domain->find('count', array('conditions' => "Domain.owner_id = -1 AND Domain.user_id = $userId"));

                    $this->uses[] = 'Serviceprice';
                    $packageInfo = $this->Serviceprice->find('first', array('conditions' => "Serviceprice.package_id = $packageId AND Serviceprice.service_id = 12"));
                    $packageLimit = $packageInfo['Serviceprice']['servicelimit'];

                    $newAssigned = $alreadyAssigned + $count;

                    if ($newAssigned > $packageLimit) {
                        $eMsg = 'You cannot assign more than the package limit for this user.';
                        //$this->Session->setFlash($eMsg, 'flash_error');
                        //$this->redirect($this->params['action']);
                    }

                    $fieldsArr = array(
                        'fieldList' => array(
                            'id', 'user_id'
                    ));
                    //pr($updateArr);
                    //exit;
                    //$this->Domain->updateAll(array('Domain.user_id' => $userId), array('Domain.id' => $user_id, 'Usercredit.service_id' => $service_id, 'Usercredit.status' => 1), false
                    if ($this->Domain->saveAll($updateArr, $fieldsArr)) {
                        $eMsg = 'Domains assigned successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'Domains could not be assigned or already assigned.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                } else {
                    $eMsg = 'Please check at least one Domain to assign.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action']);
            }

            $allDomains = $this->Domain->find('all', array(
                'conditions' => array('Domain.owner_id = -1 AND Domain.user_id = -1 AND Domain.networktype = 1'),
                'order' => ' Domain.domain ASC ',
                'recursive' => 1
            ));

            $this->set(compact('allDomains'));

            $this->User->bindModel(array(
                'hasMany' => array(
                    'Domain' => array('foreignKey' => 'user_id', 'conditions' => "Domain.owner_id = -1"),
                )
            ));

            $this->uses[] = 'Serviceprice';
            $this->User->bindModel(array(
                'belongsTo' => array(
                    'Serviceprice' => array('foreignKey' => false, 'conditions' => 'Serviceprice.package_id = User.package_id AND Serviceprice.service_id = 1'),
                )
            ));

            $clientList = $this->User->find('all', array('conditions' => "User.role = 'client'", 'order' => 'User.firstname'));

            $this->loadModel('Usercredit');
            $users = array();

            if ($clientList) {
                foreach ($clientList as $key => $client) {

                    $uId = $client['User']['id'];
                    $serviceId = $client['Serviceprice']['service_id'];
                    $serviceLimit = $client['Serviceprice']['servicelimit'];
                    $extraUnit = 0;

                    $userCreditInfo = $this->Usercredit->find('first', array('conditions' => "Usercredit.user_id = $uId AND Usercredit.service_id = $serviceId"));
                    if ($userCreditInfo) {
                        $extraUnit = $userCreditInfo['Usercredit']['extra_unit'];
                    }

                    $serviceLimit = $serviceLimit + $extraUnit;

                    if ($serviceLimit > 0) {
                        $siteAssigned = count($client['Domain']);
                        $siteDue = $serviceLimit - $siteAssigned;
                        $siteDue = $siteDue >= 0 ? $siteDue : 0;
                        $users[$uId] = $client['User']['fullname'] . " (Due MySite: $siteDue out of $serviceLimit)";
                    }
                }
            }

            //pr($users);

            $this->set(compact('users'));

            $this->set('title_for_layout', 'Manage Unassigned Mysites');
        }

        function unassignmysite($id)
        {
            if (empty($id))
                return '';
            $this->autoRender = false;
            $this->layout = 'ajax';

            $updateDomain['Domain']['id'] = $id;
            $updateDomain['Domain']['user_id'] = -1;
            if ($this->Domain->save($updateDomain, false)) {
                $eMsg = 'Domain has been unassigned successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        function viewcredits()
        {

            $this->paginate = array('conditions' => array("User.role = 'client' "), 'limit' => 500);

            $userCredits = $this->paginate('User');
            $this->set('userCredits', $userCredits);
            $this->set('title_for_layout', 'Manage Credits');
        }

        public function creditstogroup()
        {
            if ($this->request->is('post')) {
                $this->Credit->set($this->data['Credit']);
                if ($this->Credit->validates()) {

                    if ($this->Credit->updateAll(array('Credit.extra_credit' => "Credit.extra_credit + {$this->data['Credit']['assign_credit']}"), array('package_id' => $this->data['Credit']['package_id']), false)) {

                        $gUsersArr = $this->User->find('list', array(
                            'fields' => array('User.id'),
                            'conditions' => array('User.package_id' => $this->data['Credit']['package_id'])
                        ));
                        if (count($gUsersArr)) {
                            foreach ($gUsersArr as $k => $userId) {
                                $updateAccount['user_id'] = $userId;
                                $updateAccount['type'] = 'credit';
                                $updateAccount['service_id'] = -1;
                                $updateAccount['amount'] = $this->data['Credit']['assign_credit'];
                                $updateAccount['remarks'] = 'Credits assigned by the admin.';

                                $this->updateAccountDebit($updateAccount);
                            }
                        }
                        $eMsg = 'Credit assigned successfully';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        // set msg flash
                        $eMsg = 'Could not assign credit.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action']);
                } else {
                    $this->generateError('Credit');
                }
            }
            $packages = $this->Package->find('list', array(
                'fields' => array('Package.id', 'Package.packagename'),
                'conditions' => array('Package.status' => 1)
            ));
            $this->set('packages', $packages);
            $this->set('title_for_layout', 'Assign Credits to Package Group');
        }

        public function creditstouser()
        {
            if ($this->request->is('post')) {
                $this->Credit->set($this->data['Credit']);
                if ($this->Credit->validates()) {
                    if ($this->Credit->updateAll(array('Credit.extra_credit' => "Credit.extra_credit + {$this->data['Credit']['assign_credit']}"), array('id' => $this->data['Credit']['id'],), false)) {
                        $updateAccount['user_id'] = $this->data['Credit']['id'];
                        $updateAccount['service_id'] = -1;
                        $updateAccount['type'] = 'credit';
                        $updateAccount['amount'] = $this->data['Credit']['assign_credit'];
                        $updateAccount['remarks'] = 'Credits assigned by the admin.';
                        $this->updateAccountDebit($updateAccount);
                        $eMsg = 'Credit assigned successfully';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        // set msg flash
                        $eMsg = 'Could not assign credit.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action']);
                } else {
                    $this->generateError('Credit');
                }
            }
            $users = $this->User->find('list', array(
                'fields' => array('User.id', 'User.fullname'),
                'conditions' => array('User.status' => 1, 'role' => 'client')
            ));
            $this->set('users', $users);
            $this->set('title_for_layout', 'Assign Credits to Individual User');
        }

        public function credithistory()
        {
            $this->uses[] = 'Credithistory';
            $this->paginate = array('conditions' => "Credithistory.service_id = -1 AND Credithistory.type = 'credit'");
            $creditHistory = $this->paginate('Credithistory');
            $this->set(compact('creditHistory'));
            $this->set('title_for_layout', 'View Credit History');
        }

        public function debithistory()
        {
            $this->uses[] = 'Credithistory';
            $this->paginate = array('conditions' => "Credithistory.service_id != -1 AND Credithistory.type = 'debit'");
            $debitHistory = $this->paginate('Credithistory');
            $this->set(compact('debitHistory'));
            $this->set('title_for_layout', 'View Debit History');
        }

        function addinstruction()
        {
            $extRes = array('mp4', 'wmv', 'flv');
            if ($this->request->is('post')) {
                $this->Instruction->set($this->data['Instruction']);
                if ($this->Instruction->validates()) {
                    $fileName = '';
                    if ($this->data['Instruction']['video']['name'] != '') {
                        if ($this->data['Instruction']['video']['error'] == 0) {
                            if (in_array($this->fileExt($this->data['Instruction']['video']['name']), $extRes)) {
                                $folder = WWW_ROOT . 'video';
                                $fileName = time() . '_' . stripslashes(preg_replace("~\s+~", "_", str_replace("'", "", $this->data['Instruction']['video']['name'])));
                                $ufile = $folder . DS . $fileName;
                                @move_uploaded_file($this->data['Instruction']['video']['tmp_name'], $ufile);
                            } // end if in_array
                            else {
                                $eMsg = 'File Not Supported';
                                $this->Session->setFlash($eMsg, 'flash_error');
                                $this->redirect($this->params['action']);
                            }
                        }
                    } // end picure upload
                    $this->request->data['Instruction']['video'] = $fileName;
                    $this->Instruction->save($this->data['Instruction']);
                    $eMsg = 'New Instruction has been added successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                    $this->redirect($this->params['action']);
                } else {
                    $this->generateError('Instruction');
                }
            }

            $this->set('title_for_layout', 'Add Instruction');
        }

        function manageinstruction()
        {
            $allInstruction = $this->Instruction->find('all', array('order' => 'pagename ASC'));
            $this->set(compact('allInstruction'));
            $this->set('title_for_layout', 'Manage Instruction');
        }

        function editinstruction($pId = null)
        {
            $extRes = array('mp4', 'wmv', 'flv');
            if ($this->request->is('post')) {
                $this->Instruction->set($this->data['Instruction']);
                if ($this->Instruction->validates()) {
                    if ($this->data['Instruction']) {
                        $ins = $this->Instruction->findById($pId);
                        if ($this->data['Instruction']['video']['name'] != '') {
                            if ($this->data['Instruction']['video']['error'] == 0) {
                                if (in_array($this->fileExt($this->data['Instruction']['video']['name']), $extRes)) {
                                    $folder = WWW_ROOT . 'video';
                                    $fileName = time() . '_' . stripslashes(preg_replace("~\s+~", "_", str_replace("'", "", $this->data['Instruction']['video']['name'])));
                                    $ufile = $folder . '/' . $fileName;
                                    @move_uploaded_file($this->data['Instruction']['video']['tmp_name'], $ufile);
                                    $unfile = $folder . '/' . $ins['Instruction']['video'];
                                    @unlink($unfile);
                                    $this->request->data['Instruction']['video'] = $fileName;
                                } // end if in_array
                                else {
                                    $eMsg = 'File Not Supported';
                                    $this->Session->setFlash($eMsg, 'flash_error');
                                    $this->redirect($this->params['action']);
                                }
                            }
                        } // end picure upload
                        else {
                            $this->request->data['Instruction']['video'] = $ins['Instruction']['video'];
                        }

                        // delete old video file
                        if (isset($this->data['Instruction']['delOld'])) {
                            $unfile = WWW_ROOT . 'video/' . $ins['Instruction']['video'];
                            @unlink($unfile);
                            $this->request->data['Instruction']['video'] = '';
                        }


                        $this->Instruction->save($this->data['Instruction']);
                        $eMsg = 'Tutorial has been updated successfully';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'Could not Update Tutorial';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . '/' . $pId);
                } else {
                    $this->generateError('Instruction');
                }
            }
            $data = $this->Instruction->findById($pId);
            $this->data = $data;
            $this->set('data', $data);
            $this->set('cId', $pId);
            $this->set('title_for_layout', 'Edit Instruction');
        }

        function deleteinstruction($id = null)
        {
            if (empty($id)) return '';
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->Instruction->id = $id;
            $oData = $this->Instruction->read();
            $ins = $this->Instruction->findById($id);
            $folder = WWW_ROOT . 'video';
            $ufile = $folder . DS . $ins['Instruction']['video'];
            @unlink($ufile);
            if ($this->Instruction->delete($id, false)) {
                $eMsg = 'Instruction with all records has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        /*
        *  Add, Delete Media
        *
        */


        // Menu Management 
        /*___---------------------*/

        function addmenu($itemId = null)
        {
            $this->loadModel('Menu');

            if ($this->request->is('post')) {
                $this->Menu->set($this->data['Menu']);
                if ($this->Menu->validates()) {
                    if ($this->Menu->save($this->data['Menu'], false)) {
                        $eMsg = 'New Menu has been added successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'New Menu could not be added.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . '/' . $itemId);
                } else {
                    $this->generateError('Menu');
                }
            }

            if ($itemId && !$this->request->data) {
                $this->data = $this->Menu->findById($itemId);
            }

            $menus = $this->Menu->find('list', array(
                'conditions' => "parent_id IS NULL",
                'fields' => 'id,name'
            ));
            $this->set(compact('menus'));
        }

        function managemenu() {
            $this->loadModel('Menu');
            $items = $this->Menu->getEmbededMenus();
            $this->set(compact('items'));
        }

        function deletemenus($id = null ) {
            if (empty($id)) return '';
            $this->loadModel('Menu');
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->Menu->id = $id;
            if ($this->Menu->delete($id, false)) {
                $eMsg = 'Menu with all records has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        /*___---------------------*/



        // Module Management 
        /*___---------------------*/
        function add_module($itemId = null)  {
            $this->loadModel('Module');

            if ($this->request->is('post')) {
                //pr($this->data['Module']); exit;
                $this->Module->set($this->data['Module']);
                if ($this->Module->validates()) {
                    $fileName = '';
                    if ($this->data['Module']['photos']['name'] != '') {
                        if ($this->data['Module']['photos']['error'] == 0) {
                            if (in_array($this->fileExt($this->data['Module']['photos']['name']), array('jpg', 'jpeg', 'gif', 'png'))) {
                                $folder = WWW_ROOT . 'moduleexpert';
                                $fileName = time() . '_' . stripslashes(preg_replace("~\s+~", "_", str_replace("'", "", $this->data['Module']['photos']['name'])));
                                $ufile = $folder . DS . $fileName;
                                @move_uploaded_file($this->data['Module']['photos']['tmp_name'], $ufile);
                                $this->request->data['Module']['photo'] = $fileName;
                            } // end if in_array

                        }
                    } // end picure upload

                    if ($this->Module->save($this->data['Module'], false)) {
                        $eMsg = 'New Module has been added successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        @unlink($fileName);
                        $eMsg = 'New Module could not be added.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . '/' . $itemId);
                } else {
                    $this->generateError('Module');
                }
            }

            if ($itemId && !$this->request->data) {
                $this->data = $this->Module->findById($itemId);
            }
        }
        function editmodule( $pId = null ) {
            $this->loadModel('Module');
            $extRes = array('jpg', 'jpeg', 'png','gif');
            if ($this->request->is('post')) {
                $this->Module->set($this->data['Module']);
                if ($this->Module->validates()) {
                    if ($this->data['Module']) {
                        $ins = $this->Module->findById($pId);
                        if ($this->data['Module']['photos']['name'] != '') {
                            if ($this->data['Module']['photos']['error'] == 0) {
                                if (in_array($this->fileExt($this->data['Module']['photos']['name']), $extRes)) {
                                    $folder = WWW_ROOT . 'moduleexpert';
                                    $fileName = time() . '_' . stripslashes(preg_replace("~\s+~", "_", str_replace("'", "", $this->data['Module']['photos']['name'])));
                                    $ufile = $folder . '/' . $fileName;
                                    @move_uploaded_file($this->data['Module']['photos']['tmp_name'], $ufile);
                                    $unfile = $folder . '/' . $ins['Module']['photo'];
                                    @unlink($unfile);
                                    $this->request->data['Module']['photo'] = $fileName;
                                } // end if in_array
                                else {
                                    $eMsg = 'File Not Supported';
                                    $this->Session->setFlash($eMsg, 'flash_error');
                                    $this->redirect($this->params['action']);
                                }
                            }
                        } // end picure upload
                        else {
                            $this->request->data['Module']['photo'] = $ins['Module']['photo'];
                        }

                        // delete old video file
                        if (isset($this->data['Module']['delOld'])) {
                            $unfile = WWW_ROOT . 'moduleexpert/' . $ins['Module']['photo'];
                            @unlink($unfile);
                            $this->request->data['Module']['video'] = '';
                        }


                        $this->Module->save($this->data['Module']);
                        $eMsg = 'Module has been updated successfully';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'Could not Update Tutorial';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . '/' . $pId);
                } else {
                    $this->generateError('Module');
                }
            }
            $data = $this->Module->findById($pId);
            $this->data = $data;
            $this->set('data', $data);
            $this->set('cId', $pId);
            $this->set('title_for_layout', 'Edit Module');

        }
        function managemodule() {
            $this->loadModel('Module');
            $items = $this->Module->find('all', array('order' => ' menu_order DESC'));
            $this->set(compact('items'));
        }
        function deletemodule( $id = null ) {
            if (empty($id)) return '';
            $this->loadModel('Module');
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->Module->id = $id;

            $oData = $this->Module->read();
            $ins = $this->Module->findById($id);
            $folder = WWW_ROOT . 'moduleexpert';
            $ufile = $folder . DS . $ins['Module']['photo'];

            if ($this->Module->delete($id, false)) {
                @unlink($ufile);
                $eMsg = 'Module with all records has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }
        /*___---------------------*/


        // Module Lesson Management
        /*___---------------------*/
        function add_lesson($itemId = null)   {
            $this->loadModel('Module');
            $this->loadModel('Lesson');
            $this->loadModel('Lessonfile');

            if ($this->request->is('post') || $this->request->is('put')) {
                //pr($this->data); exit;
                $this->Lesson->set($this->data['Lesson']);
                if ($this->Lesson->validates()) {

                    $fileNameArr = array();
                    $fileNameTitleArr = array();

                    foreach ($this->data['Lesson']['le_files'] as $key => $file) {
                        if ($file['name']) {
                            $fileNameArr[] = $this->fileUpload($file);
                        }
                    }
                    foreach ($this->data['Lesson']['le_files_title'] as $key => $name) {
                        if ($file['name']) {
                            $fileNameTitleArr[] = $name;
                        }
                    }


                    if ($this->Lesson->save($this->data['Lesson'], false)) {
                        if ($this->data['Lesson']['id']) {
                            $lessonId = $this->data['Lesson']['id'];
                        } else {
                            $lessonId = $this->Lesson->getLastInsertId();
                        }
                        if ($fileNameArr) {
                            $i = 0;
                            foreach ($fileNameArr as $filename) {
                                $insertFile['filename'] = $filename;
                                $insertFile['lesson_id'] = $lessonId;
                                $insertFile['ftitle'] = $fileNameTitleArr[$i];
                                $this->Lessonfile->create();
                                $this->Lessonfile->save($insertFile, false);
                                $i++;
                            }
                        }

                        // remove old files
                        if ( isset ($this->data['Lesson']['lfiles']) ) {
                            $folder = WWW_ROOT . '/files/lesson_files/';
                            foreach ( $this->data['Lesson']['lfiles'] as $kk => $f ) {
                                $ff = $folder.'/'.$f;
                                @unlink($ff);
                                @$this->Lessonfile->delete($kk,false);
                            }
                        }

                        $eMsg = 'New Lesson has been added successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = 'New Lesson could not be added.';
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . '/' . $itemId);
                } else {
                    $this->generateError('Lesson');
                }
            }

            if ($itemId && !$this->request->data) {
                $this->data = $this->Lesson->findById($itemId);
                $this->set('data',$this->data);
            }

            $modules = $this->Module->find('list', array(
                'fields' => 'id,name'
            ));
            $this->set(compact('modules'));
        }
        function manage_lesson() {
            $this->loadModel('Module');
            $this->loadModel('Lesson');
            /*$this->Lesson->bindModel(array(
            'belongsTo' => array(
            'Module' => array('foreignKey' => 'module_id')
            )
            ));*/
            $items = $this->Lesson->find('all',array('recursive' => 1));
            //pr($items); 
            $this->set(compact('items'));
        }
        function deletelessons($id = null ) {
            if ( empty($id) ) return '';
            $this->loadModel('Module');
            $this->loadModel('Lesson');
            $this->loadModel('Lessonfile');

            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->Lesson->id = $id;
            $oData = $this->Lesson->read();
            $ins = $this->Lesson->findById($id);
            $folder = WWW_ROOT . '/files/lesson_files/';

            if ($this->Lesson->delete($id, false)) {

                $lids = $this->Lessonfile->findByLessonId($id);
                foreach ( array($lids) as $k => $v ) {
                    @$f = $v['Lessonfile']['filename'];
                    @$ld = $v['Lessonfile']['id'];
                    @$ufile = $folder . DS . $f;
                    @unlink($ufile);
                    @$this->Lessonfile->delete($ld,false);
                }


                $eMsg = 'Lesson with all records has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }
        /*___---------------------*/



        function community()
        {

            $this->set('title_for_layout', "Community");
        }


        function fileUpload($file, $item = 'lesson_files', $edit = false)
        {
            App::import('Vendor', 'imgupload');
            $upload = new upload($file);
            $uploadConfig = Configure::read('uploadConfig');

            foreach ($uploadConfig[$item] as $single):
                $upload->file_name_body_pre = '';
                foreach ($single as $key => $config):
                switch ($key) {
                    case "destination":
                        $path = WWW_ROOT . $config;
                        break;
                    default :
                        $upload->$key = $config;
                        break;
                }
                endforeach;
                $upload->Process($path);
                if (!$upload->processed) {
                    $eMsg = $upload->error;
                    $this->Session->setFlash($eMsg, 'flash_error');
                    $this->redirect($this->params['action']);
                }
                endforeach;
            return $upload->file_dst_name;
        }

        function fileUpdate($file, $item, $old_file)
        {
            $uploadConfig = Configure::read('uploadConfig');

            if ($file['name'] != '') {

                $filename = $this->fileUpload($file, $item);
                foreach ($uploadConfig[$item] as $single):
                    $folder = $folder = WWW_ROOT . $single['destination'];
                    $unfile = $folder . '/' . $old_file;
                    unlink($unfile);
                    endforeach;
                return $filename;
            } // end picure upload
            else {
                return $old_file;
            }
        }



        // Welcome News managements
        function addwelcomenews() {
            $this->loadModel('Welcomenews');
            if ($this->request->is('post')) {
                if ($this->data['Welcomenews']) {
                    $this->Welcomenews->save($this->data['Welcomenews']);
                    $eMsg = 'Welcome News has been updated successfully';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'Could not Update News';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action']);
            }
            $this->set('title_for_layout', 'Add New Welcome News');
        }

        // Edit Welcome News managements
        function editwelcomenews($pId = 1) {
            $this->loadModel('Welcomenews');
            $extRes = array('mp4', 'wmv', 'flv');
            if ($this->request->is('post')) {
                if ($this->data['Welcomenews']) {
                    $this->Welcomenews->save($this->data['Welcomenews']);
                    $eMsg = 'Welcome News has been updated successfully';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'Could not Update Tutorial';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action'] . '/' . $pId);
            }
            $data = $this->Welcomenews->findById($pId);
            $this->data = $data;
            $this->set('data', $data);
            $this->set('cId', $pId);
            $this->set('title_for_layout', 'Update Welcome News');
        }
        
        
        function deleteanyrecord( $model = null, $id = null ) {
            if (empty($id)) return '';
            if (empty($model)) return '';
            $model = ucfirst($model); 
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->loadModel($model);
            $this->{$model}->id = $id;
            if ($this->{$model}->delete($id, false)) {
                $eMsg = "record has been deleted successfully!";
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        } 

        function welcomenews() {
            $this->loadModel('Welcomenews');
            $news = $this->Welcomenews->find('all', array('order' => 'id DESC'));
            $this->set(compact('news'));
            $this->set('title_for_layout', 'Manage News');

        }




        // events managements
        /*___---------------------*/
        function addevents()  {
            $this->loadModel('Events');
            if ($this->request->is('post')) {
                $this->Events->set($this->data['Events']);
                if ($this->Events->validates()) {
                    $this->Events->save($this->data['Events']);
                    $eMsg = 'New Events has been added successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                    $this->redirect($this->params['action']);
                } else {
                    $this->generateError('Events');
                }
            }
            $this->set('title_for_layout', 'Add New Events');

        }
        function editevents($id = null)  {
            if ( empty($id)) return false;
            $this->loadModel('Events');
            if ($this->request->is('post')) {
                $this->Events->id = $id; 
                $this->Events->set($this->data['Events']);
                if ($this->Events->validates()) {
                    $this->Events->save($this->data['Events']);
                    $eMsg = 'Events has been Updated successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                    $this->redirect($this->params['action'] . '/' . $id);
                } else {
                    $this->generateError('Events');
                }
            }

            $data = $this->Events->findById($id);
            $this->data = $data;
            $this->set('data', $data);

            $this->set('title_for_layout', 'Edit Events');

        }

        function manageevents() {
            $this->loadModel('Events');
            $events = $this->Events->find('all', array('order' => 'id DESC'));
            $this->set(compact('events'));
            $this->set('title_for_layout', 'Manage Events');
        }

        function delevent( $id = null ) {
            if (empty($id)) return '';
            $this->loadModel('Events');
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->Events->id = $id;
            if ($this->Events->delete($id, false)) {
                $eMsg = 'Events with all records has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();

        }
        /*___---------------------*/

        // Bonus Management
        /*___---------------------*/

        function addbonus($itemId = null)
        {

            $this->loadModel('Bonus');

            $action = $itemId ? 'updated' : 'added';

            if ($this->request->is('post')) {
                $this->Bonus->set($this->data['Bonus']);
                if ($this->Bonus->validates()) {
                    if ($this->Bonus->save($this->data['Bonus'], false)) {
                        $eMsg = "New Bonus has been $action successfully.";
                        $this->Session->setFlash($eMsg, 'flash_success');
                    } else {
                        $eMsg = "New Bonus could not be $action.";
                        $this->Session->setFlash($eMsg, 'flash_error');
                    }
                    $this->redirect($this->params['action'] . ($itemId ? "/$itemId" : ''));
                } else {
                    $this->generateError('Bonus');
                }
            }

            if ($itemId && !$this->request->data) {
                $this->data = $this->Bonus->findById($itemId);
            }

            $bonuses = $this->Bonus->find('list', array(
                'conditions' => "parent_id IS NULL",
                'fields' => 'id,name'
            ));
            $this->set(compact('bonuses'));
        }

        function managebonus()
        {
            $this->loadModel('Bonus');
            $items = $this->Bonus->getEmbededBonuses();
            $this->set(compact('items'));
        }

        function deletebonus($id = null)
        {
            if (empty($id)) return '';
            $this->loadModel('Bonus');
            $this->autoRender = false;
            $this->layout = 'ajax';
            $this->Bonus->id = $id;
            if ($this->Bonus->delete($id, false)) {
                $eMsg = 'Bonus with all records has been deleted successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
            exit();
        }

        /*___---------------------*/


    } // end class
