<?php

App::uses('AppController', 'Controller');

define('TEST_EMAIL', 'itestgo@gmail.com');


set_time_limit(0);
ini_set('memory_limit', '512M');
ini_set('max_execution_time', 86400);
ini_set('max_input_time', 86400);

Configure::write('debug', 2);
Configure::write('Cache.disable', true);

class ApiController extends AppController {

    private $_cMetaKey = '_nitro_dcl_cmeta_key';
    private $_dclResultsName = '_nitro_dcl_results_name';
    private $_dclResultsValueKey = '_nitro_dcl_results_value_key';
    private $_dclDResults = null;
    private $_dclAutoPostKey = '_dcl_auto_post_key';
    private $_nitroAutoLoginKey = '_nitro_auto_login_key';
    private $_nitro_sKeyRemote = '_nitro_sKeyRemote';
    private $_dclDeletePostId = '_nitro_deletePostId';
    private $_p_action_post_key = '_nitro_p_action';
    private $_p_info_post_key = '_nitro_p_info';
    private $_p_update_post_key = '_nitro_p_update';

    public function beforeFilter() {
        //parent::beforeFilter();
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->Auth->allow();

        if (defined('CRON_DISPATCHER')) {
            //$this->Auth->allow($alllowed);
        }

        $this->layout = null;
        $this->autoRender = false;
    }

    // custom mail sender
    function cmail($to = '', $subject = '', $body = '', $attchfile = '', $from = '', $fromName = 'SEONitro', $templates = 'default') {
        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail();
        $email->helpers('Html');
        $email->viewVars(array('body' => $body));
        $email->from(array('noreply@seonitro.com' => 'SEONitro'));
        $email->to($to);
        $email->subject('[SEONitro] ' . $subject);
        $email->template('email', 'default');
        $email->emailFormat('html');
        $email->transport('Mail');
        if ($attchfile != '')
            $email->attachments($attchfile);
        $email->send();
    }

    function iwr_new() {

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $reTurn = '0';
        $postData = 'none';
        if ($this->request->is('post')) {
            if ($this->data) {
                $postData = prt($this->data);
                $this->loadModel('Iwarticle');
                $this->Iwarticle->set($this->data);
                if ($this->Iwarticle->validates()) {
                    if ($this->Iwarticle->save($this->data, false)) {
                        $articleId = 0;
                        if (isset($this->data['id'])) {
                            $articleId = $this->data['id'];
                        } else {
                            $articleId = $this->Iwarticle->getLastInsertId();
                        }
                        if ($articleId) {
                            $success['track_id'] = $articleId;
                            $success['track_url'] = Router::url('/api/iw_report/' . $articleId, true);
                            $reTurn = json_encode($success);
                        }
                    }
                } else {
                    $reTurn = json_encode($this->Iwarticle->invalidFields());
                }
            }
        }
        //$this->cmail(TEST_EMAIL, 'iWriter Post Data', $postData);
        echo $reTurn;
        //exit;
    }

    function iw_report($articleId = null) {
        $reTurn = '0';
        if ($articleId) {
            $this->loadModel('Iwreport');
            $this->Iwreport->virtualFields = array(
                'post_date' => 'SELECT DATE_FORMAT(created,"%Y-%m-%d")'
            );
            $aReport = $this->Iwreport->find('all', array('conditions' => "article_id = $articleId", 'fields' => 'post_link,post_url,post_date'));

            if ($aReport) {
                $aReport = Set::extract('/Iwreport/.', $aReport);
            }
            //pr(json_decode($reTurn));
            $this->loadModel('Iwarticle');
            $this->Iwarticle->id = $articleId;
            $currentStatus = $this->Iwarticle->field('status');
            $aReport['status'] = $currentStatus;
            $reTurn = json_encode($aReport);
            //pr(json_decode($reTurn));
        }
        echo $reTurn;
    }

    function iw_article_status($trackId = null, $newStatus = null, $pausedTill = null) {
        if ($trackId) {
            $this->loadModel('Iwarticle');
            $this->Iwarticle->id = $trackId;
            $prevStatus = $this->Iwarticle->field('status');
            if ($prevStatus != 2) {
                if ($newStatus !== null) {
                    if (in_array($newStatus, array(0, 1, 2))) {
                        $updateArr['status'] = $newStatus;
                        $updateArr['paused_till'] = null;
                        if ($newStatus == 0) {
                            if ($pausedTill) {
                                $pausedTill = date('Y-m-d', strtotime($pausedTill));
                                if ($pausedTill > date('Y-m-d')) {
                                    $updateArr['paused_till'] = $pausedTill;
                                } else {
                                    echo 'invalid date';
                                    exit;
                                }
                            }
                        } elseif ($newStatus == 1 && $prevStatus != 1) {
                            $updateArr['paused_till'] = date('Y-m-d');
                        }
                        $this->Iwarticle->save($updateArr, false);
                    } else {
                        echo 'invalid status';
                        exit;
                    }
                }
            }
            $this->Iwarticle->id = $trackId;
            $currentStatus = $this->Iwarticle->field('status');
            if ($currentStatus == 0) {
                $paused_till = $this->Iwarticle->field('paused_till');
                if ($paused_till) {
                    $currentStatus .= ' # ' . $paused_till;
                }
            }
            echo (string) $currentStatus;
        }
    }

    function test_api() {
        Configure::write('debug', 2);
        $this->loadModel('Ready');
        $postData['id'] = 7;
        $postData['post_title'] = 'Hello Post Updated';
        $postData['post_content'] = 'Hello Content';
        $postData['user_id'] = 1;
        $postData['user_type'] = 1;
        $postUrl = Router::url('/api/iwr_new', true);
        $reTurned = $this->Ready->_request($postUrl, 'POST', $postData);
        pr($reTurned);
        //exit;
        if (isset($reTurned['result'])) {
            pr(json_decode($reTurned['result']));
        }
    }

    function iw_autopost() {
        $this->loadModel('Iwarticle');
        $articles = $this->Iwarticle->find('all', array('conditions' => "status = 1"));
    }

    private function distribute_post($totalCount, $startTime, $endTime, $postDate = null, $postedCount = 0) {

        $daysArr = array();

        $daySpan = $endTime - $startTime;
        $daySpan = $daySpan > 0 ? $daySpan : 1;
        if ($daySpan >= 0) {
            $dayCount = $daySpan / (3600 * 24);
            for ($j = 0; $j <= $dayCount; $j++) {
                $dayToTime = $startTime + ($j * 3600 * 24);
                $eachDate = date('Y-m-d', $dayToTime);
                $daysArr[$j + 1]['date'] = $eachDate;
                $daysArr[$j + 1]['post_today'] = 0;
            }

            //$totalCount = 130;
            //pr($totalCount);

            if ($daysArr) {
                $toalDays = count($daysArr);
                $perDay = floor($totalCount / $toalDays);
                $post_sofar = 0;
                foreach ($daysArr as $key => $day) {
                    $post_sofar +=$perDay;
                    $daysArr[$key]['post_today'] = $perDay;
                    //$daysArr[$key]['post_sofar'] = $post_sofar;
                }
                if ($post_sofar < $totalCount) {
                    $remCount = ($totalCount - $post_sofar);
                    for ($j = 1; $j <= $remCount; $j++) {
                        $daysArr[$j]['post_today'] += 1;
                    }
                }
                $fpost_sofar = 0;
                foreach ($daysArr as $fkey => $fday) {
                    $fpost_sofar += $fday['post_today'];
                    $daysArr[$fkey]['post_sofar'] = $fpost_sofar;
                }
            }

            if ($postDate) {
                $postDate = date('Y-m-d', strtotime($postDate));
                $reCount = 0;
                foreach ($daysArr as $dA) {
                    if ($dA['date'] == $postDate) {
                        if (( $postedCount + $dA['post_today']) > $dA['post_sofar']) {
                            $reCount = ($dA['post_sofar'] - $postedCount);
                        } elseif (( $postedCount + $dA['post_today']) <= $dA['post_sofar']) {
                            $reCount = $dA['post_today'];
                        }
                    }
                }
                return $reCount;
            } else {
                return $daysArr;
            }
        }
    }

    function a() {
        $url = Router::url('/api/b', true);
        $re = get_headers($url);
        pr($re);
        exit;
    }

    function b() {
        ob_end_clean();
        ignore_user_abort();
        ob_start();
        header("Connection: close");
        header("Content-Length: " . ob_get_length());
        ob_end_flush();
        flush();
        // from here the response has been sent. 
        // you can now wait as long as you want and do some tracking stuff 
        $this->c();
        exit;
    }

    function c() {
        sleep(30);
        //$this->cmail(TEST_EMAIL, 'Hi', 'This is me');
    }

    function postDistribution($userType, $startDate = null, $preReportCount = null, $dayPosted = 0, $postDate = null) {

        $distSettings = $this->dist_settings($userType, $preReportCount);

        if ($distSettings) {

            //pr($distSettings);

            if (!$startDate) {
                $startDate = date('Y-m-d');
            }

            $startDate = strtotime($startDate);

            $daysArr = array();
            $onlyDays = array();

            //populate the date array of 30 days from start date
            $dayCount = $dayPosted ? 30 - $dayPosted : 30;
            for ($j = 0; $j < $dayCount; $j++) {
                $dayToTime = $startDate + ($j * 3600 * 24);
                $eachDate = date('Y-m-d', $dayToTime);
                $onlyDays[] = $eachDate;
                $daysArr[$j]['3'] = 0;
                $daysArr[$j]['4'] = 0;
                $daysArr[$j]['5'] = 0;
            }

            //distribute the post count for each date
            foreach ($distSettings as $type => $ds) {
                for ($index = 0; $index < $ds; $index++) {
                    $i = ($index - (floor($index / $dayCount) * $dayCount));
                    $daysArr[$i][$type]+=1;
                }
            }

            //fixed shuffle the result array
            $daysArrFinal = $this->shuffleMtSrand($daysArr);

            //set the date as the kew for each array value to get easily by date
            $finalArr = array();
            foreach ($onlyDays as $key => $date) {
                $finalArr[$date] = $daysArrFinal[$key];
            }

            //return count for a certain date if asked for
            if ($postDate) {
                $postDate = date('Y-m-d', strtotime($postDate));
                if (isset($finalArr[$postDate])) {
                    //pr($finalArr[$postDate]);
                    return $finalArr[$postDate];
                }
            }
            //else return the complete final result array
            //pr($finalArr);
            return $finalArr;
        }
    }

    private function dist_count($userType) {
        $typeArr[1] = 500;
        $typeArr[2] = 250;
        $typeArr[3] = 100;
        return $typeArr[$userType];
    }

    private function dist_settings($userType, $preReportCount = null) {
        $typeArr[1] = array(
            '3' => 50,
            '4' => 100,
            '5' => 350,
        );
        $typeArr[2] = array(
            '3' => 25,
            '4' => 75,
            '5' => 150,
        );
        $typeArr[3] = array(
            '3' => 10,
            '4' => 20,
            '5' => 70,
        );

        if (isset($typeArr[$userType])) {
            $reArr = $typeArr[$userType];
            if ($preReportCount) {
                foreach ($reArr as $nt => $count) {
                    if (isset($preReportCount[$nt])) {
                        $reArr[$nt] = $reArr[$nt] - $preReportCount[$nt];
                        $reArr[$nt] = $reArr[$nt] >= 0 ? $reArr[$nt] : 0;
                    }
                }
            }
            return $reArr;
        }
    }

    private function shuffleMtSrand($items, $mtSrand = 2) {
        if ($items) {
            mt_srand($mtSrand);
            for ($i = count($items) - 1; $i > 0; $i--) {
                $j = @mt_rand(0, $i);
                $tmp = $items[$i];
                $items[$i] = $items[$j];
                $items[$j] = $tmp;
            }
        }
        return $items;
    }

    function iwriter_posting($artcileId = null) {
        $this->loadModel('Iwarticle');
        $this->loadModel('Iwreport');

        $this->Iwreport->virtualFields = array(
            'post_count' => 'SELECT COUNT(id)'
        );

        $this->loadModel('Domain');
        $cond = array(
            'conditions' => "status = 1 " . ($artcileId ? "AND id = $artcileId" : ''),
        );
        $itemList = $this->Iwarticle->find('all', $cond);
        //pr($itemList);
        //exit;
        if ($itemList) {

            $this->loadModel('Nitro');

            App::import('Vendor', 'IXR_Library');
            $fullRpcDomain = 'http://www.villo.me/nsnew.php';
            $client = new IXR_ClientSSL($fullRpcDomain);

            foreach ($itemList as $item) {
                $arr = $item['Iwarticle'];
                //pr($arr);
                $startDate = $arr['created'];
                //get previously posted stat group by post_date,network_type
                $preReport = $this->Iwreport->find('all', array('fields' => 'post_date,network_type,post_count', 'conditions' => array('article_id' => $arr['id']), 'group' => 'post_date,network_type'));

                //format the result in array by date index and network_type index inside
                //pr($preReport);
                $preReportCount = array();
                $dayPosted = 0;
                $preReportFormatted = array();
                $totalPosted = 0;
                if ($preReport) {
                    foreach ($preReport as $value) {
                        $post_date = $value['Iwreport']['post_date'];
                        $network_type = $value['Iwreport']['network_type'];
                        $post_count = $value['Iwreport']['post_count'];
                        $preReportFormatted[$post_date][$network_type] = $post_count;
                        $totalPosted +=$post_count;
                    }
                    if ($arr['paused_till']) {
                        $startDate = $arr['paused_till'];
                        $dayPosted = count($preReportFormatted);
                        foreach ($preReport as $val) {
                            $networkType = $val['Iwreport']['network_type'];
                            $postCount = $val['Iwreport']['post_count'];
                            @$preReportCount[$networkType] += $postCount;
                        }
                    }
                }
                $totalAllowed = $this->dist_count($arr['user_type']);
                pr($totalAllowed);
                if ($totalPosted < $totalAllowed) {
                    $postRem = ($totalAllowed - $totalPosted);
                    pr($postRem);
                    //pr($preReportCount);
                    //pr($preReportFormatted);
                    //get default post distribution array
                    $postDist = $this->postDistribution($arr['user_type'], $startDate, $preReportCount, $dayPosted);
                    //pr($postDist);
                    //exit;
                    //compare previous result and default post distribution and adjust the default to get the final array
                    foreach ($postDist as $date => $pD) {
                        if (isset($preReportFormatted[$date])) {
                            foreach ($pD as $nt => $count) {
                                if (isset($preReportFormatted[$date][$nt])) {
                                    $postDist[$date][$nt] = $postDist[$date][$nt] - $preReportFormatted[$date][$nt];
                                    $postDist[$date][$nt] = $postDist[$date][$nt] >= 0 ? $postDist[$date][$nt] : 0;
                                }
                            }
                        }
                    }
                    //pr($postDist);
                    //exit;
                    //get previous domain_ids to exclude posting next to
                    $domainIdString = '0';
                    $preDomainIds = $this->Iwreport->find('list', array('fields' => 'domain_id,domain_id', 'conditions' => array('article_id' => $arr['id'])));
                    if ($preDomainIds) {
                        $preDomainIds = array_values($preDomainIds);
                    }

                    $nowDate = date('Y-m-d');
                    //loop through the final date array to post according to network types
                    foreach ($postDist as $postDate => $pdf) {
                        if ($postDate > $nowDate) {
                            break;
                        }
                        foreach ($pdf as $nt => $count) {
                            if ($count) {
                                if ($preDomainIds) {
                                    $domainIdString = implode(',', $preDomainIds);
                                }
                                //get required number number of domains for respective network_type
                                $cond = "status = 1 AND iw_active = 1 AND id NOT IN($domainIdString) AND networktype = $nt";
                                if ($artcileId) {
                                    $cond = "status = 1 AND id NOT IN($domainIdString) AND networktype IN(3,4)";
                                }
                                $domainList = $this->Domain->find('all', array('fields' => 'id,domain,networktype,hostname', 'conditions' => $cond, 'limit' => $count, 'order' => 'RAND()'));
                                //pr($domainList);
                                //loop through domain list to post the article to
                                if ($domainList) {
                                    foreach ($domainList as $dSl => $dL) {
                                        $response = array();
                                        $domainInfo = $dL['Domain'];
                                        $preDomainIds[] = $domainInfo['id'];
                                        $domain = $domainInfo['domain'];
                                        $hostName = $domainInfo['hostname'];
                                        if ($hostName == 'Siteworks') {
                                            $dbInfo = $this->Nitro->nitroDbInfo(array($domain));
                                        }
                                        //pr($dbInfo);                                    
                                        if ($hostName == 'Siteworks' && isset($dbInfo[$domain]['TABLE_SCHEMA'])) {
                                            $params = array();
                                            $params['domain'] = $domain;
                                            $params['siteUrl'] = $siteUrl = $dbInfo[$domain]['SITE_URL'];
                                            $params['dbName'] = $dbInfo[$domain]['TABLE_SCHEMA'];
                                            $params['tableName'] = $dbInfo[$domain]['TABLE_PREFIX'] . 'posts';

                                            $post_title = $this->spinContent($arr['post_title']);
                                            $post_content = str_replace(array('http:/', 'http:///', 'http://'), 'http://', $this->spinContent($arr['post_content']));
                                            $post_content = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $post_content);
                                            $post_content = str_replace('Â', '', $post_content);

                                            $params['post_title'] = $post_title;
                                            $params['post_content'] = utf8_encode(html_entity_decode($post_content));
                                            $params['post_date'] = $postDate;
                                            /**/
                                            if (!$client->query('nitro.autoPost', $params)) {
                                                $response['error'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
                                            } else {
                                                $response = $client->getResponse();
                                                if (isset($response['post_id']) && intval($response['post_id'])) {
                                                    $post_name = $response['post_name'];
                                                    $update_data['domain_id'] = $domainInfo['id'];
                                                    $update_data['article_id'] = $arr['id'];
                                                    $update_data['network_type'] = $domainInfo['networktype'];
                                                    $update_data['post_id'] = $post_id = $response['post_id'];
                                                    $update_data['post_url'] = $siteUrl . '?p=' . $post_id;
                                                    $update_data['post_link'] = $siteUrl . '/' . $post_id . '-' . $post_name;
                                                    $update_data['post_date'] = $postDate;

                                                    $this->Iwreport->create();
                                                    if ($this->Iwreport->save($update_data, false)) {
                                                        $postRem = $postRem - 1;
                                                        //$response['id'] = $arr['id'];
                                                        //$response['domain'] = $domain;
                                                    }
                                                }
                                            }
                                            //pr($response);
                                            /* */
                                        } else {
                                            $post_title2 = $this->spinContent($arr['post_title']);
                                            $post_content2 = str_replace(array('http:/', 'http:///', 'http://'), 'http://', $this->spinContent($arr['post_content']));

                                            $params2[$this->_dclAutoPostKey] = '_dcl_auto_post_key';
                                            $params2['post_title'] = $post_title2;
                                            $params2['post_content'] = utf8_encode(html_entity_decode($post_content2));
                                            $params2['post_date'] = $postDate;


                                            $rpcUrl = 'http://www.' . $domain . '/xmlrpc.php';

                                            $client = new IXR_ClientSSL($rpcUrl);
                                            $forePost = false;
                                            if (!$client->query('nitro.dclRemotePost', $params2)) {
                                                //$response['error'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
                                                //pr($response);
                                                $url = 'http://www.' . $domain . '/index.php';
                                                $responseD = $this->_autoDclPost($url, $params2);
                                                $response['post_status'] = $responseD;
                                                if (isset($responseD['post_id']) && intval($responseD['post_id'])) {
                                                    $siteUrl = 'http://' . preg_replace('/https?\:\/\//i', '', trim($domain));
                                                    $update_data['domain_id'] = $domainInfo['id'];
                                                    $update_data['article_id'] = $arr['id'];
                                                    $update_data['network_type'] = $domainInfo['networktype'];
                                                    $update_data['post_id'] = $post_id = $responseD['post_id'];
                                                    $update_data['post_url'] = $siteUrl . '?p=' . $post_id;
                                                    $update_data['post_link'] = $siteUrl . '/' . $post_id . '-' . $this->sluggify($post_title2);
                                                    $update_data['post_date'] = $postDate;

                                                    $this->Iwreport->create();
                                                    if ($this->Iwreport->save($update_data, false)) {
                                                        $postRem = $postRem - 1;
                                                        //$response['id'] = $arr['id'];
                                                        //$response['domain'] = $domain;
                                                    }
                                                }
                                                //pr($response);
                                            } else {
                                                $response = $client->getResponse();
                                                //pr($response);
                                                //exit;
                                                //echo $domain . ': ' . $post_id . '<br/><br/>';

                                                if (isset($response['post_id']) && intval($response['post_id'])) {
                                                    $siteUrl = 'http://' . preg_replace('/https?\:\/\//i', '', trim($domain));
                                                    $update_data['domain_id'] = $domainInfo['id'];
                                                    $update_data['article_id'] = $arr['id'];
                                                    $update_data['network_type'] = $domainInfo['networktype'];
                                                    $update_data['post_id'] = $post_id = $response['post_id'];
                                                    $update_data['post_url'] = $siteUrl . '?p=' . $post_id;
                                                    $update_data['post_link'] = $siteUrl . '/' . $post_id . '-' . $this->sluggify($post_title2);
                                                    $update_data['post_date'] = $postDate;

                                                    $this->Iwreport->create();
                                                    if ($this->Iwreport->save($update_data, false)) {
                                                        $postRem = $postRem - 1;
                                                        //$response['id'] = $arr['id'];
                                                        //$response['domain'] = $domain;
                                                    }
                                                }
                                            }
                                            $response['id'] = $arr['id'];
                                            $response['domain'] = $domain;
                                            @pr($response);
                                        }
                                        //exit;
                                        if ($dSl == $postRem) {
                                            break 3;
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    echo 'Completed';
                }
            }
        }
        //$this->cmail(TEST_EMAIL, 'iWriter Posting', 'Success!');
    }

    private function spinContent($txt) {
        $pattern = '#\{([^{}]*)\}#msi';
        $test = preg_match_all($pattern, $txt, $out);
        if (!$test)
            return $txt;
        $atrouver = array();
        $aremplacer = array();
        foreach ($out[0] as $id => $match) {
            $choisir = explode("|", $out[1][$id]);
            $atrouver[] = trim($match);
            $aremplacer[] = trim($choisir[rand(0, count($choisir) - 1)]);
        }
        $reponse = str_replace($atrouver, $aremplacer, $txt);
        return $this->spinContent($reponse);
    }

    function timezone() {
        echo date_default_timezone_get();
        echo '<br/>';
        $offset = $this->get_timezone_offset('UTC');
        echo $offset / 3600;
    }

    /**    Returns the offset from the origin timezone to the remote timezone, in seconds.
     *    @param $remote_tz;
     *    @param $origin_tz; If null the servers current timezone is used as the origin.
     *    @return int;
     */
    function get_timezone_offset($remote_tz, $origin_tz = null) {
        if ($origin_tz === null) {
            if (!is_string($origin_tz = date_default_timezone_get())) {
                return false; // A UTC timestamp was returned -- bail out!
            }
        }
        $origin_dtz = new DateTimeZone($origin_tz);
        $remote_dtz = new DateTimeZone($remote_tz);
        $origin_dt = new DateTime("now", $origin_dtz);
        $remote_dt = new DateTime("now", $remote_dtz);
        $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
        return $offset;
    }

    function set_iw_status() {
        $this->loadModel('Iwarticle');
        $this->Iwarticle->virtualFields = array(
            'post_count' => 'SELECT COUNT(r.id) FROM iwreports r WHERE Iwarticle.id = r.article_id'
        );
        $cond = "Iwarticle.status != 2";
        $allItems = $this->Iwarticle->find('all', array('conditions' => $cond, 'fields' => 'id,user_id,user_type,status,post_count,paused_till,created'));
        //pr($allItems);

        $allowed = array(
            1 => 500,
            2 => 250,
            3 => 100
        );

        $nowDate = date('Y-m-d');
        $updatedIds = array();
        if ($allItems) {
            foreach ($allItems as $item) {
                $arr = $item['Iwarticle'];
                $userType = $arr['user_type'];
                $updateArr = array();
                if ($arr['post_count'] >= $allowed[$userType]) {
                    $updateArr['status'] = 2;
                } elseif ($arr['status'] == 0 && (isset($arr['paused_till']) && $arr['paused_till'] <= $nowDate)) {
                    $updateArr['status'] = 1;
                }
                if ($updateArr) {
                    $updateArr['paused_till'] = null;
                    $updateArr['id'] = $arr['id'];
                    $updatedIds[] = $arr['id'];
                    $this->Iwarticle->save($updateArr, false);
                }
                //pr($updateArr);
            }
        }
        pr($updatedIds);
        //$this->cmail(TEST_EMAIL, 'iWriter Status Set', print_r($updatedIds, true));
    }

    function article_request() {
        $this->loadModel('Content');
        $cond = array('requested' => 1, 'track_id' => null);
        $fields = 'id,proj_type,title,categ,length,language,keywords,article_purpose,extra_info';
        $allItem = $this->Content->find('all', array('conditions' => $cond, 'fields' => $fields));

        if ($allItem) {
            $this->loadModel('Ready');
            $postUrl = 'iwriter.com';
            foreach ($allItem as $item) {
                $postData = $item['Content'];
                $contentId = $postData['id'];
                pr($postData);
                exit;
                unset($postData['id']);
                $returned = $this->Ready->_request($postUrl, 'POST', $postData);
                if (isset($returned['result']) && $returned['result']) {
                    $trackId = $returned['result'];
                    $this->Ready->id = $contentId;
                    $this->Ready->saveField('track_id', $trackId);
                }
            }
        }
        //pr($allItem);
    }

    function article_request_success() {
        if ($_POST) {
            pr($_POST);
            exit;
        }
    }

    function sluggify($url) {
        # Prep string with some basic normalization
        $url = strtolower($url);
        $url = strip_tags($url);
        $url = stripslashes($url);
        $url = html_entity_decode($url);

        # Remove quotes (can't, etc.)
        $url = str_replace('\'', '', $url);

        # Replace non-alpha numeric with hyphens
        $match = '/[^a-z0-9]+/';
        $replace = '-';
        $url = preg_replace($match, $replace, $url);

        $url = trim($url, '-');

        return $url;
    }

    function _autoDclPost($url, $postData) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        $result = curl_exec($curl);
        if (FALSE === ($result)) {
            return curl_errno($curl);
        } else {
            return strlen($result) < 40 ? $result : '0';
        }
        curl_close($curl);
    }

    function delPostCheck() {
        App::import('Vendor', 'IXR_Library');
        $postId = 81;
        $fullRpcDomain = 'http://localhost/wpfresh/index.php?' . $this->_dclDeletePostId . '=' . $postId;
        $fullRpcDomain = 'http://localhost/wpfresh/xmlrpc.php';
        $fullRpcDomain = 'http://www.buying-tax-free-camel-cigarettes.com/xmlrpc.php';
        $fullRpcDomain = 'http://www.buying-tax-free-camel-cigarettes.com';
        $fullRpcDomain = 'http://www.buying-tax-free-camel-cigarettes.com/index.php?' . $this->_dclDeletePostId . '=' . $postId;
        //$fullRpcDomain = 'http://localhost/wpfresh/index.php';
        $client = new IXR_ClientSSL($fullRpcDomain);

        echo $success = $this->_remoteCurlGet($fullRpcDomain);
        //exit;

        /**
          $params[$this->_dclAutoPostKey] = '_dcl_auto_post_key';
          $params['post_title'] = 'hi me';
          $params['post_content'] = 'you there';
          $params['post_date'] = date('Y-m-d');
          $params['category_name'] = 'Custom New';
          /* */
        //$success = $this->_autoDclPost($fullRpcDomain, $params);
        //pr($success);
        //$params[$this->_dclDeletePostId] = $postId;
        exit;

        if (!$client->query('nitro.dclRemotePost', $params)) {
            $response['error'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
        } else {
            $response = $client->getResponse();
        }
        pr($response);
        exit;
    }

    function _remoteCurlGet($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($curl);
        if (FALSE === ($result)) {
            return 'httperror# ' . curl_error($curl);
        } else {
            return (trim($result) == '1') ? $result : '0';
        }
    }

    function upp() {
        $upp = array(
            array(
                'plugin' => 'dclwpnext/dclwpnext.php',
                'update_url' => 'http://tracker.seonitro.com/pluginsupdates/dclwpnext.zip',
                'latest_version' => '2.2.3',
            )
        );

        $del = array(
            array(
                'plugin' => 'dclwpnext/dclwpnext.php',
                'action' => 'activate'
            )
        );

        //$params[$this->_p_update_post_key] = json_encode($upp);
        $params[$this->_p_action_post_key] = json_encode($del);

        $this->loadModel('Domain');
        $cond = array(
            'fields' => 'id,domain',
            'conditions' => "networktype IN(5) AND hostname != 'SmartSEOHosting'",
                //'limit' => 2
        );
        $allDoms = $this->Domain->find('list', $cond);

        if ($allDoms) {
            foreach ($allDoms as $dId => $dom) {
                $url = 'http://www.' . $dom . '/index.php';
                $result = $this->_autoDclPost($url, $params);
                $idnt = $dId . ' ' . $dom;
                pr(array($idnt => $result));
            }
        }
        exit;
    }

    function deleteiwpost() {
        $reStts = '0';
        if ($_GET && isset($_GET['post_url'])) {
            $reStts = '1';
            $postUrl = trim($_GET['post_url']);
            if ($postUrl) {
                $this->loadModel('Iwreport');
                $delIndex['post_url'] = $postUrl;
                if ($this->Iwreport->deleteAll($delIndex, false)) {
                    $urlArr = $this->getDomPId($postUrl);
                    //pr($linkArr);
                    $domain = rtrim($urlArr[0], '/');
                    $postId = trim($urlArr[1]);
                    $fullRpcDomain = $domain . '/index.php?' . $this->_dclDeletePostId . '=' . $postId;
                    $success = $this->_remoteCurlGet($fullRpcDomain);
                    $this->loadModel('Iwdel');
                    $updateDel['post_url'] = $postUrl;
                    if (trim($success) == 1) {
                        $updateDel['status'] = 1;
                    }
                    $prev = $this->Iwdel->find('first', array('post_url' => $postUrl, 'fields' => 'id,status', 'limit' => 1, 'order' => 'id DESC'));
                    if ($prev) {
                        $updateDel['id'] = $prev['Iwdel']['id'];
                        $reStts = '2';
                    }
                    $this->Iwdel->save($updateDel, false);
                } else {
                    $reStts = '0';
                }
            }
        }
        exit($reStts);
    }

    function getDomPId($postUrl) {
        //$postLink = str_replace(array('http://', 'http://www.'), '', $postLink);
        return $postUrl = explode('?p=', $postUrl);
    }

    function download_articles() {

        Configure::write('debug', 2);

        $this->loadModel('Article');

        $orderedArticles = $this->Article->find('all', array('conditions' => "(body = '-' OR body IS NULL) AND keyword_status = 'closed'", 'fields' => 'id,proj_id,article_id,requested,status'));

        pr($orderedArticles);
        //exit;

        if ($orderedArticles) {
            $xmlRequest = '<request>';
            $xmlRequest .= '<user_id>Buckaroo</user_id><api_key>NWI5Njg4MWI3YjczMmQ1MjJmMzliNG</api_key>';
            $xmlRequest .= '<func_name>download_articles</func_name>';
            $xmlRequest .= '<proj_id>#PROJ_ID#</proj_id>';
            $xmlRequest .= '<article_id>#ARTICLE_ID#</article_id>';
            $xmlRequest .= '</request>';
            $xmlRequest = $this->cleanXML($xmlRequest);

            $url = 'http://www.iwriter.com/api/';
            $this->loadModel('Ready');
            App::uses('Xml', 'Utility');

            foreach ($orderedArticles as $orArt) {
                $xmlRequestFinal = '';
                $itemId = $orArt['Article']['id'];
                $projId = $orArt['Article']['proj_id'];
                $articleId = $orArt['Article']['article_id'];
                $xmlRequest = str_replace('#PROJ_ID#', $projId, $xmlRequest);
                $xmlRequestFinal = str_replace('#ARTICLE_ID#', $articleId, $xmlRequest);

                $xmlResult = $this->Ready->_post($url, $xmlRequestFinal);
                $xmlResult = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $xmlResult);
                $xmlArray = Xml::toArray(Xml::build($xmlResult));
                if ($xmlArray && isset($xmlArray['result']['title'])) {
                    $this->Article->id = $itemId;
                    $updateData['title'] = $xmlArray['result']['title'];
                    if (isset($xmlArray['result']['body'])) {
                        $updateData['body'] = $xmlArray['result']['body'];
                    }
                    $updateData['requested'] = 3;
                    $updateData['approval'] = 1;
                    $updateData['approval_date'] = date('Y-m-d H:i:s');
                    $this->Article->save($updateData, false);
                    //$to[] = 'motiur.rahman@aan-nahl.com';
                }
                $xmlArray['article_id'] = $itemId;
                pr($xmlArray);
                $to[] = TEST_EMAIL;
                $body = $this->pretty($xmlArray);
                //$this->cmail($to, 'iWriter Article Download', $body);
            }
        }
    }

    function get_project_status() {

        $this->loadModel('Article');

        $orderedArticles = $this->Article->find('all', array('conditions' => "requested = 2 AND proj_id != '' AND keyword_status != 'closed'", 'fields' => 'id,proj_id,requested,status'));

        pr($orderedArticles);

        if ($orderedArticles) {
            $xmlRequest = '<request>';
            $xmlRequest .= '<user_id>Buckaroo</user_id><api_key>NWI5Njg4MWI3YjczMmQ1MjJmMzliNG</api_key>';
            $xmlRequest .= '<func_name>get_project_status</func_name>';
            $xmlRequest .= '<proj_id>#PROJ_ID#</proj_id>';
            $xmlRequest .= '</request>';
            $xmlRequest = $this->cleanXML($xmlRequest);

            $url = 'http://www.iwriter.com/api/';
            $this->loadModel('Ready');
            App::uses('Xml', 'Utility');

            foreach ($orderedArticles as $orArt) {
                $xmlRequestFinal = '';
                $itemId = $orArt['Article']['id'];
                $projId = $orArt['Article']['proj_id'];
                $xmlRequestFinal = str_replace('#PROJ_ID#', $projId, $xmlRequest);

                $xmlResult = $this->Ready->_post($url, $xmlRequestFinal);
                $xmlArray = Xml::toArray(Xml::build($xmlResult));

                if (isset($xmlArray['result']['keyword']['status'])) {
                    $this->Article->id = $itemId;
                    $updateData['keyword_status'] = $xmlArray['result']['keyword']['status'];
                    if (isset($xmlArray['result']['keyword']['review_url'])) {
                        $updateData['review_url'] = $xmlArray['result']['keyword']['review_url'];
                    }
                    if (isset($xmlArray['result']['keyword']['id'])) {
                        $updateData['article_id'] = $xmlArray['result']['keyword']['article_id'];
                    }
                    $this->Article->save($updateData, false);
                }
                $xmlArray['article_id'] = $itemId;
                pr($xmlArray);
                //$to[] = TEST_EMAIL;
                $body = $this->pretty($xmlArray);
                //$this->cmail($to, 'iWriter Project Status RR', $body);
            }
        }
    }

}
