<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

set_time_limit(1200); // 20 minute as some page are heavy weight page

define('ADD_SUCCESS', '%s added successfully!');

define('UPDATE_SUCCESS', '%s updated successfully!');

define('DELETE_SUCCESS', '%s deleted successfully!');

define('ADD_ERROR', '%s could not be added!');

define('UPDATE_ERROR', '%s could not be updated!');

define('DELETE_ERROR', '%s could not be deleted!');

define('NO_RECORD_FOUND', 'No record found!');

Configure::write(
    'uploadConfig', array(
        'allimg' => array(
            array(
                'image_x' => 210,
                'image_y' => 200,
                'destination' => 'images/logos',
                'allowed' => array('image/jpeg', 'image/pjpeg'),
                'image_resize' => true,
            )
        ),
        'lesson_files' => array(
            array(
                'file_safe_name' => true,
                'destination' => 'files/lesson_files',
            )
        )
    )
);


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{


    public $helpers = array('Html', 'Form', 'Session', 'Js', 'Time', 'Cache');

    public $uses = array('User', 'Instruction', 'Domain', 'Usercredit', 'Serviceprice', 'Credithistory', 'Activitylog');

    public $components = array(

        'Session', 'Email', 'ImageResizer',

        'Auth' => array(

            'loginRedirect' => array('controller' => 'users'),

            'logoutRedirect' => array('controller' => 'users', 'action' => 'logout'),

            'authorize' => array('Controller'),

            'authError' => 'Invalid/Inactive username and/or password!',

        )

    );


    function beforeFilter()
    {

        // if a user is logged in and tries to go back to home page like www.admin.com then redirect him to dashboard

        $this->Auth->allow('authme');

        $this->Auth->authenticate = array(AuthComponent::ALL => array('userModel' => 'User', 'scope' => array('User.status' => '1')), 'Form');


        $tut = $this->Instruction->findBypagename($this->params['action']);

        $this->set('tutorial', $tut);


    }


    public function isAuthorized($user)
    {


    }


    function generateError($modelName = '', $errs = array())
    {

        if ($modelName == '')

            return '';

        $e = '<br/>';

        $i = 1;

        $errorArray = !empty($errs) ? $errs : $this->$modelName->invalidFields();

        $errorFields = '';

        foreach ($errorArray as $k => $v) {

            $errorFields .= $k . ',';

            $e .= '(' . $i . ') ' . $v[0] . ' <br/> ';

            $i++;

        }

        $this->Session->setFlash('Oops! ' . $e, 'flash_error');

        return 1;

    }


    function getPlainDomain($url)
    {

        $url = str_replace('http://', '', strtolower($url));

        $url = str_replace('https://', '', $url);

        $plainDomain = str_replace('www.', '', $url);

        if (strpos($url, '/')) {

            $plainDomain = strstr($url, '/', true);

        }

        if (strpos($plainDomain, '?')) {

            $plainDomain = strstr($plainDomain, '?', true);

        }

        return $plainDomain;

    }


    function generate_salt($len, $type = null)
    {

        /*

        * null = all

        * n = numbers

        * a = alpha small

        * A = alpha capital

        * aA = alpha small - capital

        * an = alphanumeric small

        * An = alphanumeric capital

        */

        switch ($type) {

            case 'a':

                $salt = $this->salt('a', 'z');

                break;

            case 'n':

                $salt = $this->salt('0', '9');

                break;

            case 'A':

                $salt = $this->salt('A', 'Z');

                break;

            case 'aA':

                $salt = $salt = $this->salt('a', 'z') . $this->salt('A', 'Z');

                break;

            case 'an':

                $salt = $salt = $this->salt('a', 'z') . $this->salt('0', '9');

                break;

            case 'An':

                $salt = $salt = $this->salt('A', 'Z') . $this->salt('0', '9');

                break;

            default:

                $salt = $this->salt('a', 'z') . $this->salt('A', 'Z') . $this->salt('0', '9');

                break;

        }

        $str = "";

        for ($i = 0; $i < $len; $i++) {

            $index = rand(0, strlen($salt) - 1);

            $str .= $salt[$index];

        }

        return $str;

    }


    function salt($from, $end)
    {

        $salt = '';

        for ($no = ord($from); $no <= ord($end); $no++) {

            $salt .= chr($no);

        }

        return $salt;

    }


    function loadhelptext($pagename = null, $type = null)
    {

        Configure::write('debug', 0);

        $this->autoRender = false;

        $this->layout = '';

        if (empty($pagename)) {

            echo 'Nothing Found to show!';

            exit;

        }

        $data = $this->Instruction->findBypagename($pagename);

        if (($type == 'helpguide') || ($type == 'toptext')) {

            echo '<div class="content" style="text-align:left !important; height:500px;overflow:auto;">' . $data['Instruction'][$type];

            echo '</div>';

        } else {

            //$src =   $type == 'v' ? 'http://rankratio.com/login/video/'. $data['Instruction']['video'] : $data['Instruction']['externallink'];

            $src = 'http://rankratio.com/login/video/' . $data['Instruction']['video'];

            $vtype = 'video/mp4';

            echo '<div class="flowplayer" data-swf="http://rankratio.com/login/js/flowplayer543/flowplayer.swf" data-ratio="0.4167">

                <video controls = "controls" width = "100%" height = "95%">

                <source src = "' . $src . '" type = "' . $vtype . '">

                </video>

                </div>';

        }

        exit;

    }


    function videopreview($file = null)
    {

        Configure::write('debug', 0);

        $this->autoRender = false;

        $this->layout = '';

        if (empty($file)) {

            echo 'Nothing Found to show!';

            exit;

        }

        //$src =   $type == 'v' ? 'http://rankratio.com/login/video/'. $data['Instruction']['video'] : $data['Instruction']['externallink'];

        $src = 'http://rankratio.com/login/media/' . $file;

        $vtype = 'video/mp4';

        echo '<div class="flowplayer" data-swf="http://rankratio.com/login/js/flowplayer543/flowplayer.swf" data-ratio="0.4167">

            <video controls = "controls" width = "100%" height = "95%">

            <source src = "' . $src . '" type = "' . $vtype . '">

            </video>

            </div>';

        exit;

    }


    function quickviewdomain($id = null)
    {

        Configure::write('debug', 0);

        $this->autoRender = false;

        $this->layout = '';

        if (empty($id)) {

            echo 'Nothing Found to show!';

            exit;

        }

        $domain = $this->Domain->findById($id);

        $this->set(compact('domain'));

        $this->viewPath = 'Common';

        return $this->render('quickviewdomain');

    }


    protected function updateAccountDebit($updateAccount)
    {


        $userId = $updateAccount['user_id'];

        $serviceId = $updateAccount['service_id'];

        $quantity = isset($updateAccount['quantity']) ? $updateAccount['quantity'] : 1;


        $updateArr = array(

            'Usercredit.service_used' => "Usercredit.service_used + {$quantity}"

        );

        $indexArr = array(

            'Usercredit.user_id' => $userId,

            'Usercredit.service_id' => $serviceId

        );


        if ($this->Usercredit->updateAll($updateArr, $indexArr, false)) {

            $this->uses[] = 'Credithistory';

            if ($this->Credithistory->save($updateAccount, false)) {

                return true;

            }

        }

        return false;

    }


    function getcategorypath($id = null)
    {

        if (empty($id))

            return '';

        $this->loadModel('Category');

        return $this->Category->getPath($id);

    }


    function setActivity($foruser = null, $showincalendar = '0', $generator = 'system', $activitytype = 'others', $job = '')
    {

        if (empty($foruser) || ($job == ''))

            return false;

        $d = array(

            'user_id' => $foruser,

            'showincalendar' => $showincalendar,

            'generator' => $generator,

            'activitytype' => $activitytype,

            'job' => $job

        );

        $this->Activitylog->save($d, false);

    }


    /**
     * This function generates random string for passwords or security salts etc.
     *
     * @param int $length
     * <p>length of the desired string, default 8</p>
     * @param int $strength
     * <p>deafult : all chars</p>
     * <p>1 : only numberic</p>
     * <p>2 : only lower chars</p>
     * <p>3 : only upper chars</p>
     * <p>4 : lower & upper chars</p>
     * <p>5 : numeric & lower chars</p>
     * <p>6 : numeric & upper chars</p>
     * <p>7 : numeric, lower & upper chars</p>
     * <p>8 : special chars</p>
     * @author mainul@aan-nahl.com
     * @version 1.0.1 [05 August 2014]
     */

    function genstr($length = null, $strength = null)
    {

        $length = $length ? $length : 8;

        $strength = $strength ? $strength : 0;


        $lower_ = range('a', 'z');

        $upper_ = range('A', 'Z');

        $numeric_ = range(0, 9);

        $special_ = array('!', '@', '#', '$', '%', '^', '&', '*', '_', '-');


        $charCombinations = array(

            array_merge($numeric_, $lower_, $upper_, $special_),

            $numeric_,

            $lower_,

            $upper_,

            array_merge($lower_, $upper_),

            array_merge($numeric_, $lower_),

            array_merge($numeric_, $upper_),

            array_merge($numeric_, $lower_, $upper_),

            $special_,

        );


        if (isset($charCombinations[$strength])) {

            $chars = $charCombinations[$strength];

            $max_chars = count($chars) - 1;

            srand((double)microtime() * 1000000);

            shuffle($chars);

            $rand_str = '';

            for ($i = 0; $i < $length; $i++) {

                $rand_str = ($i == 0) ? $chars[rand(0, $max_chars)] : $rand_str . $chars[rand(0, $max_chars)];

            }

            return $rand_str;

        }

    }


    function cleanXML($xmlRequest)
    {

        $xmlRequest = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $xmlRequest);

        $dom = new DOMDocument;

        $dom->preserveWhiteSpace = FALSE;

        $dom->loadXML($xmlRequest);

        $dom->formatOutput = TRUE;

        return $dom->saveXml();

    }


    function pretty($arr)
    {

        if (is_array($arr)) {

            $reStr = '<pre>';

            $reStr .= print_r($arr, true);

            $reStr .= '</pre>';

            return $reStr;

        }

    }


    // custom mail sender

    function sendEmail($to = '', $subject = '', $body = '', $attchfile = '', $from = '', $fromName = 'RankRatio', $templates = 'default')
    {

        App::uses('CakeEmail', 'Network/Email');

        $email = new CakeEmail();

        $email->helpers('Html');

        $email->viewVars(array('body' => $body));

        $email->from(array('support@rankratio.com' => 'RankRatio'));

        $email->to($to);

        $email->subject('[RankRatio] ' . $subject);

        $email->template('email', 'default');

        $email->emailFormat('html');

        $email->transport('Mail');

        if ($attchfile != '')

            $email->attachments($attchfile);

        $email->send();

    }

    function cmail($to = '', $subject = '', $body = '', $attchfile = '', $from = '', $fromName = 'RankRatio', $templates = 'default')
    {

        App::uses('CakeEmail', 'Network/Email');

        $email = new CakeEmail();

        $email->helpers('Html');

        $email->viewVars(array('body' => $body));

        $email->from(array('support@rankratio.com' => 'RankRatio'));

        $email->to($to);

        $email->subject('[RankRatio] ' . $subject);

        $email->template('email', 'default');

        $email->emailFormat('html');

        $email->transport('Mail');

        if ($attchfile != '')

            $email->attachments($attchfile);

        $email->send();

    }

    protected function admin_ip()
    {

        $iplist = unserialize(file_get_contents("http://domaincontrol.seonitro.com/getproxy/getproxy.php"));

        $data['ip'] = $iplist['ips'];

        $access = explode(":", $iplist['access']);

        $data['username'] = $access[0];

        $data['password'] = $access[1];

        $data['port'] = $iplist['port'];

        return $data;

    }


    function all_domain($pr_campaigns_id)
    {

        $this->loadModel('Pruserdomain');

        $all_domain = $this->Pruserdomain->query("SELECT * FROM `pr_user_domain` WHERE `pr_campaigns_id` = " . $pr_campaigns_id);


        $domain_ar = array();

        foreach ($all_domain as $row) {

            $domain_ar[] = $row['pr_user_domain']['domain'];

        }

        return $domain_ar;

    }


    function single_domain($id)
    {

        $this->loadModel('Pruserdomain');

        $domain = $this->Pruserdomain->query("SELECT * FROM `pr_user_domain` WHERE `id` = " . $id);

        $single_domain['domain'] = $domain[0]['pr_user_domain']['domain'];

        $single_domain['pr_campaigns_id'] = $domain[0]['pr_user_domain']['pr_campaigns_id'];

        return $single_domain;

    }


    function pr_instant_check_limit_per_day()
    {

        $this->loadModel('Setting');

        $settings = $this->Setting->query("SELECT * FROM `settings` LIMIT 1");

        return $settings[0]['settings']['pr_instant_check_limit_per_day'];

    }


    function credit_needed()
    {

        $year = date('Y');

        $month = date('m');

        $today = date('d');

        $total_day_of_this_month = date('t');

        //$total_day_of_this_month =  $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year %400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);

        $left_day = $total_day_of_this_month - $today;


        $this->loadModel('Rankcampaign');

        $this->loadModel('Setting');

        $all_active_campaign = $this->Rankcampaign->query('SELECT * FROM rankcampaigns WHERE status = 1');

        $total_kds_engine = 0;

        foreach ($all_active_campaign as $campaign) {

            $total_engine = $campaign['rankcampaigns']['google'] + $campaign['rankcampaigns']['yahoo'] + $campaign['rankcampaigns']['bing'];

            $kds = explode("\n", trim($campaign['rankcampaigns']['c_keywords']));

            $total_kds_engine += $total_engine + count($kds);

        }


        $setting_info = $this->Setting->query('SELECT * FROM settings WHERE id = 1');

        //$instant_price = $setting_info[0]['settings']['instant_price'];

        $keyword_price = $setting_info[0]['settings']['keyword_price'];

        $gorilla_credit = $setting_info[0]['settings']['gorilla_credit'];

        $credit_needed = ($keyword_price / $total_day_of_this_month * $total_kds_engine * $left_day);

        return array('credit_needed' => $credit_needed, 'gorilla_credit' => $gorilla_credit);

    }

    function fileExt($name = null) {
        return strtolower(array_pop(explode(".",$name)));
    }

    function array_to_csv($array, $download = "")
    {

        if ($download != "") {

            header('Content-Type: application/csv');

            header('Content-Disposition: attachement; filename="' . $download . '"');

        }


        ob_start();

        $f = fopen('php://output', 'w') or show_error("Can't open php://output");

        $n = 0;


        foreach ($array as $line) {

            $n++;

            if (!fputcsv($f, $line)) {

                show_error("Can't write line $n: $line");

            }

        }

        fclose($f) or show_error("Can't close php://output");

        $str = ob_get_contents();

        ob_end_clean();


        if ($download == "") {

            return $str;

        } else {

            echo $str;

        }

    }


    function recredit($userId, $serviceId, $quantity)
    {

        $this->loadModel('Usercredit');


        $updateArr = array(

            'Usercredit.service_used' => "Usercredit.service_used - {$quantity}"

        );

        $indexArr = array(

            'Usercredit.user_id' => $userId,

            'Usercredit.service_id' => $serviceId

        );


        if ($this->Usercredit->updateAll($updateArr, $indexArr, false)) {

            return true;

        }

        return false;

    }


    function remapcredit($userId, $serviceId, $serviceUsed)
    {

        $this->loadModel('Usercredit');


        $updateArr = array(

            'Usercredit.service_used' => $serviceUsed

        );

        $indexArr = array(

            'Usercredit.user_id' => $userId,

            'Usercredit.service_id' => $serviceId

        );


        if ($this->Usercredit->updateAll($updateArr, $indexArr, false)) {

            return true;

        }

        return false;

    }


    function varExp($arr)
    {

        if (is_array($arr)) {

            $reStr = '<pre>';

            $reStr .= var_export($arr, true);

            $reStr .= '</pre>';

            return $reStr;

        }

    }


    function deletemediafile($table = null, $id = null, $column = null, $file = null)
    {

        $this->autoRender = false;

        $this->layout = '';

        $this->loadModel('Commonmodel');

        $this->Commonmodel->useTable = $table;

        $data = array('id' => $id, $column => '');

        if ($this->Commonmodel->save($data, false)) {

            $folder = WWW_ROOT . 'media';

            $unfile = $folder . '/' . $file;

            @unlink($unfile);

            echo '';


        } else {

            echo 'error';

        }

    }


    function majesticseo_update()
    {

        //$this->loadModel('GoogePageRank');

        $this->loadModel('Majesticseo');


        //$before_date = date("Y-m-d", time() - 24 * 3600 * 180);

        $all_domain = $this->Domain->query("SELECT id,domain,cloud FROM domains WHERE `archive` = 0 AND programs_id != 2  ORDER BY `id` ASC LIMIT 1"); //

        //echo count($all_domain); exit;

        foreach ($all_domain as $row) {

            $domain = $row['domains']['domain'];

            $id = $row['domains']['id'];

            $cloud = $row['domains']['cloud'];

            $majesticseo = $this->Majesticseo->GetIndexItemInfo($domain);


            App::import('Vendor', 'seomoz_vendor/seomoz');

            $obj = new seomoz();

            $seoObj = $obj->getData($domain);

            $seomozrank = $seoObj['seomozrank'];


            if ($seomozrank !== false) {

                $update['seomoz_rank'] = $seomozrank;

            }


            $ext_backlinks = $update['ext_backlinks'] = $majesticseo['row'][4];

            $cflow = $update['cflow'] = $majesticseo['row'][33];

            $tflow = $update['tflow'] = $majesticseo['row'][34];


            $update['id'] = $id;

            $update['cloud'] = $cloud;

            //$update['pr_script_run'] = date('Y-m-d G:i:s');


            $this->Domain->save($update, false);


            $sql = "UPDATE `domains` SET `cflow` = '$cflow',`tflow` = '$tflow',`seomoz_rank` = '$seomozrank',`ext_backlinks` = '$ext_backlinks' WHERE `mother_id` = $id ";

            $this->DomainNitro->query($sql);

            unset($update);

        }

    }


    function update_flow()
    {

        //$this->loadModel('GoogePageRank');

        $this->loadModel('Majesticseo');

        //$this->loadModel('Robowhois');

        $this->autoRender = false;

        $admin_ip = $this->admin_ip();

        //$before_date = date("Y-m-d", time() - 24 * 3600 * 180);

        $all_domain = $this->Domain->query("SELECT id,domain,cflow,tflow,seomoz_rank,ext_backlinks,cloud FROM domains WHERE `archive` = 0 AND programs_id = 1");


        //pr($all_domain);

        //exit;

        //pr(count($all_domain));exit;

        foreach ($all_domain as $row) {

            $domain = $row['domains']['domain'];

            $id = $row['domains']['id'];

            $cloud = $row['domains']['cloud'];

            //*

            //$majesticseo = $this->Majesticseo->GetIndexItemInfo($domain);


            $proxyIp = $admin_ip['ip'];


            $proxy_user_pass = $admin_ip['password'];

            $proxy_port = $admin_ip['port'];


            App::import('Vendor', 'seomoz_vendor/seomoz');

            $obj = new seomoz();

            $seoObj = $obj->getData($domain);

            $seomozrank = $seoObj['seomozrank'];


            if ($seomozrank !== false) {

                $update['seomoz_rank'] = $seomozrank;

            }

            //$ext_backlinks = $update['ext_backlinks'] = $majesticseo['row'][4];

            //$cflow = $update['cflow'] = $majesticseo['row'][33];

            //$tflow = $update['tflow'] = $majesticseo['row'][34];

            //*/

            $update['id'] = $id;

            $update['cloud'] = $cloud;

            $update['pr_script_run'] = date('Y-m-d G:i:s');


            pr($row);

            pr($update);

            echo '<br/>=======<br/>';

            //exit;


            $this->Domain->save($update, false);


            $modified = date('Y-m-d H:i:s');


            //$sql = "UPDATE `domains` SET `cflow` = '$cflow',`tflow` = '$tflow',`seomoz_rank` = '$seomozrank',`ext_backlinks` = '$ext_backlinks',modified = '$modified' WHERE `mother_id` = $id ";

            $sql = "UPDATE `domains` SET `seomoz_rank` = '$seomozrank',modified = '$modified' WHERE `mother_id` = $id ";

            $this->DomainNitro->query($sql);

            unset($update);

            sleep(5);

        }

        exit('success');

    }


}
