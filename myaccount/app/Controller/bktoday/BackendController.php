<?php

class BackendController extends AppController {

    function beforeFilter() {
        if (!method_exists($this, $this->params['action'])) {
            $this->redirect('dashboard');
        }
        parent::beforeFilter();
        $uRole = AuthComponent::user('role');
        $this->set('uRole', $uRole);
        $this->set('userRoles', array('master', 'admin', 'seo'));

        $aActions = get_class_methods($this);
        $parentMethods = get_class_methods(get_parent_class($this));
        $cActions = array_diff($aActions, $parentMethods);

        $authDeny = array(
            'master' => array(),
            'admin' => array('user', 'users'),
            'seo' => array('user', 'users')
        );
        $authAllow = array_diff($cActions, $authDeny[$uRole]);
        //pr($authAllow);
        //exit;
        $this->Auth->allow($authAllow);

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);
    }

    function beforeRender() {
        parent::beforeRender();
    }

    function logout() {
        $this->redirect($this->Auth->logout());
    }

    function dashboard() {
        
    }

    function user($itemId = null) {
        $this->loadModel('Register');
        $userId = AuthComponent::user('id');
        if ($this->request->is('post')) {
            //pr($this->data); exit;

            $password = $this->data['Register']['user_password'];
            $this->request->data['Register']['password'] = $password;
            if ($itemId) {
                $this->Register->id = $itemId;
                unset($this->request->data['Register']['created_by']);
                unset($this->Register->validate['username']['already_taken']);
                unset($this->Register->validate['email']['already_taken']);
                unset($this->Register->validate['password']['not_empty']);
                if (!$password) {
                    unset($this->Register->validate['password']['password_length']);
                }
            }

            $this->Register->set($this->data['Register']);
            if ($this->Register->validates()) {
                $this->request->data['Register']['created_by'] = $userId;
                $this->request->data['Register']['modified_by'] = $userId;

                if ($itemId == null || ($itemId && $password)) {
                    $this->request->data['Register']['password'] = AuthComponent::password($password);
                    $this->request->data['Register']['plainpass'] = $password;
                }
                unset($this->request->data['Register']['user_password']);

                //pr($this->data['Register']); exit;
                if ($this->Register->save($this->data['Register'], false)) {
                    $fMsg = sprintf($itemId ? UPDATE_SUCCESS : ADD_SUCCESS, 'User');
                    $this->Session->setFlash($fMsg, 'flash_success');
                } else {
                    $fMsg = sprintf($itemId ? UPDATE_ERROR : ADD_ERROR, 'User');
                    $this->Session->setFlash($fMsg, 'flash_error');
                }
                $this->redirect($this->params['action'] . '/' . $itemId);
            } else {
                $this->generateError('Register');
            }
        } else {
            if ($itemId) {
                $itemInfo = $this->Register->find('first', array('conditions' => array('id' => $itemId)));
                if ($itemInfo) {
                    unset($itemInfo['Register']['password']);
                    $this->data = $itemInfo;
                }
            }
        }
        $this->set('title_for_layout', ($itemId ? 'Updating' : 'Adding') . ' User');
    }

    function users() {
        $this->loadModel('User');
        $itemList = $this->User->find('all', array('conditions' => ""));
        $this->set(compact('itemList'));
        $this->set('title_for_layout', 'Manage Users');
    }

    function article($itemId = null) {
        $this->loadModel('Article');
        $userId = AuthComponent::user('id');
        if ($this->request->is('post')) {
            //pr($this->data);exit;
            $this->Article->set($this->data['Article']);
            if ($this->Article->validates()) {
                $this->request->data['Article']['user_id'] = $userId;
                if ($itemId) {
                    $this->Article->id = $itemId;
                }
                if ($this->Article->save($this->data['Article'], false)) {
                    $fMsg = sprintf($itemId ? UPDATE_SUCCESS : ADD_SUCCESS, 'Article');
                    $this->Session->setFlash($fMsg, 'flash_success');
                } else {
                    $fMsg = sprintf($itemId ? UPDATE_ERROR : ADD_ERROR, 'Article');
                    $this->Session->setFlash($fMsg, 'flash_error');
                }
                $this->redirect($this->params['action'] . '/' . $itemId);
            } else {
                $this->generateError('Article');
            }
        } else {
            if ($itemId) {
                $itemInfo = $this->Article->find('first', array('conditions' => array('id' => $itemId, 'user_id' => $userId)));
                if ($itemInfo) {
                    $this->data = $itemInfo;
                }
            }
        }

        $this->loadModel('Any');
        $this->Any->useTable = 'categs';
        $allCategs = $this->Any->find('list', array('fields' => 'id,name'));
        $this->set(compact('allCategs'));
        $this->set('title_for_layout', 'Add Article');
    }

    function articles() {
        $userId = AuthComponent::user('id');
        $this->loadModel('Article');
        $itemList = $this->Article->find('all', array('conditions' => "user_id = $userId AND requested NOT IN(1,2)"));
        $this->set(compact('itemList'));
        $this->set('title_for_layout', 'Manage Articles');
    }

    function order_article() {
        $this->loadModel('Article');
        $this->Article->validate['special_instructions'] = array(
            'not_empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Special Instruction is required.',
                'allowEmpty' => false,
                'required' => true,
            ),
        );
        $userId = AuthComponent::user('id');
        if ($this->request->is('post')) {

            $this->request->data['Article']['body'] = '-';
            $this->Article->set($this->data['Article']);
            if ($this->Article->validates()) {
                $this->request->data['Article']['user_id'] = $userId;
                $this->request->data['Article']['requested'] = 1;
                $this->request->data['Article']['request_date'] = date('Y-m-d H:i:s');
                if ($this->Article->save($this->data['Article'], false)) {
                    $fMsg = 'Request accepted successfully.';
                    $this->Session->setFlash($fMsg, 'flash_success');
                } else {
                    $fMsg = 'Request could not be saved.';
                    $this->Session->setFlash($fMsg, 'flash_error');
                }
                //pr($this->data); exit;
                $this->redirect($this->params['action']);
            } else {
                $this->generateError('Article');
            }
        }

        $itemList = $this->Article->find('all', array('conditions' => array('user_id' => $userId, 'requested !=' => 0), 'order' => 'id DESC'));
        $this->set(compact('itemList'));
        $this->set('title_for_layout', 'My Articles');

        $this->loadModel('Any');
        $this->Any->useTable = 'categs';
        $allCategs = $this->Any->find('list', array('fields' => 'id,name', 'order' => 'name ASC'));
        $this->Any->useTable = null;

        $aLanguage = array('en' => 'English (US)', 'uk' => 'English (UK)', 'fr' => 'French', 'de' => 'German', 'es' => 'Spanish');
        $this->set(compact('aLanguage'));

        $this->set(compact('allCategs'));
        $this->set('title_for_layout', 'Order Article');
    }

    function domain($itemId = null) {
        $this->loadModel('Domain');
        if ($this->request->is('post')) {
            if ($itemId) {
                unset($this->Domain->validate['domain']['already_taken']);
                $this->Domain->id = $itemId;
            }
            $this->Domain->set($this->data['Domain']);
            if ($this->Domain->validates()) {
                $domain = $this->getPlainDomain($this->data['Domain']['domain']);
                $this->request->data['Domain']['domain'] = $domain;
                $this->request->data['Domain']['user_id'] = $this->Auth->user('id');
                $this->request->data['Domain']['owner_id'] = $this->Auth->user('id');

                if ($this->Domain->save($this->data['Domain'], false)) {
                    $fMsg = sprintf($itemId ? UPDATE_SUCCESS : ADD_SUCCESS, 'Domain');
                    $this->Session->setFlash($fMsg, 'flash_success');
                } else {
                    $fMsg = sprintf($itemId ? UPDATE_ERROR : ADD_ERROR, 'Domain');
                    $this->Session->setFlash($fMsg, 'flash_error');
                }

                $this->redirect($this->params['action'] . '/' . $itemId);
            } else {
                $this->generateError('Domain');
            }
        } else {
            if ($itemId) {
                $itemInfo = $this->Domain->find('first', array('conditions' => array('id' => $itemId)));
                if ($itemInfo) {
                    $this->data = $itemInfo;
                }
            }
        }
        $this->set('title_for_layout', 'Add New Domain');
    }

    function import_domains() {
        if ($this->request->is('post')) {
            if ($this->data['Domain']['file']['name'] != '') {
                $fname = $this->data['Domain']['file']['name'];
                $filename = $this->data['Domain']['file']['tmp_name'];

                App::import('Vendor', 'PHPExcel');
                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                    $worksheetTitle = $worksheet->getTitle();
                    $highestRow = $worksheet->getHighestRow(); // e.g. 10
                    $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                }

                $columnArr[] = 'domain';
                $columnArr[] = 'ausername';
                $columnArr[] = 'apassword';
                $columnArr[] = 'cms';

                $firstRowArr = array();
                $valueArr = array();
                for ($row = 1; $row <= $highestRow; ++$row) {
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        if ($row == 1) {
                            $firstRowArr[] = trim($cell->getValue());
                        } else {
                            $dbCol = strtolower($firstRowArr[$col]);
                            if ($dbCol) {
                                $dbCol = ($dbCol == 'username') ? 'ausername' : $dbCol;
                                $dbCol = ($dbCol == 'password') ? 'apassword' : $dbCol;
                                if (in_array($dbCol, $columnArr)) {
                                    $valueArr[$row][$dbCol] = trim($cell->getValue());
                                    if ($dbCol == 'domain') {
                                        $valueArr[$row][$dbCol] = $this->getPlainDomain(trim($cell->getValue()));
                                    }
                                }
                                $valueArr[$row]['networktype'] = 1;
                                $valueArr[$row]['user_id'] = $this->Auth->user('id');
                                $valueArr[$row]['owner_id'] = $this->Auth->user('id');
                            }
                        }
                    }
                }

                //pr($firstRowArr);
                //pr($valueArr);
                //exit;

                $eMsg = '';
                if ($valueArr) {
                    $inserted = array();
                    foreach ($valueArr as $key => $vals) {
                        //echo 'here';exit;
                        $finalValues['Domain'] = $vals;
                        $this->Domain->set($finalValues);
                        if ($this->Domain->validates()) {
                            $cms = $vals['cms'];
                            $domain = $vals['domain'];
                            $ausername = $vals['ausername'];
                            $apassword = $vals['apassword'];

                            if ($cms == 'wordpress') {
                                $wpUserInfo['site_url'] = 'http://' . $domain;
                                $wpUserInfo['username'] = $ausername;
                                $wpUserInfo['password'] = $apassword;
                            }
                            $this->Domain->create();
                            if ($this->Domain->save($finalValues, false)) {
                                $inserted[] = $domain;
                            }
                        } else {
                            $this->generateError('Domain');
                        }
                    }
                }
                if (!empty($inserted)) {
                    $inserted = implode(',', $inserted);
                    $eMsg .= 'Domain has been Inserted successfully For: ' . $inserted;
                }
                $this->Session->setFlash($eMsg, 'flash_success');
            }
            $this->redirect($this->params['action']);
        }
        $this->set('title_for_layout', 'Import Primate Domains');
    }

    function domains() {
        $this->loadModel('Domain');
        $itemList = $this->Domain->find('all');
        $this->set(compact('itemList'));
        $this->set('title_for_layout', 'Manage Domains');
    }

    function support() {
        echo AuthComponent::password('Hcm13$$!!');
        exit;
        echo '<br/>';
        echo $this->generate_salt(40, 'all');
        echo '<br/>';
        echo $this->generate_salt(29, 'n');
        exit;
        //$this->render('sql');
    }

}
