<?php

class AdminsController extends AppController {

    public $uses = array('User', 'Admin', 'Register', 'Campaign', 'Anchortext', 'Domain', 'Blogroll', 'Blogrolltext', 'Trackeddomain', 'Package', 'Credit', 'Mysite', 'Creditlog', 'Instruction', 'Setting');

    function beforeFilter() {
        Configure::write('debug', 2);
        $this->params['action'];
        if (!method_exists($this, $this->params['action'])) {
            $this->redirect('dashboard');
        }
        AuthComponent::$sessionKey = 'Auth.Usera';
        parent::beforeFilter();
    }

    function isAuthorized($user) {
        if (isset($user['role']) && 'admin' === $user['role']) {
            return true; //Admin can access every action
        } else {
            $this->redirect('/users/login');
        }
        return false; // The rest don't
    }

    function beforeRender() {
        App::uses('CakeNumber', 'Utility');
        parent::beforeRender();
        $this->Auth->allow('gorilla_credits');
        $siteCountAllCamp = $this->Campaign->find('all', array(
            'conditions' => array(""),
            'recursive' => 1
        ));
        $siteAllCountUser = $this->User->find('all', array(
            'conditions' => array("User.role = 'client' "),
            'recursive' => 1
        ));

        $this->set('siteAllCountUser', count($siteAllCountUser));
        $this->set('siteCountAllCamp', count($siteCountAllCamp));
    }

    // check if a user can edit, delete a record. I.e IS that belongs to him or not
    public function haveAuthToDoAction($model = '', $searchid = '', $relationalKey = '') {
        $cond = $model . '.' . $relationalKey . ' = ' . $this->Auth->user('id') . ' AND ' . $model . '.id = ' . $searchid;
        return $this->{$model}->find('count', array('recursive' => -1, 'conditions' => array($cond)));
    }

    // log out users
    function logout() {
        AuthComponent::$sessionKey = 'Auth.Usera';
        //$this->Session->setFlash('You\'ve successfully logged out.', 'flash_success');
        $this->redirect($this->Auth->logout());
    }

    function sendwelcomeemail() {
        $this->autoRender = false;
        $this->layout = '';
        die();
        // send an email to the user that his account has heen created
        $replace = array(
            '/\[firstname\]/',
            '/\[lastname\]/',
            '/\[username\]/',
            '/\[password\]/',
            '/\[email\]/',
        );
        $body = file_get_contents(WWW_ROOT . 'files/adduser.ctp');

        $users = $this->User->find('all', array('conditions' => array("User.role = 'client'")));
        foreach ($users as $k => $data) {
            $replacement = array(
                $data['User']['firstname'],
                $data['User']['lastname'],
                $data['User']['username'],
                $data['User']['ccode'],
                $data['User']['email'],
            );
            $ebody = preg_replace($replace, $replacement, $body);
            echo 'sending email to - ' . $data['User']['email'] . ' ' . $data['User']['username'] . ' ' . $data['User']['ccode'] . '<br>';
            $this->sendEmail($data['User']['email'], 'Welcome to SEONitro Dashboard', $ebody);
            sleep(1);
        }
        $this->set('title_for_layout', 'sendwelcome');
    }

    // dashboard reports
    function dashboard() {
        $this->set('title_for_layout', 'dashboard');
    }

    function services() {
        $this->uses[] = 'Service';
        $allServices = $this->Service->find('all');
        $this->set(compact('allServices'));

        $this->set('title_for_layout', 'Manage Services');
    }

    function addservice() {
        if ($this->request->is('post')) {
            $this->uses[] = 'Service';
            $this->Service->set($this->data['Service']);
            if ($this->Service->validates()) {
                if ($this->Service->save($this->data['Service'], false)) {
                    $eMsg = 'New Service has been added successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'New Service could not be added.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action']);
            } else {
                $this->generateError('Service');
            }
        }

        $this->set('title_for_layout', 'Add Service');
    }

    function editservice($sId = null) {
        $this->uses[] = 'Service';
        if ($this->request->is('post')) {
            $this->Service->set($this->data['Service']);
            if ($this->Service->validates()) {
                if ($this->Service->save($this->data['Service'], false)) {
                    $eMsg = 'Service has been updated uccessfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'Could not Update Service.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action'] . '/' . $sId);
            } else {
                $this->generateError('Service');
            }
        }

        $data = $this->Service->findById($sId);

        $this->data = $data;
        $this->set('data', $data);
        $this->set('title_for_layout', 'Edit Service');
    }

    function packages() {
        $allPackages = $this->Package->find('all');
        $this->set(compact('allPackages'));

        $this->set('title_for_layout', 'Manage Packages');
    }

    function addpackage() {
        if ($this->request->is('post')) {
            $this->Package->set($this->data['Package']);
            if ($this->Package->validates()) {
                $transactionFlag = true;
                $packageId = 0;
                $this->Package->begin();

                if ($transactionFlag) {
                    if (!$this->Package->save($this->data['Package'])) {
                        $transactionFlag = false;
                    } else {
                        $packageId = $this->Package->getLastInsertId();
                    }
                }
                if ($transactionFlag) {
                    if (isset($this->data['Serviceprice']['unitprice'])) {
                        $this->uses[] = 'Serviceprice';
                        $servicePrice = array();
                        $count = 0;
                        foreach ($this->data['Serviceprice']['unitprice'] as $k => $price) {
                            $servicePrice[$count]['service_id'] = intval($k);
                            $servicePrice[$count]['package_id'] = intval($packageId);
                            //$servicePrice[$count]['unitprice'] = floatval($price);
                            $servicePrice[$count]['servicelimit'] = intval($this->data['Serviceprice']['servicelimit'][$k]);
                            //$servicePrice[$count]['status'] = $this->data['Serviceprice']['status'][$k];
                            $count++;
                        }
                        if (!$this->Serviceprice->saveAll($servicePrice)) {
                            $transactionFlag = false;
                        }
                    }
                }
                if ($transactionFlag) {
                    $this->Package->commit();
                    $eMsg = 'New Package has been added successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'New Package could not be added.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action']);
            } else {
                $this->generateError('Package');
            }
        }

        $this->uses[] = 'Service';
        $cond = array(
            'conditions' => 'Service.status = 1'
        );
        $serviceList = $this->Service->find('all', $cond);

        $this->set(compact('serviceList'));

        $this->set('title_for_layout', 'Add Package');
    }

    function editpackage($pId = null) {
        if ($this->request->is('post')) {

            $this->Package->set($this->data['Package']);
            if ($this->Package->validates()) {
                $transactionFlag = true;
                $this->Package->begin();

                if ($transactionFlag) {
                    if (!$this->Package->save($this->data['Package'])) {
                        $transactionFlag = false;
                    }
                }
                if ($transactionFlag) {
                    if (isset($this->data['Serviceprice']['unitprice'])) {
                        $this->uses[] = 'Serviceprice';
                        $servicePrice = array();
                        $count = 0;
                        foreach ($this->data['Serviceprice']['unitprice'] as $k => $price) {
                            $servicePrice[$count]['service_id'] = intval($k);
                            $servicePrice[$count]['package_id'] = intval($pId);
                            //$servicePrice[$count]['unitprice'] = floatval($price);
                            $servicePrice[$count]['servicelimit'] = intval($this->data['Serviceprice']['servicelimit'][$k]);
                            //$servicePrice[$count]['status'] = $this->data['Serviceprice']['status'][$k];
                            $count++;
                        }
                        if (!$this->Serviceprice->deleteAll(array('Serviceprice.package_id' => $pId))) {
                            $transactionFlag = false;
                        }
                        if (!$this->Serviceprice->saveAll($servicePrice)) {
                            $transactionFlag = false;
                        }
                    }
                }
                if ($transactionFlag) {
                    $this->Package->commit();
                    $eMsg = 'Package has been updated uccessfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'Could not Update Package.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action'] . '/' . $pId);
            } else {
                $this->generateError('Package');
            }
        }

        $this->uses[] = 'Service';
        $cond2 = array(
            'conditions' => 'Service.status = 1'
        );
        $serviceList = $this->Service->find('all', $cond2);

        //pr($serviceList);
        //exit;

        $this->set(compact('serviceList'));

        $this->uses[] = 'Serviceprice';
        $cond3 = array(
            'conditions' => "Serviceprice.package_id = $pId"
        );
        $servicePrices = $this->Serviceprice->find('all', $cond3);

        $currentPrices = array();
        foreach ($serviceList as $k => $service) {
            $serviceId = $service['Service']['id'];
            $defaultUnitPrice = $service['Service']['default_unit_price'];
            foreach ($servicePrices as $sP) {
                if ($sP['Serviceprice']['service_id'] == $serviceId) {
                    $currentPrices['unitprice'][$serviceId] = $defaultUnitPrice;
                    $currentPrices['servicelimit'][$serviceId] = $sP['Serviceprice']['servicelimit'];
                    $currentPrices['status'][$serviceId] = $sP['Serviceprice']['status'];
                }
            }
        }

        $data = $this->Package->findById($pId);

        $data['Serviceprice'] = $currentPrices;
        //pr($data);; exit;
        $this->data = $data;
        $this->set('data', $data);
        $this->set('cId', $pId);
        $this->set('title_for_layout', 'Edit Package');
    }

    function newclient() {
        if ($this->request->is('post')) {

            $givenPass = $this->data['User']['password'];
            $this->request->data['User']['password'] = ( trim($this->data['User']['password']) != '' ) ? AuthComponent::password($this->data['User']['password']) : '';
            $this->request->data['User']['next_package_id'] = $packageId = $this->data['User']['package_id'];
            $this->request->data['User']['package_renew_date'] = date('Y-m-d');
            $this->request->data['User']['package_apply_date'] = date('Y-m-d');
            $this->request->data['User']['lastsignin'] = date('Y-m-d G:i:s');
            $this->request->data['User']['status'] = '1';
            $this->request->data['User']['verified'] = '1';
            $this->request->data['User']['ccode'] = trim($givenPass);
            $this->request->data['User']['created_by'] = 'Admin';
            $this->Register->set($this->data['User']);

            if ($this->Register->validates()) {
                if ($this->User->save($this->data['User'], false)) {
                    $userId = $this->User->getLastInsertId();
                    $this->uses[] = 'Serviceprice';
                    $servicePricesByPackage = $this->Serviceprice->find('all', array('conditions' => "package_id = $packageId"));

                    $userCredit = array();
                    $count = 0;
                    foreach ($servicePricesByPackage as $k => $sPBP) {
                        $userCredit[$count]['user_id'] = intval($userId);
                        $userCredit[$count]['service_id'] = intval($sPBP['Serviceprice']['service_id']);
                        $count++;
                    }
                    if ($userCredit) {
                        $this->uses[] = 'Usercredit';
                        $this->Usercredit->set($userCredit);
                        if ($this->Usercredit->saveAll($userCredit)) {
                            //$this->User->saveField('currentcredit', $totalPrice);
                            $eMsg = 'New Client has been added successfully.';
                            $this->Session->setFlash($eMsg, 'flash_success');

                            // send an email to the user that his account has heen created
                            $replace = array(
                                '/\[firstname\]/',
                                '/\[lastname\]/',
                                '/\[username\]/',
                                '/\[password\]/',
                                '/\[email\]/',
                            );
                            $body = file_get_contents(WWW_ROOT . 'files/adduser.ctp');
                            $replacement = array(
                                $this->data['User']['firstname'],
                                $this->data['User']['lastname'],
                                $this->data['User']['username'],
                                trim($givenPass),
                                $this->data['User']['email'],
                            );
                            $ebody = preg_replace($replace, $replacement, $body);
                            $this->sendEmail($this->data['User']['email'], 'Welcome to SEONitro Dashboard', $ebody);
                        } else {
                            $this->User->deleteAll(array('User.id' => $userId), false);
                            $eMsg = 'Client info could not be added.';
                            $this->Session->setFlash($eMsg, 'flash_error');
                        }
                    } else {
                        $eMsg = 'New Client has been added successfully.';
                        $this->Session->setFlash($eMsg, 'flash_success');
                    }
                } else {
                    $eMsg = 'Client could not be added.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action']);
            } else {
                $this->request->data['User']['password'] = $givenPass;
                $this->generateError('Register');
            }
        }

        // Find all the packages
        $packages = array('Paid' => array(), 'Free' => array());
        $package = $this->Package->find('all');
        if (count($package) > 0) {
            foreach ($package as $p) {
                if ($p['Package']['type'] == 1) {
                    $packages['Paid'][$p['Package']['id']] = $p['Package']['packagename'];
                } else {
                    $packages['Free'][$p['Package']['id']] = $p['Package']['packagename'];
                }
            }
        }
        $this->set('packages', $packages);
        $this->set('title_for_layout', 'Add New Client');
    }

    function manageclients() {
        $allUser = $this->User->find('all', array(
            'conditions' => array("User.role = 'client' "),
            'order' => array('User.firstname ASC'),
            'recursive' => 1,
        ));
        $this->set(compact('allUser'));

        $this->set('title_for_layout', 'Manage Clients');
    }

    function monthlyrenewals() {
        $this->loadModel('Monthlyrenewal');

        $this->Monthlyrenewal->bindModel(array(
            'hasOne' => array(
                'User' => array('foreignKey' => false, 'conditions' => "Monthlyrenewal.user_id = User.id"),
            )
        ));

        $allUsers = $this->Monthlyrenewal->find('all', array('conditions' => "Monthlyrenewal.status=0", 'order' => 'Monthlyrenewal.renew_date DESC'));

        //pr($allUsers);
        //exit;

        $this->set(compact('allUsers'));

        $this->set('title_for_layout', 'Monthly Renewal');
    }

    function renewal_action($action, $itemId) {

        if (empty($action) || empty($itemId)) {
            return '';
        }
        $this->autoRender = false;
        $this->layout = 'ajax';

        $status = 99999;
        if ($action == 'paid') {
            $status = 1;
        } elseif ($action == 'unpaid') {
            $status = 0;
        }

        if ($status == 0 || $status == 1) {
            $this->loadModel('Monthlyrenewal');
            $this->Monthlyrenewal->id = $itemId;
            if ($this->Monthlyrenewal->saveField('status', $status)) {
                $eMsg = 'Status updated successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
                echo '';
            } else {
                echo 'error';
            }
        } else {
            echo 'error';
        }
        exit;
    }

    function viewclient($id) {
        $data = $this->User->find('first', array(
            'conditions' => array("User.id = " . $id . " AND User.role = 'client' "),
            'recursive' => 1
        ));
        $this->set(compact('data'));
        $this->set('title_for_layout', 'View Client');
    }

    function editclient($uId = null) {
        if ($this->request->is('post')) {
            if ($this->User->validates()) {
                if (trim($this->data['UserEx']['password']) != '') {
                    $this->request->data['User']['password'] = AuthComponent::password($this->data['UserEx']['password']);
                }
                $this->request->data['User']['next_package_id'] = $this->data['User']['package_id'];
                $this->request->data['User']['package_id'] = $this->data['User']['package_id'];
                $this->request->data['User']['package_apply_date'] = date('Y-m-d');
                if ($this->User->save($this->data['User'], false)) {

                    $this->uses[] = 'Usercredit';
                    //$priceHas = $this->Usercredit->find('count', array('conditions' => "Usercredit.user_id = $uId"));

                    $this->uses[] = 'Serviceprice';
                    $packageId = $this->data['User']['package_id'];
                    $servicePricesByPackage = $this->Serviceprice->find('all', array('conditions' => "package_id = $packageId"));

                    $userCredit = array();
                    foreach ($servicePricesByPackage as $k => $sPBP) {
                        $userCredit['user_id'] = intval($uId);
                        $userCredit['service_id'] = intval($sPBP['Serviceprice']['service_id']);
                        $this->Usercredit->set($userCredit);
                        $this->Usercredit->save($userCredit);
                    }

                    $eMsg = 'Client information has been updated successfully';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    // set msg flash
                    $eMsg = 'Could not update Client information';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action'] . '/' . $uId);
            } else {
                $this->generateError('User');
            }
        }
        // Find all the packages
        $packages = array('Paid' => array(), 'Free' => array());
        $package = $this->Package->find('all');
        if (count($package) > 0) {
            foreach ($package as $p) {
                if ($p['Package']['type'] == 1) {
                    $packages['Paid'][$p['Package']['id']] = $p['Package']['packagename'];
                } else {
                    $packages['Free'][$p['Package']['id']] = $p['Package']['packagename'];
                }
            }
        }

        $this->set('packages', $packages);

        $packageList = $this->Package->find('list', array('fields' => 'id,packagename'));

        $this->set('packageList', $packageList);

        $data = $this->User->findById($uId);
        $this->data = $data;

        $this->set('data', $data);
        $this->set('cId', $uId);
        $this->set('title_for_layout', 'Edit Client');
    }

    function deleteclient($id) {
        if (empty($id))
            return '';
        $this->autoRender = false;
        $this->layout = 'ajax';

        $this->User->id = $id;
        $oData = $this->User->read();
        if ($this->User->delete($id, false)) {
            $this->Mysite->deleteall(array('Mysite.user_id' => $id), false);
            // delete all the anchor recored under this campaign
            $eMsg = 'Client with all records has been deleted successfully!';
            $this->Session->setFlash($eMsg, 'flash_success');
            echo '';
        } else {
            echo 'error';
        }
        exit();
    }

    function newdomain() {
        if ($this->request->is('post')) {
            $this->Domain->set($this->data['Domain']);
            if ($this->Domain->validates()) {
                $loginOk = 'success';
                $user_id = -1;
                if ($this->data['Domain']['networktype'] == 1) {
                    $user_id = $this->data['Domain']['user_id'];
                }
                $this->request->data['domain'] = $this->getPlainDomain($this->data['Domain']['domain']);
                $this->request->data['user_id'] = $user_id;
                $this->request->data['adddate'] = date('Y-m-d G:i:s', time());
                $this->request->data['expires_on'] = date('Y-m-d', strtotime($this->data['Domain']['expires_on']));

                if ($this->Domain->save($this->data['Domain'], false)) {
                    // set msg flash
                    $eMsg = 'New Domain has been added successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'New Domain could not be created.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action']);
            } else {
                $this->generateError('Domain');
            }
        }
        $this->User->bindModel(array(
            'hasMany' => array(
                'Domain' => array('foreignKey' => 'user_id', 'conditions' => "Domain.owner_id = -1"),
            )
        ));

        $this->uses[] = 'Serviceprice';
        $this->User->bindModel(array(
            'belongsTo' => array(
                'Serviceprice' => array('foreignKey' => false, 'conditions' => 'Serviceprice.package_id = User.package_id AND Serviceprice.service_id = 1'),
            )
        ));

        $clientList = $this->User->find('all', array('conditions' => "User.role = 'client'", 'order' => 'User.firstname ASC'));

        $users = array();

        if ($clientList) {
            $users[-1] = 'SEONitro';
            foreach ($clientList as $key => $client) {
                $serviceLimit = $client['Serviceprice']['servicelimit'];
                if ($serviceLimit > 0) {
                    $siteAssigned = count($client['Domain']);
                    $siteDue = $serviceLimit - $siteAssigned;
                    $siteDue = $siteDue >= 0 ? $siteDue : 0;
                    $uId = $client['User']['id'];
                    $users[$uId] = $client['User']['fullname'] . " (Due MySite: $siteDue out of $serviceLimit)";
                }
            }
        }
        $this->set(compact('users'));

        $this->set('title_for_layout', 'Add New Domain');
    }

    /* ---------------------------------------------
     * 
     *  Edit Domain 
     */

    function editdomain($cId = null) {

        // if action is present in referer url then it seems same action send the request to keep the previous one intact
        //echo strpos($this->request->referer(), $this->params['action']);
        if (strpos($this->request->referer(), $this->params['action']) === false) {
            $_SESSION['refrr'] = $this->request->referer();
        }
        // pr($_SESSION);

        $data = $this->Domain->findById($cId);

        if ($this->request->is('post')) {
            $this->Domain->set($this->data['Domain']);
            $this->request->data['Domain']['domain'] = $this->getPlainDomain($this->data['Domain']['domain']);
            $this->request->data['expires_on'] = date('Y-m-d', strtotime($this->data['Domain']['expires_on']));

            if ($this->Domain->validates()) {
                $loginOk = true;

                $user_id = -1;
                if ($this->data['Domain']['networktype'] == 1) {
                    $user_id = $this->data['Domain']['user_id'];
                }

                if ($data['Domain']['user_id'] != $user_id) {
                    $this->request->data['Domain']['cusername'] = '';
                    $this->request->data['Domain']['cpassword'] = '';
                }
                $this->request->data['Domain']['user_id'] = $user_id;
                $this->Domain->save($this->data['Domain'], false);
                $eMsg = 'Domain has been updated successfully.';
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->redirect(isset($_SESSION['refrr']) ? $_SESSION['refrr'] : $this->params['action'] );
            } else {
                $this->generateError('Domain');
            }
        }

        $this->data = $data;

        $this->User->bindModel(array(
            'hasMany' => array(
                'Domain' => array('foreignKey' => 'user_id', 'conditions' => "Domain.owner_id = -1"),
            )
        ));

        $this->uses[] = 'Serviceprice';
        $this->User->bindModel(array(
            'belongsTo' => array(
                'Serviceprice' => array('foreignKey' => false, 'conditions' => 'Serviceprice.package_id = User.package_id AND Serviceprice.service_id = 1'),
            )
        ));

        $clientList = $this->User->find('all', array('conditions' => "User.role = 'client'", 'order' => 'User.firstname ASC'));

        $users = array();

        if ($clientList) {
            $users[-1] = 'SEONitro';
            foreach ($clientList as $key => $client) {
                $serviceLimit = $client['Serviceprice']['servicelimit'];
                if ($serviceLimit > 0) {
                    $siteAssigned = count($client['Domain']);
                    $siteDue = $serviceLimit - $siteAssigned;
                    $siteDue = $siteDue >= 0 ? $siteDue : 0;
                    $uId = $client['User']['id'];
                    $users[$uId] = $client['User']['fullname'] . " (Due MySite: $siteDue out of $serviceLimit)";
                }
            }
        }
        $this->set(compact('users'));
        $this->set('data', $data);
        $this->set('cId', $cId);
        $this->set('title_for_layout', 'Edit Site');
    }

    function deletedomain($id) {
        if (empty($id))
            return '';
        $this->autoRender = false;
        $this->layout = 'ajax';

        $this->Domain->id = $id;
        $oData = $this->Domain->read();
        if ($this->Domain->delete($id, false)) {
            // delete all the anchor recored under this campaign
            $eMsg = 'Domain with all records has been deleted successfully!';
            $this->Session->setFlash($eMsg, 'flash_success');
            echo '';
        } else {
            echo 'error';
        }
        exit();
    }

    // put a domain under maintanance mode or live mode
    function statuschange($id = null, $status = 'ok') {
        if (empty($id))
            return '';
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->Domain->id = $id;
        $dt = array(
            'livestatus' => 'problematic',
            'status' => 0
        );
        if ($status == 'ok') {
            $dt = array(
                'livestatus' => 'ok',
                'status' => 1, //  no post or no action will be working if stauts is 0
                'httpcode' => '200',
                'httpstatus' => 'OK'
            );
        }
        if ($this->Domain->save($dt, false)) {
            $eMsg = "Domain has been marked as " . strtoupper($status);
            $this->Session->setFlash($eMsg, 'flash_success');
            echo '';
        } else {
            echo 'error';
        }
        exit();
    }

    // 1 = mysites, 2 = manula sites, 3 = premium sites, 4 = prime sites, 5 = lowgrade sites, 6 = guest blog
    function managemanualsites() {
        $params = array(
            'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 2'),
            'order' => ' Domain.domain ASC ',
            'recursive' => 1
        );
        $allDomains = $this->Domain->find('all', $params);
        $this->set(compact('allDomains'));
        $this->set('title_for_layout', 'Manage Manual Sites');
    }

    function manageautosites() {
        $params = array(
            'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 3'),
            'order' => ' Domain.domain ASC ',
            'recursive' => 1
        );
        $allDomains = $this->Domain->find('all', $params);
        $this->set(compact('allDomains'));
        $this->set('title_for_layout', 'Manage Automation Sites');
    }

    // automated premium sites, id = 3 (pr 3)
    function managepremium() {
        $params = array(
            'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 3'),
            'order' => ' Domain.domain ASC ',
            'recursive' => 1
        );
        $allDomains = $this->Domain->find('all', $params);
        $this->set(compact('allDomains'));
        $this->set('title_for_layout', 'Manage Premium Automation Sites');
    }

    // automated premium sites, id = 4 (pr 2/1)
    function manageprime() {
        $params = array(
            'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 4'),
            'order' => ' Domain.domain ASC ',
            'recursive' => 1
        );
        $allDomains = $this->Domain->find('all', $params);
        $this->set(compact('allDomains'));
        $this->set('title_for_layout', 'Manage Prime Automation Sites');
    }

    // automated premium sites, id = 5 (pr 0)
    function managelowgrade() {
        $params = array(
            'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 5'),
            'order' => ' Domain.domain ASC ',
            'recursive' => 1
        );
        $allDomains = $this->Domain->find('all', $params);
        $this->set(compact('allDomains'));
        $this->set('title_for_layout', 'Manage LowGrade Automation Sites');
    }

    // Guest Blog sites (pr >= 4)
    function manageguestsites() {
        $params = array(
            'conditions' => array(' Domain.owner_id = -1 AND Domain.networktype = 6'),
            'order' => ' Domain.domain ASC ',
            'recursive' => 1
        );
        $allDomains = $this->Domain->find('all', $params);
        $this->set(compact('allDomains'));

        $this->set('title_for_layout', 'Manage Guest Blogging Sites');
    }

    // All types of domains
    function managedomains() {
        /*
          $this->loadModel('Commonmodel');
          $this->Commonmodel->useTable = 'dmclass';
          $params = array(
          'conditions' => null, // array(' Domain.owner_id = -1'),
          'order' => ' Domain.domain ASC ',
          'recursive' => 1,
          //'limit' => 50,
          //'fields' => 'Domain.'
          );
          $this->Domain->bindModel(array(
          'belongsTo' => array(
          'User' => array('foreignKey' => 'user_id', 'conditions' => array("User.role = 'client'"), 'fields' => 'User.firstname,User.lastname,User.id'),
          'Commonmodel' => array('foreignKey' => 'networktype', 'fields' => 'Commonmodel.name,Commonmodel.id'),
          )
          ));
          $allDomains = $this->Domain->find('all', $params);
          //pr($allDomains); exit;
          $this->set(compact(array('allDomains')));
         */
        $this->set('title_for_layout', 'Manage All Domains');
    }

    // database ajax
    function getdomainsajax() {
        $this->autoRender = false;
        $this->layout = '';
        Configure::write('debug', 2);
        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        $aColumns = array('`Domain`.`domain`', '`User`.`firstname`', '`Domainclass`.`name`', '`Domain`.`pr`', '`Domain`.`cflow`', '`Domain`.`tflow`', '`Domain`.`ljpr`', '`Domain`.`seomoz_rank`', '`Domain`.`ext_backlinks`', '`Domain`.`expires_on`', '`Domain`.`cms`');

        /*
         * Paging
         */
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . intval($_GET['iDisplayStart']) . ", " .
                    intval($_GET['iDisplayLength']);
        }
        /*
         * Ordering
         */
        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] .
                            ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        } else {
            $sOrder = " ORDER BY  `Domain`.`domain` ASC";
        }
        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "";
        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = "WHERE (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        /* Individual column filtering */
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch_' . $i] . "%' ";
            }
        }


        $cols = "`Domain`.`id`, `Domain`.`domain`, `Domain`.`expires_on`, `Domain`.`created`, `Domain`.`pr`, `Domain`.`cflow`, `Domain`.`tflow`, `Domain`.`ljpr`,  `Domain`.`seomoz_rank`, `Domain`.`ext_backlinks`, `Domain`.`cms`, `Domain`.`ausername`, `Domain`.`apassword`, `Domain`.`cusername`, `Domain`.`status`, `User`.`firstname`, `User`.`lastname`, `User`.`id`, `Domainclass`.`name`,`Domainclass`.`id`";
        $q = "SELECT SQL_CALC_FOUND_ROWS $cols FROM `domains` AS `Domain` LEFT JOIN `users` AS `User` ON (`Domain`.`user_id` = `User`.`id`) LEFT JOIN `dmclass` AS `Domainclass` ON (`Domain`.`networktype` = `Domainclass`.`id`) $sWhere $sOrder $sLimit"; // $sWhere $sOrder $sLimit  WHERE 1 = 1 ORDER BY `Domain`.`domain` ASC LIMIT 100



        $res = $this->Domain->query($q);
        /* Data set length after filtering */
        $resC = $this->Domain->query("SELECT FOUND_ROWS()");
        $iFilteredTotal = $resC[0][0]['FOUND_ROWS()'];
        /* Total data set length */
        $iTotal = $this->Domain->find('count', array('conditions' => null));

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        $color = array(
            '1' => '#ED7A53',
            '2' => '#0e830e',
            '3' => '#1baa47',
            '4' => '#b40b8a',
            '5' => '#a06508',
            '6' => '#09aea0',
            '7' => '#AAAAAA',
        );
        foreach ($res as $r) {
            $clor = $r['Domainclass']['id']; //$color[$r['Domainclass']['id']];
            $row = array();
            $d = 'http://' . str_replace('http://', '', $r['Domain']['domain']);
            $row[] = '<a href="' . $d . '"  target="_blank">' . strtolower($r['Domain']['domain']) . '</a>';

            $row[] = ( $r['User']['firstname'] != '' ? '<span style="color:#b40b8a;">' . $r['User']['firstname'] . ' ' . $r['User']['lastname'] . '</span>' : 'SeoNitro');
            $row[] = '<span style="color:' . $clor . ';">' . $r['Domainclass']['name'] . '</span>';
            $row[] = $r['Domain']['pr'];
            $row[] = $r['Domain']['cflow'];
            $row[] = $r['Domain']['tflow'];
            $row[] = $r['Domain']['ljpr'];
            $row[] = $r['Domain']['seomoz_rank'];
            $row[] = $r['Domain']['ext_backlinks'];
            $row[] = $r['Domain']['cms'] . '<br>' . $r['Domain']['ausername'] . '<br>' . $r['Domain']['apassword'];

            $row[] = '<div class="controls" style="text-align:left !important;">
                <a href="' . Router::url('detailsdomain/' . $r['Domain']['id']) . '" title="See Details about this Domain" class="tip">&rArr;details</a> <br>
                <!--a href="' . Router::url('editdomain/' . $r['Domain']['id']) . '" title="Edit This Domain?" class="tip">
                &rArr;edit</a> <br>
                <a href="' . Router::url('deletedomain/' . $r['Domain']['id']) . ' " title="Remove This Domain?" class="tip callAction">&rArr;delete</a>
                <br-->
                <a target="_blank" href="http://core.seoplushosting.com:2080/nodeworx/siteworx?action=siteworxLoginCommit&login_domain=' . strtolower($r['Domain']['domain']) . '" title="Go SiteWorx Account" class="tip">&rArr; Go SiteWorx</a> 
                </div>';
            $output['aaData'][] = $row;
        }

        //pr($output);
        echo json_encode($output);
        exit;
    }

    // end function
    // Move domain from one network to another
    function movedomains() {

        $nArray = array('1' => 'MySites', '2' => 'Manual Networks', '3' => 'Premium', '4' => 'Prime', '5' => 'Low Grade', '6' => 'Guest Blog');

        if ($this->request->is('post')) {
            $networkType = intval($this->data['Moveto']['networktype']);
            if ($networkType) {
                if (count($this->data['Moveto']['ids'])) {
                    $ids = implode(",", array_keys($this->data['Moveto']['ids']));
                    $q = 'Domain.id IN (' . $ids . ')';
                    //echo $networkType.' '. $q;
                    if ($this->Domain->updateAll(
                                    array('Domain.networktype' => $networkType, 'Domain.user_id' => '-1'), array($q)
                            )) {
                        $eMsg = 'Selected Domain has been Moved to ' . $nArray[$networkType];
                        $this->Session->setFlash($eMsg, 'flash_success');
                    }
                } else {
                    $eMsg = ' You did not select domains to move. ';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
            } else {
                $eMsg = ' You did not select a network where domains to move';
                $this->Session->setFlash($eMsg, 'flash_error');
            }
        }

        $this->loadModel('Commonmodel');
        $this->Commonmodel->useTable = 'dmclass';
        $params = array(
            'conditions' => array(" Domain.owner_id = -1 "),
            'order' => ' Domain.domain ASC ',
            'recursive' => 1,
        );
        $this->Domain->bindModel(array(
            'belongsTo' => array(
                'Commonmodel' => array('foreignKey' => 'networktype', 'fields' => 'Commonmodel.name,Commonmodel.id'),
            )
        ));
        $allDomains = $this->Domain->find('all', $params);
        //pr($allDomains);            
        $this->set(compact(array('allDomains')));
        $this->set('title_for_layout', 'Moving Domain inter network');
    }

    // MySites
    function managemysites() {
        $params = array(
            'conditions' => array(' Domain.networktype = 1 AND owner_id = -1'),
            //'limit' => 20,
            'order' => ' Domain.id ASC ',
            'recursive' => 1
        );

        $this->Domain->bindModel(array(
            'hasOne' => array(
                'User' => array('foreignKey' => false, 'conditions' => array('Domain.user_id = User.id')),
            )
        ));
        $allDomains = $this->Domain->find('all', $params);
        $this->set(compact('allDomains'));

        $this->set('title_for_layout', 'Manage My Sites');
    }

    // MySites
    function manageclprvsites() {
        $params = array(
            'conditions' => array(' Domain.networktype = 1 AND owner_id > 1'),
            //'limit' => 20,
            'order' => ' Domain.id ASC ',
            'recursive' => 1
        );

        $this->Domain->bindModel(array(
            'hasOne' => array(
                'User' => array('foreignKey' => false, 'conditions' => array('Domain.user_id = User.id')),
            )
        ));
        $allDomains = $this->Domain->find('all', $params);
        $this->set(compact('allDomains'));

        $this->set('title_for_layout', 'Manage Client Private Sites');
    }

    function assignedmysites() {

        $allDomains = $this->paginate('Domain');

        $this->set(compact('allDomains'));

        $this->Domain->bindModel(array(
            'hasOne' => array(
                'User' => array('foreignKey' => false, 'conditions' => "Domain.user_id = User.id", 'fields' => 'id,fullname'),
            )
        ));


        $clientList = $this->Domain->find('all', array('conditions' => "Domain.owner_id = -1 AND Domain.user_id != -1 AND Domain.networktype = 1"));

        //pr($clientList);
        //exit;

        $this->set(compact('clientList'));
        $this->set('title_for_layout', 'Manage Assigned Mysites');
    }

    function assignedclients() {

        $allDomains = $this->paginate('Domain');

        $this->set(compact('allDomains'));

        $this->User->bindModel(array(
            'hasMany' => array(
                'Domain' => array('foreignKey' => 'user_id', 'conditions' => "Domain.owner_id = -1"),
            )
        ));

        $this->uses[] = 'Serviceprice';
        $this->User->bindModel(array(
            'belongsTo' => array(
                'Serviceprice' => array('foreignKey' => false, 'conditions' => 'Serviceprice.package_id = User.package_id AND Serviceprice.service_id = 1'),
            )
        ));

        $clientList = $this->User->find('all', array('conditions' => "User.role = 'client'"));

        $this->set(compact('clientList'));
        $this->set('title_for_layout', 'Manage Assigned Mysites');
    }

    function clientmysites($userId = null) {

        if ($this->request->is('post')) {
            $checkedDomains = $this->data['Checked'];
            $updateArr = array();
            if ($checkedDomains) {
                $count = 0;
                foreach ($checkedDomains as $key => $val) {
                    $updateArr[$count]['id'] = $key;
                    $updateArr[$count]['user_id'] = -1;
                    $count++;
                }
                $fieldsArr = array(
                    'fieldList' => array(
                        'id', 'user_id'
                ));
                if ($this->Domain->saveAll($updateArr, $fieldsArr)) {
                    $eMsg = 'Domains unassigned successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'Domains could not be unassigned.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
            } else {
                $eMsg = 'Please check at least one Domain to unassign.';
                $this->Session->setFlash($eMsg, 'flash_error');
            }
            $this->redirect($this->params['action'] . "/$userId");
        }

        $allDomains = $this->Domain->find('all', array(
            'conditions' => array("Domain.owner_id = -1 AND Domain.user_id = $userId AND Domain.networktype = 1"),
            'order' => ' Domain.id ASC ',
            'recursive' => 1
        ));


        $this->set(compact('allDomains'));

        $userInfo = $this->User->findById($userId);

        $this->set(compact('userInfo'));


        $this->set('title_for_layout', 'Manage Assigned Mysites');
    }

    function freemysites() {

        if ($this->request->is('post')) {
            $checkedDomains = @$this->data['Checked']; // could be all unselected 
            $userId = $this->data['Domain']['user_id'];
            $updateArr = array();
            if ($checkedDomains && $userId) {
                $count = 0;
                foreach ($checkedDomains as $key => $val) {
                    $updateArr[$count]['id'] = $key;
                    $updateArr[$count]['user_id'] = $userId;
                    $count++;
                }

                $this->User->id = $userId;
                $packageId = $this->User->field('package_id');

                $alreadyAssigned = $this->Domain->find('count', array('conditions' => "Domain.owner_id = -1 AND Domain.user_id = $userId"));

                $this->uses[] = 'Serviceprice';
                $packageInfo = $this->Serviceprice->find('first', array('conditions' => "Serviceprice.package_id = $packageId AND Serviceprice.service_id = 12"));
                $packageLimit = $packageInfo['Serviceprice']['servicelimit'];

                $newAssigned = $alreadyAssigned + $count;

                if ($newAssigned > $packageLimit) {
                    $eMsg = 'You cannot assign more than the package limit for this user.';
                    //$this->Session->setFlash($eMsg, 'flash_error');
                    //$this->redirect($this->params['action']);
                }

                $fieldsArr = array(
                    'fieldList' => array(
                        'id', 'user_id'
                ));
                //pr($updateArr);
                //exit;
                //$this->Domain->updateAll(array('Domain.user_id' => $userId), array('Domain.id' => $user_id, 'Usercredit.service_id' => $service_id, 'Usercredit.status' => 1), false
                if ($this->Domain->saveAll($updateArr, $fieldsArr)) {
                    $eMsg = 'Domains assigned successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'Domains could not be assigned or already assigned.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
            } else {
                $eMsg = 'Please check at least one Domain to assign.';
                $this->Session->setFlash($eMsg, 'flash_error');
            }
            $this->redirect($this->params['action']);
        }

        $allDomains = $this->Domain->find('all', array(
            'conditions' => array('Domain.owner_id = -1 AND Domain.user_id = -1 AND Domain.networktype = 1'),
            'order' => ' Domain.domain ASC ',
            'recursive' => 1
        ));

        $this->set(compact('allDomains'));

        $this->User->bindModel(array(
            'hasMany' => array(
                'Domain' => array('foreignKey' => 'user_id', 'conditions' => "Domain.owner_id = -1"),
            )
        ));

        $this->uses[] = 'Serviceprice';
        $this->User->bindModel(array(
            'belongsTo' => array(
                'Serviceprice' => array('foreignKey' => false, 'conditions' => 'Serviceprice.package_id = User.package_id AND Serviceprice.service_id = 1'),
            )
        ));

        $clientList = $this->User->find('all', array('conditions' => "User.role = 'client'", 'order' => 'User.firstname'));

        $this->loadModel('Usercredit');
        $users = array();

        if ($clientList) {
            foreach ($clientList as $key => $client) {

                $uId = $client['User']['id'];
                $serviceId = $client['Serviceprice']['service_id'];
                $serviceLimit = $client['Serviceprice']['servicelimit'];
                $extraUnit = 0;

                $userCreditInfo = $this->Usercredit->find('first', array('conditions' => "Usercredit.user_id = $uId AND Usercredit.service_id = $serviceId"));
                if ($userCreditInfo) {
                    $extraUnit = $userCreditInfo['Usercredit']['extra_unit'];
                }

                $serviceLimit = $serviceLimit + $extraUnit;

                if ($serviceLimit > 0) {
                    $siteAssigned = count($client['Domain']);
                    $siteDue = $serviceLimit - $siteAssigned;
                    $siteDue = $siteDue >= 0 ? $siteDue : 0;
                    $users[$uId] = $client['User']['fullname'] . " (Due MySite: $siteDue out of $serviceLimit)";
                }
            }
        }

        //pr($users);

        $this->set(compact('users'));

        $this->set('title_for_layout', 'Manage Unassigned Mysites');
    }

    function unassignmysite($id) {
        if (empty($id))
            return '';
        $this->autoRender = false;
        $this->layout = 'ajax';

        $updateDomain['Domain']['id'] = $id;
        $updateDomain['Domain']['user_id'] = -1;
        if ($this->Domain->save($updateDomain, false)) {
            $eMsg = 'Domain has been unassigned successfully!';
            $this->Session->setFlash($eMsg, 'flash_success');
            echo '';
        } else {
            echo 'error';
        }
        exit();
    }

    function viewcredits() {

        $this->paginate = array('conditions' => array("User.role = 'client' "), 'limit' => 500);

        $userCredits = $this->paginate('User');
        $this->set('userCredits', $userCredits);
        $this->set('title_for_layout', 'Manage Credits');
    }

    public function creditstogroup() {
        if ($this->request->is('post')) {
            $this->Credit->set($this->data['Credit']);
            if ($this->Credit->validates()) {

                if ($this->Credit->updateAll(array('Credit.extra_credit' => "Credit.extra_credit + {$this->data['Credit']['assign_credit']}"), array('package_id' => $this->data['Credit']['package_id']), false)) {

                    $gUsersArr = $this->User->find('list', array(
                        'fields' => array('User.id'),
                        'conditions' => array('User.package_id' => $this->data['Credit']['package_id'])
                    ));
                    if (count($gUsersArr)) {
                        foreach ($gUsersArr as $k => $userId) {
                            $updateAccount['user_id'] = $userId;
                            $updateAccount['type'] = 'credit';
                            $updateAccount['service_id'] = -1;
                            $updateAccount['amount'] = $this->data['Credit']['assign_credit'];
                            $updateAccount['remarks'] = 'Credits assigned by the admin.';

                            $this->updateAccountDebit($updateAccount);
                        }
                    }
                    $eMsg = 'Credit assigned successfully';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    // set msg flash
                    $eMsg = 'Could not assign credit.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action']);
            } else {
                $this->generateError('Credit');
            }
        }
        $packages = $this->Package->find('list', array(
            'fields' => array('Package.id', 'Package.packagename'),
            'conditions' => array('Package.status' => 1)
        ));
        $this->set('packages', $packages);
        $this->set('title_for_layout', 'Assign Credits to Package Group');
    }

    public function creditstouser() {
        if ($this->request->is('post')) {
            $this->Credit->set($this->data['Credit']);
            if ($this->Credit->validates()) {
                if ($this->Credit->updateAll(array('Credit.extra_credit' => "Credit.extra_credit + {$this->data['Credit']['assign_credit']}"), array('id' => $this->data['Credit']['id'],), false)) {
                    $updateAccount['user_id'] = $this->data['Credit']['id'];
                    $updateAccount['service_id'] = -1;
                    $updateAccount['type'] = 'credit';
                    $updateAccount['amount'] = $this->data['Credit']['assign_credit'];
                    $updateAccount['remarks'] = 'Credits assigned by the admin.';
                    $this->updateAccountDebit($updateAccount);
                    $eMsg = 'Credit assigned successfully';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    // set msg flash
                    $eMsg = 'Could not assign credit.';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action']);
            } else {
                $this->generateError('Credit');
            }
        }
        $users = $this->User->find('list', array(
            'fields' => array('User.id', 'User.fullname'),
            'conditions' => array('User.status' => 1, 'role' => 'client')
        ));
        $this->set('users', $users);
        $this->set('title_for_layout', 'Assign Credits to Individual User');
    }

    public function credithistory() {
        $this->uses[] = 'Credithistory';
        $this->paginate = array('conditions' => "Credithistory.service_id = -1 AND Credithistory.type = 'credit'");
        $creditHistory = $this->paginate('Credithistory');
        $this->set(compact('creditHistory'));
        $this->set('title_for_layout', 'View Credit History');
    }

    public function debithistory() {
        $this->uses[] = 'Credithistory';
        $this->paginate = array('conditions' => "Credithistory.service_id != -1 AND Credithistory.type = 'debit'");
        $debitHistory = $this->paginate('Credithistory');
        $this->set(compact('debitHistory'));
        $this->set('title_for_layout', 'View Debit History');
    }

    function addinstruction() {
        $extRes = array('mp4', 'wmv', 'flv');
        if ($this->request->is('post')) {
            $this->Instruction->set($this->data['Instruction']);
            if ($this->Instruction->validates()) {
                $fileName = '';
                if ($this->data['Instruction']['video']['name'] != '') {
                    if ($this->data['Instruction']['video']['error'] == 0) {
                        if (in_array($this->fileExt($this->data['Instruction']['video']['name']), $extRes)) {
                            $folder = WWW_ROOT . 'video';
                            $fileName = time() . '_' . stripslashes(preg_replace("~\s+~", "_", str_replace("'", "", $this->data['Instruction']['video']['name'])));
                            $ufile = $folder . DS . $fileName;
                            @move_uploaded_file($this->data['Instruction']['video']['tmp_name'], $ufile);
                        } // end if in_array
                        else {
                            $eMsg = 'File Not Supported';
                            $this->Session->setFlash($eMsg, 'flash_error');
                            $this->redirect($this->params['action']);
                        }
                    }
                } // end picure upload
                $this->request->data['Instruction']['video'] = $fileName;
                $this->Instruction->save($this->data['Instruction']);
                $eMsg = 'New Instruction has been added successfully.';
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->redirect($this->params['action']);
            } else {
                $this->generateError('Instruction');
            }
        }

        $this->set('title_for_layout', 'Add Instruction');
    }

    function manageinstruction() {
        $allInstruction = $this->Instruction->find('all', array('order' => 'pagename ASC'));
        $this->set(compact('allInstruction'));
        $this->set('title_for_layout', 'Manage Instruction');
    }

    function editinstruction($pId = null) {
        $extRes = array('mp4', 'wmv', 'flv');
        if ($this->request->is('post')) {
            $this->Instruction->set($this->data['Instruction']);
            if ($this->Instruction->validates()) {
                if ($this->data['Instruction']) {
                    $ins = $this->Instruction->findById($pId);
                    if ($this->data['Instruction']['video']['name'] != '') {
                        if ($this->data['Instruction']['video']['error'] == 0) {
                            if (in_array($this->fileExt($this->data['Instruction']['video']['name']), $extRes)) {
                                $folder = WWW_ROOT . 'video';
                                $fileName = time() . '_' . stripslashes(preg_replace("~\s+~", "_", str_replace("'", "", $this->data['Instruction']['video']['name'])));
                                $ufile = $folder . '/' . $fileName;
                                @move_uploaded_file($this->data['Instruction']['video']['tmp_name'], $ufile);
                                $unfile = $folder . '/' . $ins['Instruction']['video'];
                                @unlink($unfile);
                                $this->request->data['Instruction']['video'] = $fileName;
                            } // end if in_array
                            else {
                                $eMsg = 'File Not Supported';
                                $this->Session->setFlash($eMsg, 'flash_error');
                                $this->redirect($this->params['action']);
                            }
                        }
                    } // end picure upload
                    else {
                        $this->request->data['Instruction']['video'] = $ins['Instruction']['video'];
                    }

                    // delete old video file 
                    if (isset($this->data['Instruction']['delOld'])) {
                        $unfile = WWW_ROOT . 'video/' . $ins['Instruction']['video'];
                        @unlink($unfile);
                        $this->request->data['Instruction']['video'] = '';
                    }


                    $this->Instruction->save($this->data['Instruction']);
                    $eMsg = 'Tutorial has been updated successfully';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'Could not Update Tutorial';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action'] . '/' . $pId);
            } else {
                $this->generateError('Instruction');
            }
        }
        $data = $this->Instruction->findById($pId);
        $this->data = $data;
        $this->set('data', $data);
        $this->set('cId', $pId);
        $this->set('title_for_layout', 'Edit Instruction');
    }

    function deleteinstruction($id = null) {
        if (empty($id))
            return '';
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->Instruction->id = $id;
        $oData = $this->Instruction->read();
        $ins = $this->Instruction->findById($id);
        $folder = WWW_ROOT . 'video';
        $ufile = $folder . DS . $ins['Instruction']['video'];
        @unlink($ufile);
        if ($this->Instruction->delete($id, false)) {
            $eMsg = 'Instruction with all records has been deleted successfully!';
            $this->Session->setFlash($eMsg, 'flash_success');
            echo '';
        } else {
            echo 'error';
        }
        exit();
    }

    /*
     *  Add, Delete Media
     *  
     */

    function addmedia() {
        $this->uses[] = 'Media';
        $extRes = array('mp4', 'wmv', 'flv', 'jpg', 'jpeg', 'gif', 'png', 'pdf', 'doc', 'txt', 'docx', 'xls', 'xlsx', 'tiff');
        $filetype = 'link'; // could be image, video, external link
        if ($this->request->is('post')) {

            //pr($this->data);
            //$eVf = ( $this->data['Media']['type'] == 'link' ) ? 'src' : 'file';
            // add validation if external souce selected
            if ($this->data['Media']['type'] == 'link') {
                $this->Media->validate = Set::merge($this->Media->validate, array(
                            'src' => array(
                                'notempty' => array(
                                    'rule' => 'notEmpty',
                                    'message' => 'Please enter external source',
                                    'allowEmpty' => false,
                                    'required' => true,
                                )
                            )
                ));
            }
            //pr($this->data);
            $this->Media->set($this->data['Media']);
            if ($this->Media->validates()) {

                if ($this->data['Media']['type'] == 'link') {
                    $this->request->data['Media']['source'] = $this->data['Media']['src'];
                    $this->Media->save($this->data['Media']);
                    $eMsg = 'New external media link has been inserted successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                    $this->redirect($this->params['action']);
                } else {
                    $fileName = '';
                    if ($this->data['Media']['file']['name'] != '') {
                        if ($this->data['Media']['file']['error'] == 0) {
                            if (in_array($this->fileExt($this->data['Media']['file']['name']), $extRes)) {
                                $folder = WWW_ROOT . 'media';
                                $fileName = time() . '_' . stripslashes(preg_replace("~\s+~", "_", str_replace("'", "", $this->data['Media']['file']['name'])));
                                $ufile = $folder . DS . $fileName;
                                // resize image
                                if (in_array($this->fileExt($this->data['Media']['file']['name']), array('jpg', 'jpeg', 'gif', 'png'))) {
                                    $w = intval(trim($this->data['Media']['w']));
                                    $h = intval(trim($this->data['Media']['h']));
                                    if ($w > 0 || $h > 0) {
                                        $this->ImageResizer->resizeImage($this->data['Media']['file']['tmp_name'], array(
                                            'output' => $ufile,
                                            'ignoreSmallImages' => true,
                                            'maxHeight' => $w,
                                            'maxWidth' => $h
                                        ));
                                    } else {
                                        @move_uploaded_file($this->data['Media']['file']['tmp_name'], $ufile);
                                    }
                                } else {
                                    @move_uploaded_file($this->data['Media']['file']['tmp_name'], $ufile);
                                }
                                $this->request->data['Media']['source'] = $fileName;
                                $this->Media->save($this->data['Media']);
                                $eMsg = 'New Media has been uploaded successfully.';
                                $this->Session->setFlash($eMsg, 'flash_success');
                                $this->redirect($this->params['action']);
                            } // end if in_array
                            else {
                                $eMsg = 'File Not Supported';
                                $this->Session->setFlash($eMsg, 'flash_error');
                            }
                        }
                    } // end picure upload
                }
            } else {
                $this->generateError('Media');
            }
        }

        $this->set('title_for_layout', 'Add Media');
    }

    function managemedia() {
        $this->uses[] = 'Media';
        $allMedia = $this->Media->find('all', array('order' => 'title ASC'));
        $this->set(compact('allMedia'));
        $this->set('title_for_layout', 'Manage Media');
    }

    function deletemedia($id = null) {
        $this->uses[] = 'Media';
        if (empty($id))
            return '';
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->Media->id = $id;
        $oData = $this->Media->read();
        $ins = $this->Media->findById($id);
        $folder = WWW_ROOT . 'media';
        $ufile = $folder . DS . $ins['Media']['source'];
        @unlink($ufile);
        if ($this->Media->delete($id, false)) {
            $eMsg = 'Media with all records has been deleted successfully!';
            $this->Session->setFlash($eMsg, 'flash_success');
            echo '';
        } else {
            echo 'error';
        }
        exit();
    }

    /*
     *  Education Management 
     * 
     */

    // add new category for education page with ajax call
    function addcategories() {
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->loadModel('Category');
        if ($this->request->is('post')) {
            if ($this->data['parent_id'] > 0) {
                $data['parent_id'] = $this->data['parent_id'];
            }
            //echo $data['parent_id'] = '';
            $data['name'] = $this->data['name'];
            if ($this->Category->save($data)) {
                echo true;
            } else {
                echo false;
            }
        }
        exit;
    }

    function addlesson() {

        $this->uses[] = 'Education';
        if ($this->request->is('post')) {
            $this->Education->set($this->data['Education']);
            if ($this->Education->validates()) {
                $pack = '';
                foreach ($this->data['Education']['package'] as $p => $v) {
                    if ($v)
                        $pack .= $p . ',';
                }
                if ($pack != '') {
                    $pack = ',' . $pack . ',';
                }
                $this->request->data['Education']['packages'] = str_replace(',,', ',', $pack);
                $this->Education->save($this->data['Education']);
                $eMsg = 'New topic has been added successfully.';
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->redirect($this->params['action']);
            } else {
                $this->generateError('Education');
            }
        }
        $this->set('title_for_layout', 'Add Education');

        // get all packages
        $allPackages = $this->Package->find('all');
        $this->set(compact('allPackages'));
        //get all categories
        $this->loadModel('Category');
        $tree = $this->Category->generateTreeList(null, null, null, '---');
        //pr($tree);
        $this->set('cateTree', $tree);
    }

    function managelesson() {
        $this->uses[] = 'Education';
        $this->loadModel('Category');
        $this->Education->bindModel(array(
            'belongsTo' => array(
                'Category' => array('foreignKey' => 'cat_id'),
            )
        ));
        $allLesson = $this->Education->find('all', array('order' => 'title ASC', 'recursive' => 1));
        //pr($allLesson);
        $this->set(compact('allLesson'));
        // get all packages
        $allPackages = $this->Package->find('list', array(
            'fields' => array('Package.id', 'Package.packagename'),
            'recursive' => 0
        ));
        //pr($allPackages);
        $this->set(compact('allPackages'));


        $this->set('title_for_layout', 'Manage Education');
    }

    function editlesson($id = null) {
        $this->uses[] = 'Education';
        $this->Education->id = $id;
        if ($this->request->is('post')) {
            $this->Education->set($this->data['Education']);
            if ($this->Education->validates()) {
                $this->Education->save($this->data['Education']);
                $eMsg = 'Education has been updated successfully.';
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->redirect($this->params['action'] . '/' . $id);
            } else {
                $this->generateError('Education');
            }
        }
        $data = $this->Education->findById($id);
        $this->data = $data;
        $this->set('data', $data);


        // get all packages
        $allPackages = $this->Package->find('all');
        $this->set(compact('allPackages'));
        //get all categories
        $this->loadModel('Category');
        $tree = $this->Category->generateTreeList(null, null, null, '---');
        //pr($tree);
        $this->set('cateTree', $tree);



        $this->set('title_for_layout', 'Edit Education');
    }

    function deletelesson($id) {
        $this->uses[] = 'Education';
        if (empty($id))
            return '';
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->Education->id = $id;
        if ($this->Education->delete($id, false)) {
            $eMsg = 'Education has been deleted successfully!';
            $this->Session->setFlash($eMsg, 'flash_success');
            echo '';
        } else {
            echo 'error';
        }
        exit();
    }

    // end Education /

    function importdomains() {
        App::import('Vendor', 'PHPExcel');
        if ($this->request->is('post')) {
            $notifications = array();
            if ($this->data['Domain']['file']['name'] != '') {
                $fname = $this->data['Domain']['file']['name'];

                $filename = $this->data['Domain']['file']['tmp_name'];

                $objPHPExcel = PHPExcel_IOFactory::load($filename);
                foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                    $worksheetTitle = $worksheet->getTitle();
                    $highestRow = $worksheet->getHighestRow(); // e.g. 10
                    $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                }

                /**
                  $nrColumns = ord($highestColumn) - 64;
                  echo "File " . $worksheetTitle . " has ";
                  echo $nrColumns . ' columns';
                  echo ' y ' . $highestRow . ' rows.';
                  /* */
                $firstRowArr = array();
                $valueArr = array();
                for ($row = 1; $row <= $highestRow; ++$row) {
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        if ($row == 1) {
                            $firstRowArr[] = trim($cell->getValue());
                        } else {
                            $dbCol = strtolower($firstRowArr[$col]);
                            if ($dbCol) {
                                if ($dbCol == 'livestatus' && trim($cell->getValue()) != '') {
                                    //continue 1;
                                }
                                $valueArr[$row]['networktype'] = $this->data['Domain']['networktype'];
                                if ($this->data['Domain']['networktype'] == 1) {
                                    $valueArr[$row]['user_id'] = $this->data['Domain']['user_id'];
                                }
                                $valueArr[$row]['status'] = 0;
                                $valueArr[$row][$dbCol] = trim($cell->getValue());
                                if ($dbCol == 'domain') {
                                    $valueArr[$row][$dbCol] = $this->getPlainDomain(trim($cell->getValue()));
                                }
                                if ($dbCol == 'cms') {
                                    $valueArr[$row][$dbCol] = strtolower(trim($cell->getValue()));
                                }
                                if (( $dbCol == 'registrar' || $dbCol == 'rootdomain_id' || $dbCol == 'ftpuser' || $dbCol == 'ftppassword' || $dbCol == 'dbname' || $dbCol == 'dbuser' || $dbCol == 'dbpassword') && $cell->getValue() == ''
                                ) {
                                    $valueArr[$row][$dbCol] = @$valueArr[$row - 1][$dbCol];
                                }
                            }
                        }
                    }
                }

                //pr($valueArr);
                //exit;

                if ($valueArr) {
                    $finalArr = array();
                    foreach ($valueArr as $key => $arr) {
                        if (!isset($arr['livestatus'])) {
                            $valueArr[$key]['livestatus'] = '';
                        }
                    }
                    //pr($valueArr);

                    foreach ($valueArr as $key => $arr) {
                        if (trim($arr['livestatus']) == '' || trim(strtolower($arr['livestatus'])) == 'fixed') {
                            $finalArr[] = $arr;
                        }
                    }

                    //pr($finalArr);
                    //exit;

                    foreach ($finalArr as $key => $vals) {
                        $finalValues['Domain'] = $vals;
                        $this->Domain->set($finalValues);
                        $domainName = $finalValues['Domain']['domain'];
                        if ($this->Domain->validates()) {
                            $this->Domain->create();
                            if ($this->Domain->save($finalValues, false)) {
                                $notifications[] = ($key + 1) . ' => ' . $domainName . ': Success!';
                            }
                        } else {
                            $errorField = $this->Domain->invalidFields();
                            $notifications[] = '<span style="color:red;">' . ($key + 1) . ' => ' . $domainName . ': ' . json_encode($errorField) . '</span>';
                        }
                    }
                }
            }
            //exit;
            if ($notifications) {
                $eMsg = implode(',<br/>', $notifications);
                $this->Session->setFlash($eMsg, 'flash_success');
            } else {
                $eMsg = 'Please upload a valid file.';
                $this->Session->setFlash($eMsg, 'flash_error');
            }
            $this->redirect($this->params['action']);
        }

        $this->User->bindModel(array(
            'hasMany' => array(
                'Domain' => array('foreignKey' => 'user_id', 'conditions' => "Domain.owner_id = -1"),
            )
        ));

        $this->uses[] = 'Serviceprice';
        $this->User->bindModel(array(
            'belongsTo' => array(
                'Serviceprice' => array('foreignKey' => false, 'conditions' => 'Serviceprice.package_id = User.package_id AND Serviceprice.service_id = 1'),
            )
        ));

        $clientList = $this->User->find('all', array('conditions' => "User.role = 'client'"));

        $users = array();

        if ($clientList) {
            $users[-1] = 'SEONitro';
            foreach ($clientList as $key => $client) {
                $serviceLimit = $client['Serviceprice']['servicelimit'];
                if ($serviceLimit > 0) {
                    $siteAssigned = count($client['Domain']);
                    $siteDue = $serviceLimit - $siteAssigned;
                    $siteDue = $siteDue >= 0 ? $siteDue : 0;
                    $uId = $client['User']['id'];
                    $users[$uId] = $client['User']['fullname'] . " (Due MySite: $siteDue out of $serviceLimit)";
                }
            }
        }
        $this->set(compact('users'));

        $this->set('title_for_layout', 'Import Domains');
    }

    function managesettings($cId = 1) {
        Configure::write('Cache.disable', true);
        if ($this->request->is('post')) {
            // pr($_POST); exit;
            $this->Setting->set($this->data['Setting']);
            if ($this->Setting->save($this->data['Setting'], false)) {

                $eMsg = 'Your Setting Has Been Updated Successfully';
                $this->Session->setFlash($eMsg, 'flash_success');
            } else {
                // set msg flash
                $eMsg = 'Could not Update Setting';
                $this->Session->setFlash($eMsg, 'flash_error');
            }
            $this->redirect($this->params['action'] . '/' . $cId);
        }
        $data = $this->Setting->findById($cId);
        $this->data = $data;

        $this->set('data', $data);
        $this->set('title_for_layout', 'Manage Settings');
    }

    function newblogger() {
        if ($this->request->is('post')) {
            $givenPass = $this->data['User']['password'];
            $this->request->data['User']['role'] = 'blogger';
            $this->request->data['User']['password'] = ( trim($this->data['User']['password']) != '' ) ? AuthComponent::password($this->data['User']['password']) : '';
            $this->request->data['User']['createdate'] = date('Y-m-d G:i:s', time());
            $this->request->data['User']['lastsignin'] = date('Y-m-d G:i:s', time());
            $this->request->data['User']['status'] = '1';
            $this->request->data['User']['varified'] = '1';
            $this->Register->set($this->data['User']);

            if ($this->Register->validates()) {
                $this->User->save($this->data['User'], false);
                // set msg flash
                $eMsg = 'New Blogger has been added successfully.';
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->redirect($this->params['action']);
            } else {
                $this->request->data['User']['password'] = $givenPass;
                $this->generateError('Register');
            }
        }

        $this->set('title_for_layout', 'Add New Blogger');
    }

    function managebloggers() {
        $allUser = $this->User->find('all', array(
            'conditions' => array("User.role = 'blogger' "),
            'recursive' => 1
        ));
        $this->set(compact('allUser'));

        $this->set('title_for_layout', 'Manage Bloggers');
    }

    function viewblogger($id) {
        $data = $this->User->find('first', array(
            'conditions' => array("User.id = " . $id . " AND User.role = 'blogger' "),
            'recursive' => 1
        ));
        $this->set(compact('data'));
        $this->set('title_for_layout', 'View Blogger');
    }

    function editblogger($cId = null) {
        if ($this->request->is('post')) {
            if ($this->User->validates()) {
                if (trim($this->data['UserEx']['password']) != '') {
                    $this->request->data['User']['password'] = AuthComponent::password($this->data['UserEx']['password']);
                }
                if ($this->User->save($this->data['User'], false)) {

                    $eMsg = 'Blogger Has Been Updated Successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    // set msg flash
                    $eMsg = 'Could not Update Blogger';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action'] . '/' . $cId);
            } else {
                $this->generateError('User');
            }
        }

        $data = $this->User->findById($cId);
        $this->data = $data;

        $this->set('data', $data);
        $this->set('cId', $cId);
        $this->set('title_for_layout', 'Edit Client');
    }

    function deleteblogger($id) {
        if (empty($id))
            return '';
        $this->autoRender = false;
        $this->layout = 'ajax';

        $this->User->id = $id;
        $oData = $this->User->read();
        if ($this->User->delete($id, false)) {
            $this->Mysite->deleteall(array('Mysite.user_id' => $id), false);
            // delete all the anchor recored under this campaign
            $eMsg = 'Blogger with all records has been deleted successfully!';
            $this->Session->setFlash($eMsg, 'flash_success');
            echo '';
        } else {
            echo 'error';
        }
        exit();
    }

    function bazookacategories() {
        $this->uses[] = 'Bazooka';
        if ($this->request->is('post')) {

            //pr($this->data['Bazooka']['status']); exit;

            $bCats = array();
            $count = 0;
            foreach ($this->data['Bazooka']['status'] as $k => $status) {
                $bCats[$count]['id'] = intval($k);
                $bCats[$count]['status'] = intval($status);
                $count++;
            }
            //pr($bCats); exit;
            if ($this->Bazooka->saveMany($bCats)) {
                $eMsg = 'Bazooka categories updated successfully!';
                $this->Session->setFlash($eMsg, 'flash_success');
            } else {
                $eMsg = 'Bazooka categories could not be updated!';
                $this->Session->setFlash($eMsg, 'flash_success');
            }
            $this->redirect($this->params['action']);
        }
        $bazookaCats = $this->Bazooka->find('all', array('order' => 'Bazooka.name ASC'));
        $data = array();
        foreach ($bazookaCats as $k => $bCats) {
            $catId = $bCats['Bazooka']['id'];
            $data['Bazooka']['status'][$catId] = $bCats['Bazooka']['status'];
        }

        $this->set(compact('bazookaCats'));
        $this->data = $data;
        $this->set('data', $data);

        $this->set('title_for_layout', 'Bazooka Categories');
    }

    function addfaqcategory() {
        $this->uses[] = 'Faqcategory';
        if ($this->request->is('post')) {
            $this->Faqcategory->set($this->data['Faqcategory']);
            if ($this->Faqcategory->validates()) {
                $this->Faqcategory->save($this->data['Faqcategory'], false);
                $eMsg = 'New Category has been added successfully.';
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->redirect($this->params['action']);
            } else {
                $this->generateError('Faqcategory');
            }
        }

        $allCategory = $this->Faqcategory->find('all', array(
            'conditions' => array(""),
            'recursive' => 1
        ));
        $this->set(compact('allCategory'));
        $this->set('title_for_layout', 'Add FAQ Category');
    }

    function managefaqcategories() {
        $this->uses[] = 'Faqcategory';
        $allCategory = $this->Faqcategory->find('all', array(
            'conditions' => array(""),
            'recursive' => 1
        ));
        $this->set(compact('allCategory'));
        $this->set('title_for_layout', 'Manage Categories');
    }

    function editfaqcategory($cId = null) {
        $this->uses[] = 'Faqcategory';
        if ($this->request->is('post')) {
            $this->Faqcategory->set($this->data['Faqcategory']);
            if ($this->Faqcategory->validates()) {
                if ($this->Faqcategory->save($this->data['Faqcategory'], false)) {
                    $eMsg = 'Category Has Been Updated Successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'Could not Update Category';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action'] . '/' . $cId);
            } else {
                $this->generateError('Faqcategory');
            }
        }
        $data = $this->Faqcategory->findById($cId);
        $this->data = $data;
        $this->set('data', $data);
        $allCategory = $this->Faqcategory->find('all', array(
            'conditions' => array(""),
            'recursive' => 1
        ));
        $this->set(compact('allCategory'));

        $this->set('cId', $cId);
        $this->set('title_for_layout', 'Edit Category');
    }

    function deletefaqcategory($id) {
        $this->uses[] = 'Faqcategory';
        $this->uses[] = 'Faq';
        if (empty($id))
            return '';
        $this->autoRender = false;
        $this->layout = 'ajax';

        $this->Faqcategory->id = $id;
        $oData = $this->Faqcategory->read();
        if ($this->Faqcategory->delete($id, false)) {
            $this->Faq->deleteall(array('Faq.category_id' => $id), false);
            $eMsg = 'Category with all records has been deleted successfully!';
            $this->Session->setFlash($eMsg, 'flash_success');
            echo '';
        } else {
            echo 'error';
        }
        exit();
    }

    function addfaq() {
        $this->uses[] = 'Faq';
        $this->uses[] = 'Faqcategory';
        if ($this->request->is('post')) {
            //            pr($_POST); exit;
            $this->Faq->set($this->data['Faq']);
            if ($this->Faq->validates()) {
                $this->Faq->save($this->data['Faq'], false);
                $eMsg = 'New FAQ has been added successfully.';
                $this->Session->setFlash($eMsg, 'flash_success');
                $this->redirect($this->params['action']);
            } else {
                $this->generateError('Faq');
            }
        }
        $cond = array(
            'fields' => 'Faqcategory.id,Faqcategory.category',
            'conditions' => 'Faqcategory.status = 1'
        );
        $categoryList = $this->Faqcategory->find('list', $cond);

        $this->set(compact('categoryList'));
        $this->set('title_for_layout', 'Add FAQ');
    }

    function managefaqs() {
        $this->uses[] = 'Faq';
        $allCategory = $this->Faq->find('all', array(
            'conditions' => array(""),
            'recursive' => 1
        ));
        $this->set(compact('allCategory'));

        $this->set('title_for_layout', 'Manage FAQs');
    }

    function editfaq($cId = null) {
        $this->uses[] = 'Faq';
        $this->uses[] = 'Faqcategory';
        if ($this->request->is('post')) {
            $this->Faq->set($this->data['Faq']);
            if ($this->Faq->validates()) {
                if ($this->Faq->save($this->data['Faq'], false)) {
                    $eMsg = 'FAQ Has Been Updated Successfully.';
                    $this->Session->setFlash($eMsg, 'flash_success');
                } else {
                    $eMsg = 'Could not Update FAQ';
                    $this->Session->setFlash($eMsg, 'flash_error');
                }
                $this->redirect($this->params['action'] . '/' . $cId);
            } else {
                $this->generateError('Faq');
            }
        }
        $data = $this->Faq->findById($cId);
        $this->data = $data;
        $this->set('data', $data);

        $cond = array(
            'fields' => 'Faqcategory.id,Faqcategory.category',
            'conditions' => 'Faqcategory.status = 1'
        );
        $categoryList = $this->Faqcategory->find('list', $cond);

        $this->set('categoryList', $categoryList);

        $this->set('cId', $cId);
        $this->set('title_for_layout', 'Edit FAQ');
    }

    function deletefaq($id) {
        $this->uses[] = 'Faq';
        if (empty($id))
            return '';
        $this->autoRender = false;
        $this->layout = 'ajax';

        $this->Faq->id = $id;
        $oData = $this->Faq->read();
        if ($this->Faq->delete($id, false)) {
            $eMsg = 'FAQ with all records has been deleted successfully!';
            $this->Session->setFlash($eMsg, 'flash_success');
            echo '';
        } else {
            echo 'error';
        }
        exit();
    }

    /*
     *  Domain Control 
     */

    function rootdomains() {
        $this->uses[] = "Commonmodel";
        $this->Commonmodel->useTable = 'rootdomains';
        $roots = $this->Commonmodel->find('all', array(
            'conditions' => array(),
            'order' => 'domain ASC',
            'recursive' => -1,
            //'limit' => 2,
            'fields' => "Commonmodel.*, 
                (select count(*) from domains as Domain where Domain.rootdomain_id = Commonmodel.id) as tD"
                )
        );
        // pr($roots);
        $this->set(compact("roots"));

        $this->set('title_for_layout', 'Root Domains');
    }

    function secondarydomains() {
        $this->uses[] = "Commonmodel";
        $this->Commonmodel->useTable = 'rootdomains';
        $roots = $this->Commonmodel->find('all', array(
            'conditions' => array(),
            'order' => 'domain ASC',
            'recursive' => -1,
            //'limit' => 2,
            'fields' => "Commonmodel.*, 
                (select count(*) from domains as Domain where Domain.rootdomain_id = Commonmodel.id) as tD"
                )
        );
        // pr($roots);
        $this->set(compact("roots"));

        $this->set('title_for_layout', 'Secondary Domains');
    }

    /*
     * all domain with http status good. 200 ok and not parking page etc 
     */

    function gooddomains() {
        $this->loadModel('Commonmodel');
        $this->Commonmodel->useTable = 'dmclass';
        $params = array(
            'conditions' => array(" Domain.livestatus = 'ok' "),
            'order' => ' Domain.domain ASC ',
            'recursive' => 1,
        );
        $this->Domain->bindModel(array(
            'belongsTo' => array(
                'User' => array('foreignKey' => 'user_id', 'conditions' => array("User.role = 'client'"), 'fields' => 'User.firstname,User.lastname,User.id'),
                'Commonmodel' => array('foreignKey' => 'networktype', 'fields' => 'Commonmodel.name,Commonmodel.id'),
            )
        ));
        $allDomains = $this->Domain->find('all', $params);
        //pr($allDomains);            
        $this->set(compact(array('allDomains')));
        $this->set('title_for_layout', 'Working with no Problem Domains');
    }

    /*
     * all domain with http status not 200. many sites http code maybe be good (200 ok) but it might be a parking page 
     */

    function problematics() {
        $this->loadModel('Commonmodel');
        $this->Commonmodel->useTable = 'dmclass';
        $params = array(
            'conditions' => array(" Domain.livestatus != 'ok' "),
            'order' => ' Domain.domain ASC ',
            'recursive' => 1,
                // 'limit' => 10
        );
        $this->Domain->bindModel(array(
            'belongsTo' => array(
                'User' => array('foreignKey' => 'user_id', 'conditions' => array("User.role = 'client'"), 'fields' => 'User.firstname,User.lastname,User.id'),
                'Commonmodel' => array('foreignKey' => 'networktype', 'fields' => 'Commonmodel.name,Commonmodel.id'),
            )
        ));
        $allDomains = $this->Domain->find('all', $params);
        //pr($allDomains);            
        $this->set(compact(array('allDomains')));
        $this->set('title_for_layout', 'NOT WORKING Domains');
    }

    /*
     *  bulk update / refresh domains status
     */

    function updatehttpstatus() {

        $this->loadModel('Commonmodel');
        $this->Commonmodel->useTable = 'dmclass';
        $params = array(
            'conditions' => null, // array( " Domain.livestatus = 'ok' "),
            'order' => ' RAND() ', // ' Domain.livestatus ASC ',
            'recursive' => 1,
            'limit' => 100
        );
        $this->Domain->bindModel(array(
            'belongsTo' => array(
                'User' => array('foreignKey' => 'user_id', 'conditions' => array("User.role = 'client'"), 'fields' => 'User.firstname,User.lastname,User.id'),
                'Commonmodel' => array('foreignKey' => 'networktype', 'fields' => 'Commonmodel.name,Commonmodel.id'),
            )
        ));
        $allDomains = $this->Domain->find('all', $params);
        //pr($allDomains);            
        $this->set(compact(array('allDomains')));

        $this->set('title_for_layout', 'Check Current Site Status');
    }

    /*
     *  update / refresh a domian live status
     */

    function refreshstatus($id = null) {
        if (empty($id))
            return '';
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->Domain->id = $id;
        $info = $this->Domain->findById($id);
        $domain = 'http://' . str_replace('http://', '', $info['Domain']['domain']);

        $http_status_codes = array(0 => "Server Not Found / Unknown", 100 => "Continue", 101 => "Switching Protocols", 102 => "Processing", 200 => "OK", 201 => "Created", 202 => "Accepted", 203 => "Non-Authoritative Information", 204 => "No Content", 205 => "Reset Content", 206 => "Partial Content", 207 => "Multi-Status", 300 => "Multiple Choices", 301 => "Moved Permanently", 302 => "Found", 303 => "See Other", 304 => "Not Modified", 305 => "Use Proxy", 306 => "(Unused)", 307 => "Temporary Redirect", 308 => "Permanent Redirect", 400 => "Bad Request", 401 => "Unauthorized", 402 => "Payment Required", 403 => "Forbidden", 404 => "Not Found", 405 => "Method Not Allowed", 406 => "Not Acceptable", 407 => "Proxy Authentication Required", 408 => "Request Timeout", 409 => "Conflict", 410 => "Gone", 411 => "Length Required", 412 => "Precondition Failed", 413 => "Request Entity Too Large", 414 => "Request-URI Too Long", 415 => "Unsupported Media Type", 416 => "Requested Range Not Satisfiable", 417 => "Expectation Failed", 418 => "I'm a teapot", 419 => "Authentication Timeout", 420 => "Enhance Your Calm", 422 => "Unprocessable Entity", 423 => "Locked", 424 => "Failed Dependency", 424 => "Method Failure", 425 => "Unordered Collection", 426 => "Upgrade Required", 428 => "Precondition Required", 429 => "Too Many Requests", 431 => "Request Header Fields Too Large", 444 => "No Response", 449 => "Retry With", 450 => "Blocked by Windows Parental Controls", 451 => "Unavailable For Legal Reasons", 494 => "Request Header Too Large", 495 => "Cert Error", 496 => "No Cert", 497 => "HTTP to HTTPS", 499 => "Client Closed Request", 500 => "Internal Server Error", 501 => "Not Implemented", 502 => "Bad Gateway", 503 => "Service Unavailable", 504 => "Gateway Timeout", 505 => "HTTP Version Not Supported", 506 => "Variant Also Negotiates", 507 => "Insufficient Storage", 508 => "Loop Detected", 509 => "Bandwidth Limit Exceeded", 510 => "Not Extended", 511 => "Network Authentication Required", 598 => "Network read timeout error", 599 => "Network connect timeout error");

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $domain);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120); // how long it will try to get contents
        curl_setopt($ch, CURLOPT_PORT, 80);
        //curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        $result = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $httpstatus = '';
        if (array_key_exists($responseCode, $http_status_codes)) {
            $httpstatus = $http_status_codes[$responseCode];
        } else {
            $httpstatus = 'unknown';
        }
        $livestatus = 'ok';
        // if response code is 200 then check for db error, hacking, expired etc
        if (!preg_match("~\/wp-content\/themes\/~i", $result)) {
            $livestatus = 'problematic';
        }
        $dt = array(
            'livestatus' => $livestatus,
            'status' => ($livestatus == 'ok' ? 1 : 0), //  no post or no action will be working if stauts is 0
            'httpcode' => $responseCode,
            'httpstatus' => $httpstatus
        );
        //echo json_encode($dt); exit();

        if ($this->Domain->save($dt, false)) {
            echo json_encode($dt);
        } else {
            $dt = array(
                'livestatus' => $info['Domain']['livestatus'] . ' ( old status ) ',
                'status' => 'erro',
                'httpcode' => $info['Domain']['httpcode'],
                'httpstatus' => $info['Domain']['httpstatus']
            );
            echo json_encode($dt);
        }
        exit();
    }

    // export domains status 
    function exportdomainstatus($type = 'ok') {
        $this->autoRender = false;
        $this->layout = 'ajax';
        App::import('Vendor', 'PHPExcel');

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("SeoNitro Dashboard")
                ->setLastModifiedBy("SeoNitro Dashboard")
                ->setTitle("Domain Status")
                ->setSubject("Domain Status")
                ->setDescription("Domain Status with Status Code. Which Need to be fixed ASAP");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Domain Name')
                ->setCellValue('B1', 'Root Domain')
                ->setCellValue('C1', 'Account Number')
                ->setCellValue('D1', 'Expires On')
                ->setCellValue('E1', 'NameServers')
                ->setCellValue('F1', 'PR')
                ->setCellValue('G1', 'Indexed')
                ->setCellValue('H1', 'Assigned User')
                ->setCellValue('I1', 'Assigned Network')
                ->setCellValue('J1', 'Http Code')
                ->setCellValue('K1', 'Http Status')
                ->setCellValue('L1', 'CMS')
                ->setCellValue('M1', 'username')
                ->setCellValue('N1', 'Password')
                ->setCellValue('O1', 'Remarks')
                ->setCellValue('P1', 'Your Comments')
        ;

        $this->loadModel('Commonmodel');
        $this->Commonmodel->useTable = 'dmclass';
        $params = array(
            'conditions' => array(" Domain.livestatus = '" . $type . "' "),
            'order' => ' Domain.networktype ASC ',
            'recursive' => 1,
        );
        $this->Domain->bindModel(array(
            'belongsTo' => array(
                'User' => array('foreignKey' => 'user_id', 'conditions' => array("User.role = 'client'"), 'fields' => 'User.firstname,User.lastname,User.id'),
                'Commonmodel' => array('foreignKey' => 'networktype', 'fields' => 'Commonmodel.name,Commonmodel.id'),
            )
        ));
        $allDomains = $this->Domain->find('all', $params);
        $inc = 2;
        foreach ($allDomains as $k => $d) {

            $domain = $d['Domain']['domain'];
            $root = $d['Domain']['rootdomain_id'];
            $acnumber = $d['Domain']['regnumber'];
            $expires = $d['Domain']['expires_on'];
            $dns = $d['Domain']['dns1'];
            $pr = $d['Domain']['pr'];
            $index = $d['Domain']['is_indexed'];
            $assignedUser = ( trim($d['User']['firstname']) != '') ? $d['User']['firstname'] . ' ' . $d['User']['lastname'] : '-';
            $assignedNetwork = $d['Commonmodel']['name'];
            $httpCode = $d['Domain']['httpcode'];
            $httpStatus = $d['Domain']['httpstatus'];
            $livestatus = $d['Domain']['livestatus'];
            $cms = $d['Domain']['cms'];
            $user = $d['Domain']['ausername'];
            $pass = $d['Domain']['apassword'];

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $inc, $domain)
                    ->setCellValue('B' . $inc, $root)
                    ->setCellValue('C' . $inc, $acnumber)
                    ->setCellValue('D' . $inc, $expires)
                    ->setCellValue('E' . $inc, $dns)
                    ->setCellValue('F' . $inc, $pr)
                    ->setCellValue('G' . $inc, $index)
                    ->setCellValue('H' . $inc, $assignedUser)
                    ->setCellValue('I' . $inc, $assignedNetwork)
                    ->setCellValue('J' . $inc, $httpCode)
                    ->setCellValue('K' . $inc, $httpStatus)
                    ->setCellValue('L' . $inc, $cms)
                    ->setCellValue('M' . $inc, $user)
                    ->setCellValue('N' . $inc, $pass)
                    ->setCellValue('O' . $inc, '')
                    ->setCellValue('P' . $inc, '')
            ;
            $inc++;
        }

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Domain Stauts');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel2007)
        $fileName = date('Y_m_d_h_i_s') . '_Domain_status.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');


        exit;
    }

    function adddomains() {
        $this->set('title_for_layout', 'Add Domains');
    }

    function editdomains($id = null) {
        $this->set('title_for_layout', 'Edit Domains');
    }

    function deletedomains($id = null) {
        $this->set('title_for_layout', 'Delete Domains');
    }

    function dnslookups() {

        $this->set('title_for_layout', 'Expired Domains');
    }

    function adomaindetails($id = null) {
        if (empty($id)) {
            return false;
        }
        $this->uses[] = "Commonmodel";
        $this->Commonmodel->useTable = 'rootdomains';

        $this->Commonmodel->bindModel(array(
            'hasMany' => array(
                'Domain' => array('foreignKey' => 'rootdomain_id'),
            )
        ));

        $data = $this->Commonmodel->find('all', array(
            'conditions' => array(" Commonmodel.id = $id "),
            'recursive' => 1,
                )
        );
        //pr($data);
        $this->set(compact("data"));

        $this->set('title_for_layout', 'Root Domains Details');
    }

    // ajax show

    /* for user credit update */

    function viewusercredits($userId) {
        $userInfo = $this->User->find('first', array(
            'conditions' => array("User.id = $userId"),
            'recursive' => 1
        ));
        $this->set(compact('userInfo'));

        $serviceList = $this->getUserPlanInfo($userId);

        $this->set(compact('serviceList'));
        $this->set('title_for_layout', 'User Plan');
    }

    function updateusercredits($userId) {
        $userInfo = $this->User->find('first', array(
            'conditions' => array("User.id = $userId"),
            'recursive' => 1
        ));
        $this->set(compact('userInfo'));
        $serviceList = $this->getUserPlanInfo($userId);
        //pr($serviceList);
        $this->set(compact('serviceList'));

        if ($this->request->is('post')) {
            $extraCredits = $this->request->data['User'];
            //pr($extraCredits); exit;
            $this->User->save($extraCredits, false);

            $userCredits = $this->data['Usercredit'];
            if ($userCredits) {
                $this->loadModel('Usercredit');
                foreach ($userCredits as $uc) {
                    if ($uc['id'] == '') {
                        $this->Usercredit->create();
                    }
                    $this->Usercredit->save($uc, false);
                }
            }
            $eMsg = 'User Credits updated successfully';
            $this->Session->setFlash($eMsg, 'flash_success');
            $this->redirect(array('action' => 'updateusercredits', $userId));
        }
        $this->set('title_for_layout', 'Update User Plan');
    }

    private function getUserPlanInfo($userId) {

        $this->loadModel('Service');
        $serviceList = $this->Service->find('all');

        $this->loadModel('Serviceprice');

        $this->User->id = $userId;
        $packageId = $this->User->field('package_id');

        $servicePrices = $this->Serviceprice->find('all', array('conditions' => "Serviceprice.package_id = $packageId"));

        //pr($servicePrices);

        $this->loadModel('Usercredit');
        $userCredits = $this->Usercredit->find('all', array('conditions' => "Usercredit.user_id = $userId AND Usercredit.status = 1"));

        if ($serviceList) {
            foreach ($serviceList as $key => $service) {
                if ($servicePrices) {
                    foreach ($servicePrices as $price) {
                        if ($price['Serviceprice']['service_id'] == $service['Service']['id']) {
                            $serviceList[$key]['Service']['servicelimit'] = $price['Serviceprice']['servicelimit'];
                        }
                    }
                    if ($userCredits) {
                        foreach ($userCredits as $credit) {
                            if ($credit['Usercredit']['service_id'] == $service['Service']['id']) {
                                $serviceList[$key]['Service']['uc_id'] = $credit['Usercredit']['id'];
                                $serviceList[$key]['Service']['service_used'] = $credit['Usercredit']['service_used'];
                                $serviceList[$key]['Service']['extra_unit'] = $credit['Usercredit']['extra_unit'];
                            }
                        }
                    }
                }
            }
        }

        return $serviceList;
    }

    /**/

    function gorilla_credits() {
        Configure::write('Cache.disable', true);
        $this->loadModel('Setting');
        $credit_info = $this->credit_needed();
        $credit_needed = $credit_info['credit_needed'];
        //$gorilla_credit = $credit_info['gorilla_credit'];
        $this->set('credit_needed', $credit_needed);
        $this->set('payment_form', 0);

        if (isset($_GET['success']) == 1 && isset($_POST['Transaction_Approved'])) {
            if (($_POST['Transaction_Approved'] == 'YES')) {
                $amount = intval($_POST['x_amount']);
                $this->Setting->query("UPDATE `settings` SET gorilla_credit = gorilla_credit + $amount ");
                $eMsg = 'You have successfully added your credit.';
                $this->Session->setFlash($eMsg, 'flash_success');
            }
            $this->redirect('gorilla_credits');
        }

        if ($this->request->is('post') && isset($this->request->data['Card']['amount'])) {
            $this->set('payment_form', 1);
            $amount = $this->request->data['Card']['amount'];
            $this->set('amount', $amount);
        }
        $data = $this->Setting->find('first');
        $this->data = $data;
        $this->set('data', $data);

        $this->set('title_for_layout', 'Gorilla Credits');
    }

    function mashup_report() {

        $allData = array();
        $this->loadModel('Socialposthistory');

        $allData = $this->Socialposthistory->find('all', array(
            'fields' => array(
                'COUNT(Socialposthistory.id) AS post_count',
                'DATE(Socialposthistory.created) AS created',
                'DATE_FORMAT(Socialposthistory.created,"%Y-%m") AS yr_mnth',
                '(CASE WHEN DAY(Socialposthistory.created) BETWEEN 1 AND 7 THEN 1 '
                . 'WHEN DAY(Socialposthistory.created) BETWEEN 8 AND 14 THEN 2 '
                . 'WHEN DAY(Socialposthistory.created) BETWEEN 15 AND 21 THEN 3 '
                . 'WHEN DAY(Socialposthistory.created) BETWEEN 22 AND 28 THEN 4 ELSE 5 END) AS week_no'
            ),
            'conditions' => 'Socialposthistory.type = 1',
            'group' => 'yr_mnth,week_no',
            'order' => 'yr_mnth DESC'
        ));

        $this->set(compact('allData'));

        $this->set('title_for_layout', "Rogers' Mashup Report");
    }

    function linkemperor_report() {

        $allData = array();
        $this->loadModel('Linkemperor');

        $allData = $this->Linkemperor->find('all', array(
            'fields' => array(
                'COUNT(Linkemperor.id) AS order_count',
                'order_type',
                'DATE_FORMAT(Linkemperor.created,"%Y-%m") AS yr_mnth'
            ),
            'conditions' => "",
            'group' => 'yr_mnth,order_type',
            'order' => 'yr_mnth DESC'
        ));

        //pr($allData);
        //exit;
        $this->set(compact('allData'));
        $this->set('title_for_layout', "Linkemperor Order Count");
    }

    function iwriter_report() {
        $this->loadModel('Iwarticle');
        $this->loadModel('Iwreport');

        $this->Iwreport->virtualFields = array(
            'post_count' => 'SELECT COUNT(id)',
            'mnth_yr' => 'DATE_FORMAT(Iwreport.created,"%M, %Y")',
        );

        $preReport = $this->Iwreport->find('all', array('fields' => 'network_type,post_count,mnth_yr', 'conditions' => array(), 'group' => 'DATE_FORMAT(created,"%Y-%m"),network_type', 'order' => 'mnth_yr'));

        $itemList = array();
        if ($preReport) {
            foreach ($preReport as $item) {
                $mnth_yr = $item['Iwreport']['mnth_yr'];
                $network_type = $item['Iwreport']['network_type'];
                $itemList[$mnth_yr][$network_type] = $post_count = $item['Iwreport']['post_count'];
                @$itemList[$mnth_yr]['total'] += $post_count;
            }
        }

        $this->set(compact('itemList'));
        $this->set('title_for_layout', "iWriter Posting Report");
    }

    function ranking_report() {

        //Configure::write('debug', 2);
        //Configure::write('Cache.disable', true);

        $this->loadModel('Rankreport');
        $this->Rankreport->virtualFields = array(
            'username' => 'SELECT username FROM users WHERE Rankreport.user_id = users.id',
            'fullname' => 'SELECT CONCAT(users.firstname, " ", users.lastname) FROM users WHERE Rankreport.user_id = users.id',
            'email' => 'SELECT email FROM users WHERE Rankreport.user_id = users.id',
        );

        $fields = 'id,user_id,username,fullname,email,keyword,position,rank_date,engine';

        $cond = "(position LIKE '%i:0;i:1;i:1;i:1;%' OR position LIKE '%i:0;i:1;i:1;i:2;%' OR position LIKE '%i:0;i:1;i:1;i:3;%' OR position LIKE '%i:0;i:1;i:1;i:4;%' OR position LIKE '%i:0;i:1;i:1;i:5;%' OR position LIKE '%i:0;i:1;i:1;i:6;%' OR position LIKE '%i:0;i:1;i:1;i:7;%' OR position LIKE '%i:0;i:1;i:1;i:8;%'  OR position LIKE '%i:0;i:1;i:1;i:9;%')";
        $allRanked = $this->Rankreport->find('all', array('conditions' => $cond, 'fields' => $fields));

        if ($allRanked) {
            foreach ($allRanked as $key => $item) {
                $allRanked[$key]['Rankreport']['result'] = unserialize($allRanked[$key]['Rankreport']['position']);
            }
        }

        $itemList = array();
        $haystack = array();

        foreach ($allRanked as $key => $item) {
            $arr = $item['Rankreport'];
            if (is_array($arr['result'])) {
                foreach ($arr['result'] as $url => $report) {
                    if ($report[0] == 1) {
                        $k_u = $arr['keyword'] . ' ' . $url . ' ' . $arr['engine'];
                        if (!in_array($k_u, $haystack)) {
                            if ($arr['fullname']) {
                                $temp['id'] = $arr['id'];
                                $temp['user_id'] = $arr['user_id'];
                                $temp['username'] = $arr['username'];
                                $temp['fullname'] = $arr['fullname'];
                                $temp['email'] = $arr['email'];
                                $temp['keyword'] = $arr['keyword'];
                                $temp['url'] = $url;
                                $temp['ranking'] = $report[1];
                                $temp['engine'] = $arr['engine'];
                                $temp['rank_date'] = $arr['rank_date'];
                                $itemList[] = $temp;
                            }
                        }
                        $haystack[] = $k_u;
                    }
                }
            }
        }
        $this->Session->write('Rank.report', $itemList);

        $this->set(compact('itemList'));
        $this->set('title_for_layout', "Current Ranking Report");
    }

    function ranking_report_export() {
        $this->autoRender = false;

        if ($this->Session->check('Rank.report')) {
            $itemList = $this->Session->read('Rank.report');

            if ($itemList) {
                $resultRows = array();
                $count = 0;
                $resultRows[$count]['fullname'] = 'Name';
                $resultRows[$count]['keyword'] = 'Keyword';
                $resultRows[$count]['url'] = 'URL';
                $resultRows[$count]['ranking'] = 'Ranking';
                $resultRows[$count]['engine'] = 'Engine';
                $resultRows[$count]['email'] = 'Email';
                $count++;
                $resultRows[$count]['fullname'] = '';
                $resultRows[$count]['keyword'] = '';
                $resultRows[$count]['url'] = '';
                $resultRows[$count]['ranking'] = '';
                $resultRows[$count]['engine'] = '';
                $resultRows[$count]['email'] = '';

                foreach ($itemList as $row) {
                    $count++;
                    $resultRows[$count]['fullname'] = $row['fullname'];
                    $resultRows[$count]['keyword'] = $row['keyword'];
                    $resultRows[$count]['url'] = $row['url'];
                    $resultRows[$count]['ranking'] = $row['ranking'];
                    $resultRows[$count]['engine'] = $row['engine'];
                    $resultRows[$count]['email'] = $row['email'];
                }
                $dateTimeString = date('m_d_Y_H_i_s');
                $this->array_to_csv($resultRows, 'Ranking_Report_' . $dateTimeString . '.csv');
            }
        }
        exit;
    }

}
