<?php
App::uses('AppController', 'Controller');

if (!defined('TEST_EMAIL')) {
    //define('TEST_EMAIL', 'mainuljs@outlook.com');
    define('TEST_EMAIL', 'mainul@aan-nahl.com');
}

Configure::write('debug', 2);

class ZpnController extends AppController {

    public $_packageArr = array('2629453511725', '2629443911727', '2629495311728');
    public $_upgradeArr = array('2629479312011', '2629404312012');

    public function beforeFilter() {
        //parent::beforeFilter();

        set_time_limit(0);
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 86400);
        ini_set('max_input_time', 86400);

        $this->Auth->allow();

        $this->layout = null;
        $this->autoRender = false;
    }

    function beforeRender() {
        
    }

    function test() {
        $this->cmail(TEST_EMAIL, 'Hello Dear!', 'This is message!');

        $this->loadModel('Domain');
        $submissionList = $this->Domain->find('first');

        $mBody = print_r($submissionList, true);
        $this->cmail(TEST_EMAIL, 'Bye Bye!', $mBody);
    }

    function index() {
        $this->layout = null;
        $this->autoRender = false;
        $this->loadModel('User');

        //$signature = 'G5QGERZTVBM11W1N'; // test account api at mainul@aan-nahl.com
        $signature = '8KUTL8LL8JPL51FL'; // live api for rankratio

        if (!empty($_POST['trans_type'])) {

            $emailBody = '<pre>' . print_r($_POST, true) . '</pre>';

            $this->cmail(TEST_EMAIL, 'ZPN-Listener!', $emailBody);

            // Read POST data and validate the ZPN
            $transType = $_POST['trans_type'];
            $transReceipt = $_POST['trans_receipt'];
            $transAmount = $_POST['trans_amount'];
            $sellerID = $_POST['seller_id'];
            $hashKey = $_POST['hash_key'];

            $myHashKey = strtoupper(md5($sellerID . $signature . $transReceipt . $transAmount));
            if ($myHashKey == $hashKey) {
                // ZPN is valid
                // Do something...
                // You can do more validation like matching your seller ID
                // or matching your email address...
                // or validate the received amount...

                $products = $_POST['products'];
                switch ($transType) {
                    case 'SALE':
                        // New payment for one-time product(s)
                        foreach ($products as $product) {
                            // This ZPN may contain a payment for multiple products
                            // do something for each of the product here..
                            $sku = $product['prod_number'];
                        }
                        if ($products) {

                            $sku = $product['prod_number'];

                            $userInfo['product_id'] = $prodNumber = $products[0]['prod_number'];

                            $email = $_POST['cust_email'];
                            $userPrev = $this->User->find('first', array('conditions' => "email = '$email'"));

                            $this->loadModel('Package');
                            $packageInfo = $this->Package->find('first', array('conditions' => array('product_id' => "$prodNumber")));

                            if ($userPrev) {
                                $userId = $userPrev['User']['id'];
                                $this->User->id = $userId;
                                $currentDateTime = date('Y-m-d H:i:s');

                                $userData = array();
                                if ($packageInfo) {
                                    $userData['package_id'] = $packageInfo['Package']['id'];
                                    $userData['package_renew_date'] = $currentDateTime;
                                    $userData['trans_receipt'] = $_POST['trans_receipt'];
                                    $userData['product_id'] = $prodNumber;
                                    $userData['recurring_id'] = $product['recurring_id'];
                                }

                                $userCredit = array();
                                if ($prodNumber == '2629479312011') {
                                    $userCredit['service_id'] = 18;
                                    $userCredit['extra_unit'] = 100;

                                    $userData['high_pr'] = 1;
                                    $userData['high_pr_type'] = 'Pro';
                                    $userData['high_pr_date'] = $currentDateTime;
                                    $userData['high_pr_trans_receipt'] = $_POST['trans_receipt'];
                                    if ($product['payment_type'] == 'subscription') {
                                        $userData['high_pr_recurring_id'] = $product['recurring_id'];
                                    }
                                } elseif ($prodNumber == '2629404312012') {
                                    $userCredit['service_id'] = 18;
                                    $userCredit['extra_unit'] = 50;

                                    $userData['high_pr'] = 1;
                                    $userData['high_pr_type'] = 'Standard';
                                    $userData['high_pr_date'] = $currentDateTime;
                                    $userData['high_pr_trans_receipt'] = $_POST['trans_receipt'];
                                    if ($product['payment_type'] == 'subscription') {
                                        $userData['high_pr_recurring_id'] = $product['recurring_id'];
                                    }
                                } elseif ($prodNumber == '2629408512013') {
                                    $userData['link_density'] = 1;
                                    $userData['link_density_date'] = $currentDateTime;
                                    $userData['link_density_trans_receipt'] = $_POST['trans_receipt'];
                                    if ($product['payment_type'] == 'subscription') {
                                        $userData['link_density_recurring_id'] = $product['recurring_id'];
                                    }
                                }
                                //exit(pr($userData));
                                if ($userData) {
                                    $this->User->save($userData, false);
                                }
                                if ($userCredit) {
                                    $serviceId = $userCredit['service_id'];
                                    $this->loadModel('Usercredit');
                                    $userCredit['user_id'] = $userId;

                                    $cInfo = $this->Usercredit->find('first', array('conditions' => "user_id = $userId AND service_id = $serviceId"));
                                    if ($cInfo) {
                                        $userCredit['id'] = $cInfo['Usercredit']['id'];
                                    }
                                    $this->Usercredit->save($userCredit, false);
                                }
                                $this->cmail($email, 'RankRatio Account Update', 'Your account is updated successfully!');
                            } else {
                                $prevUsers = $this->User->find('list', array('fields' => 'id,username'));

                                $firstName = $_POST['cust_firstname'];
                                $lastName = $_POST['cust_lastname'];

                                $userName = $this->userNameCheck(strtolower($firstName), strtolower($lastName), $prevUsers);
                                $password = $this->generate_salt(8);

                                $this->loadModel('Package');
                                $packageInfo = $this->Package->find('first', array('conditions' => array('product_id' => $prodNumber)));
                                //pr($packageInfo);                                    

                                if ($packageInfo) {
                                    $userInfo['package_id'] = $packageInfo['Package']['id'];

                                    $userInfo['username'] = $userName;
                                    $userInfo['password'] = AuthComponent::password($password);
                                    $userInfo['ccode'] = $password;
                                    $userInfo['firstname'] = $firstName;
                                    $userInfo['lastname'] = $lastName;
                                    $userInfo['email'] = $_POST['cust_email'];
                                    $userInfo['verified'] = 1;
                                    $userInfo['package_renew_date'] = date('Y-m-d');
                                    $userInfo['created_by'] = 'Zaxaa';

                                    $userInfo['trans_receipt'] = $_POST['trans_receipt'];

                                    // NOTE: If this is a ZPN for OTO with a subscription product,
                                    // then this ZPN may also contain information for one-time product(s)
                                    if ($product['payment_type'] == 'subscription') {
                                        // The product is a subscription
                                        // do something...
                                        $userInfo['recurring_id'] = $product['recurring_id'];
                                    } else if ($product['payment_type'] == 'onetime') {
                                        // The product is a one-time...
                                        // do something...
                                    }

                                    try {
                                        $this->register($userInfo);
                                    } catch (Exception $exc) {
                                        //echo $exc->getTraceAsString();
                                    }
                                }
                            }
                        }
                        break;

                    case 'FIRST_BILL':
                        // New payment for a subscription product(s)
                        foreach ($products as $product) {
                            // This ZPN may contain a payment for multiple products
                            // do something for each of the product here..
                            if ($products) {

                                $sku = $product['prod_number'];

                                $userInfo['product_id'] = $prodNumber = $products[0]['prod_number'];

                                $email = $_POST['cust_email'];
                                $userPrev = $this->User->find('first', array('conditions' => "email = '$email'"));

                                $this->loadModel('Package');
                                $packageInfo = $this->Package->find('first', array('conditions' => array('product_id' => "$prodNumber")));

                                if ($userPrev) {
                                    $userId = $userPrev['User']['id'];
                                    $this->User->id = $userId;
                                    $currentDateTime = date('Y-m-d H:i:s');

                                    $userData = array();
                                    if ($packageInfo) {
                                        $userData['package_id'] = $packageInfo['Package']['id'];
                                        $userData['package_renew_date'] = $currentDateTime;
                                        $userData['trans_receipt'] = $_POST['trans_receipt'];
                                        $userData['product_id'] = $prodNumber;
                                        $userData['recurring_id'] = $product['recurring_id'];
                                    }

                                    $userCredit = array();
                                    if ($prodNumber == '2629479312011') {
                                        $userCredit['service_id'] = 18;
                                        $userCredit['extra_unit'] = 100;

                                        $userData['high_pr'] = 1;
                                        $userData['high_pr_type'] = 'Pro';
                                        $userData['high_pr_date'] = $currentDateTime;
                                        $userData['high_pr_trans_receipt'] = $_POST['trans_receipt'];
                                        if ($product['payment_type'] == 'subscription') {
                                            $userData['high_pr_recurring_id'] = $product['recurring_id'];
                                        }
                                    } elseif ($prodNumber == '2629404312012') {
                                        $userCredit['service_id'] = 18;
                                        $userCredit['extra_unit'] = 50;

                                        $userData['high_pr'] = 1;
                                        $userData['high_pr_type'] = 'Standard';
                                        $userData['high_pr_date'] = $currentDateTime;
                                        $userData['high_pr_trans_receipt'] = $_POST['trans_receipt'];
                                        if ($product['payment_type'] == 'subscription') {
                                            $userData['high_pr_recurring_id'] = $product['recurring_id'];
                                        }
                                    } elseif ($prodNumber == '2629408512013') {
                                        $userData['link_density'] = 1;
                                        $userData['link_density_date'] = $currentDateTime;
                                        $userData['link_density_trans_receipt'] = $_POST['trans_receipt'];
                                        if ($product['payment_type'] == 'subscription') {
                                            $userData['link_density_recurring_id'] = $product['recurring_id'];
                                        }
                                    }
                                    //exit(pr($userData));
                                    if ($userData) {
                                        $this->User->save($userData, false);
                                    }
                                    if ($userCredit) {
                                        $serviceId = $userCredit['service_id'];
                                        $this->loadModel('Usercredit');
                                        $userCredit['user_id'] = $userId;

                                        $cInfo = $this->Usercredit->find('first', array('conditions' => "user_id = $userId AND service_id = $serviceId"));
                                        if ($cInfo) {
                                            $userCredit['id'] = $cInfo['Usercredit']['id'];
                                        }
                                        $this->Usercredit->save($userCredit, false);
                                    }
                                    $this->cmail($email, 'RankRatio Account Update', 'Your account is updated successfully!');
                                } else {
                                    $prevUsers = $this->User->find('list', array('fields' => 'id,username'));

                                    $firstName = $_POST['cust_firstname'];
                                    $lastName = $_POST['cust_lastname'];

                                    $userName = $this->userNameCheck(strtolower($firstName), strtolower($lastName), $prevUsers);
                                    $password = $this->generate_salt(8);

                                    $this->loadModel('Package');
                                    $packageInfo = $this->Package->find('first', array('conditions' => array('product_id' => $prodNumber)));
                                    //pr($packageInfo);                                    

                                    if ($packageInfo) {
                                        $userInfo['package_id'] = $packageInfo['Package']['id'];

                                        $userInfo['username'] = $userName;
                                        $userInfo['password'] = AuthComponent::password($password);
                                        $userInfo['ccode'] = $password;
                                        $userInfo['firstname'] = $firstName;
                                        $userInfo['lastname'] = $lastName;
                                        $userInfo['email'] = $_POST['cust_email'];
                                        $userInfo['verified'] = 1;
                                        $userInfo['package_renew_date'] = date('Y-m-d');
                                        $userInfo['created_by'] = 'Zaxaa';

                                        $userInfo['trans_receipt'] = $_POST['trans_receipt'];

                                        // NOTE: If this is a ZPN for OTO with a subscription product,
                                        // then this ZPN may also contain information for one-time product(s)
                                        if ($product['payment_type'] == 'subscription') {
                                            // The product is a subscription
                                            // do something...
                                            $userInfo['recurring_id'] = $product['recurring_id'];
                                        } else if ($product['payment_type'] == 'onetime') {
                                            // The product is a one-time...
                                            // do something...
                                        }

                                        try {
                                            $this->register($userInfo);
                                        } catch (Exception $exc) {
                                            //echo $exc->getTraceAsString();
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    case 'REBILL':
                        // Recurring payment received
                        foreach ($products as $product) {
                            $sku = $product['prod_number'];
                            $recurring_id = $product['recurring_id']; // This might be useful to recognize past recurring payments
                            // do something...
                            $this->loadModel('User');
                            $userInfo = $this->User->find('first', array('conditions' => array('recurring_id' => $recurring_id)));
                            if ($userInfo) {
                                $updateData['package_renew_date'] = date('Y-m-d');
                                $this->User->id = $userInfo['User']['id'];
                                $this->User->save($updateData, false);
                            }
                        }
                        break;

                    case 'CANCELED':
                        // Subscription is canceled
                        foreach ($products as $product) {
                            $sku = $product['prod_number'];
                            $currentDateTime = date('Y-m-d H:i:s');
                            $recurring_id = $product['recurring_id']; // This might be useful to recognize past recurring payments

                            $updateData = array();
                            $userCredit = array();

                            $cond = '';

                            if (in_array($sku, $this->_packageArr)) {
                                $cond = "recurring_id = '$recurring_id'";
                                $updateData['package_renew_date'] = date('Y-m-d');
                                $updateData['package_id'] = 1;
                                $updateData['trans_receipt'] = null;
                                $updateData['product_id'] = null;
                                $updateData['recurring_id'] = null;


                                $updateData['high_pr'] = 0;
                                $updateData['high_pr_type'] = null;
                                $updateData['high_pr_trans_receipt'] = null;
                                $updateData['high_pr_recurring_id'] = null;
                                $updateData['high_pr_date'] = $currentDateTime;

                                $userCredit['service_id'] = 18;
                                $userCredit['extra_unit'] = 0;

                                $updateData['link_density'] = 0;
                                $updateData['link_density_trans_receipt'] = null;
                                $updateData['link_density_recurring_id'] = null;
                                $updateData['link_density_date'] = $currentDateTime;
                            } elseif (in_array($sku, $this->_upgradeArr)) {
                                $cond = "high_pr_recurring_id = '$recurring_id'";
                                $updateData['high_pr'] = 0;
                                $updateData['high_pr_type'] = null;
                                $updateData['high_pr_trans_receipt'] = null;
                                $updateData['high_pr_recurring_id'] = null;
                                $updateData['high_pr_date'] = $currentDateTime;

                                $userCredit['service_id'] = 18;
                                $userCredit['extra_unit'] = 0;
                            } elseif ($sku == '2629408512013') {
                                $cond = "link_density_recurring_id = '$recurring_id'";
                                $updateData['link_density'] = 0;
                                $updateData['link_density_trans_receipt'] = null;
                                $updateData['link_density_recurring_id'] = null;
                                $updateData['link_density_date'] = $currentDateTime;
                            }
                            // do something...
                            $this->loadModel('User');
                            $userInfo = $this->User->find('first', array('conditions' => $cond));
                            //pr($userInfo);
                            if ($userInfo) {
                                $userId = $userInfo['User']['id'];
                                if ($updateData) {
                                    $this->User->id = $userId;
                                    $this->User->save($updateData, false);
                                }
                                if ($userCredit) {
                                    $this->loadModel('Usercredit');
                                    $serviceId = $userCredit['service_id'];
                                    $this->loadModel('Usercredit');
                                    $userCredit['user_id'] = $userId;

                                    $cInfo = $this->Usercredit->find('first', array('conditions' => "user_id = $userId AND service_id = $serviceId"));
                                    if ($cInfo) {
                                        $userCredit['id'] = $cInfo['Usercredit']['id'];
                                    }
                                    $this->Usercredit->save($userCredit, false);
                                }
                                $email = $_POST['cust_email'];
                                $this->cmail($email, 'RankRatio Account Cancelation', 'Your account is canceled!');
                            }
                        }
                        break;

                    case 'REFUND':
                        // Refunded payment...
                        foreach ($products as $product) {
                            $sku = $product['prod_number'];
                            $currentDateTime = date('Y-m-d H:i:s');
                            $recurring_id = $product['recurring_id']; // This might be useful to recognize past recurring payments

                            $updateData = array();
                            $userCredit = array();

                            $cond = '';

                            if (in_array($sku, $this->_packageArr)) {
                                $cond = "recurring_id = '$recurring_id'";
                                $updateData['package_renew_date'] = date('Y-m-d');
                                $updateData['package_id'] = 1;
                                $updateData['trans_receipt'] = null;
                                $updateData['product_id'] = null;
                                $updateData['recurring_id'] = null;


                                $updateData['high_pr'] = 0;
                                $updateData['high_pr_type'] = null;
                                $updateData['high_pr_trans_receipt'] = null;
                                $updateData['high_pr_recurring_id'] = null;
                                $updateData['high_pr_date'] = $currentDateTime;

                                $userCredit['service_id'] = 18;
                                $userCredit['extra_unit'] = 0;

                                $updateData['link_density'] = 0;
                                $updateData['link_density_trans_receipt'] = null;
                                $updateData['link_density_recurring_id'] = null;
                                $updateData['link_density_date'] = $currentDateTime;
                            } elseif (in_array($sku, $this->_upgradeArr)) {
                                $cond = "high_pr_recurring_id = '$recurring_id'";
                                $updateData['high_pr'] = 0;
                                $updateData['high_pr_type'] = null;
                                $updateData['high_pr_trans_receipt'] = null;
                                $updateData['high_pr_recurring_id'] = null;
                                $updateData['high_pr_date'] = $currentDateTime;

                                $userCredit['service_id'] = 18;
                                $userCredit['extra_unit'] = 0;
                            } elseif ($sku == '2629408512013') {
                                $cond = "link_density_recurring_id = '$recurring_id'";
                                $updateData['link_density'] = 0;
                                $updateData['link_density_trans_receipt'] = null;
                                $updateData['link_density_recurring_id'] = null;
                                $updateData['link_density_date'] = $currentDateTime;
                            }
                            // do something...
                            $this->loadModel('User');
                            $userInfo = $this->User->find('first', array('conditions' => $cond));
                            //pr($userInfo);
                            if ($userInfo) {
                                $userId = $userInfo['User']['id'];
                                if ($updateData) {
                                    $this->User->id = $userId;
                                    $this->User->save($updateData, false);
                                }
                                if ($userCredit) {
                                    $this->loadModel('Usercredit');
                                    $serviceId = $userCredit['service_id'];
                                    $this->loadModel('Usercredit');
                                    $userCredit['user_id'] = $userId;

                                    $cInfo = $this->Usercredit->find('first', array('conditions' => "user_id = $userId AND service_id = $serviceId"));
                                    if ($cInfo) {
                                        $userCredit['id'] = $cInfo['Usercredit']['id'];
                                    }
                                    $this->Usercredit->save($userCredit, false);
                                }
                                $email = $_POST['cust_email'];
                                $this->cmail($email, 'RankRatio Account Cancelation', 'Your account is canceled!');
                            }
                        }
                        break;
                }
            } else {
                // ZPN is Invalid
                // log for manual investigation
            }
        }
        exit;
    }

    function testpayment() {
        $this->layout = null;
        $this->autoRender = false;

        $this->loadModel('User');

        $signature = 'G5QGERZTVBM11W1N'; // test account api at mainul@aan-nahl.com
        //$signature = '8KUTL8LL8JPL51FL'; // live api for rankratio

        $_POST_ = array(
            'trans_gateway' => 'PAYPAL_ADAPTIVE',
            'trans_receipt' => '20AD8AACDEB6AA416DC69B0B1B440182',
            //'trans_type' => 'SALE',
            //'trans_type' => 'REFUND',
            'trans_type' => 'FIRST_BILL',
            'trans_date' => '2014-08-03 06:52:38 UTC',
            'trans_amount' => '1.00',
            'trans_currency' => 'USD',
            'trans_testmode' => '1',
            'is_oto' => '0',
            'seller_id' => '44927',
            'seller_email' => 'mainul@aan-nahl.com',
            'cust_firstname' => 'Mainul',
            'cust_lastname' => 'Islam',
            'cust_address' => '',
            'cust_state' => '',
            'cust_city' => '',
            'cust_country' => '',
            'cust_email' => 'mainul@seonitro.com',
            'item_count' => '1',
            'products' =>
            array(
                0 =>
                array(
                    //'prod_number' => '4492782612090',
                    //'prod_number' => '2629453511725',
                    //'prod_number' => '4492768712200', //recurring test
                    //'prod_number' => '2629495311728', //recurring test
                    //'prod_number' => '2629443911727', //recurring test
                    'prod_number' => '2629495311728', //recurring test
                    'prod_name' => 'aan-nahl test',
                    'prod_type' => 'FRONTEND',
                    'amount' => '1.00',
                    //'payment_type' => 'onetime',
                    'payment_type' => 'subscription',
                    'recurring_id' => 'sub_4yGnO0VhokwKnK',
                    'custom' => '',
                ),
            ),
            'seller_paypal_email' => 'mainul@aan-nahl.com',
            'cust_paypal_email' => 'itestgo@gmail.com',
            'paypal_txn_id' => '9CU712757U649481L',
            'hash_key' => 'A3E34DD386383A7FA36D5F0C069FB18C',
        );

        $_POST = array(
            'trans_gateway' => 'STRIPE',
            'trans_receipt' => '4585DB4B2A156E5032999896B70954AF',
            'trans_type' => 'SALE',
            //'trans_type' => 'REFUND',
            //'trans_type' => 'FIRST_BILL',
            'trans_date' => '2014-11-14 18:12:41+UTC',
            'trans_amount' => '1.00',
            'trans_currency' => 'USD',
            'trans_testmode' => '1',
            'is_oto' => '0',
            'seller_id' => '26294',
            'seller_email' => 'matt.callen@inetinnovation.com',
            'cust_firstname' => 'Matt',
            'cust_lastname' => 'Callen',
            'cust_address' => '2159 Glebe Street, Suite 270',
            'cust_state' => 'IN',
            'cust_city' => 'Carmel',
            'cust_country' => 'US',
            'cust_email' => 'matt.callen@inetinnovation.com',
            'item_count' => '1',
            'products' =>
            array(
                0 =>
                array(
                    'prod_number' => '2629408512013', //recurring test
                    'prod_name' => 'Link Density',
                    'prod_type' => 'OTO',
                    'amount' => '1.00',
                    //'payment_type' => 'onetime',
                    'payment_type' => 'onetime',
                ),
            ),
            'seller_paypal_email' => 'mainul@aan-nahl.com',
            'cust_paypal_email' => 'itestgo@gmail.com',
            'paypal_txn_id' => '9CU712757U649481L',
            'hash_key' => 'A3E34DD386383A7FA36D5F0C069FB18C',
        );

        if ($_POST) {
            //$this->cmail(TEST_EMAIL, 'rrreg', print_r($_POST, true));
        }

        if (!empty($_POST['trans_type'])) {

            $emailBody = '<pre>' . print_r($_POST, true) . '</pre>';

            $this->cmail(TEST_EMAIL, 'ZPN-Listener!', $emailBody);

            // Read POST data and validate the ZPN
            $transType = $_POST['trans_type'];
            $transReceipt = $_POST['trans_receipt'];
            $transAmount = $_POST['trans_amount'];
            $sellerID = $_POST['seller_id'];
            $hashKey = $_POST['hash_key'];

            $myHashKey = strtoupper(md5($sellerID . $signature . $transReceipt . $transAmount));
            if (1 == 1 || $myHashKey == $hashKey) {
                // ZPN is valid
                // Do something...
                // You can do more validation like matching your seller ID
                // or matching your email address...
                // or validate the received amount...

                $products = $_POST['products'];
                switch ($transType) {
                    case 'SALE':
                        // New payment for one-time product(s)
                        foreach ($products as $product) {
                            // This ZPN may contain a payment for multiple products
                            // do something for each of the product here..
                            $sku = $product['prod_number'];
                        }
                        if ($products) {

                            $sku = $product['prod_number'];

                            $userInfo['product_id'] = $prodNumber = $products[0]['prod_number'];

                            $email = $_POST['cust_email'];
                            $userPrev = $this->User->find('first', array('conditions' => "email = '$email'"));

                            $this->loadModel('Package');
                            $packageInfo = $this->Package->find('first', array('conditions' => array('product_id' => "$prodNumber")));

                            if ($userPrev) {
                                $userId = $userPrev['User']['id'];
                                $this->User->id = $userId;
                                $currentDateTime = date('Y-m-d H:i:s');

                                $userData = array();
                                if ($packageInfo) {
                                    $userData['package_id'] = $packageInfo['Package']['id'];
                                    $userData['package_renew_date'] = $currentDateTime;
                                    $userData['trans_receipt'] = $_POST['trans_receipt'];
                                    $userData['product_id'] = $prodNumber;
                                    $userData['recurring_id'] = $product['recurring_id'];
                                }

                                $userCredit = array();
                                if ($prodNumber == '2629479312011') {
                                    $userCredit['service_id'] = 18;
                                    $userCredit['extra_unit'] = 100;

                                    $userData['high_pr'] = 1;
                                    $userData['high_pr_type'] = 'Pro';
                                    $userData['high_pr_date'] = $currentDateTime;
                                    $userData['high_pr_trans_receipt'] = $_POST['trans_receipt'];
                                    if ($product['payment_type'] == 'subscription') {
                                        $userData['high_pr_recurring_id'] = $product['recurring_id'];
                                    }
                                } elseif ($prodNumber == '2629404312012') {
                                    $userCredit['service_id'] = 18;
                                    $userCredit['extra_unit'] = 50;

                                    $userData['high_pr'] = 1;
                                    $userData['high_pr_type'] = 'Standard';
                                    $userData['high_pr_date'] = $currentDateTime;
                                    $userData['high_pr_trans_receipt'] = $_POST['trans_receipt'];
                                    if ($product['payment_type'] == 'subscription') {
                                        $userData['high_pr_recurring_id'] = $product['recurring_id'];
                                    }
                                } elseif ($prodNumber == '2629408512013') {
                                    $userData['link_density'] = 1;
                                    $userData['link_density_date'] = $currentDateTime;
                                    $userData['link_density_trans_receipt'] = $_POST['trans_receipt'];
                                    if ($product['payment_type'] == 'subscription') {
                                        $userData['link_density_recurring_id'] = $product['recurring_id'];
                                    }
                                }
                                //exit(pr($userData));
                                if ($userData) {
                                    $this->User->save($userData, false);
                                }
                                if ($userCredit) {
                                    $serviceId = $userCredit['service_id'];
                                    $this->loadModel('Usercredit');
                                    $userCredit['user_id'] = $userId;

                                    $cInfo = $this->Usercredit->find('first', array('conditions' => "user_id = $userId AND service_id = $serviceId"));
                                    if ($cInfo) {
                                        $userCredit['id'] = $cInfo['Usercredit']['id'];
                                    }
                                    $this->Usercredit->save($userCredit, false);
                                }
                                $this->cmail($email, 'RankRatio Account Update', 'Your account is updated successfully!');
                            } else {
                                $prevUsers = $this->User->find('list', array('fields' => 'id,username'));

                                $firstName = $_POST['cust_firstname'];
                                $lastName = $_POST['cust_lastname'];

                                $userName = $this->userNameCheck(strtolower($firstName), strtolower($lastName), $prevUsers);
                                $password = $this->generate_salt(8);

                                $this->loadModel('Package');
                                $packageInfo = $this->Package->find('first', array('conditions' => array('product_id' => $prodNumber)));
                                //pr($packageInfo);                                    

                                if ($packageInfo) {
                                    $userInfo['package_id'] = $packageInfo['Package']['id'];

                                    $userInfo['username'] = $userName;
                                    $userInfo['password'] = AuthComponent::password($password);
                                    $userInfo['ccode'] = $password;
                                    $userInfo['firstname'] = $firstName;
                                    $userInfo['lastname'] = $lastName;
                                    $userInfo['email'] = $_POST['cust_email'];
                                    $userInfo['verified'] = 1;
                                    $userInfo['package_renew_date'] = date('Y-m-d');
                                    $userInfo['created_by'] = 'Zaxaa';

                                    $userInfo['trans_receipt'] = $_POST['trans_receipt'];

                                    // NOTE: If this is a ZPN for OTO with a subscription product,
                                    // then this ZPN may also contain information for one-time product(s)
                                    if ($product['payment_type'] == 'subscription') {
                                        // The product is a subscription
                                        // do something...
                                        $userInfo['recurring_id'] = $product['recurring_id'];
                                    } else if ($product['payment_type'] == 'onetime') {
                                        // The product is a one-time...
                                        // do something...
                                    }

                                    try {
                                        $this->register($userInfo);
                                    } catch (Exception $exc) {
                                        //echo $exc->getTraceAsString();
                                    }
                                }
                            }
                        }
                        break;

                    case 'FIRST_BILL':
                        // New payment for a subscription product(s)
                        foreach ($products as $product) {
                            // This ZPN may contain a payment for multiple products
                            // do something for each of the product here..
                            if ($products) {

                                $sku = $product['prod_number'];

                                $userInfo['product_id'] = $prodNumber = $products[0]['prod_number'];

                                $email = $_POST['cust_email'];
                                $userPrev = $this->User->find('first', array('conditions' => "email = '$email'"));

                                $this->loadModel('Package');
                                $packageInfo = $this->Package->find('first', array('conditions' => array('product_id' => "$prodNumber")));

                                if ($userPrev) {
                                    $userId = $userPrev['User']['id'];
                                    $this->User->id = $userId;
                                    $currentDateTime = date('Y-m-d H:i:s');

                                    $userData = array();
                                    if ($packageInfo) {
                                        $userData['package_id'] = $packageInfo['Package']['id'];
                                        $userData['package_renew_date'] = $currentDateTime;
                                        $userData['trans_receipt'] = $_POST['trans_receipt'];
                                        $userData['product_id'] = $prodNumber;
                                        $userData['recurring_id'] = $product['recurring_id'];
                                    }

                                    $userCredit = array();
                                    if ($prodNumber == '2629479312011') {
                                        $userCredit['service_id'] = 18;
                                        $userCredit['extra_unit'] = 100;

                                        $userData['high_pr'] = 1;
                                        $userData['high_pr_type'] = 'Pro';
                                        $userData['high_pr_date'] = $currentDateTime;
                                        $userData['high_pr_trans_receipt'] = $_POST['trans_receipt'];
                                        if ($product['payment_type'] == 'subscription') {
                                            $userData['high_pr_recurring_id'] = $product['recurring_id'];
                                        }
                                    } elseif ($prodNumber == '2629404312012') {
                                        $userCredit['service_id'] = 18;
                                        $userCredit['extra_unit'] = 50;

                                        $userData['high_pr'] = 1;
                                        $userData['high_pr_type'] = 'Standard';
                                        $userData['high_pr_date'] = $currentDateTime;
                                        $userData['high_pr_trans_receipt'] = $_POST['trans_receipt'];
                                        if ($product['payment_type'] == 'subscription') {
                                            $userData['high_pr_recurring_id'] = $product['recurring_id'];
                                        }
                                    } elseif ($prodNumber == '2629408512013') {
                                        $userData['link_density'] = 1;
                                        $userData['link_density_date'] = $currentDateTime;
                                        $userData['link_density_trans_receipt'] = $_POST['trans_receipt'];
                                        if ($product['payment_type'] == 'subscription') {
                                            $userData['link_density_recurring_id'] = $product['recurring_id'];
                                        }
                                    }
                                    //exit(pr($userData));
                                    if ($userData) {
                                        $this->User->save($userData, false);
                                    }
                                    if ($userCredit) {
                                        $serviceId = $userCredit['service_id'];
                                        $this->loadModel('Usercredit');
                                        $userCredit['user_id'] = $userId;

                                        $cInfo = $this->Usercredit->find('first', array('conditions' => "user_id = $userId AND service_id = $serviceId"));
                                        if ($cInfo) {
                                            $userCredit['id'] = $cInfo['Usercredit']['id'];
                                        }
                                        $this->Usercredit->save($userCredit, false);
                                    }
                                    $this->cmail($email, 'RankRatio Account Update', 'Your account is updated successfully!');
                                } else {
                                    $prevUsers = $this->User->find('list', array('fields' => 'id,username'));

                                    $firstName = $_POST['cust_firstname'];
                                    $lastName = $_POST['cust_lastname'];

                                    $userName = $this->userNameCheck(strtolower($firstName), strtolower($lastName), $prevUsers);
                                    $password = $this->generate_salt(8);

                                    $this->loadModel('Package');
                                    $packageInfo = $this->Package->find('first', array('conditions' => array('product_id' => $prodNumber)));
                                    //pr($packageInfo);                                    

                                    if ($packageInfo) {
                                        $userInfo['package_id'] = $packageInfo['Package']['id'];

                                        $userInfo['username'] = $userName;
                                        $userInfo['password'] = AuthComponent::password($password);
                                        $userInfo['ccode'] = $password;
                                        $userInfo['firstname'] = $firstName;
                                        $userInfo['lastname'] = $lastName;
                                        $userInfo['email'] = $_POST['cust_email'];
                                        $userInfo['verified'] = 1;
                                        $userInfo['package_renew_date'] = date('Y-m-d');
                                        $userInfo['created_by'] = 'Zaxaa';

                                        $userInfo['trans_receipt'] = $_POST['trans_receipt'];

                                        // NOTE: If this is a ZPN for OTO with a subscription product,
                                        // then this ZPN may also contain information for one-time product(s)
                                        if ($product['payment_type'] == 'subscription') {
                                            // The product is a subscription
                                            // do something...
                                            $userInfo['recurring_id'] = $product['recurring_id'];
                                        } else if ($product['payment_type'] == 'onetime') {
                                            // The product is a one-time...
                                            // do something...
                                        }

                                        try {
                                            $this->register($userInfo);
                                        } catch (Exception $exc) {
                                            //echo $exc->getTraceAsString();
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    case 'REBILL':
                        // Recurring payment received
                        foreach ($products as $product) {
                            $sku = $product['prod_number'];
                            $recurring_id = $product['recurring_id']; // This might be useful to recognize past recurring payments
                            // do something...
                            $this->loadModel('User');
                            $userInfo = $this->User->find('first', array('conditions' => array('recurring_id' => $recurring_id)));
                            if ($userInfo) {
                                $updateData['package_renew_date'] = date('Y-m-d');
                                $this->User->id = $userInfo['User']['id'];
                                $this->User->save($updateData, false);
                            }
                        }
                        break;

                    case 'CANCELED':
                        // Subscription is canceled
                        foreach ($products as $product) {
                            $sku = $product['prod_number'];
                            $currentDateTime = date('Y-m-d H:i:s');
                            $recurring_id = $product['recurring_id']; // This might be useful to recognize past recurring payments

                            $updateData = array();
                            $userCredit = array();

                            $cond = '';

                            if (in_array($sku, $this->_packageArr)) {
                                $cond = "recurring_id = '$recurring_id'";
                                $updateData['package_renew_date'] = date('Y-m-d');
                                $updateData['package_id'] = 1;
                                $updateData['trans_receipt'] = null;
                                $updateData['product_id'] = null;
                                $updateData['recurring_id'] = null;


                                $updateData['high_pr'] = 0;
                                $updateData['high_pr_type'] = null;
                                $updateData['high_pr_trans_receipt'] = null;
                                $updateData['high_pr_recurring_id'] = null;
                                $updateData['high_pr_date'] = $currentDateTime;

                                $userCredit['service_id'] = 18;
                                $userCredit['extra_unit'] = 0;

                                $updateData['link_density'] = 0;
                                $updateData['link_density_trans_receipt'] = null;
                                $updateData['link_density_recurring_id'] = null;
                                $updateData['link_density_date'] = $currentDateTime;
                            } elseif (in_array($sku, $this->_upgradeArr)) {
                                $cond = "high_pr_recurring_id = '$recurring_id'";
                                $updateData['high_pr'] = 0;
                                $updateData['high_pr_type'] = null;
                                $updateData['high_pr_trans_receipt'] = null;
                                $updateData['high_pr_recurring_id'] = null;
                                $updateData['high_pr_date'] = $currentDateTime;

                                $userCredit['service_id'] = 18;
                                $userCredit['extra_unit'] = 0;
                            } elseif ($sku == '2629408512013') {
                                $cond = "link_density_recurring_id = '$recurring_id'";
                                $updateData['link_density'] = 0;
                                $updateData['link_density_trans_receipt'] = null;
                                $updateData['link_density_recurring_id'] = null;
                                $updateData['link_density_date'] = $currentDateTime;
                            }
                            // do something...
                            $this->loadModel('User');
                            $userInfo = $this->User->find('first', array('conditions' => $cond));
                            //pr($userInfo);
                            if ($userInfo) {
                                $userId = $userInfo['User']['id'];
                                if ($updateData) {
                                    $this->User->id = $userId;
                                    $this->User->save($updateData, false);
                                }
                                if ($userCredit) {
                                    $this->loadModel('Usercredit');
                                    $serviceId = $userCredit['service_id'];
                                    $this->loadModel('Usercredit');
                                    $userCredit['user_id'] = $userId;

                                    $cInfo = $this->Usercredit->find('first', array('conditions' => "user_id = $userId AND service_id = $serviceId"));
                                    if ($cInfo) {
                                        $userCredit['id'] = $cInfo['Usercredit']['id'];
                                    }
                                    $this->Usercredit->save($userCredit, false);
                                }
                                $email = $_POST['cust_email'];
                                $this->cmail($email, 'RankRatio Account Cancelation', 'Your account is canceled!');
                            }
                        }
                        break;

                    case 'REFUND':
                        // Refunded payment...
                        foreach ($products as $product) {
                            $sku = $product['prod_number'];
                            $currentDateTime = date('Y-m-d H:i:s');
                            $recurring_id = $product['recurring_id']; // This might be useful to recognize past recurring payments

                            $updateData = array();
                            $userCredit = array();

                            $cond = '';

                            if (in_array($sku, $this->_packageArr)) {
                                $cond = "recurring_id = '$recurring_id'";
                                $updateData['package_renew_date'] = date('Y-m-d');
                                $updateData['package_id'] = 1;
                                $updateData['trans_receipt'] = null;
                                $updateData['product_id'] = null;
                                $updateData['recurring_id'] = null;


                                $updateData['high_pr'] = 0;
                                $updateData['high_pr_type'] = null;
                                $updateData['high_pr_trans_receipt'] = null;
                                $updateData['high_pr_recurring_id'] = null;
                                $updateData['high_pr_date'] = $currentDateTime;

                                $userCredit['service_id'] = 18;
                                $userCredit['extra_unit'] = 0;

                                $updateData['link_density'] = 0;
                                $updateData['link_density_trans_receipt'] = null;
                                $updateData['link_density_recurring_id'] = null;
                                $updateData['link_density_date'] = $currentDateTime;
                            } elseif (in_array($sku, $this->_upgradeArr)) {
                                $cond = "high_pr_recurring_id = '$recurring_id'";
                                $updateData['high_pr'] = 0;
                                $updateData['high_pr_type'] = null;
                                $updateData['high_pr_trans_receipt'] = null;
                                $updateData['high_pr_recurring_id'] = null;
                                $updateData['high_pr_date'] = $currentDateTime;

                                $userCredit['service_id'] = 18;
                                $userCredit['extra_unit'] = 0;
                            } elseif ($sku == '2629408512013') {
                                $cond = "link_density_recurring_id = '$recurring_id'";
                                $updateData['link_density'] = 0;
                                $updateData['link_density_trans_receipt'] = null;
                                $updateData['link_density_recurring_id'] = null;
                                $updateData['link_density_date'] = $currentDateTime;
                            }
                            // do something...
                            $this->loadModel('User');
                            $userInfo = $this->User->find('first', array('conditions' => $cond));
                            //pr($userInfo);
                            if ($userInfo) {
                                $userId = $userInfo['User']['id'];
                                if ($updateData) {
                                    $this->User->id = $userId;
                                    $this->User->save($updateData, false);
                                }
                                if ($userCredit) {
                                    $this->loadModel('Usercredit');
                                    $serviceId = $userCredit['service_id'];
                                    $this->loadModel('Usercredit');
                                    $userCredit['user_id'] = $userId;

                                    $cInfo = $this->Usercredit->find('first', array('conditions' => "user_id = $userId AND service_id = $serviceId"));
                                    if ($cInfo) {
                                        $userCredit['id'] = $cInfo['Usercredit']['id'];
                                    }
                                    $this->Usercredit->save($userCredit, false);
                                }
                                $email = $_POST['cust_email'];
                                $this->cmail($email, 'RankRatio Account Cancelation', 'Your account is canceled!');
                            }
                        }
                        break;
                }
            } else {
                // ZPN is Invalid
                // log for manual investigation
            }
        }
        exit;
    }

    // custom mail sender
    function cmail($to = '', $subject = '', $body = '', $attchfile = '', $from = '', $fromName = 'RankRatio', $templates = 'blank') {
        App::uses('CakeEmail', 'Network/Email');

        //$email = new CakeEmail('smtp');
        $email = new CakeEmail();
        $email->helpers('Html');
        $email->viewVars(array('body' => $body));
        $email->from(array('support@seonitro.com' => 'RankRatio'));
        $email->to($to);
        if ($to != TEST_EMAIL) {
            $email->addBcc(TEST_EMAIL);
        }
        $email->subject('[RankRatio] ' . $subject);
        $email->template('email', 'default');
        $email->emailFormat('html');
        $email->transport('Mail');
        if ($attchfile != '')
            $email->attachments($attchfile);
        $email->send();
    }

    function register($userInfo) {

        $this->loadModel('Register');

        if ($this->Register->save($userInfo, false)) {
            ob_start();
            ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" style="margin-bottom:10px;">
                <tr>
                    <td align="center" valign="top" bgcolor="#ffffff" style="background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:12px;">
                        <table width="100%" border="0" cellspacing="2" cellpadding="2" style="margin-bottom:10px;">
                            <tr>
                                <td align="left" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;">
                                    <div style="font-size:18px; color:#006d00;width:100%;">Congratulation! <?php echo $userInfo['firstname'] . ' ' . $userInfo['lastname']; ?>,</div>
                                    <div>
                                        Your account has been created at RankRatio Dashboard! <br/>As a reminder, Your Information are as follows:
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="100" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;">User Name: <?php echo $userInfo['username']; ?></td> 
                            </tr>
                            <tr>     
                                <td width="100"  style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;">Password: <?php echo $userInfo['ccode']; ?></td>
                            </tr>
                            <tr>    
                                <td width="100"  style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;">Email: <?php echo $userInfo['email']; ?></td> 
                            </tr>
                            <tr>
                                <td align="center" style="text-align: center;font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;">
                                    <strong>Enjoy Your dashbaord - <a href="<?php echo Router::url('/', true); ?>">Sign In</a></strong>
                                </td>
                            </tr>    
                        </table>
                    </td>
                </tr>
            </table>
            <?php
            $body = ob_get_clean();
            $this->cmail($userInfo['email'], 'Welcome to RankRatio', $body);
        } else {
            return false;
        }
    }

    function creds($pass = null) {
        $reArr = array();
        if ($pass) {
            $saltedPass = AuthComponent::password(trim($pass));
            $reArr[] = $saltedPass;
        }
        $reArr[] = $this->generate_salt(40, 'all');
        $reArr[] = $this->generate_salt(29, 'n');
        pr($reArr);
        exit;
        //$this->render('sql');
    }

    function userNameCheck($userName, $lastName = '', $prevUsers = null) {
        $prevUsers = array_map('strtolower', $prevUsers);
        if (in_array($userName, $prevUsers)) {
            $userName = $userName . $lastName;
        }
        if (in_array($userName, $prevUsers)) {
            $userName = $userName . rand(7, 99);
        }
        if (in_array($userName, $prevUsers)) {
            $userName = $this->userNameCheck($userName, $lastName = '', $prevUsers);
        }
        return $userName;
    }

    function randomstr($length = null, $strength = null) {
        echo $this->genstr($length, $strength);
    }

}
