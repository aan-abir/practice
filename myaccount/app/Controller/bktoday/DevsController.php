<?php
App::uses('AppController', 'Controller');

if (!defined('TEST_EMAIL')) {
    //define('TEST_EMAIL', 'itestgo@aan-nahl.com');
    define('TEST_EMAIL', 'abir@aan-nahl.com');
}

Configure::write('debug', 2);
Configure::write('Cache.disable', true);

set_time_limit(7200);
ini_set('memory_limit', '512M');
ini_set('max_execution_time', 7200);
ini_set('max_input_time', 7200);

class DevsController extends AppController
{

    private $_cMetaKey = '_nitro_dcl_cmeta_key';
    private $_dclResultsName = '_nitro_dcl_results_name';
    private $_dclResultsValueKey = '_nitro_dcl_results_value_key';
    private $_dclDResults = null;
    private $_dclAutoPostKey = '_dcl_auto_post_key';
    private $_globalArr = null;

    public function beforeFilter()
    {
        //parent::beforeFilter();

        if (defined('CRON_DISPATCHER')) {
            //$this->Auth->allow($alllowed);
        } else {
            //$this->Auth->allow($alllowed);
        }
        $this->Auth->allow();
        $this->layout = null;
        $this->autoRender = false;
    }

    function beforeRender()
    {

    }

    public function test()
    {
        $this->cmail(TEST_EMAIL, 'Hello Dear!', 'This is message!');
        exit;
    }

    function nitro_get_stats($domain = null)
    {
        $this->loadModel('Nitrocamp');
        $this->Nitrocamp->getCampaignStats($domain);
    }

    function nitro_get_stats_private($domain = null)
    {
        $this->loadModel('Nitrocamp');
        $this->Nitrocamp->getCampaignStatsPrivate($domain);
    }

    private function getHowManyToPostToday($totalCount, $startTime, $endTime, $postDate = null, $postedCount = 0)
    {

        $daysArr = array();

        $daySpan = $endTime - $startTime;
        $daySpan = $daySpan > 0 ? $daySpan : 1;

        if ($daySpan >= 0) {
            $dayCount = $daySpan / (3600 * 24);
            if ($dayCount > $totalCount) {
                $dayCount = $totalCount;
            }
            for ($j = 0; $j <= $dayCount; $j++) {
                $dayToTime = $startTime + ($j * 3600 * 24);
                $eachDate = date('Y-m-d', $dayToTime);
                $daysArr[$j + 1]['date'] = $eachDate;
                $daysArr[$j + 1]['post_today'] = 0;
            }

            //$totalCount = 130;

            if ($daysArr) {
                $totalDays = count($daysArr);
                $perDay = floor($totalCount / $totalDays);
                $post_sofar = 0;
                foreach ($daysArr as $key => $day) {
                    $post_sofar += $perDay;
                    $daysArr[$key]['post_today'] = $perDay;
                    //$daysArr[$key]['post_sofar'] = $post_sofar;
                }
                if ($post_sofar < $totalCount) {
                    $remCount = ($totalCount - $post_sofar);
                    for ($j = 1; $j <= $remCount; $j++) {
                        $daysArr[$j]['post_today'] += 1;
                    }
                }
                $fpost_sofar = 0;
                foreach ($daysArr as $fkey => $fday) {
                    $fpost_sofar += $fday['post_today'];
                    $daysArr[$fkey]['post_sofar'] = $fpost_sofar;
                }
            }

            if ($postDate) {
                $postDate = date('Y-m-d', strtotime($postDate));
                $reCount = 0;
                foreach ($daysArr as $dA) {
                    if ($dA['date'] == $postDate) {
                        if (($postedCount + $dA['post_today']) > $dA['post_sofar']) {
                            $reCount = ($dA['post_sofar'] - $postedCount);
                        } elseif (($postedCount + $dA['post_today']) <= $dA['post_sofar']) {
                            $reCount = $dA['post_today'];
                        }
                    }
                }
                return $reCount;
            } else {
                return $daysArr;
            }
        }
    }

    private function spinContent($txt)
    {
        $pattern = '#\{([^{}]*)\}#msi';
        $test = preg_match_all($pattern, $txt, $out);
        if (!$test)
            return $txt;
        $atrouver = array();
        $aremplacer = array();
        foreach ($out[0] as $id => $match) {
            $choisir = explode("|", $out[1][$id]);
            $atrouver[] = trim($match);
            $aremplacer[] = trim($choisir[rand(0, count($choisir) - 1)]);
        }
        $reponse = str_replace($atrouver, $aremplacer, $txt);
        return $this->spinContent($reponse);
    }

    function nitro_create_user()
    {
        App::import('Vendor', 'IXR_Library');
        $fullRpcDomain = 'http://www.25kw.info/nitro/nitroserver.php';
        $client = new IXR_Client($fullRpcDomain);
        $params['domain'] = 'alejandrorivasfoto.com';
        $params['user_login'] = 'admin_demo';
        $params['user_pass'] = 'surfin42';
        $params['user_email'] = 'admin_demo@seonitro.com';
        $params['user_nicename'] = 'Admin Demo';
        $response = array();
        if (!$client->query('nitro.createUser', $params)) {
            $response[] = 'Error: ' . $client->getErrorCode() . " : " . $client->getErrorMessage();
        } else {
            pr($response);
            if ($client->getResponse() === true) {
                $response = array('success');
            }
        }
        pr($response);
        exit;
    }

    function nitro_monthly_renewal()
    {

        echo 'here<br/>';

        Configure::write('debug', 2);

        //$this->cmail(TEST_EMAIL, 'Monthly Renewal', 'Renewal success!');

        $this->loadModel('User');
        $this->loadModel('Usercredit');

        $cond = array(
            'fields' => "id,package_id,package_renew_date",
            'conditions' => "User.role = 'client'"
        );
        $userList = $this->User->find('all', $cond);

        $nowDate = date('Y-m-d');
        $currentDateTimeStamp = strtotime(date('Y-m-d'));

        $excludeService = array(1, 2, 9, 12);

        if ($userList) {

            $this->loadModel('Monthlyrenewal');
            $this->loadModel('Usercredit');

            foreach ($userList as $user) {
                $userId = $user['User']['id'];

                // renew date for each user from database
                $renewDate = $user['User']['package_renew_date'];
                $newRenewDate = date('Y-m-d', strtotime($user['User']['package_renew_date']) + 24 * 3600 * 29);

                $renewDateTimeStamp = strtotime(date('Y-m-d', strtotime($user['User']['package_renew_date'])));
                $days_between = floor(abs($currentDateTimeStamp - $renewDateTimeStamp) / 7200);

                //check whether 29 days have past
                if (($days_between) > 29) {
                    pr(array($userId => $days_between));

                    $rCond = array(
                        'conditions' => "Monthlyrenewal.user_id = $userId AND Monthlyrenewal.renew_date = '$renewDate'"
                    );
                    $alreadyRenewed = $this->Monthlyrenewal->find('all', $rCond);

                    pr($alreadyRenewed);

                    if (1 == 1) {
                        //echo '_here_';
                        //update the renew date in user table
                        $this->User->id = $userId;
                        if ($this->User->saveField('package_renew_date', $newRenewDate)) {

                            $userCreditsAll = $this->Usercredit->find('all', array('conditions' => "user_id = $userId"));

                            /* insert a record in monthlyrenewal table to track whether already renewed */
                            $monthlyReData['user_id'] = $userId;
                            $monthlyReData['renew_date'] = $newRenewDate;
                            $monthlyReData['prev_usage'] = serialize($userCreditsAll);
                            $this->Monthlyrenewal->create();
                            $this->Monthlyrenewal->save($monthlyReData);
                            /* insert record ends */

                            /* reset service used to 0 except for mysites and manual sites */

                            $userCredit = array();
                            foreach ($userCreditsAll as $k => $uCAll) {
                                $serviceId = $uCAll['Usercredit']['service_id'];
                                if (!in_array($serviceId, $excludeService)) {
                                    $userCredit['id'] = $uCAll['Usercredit']['id'];
                                    $userCredit['user_id'] = $userId;
                                    $userCredit['service_id'] = $serviceId;
                                    $userCredit['service_used'] = 0;
                                    @$this->Usercredit->save($userCredit);
                                }
                            }
                            pr(array($userId => $days_between));
                            /* reset service ends */
                        }
                    }
                }
            }
        }
        $this->cmail(TEST_EMAIL, 'Monthly Renewal', 'Renewal success!');
    }

    function update912()
    {
        $this->loadModel('Usercredit');
        $this->loadModel('Campaign');
        $this->loadModel('Blogroll');
        $this->loadModel('Manualsharedsite');

        $all912 = $this->Usercredit->find('all', array('conditions' => "Usercredit.service_id IN(2,9,12)"));

        if ($all912) {
            foreach ($all912 as $value) {
                $arr = $value['Usercredit'];
                $uId = $arr['id'];
                $userId = $arr['user_id'];
                if ($arr['service_id'] == 9) {
                    $uCamp = $this->Campaign->find('list', array('conditions' => "Campaign.user_id = $userId", 'fields' => "id,id"));

                    if ($uCamp) {
                        /**/
                        $allCampIdsString = implode(',', $uCamp);
                        $blogRollCount = $this->Blogroll->find('count', array('conditions' => "Blogroll.campaign_id IN($allCampIdsString)"));
                        $this->Usercredit->id = $uId;
                        $this->Usercredit->saveField('service_used', $blogRollCount);
                        echo '<br/>';
                        /* */
                    }
                    //pr($uCamp);
                } elseif ($arr['service_id'] == 12) {
                    $sUsed12 = $this->Campaign->find('count', array('conditions' => "Campaign.user_id = $userId AND custom = 0"));
                    $this->Usercredit->id = $uId;
                    $this->Usercredit->saveField('service_used', $sUsed12);
                    //echo '<br/>';
                } elseif ($arr['service_id'] == 2) {
                    $sUsed2 = $this->Manualsharedsite->find('count', array('conditions' => "Manualsharedsite.user_id = $userId"));
                    $this->Usercredit->id = $uId;
                    $this->Usercredit->saveField('service_used', $sUsed2);
                    echo '<br/>';
                }
            }
        }
        pr($all912);

        exit;
    }

    function process_instant_ay($ids = null)
    {
        $this->loadModel('Rankinstant');

        $this->Rankinstant->process_amazon_youtube($ids);
    }

    function update_domain_title()
    {
        $this->loadModel('Domain');
        $this->loadModel('Nitro');

        $allDomains = $this->Domain->find('all', array('fields' => "id,domain,title"));

        $count = 0;
        if ($allDomains) {
            foreach ($allDomains as $dom) {
                $domain = $dom['Domain']['domain'];
                $domainId = $dom['Domain']['id'];

                //$dbInfoSingle = $this->Nitro->nitroDbInfo(array($domain));
                $dbInfoSingle = $this->Nitro->nitroDbInfoNew(array($domain));
                if ($dbInfoSingle) {
                    $siteTitle = $dbInfoSingle[$domain]['SITE_TITLE'];

                    $this->Domain->id = $domainId;
                    $updateData['title'] = $siteTitle;
                    $this->Domain->save($updateData, false);
                    $count++;
                }
            }
        }
        echo $count;
    }

    function prreach_remote()
    {
        $this->loadModel('Pressrelease');
        $this->loadModel('Commonmodel');

        $cond = array(
            'conditions' => "Pressrelease.status = 'active'"
        );
        $pressReleaseList = $this->Pressrelease->find('all', $cond);

        //pr($pressReleaseList);
        //exit;

        $fileFieldsArr[] = 'image_1_file';
        $fileFieldsArr[] = 'image_2_file';
        $fileFieldsArr[] = 'image_3_file';
        $fileFieldsArr[] = 'image_4_file';
        $fileFieldsArr[] = 'attachment_1_file';
        $fileFieldsArr[] = 'attachment_2_file';
        $fileFieldsArr[] = 'attachment_3_file';
        $fileFieldsArr[] = 'attachment_4_file';

        $mailArr = array();
        if ($pressReleaseList) {
            $remoteArr = array();

            $count = 0;
            foreach ($pressReleaseList as $key => $pRelease) {
                $remoteArr = $pRelease['Pressrelease'];
                foreach ($fileFieldsArr as $fileField) {
                    if ($remoteArr[$fileField]) {
                        $remoteArr[$fileField] = BASEURL . 'press_release/' . $remoteArr[$fileField];
                    }
                }
                $remoteArr['callback_url'] = BASEURL . 'devs/prreach_remote_callback/' . $remoteArr['id'];
                $remoteArr['industry_label'] = $this->Pressrelease->getIndustryList($remoteArr['industry']);
                $remoteArr['tracking_id'] = $mailArr = $remoteArr['id'];
                unset($remoteArr['id']);
                unset($remoteArr['user_id']);
                unset($remoteArr['status']);
                unset($remoteArr['created']);
                unset($remoteArr['modified']);
                unset($remoteArr['status_msg']);

                echo '<pre>';
                //var_export($remoteArr);

                $url = 'https://www.prreach.com/api/receive_post_seo.php';
                $prReturn = $this->Commonmodel->_request($url, 'POST', $remoteArr);


                echo '<br/>';
                echo 'Result returned:';
                echo '<br/>';
                pr($prReturn);

                if (isset($prReturn['result']) && $prReturn['result']) {
                    $this->Pressrelease->id = $remoteArr['tracking_id'];
                    $this->Pressrelease->saveField('status', 'inprocess');
                }
                $count++;
            }
        }
        $this->cmail(TEST_EMAIL, 'PRREACH_POSTED', print_r($mailArr, true) . ' TOTAL: ' . count($mailArr));

        //pr($pressReleaseList);
    }

    function prreach_remote_callback($itemId)
    {
        $this->loadModel('Pressrelease');

        $cond = array(
            'conditions' => "Pressrelease.status = 'inprocess'"
        );
        $pressReleaseList = $this->Pressrelease->find('all', $cond);

        pr($pressReleaseList);

        if ($_POST) {

        } else {
            $_POST = array();
        }
        $this->cmail(TEST_EMAIL, 'PRREACH', print_r($_POST, true) . ' ITEM ID: ' . $itemId);
    }

    function prreach_remote_status($itemId = null)
    {

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->loadModel('Pressrelease');
        $this->loadModel('Commonmodel');

        $itemClause = '';
        if ($itemId) {
            $itemClause = "AND id = $itemId";
        }
        $cond = array(
            'conditions' => "Pressrelease.status NOT IN('draft','active','trash','publish') $itemClause"
        );
        $pressReleaseList = $this->Pressrelease->find('all', $cond);

        $mailArr = array();
        if ($pressReleaseList) {
            foreach ($pressReleaseList as $key => $pRelease) {
                $pArr = $pRelease['Pressrelease'];
                $remoteUrl = 'https://www.prreach.com/api/check_status_seo.php?tracking_id=' . $pArr['id'];
                $prReturn = $this->Commonmodel->_request($remoteUrl, 'GET');

                if (isset($prReturn['result']) && $prReturn['result']) {
                    $result = json_decode($prReturn['result']);
                    $updateArr['id'] = $pArr['id'];
                    $updateArr['status'] = $result->status;
                    $updateArr['release_url'] = (isset($result->url) && $result->url) ? $result->url : null;
                    $updateArr['status_msg'] = (isset($result->notes) && $result->notes) ? $result->notes : null;
                    $mailArr[$key] = $updateArr;
                    pr($updateArr);
                    $this->Pressrelease->save($updateArr, false);
                }
            }
        }
        $this->cmail(TEST_EMAIL, 'PRREACH_STATUS_CHECKED', print_r($mailArr, true) . ' TOTAL: ' . count($mailArr));
    }

    function nitro_auto_post($duePostDateLoop = null, $userIdLoop = null, $exitLoop = null)
    {

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $autoPostLive = true;

        set_time_limit(7200);
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 7200);
        ini_set('max_input_time', 7200);
        //exit;
        $postDate = date('Y-m-d');
        $postDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d')));
        $nowTime = strtotime(date('Y-m-d'));

        $duePost = false;
        if (isset($_GET['duePost'])) {
            $duePost = true;
        }

        $duePostDate = date('Y-m-d H:i:s');

        if (isset($_GET['duePostDate'])) {
            $duePostDate = trim($_GET['duePostDate']) . ' ' . date('H:i:s');
            $dueTimeStamp = strtotime(trim($_GET['duePostDate']));
            $postDate = date('Y-m-d H:i:s', $dueTimeStamp);
            $nowTime = $dueTimeStamp;
        } elseif ($duePostDateLoop) {
            $duePostDate = trim($duePostDateLoop) . ' ' . date('H:i:s');
            $dueTimeStamp = strtotime(trim($duePostDateLoop));
            $postDate = date('Y-m-d H:i:s', $dueTimeStamp);
            $nowTime = $dueTimeStamp;
        }

        pr(array($postDate, $duePostDate));
        //exit;

        $userIdClause = '';
        if (isset($_GET['userId'])) {
            $userId = $_GET['userId'];
            if ($userId) {
                $userIdClause = "AND user_id = $userId";
            }
        } elseif ($userIdLoop) {
            $userId = $userIdLoop;
            if ($userId) {
                $userIdClause = "AND user_id = $userId";
            }
        }

        $submissionIdClause = '';
        if (isset($_GET['submissionId'])) {
            $submissionId = $_GET['submissionId'];
            if ($submissionId) {
                $submissionIdClause = "AND Submission.id = $submissionId";
            }
        }

        $this->loadModel('Submission');
        $this->loadModel('Submissionreport');
        $this->loadModel('Domain');
        $this->loadModel('Nitro');
        $this->loadModel('Nitrocamp');
        $this->loadModel('DclDomain');


        if ($autoPostLive) {
            $cond = array(
                'conditions' => "Submission.status = 1 $userIdClause $submissionIdClause"
            );
            $submissionList = $this->Submission->find('all', $cond);
            $submissionList = Set::extract('/Submission/.', $submissionList);

            //pr($submissionList); exit;
        } else {
            $submissionList[0]['id'] = 5;
            $submissionList[0]['user_id'] = 2;
            $submissionList[0]['category_id'] = 2;
            $submissionList[0]['howmanytopost'] = 1;
            $submissionList[0]['startdate'] = date('Y-m-d', strtotime("- 1 day"));
            $submissionList[0]['enddate'] = date('Y-m-d', strtotime("+ 1 day"));
            $submissionList[0]['pr_service_id'] = 3;
            $submissionList[0]['submissiontype'] = '-1';

            $submissionList[0]['post_title'] = 'This is demo title ignore!';
            $submissionList[0]['post_content'] = 'This is demo content ignore! [dcl=1] | [dcl=2] | [dcl=2] | [dcl=2] | [dcl=3]';
            $submissionList[0]['category_name'] = 'Local Test Ignore';
        }

        //pr($submissionList);
        //exit;

        $totalPosted = 0;
        $recordedCidsAll = array();
        if (is_array($submissionList) && !empty($submissionList)) {

            $this->loadModel('Any');
            $this->Any->useTable = 'article_categories';
            $allCats = $this->Any->find('list', array('fields' => 'id,name'));

            App::import('Vendor', 'IXR_Library');
            $fullRpcDomain = 'http://www.25kw.info/nitro/nitroserver.php';
            // Create the client object
            $client = new IXR_ClientSSL($fullRpcDomain);

            $SEOHosting = array();
            foreach ($submissionList as $lmt => $sList) {
                $response = array();

                $submissionId = $sList['id'];
                $userId = $sList['user_id'];
                $categoryId = $sList['category_id'];
                $howMany = intval($sList['howmanytopost']);
                $startTime = strtotime(date('Y-m-d', strtotime($sList['startdate'])));
                $endTime = strtotime(date('Y-m-d', strtotime($sList['enddate'])));

                //pr(array($nowTime, $startTime, $endTime));

                $postDistribution = $this->getHowManyToPostToday($howMany, $startTime, $endTime);

                //pr($postDistribution);
                //pr($sList);
                //exit;

                if (isset($_GET['exit']) || $exitLoop) {
                    pr($postDistribution);
                    pr($sList);
                }

                if ($autoPostLive) {
                    $submittedList = $this->Submissionreport->find('all', array('conditions' => "Submissionreport.submission_id = $submissionId"));
                } else {
                    $submittedList = array();
                }
                $submittedCount = count($submittedList);
                $remainingCount = ($howMany - $submittedCount);

                if ($remainingCount <= 0) {
                    $this->Submission->id = $submissionId;
                    $this->Submission->saveField('status', 2);
                    continue;
                }


                if ($nowTime >= $startTime && $nowTime <= $endTime) {

                    if ($autoPostLive) {
                        $submittedTillDateCount = $this->Submissionreport->find('count', array('conditions' => "Submissionreport.submission_id = $submissionId AND Submissionreport.created <= '$duePostDate'"));
                    } else {
                        $submittedTillDateCount = 0;
                    }
                    $subCount['total'] = $submittedCount;
                    $subCount['till_date'] = $submittedTillDateCount;
                    $subCount['rem_count'] = $remainingCount;

                    $howManyToPostToday = $this->getHowManyToPostToday($howMany, $startTime, $endTime, $postDate, $submittedTillDateCount);

                    $subCount['post_today'] = $howManyToPostToday;

                    if ($howManyToPostToday > $remainingCount) {
                        $howManyToPostToday = ($howManyToPostToday - $remainingCount);
                    }

                    $subCount['post_today_recounted'] = $howManyToPostToday;

                    if (isset($_GET['exit']) || $exitLoop) {
                        pr($subCount);
                        exit;
                    }

                    if ($howManyToPostToday > 0) {

                        $submittedDomainIdArr = array();
                        $submittedDomainIds = '0';
                        foreach ($submittedList as $subL) {
                            $sL = $subL['Submissionreport'];
                            $submittedDomainIdArr[] = $sL['domain_id'];
                        }
                        if ($submittedDomainIdArr) {
                            $submittedDomainIds = implode(',', $submittedDomainIdArr);
                        }

                        $prService = intval($sList['pr_service_id']);
                        $networkType = '';
                        $submissionType = intval($sList['submissiontype']);
                        if ($submissionType == 1) {
                            $networkType = " AND owner_id = $userId";
                        } else if ($submissionType == -1) {
                            $networkType = " AND owner_id = -1 AND networktype = $prService";
                        }
                        //echo $networkType;
                        //echo '<br/>';

                        if ($autoPostLive) {

                            $toDay = date('Y-m-d');
                            $domainList = $this->Domain->find('all', array(
                                'conditions' => "Domain.id NOT IN($submittedDomainIds) AND Domain.status = 1 AND expires_on > '$toDay'",
                                'order' => 'RAND()',
                                'limit' => $howManyToPostToday,
                                'fields' => 'id,domain,cms,ausername,apassword,status,hostname'
                            ));
                            $domainList = Set::extract('/Domain/.', $domainList);
                        } else {
                            $domainList[0]['id'] = 9999;
                            $domainList[0]['domain'] = 'aan-mainul/wpfresh';
                            $domainList[0]['domain'] = 'it-wh.com';
                            $domainList[0]['cms'] = 'wordpress';
                            $domainList[0]['ausername'] = 'admin3';
                            $domainList[0]['apassword'] = 'admin420';
                            $domainList[0]['status'] = 1;
                            $domainList[0]['hostname'] = 'SmartSEOHosting';
                        }

                        //pr($domainList);

                        if (isset($_GET['exit'])) {
                            //pr($domainList);
                            //exit;
                        }
                        if (!empty($domainList)) {
                            foreach ($domainList as $dList) {
                                /**
                                 * $dList['hostname'] = 'SmartSEOHosting';
                                 * $dList['domain'] = 'it-wh.com';
                                 * /* */
                                $response = array();
                                if ($dList['cms'] == 'wordpress') {
                                    $domain = $dList['domain'];
                                    $hostName = $dList['hostname'];

                                    //$dbInfo = $this->Nitro->nitroDbInfo(array($domain));
                                    $dbInfo = $this->Nitro->nitroDbInfoNew(array($domain));
                                    //pr($dbInfo);

                                    if (isset($dbInfo[$domain]['TABLE_SCHEMA'])) {
                                        $params = array();
                                        $post_title = $this->spinContent($sList['posttitle']);
                                        $post_content = $this->spinContent($sList['postcontent']);
                                        $post_content = str_replace('http:/', 'http://', $post_content);
                                        $post_content = str_replace('http:///', 'http://', $post_content);
                                        $post_content = str_replace('http:// ', 'http://', $post_content);

                                        $params['domain'] = $dList['domain'];
                                        $params['siteUrl'] = $siteUrl = $dbInfo[$domain]['SITE_URL'];
                                        $params['dbName'] = $dbInfo[$domain]['TABLE_SCHEMA'];
                                        $params['tableName'] = $dbInfo[$domain]['TABLE_PREFIX'] . 'posts';
                                        $params['tablePrefix'] = $dbInfo[$domain]['TABLE_PREFIX'];
                                        $params['category'] = $allCats[$categoryId];
                                        $params['post_title'] = $post_title;
                                        $params['post_content'] = utf8_encode(html_entity_decode($post_content));
                                        $params['post_date'] = $postDate;

                                        /**/
                                        if ($submissionType == -1) {
                                            // Create the client object
                                            $client = new IXR_ClientSSL($fullRpcDomain);
                                            if (!$client->query('nitro.autoPostCat', $params)) {
                                                $response['error'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
                                            } else {
                                                $response = $client->getResponse();

                                                if (isset($response['post_id']) && intval($response['post_id'])) {
                                                    $post_id = intval($response['post_id']);

                                                    $siteUrl = 'http://' . preg_replace('/https?\:\/\//i', '', trim($domain));

                                                    $insert_data['domain_id'] = $dList['id'];
                                                    $insert_data['post_url'] = $siteUrl . '?p=' . $post_id;
                                                    $insert_data['post_link'] = $siteUrl . '/' . $post_id . '-' . $this->sluggify($post_title);
                                                    $insert_data['submission_id'] = $sList['id'];
                                                    $insert_data['post_id'] = $post_id;
                                                    $insert_data['created'] = date('Y-m-d');
                                                    $insert_data['modified'] = date('Y-m-d');

                                                    $this->Submissionreport->create();
                                                    if ($this->Submissionreport->save($insert_data)) {
                                                        $updateDStas['domain_id'] = $dList['id'];
                                                        $updateDStas['post_id'] = $post_id;
                                                        //$recordedCids1 = $this->Nitrocamp->updateCodeFound($insert_data, $post_content);
                                                        $recordedCids1 = $this->updateNewDStats($updateDStas, $post_content);
                                                        if ($recordedCids1) {
                                                            foreach ($recordedCids1 as $rCid) {
                                                                $recordedCidsAll[] = $rCid;
                                                            }
                                                        }
                                                    }
                                                    //pr($response);
                                                    $totalPosted++;
                                                }
                                            }
                                        } else if ($submissionType == 1) {
                                            $client = new IXR_Client('http://' . $domain . '/xmlrpc.php');
                                            $ausername = $dList['ausername'];
                                            $apassword = $dList['apassword'];
                                            //$client = new IXR_Client('http://localhost/wptest/xmlrpc.php');
                                            //$ausername = 'admin';
                                            //$apassword = '123456';

                                            $content['title'] = $post_title;
                                            $content['description'] = $post_content;

                                            if (!$client->query('metaWeblog.newPost', '', $ausername, $apassword, $content, true)) {
                                                $response['error'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
                                            } else {
                                                $ID = $client->getResponse();
                                                $post_id = intval($ID);
                                                if ($post_id) {
                                                    $siteUrl = 'http://' . preg_replace('/https?\:\/\//i', '', trim($domain));
                                                    $insert_data['domain_id'] = $dList['id'];
                                                    $insert_data['post_url'] = $siteUrl . '?p=' . $post_id;
                                                    $insert_data['submission_id'] = $sList['id'];
                                                    $insert_data['post_id'] = $post_id;
                                                    $insert_data['created'] = $postDate;
                                                    $insert_data['modified'] = $postDate;

                                                    $this->Submissionreport->create();
                                                    if ($this->Submissionreport->save($insert_data)) {
                                                        $updateDStas['domain_id'] = $dList['id'];
                                                        $updateDStas['post_id'] = $post_id;
                                                        $recordedCids2 = $this->updateNewDStats($updateDStas, $post_content);
                                                        if ($recordedCids2) {
                                                            foreach ($recordedCids2 as $rCid) {
                                                                $recordedCidsAll[] = $rCid;
                                                            }
                                                        }
                                                        $response['post_id'] = $post_id;
                                                    }
                                                    //pr($response);
                                                    $totalPosted++;
                                                }
                                            }
                                        }
                                        /* */
                                    } else {
                                        $params = array();
                                        $post_title = $this->spinContent($sList['posttitle']);
                                        $post_content = $this->spinContent($sList['postcontent']);
                                        $post_content = str_replace('http:/', 'http://', $post_content);
                                        $post_content = str_replace('http:///', 'http://', $post_content);
                                        $post_content = str_replace('http:// ', 'http://', $post_content);

                                        $params[$this->_dclAutoPostKey] = '_dcl_auto_post_key';
                                        $params['post_title'] = $post_title;
                                        $params['post_content'] = utf8_encode(html_entity_decode($post_content));
                                        $params['category_name'] = $allCats[$categoryId];
                                        $params['post_date'] = date('Y-m-d');


                                        $rpcUrl = 'http://www.' . $domain . '/xmlrpc.php';

                                        $client = new IXR_ClientSSL($rpcUrl);
                                        if (!$client->query('nitro.dclRemotePost', $params)) {
                                            $response['error'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
                                        } else {
                                            $response = $client->getResponse();

                                            if (isset($response['post_id']) && intval($response['post_id'])) {

                                                $SEOHosting[] = $dList['id'] . ' : ' . $domain . ' : PostViaPHPEmbed';

                                                $siteUrl = 'http://' . preg_replace('/https?\:\/\//i', '', trim($domain));
                                                $post_id = intval($response['post_id']);
                                                $insert_data['domain_id'] = $dList['id'];
                                                $insert_data['post_url'] = $siteUrl . '?p=' . $post_id;
                                                $insert_data['post_link'] = $siteUrl . '/' . $post_id . '-' . $this->sluggify($post_title);
                                                $insert_data['submission_id'] = $sList['id'];
                                                $insert_data['post_id'] = $post_id;
                                                $insert_data['created'] = $postDate;
                                                $insert_data['modified'] = $postDate;

                                                $this->Submissionreport->create();
                                                if ($this->Submissionreport->save($insert_data)) {
                                                    $updateDStas['domain_id'] = $dList['id'];
                                                    $updateDStas['post_id'] = $post_id;
                                                    //$recordedCids1 = $this->Nitrocamp->updateCodeFound($insert_data, $post_content);
                                                    $recordedCids1 = $this->updateNewDStats($updateDStas, $post_content);
                                                    if ($recordedCids1) {
                                                        foreach ($recordedCids1 as $rCid) {
                                                            $recordedCidsAll[] = $rCid;
                                                        }
                                                    }
                                                }
                                                //pr($response);
                                                $totalPosted++;
                                            }
                                        }
                                    }
                                    $response['domain'] = $domain;
                                    $response['submission_id'] = $submissionId;
                                    pr($response);
                                } else if ($dList['cms'] == 'joomla') {

                                }
                                //sleep(2);
                            }
                        }
                    } else {
                        $nothing['submission_id'] = $submissionId;
                        $nothing['msg'] = 'No article to post today';
                        //pr($nothing);
                    }
                } else {
                    $nothing['submission_id'] = $submissionId;
                    $nothing['msg'] = 'No article to post today';
                    //pr($nothing);
                }
            }
            if ($SEOHosting) {
                $body = $this->prettyEmailBody(print_r($SEOHosting, true));
                //$this->cmail(TEST_EMAIL, 'AutoPostSEOHosting', $body);
            }
        }
        if ($totalPosted) {
            //$this->cmail(TEST_EMAIL, 'AutoPost', "AutoPost Cron Success. Total posted: $totalPosted");
            if (!isset($_GET['skipset'])) {
                $this->setCStats();
            }
        }
        exit;
    }

    function sluggify($url)
    {
        # Prep string with some basic normalization
        $url = strtolower($url);
        $url = strip_tags($url);
        $url = stripslashes($url);
        $url = html_entity_decode($url);

        # Remove quotes (can't, etc.)
        $url = str_replace('\'', '', $url);

        # Replace non-alpha numeric with hyphens
        $match = '/[^a-z0-9]+/';
        $replace = '-';
        $url = preg_replace($match, $replace, $url);

        $url = trim($url, '-');

        return $url;
    }

    function updateNewDStats($postInfo, $post_content)
    {
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $reCids = array();
        if (count($post_content)) {
            $this->loadModel('DclDomain');
            $domainId = $postInfo['domain_id'];
            $dStatsInfo = $this->DclDomain->find('list', array('fields' => 'id,d_stats', 'conditions' => "id=$domainId"));
            $dStats = isset($dStatsInfo[$domainId]) ? json_decode($dStatsInfo[$domainId], true) : array();

            //pr($dStats);

            $postId = $postInfo['post_id'];

            preg_match_all('/\[dcl\=(\d+)\]/is', $post_content, $matches);
            $foundIds = $matches[1];

            // if any codes are found start the process
            if (count($foundIds)) {
                // get the unique campaign ids as multiple codes for single campaign can exist
                $cIds = array_unique($foundIds);

                // start looping through the found campaign ids
                foreach ($cIds as $cId) {
                    // get the total code found for the current campaign id
                    $totalCodeFound = 0;
                    foreach ($foundIds as $fId) {
                        if ($fId == $cId)
                            $totalCodeFound++;
                    }

                    @$dStats[$postId][$cId] += $totalCodeFound;
                    $reCids[] = $cId;
                }
                ksort($dStats);
                $updateData['id'] = $domainId;
                $updateData['d_stats'] = json_encode($dStats);
                //pr($updateData);
                $this->DclDomain->save($updateData, false);
            }
        }
        return $reCids;
    }

    function nitro_auto_post_loop($exitLoop = false, $startDate = null, $endDate = null, $userIdOne = null)
    {

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->loadModel('Submission');

        $cond = array(
            'conditions' => "Submission.status = 1",
            'fields' => 'user_id,user_id',
            'group' => 'user_id',
            'order' => 'user_id asc',
        );

        $userIdList = $this->Submission->find('list', $cond);

        if ($userIdOne) {
            $userIdList = array();
            $userIdList[] = intval($userIdOne);
        }


        if ($userIdList) {
            $userIdList = array_values($userIdList);

            if (!$startDate) {
                $startDate = date('Y-m-01');
            }
            if (!$endDate) {
                //$endDate = date('Y-m-t');
                $endDate = date('Y-m-d');
            }

            $dateArr = $this->range_date($startDate, $endDate);

            pr($dateArr);

            if ($exitLoop) {
                //exit;
            }
            pr($userIdList);

            foreach ($userIdList as $userId) {

                foreach ($dateArr as $dateNow) {
                    $this->nitro_auto_post($dateNow, $userId, $exitLoop);
                }
            }
        }
        pr($userIdList);
        exit;
    }

    function nitro_set_results($domains = null)
    {
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        set_time_limit(7200);
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 7200);
        ini_set('max_input_time', 7200);
        //$this->cmail(TEST_EMAIL, 'Setting Campaign Results', 'Setting success!');
        //$nowDomains[] = 'spokenbyzurek.com';
        //$nowDomains[] = 'mykvutza.com';
        //$nowDomains[] = 'spellbound-wordgame.com';
        //$nowDomains[] = 'merchantaccountusa.us';
        //$nowDomains[] = 'hairsalonsindunwoody';
        //$nowDomains[] = 'twdnc.com';
        //$nowDomains = array();
        //$this->loadModel('Commonmodel');
        //$this->Commonmodel->curl_get_contents('http://tracker.seonitro.com/generateResults.php');

        if ($domains && is_array($domains)) {
            $_GET['skip'] = 'skip';
        }

        if (!isset($_GET['skip'])) {
            //$generateSuccess = $this->nitro_generate_campaign_results();
        }
        //sleep(120);

        $domainClause = '';
        if (isset($_GET['domain'])) {
            $domainGet = $_GET['domain'];
            $domainClause = " AND Trackeddomain.pinger like '%$domainGet%'";
        }

        $domainClause2 = '';
        if ($domains && is_array($domains)) {
            $domainString = implode(',', $domains ? $domains : array(0));
            $domainClause2 = " AND Trackeddomain.domain_id IN($domainString)";
        }

        $this->loadModel('Trackeddomain');

        $cond = array(
            'conditions' => "Trackeddomain.id != 0 $domainClause $domainClause2"
        );

        $allCampaignResults = $this->Trackeddomain->find('all', $cond);
        //$allCampaignResults = result_array_raw("SELECT domains.id,domains.domain,domains.cms,trackeddomains.* FROM trackeddomains JOIN domains ON domains.id = trackeddomains.domain_id $domainClause");
        //pr($allCampaignResults); exit;
        if (isset($_GET['exit'])) {
            pr($allCampaignResults);
        }

        $domainArr = array();
        $finalArr = array();
        $response = array();

        if ($allCampaignResults) {
            foreach ($allCampaignResults as $aCR) {
                $campInfo = $aCR['Trackeddomain'];
                if ($campInfo['domain']) {
                    $domain = $campInfo['domain'];

                    if (isset($nowDomains) && isset($_GET['custom'])) {
                        if (!in_array($domain, $nowDomains)) {
                            continue;
                        }
                    }

                    //$temp['domain'] = $domain;
                    if ($campInfo['results']) {
                        $temp['campaign_id'] = $campInfo['campaign_id'];
                        $temp['pinger'] = $campInfo['pinger'];
                        $temp['results'] = $campInfo['results'];
                        $finalArr[$domain][] = $temp;
                    }
                }
            }

            if (isset($_GET['exit'])) {
                pr($finalArr);
            }
            //pr($finalArr);
            //exit;
            //$finalArr = array();
            if ($finalArr) {
                App::import('Vendor', 'IXR_Library');
                $fullRpcDomain = 'http://www.25kw.info/nitro/nitroserver.php';
                //$fullRpcDomain = 'http://www.egyptjobsearch.com/nitroserver.php';
                // Create the client object
                $client = new IXR_ClientSSL($fullRpcDomain);
                //$client->debug = true;

                $finalArrChunked = array_chunk($finalArr, 10, true);

                if (isset($_GET['exit'])) {
                    pr($finalArrChunked);
                    exit;
                }
                /**/
                foreach ($finalArrChunked as $chunk) {

                    $params['allResults'] = $chunk;

                    //pr($params);

                    $response = array();

                    if (!$client->query('nitro.setCampaignResults', $params)) {
                        $response[] = 'Error: ' . $client->getErrorCode() . " : " . $client->getErrorMessage();
                        //pr($response);
                    } else {
                        $response = $client->getResponse();
                        //pr(array('success'));
                    }
                    pr($response);
                    //exit;
                }
                /* */
            }
        }
        $mailSentAt = date('Y-m-d H:i');
        $this->cmail(TEST_EMAIL, 'Setting Campaign Results', 'Domains: ' . print_r($domains, true));
        if (!isset($_GET['domain'])) {
            return $response;
        }
    }

    function nitro_set_results_private($domain = null)
    {
        $this->loadModel('Nitrocamp');
        $this->Nitrocamp->setCampaignResultsPrivate($domain);
    }

    function nitro_set_results_manual($campId = null)
    {

        ob_end_clean();
        ignore_user_abort();
        ob_start();
        header("Connection: close");
        header("Content-Length: " . ob_get_length());
        ob_end_flush();
        flush();

        sleep(200);
        exit;

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);
        set_time_limit(7200);
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 7200);
        ini_set('max_input_time', 7200);

        $this->loadModel('Campaign');
        $this->loadModel('Trackeddomains');

        $userIdClause = '';
        if (isset($_GET['userId'])) {
            $userId = $_GET['userId'];
            $userIdClause = "user_id = $userId";
        }

        $campaignIds = $this->Campaign->find('list', array('fileds' => "id,id", 'conditions' => "$userIdClause", 'order' => "id desc"));
        $campaignIds = array_values($campaignIds);

        //$campaignIdsFromTracked = $this->Campaign->find('list', array('fileds' => "campaign_id,campaign_id", 'conditions' => "", 'order' => "id desc"));
        //$campaignIdsFromTracked = array_unique(array_values($campaignIdsFromTracked));
        //pr($campaignIds);
        //pr($campaignIdsFromTracked);
        //$notFoundCamp = array_diff($campaignIds, $campaignIdsFromTracked);
        //pr($notFoundCamp);
        //exit;


        if ($campId) {
            $campaignIds = array();
            $campaignIds = explode(',', $campId);
        }

        //pr($campaignIds);

        $campaignIds = array_values($campaignIds);
        //exit;

        $reTest = array(88888888888888);
        foreach ($campaignIds as $key => $value) {
            $reTest = $this->nitro_generate_campaign_results($value);
            //pr($reTest);
        }
        return $reTest;
    }

    function nitro_set_results_loop()
    {
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);
        set_time_limit(7200);
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 7200);
        ini_set('max_input_time', 7200);

        $this->loadModel('Campaign');

        $cond = "Campaign.last_generated IS NULL OR Campaign.created > Campaign.last_generated OR Campaign.modified > Campaign.last_generated";

        $campaignIds = $this->Campaign->find('list', array('fileds' => "id,id", 'conditions' => "$cond", 'order' => "id desc"));

        $campIdChunkIndex = $this->time_chunk();
        $campIdListChunkedAll = array_chunk($campaignIds, ceil(count($campaignIds) / 4));
        $campIdListChunked = $campIdListChunkedAll[$campIdChunkIndex];

        foreach ($campIdListChunked as $key => $value) {
            $reTest = $this->nitro_generate_campaign_results($value);
        }
    }

    function time_chunk()
    {
        $nowHour = date('H');
        $reCount = 0;
        if ($nowHour >= 0 && $nowHour < 6) {
            $reCount = 0;
        } elseif ($nowHour >= 6 && $nowHour < 12) {
            $reCount = 1;
        } elseif ($nowHour >= 12 && $nowHour < 18) {
            $reCount = 2;
        } elseif ($nowHour >= 18 && $nowHour <= 23) {
            $reCount = 3;
        }
        return $reCount;
    }

    function nitro_generate_campaign_results($campId = null, $skip = null)
    {
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);
        set_time_limit(7200);
        ini_set('memory_limit', '1024M');
        //ini_set('output_buffering', '4096');
        ini_set('max_execution_time', 7200);
        ini_set('max_input_time', 7200);


        $this->loadModel('Nitrocamp');
        $this->loadModel('Trackeddomain');

        $dbSource = ClassRegistry::init('ConnectionManager')->getDataSource('default');
        $dbName = $dbSource->config['database'];
        $dbConnection = mysql_connect($dbSource->config['host'], $dbSource->config['login'], $dbSource->config['password']);

        $dbTableName = 'trackeddomains';
        $lastGenerated = date('Y-m-d H:i:s');

        $response = array();
        if (connect_db_raw($dbConnection, $dbName)) {
            //echo 'Generate Script Starts! ';

            $camPaignIdClauseB = '';
            $camPaignIdClauseC = '';
            if ($campId) {
                $camPaignIdClauseB = "AND campaign_id = $campId";
                $camPaignIdClauseC = "AND id = $campId";
            }

            //get previously generated results if any
            $allTrackedDomains = result_array_raw("SELECT id,domain_id,campaign_id,pinger,post_id,codefound,results FROM trackeddomains WHERE 1=1 $camPaignIdClauseB");

            //pr($allBlogRolls);
            //pr($allTrackedDomains);
            //exit;

            $allUniqueDomainIds = $this->Nitrocamp->getUniqueIdsByField(array(), 'domain_id', $allTrackedDomains);

            $allBlogRolls = array();
            //get all the blogrolls
            $allB = result_array_raw("SELECT domain_id FROM blogrolls WHERE 1=1 $camPaignIdClauseB");
            $dmnIdArr = array();
            $dmnIdString = '0';
            if ($allB) {
                foreach ($allB as $vl) {
                    $dmnIdArr[] = $vl['domain_id'];
                }
            }
            //pr($allUniqueDomainIds);
            //pr($dmnIdArr);
            $allUniqueDomainIds = $dmnIdArr + (array_values($allUniqueDomainIds));
            pr($allUniqueDomainIds);
            //exit;
            if ($dmnIdArr) {
                $dmnIdString = implode(',', $dmnIdArr);
                $dmnIdIn = "AND domain_id IN($dmnIdString)";
                $allBlogRolls = result_array_raw("SELECT * FROM blogrolls WHERE 1=1 $dmnIdIn");
            }

            //$allBlogRolls = result_array_raw("SELECT * FROM blogrolls WHERE 1=1 $camPaignIdClauseB");

            $allUniqueCampaignIds = $this->Nitrocamp->getUniqueIdsByField($allBlogRolls, 'campaign_id', $allTrackedDomains);

            //pr($allBlogRolls);
            //pr($allUniqueDomainIds);
            //pr($allUniqueCampaignIds);
            //exit;

            $domainWhereClause = '';
            if ($campId) {
                $allDomainIdString = implode(',', $allUniqueDomainIds ? $allUniqueDomainIds : array(0));
                $domainWhereClause = " AND id IN($allDomainIdString)";
            }
            $campWhereClause = '';
            $campWhereClause2 = '';
            //if ($allUniqueCampaignIds) {}
            //echo $campId;
            if ($campId) {
                $allCampIdString = implode(',', $allUniqueCampaignIds ? $allUniqueCampaignIds : array(0));
                $campWhereClause = " AND id IN($allCampIdString)";
                $campWhereClause2 = " AND campaign_id IN($allCampIdString)";
            }
            //pr($uAllDomainIds);
            //get all the active campaigns

            $allCampaigns = result_array_raw("SELECT id,campignname,modified,last_generated FROM campaigns WHERE status = 1 $campWhereClause");


            $chunkedCampaigns = array_chunk($allCampaigns, 2);

            //pr($chunkedCampaigns);
            //exit;
            //get all the anchors
            $allAnchors = result_array_raw("SELECT * from anchortexts WHERE 1=1 $campWhereClause2 ORDER BY targetDensity DESC");

            //get all the domains
            $allDomains = result_array_raw("SELECT id,domain FROM domains WHERE 1=1 $domainWhereClause");

            //pr($chunkedCampaigns);
            //exit;
            //initialize the volume array

            $allAnchorSets = array();

            $domForThisCampaign = array();

            $updateCounter = 0;
            //if ($chunkedCampaigns) {
            //foreach ($chunkedCampaigns as $chunkNumb => $chunk) {
            //if ($chunkNumb == 1)
            //break;
            $volumeArray = array();
            //pr($chunk);
            //loop through the campaigns
            /**/
            foreach ($allCampaigns as $aC) {
                //foreach ($chunk as $aC) {
                //campaign id
                $uCI = $aC['id'];
                $lastGenerated = date('Y-m-d H:i:s');
                //echo '<br/>';

                $allUniqueDomainIdsByCid = $this->Nitrocamp->getUniqueDomainIdsByCid($allBlogRolls, $allTrackedDomains, $uCI);

                //get all the blogrolls using this campaign id
                $blogRollCountByCid = $this->Nitrocamp->getBlogRollByCid($allBlogRolls, $uCI);

                //get all dcl codes found by this campaign id
                $getCodeFoundByCid = $this->Nitrocamp->getCodeFoundByCid($allTrackedDomains, $uCI);

                //add blogrolls found and dcl codes found to get the total
                $totalCodeFoundByCid = $blogRollCountByCid + $getCodeFoundByCid;

                //get anchor list by this campaign id
                $anchorListByCid = ClassRegistry::init('Nitrocamp')->getAnchorListByCid($allAnchors, $uCI);

                //pr($anchorListByCid);
                //get the generated anchor list counted by the total found codes
                $anchorArrayByCid = $this->Nitrocamp->getContentAnchorArray($anchorListByCid, $totalCodeFoundByCid);

                //pr($anchorArrayByCid);
                //exit;

                if ($anchorArrayByCid) {
                    //pr($anchorArrayByCid[0]);
                    //get the volume array ready per campaign id
                    $volumeArray[$uCI]['campaign_id'] = $uCI;
                    $allAnchorSets[$uCI] = $anchorArrayByCid['aTexts'];
                    //$volumeArray[$uCI]['campaign_anchors'] = $anchorArrayByCid;

                    if (is_array($allDomains) && count($allDomains)) {
                        //loop through all the domains
                        $aCountByCid = 0;
                        foreach ($allDomains as $k => $aD) {
                            //domain id
                            $uADI = $aD['id'];

                            if (in_array($uADI, $allUniqueDomainIdsByCid)) {

                                //initialize a counter
                                //get if any blogrolls added for this domain by this campaign id
                                $getCodeFoundByDomainCampaignId = $this->Nitrocamp->getCodeFoundByDomainCampaignId($allBlogRolls, $uADI, $uCI);

                                //blogrolls for sidebar
                                $sidebarCount = isset($getCodeFoundByDomainCampaignId['sidebar']) ? $getCodeFoundByDomainCampaignId['sidebar'] : 0;

                                //blogrolls for footer
                                $footerCount = isset($getCodeFoundByDomainCampaignId['footer']) ? $getCodeFoundByDomainCampaignId['footer'] : 0;

                                $volumeArray[$uCI]['domains'][$k]['sidebar_anchors'] = array();
                                $volumeArray[$uCI]['domains'][$k]['footer_anchors'] = array();
                                $volumeArray[$uCI]['domains'][$k]['domain_id'] = $uADI;
                                $volumeArray[$uCI]['domains'][$k]['domain'] = $aD['domain'];
                                $volumeArray[$uCI]['domains'][$k]['campaign_id'] = $uCI;
                                $volumeArray[$uCI]['domains'][$k]['sidebar_count'] = $sidebarCount;
                                $volumeArray[$uCI]['domains'][$k]['footer_count'] = $footerCount;

                                if (($sidebarCount + $footerCount) > 0) {
                                    for ($s = 0; $s < $sidebarCount; $s++) {
                                        $volumeArray[$uCI]['domains'][$k]['sidebar_anchors']['aLinks'][] = $anchorArrayByCid['aLinks'][$aCountByCid];
                                        $aCountByCid++;
                                    }
                                    for ($f = 0; $f < $footerCount; $f++) {
                                        $volumeArray[$uCI]['domains'][$k]['footer_anchors']['aLinks'][] = $anchorArrayByCid['aLinks'][$aCountByCid];
                                        $aCountByCid++;
                                    }
                                }

                                //echo $uCI . '=================' . $uADI . '<br/>';
                                $pingersByCid = $this->Nitrocamp->getAllPingersBy_DomainId_cId($allTrackedDomains, $uCI, $uADI);

                                if ($pingersByCid) {
                                    foreach ($pingersByCid as $pk => $pBC) {
                                        $codeFound = $pBC['codefound'];
                                        $volumeArray[$uCI]['domains'][$k]['pingers_found'] = count($pingersByCid);
                                        $volumeArray[$uCI]['domains'][$k]['pingers_all'][$pk]['aLinks'] = array();
                                        for ($cf = 0; $cf < $codeFound; $cf++) {
                                            $volumeArray[$uCI]['domains'][$k]['pingers_all'][$pk]['aLinks'][] = @$anchorArrayByCid['aLinks'][$aCountByCid];
                                            $aCountByCid++;
                                        }
                                        $volumeArray[$uCI]['domains'][$k]['pingers_all'][$pk]['pinger'] = $pBC['pinger'];
                                        $volumeArray[$uCI]['domains'][$k]['pingers_all'][$pk]['id'] = $pBC['id'];
                                        $volumeArray[$uCI]['domains'][$k]['pingers_all'][$pk]['post_id'] = $pBC['post_id'];
                                        $volumeArray[$uCI]['domains'][$k]['pingers_all'][$pk]['codefound'] = $pBC['codefound'];
                                    }
                                }
                                //pr($pingersByCid);
                                //unset($pingersByCid);
                            }
                        }
                        $updateLastGenerated['last_generated'] = $lastGenerated;
                        update_raw('campaigns', $updateLastGenerated, array('id' => $uCI));
                    }
                }
                //unset($anchorArrayByCid);
                //}
                //}
            }
            /**/
            //unset($allCampaigns);
            //pr($volumeArray);
            //exit;

            /**/
            $domainArr = array();
            if (is_array($allDomains) && count($allDomains)) {
                foreach ($allDomains as $k => $aD) {
                    $domainId = $aD['id'];
                    $domainArr[$k]['id'] = $domainId;
                    $domainArr[$k]['domain'] = $aD['domain'];

                    foreach ($volumeArray as $vA) {
                        if (isset($vA['domains'])) {
                            foreach ($vA['domains'] as $uD) {
                                if ($uD['domain_id'] == $domainId) {
                                    $campaignId = $uD['campaign_id'];
                                    if (isset($uD['sidebar_anchors']['aLinks']) && $uD['sidebar_anchors']['aLinks']) {
                                        foreach ($uD['sidebar_anchors']['aLinks'] as $sA) {
                                            $domainArr[$k]['sidebar_anchors'][] = $sA;
                                        }
                                    }
                                    if (isset($uD['footer_anchors']['aLinks']) && $uD['footer_anchors']['aLinks']) {
                                        foreach ($uD['footer_anchors']['aLinks'] as $fA) {
                                            $domainArr[$k]['footer_anchors'][] = $fA;
                                        }
                                    }
                                    if (isset($uD['pingers_all']) && $uD['pingers_all']) {
                                        foreach ($uD['pingers_all'] as $pA) {
                                            if ($pA['aLinks']) {
                                                $pinger = $pA['pinger'];
                                                $domainArr[$k]['pingers_all'][$pinger][$campaignId] = $pA['aLinks'];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /**/
            //unset($volumeArray);
            //unset($allDomains);
            //pr($domainArr);
            //pr($volumeArray);

            $finalArray = array();

            /**/

            if ($domainArr) {
                foreach ($domainArr as $key => $dA) {
                    $blogRollsForDomain = array();
                    $blogRollsForDomain['domain_id'] = $dA['id'];
                    $blogRollsForDomain['pinger'] = $dA['domain'];
                    $blogRollsForDomain['campaign_id'] = -1;
                    if (isset($dA['sidebar_anchors']) && $dA['sidebar_anchors']) {
                        for ($mt = 0; $mt < 4; $mt++) {
                            $sidebarHtml = '<ul>';
                            foreach ($dA['sidebar_anchors'] as $sA) {
                                $cId = $sA['campaign_id'];
                                $arrIndex = $sA['arrIndex'];
                                //$finalLink = '<a ' . $sA['noFollow'] . 'href="http://' . preg_replace('/https?\:\/\//i', '', $sA['internalPageUrl']) . '">' . $allAnchorSets[$cId][$mt][$arrIndex] . '</a>';
                                $finalLink = $sA['finalLink'];
                                $sidebarHtml .= '<li>' . $finalLink . '</li>';
                            }
                            $sidebarHtml .= '</ul>';
                            $blogRollsForDomain['results']['sidebar_html'][$mt] = $sidebarHtml;
                        }
                    }

                    if (isset($dA['footer_anchors']) && $dA['footer_anchors']) {
                        for ($mt = 0; $mt < 4; $mt++) {
                            $footerHtml = '<ul>';
                            foreach ($dA['footer_anchors'] as $k => $fA) {
                                $cId = $fA['campaign_id'];
                                $arrIndex = $fA['arrIndex'];
                                //$finalLink = '<a ' . $fA['noFollow'] . 'href="http://' . preg_replace('/https?\:\/\//i', '', $fA['internalPageUrl']) . '">' . $allAnchorSets[$cId][$mt][$arrIndex] . '</a>';
                                $finalLink2 = $fA['finalLink'];
                                $liBorder = ($k + 1 != count($dA['footer_anchors'])) ? 'border-right:1px solid gray;' : '';
                                $footerHtml .= '<li style="display:inline;padding:0 3px;margin-left:0;' . $liBorder . '">' . $finalLink2 . '</li>';
                            }
                            $footerHtml .= '</ul>';
                            $blogRollsForDomain['results']['footer_html'][$mt] = $footerHtml;
                        }
                    }
                    if (isset($blogRollsForDomain['results'])) {
                        $finalArray[] = $blogRollsForDomain;
                    }

                    if (isset($dA['pingers_all']) && $dA['pingers_all']) {
                        foreach ($dA['pingers_all'] as $pinger => $pA) {
                            $pingerForDomain = array();
                            $pingerForDomain['domain_id'] = $dA['id'];
                            $pingerForDomain['pinger'] = $pinger;

                            $campaignAnchors = $pA;

                            foreach ($campaignAnchors as $k => $cA) {
                                $pingerForDomain['campaign_id'] = $k;
                                $dclAnchors = array();
                                for ($mt = 0; $mt < 4; $mt++) {
                                    foreach ($cA as $k => $aLink) {
                                        $pCampaignId = $aLink['campaign_id'];
                                        $arrIndex = $aLink['arrIndex'];
                                        //$finalLink = '<a ' . $aLink['noFollow'] . 'href="http://' . preg_replace('/https?\:\/\//i', '', $aLink['internalPageUrl']) . '">' . $allAnchorSets[$pCampaignId][$mt][$arrIndex] . '</a>';
                                        //$dclAnchors[$mt][] = $finalLink;
                                        $dclAnchors[$mt][] = $aLink['finalLink'];
                                    }
                                    $pingerForDomain['results'] = $dclAnchors;
                                }
                                if (isset($pingerForDomain['results'])) {
                                    $finalArray[] = $pingerForDomain;
                                }
                            }
                        }
                    }
                }
            }
            /**/

            //pr($finalArray);
            //exit;
            //addslashes($footerHtml);

            /**/
            if ($finalArray) {
                foreach ($finalArray as $fA) {
                    $codeFound = isset($fA['results'][0]) ? count($fA['results'][0]) : 0;
                    $fA['results'] = isset($fA['results']) ? (serialize($fA['results'])) : serialize(array());
                    $resultsNow = $this->Nitrocamp->getTrackedDomainsId($allTrackedDomains, $fA['campaign_id'], $fA['domain_id'], $fA['pinger']);

                    if (!$resultsNow) {
                        if ($fA['campaign_id'] == -1) {
                            $resultsNow = $this->Trackeddomain->find('first', array('conditions' => "Trackeddomain.campaign_id = -1 AND Trackeddomain.domain_id = " . $fA['domain_id']));
                            if ($resultsNow) {
                                $resultsNow['id'] = $resultsNow['Trackeddomain']['id'];
                            }
                        }
                    }
                    if ($resultsNow) {
                        $updateData['results'] = $fA['results'];
                        //$updateData['modified'] = date('Y-m-d H:i:s');
                        $updateData['modified'] = $lastGenerated;
                        update_raw($dbTableName, $updateData, array('id' => $resultsNow['id']));
                    } else {
                        //$fA['created'] = date('Y-m-d H:i:s');
                        $fA['created'] = $lastGenerated;
                        $fA['modified'] = $lastGenerated;
                        insert_raw($dbTableName, $fA);
                    }
                    $updateCounter++;
                }
            }
            /* */
            //echo "Total generated: $updateCounter";
            //echo '<br/>';
            //close_db_raw($dbConnection);
            //}
            pr(array('DOMAIN_IDS' => $allUniqueDomainIds));

            //$skip = isset($_GET['skip']);

            $campaignText = '';
            $reTest = array('ggg');
            if ($campId) {
                if (!$skip) {
                    if ($allUniqueDomainIds) {
                        $reTest = $this->nitro_set_results($allUniqueDomainIds);
                    }
                    //pr($reTest);
                }
                $campaignText = 'Campaign ID: ' . $campId;
            }
            $mailSentAt = date('Y-m-d H:i:s');
            $this->cmail(TEST_EMAIL, 'Generating Campaign Results', "Total: $updateCounter; $campaignText");
            //}
            close_db_raw($dbConnection);

            return $reTest;
        }
    }

    function range_date($first, $last)
    {
        $arr = array();
        $now = strtotime($first);
        $last = strtotime($last);

        while ($now <= $last) {
            $arr[] = date('Y-m-d', $now);
            $now = strtotime('+1 day', $now);
        }

        return $arr;
    }

    function checkAuthLabParse()
    {
        $this->loadModel('Authoritylab');

        ob_start();
        ?>{"get_count":101,"video_packs":[9,17,21,63],"auto_corrected_to":"","image":true,"keyword":"light stix","suggested_search_spelling":"","auto_corrected":false,"lang_only":"false","s3_key":"","safe_search":"on","shopping":false,"news":false,"geo":"","data_center":"us","brand_pack":false,"serp":{"101":{"href":"http://www.creativemin.com/category/dowel","base_url":"www.creativemin.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"For more than ten years, Jeff Smith of Salt & Light Ministries has been using \nwooden dowel rod sticks to interpret and bring to life contemporary Christian \nmusic\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.creativemin.com/category/dowel&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CN0DEBYwTzgU&usg=AFQjCNHm-QkviI18xmynYuQlVG3ggB2i5w","news":false,"local_pack":{},"page":11,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.creativemin.com/category/dowel","title":"Dowel Rod Interpretive Movement - Creative Ministry Solutions","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"creativemin.com","place_details":{},"place_pack":{}},"95":{"href":"http://miheadphones.blogspot.com/2012/04/stix-loco-light-it-up-video.html","base_url":"miheadphones.blogspot.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Apr 22, 2012 ... THE HOMIE STIX LOCO JUST PUT ON FOR HIS STATE, HIS HOOD, & HIS \nTEAM... STAND UP LORIS U GOT YOU ONE WITH THIS ONE.","shopping":false,"dirty_url":"/url?q=http://miheadphones.blogspot.com/2012/04/stix-loco-light-it-up-video.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CLsDEBYwSTgU&usg=AFQjCNG_tnQmpBDpokw9AMq3JHuFmQznRw","news":false,"local_pack":{},"page":10,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://miheadphones.blogspot.com/2012/04/stix-loco-light-it-up-video.html","title":"MIHEADPHONES: STIX LOCO - LIGHT IT UP VIDEO","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"blogspot.com","place_details":{},"place_pack":{}},"89":{"href":"http://www.stevespanglerscience.com/blog/halloween-2/the-dangers-of-glow-sticks-always-follow-safe-science-warnings-and-precautions/","base_url":"www.stevespanglerscience.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Jul 3, 2012 ... Steve Spangler Science also sells a few different varieties of glow sticks. Kids \nlove cracking them and watching the light glow like a firefly.","shopping":false,"dirty_url":"/url?q=http://www.stevespanglerscience.com/blog/halloween-2/the-dangers-of-glow-sticks-always-follow-safe-science-warnings-and-precautions/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJgDEBYwQzgU&usg=AFQjCNHwL7c9FlBvDArwn2WBy8IET1O8YA","news":false,"local_pack":{},"page":9,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.stevespanglerscience.com/blog/halloween-2/the-dangers-of-glow-sticks-always-follow-safe-science-warnings-and-precautions/","title":"The Dangers of Glow Sticks - Steve Spangler Science","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"stevespanglerscience.com","place_details":{},"place_pack":{}},"10":{"href":"http://www.walmart.com/ip/Kurt-Adler-Warm-Meteor-Shower-LED-Light-Stix-in-White/15722392?page=seeAllReviews","base_url":"www.walmart.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Customer Reviews for Kurt Adler Warm Meteor Shower LED Light Stix in White - \nWalmart.com.","shopping":false,"dirty_url":"/url?q=http://www.walmart.com/ip/Kurt-Adler-Warm-Meteor-Shower-LED-Light-Stix-in-White/15722392%3Fpage%3DseeAllReviews&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CGkQFjAN&usg=AFQjCNHKPGval-2w_PQ2RQscSoQnbhlyrw","news":false,"local_pack":{},"page":1,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.walmart.com/ip/Kurt-Adler-Warm-Meteor-Shower-LED-Light-Stix-in-White/15722392%3Fpage%3DseeAllReviews","title":"Customer Reviews for Kurt Adler Warm Meteor Shower LED Light ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"walmart.com","place_details":{},"place_pack":{}},"92":{"href":"http://www.4c.com/documents/products.html","base_url":"www.4c.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"4C Totally Light\u2122 Iced Tea Mix Tub Canisters. Lemon, Lemon 14qt ... 4C Iced \nTea Stix. 4C Totally Light Tea2Go\u2122 Iced Tea Mixes. Lemon, Lemon 20ct.","shopping":false,"dirty_url":"/url?q=http://www.4c.com/documents/products.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CKkDEBYwRjgU&usg=AFQjCNGdvj2YSuZlv2wTJqqNw0k3oGFQUw","news":false,"local_pack":{},"page":10,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.4c.com/documents/products.html","title":"Our Products - 4C.com","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"4c.com","place_details":{},"place_pack":{}},"86":{"href":"http://www.winestix.com/beerstix-oak-alternatives-for-brewing/","base_url":"www.winestix.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"The Beer Stix\u2122 milling gives 60% more surface area than Wine Stix\u00ae products \nfor ... Light Toast Light Vanillin Almonds Coconut Fresh Oak Medium Toast More\n\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.winestix.com/beerstix-oak-alternatives-for-brewing/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CIcDEBYwQDgU&usg=AFQjCNEkYXF4QB-PpEHMvNyYGPBHCf2oWA","news":false,"local_pack":{},"page":9,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.winestix.com/beerstix-oak-alternatives-for-brewing/","title":"BeerStix \u2013 Oak Alternatives For Brewing - WineStix","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"winestix.com","place_details":{},"place_pack":{}},"77":{"href":"http://www.clubsparklers.com/18-inch-customized-led-foam-sticks/","base_url":"www.clubsparklers.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"LED custom foam sticks, light up stick for party, events, grand opening, wedding, \ncustom print with your name or logo.","shopping":false,"dirty_url":"/url?q=http://www.clubsparklers.com/18-inch-customized-led-foam-sticks/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CNMCEBYwNzgU&usg=AFQjCNE6dAWiy0ZR6M7q2MuZ99rm3FES9Q","news":false,"local_pack":{},"page":8,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.clubsparklers.com/18-inch-customized-led-foam-sticks/","title":"18\" inch customized led foam sticks - DBA Club Sparklers","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"clubsparklers.com","place_details":{},"place_pack":{}},"83":{"href":"http://www.metrix-electronics.com/HTML/P3T_Stix_light_meters.html","base_url":"www.metrix-electronics.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Convenient STIX digital light meter testers for service and maintenance \napplications.","shopping":false,"dirty_url":"/url?q=http://www.metrix-electronics.com/HTML/P3T_Stix_light_meters.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CPYCEBYwPTgU&usg=AFQjCNFca5VJWFArxCtI2gHkNK4thybXPw","news":false,"local_pack":{},"page":9,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.metrix-electronics.com/HTML/P3T_Stix_light_meters.html","title":"STIX Light Meters - Metrix Electronics Limited","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"metrix-electronics.com","place_details":{},"place_pack":{}},"74":{"href":"http://www.flickr.com/photos/walloftvs/3177799369/","base_url":"www.flickr.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"May 25, 2008 ... Backpacking Light Stix. ... keyboard shortcuts: \u2190 previous photo \u2192 next photo L \nview in light box F favorite < scroll film strip left > scroll film strip\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.flickr.com/photos/walloftvs/3177799369/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CMUCEBYwNDgU&usg=AFQjCNGEf4Ln_DcxSwqRweW0wzXpLay7lw","news":false,"local_pack":{},"page":8,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.flickr.com/photos/walloftvs/3177799369/","title":"Backpacking Light Stix | Flickr - Photo Sharing!","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"flickr.com","place_details":{},"place_pack":{}},"80":{"href":"http://www.morbidstix.com/morbidstix-skull-ps4-light-bar-sticker/","base_url":"www.morbidstix.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Feb 28, 2014 ... Customize your PS4 Controller with this perfect MorbidStix Skull PS4 Light Bar \nVinyl Sticker that you can customize from over 20 plus colors.","shopping":false,"dirty_url":"/url?q=http://www.morbidstix.com/morbidstix-skull-ps4-light-bar-sticker/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0COUCEBYwOjgU&usg=AFQjCNGdumotj1alk9Fm5tprww6VBnrSCQ","news":false,"local_pack":{},"page":9,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.morbidstix.com/morbidstix-skull-ps4-light-bar-sticker/","title":"MorbidStix Skull PlayStation 4 Light Bar Sticker - MorbidStix.com","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"morbidstix.com","place_details":{},"place_pack":{}},"68":{"href":"http://hol.abime.net/218","base_url":"hol.abime.net","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Bubba 'n' Stix : Hall Of Light \u2013 The database of Amiga games. Bubba 'n' Stix - \nDouble Barrel Screenshot. Information: - Chipset: ECS/OCS CD32 - Year\u00a0...","shopping":false,"dirty_url":"/url?q=http://hol.abime.net/218&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CKQCEBYwLjgU&usg=AFQjCNGbSyXpoWXSfG33W56eqIn5wo1PFg","news":false,"local_pack":{},"page":7,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://hol.abime.net/218","title":"Bubba 'n' Stix : Hall Of Light \u2013 The database of Amiga games","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"abime.net","place_details":{},"place_pack":{}},"59":{"href":"http://www.megaglow.com/","base_url":"www.megaglow.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Glow necklaces, glow stick, glow sticks, glowsticks, body lights, flashing blinkys, \nmagnetic flashers, glow-in-the-dark, light sticks, glow bracelets, blinking body\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.megaglow.com/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CPIBEBYwJTgU&usg=AFQjCNGKWEpXFRoWxrrRWAM-Nij8PstBHQ","news":false,"local_pack":{},"page":6,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.megaglow.com/","title":"Wholesale Glow Sticks Body Lights Mini Glow Stick","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"megaglow.com","place_details":{},"place_pack":{}},"65":{"href":"https://www.facebook.com/events/489235077834744/?ref=22","base_url":"www.facebook.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Time to light it up at Maduro Stix!!! Join us for a night of cigars and fun on \nSaturday, July 20th at 4:30pm - 7:30pm. See below for complete details. Date: 7/\n20/\u00a0...","shopping":false,"dirty_url":"/url?q=https://www.facebook.com/events/489235077834744/%3Fref%3D22&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJUCEBYwKzgU&usg=AFQjCNFAx_2BQ2Gu2vbO-A7vHinpZIURdw","news":false,"local_pack":{},"page":7,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"https://www.facebook.com/events/489235077834744/%3Fref%3D22","title":"Blue Flame Cigar Society Lights Up Maduro Stix | Facebook","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"facebook.com","place_details":{},"place_pack":{}},"71":{"href":"http://www.waxahachietx.com/sports/local_sports/u--u-stix-win-tournament/image_9846f8ea-adf2-5ded-a1d7-8b5363a8abf2.html","base_url":"www.waxahachietx.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Sep 17, 2013 ... The Stix Baseball Club took the top spot in the 15U-16U Open (high school \ndivision) Field of Dreams Fall Classic Tournament in Irving Sept.","shopping":false,"dirty_url":"/url?q=http://www.waxahachietx.com/sports/local_sports/u--u-stix-win-tournament/image_9846f8ea-adf2-5ded-a1d7-8b5363a8abf2.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CLYCEBYwMTgU&usg=AFQjCNGPGGQ0wvlzRCuDNkHd_q23SB4OOA","news":false,"local_pack":{},"page":8,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.waxahachietx.com/sports/local_sports/u--u-stix-win-tournament/image_9846f8ea-adf2-5ded-a1d7-8b5363a8abf2.html","title":"15U-16U Stix win tournament - Waxahachie Daily Light: Local Sports","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"waxahachietx.com","place_details":{},"place_pack":{}},"62":{"href":"http://www.anwinc.com/product/Light_Up_Safety_Stix_Orange_562497","base_url":"www.anwinc.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Batteries are included and installed. 8\" Light-Up Safety Stix. Patents Pending. \nOrder in increments of 24 pieces.","shopping":false,"dirty_url":"/url?q=http://www.anwinc.com/product/Light_Up_Safety_Stix_Orange_562497&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CIUCEBYwKDgU&usg=AFQjCNHvJVSd2qlmNE4xHFqcXpNot9V4qw","news":false,"local_pack":{},"page":7,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.anwinc.com/product/Light_Up_Safety_Stix_Orange_562497","title":"251390 Light-Up Safety Stix-Orange - Aberson Narotzky & White Inc","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"anwinc.com","place_details":{},"place_pack":{}},"56":{"href":"http://www.webexhibits.org/causesofcolor/4AD.html","base_url":"www.webexhibits.org","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Light made through chemical reactions. Chemoluminescence. Light sticks emit a \nvariety of bright colors. The vibrant glow of a breaking wave at midnight, the\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.webexhibits.org/causesofcolor/4AD.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0COABEBYwIjgU&usg=AFQjCNE8wg-a1zLI399-7Stmq149_HK7_Q","news":false,"local_pack":{},"page":6,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.webexhibits.org/causesofcolor/4AD.html","title":"Chemiluminescence | Causes of Color - Webexhibits","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"webexhibits.org","place_details":{},"place_pack":{}},"47":{"href":"http://www.hasbro.com/common/instruct/Simon_Stix.pdf","base_url":"www.hasbro.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"CONTENTS: Two Simon Stix, connecting cord. OBJECT: Get your memory \nmoving! Shake and cross your Simon Stix to respond to the \ufb02ashing lights and \nsounds\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.hasbro.com/common/instruct/Simon_Stix.pdf&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CLEBEBYwGTgU&usg=AFQjCNHhM6to7Uks5jvr8rdbAahNnZFWrg","news":false,"local_pack":{},"page":5,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.hasbro.com/common/instruct/Simon_Stix.pdf","title":"Simon Stix Instructions - Hasbro","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"hasbro.com","place_details":{},"place_pack":{}},"53":{"href":"https://www.gamefaces.com/category.pcgi?fm_catid=630","base_url":"www.gamefaces.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"LED Light-up Spirit Stix - There are 7 products in this category. Blue Light-up \nSpirit Stix XI-STX-LED-2 \u00b7 Green Light-up Spirit Stix XI-STX-LED-6.","shopping":false,"dirty_url":"/url?q=https://www.gamefaces.com/category.pcgi%3Ffm_catid%3D630&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CNEBEBYwHzgU&usg=AFQjCNEsslswM3dEIF3n0r5XalnOd3Jg0Q","news":false,"local_pack":{},"page":6,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"https://www.gamefaces.com/category.pcgi%3Ffm_catid%3D630","title":"LED Light-up Spirit Stix - Team Dynamics","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"gamefaces.com","place_details":{},"place_pack":{}},"44":{"href":"http://www.samash.com/firestix-light-up-drum-sticks-fx12color","base_url":"www.samash.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Perfect for drum solos! Firestix drumsticks are made of durable Lexan and light \nup upon impact for a visually exciting and dramatic lighting effect. Firestix are\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.samash.com/firestix-light-up-drum-sticks-fx12color&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CKABEBYwFjgU&usg=AFQjCNGz0-bw42rHg22gG2A41VPBWykY-g","news":false,"local_pack":{},"page":5,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.samash.com/firestix-light-up-drum-sticks-fx12color","title":"Firestix Light-Up Drum Sticks | SamAsh","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"samash.com","place_details":{},"place_pack":{}},"50":{"href":"http://www.pickupstix.com/asiafit.php","base_url":"www.pickupstix.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Fit starts with Asia Fit, a new, healthier line of Asian cuisine from Pick Up Stix. ... \nbold flavors and satisfying portions, prepared with lean proteins, light sauces\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.pickupstix.com/asiafit.php&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CMIBEBYwHDgU&usg=AFQjCNFvPcJUFzLPz9k2dDsRN53QN4_rLQ","news":false,"local_pack":{},"page":6,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.pickupstix.com/asiafit.php","title":"AsiaFit Menu - Pick Up Stix","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"pickupstix.com","place_details":{},"place_pack":{}},"38":{"href":"https://github.com/STIXProject/stix-to-html/issues/25","base_url":"github.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Feb 18, 2014 ... Sometimes, the traffic light protocol information does not display. This is probably \ncause by some xpath/xslt logic that causes only the marking\u00a0...","shopping":false,"dirty_url":"/url?q=https://github.com/STIXProject/stix-to-html/issues/25&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CIEBEBYwEDgU&usg=AFQjCNEM55iMYiKD1kMmiQ9ONvHYADhc4g","news":false,"local_pack":{},"page":4,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"https://github.com/STIXProject/stix-to-html/issues/25","title":"STIXProject/stix-to-html - GitHub","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"github.com","place_details":{},"place_pack":{}},"41":{"href":"http://ht.ly/2yKDM8","base_url":"ht.ly","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"4C CAP-IT or Totally Light Stix/Tubs on any. 0 Comments. 193 days ago / All-in-\none Internet Search. Value: 1.00 Expiry: 08/31/13 Handling Fee: 0.10","shopping":false,"dirty_url":"/url?q=http://ht.ly/2yKDM8&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJIBEBYwEzgU&usg=AFQjCNFXdWVSBXggStQWQ6O6qUyfZwSCUQ","news":false,"local_pack":{},"page":5,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://ht.ly/2yKDM8","title":"4C CAP-IT or Totally Light Stix/Tubs on any | All-in-one Internet ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"ht.ly","place_details":{},"place_pack":{}},"29":{"href":"http://www.copykat.com/2010/01/28/make-light-and-flaky-cinnabon-stix-at-home/","base_url":"www.copykat.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Jan 28, 2010 ... Cinnabon Stix can be made at home. You can do so easily with this recipe. Light \nand crispy with a touch of cinnamon and sugar these will\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.copykat.com/2010/01/28/make-light-and-flaky-cinnabon-stix-at-home/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CEsQFjAHOBQ&usg=AFQjCNGHq_x2RrR6ljg30TJPMVWLrFEGyw","news":false,"local_pack":{},"page":3,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.copykat.com/2010/01/28/make-light-and-flaky-cinnabon-stix-at-home/","title":"Cinnabon Stix | CopyKat Recipes | Restaurant Recipes","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"copykat.com","place_details":{},"place_pack":{}},"7":{"href":"http://www.christianbook.com/wikki-stix-light-blue/pd/118517","base_url":"www.christianbook.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Wikki Stix are the creative, one-of-a-kind creatables that will not crumble, break, \nstiffen, or dry out. ... Includes 36 light blue Wikki Stix; recommended for ages 3+.","shopping":false,"dirty_url":"/url?q=http://www.christianbook.com/wikki-stix-light-blue/pd/118517&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CFkQFjAK&usg=AFQjCNFlXIC31C-RB7ONk11jtAzbbRBVdg","news":false,"local_pack":{},"page":1,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.christianbook.com/wikki-stix-light-blue/pd/118517","title":"Christianbook.com: Wikki Stix Light Blue","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"christianbook.com","place_details":{},"place_pack":{}},"35":{"href":"http://www.originalhoneystix.com/light-sweet-agave-nectar-stix-1000ct-p-128.html","base_url":"www.originalhoneystix.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Natures Kick Original Honeystix Light Sweet Agave Nectar Stix 1000ct [B1000-\nALT] - Pure Agave Nectar with no flavors or additives. The nectar from the Agave\n\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.originalhoneystix.com/light-sweet-agave-nectar-stix-1000ct-p-128.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CG4QFjANOBQ&usg=AFQjCNHDPiDIv_e0ZL56GL-Ciy4gArBdWw","news":false,"local_pack":{},"page":4,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.originalhoneystix.com/light-sweet-agave-nectar-stix-1000ct-p-128.html","title":"Light Sweet Agave Nectar Stix 1000ct [B1000-ALT] - $90.00 ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"originalhoneystix.com","place_details":{},"place_pack":{}},"26":{"href":"http://glow-sticks.com/","base_url":"glow-sticks.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Northern Light Sticks, Inc. - Glow sticks, necklaces, bracelets - at our low factory \ndirect prices! Glow sticks, necklaces, bracelets - at our low factory direct prices!","shopping":false,"dirty_url":"/url?q=http://glow-sticks.com/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CDoQFjAEOBQ&usg=AFQjCNFRyNaLEJKFuy2saPlaQIg-TYYD2Q","news":false,"local_pack":{},"page":3,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://glow-sticks.com/","title":"Northern Light Sticks, Inc.","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"glow-sticks.com","place_details":{},"place_pack":{}},"117":{"href":"http://www.backscatter.com/sku/sx-fbf.lasso","base_url":"www.backscatter.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Totally overcome the negative buoyancy of the Bluefin Housing and lights in one \neasy step. The StiX Custom Buoyancy Float for the Bluefin Video Housings are\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.backscatter.com/sku/sx-fbf.lasso&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CLgEEBYwXzgU&usg=AFQjCNEtZLwDmMVfns_fUcooETDPULau_A","news":false,"local_pack":{},"page":12,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.backscatter.com/sku/sx-fbf.lasso","title":"Stix Float for Bluefin Video Housing - Backscatter","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"backscatter.com","place_details":{},"place_pack":{}},"4":{"href":"http://www.google.com/images?q=light stix&gl=us&hl=en&sa=X&oi=image_result_group&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CEEQsAQ","base_url":"Unable to Process","brand_pack_results":{},"video_pack_results":{},"image":true,"description":"","shopping":false,"dirty_url":"/images?q=light+stix&gl=us&hl=en&sa=X&oi=image_result_group&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CEEQsAQ","news":false,"local_pack":{},"page":1,"micro_format":false,"real_time":false,"image_pack_results":[{"dirty_url":"/url?q=http://backpackinglight.com/cgi-bin/backpackinglight/stix_carbon_fiber_trekking_poles_08.html&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CEMQ9QEwAw&usg=AFQjCNGHI47Q8uo4CYKQDhmPpGBxcwgnWw","url":"http://backpackinglight.com/cgi-bin/backpackinglight/stix_carbon_fiber_trekking_poles_08.html"},{"dirty_url":"/url?q=http://www.lumivisions.net/lighting/led-lightstix-products/&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CEUQ9QEwBA&usg=AFQjCNHv7f8f-udV1BmLPGpv-hVUqlRmSg","url":"http://www.lumivisions.net/lighting/led-lightstix-products/"},{"dirty_url":"/url?q=http://store.drumbum.com/skuSTK-20M.html&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CEcQ9QEwBQ&usg=AFQjCNGKLWKfxQUPCGUcXkWMDNkUVgcR5A","url":"http://store.drumbum.com/skuSTK-20M.html"},{"dirty_url":"/url?q=http://www.lumivisions.net/lighting/led-lightstix-products/&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CEkQ9QEwBg&usg=AFQjCNHv7f8f-udV1BmLPGpv-hVUqlRmSg","url":"http://www.lumivisions.net/lighting/led-lightstix-products/"},{"dirty_url":"/url?q=http://www.clubsparklers.com/customize-wedding-led-foam-sticks-18-inch/&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CEsQ9QEwBw&usg=AFQjCNFN9FPN6mpCkO3mG5DwM9fgsAJO_A","url":"http://www.clubsparklers.com/customize-wedding-led-foam-sticks-18-inch/"}],"maps":false,"url":"/images?q=light+stix&gl=us&hl=en&sa=X&oi=image_result_group&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CEEQsAQ","title":"Images for light stix","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"Unable to Process","place_details":{},"place_pack":{}},"32":{"href":"http://www.ebay.com/itm/Electrostar-Bicycle-Wheel-Sparx-Stix-Rainbow-Light-Set-Screws-on-Schrader-Tubes-/400608738662","base_url":"www.ebay.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Electrostar Bicycle Wheel Sparx Stix Rainbow Light Set Screws on Schrader \nTubes in Sporting Goods, Cycling, Bicycle Accessories | eBay.","shopping":false,"dirty_url":"/url?q=http://www.ebay.com/itm/Electrostar-Bicycle-Wheel-Sparx-Stix-Rainbow-Light-Set-Screws-on-Schrader-Tubes-/400608738662&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CF0QFjAKOBQ&usg=AFQjCNGfnHEVCeQXGu7n7x7VYic0z3b-6Q","news":false,"local_pack":{},"page":4,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.ebay.com/itm/Electrostar-Bicycle-Wheel-Sparx-Stix-Rainbow-Light-Set-Screws-on-Schrader-Tubes-/400608738662","title":"Electrostar Bicycle Wheel Sparx Stix Rainbow Light Set Screws on ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"ebay.com","place_details":{},"place_pack":{}},"114":{"href":"http://www.nightclubsupplier.com/led-foam-sticks-18-inch/","base_url":"www.nightclubsupplier.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"LED, FOAM, lite, light, sticks, stix, club, rave, party, dance, custom party batons, \ncustom party sticks, led foam sticks, led sticks, led batons, party custom sticks,\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.nightclubsupplier.com/led-foam-sticks-18-inch/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CKcEEBYwXDgU&usg=AFQjCNFBKzLlmnhkyhIlFtsfqLr4tTl_9A","news":false,"local_pack":{},"page":12,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.nightclubsupplier.com/led-foam-sticks-18-inch/","title":"LED FOAM Lite Stix are the ultimate dance accessory to light up the ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"nightclubsupplier.com","place_details":{},"place_pack":{}},"1":{"href":"http://www.amazon.com/Emergency-Industrial-Providing-Chemlight-Intensity/dp/B00GFGWM0E","base_url":"www.amazon.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Emergency Industrial Grade Light Sticks Used for Safety, By Hunters & Hikers \nProviding a Chemlight High Intensity Luminous Glow - Similar to Cyalume \nMilitary\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.amazon.com/Emergency-Industrial-Providing-Chemlight-Intensity/dp/B00GFGWM0E&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CDAQFjAA&usg=AFQjCNEvcyU6m5dl5C-mP1711dJ3Bod21g","news":false,"local_pack":{},"page":1,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.amazon.com/Emergency-Industrial-Providing-Chemlight-Intensity/dp/B00GFGWM0E","title":"Amazon.com: Emergency Industrial Grade Light Sticks Used for ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"amazon.com","place_details":{},"place_pack":{}},"17":{"href":"http://www.youtube.com/watch?v=IFjLHYQ_Va0","base_url":"www.youtube.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Drum solo with Light up Drumsticks (Fire Stix) - YouTube. Subscribe 126. All  comments (10 ...","shopping":false,"dirty_url":"/url?q=http://www.youtube.com/watch%3Fv%3DIFjLHYQ_Va0&sa=U&ei=rOdMU-yFF-aNywOw_oKAAQ&ved=0CEYQtwIwBTgK&usg=AFQjCNGzl-Fv2lsi7_fgCRJop-dVHxes3w","news":false,"local_pack":{},"page":2,"micro_format":true,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.youtube.com/watch%3Fv%3DIFjLHYQ_Va0","title":"Drum solo with Light up Drumsticks (Fire Stix) - YouTube","blog_posts":false,"authorship":false,"indented":0,"video":true,"authorship_details":{},"base_domain":"youtube.com","place_details":{},"place_pack":{}},"120":{"href":"http://coolglow.com/","base_url":"coolglow.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"LED Eyeglasses LED Ice Cubes LED Balloons LED Foam Sticks LED Necklaces \nLED Bracelets LED Rings \u00b7 LED Finger Lights LED Earrings LED Gloves LED\u00a0...","shopping":false,"dirty_url":"/url?q=http://coolglow.com/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CMoEEBYwYjgU&usg=AFQjCNGmP7k2tJFj_ZDN6gKh7YgKSbQ5Yw","news":false,"local_pack":{},"page":13,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://coolglow.com/","title":"Glow Sticks","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"coolglow.com","place_details":{},"place_pack":{}},"108":{"href":"http://www.saferwholesale.com/Genz-Stix-3-Pack-Ice-Fishing-Rod-23-Ultra-Light-p/3%20genz%20stix%20ice%20fish%20rod%2023%20in.htm","base_url":"www.saferwholesale.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Designed by ice angling guru Dave Genz, these rods and reels play gamefish \nlike fine-tuned instruments. Every rod in the exclusive Genz Stix Series has been\n\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.saferwholesale.com/Genz-Stix-3-Pack-Ice-Fishing-Rod-23-Ultra-Light-p/3%2520genz%2520stix%2520ice%2520fish%2520rod%252023%2520in.htm&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CIUEEBYwVjgU&usg=AFQjCNHFTCIIrEjGx1ycH6ZP8_BaFpPGlQ","news":false,"local_pack":{},"page":11,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.saferwholesale.com/Genz-Stix-3-Pack-Ice-Fishing-Rod-23-Ultra-Light-p/3%2520genz%2520stix%2520ice%2520fish%2520rod%252023%2520in.htm","title":"Brand New Genz Stix 3 Pack Ice Fishing Rod 23\" Ultra Light","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"saferwholesale.com","place_details":{},"place_pack":{}},"23":{"href":"http://www.nightclubshop.com/foam-sticks-glow-stix/","base_url":"www.nightclubshop.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"FOAM STICKS/L.E.D.STIX. We Carry several different kinds of \"Foam\" LED Light/\nGlow Sticks as well as Traditional Liquid Filled \"GLOW\" Sticks. *We have the\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.nightclubshop.com/foam-sticks-glow-stix/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CCoQFjABOBQ&usg=AFQjCNGB2Ke1LVo7uy791yvACxGzfHVrvg","news":false,"local_pack":{},"page":3,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.nightclubshop.com/foam-sticks-glow-stix/","title":"FOAM STICKS/GLOW STIX - NightclubShop.com","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"nightclubshop.com","place_details":{},"place_pack":{}},"111":{"href":"http://www.slvlightingdirect.com/led-lighting/led-wall-lights/slv-146274-stix-led-white-reading-light-silver-grey.html","base_url":"www.slvlightingdirect.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Product Code, 146274. Name, SLV 146274 Stix LED White Reading Light Silver \nGrey. Colour, Silver Grey. Material, Aluminium. Voltage, 240 Volt. Rotation, 350\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.slvlightingdirect.com/led-lighting/led-wall-lights/slv-146274-stix-led-white-reading-light-silver-grey.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJYEEBYwWTgU&usg=AFQjCNHJV_DTA986Uy-cLZJG1WK_hWbVCg","news":false,"local_pack":{},"page":12,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.slvlightingdirect.com/led-lighting/led-wall-lights/slv-146274-stix-led-white-reading-light-silver-grey.html","title":"SLV 146274 Stix LED White Reading Light Silver Grey","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"slvlightingdirect.com","place_details":{},"place_pack":{}},"14":{"href":"http://www.lightupdrumgear.com/","base_url":"www.lightupdrumgear.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Light up drum gear including RockStix Motion Activated LED Light Up Drumsticks \n(aka FireStix) and Neptune Light Systems.","shopping":false,"dirty_url":"/url?q=http://www.lightupdrumgear.com/&sa=U&ei=rOdMU-yFF-aNywOw_oKAAQ&ved=0CDQQFjACOAo&usg=AFQjCNH4fbX9CYlpeMteyKfezL_KW6jgfA","news":false,"local_pack":{},"page":2,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.lightupdrumgear.com/","title":"LightUpDrumGear.com - RockStix Light Up Drumsticks & Neptune ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"lightupdrumgear.com","place_details":{},"place_pack":{}},"105":{"href":"http://fittipdaily.com/healthy-fast-food-on-the-go-pick-up-stix-light-menu-11371/","base_url":"fittipdaily.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Dec 16, 2013 ... On the go but hate fast food? Use this fitness tip to get the most out of your \ncalories without all the fat and chemicals. Healthy Fast Food - Pick\u00a0...","shopping":false,"dirty_url":"/url?q=http://fittipdaily.com/healthy-fast-food-on-the-go-pick-up-stix-light-menu-11371/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CPMDEBYwUzgU&usg=AFQjCNGMOhAdTNks7vlKG7cj7Y1WWZRPVg","news":false,"local_pack":{},"page":11,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://fittipdaily.com/healthy-fast-food-on-the-go-pick-up-stix-light-menu-11371/","title":"Healthy Fast Food On The Go \u2013 Pick Up Stix Light Menu - Fit Tip Daily","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"fittipdaily.com","place_details":{},"place_pack":{}},"99":{"href":"http://www.evapepens.com/shop/all-in-one/trippy-stix-essential-oil-vaporizer/","base_url":"www.evapepens.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Got Vape is proud to introduce, the Trippy Stix Essential Oil Vaporizer; the first ... \nThe battery LED light will flash 3 times and remain lit on the 4th flash until your\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.evapepens.com/shop/all-in-one/trippy-stix-essential-oil-vaporizer/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CNIDEBYwTTgU&usg=AFQjCNHW9M172GRhRClYyUiZLyO8z570xQ","news":false,"local_pack":{},"page":10,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.evapepens.com/shop/all-in-one/trippy-stix-essential-oil-vaporizer/","title":"Trippy Stix Essential Oil Vaporizer | E-Vape Pens","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"evapepens.com","place_details":{},"place_pack":{}},"20":{"href":"http://www.frostproductions.biz/portfolio/moma-light-stix","base_url":"www.frostproductions.biz","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"MOMA Light Stix. Video Decor. About Frost. Frost began more than 30 years ago \nas a special event lighting company. Since that time it has grown into a national\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.frostproductions.biz/portfolio/moma-light-stix&sa=U&ei=rOdMU-yFF-aNywOw_oKAAQ&ved=0CFYQFjAIOAo&usg=AFQjCNHHukWnWDBOwr2UERyH_Nr1Dc26Sw","news":false,"local_pack":{},"page":2,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.frostproductions.biz/portfolio/moma-light-stix","title":"MOMA Light Stix | Frost Productions","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"frostproductions.biz","place_details":{},"place_pack":{}},"11":{"href":"http://science.howstuffworks.com/innovation/everyday-innovations/light-stick2.htm","base_url":"science.howstuffworks.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Light sticks are just one application of an important natural phenomenon -- \nluminescence. Generally speaking, luminescence is any emission of light that is \nnot\u00a0...","shopping":false,"dirty_url":"/url?q=http://science.howstuffworks.com/innovation/everyday-innovations/light-stick2.htm&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CG4QFjAO&usg=AFQjCNGk4gLrclT3tu1KljFF0G2IRqG7Jw","news":false,"local_pack":{},"page":1,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://science.howstuffworks.com/innovation/everyday-innovations/light-stick2.htm","title":"HowStuffWorks \"Light Stick Activation\"","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"howstuffworks.com","place_details":{},"place_pack":{}},"102":{"href":"https://store.litegear.com/category_s/1826.htm","base_url":"store.litegear.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"The end result is a beautiful finished light fixture in any length up to 1 meter long. \nSeveral lengths can be positioned together or cut at an angle to fit into nearly\u00a0...","shopping":false,"dirty_url":"/url?q=https://store.litegear.com/category_s/1826.htm&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0COMDEBYwUDgU&usg=AFQjCNEavvhZXIShQEaIVwrIJEN9W8sE3A","news":false,"local_pack":{},"page":11,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"https://store.litegear.com/category_s/1826.htm","title":"LED | LiteStix - Store - LiteGear","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"litegear.com","place_details":{},"place_pack":{}},"96":{"href":"http://www.lazerbrite.com/","base_url":"www.lazerbrite.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"glow sticks & chem lights Each LazerBrite\u00ae unit can emulate chemical glow sticks, function as a wide-angle flashlight, separate into two lights for sharing, and\n\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.lazerbrite.com/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CMEDEBYwSjgU&usg=AFQjCNFBBZn_yPQt_3nb-10gN-wmPMK7_g","news":false,"local_pack":{},"page":10,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.lazerbrite.com/","title":"The Best Tactical Flashlight & Modular LED System Available","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"lazerbrite.com","place_details":{},"place_pack":{}},"87":{"href":"http://www.lhwfoods.com/products/","base_url":"www.lhwfoods.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Then, we have vacuumed sealed these single serve Stix for maximum freshness. \n... flax stix Ground Flaxseed is very susceptible to deterioration by both light\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.lhwfoods.com/products/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CIwDEBYwQTgU&usg=AFQjCNFJPeGdX4NzoXSw5zyb3nBZMWBLqg","news":false,"local_pack":{},"page":9,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.lhwfoods.com/products/","title":"Flax Stix Chia Stix Flax n' Chia Stix Health and Convenience","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"lhwfoods.com","place_details":{},"place_pack":{}},"93":{"href":"http://www.homedepot.com/p/BAZZ-LED-White-Under-Cabinet-3-Sticks-Kit-LED102/203258703","base_url":"www.homedepot.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Visit The Home Depot to buy BAZZ Under Cabinet LED 3 Sticks kit LED102at ... \nWith its super-bright SMD LED, the Bazz stick lights offer over 30,000 hours of\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.homedepot.com/p/BAZZ-LED-White-Under-Cabinet-3-Sticks-Kit-LED102/203258703&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CK8DEBYwRzgU&usg=AFQjCNE_EaXClZL4dunbsvgl721WVoy7kQ","news":false,"local_pack":{},"page":10,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.homedepot.com/p/BAZZ-LED-White-Under-Cabinet-3-Sticks-Kit-LED102/203258703","title":"BAZZ LED White Under Cabinet 3 Sticks Kit-LED102 at The Home ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"homedepot.com","place_details":{},"place_pack":{}},"84":{"href":"http://www.trauthdairy.com/02_nov_snow_cream_stix.shtml","base_url":"www.trauthdairy.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Snow Cream Stix - Vanilla Flavored Stix. ... Cream Bars \u00b7 Fudge Bars \u00b7 Fudge Stix Light Fudge Stix Ice Cream Sandwiches \u00b7 Mini Ice Cream Sandwiches","shopping":false,"dirty_url":"/url?q=http://www.trauthdairy.com/02_nov_snow_cream_stix.shtml&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CPsCEBYwPjgU&usg=AFQjCNHN_kEsi1w3D6B3aQpXauBf3EAAjA","news":false,"local_pack":{},"page":9,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.trauthdairy.com/02_nov_snow_cream_stix.shtml","title":"Mayfield Dairy - Frozen Novelties - Snow Cream Stix - Trauth Dairy","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"trauthdairy.com","place_details":{},"place_pack":{}},"90":{"href":"http://stealthstix.com/","base_url":"stealthstix.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Stealth Stix Push Poles. Absolutely The Strongest Push Pole Available. Stealth Stix Push Pole. Foam Grips Just In The Right Locations For A Firm Hold. Light\u00a0...","shopping":false,"dirty_url":"/url?q=http://stealthstix.com/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJ4DEBYwRDgU&usg=AFQjCNG9BITeSH4-Kwt68zB613IG6Q2p8w","news":false,"local_pack":{},"page":10,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://stealthstix.com/","title":"Stealth Stix","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"stealthstix.com","place_details":{},"place_pack":{}},"78":{"href":"http://www.dhgate.com/wholesale/led+sticks.html","base_url":"www.dhgate.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"2535 items ... Brand new Low price Telescopic LED Glow Stick Flash Light Toy fluorescent \nSword concert Christmas Carnival Toys 20pcs lot free shipping.","shopping":false,"dirty_url":"/url?q=http://www.dhgate.com/wholesale/led%2Bsticks.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CNgCEBYwODgU&usg=AFQjCNEz-dXwAxln7PK8WQFyKbtleY43HQ","news":false,"local_pack":{},"page":8,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.dhgate.com/wholesale/led%2Bsticks.html","title":"Wholesale Led Sticks - Buy Cheap Led Sticks from Best Led Sticks ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"dhgate.com","place_details":{},"place_pack":{}},"75":{"href":"http://www.modalight.com/kandi-stix.html","base_url":"www.modalight.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"KANDI STIX 6 is the ultimate choice for replacing T5 light sources and achieving \nconsistent linear illumination. Each KANDI STIX 6 circuit board has 6 high\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.modalight.com/kandi-stix.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CMgCEBYwNTgU&usg=AFQjCNEy8CGnwYdcOhFyTBp-PGa-jDdMBw","news":false,"local_pack":{},"page":8,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.modalight.com/kandi-stix.html","title":"KANDI STIX - Moda Light","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"modalight.com","place_details":{},"place_pack":{}},"81":{"href":"http://reviews.avon.com/5588/37575/reviews.htm?sort=rating","base_url":"reviews.avon.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Avon product reviews and customer ratings for Bright Lights Shadow Stix. Read \nand compare experiences customers have had with AVON products.","shopping":false,"dirty_url":"/url?q=http://reviews.avon.com/5588/37575/reviews.htm%3Fsort%3Drating&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0COsCEBYwOzgU&usg=AFQjCNFKThoU0RAP3dUVvpmU1QPk8WixEw","news":false,"local_pack":{},"page":9,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://reviews.avon.com/5588/37575/reviews.htm%3Fsort%3Drating","title":"Avon - Bright Lights Shadow Stix customer reviews - product reviews ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"avon.com","place_details":{},"place_pack":{}},"69":{"href":"http://www.premierglow.com/glow-sticks.html","base_url":"www.premierglow.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Items 1 - 13 of 13 ... Glow sticks sold at wholesale prices available in a variety of packages and colors\n. Get your favorite glow sticks for your next event or party.","shopping":false,"dirty_url":"/url?q=http://www.premierglow.com/glow-sticks.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CKoCEBYwLzgU&usg=AFQjCNHHngaZ1WwCJ3EQE3xu0fown63qIA","news":false,"local_pack":{},"page":7,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.premierglow.com/glow-sticks.html","title":"Glow Sticks > Bulk > Wholesale > Glow Light Stick - Premier Glow","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"premierglow.com","place_details":{},"place_pack":{}},"72":{"href":"http://www.musicarts.com/Firestix-Light-up-Drumsticks-491039-i1421624.mac","base_url":"www.musicarts.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Get the guaranteed lowest price on the Firestix Light up Drumsticks at Music & \nArts. Nobody has more new and used music instruments and gear.","shopping":false,"dirty_url":"/url?q=http://www.musicarts.com/Firestix-Light-up-Drumsticks-491039-i1421624.mac&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CLkCEBYwMjgU&usg=AFQjCNHRX-ZWKU_dsWb9RsJXdeeJ3lR3pw","news":false,"local_pack":{},"page":8,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.musicarts.com/Firestix-Light-up-Drumsticks-491039-i1421624.mac","title":"Firestix Light up Drumsticks | Music & Arts","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"musicarts.com","place_details":{},"place_pack":{}},"66":{"href":"http://www.stumpsparty.com/event-supplies/team-supplies/noisemakers/spirit-sticks/50037","base_url":"www.stumpsparty.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Imprint inflatable thunder sticks with your favorite team to create a unique ... \nInflatable Spirit Stix. Starting at: $1.69 pair. Unimprinted Yellow Light Up Bam \nBams.","shopping":false,"dirty_url":"/url?q=http://www.stumpsparty.com/event-supplies/team-supplies/noisemakers/spirit-sticks/50037&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJoCEBYwLDgU&usg=AFQjCNFdxUrdgB9yDrjJZSkSzMsQa6_P-w","news":false,"local_pack":{},"page":7,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.stumpsparty.com/event-supplies/team-supplies/noisemakers/spirit-sticks/50037","title":"Inflatable Spirit Sticks, Personalized Thunder Sticks | Stumps Party","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"stumpsparty.com","place_details":{},"place_pack":{}},"57":{"href":"http://www.bloody-disgusting.com/legacy/showthread.php?t=95460","base_url":"www.bloody-disgusting.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Star Wars Light-Up 'Chop Sabers' (chop stix) Collectibles, figures, and \nmemorabilia.","shopping":false,"dirty_url":"/url?q=http://www.bloody-disgusting.com/legacy/showthread.php%3Ft%3D95460&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0COYBEBYwIzgU&usg=AFQjCNGgnMmghAAMPbEflO8Tk4Zal4RmkA","news":false,"local_pack":{},"page":6,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.bloody-disgusting.com/legacy/showthread.php%3Ft%3D95460","title":"Star Wars Light-Up 'Chop Sabers' (chop stix) - BLOODY-DISGUSTING","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"bloody-disgusting.com","place_details":{},"place_pack":{}},"63":{"href":"http://www.dailymotion.com/video/x19275s_industrial-grade-light-sticks-and-how-they-function-training-video-training_lifestyle","base_url":"www.dailymotion.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Nite-Stix dot com offers six inch Industrial Glow Sticks which are perfect for ...  Industrial Grade ...","shopping":false,"dirty_url":"/url?q=http://www.dailymotion.com/video/x19275s_industrial-grade-light-sticks-and-how-they-function-training-video-training_lifestyle&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CIsCELcCMCk4FA&usg=AFQjCNEVJJXSmPXykEOJKFfF1G9FIchdoA","news":false,"local_pack":{},"page":7,"micro_format":true,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.dailymotion.com/video/x19275s_industrial-grade-light-sticks-and-how-they-function-training-video-training_lifestyle","title":"Industrial Grade Light Sticks and How They Function - Training ...","blog_posts":false,"authorship":false,"indented":0,"video":true,"authorship_details":{},"base_domain":"dailymotion.com","place_details":{},"place_pack":{}},"54":{"href":"http://www.etsy.com/market/light_stix","base_url":"www.etsy.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Shop outside the big box, with unique items for light stix from thousands of \nindependent designers and vintage collectors on Etsy.","shopping":false,"dirty_url":"/url?q=http://www.etsy.com/market/light_stix&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CNYBEBYwIDgU&usg=AFQjCNERJ5OTC9fPcIGYM-NQrMAuV3ra7Q","news":false,"local_pack":{},"page":6,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.etsy.com/market/light_stix","title":"Popular items for light stix on Etsy","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"etsy.com","place_details":{},"place_pack":{}},"60":{"href":"http://honeystixandagavestix.glorybee.com/content/honeystix-flavors","base_url":"honeystixandagavestix.glorybee.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"HoneyStix Flavors. Amaretto (very light color); Blackberry (dark purple color); \nCaramel (tan color); Cinnamon (red stripe); Chocolate (dark brown); Clover (\nnatural\u00a0...","shopping":false,"dirty_url":"/url?q=http://honeystixandagavestix.glorybee.com/content/honeystix-flavors&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CPgBEBYwJjgU&usg=AFQjCNFflAmFT3P1nvwEmSKjqoVS5-jHjg","news":false,"local_pack":{},"page":7,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://honeystixandagavestix.glorybee.com/content/honeystix-flavors","title":"HoneyStix Flavors, Honey Stix and Agave Stix, by GloryBee","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"glorybee.com","place_details":{},"place_pack":{}},"48":{"href":"http://www.musicradar.com/us/news/drums/vic-firth-usb-powered-led-lite-stix-light-up-the-stage-201217","base_url":"www.musicradar.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Mar 24, 2009 ... 7 lights, 3 strobes, 2 sticks\u2026 for \u00a3209!. Drum stick giants Vic Firth have taken the \nhumble hickory hitter to a whole new level: by addin.","shopping":false,"dirty_url":"/url?q=http://www.musicradar.com/us/news/drums/vic-firth-usb-powered-led-lite-stix-light-up-the-stage-201217&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CLcBEBYwGjgU&usg=AFQjCNEXvSPJNBzWwpTLoVwbRHn_o8m4bA","news":false,"local_pack":{},"page":5,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.musicradar.com/us/news/drums/vic-firth-usb-powered-led-lite-stix-light-up-the-stage-201217","title":"Vic Firth USB-powered LED Lite Stix light up the stage ... - MusicRadar","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"musicradar.com","place_details":{},"place_pack":{}},"51":{"href":"https://shop.flabulb.com/index.cfm?fuseaction=product.display&product_ID=800&ParentCat=740","base_url":"shop.flabulb.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"TASK/WORK LIGHT 60 LEDs. OPERATE 3+ HOURS ON A SINGLE CHARGE. \nINDOOR/OUTDOOR USE/ 25'W X 25'D LIGHT SPREAD. WATER RESISTANT.","shopping":false,"dirty_url":"/url?q=https://shop.flabulb.com/index.cfm%3Ffuseaction%3Dproduct.display%26product_ID%3D800%26ParentCat%3D740&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CMgBEBYwHTgU&usg=AFQjCNHvoi9i_qO6mA5mJvo0XNESU4vJKw","news":false,"local_pack":{},"page":6,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"https://shop.flabulb.com/index.cfm%3Ffuseaction%3Dproduct.display%26product_ID%3D800%26ParentCat%3D740","title":"LMBSR LED RECHARGEABLE LIGHT STIX - Induction - Florida ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"flabulb.com","place_details":{},"place_pack":{}},"39":{"href":"https://www.uvpaqlite.com/tooblite.html","base_url":"www.uvpaqlite.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Reusable Glow Sticks- What makes Tooblites reusable glow sticks? ... If it is \nbright enough for you to see the Tooblite is absorbing light. So even on a cloudy\n\u00a0...","shopping":false,"dirty_url":"/url?q=https://www.uvpaqlite.com/tooblite.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CIcBEBYwETgU&usg=AFQjCNEqHBQKzjq4BjWE-yyoJ2tdxQH_tg","news":false,"local_pack":{},"page":4,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"https://www.uvpaqlite.com/tooblite.html","title":"Reusable Glow Sticks - 4EverLight - Home of the Amazing Light ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"uvpaqlite.com","place_details":{},"place_pack":{}},"45":{"href":"http://asupersavvysaver.com/2012/06/14/free-4c-totally-light-stix-or-tote-bag-mir/","base_url":"asupersavvysaver.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Jun 14, 2012 ... Simply enclose 2 proofs of purchase (UPC Barcodes) from any 4C totally Light Stix AND Store cash Register Receipt, along with YOUR mailing\u00a0...","shopping":false,"dirty_url":"/url?q=http://asupersavvysaver.com/2012/06/14/free-4c-totally-light-stix-or-tote-bag-mir/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CKYBEBYwFzgU&usg=AFQjCNEWr2qTVMrMrRT5eziB8kv-wdBOxQ","news":false,"local_pack":{},"page":5,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://asupersavvysaver.com/2012/06/14/free-4c-totally-light-stix-or-tote-bag-mir/","title":"Free 4C Totally Light Stix or Tote Bag MIR - A Super Savvy Saver","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"asupersavvysaver.com","place_details":{},"place_pack":{}},"36":{"href":"http://creatingreadersandwriters.blogspot.com/2013/03/wikki-stix-and-finger-lights.html","base_url":"creatingreadersandwriters.blogspot.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Mar 8, 2013 ... For years now, I've been using Wikki Stix and Finger Lights with K/1 kids in small \nreading groups. The Wikki Stix are nothing more than colorful\u00a0...","shopping":false,"dirty_url":"/url?q=http://creatingreadersandwriters.blogspot.com/2013/03/wikki-stix-and-finger-lights.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CHMQFjAOOBQ&usg=AFQjCNHGtlYp2Jfj74vRrPqhxlIMLzqdNg","news":false,"local_pack":{},"page":4,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://creatingreadersandwriters.blogspot.com/2013/03/wikki-stix-and-finger-lights.html","title":"Creating Readers and Writers: Wikki Stix and Finger Lights","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"blogspot.com","place_details":{},"place_pack":{}},"42":{"href":"http://www.wikkistix.com/blog/crafts-for-kids/valentines-day-light-decor-for-kids/","base_url":"www.wikkistix.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Feb 4, 2014 ... From windows and doors to lights and patio furniture \u2013 my kids would cover the ... \nIf the light is covered in dust, the Wikki Stix will not attach.","shopping":false,"dirty_url":"/url?q=http://www.wikkistix.com/blog/crafts-for-kids/valentines-day-light-decor-for-kids/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJQBEBYwFDgU&usg=AFQjCNFo1YLDNcV8vv85jbp5KONUIG3dtg","news":false,"local_pack":{},"page":5,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.wikkistix.com/blog/crafts-for-kids/valentines-day-light-decor-for-kids/","title":"Valentine's Day Light D\u00e9cor for Kids - Wikki Stix Educational Toys ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"wikkistix.com","place_details":{},"place_pack":{}},"8":{"href":"http://glowproducts.com/batteryoperated/7inchledlightstick/","base_url":"glowproducts.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"7 Inch LED Light Sticks are great re-usable battery operated light sticks. \nGlowproducts.com carries a variety of light sticks and glow products.","shopping":false,"dirty_url":"/url?q=http://glowproducts.com/batteryoperated/7inchledlightstick/&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CF4QFjAL&usg=AFQjCNH6e2sVN33-XnQUaugLwCdPsazUdg","news":false,"local_pack":{},"page":1,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://glowproducts.com/batteryoperated/7inchledlightstick/","title":"7 Inch LED Light Sticks | Glowproducts.com","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"glowproducts.com","place_details":{},"place_pack":{}},"27":{"href":"http://www.musiciansfriend.com/drums-percussion/firestix-light-up-drumsticks","base_url":"www.musiciansfriend.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Get the guaranteed best price on Drum Sticks like the Firestix Light up Drumsticks \nat Musicians Friend. Get a low price and free shipping on thousands of items.","shopping":false,"dirty_url":"/url?q=http://www.musiciansfriend.com/drums-percussion/firestix-light-up-drumsticks&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CD8QFjAFOBQ&usg=AFQjCNFAyu5DGTClXOmnNQ_8q2trawqvNg","news":false,"local_pack":{},"page":3,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.musiciansfriend.com/drums-percussion/firestix-light-up-drumsticks","title":"Firestix Light up Drumsticks | Musician's Friend","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"musiciansfriend.com","place_details":{},"place_pack":{}},"118":{"href":"http://www.ehow.com/how_5729220_make-sticks-light-up-again.html","base_url":"www.ehow.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"How to Make Glow Sticks Light Up Again. Whether used for safety, night diving, \nnight sports, concerts, parties or special events, glow sticks provide convenient\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.ehow.com/how_5729220_make-sticks-light-up-again.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CL4EEBYwYDgU&usg=AFQjCNFjFF2BRF1fact0vI5q3I9bUKHLKg","news":false,"local_pack":{},"page":12,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.ehow.com/how_5729220_make-sticks-light-up-again.html","title":"How to Make Glow Sticks Light Up Again | eHow","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"ehow.com","place_details":{},"place_pack":{}},"5":{"href":"http://www.4cstore.com/detail/TCL+41387-41600-8","base_url":"www.4cstore.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"4C Totally Light Energy Rush-Variety Pk- 18 Stix, 87-41600-8,4CStore.com.","shopping":false,"dirty_url":"/url?q=http://www.4cstore.com/detail/TCL%2B41387-41600-8&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CE0QFjAI&usg=AFQjCNGUdPqmCTQiBC0QGZzM-2poQd4d2A","news":false,"local_pack":{},"page":1,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.4cstore.com/detail/TCL%2B41387-41600-8","title":"4C Totally Light Energy Rush-Variety Pk- 18 Stix | 4CStore.com","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"4cstore.com","place_details":{},"place_pack":{}},"33":{"href":"https://myspace.com/swisschris777/mixes/classic-light-stix-391657","base_url":"myspace.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Feb 21, 2014 ... Classic - light stix by Swiss Chris 777 (swisschris777). Stream music on Myspace\n, a place where people come to connect, discover, and share.","shopping":false,"dirty_url":"/url?q=https://myspace.com/swisschris777/mixes/classic-light-stix-391657&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CGIQFjALOBQ&usg=AFQjCNHvQdY_HCDLmDpEE1Omj7c_ExMAFw","news":false,"local_pack":{},"page":4,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"https://myspace.com/swisschris777/mixes/classic-light-stix-391657","title":"Classic - light stix By Swiss Chris 777 (swisschris777) on Myspace","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"myspace.com","place_details":{},"place_pack":{}},"121":{"href":"http://www.azlyrics.com/lyrics/deertick/mrsticks.html","base_url":"www.azlyrics.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Oh, Mr. Sticks, with the hug and kiss. You may say goodbye to all you've ever \nknown. Oh, Mr. Mist, how's your life like this? Oh, the light that's at the end is the\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.azlyrics.com/lyrics/deertick/mrsticks.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CNAEEBYwYzgU&usg=AFQjCNFXDk7VPKb0-OhPJe6zZflGpWJQzw","news":false,"local_pack":{},"page":13,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.azlyrics.com/lyrics/deertick/mrsticks.html","title":"DEER TICK LYRICS - Mr. Sticks - A-Z Lyrics","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"azlyrics.com","place_details":{},"place_pack":{}},"109":{"href":"http://www.beachbikes.net/products/Stix-Sparx-Valve-Stem-Light-Rainbow-1318.html","base_url":"www.beachbikes.net","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Stix Sparx Valve Stem - The Stix Sparx Valve Stem Lights, Rainbow are a cool, \ncolorful way to light your night rides.","shopping":false,"dirty_url":"/url?q=http://www.beachbikes.net/products/Stix-Sparx-Valve-Stem-Light-Rainbow-1318.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CIsEEBYwVzgU&usg=AFQjCNEusTBRRyJWjpg-8yFuutG9atBb2w","news":false,"local_pack":{},"page":11,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.beachbikes.net/products/Stix-Sparx-Valve-Stem-Light-Rainbow-1318.html","title":"Bicycle LIghts - Stix Sparx Valve Stem Light, Rainbow | Beachbikes","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"beachbikes.net","place_details":{},"place_pack":{}},"24":{"href":"http://q-stix.myshopify.com/products/34-bauer-apx-light-grip-87-flex-45","base_url":"q-stix.myshopify.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Please note that this shaft is from a broken one piece stick and may need to be \ncut additionally to take a blade. some shafts can not take tapered blades. I can.","shopping":false,"dirty_url":"/url?q=http://q-stix.myshopify.com/products/34-bauer-apx-light-grip-87-flex-45&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CDAQFjACOBQ&usg=AFQjCNHNQ4vftw9DQHh5YdcgWPkq75VxEg","news":false,"local_pack":{},"page":3,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://q-stix.myshopify.com/products/34-bauer-apx-light-grip-87-flex-45","title":"34 - Bauer apx - light grip - 87 flex - 45\" \u2013 Q-stix","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"myshopify.com","place_details":{},"place_pack":{}},"115":{"href":"http://www.amazon.co.uk/FireStix-Light-Drumsticks-Brilliant-Blue/dp/B000XZX4R8","base_url":"www.amazon.co.uk","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Motion Activated Light Up Drumsticks, Frosted Tips for Great Effects! Made from \nstrong LEXAN (TM) Plastic! (as used in riot shields); Ideal for the Professional\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.amazon.co.uk/FireStix-Light-Drumsticks-Brilliant-Blue/dp/B000XZX4R8&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CK0EEBYwXTgU&usg=AFQjCNEbFLrXKEDkn9ai7zaepWdAt_p4Vw","news":false,"local_pack":{},"page":12,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.amazon.co.uk/FireStix-Light-Drumsticks-Brilliant-Blue/dp/B000XZX4R8","title":"FireStix - LED Light Up Drumsticks - Brilliant Blue: Amazon.co.uk ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"amazon.co.uk","place_details":{},"place_pack":{}},"2":{"href":"http://backpackinglight.com/cgi-bin/backpackinglight/stix_carbon_fiber_trekking_poles_08.html","base_url":"backpackinglight.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Light, stiff and strong - these single piece, carbon fiber trekking poles come with \nnew features, improved design, and a lower price in 2009. Overview. STIX\u00a0...","shopping":false,"dirty_url":"/url?q=http://backpackinglight.com/cgi-bin/backpackinglight/stix_carbon_fiber_trekking_poles_08.html&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CDUQFjAB&usg=AFQjCNGHI47Q8uo4CYKQDhmPpGBxcwgnWw","news":false,"local_pack":{},"page":1,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://backpackinglight.com/cgi-bin/backpackinglight/stix_carbon_fiber_trekking_poles_08.html","title":"Backpacking Light STIX Carbon Fiber Trekking Poles - CLOSEOUT ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"backpackinglight.com","place_details":{},"place_pack":{}},"30":{"href":"http://caloriecount.about.com/calories-4c-totally-light-go-fruit-i147592","base_url":"caloriecount.about.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Curious about how many calories are in Totally Light To Go Fruit Punch Stix, 4C? \nGet nutrition information and sign up for a free online diet program at\u00a0...","shopping":false,"dirty_url":"/url?q=http://caloriecount.about.com/calories-4c-totally-light-go-fruit-i147592&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CFMQFjAIOBQ&usg=AFQjCNFhCepfYfcfh1to-MpjHWP5zg5RBw","news":false,"local_pack":{},"page":4,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://caloriecount.about.com/calories-4c-totally-light-go-fruit-i147592","title":"Calories in 4C - Totally Light To Go Fruit Punch Stix, 4C | Nutrition ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"about.com","place_details":{},"place_pack":{}},"18":{"href":"http://www.glow-glow.com/","base_url":"www.glow-glow.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Glow-glow.com. China professional manufacturer for all kinds of chemical glow in \nthe dark novelty toys, glow sticks, light sticks, lite stix, lightsticks, glowing sticks,\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.glow-glow.com/&sa=U&ei=rOdMU-yFF-aNywOw_oKAAQ&ved=0CEoQFjAGOAo&usg=AFQjCNElyXsckQsU7oUNyKK52JBSoEH5bw","news":false,"local_pack":{},"page":2,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.glow-glow.com/","title":"glow sticks,light sticks China manufacturer,glowsticks,lightstick,glow ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"glow-glow.com","place_details":{},"place_pack":{}},"106":{"href":"http://www.drugs.com/vet/stypt-stix.html","base_url":"www.drugs.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Learn about Stypt-Stix for animal usage including: active ingredients, ... in the \nclosed package, in a dry place, protected from light, in the closed package.","shopping":false,"dirty_url":"/url?q=http://www.drugs.com/vet/stypt-stix.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CPkDEBYwVDgU&usg=AFQjCNE82-w2xz5TYPSTyrsIQFmtnLltDg","news":false,"local_pack":{},"page":11,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.drugs.com/vet/stypt-stix.html","title":"Stypt-Stix for Animal Use - Drugs.com","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"drugs.com","place_details":{},"place_pack":{}},"21":{"href":"http://www.lightinguniverse.com/semi-flush-ceiling-lights/nuvo-lighting-62-119-1-light-stix-led-semi-flush-ceiling-light_11200473.html","base_url":"www.lightinguniverse.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"1 Light LED Semi Flush Ceiling Light. Dramatic fixtures that may be mounted on  the wall or ...","shopping":false,"dirty_url":"/url?q=http://www.lightinguniverse.com/semi-flush-ceiling-lights/nuvo-lighting-62-119-1-light-stix-led-semi-flush-ceiling-light_11200473.html&sa=U&ei=rOdMU-yFF-aNywOw_oKAAQ&ved=0CFwQtwIwCTgK&usg=AFQjCNEwSofQ3eYuCany1D_kCTi2sb6YGw","news":false,"local_pack":{},"page":2,"micro_format":true,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.lightinguniverse.com/semi-flush-ceiling-lights/nuvo-lighting-62-119-1-light-stix-led-semi-flush-ceiling-light_11200473.html","title":"Nuvo Lighting 62-119 1 Light Stix LED Semi Flush Ceiling Light","blog_posts":false,"authorship":false,"indented":0,"video":true,"authorship_details":{},"base_domain":"lightinguniverse.com","place_details":{},"place_pack":{}},"112":{"href":"http://www.wikihow.com/Start-a-Fire-with-Sticks","base_url":"www.wikihow.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Start a Fire with Sticks Step 5.jpg. 5. Make a fireboard. The fireboard and the drill \nboth need to be made from light, dry, non-resinous wood. The best wood for this\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.wikihow.com/Start-a-Fire-with-Sticks&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJsEEBYwWjgU&usg=AFQjCNGd5APEEd1b9iUk1e64_lKN3S-vtw","news":false,"local_pack":{},"page":12,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.wikihow.com/Start-a-Fire-with-Sticks","title":"How to Start a Fire with Sticks: 18 Steps (with Pictures)","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"wikihow.com","place_details":{},"place_pack":{}},"15":{"href":"http://www.whatonearthcatalog.com/whatonearth/Toys-Games_3LA/View-All-Toys-Games_3LB/Item_s-LIGHT-UP-DRUM-STIX-BLUE-SET-OF-2_CC8412.html","base_url":"www.whatonearthcatalog.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"These virtually indestructible polycarbonate drumsticks are professional grade, \nmeaning you can hammer out the backbeat on your kit, digital drums or\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.whatonearthcatalog.com/whatonearth/Toys-Games_3LA/View-All-Toys-Games_3LB/Item_s-LIGHT-UP-DRUM-STIX-BLUE-SET-OF-2_CC8412.html&sa=U&ei=rOdMU-yFF-aNywOw_oKAAQ&ved=0CDkQFjADOAo&usg=AFQjCNFS6kgxwzPb_M5zDh9UvBRhqG8a_A","news":false,"local_pack":{},"page":2,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.whatonearthcatalog.com/whatonearth/Toys-Games_3LA/View-All-Toys-Games_3LB/Item_s-LIGHT-UP-DRUM-STIX-BLUE-SET-OF-2_CC8412.html","title":"LIGHT UP DRUM STIX BLUE (SET OF 2) at What on Earth | CC8412","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"whatonearthcatalog.com","place_details":{},"place_pack":{}},"97":{"href":"http://wetpixel.com/forums/index.php?showtopic=47463","base_url":"wetpixel.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"... and red filters. $100 each 1 new Stix 6\u201d arm with large floats for Fisheye light (\nSN-906) $45 The lights work fine, I am no longer using them.","shopping":false,"dirty_url":"/url?q=http://wetpixel.com/forums/index.php%3Fshowtopic%3D47463&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CMcDEBYwSzgU&usg=AFQjCNGeaRaXo9xGEMNwLLTt5I2MdL02fQ","news":false,"local_pack":{},"page":10,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://wetpixel.com/forums/index.php%3Fshowtopic%3D47463","title":"FS Fisheye Fix LED 48DX focus lights and Stix arm - Classifieds ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"wetpixel.com","place_details":{},"place_pack":{}},"12":{"href":"http://www.vicfirth.com/product/buynow/product.php?button=LITE","base_url":"www.vicfirth.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Lite Stix features an LED light in the tip for 7 color settings including green\u201a blue\u201a \nred\u201a violet\u201a aqua\u201a yellow and ice blue\u201a as well as slow\u201a medium and fast strobe\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.vicfirth.com/product/buynow/product.php%3Fbutton%3DLITE&sa=U&ei=rOdMU-yFF-aNywOw_oKAAQ&ved=0CCcQFjAAOAo&usg=AFQjCNFrSJckiK3GkJVvKf4LtV-MwoaUew","news":false,"local_pack":{},"page":2,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.vicfirth.com/product/buynow/product.php%3Fbutton%3DLITE","title":"Lite Stix - Vic Firth","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"vicfirth.com","place_details":{},"place_pack":{}},"103":{"href":"http://www.bagelstixnyc.com/","base_url":"www.bagelstixnyc.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Home-Made Granola with Plain Non Fat Yogurt & Fresh Fruit, $3.95. Oatmeal, \n$3.00. Oatmeal With Milk, $3.50.","shopping":false,"dirty_url":"/url?q=http://www.bagelstixnyc.com/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0COgDEBYwUTgU&usg=AFQjCNF3o3NDfLh-UGx2qMuRtfgjW4ZhGg","news":false,"local_pack":{},"page":11,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.bagelstixnyc.com/","title":"Bagel Stix - New York City - Order food online | Ordering Food ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"bagelstixnyc.com","place_details":{},"place_pack":{}},"88":{"href":"http://www.lavistakeno.com/page/swizzle-stix-lounge","base_url":"www.lavistakeno.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Swizzle Stix Lounge is located in LaVista Keno, offering the best priced drinks in \ntown. ... Wells - $2.20; Calls - $2.35; Draws \u2013 Miller Light and Coors Light $1.05\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.lavistakeno.com/page/swizzle-stix-lounge&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJIDEBYwQjgU&usg=AFQjCNEuVmPe5BsIAqt8M9Rk_bUODhoVKg","news":false,"local_pack":{},"page":9,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.lavistakeno.com/page/swizzle-stix-lounge","title":"Swizzle Stix Lounge \u00bb LaVista Keno","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"lavistakeno.com","place_details":{},"place_pack":{}},"100":{"href":"http://www.officedepot.com/a/products/591428/Crystal-Light-On-The-Go-Mix/","base_url":"www.officedepot.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Crystal Light On The Go Mix Sticks Peach Tea Box Of 30 Packets, Just add water \nfor a refreshing drink, Choose from a variety of great tasting flavors at Office\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.officedepot.com/a/products/591428/Crystal-Light-On-The-Go-Mix/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CNcDEBYwTjgU&usg=AFQjCNGqcotmqAXCm-raI3mI8tF7nr3BcA","news":false,"local_pack":{},"page":11,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.officedepot.com/a/products/591428/Crystal-Light-On-The-Go-Mix/","title":"Crystal Light On The Go Mix Sticks Peach Tea Box Of 30 Packets by ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"officedepot.com","place_details":{},"place_pack":{}},"94":{"href":"http://www.corestix.com/fitness/more-results-for-the-black-n-gold-girls-core-stix-challenge/","base_url":"www.corestix.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Mar 28, 2013 ... The general workouts consisted of a 5-10 minute dynamic warm up and stretch \nwith light cardio (jumping jacks, split jumping jacks, crickets, etc)\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.corestix.com/fitness/more-results-for-the-black-n-gold-girls-core-stix-challenge/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CLUDEBYwSDgU&usg=AFQjCNHAGA-WiFIfyP2NDQ-UG2c7J7B4RQ","news":false,"local_pack":{},"page":10,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.corestix.com/fitness/more-results-for-the-black-n-gold-girls-core-stix-challenge/","title":"fitness/more-results-for-the-black-n-gold-girls-core-stix-challenge","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"corestix.com","place_details":{},"place_pack":{}},"85":{"href":"http://store.drumbum.com/skuSTK-20.html","base_url":"store.drumbum.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Buy these unique Electric Light Up Drumsticks! These amazing sticks will add an \nextra special touch at your next performance!","shopping":false,"dirty_url":"/url?q=http://store.drumbum.com/skuSTK-20.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CIEDEBYwPzgU&usg=AFQjCNHc9s5FCsLXjU_2RQ0QQYDRoXYVOQ","news":false,"local_pack":{},"page":9,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://store.drumbum.com/skuSTK-20.html","title":"DRUM BUM: STICKS - Electric Light Up Drumsticks","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"drumbum.com","place_details":{},"place_pack":{}},"91":{"href":"http://www.moneysavingmaineac.com/2012/05/16/4c-totaly-light-stix-mail-in-rebate/","base_url":"www.moneysavingmaineac.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"May 16, 2012 ... Look for this tear pad mail-in rebate next time you're at Shaw's: Buy Any (2) 4C \nTotally Light Stix & Get One FREE! Simply enclose 2 proofs of\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.moneysavingmaineac.com/2012/05/16/4c-totaly-light-stix-mail-in-rebate/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CKMDEBYwRTgU&usg=AFQjCNFHPiwtUwSGpF6Rh9McC6NWPjM4cQ","news":false,"local_pack":{},"page":10,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.moneysavingmaineac.com/2012/05/16/4c-totaly-light-stix-mail-in-rebate/","title":"4C Totaly Light Stix Mail-In Rebate - Money Saving Maine-iac","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"moneysavingmaineac.com","place_details":{},"place_pack":{}},"79":{"href":"http://www.homeofpoi.com/community/ubbthreads.php/topics/931987/Flow_Light_devi_sticks.html","base_url":"www.homeofpoi.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"May 15, 2011 ... So I've had my fun with the Yoho Electro Spin stix, but was not fully satisfied with \ntheir durability and with the explosion of flo-ligths on my scene\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.homeofpoi.com/community/ubbthreads.php/topics/931987/Flow_Light_devi_sticks.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CN8CEBYwOTgU&usg=AFQjCNEyGR6N-qZTrdGqO3BtY8JNKof8mA","news":false,"local_pack":{},"page":8,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.homeofpoi.com/community/ubbthreads.php/topics/931987/Flow_Light_devi_sticks.html","title":"Flow Light devi sticks? - Home of Poi","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"homeofpoi.com","place_details":{},"place_pack":{}},"82":{"href":"http://www.cw-usa.com/flavored-drinks.html","base_url":"www.cw-usa.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Flavored drinks including Crystal Light, apple cider, cappuccino, granita, and \nsmoothie mixes, many of which come in multiple flavors and sizes. Featured\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.cw-usa.com/flavored-drinks.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CPACEBYwPDgU&usg=AFQjCNGeRoDfGXg770LrAv1yw1kr9GFhkw","news":false,"local_pack":{},"page":9,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.cw-usa.com/flavored-drinks.html","title":"Crystal Light On-The-Go, Arizona Tea Sticks, Water Enhancers and ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"cw-usa.com","place_details":{},"place_pack":{}},"76":{"href":"http://www.yeoldearcheryshoppe.com/specialty-super-stix-ultralight-stabilizers-p-715.html","base_url":"www.yeoldearcheryshoppe.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Specialty Super Stix Ultra-Light Stabilizers, Specialty Archery, Stabilizers- Ye \nOlde Archery Shoppe offers you Specialty Super Stix Ultra-Light Stabilizers,\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.yeoldearcheryshoppe.com/specialty-super-stix-ultralight-stabilizers-p-715.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CM0CEBYwNjgU&usg=AFQjCNFRs2kmF5BkMOGnwOBCJEBflnCx_Q","news":false,"local_pack":{},"page":8,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.yeoldearcheryshoppe.com/specialty-super-stix-ultralight-stabilizers-p-715.html","title":"Specialty Super Stix Ultra-Light Stabilizers - Ye Olde Archery Shoppe","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"yeoldearcheryshoppe.com","place_details":{},"place_pack":{}},"67":{"href":"http://www.glu-stix.com/shop/page/product_detail/Product/5dd3e66765a2e4bfd62492ece037430f.html","base_url":"www.glu-stix.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Hang Lights Quickly & Easily - Unique Staple Design Won't Damage Wire. An \ninexpensive holiday cable tacker staple gun. Hangs holiday lights and garland\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.glu-stix.com/shop/page/product_detail/Product/5dd3e66765a2e4bfd62492ece037430f.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJ8CEBYwLTgU&usg=AFQjCNG2LYW6qz_g59VDaeJ3sM-jtSwKEQ","news":false,"local_pack":{},"page":7,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.glu-stix.com/shop/page/product_detail/Product/5dd3e66765a2e4bfd62492ece037430f.html","title":"Holiday Light Tacker (Glu-Stix)","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"glu-stix.com","place_details":{},"place_pack":{}},"73":{"href":"http://www.glowwithus.com/","base_url":"www.glowwithus.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"YOUR Ultimate Source of Bulk Glow Sticks, Wholesale Glow Necklaces, Glow Light Sticks, Glow Bracelets, Light Up Glasses & Glow in the Dark Party Supplies!","shopping":false,"dirty_url":"/url?q=http://www.glowwithus.com/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CL8CEBYwMzgU&usg=AFQjCNHjUYo35Az7h2widC3duStw1fG1IQ","news":false,"local_pack":{},"page":8,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.glowwithus.com/","title":"Bulk Glow Sticks | Wholesale Glow Necklaces | Glow Light Sticks ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"glowwithus.com","place_details":{},"place_pack":{}},"64":{"href":"http://www.selectsafetysales.com/c-159-cyalume-light-sticks.aspx","base_url":"www.selectsafetysales.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Cyalume Light Sticks are a non-toxic chemical light stick that are safe in all \nenvironments and are used by the military, government agencies, utilities, police\n\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.selectsafetysales.com/c-159-cyalume-light-sticks.aspx&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CI8CEBYwKjgU&usg=AFQjCNGOIj5ilz5Je2xIhj3FsbLtJ3EwJA","news":false,"local_pack":{},"page":7,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.selectsafetysales.com/c-159-cyalume-light-sticks.aspx","title":"Cyalume Light Sticks - Select Safety Sales","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"selectsafetysales.com","place_details":{},"place_pack":{}},"70":{"href":"http://www.nuvolightinglights.com/product/nuvo-lighting-stix-pendant-62-113.html","base_url":"www.nuvolightinglights.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"This 3 light Pendant from the Stix collection by Nuvo will enhance your home with \na perfect mix of form and function. The features include a Patina Bronze finish\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.nuvolightinglights.com/product/nuvo-lighting-stix-pendant-62-113.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CLECEBYwMDgU&usg=AFQjCNHkn9winsekJkR-afzJgJd30uS_IA","news":false,"local_pack":{},"page":8,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.nuvolightinglights.com/product/nuvo-lighting-stix-pendant-62-113.html","title":"Nuvo Lighting Stix 3 Light Pendant in Patina Bronze 62/113 | Nuvo ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"nuvolightinglights.com","place_details":{},"place_pack":{}},"58":{"href":"http://www.glowsource.com/","base_url":"www.glowsource.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Glowsource has all your LED Light Up Novelty and Glow in the Dark Party Supply\n, including Glow Sticks, Glow Products, and Lighted Ice Cubes. Huge quantity\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.glowsource.com/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0COwBEBYwJDgU&usg=AFQjCNGo252TyZPrquqCpCG5KjfMC9VDcg","news":false,"local_pack":{},"page":6,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.glowsource.com/","title":"Glowsource.com | Glow Sticks | Light Up Ice Cubes | Light Up ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"glowsource.com","place_details":{},"place_pack":{}},"61":{"href":"http://www.wtstoyreview.com/reviews/2009/07/20/rock-stix-light-up-drum-sticks/","base_url":"www.wtstoyreview.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Jul 20, 2009 ... Rock-Stix are ultra bright LED drum sticks that illuminate with every tap. They are \nconstructed of Lexan plastic to provide professional strength\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.wtstoyreview.com/reviews/2009/07/20/rock-stix-light-up-drum-sticks/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CP4BEBYwJzgU&usg=AFQjCNEGhMuBlFeEXrPuAGVFhQYHLnN23A","news":false,"local_pack":{},"page":7,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.wtstoyreview.com/reviews/2009/07/20/rock-stix-light-up-drum-sticks/","title":"Rock-Stix Light-Up Drum Sticks | WTS Toy Review","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"wtstoyreview.com","place_details":{},"place_pack":{}},"49":{"href":"http://www.theallnaturalface.com/vegan-diva-stix-in-light-brown/","base_url":"www.theallnaturalface.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"These Diva Stix are eyeshadow crayons and can also be used as eyeliners! A \nbeautiful light brown color:). Ingredients: Organic Jojoba Oil, Candelilla Wax, \nMica\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.theallnaturalface.com/vegan-diva-stix-in-light-brown/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CL0BEBYwGzgU&usg=AFQjCNHDoRbVHmJRuylZpNTsXnf5Y9kuRQ","news":false,"local_pack":{},"page":5,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.theallnaturalface.com/vegan-diva-stix-in-light-brown/","title":"Vegan Diva Stix in Light Brown - The All Natural Face","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"theallnaturalface.com","place_details":{},"place_pack":{}},"55":{"href":"http://www.franciscom.com/product/Light_Up_Safety_Stix_Red_562496","base_url":"www.franciscom.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Batteries are included and installed. 8\" Light-Up Safety Stix. Patents Pending. \nOrder in increments of 24 pieces.","shopping":false,"dirty_url":"/url?q=http://www.franciscom.com/product/Light_Up_Safety_Stix_Red_562496&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CNsBEBYwITgU&usg=AFQjCNHZp3ItjAVg9hQimHMjkJdRW1Ry9w","news":false,"local_pack":{},"page":6,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.franciscom.com/product/Light_Up_Safety_Stix_Red_562496","title":"Promotional Light-Up Safety Stix-Red - Francis Communications","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"franciscom.com","place_details":{},"place_pack":{}},"52":{"href":"http://www.stljewishlight.com/obituaries/article_4bed76ec-f4eb-11e0-ae1e-001cc4c03286.html","base_url":"www.stljewishlight.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Oct 12, 2011 ... Ernest W. Stix, Jr., member of a prominent Jewish family which owned the Rice-Stix, Inc., a manufacturer and wholesaler of dry goods, died\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.stljewishlight.com/obituaries/article_4bed76ec-f4eb-11e0-ae1e-001cc4c03286.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CMoBEBYwHjgU&usg=AFQjCNF8d45iS_ymteKeG9BlsXUVHTYXdQ","news":false,"local_pack":{},"page":6,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.stljewishlight.com/obituaries/article_4bed76ec-f4eb-11e0-ae1e-001cc4c03286.html","title":"Ernest W. Stix, Jr. 95; businessman, artist, philanthropist - St. Louis ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"stljewishlight.com","place_details":{},"place_pack":{}},"46":{"href":"http://mccallwomenshockey.com/wp-content/uploads/2012/10/2014-Chix-Schedule1.pdf","base_url":"mccallwomenshockey.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"th. Home. Jersey Locker. Away. Jersey Locker. 4:00 p.m.. Black Stars Rec dark. 5 \nvs. N'Iice Girls light. 3. 5:10 p.m.. WSU Cougars light. 2 vs. The Breakfast Club.","shopping":false,"dirty_url":"/url?q=http://mccallwomenshockey.com/wp-content/uploads/2012/10/2014-Chix-Schedule1.pdf&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CKwBEBYwGDgU&usg=AFQjCNHUHcJEqbsBnZcaM5BYUcGdssTEWw","news":false,"local_pack":{},"page":5,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://mccallwomenshockey.com/wp-content/uploads/2012/10/2014-Chix-Schedule1.pdf","title":"Chix with Stix 2014 - McCall Ospreys","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"mccallwomenshockey.com","place_details":{},"place_pack":{}},"9":{"href":"https://www.kickstarter.com/projects/bigltd/rockstix-2-colour-change-led-light-up-drumsticks","base_url":"www.kickstarter.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Following on from the success of our light-up drumsticks we are now developing  the worlds ...","shopping":false,"dirty_url":"/url?q=https://www.kickstarter.com/projects/bigltd/rockstix-2-colour-change-led-light-up-drumsticks&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CGUQtwIwDA&usg=AFQjCNEWm5cIds00OZopMRuHPtKlYo2i1g","news":false,"local_pack":{},"page":1,"micro_format":true,"real_time":false,"image_pack_results":[],"maps":false,"url":"https://www.kickstarter.com/projects/bigltd/rockstix-2-colour-change-led-light-up-drumsticks","title":"RockStix 2 - Colour Change LED Light Up Drumsticks by Bright ...","blog_posts":false,"authorship":false,"indented":0,"video":true,"authorship_details":{},"base_domain":"kickstarter.com","place_details":{},"place_pack":{}},"37":{"href":"http://ice-pix.com/ice-pix-stix-71mm-6-pack-light-med/","base_url":"ice-pix.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Jan 9, 2012 ... Comes in assorted celluloid colors. \u2022 only Ice-Pix uses micro-suction technology \u2022 sticks via suction- no adhesive residue \u2022 never be without a\u00a0...","shopping":false,"dirty_url":"/url?q=http://ice-pix.com/ice-pix-stix-71mm-6-pack-light-med/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CHoQFjAPOBQ&usg=AFQjCNERuCyU9MKoevfKRrCAy4m9nlBYWg","news":false,"local_pack":{},"page":4,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://ice-pix.com/ice-pix-stix-71mm-6-pack-light-med/","title":"Ice Pix Stix - .71mm 6 pack - Light/Med - Solid Sound - Home of ICE ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"ice-pix.com","place_details":{},"place_pack":{}},"43":{"href":"https://marketplace.secondlife.com/p/Miyos-Rave-Hand-Glow-Stix-Light-Blue/1009636","base_url":"marketplace.secondlife.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Aug 23, 2011 ... Some peoples said... Those are the best dance stix ever seen in SL. Its coz they \nuse real texture glow effect and no script. So they are beaty\u00a0...","shopping":false,"dirty_url":"/url?q=https://marketplace.secondlife.com/p/Miyos-Rave-Hand-Glow-Stix-Light-Blue/1009636&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJoBEBYwFTgU&usg=AFQjCNEyjCnZmCAMhX4tZ587vQ28KG79rg","news":false,"local_pack":{},"page":5,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"https://marketplace.secondlife.com/p/Miyos-Rave-Hand-Glow-Stix-Light-Blue/1009636","title":"Second Life Marketplace - Miyo's Rave Hand Glow Stix Light Blue","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"secondlife.com","place_details":{},"place_pack":{}},"119":{"href":"http://rapgenius.com/Stu-Sauce-N-the-Syna-Stix","base_url":"rapgenius.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Jul 29, 2013 ... See Stu Sauce and the Syna-Stix's explanations on Rap Genius (a rap lyrics \nencyclopedia anyone can edit)","shopping":false,"dirty_url":"/url?q=http://rapgenius.com/Stu-Sauce-N-the-Syna-Stix&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CMQEEBYwYTgU&usg=AFQjCNGN832vZpKAeCnlXkrEKzjts6gxwg","news":false,"local_pack":{},"page":12,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://rapgenius.com/Stu-Sauce-N-the-Syna-Stix","title":"Stu Sauce and the Syna-Stix's profile | Rap Genius","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"rapgenius.com","place_details":{},"place_pack":{}},"6":{"href":"http://www.guitarcenter.com/Firestix-Light-up-Drumsticks-491039-i1421624.gc","base_url":"www.guitarcenter.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Firestix drumsticks are made of durable Lexan and light up upon impact for a \nbrilliant, dramatic light show effect on stage. They are perfect for drum solos!","shopping":false,"dirty_url":"/url?q=http://www.guitarcenter.com/Firestix-Light-up-Drumsticks-491039-i1421624.gc&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CFMQFjAJ&usg=AFQjCNFdCGtRRyhBlJyiDF9AmXG3hEAYug","news":false,"local_pack":{},"page":1,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.guitarcenter.com/Firestix-Light-up-Drumsticks-491039-i1421624.gc","title":"Firestix Light up Drumsticks | GuitarCenter","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"guitarcenter.com","place_details":{},"place_pack":{}},"34":{"href":"http://www.duraflame.com/products/stix-firestarter-light-bbqs-and-fireplaces","base_url":"www.duraflame.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Lighting wood log fires and BBQ fires just got easier with duraflame stix ... Two stix firelighters stack like an \u201cx\u201d to start wood fires, and the wood fire lights within\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.duraflame.com/products/stix-firestarter-light-bbqs-and-fireplaces&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CGgQFjAMOBQ&usg=AFQjCNF-gNER0kHsFoNfXFJ4YD8ozWdOtg","news":false,"local_pack":{},"page":4,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.duraflame.com/products/stix-firestarter-light-bbqs-and-fireplaces","title":"Duraflame\u00ae Stix Multi-Use Firestarters | Easier Wood Fire Starting","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"duraflame.com","place_details":{},"place_pack":{}},"40":{"href":"http://www.ionaudio.com/products/details/firestix-blue","base_url":"www.ionaudio.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Firestix are light-up drumsticks that will bring excitement to any drummer's \nperformance. Firestix aren't just glow-in-the-dark drumsticks; they really light up!","shopping":false,"dirty_url":"/url?q=http://www.ionaudio.com/products/details/firestix-blue&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CIwBEBYwEjgU&usg=AFQjCNGpWYk0Ab5nxPJjc3GblmFYa5RntQ","news":false,"local_pack":{},"page":5,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.ionaudio.com/products/details/firestix-blue","title":"Firestix Blue - Blue Light-Up Drumsticks - ION Audio - Technology ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"ionaudio.com","place_details":{},"place_pack":{}},"28":{"href":"http://www.designboom.com/project/light-stix/","base_url":"www.designboom.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Introducing Light-Stix Light-Stix details Light-Stix around the home.","shopping":false,"dirty_url":"/url?q=http://www.designboom.com/project/light-stix/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CEYQFjAGOBQ&usg=AFQjCNG-qeHVh4fuMz-Ls_Nz78NS2-FqNw","news":false,"local_pack":{},"page":3,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.designboom.com/project/light-stix/","title":"light stix - Designboom","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"designboom.com","place_details":{},"place_pack":{}},"116":{"href":"http://www.koolaidworld.com/en/4c-vitamin-stix/150-4c-vision-strawberry-kiwi-vitamin-stix.html","base_url":"www.koolaidworld.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Home \u00b7 Delivery \u00b7 Legal Notice \u00b7 Secure payment \u00b7 Privacy \u00b7 Home-4C Totally Light-Vitamin Stix-4C \"Vision\" Strawberry/Kiwi vitamin stix\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.koolaidworld.com/en/4c-vitamin-stix/150-4c-vision-strawberry-kiwi-vitamin-stix.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CLMEEBYwXjgU&usg=AFQjCNH9Bb71fuun4uH1vatEcgKzTqHgtA","news":false,"local_pack":{},"page":12,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.koolaidworld.com/en/4c-vitamin-stix/150-4c-vision-strawberry-kiwi-vitamin-stix.html","title":"4C \"Vision\" Strawberry/Kiwi vitamin stix - KoolaidWorld","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"koolaidworld.com","place_details":{},"place_pack":{}},"3":{"href":"http://www.lumivisions.net/lighting/led-lightstix-products/","base_url":"www.lumivisions.net","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"SPANLITE'S custom made LED Lightsticks can be made Cool, Warm or Hot (K \nTemp Selectable) or Color Changing and come with LumiVision's US warranty.","shopping":false,"dirty_url":"/url?q=http://www.lumivisions.net/lighting/led-lightstix-products/&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CDsQFjAC&usg=AFQjCNHv7f8f-udV1BmLPGpv-hVUqlRmSg","news":false,"local_pack":{},"page":1,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.lumivisions.net/lighting/led-lightstix-products/","title":"LED Light Stix Products | LumiVisions Architectural Elements, Inc.","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"lumivisions.net","place_details":{},"place_pack":{}},"31":{"href":"http://caloriecount.about.com/calories-4c-totally-light-go-cranberry-i147593","base_url":"caloriecount.about.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Curious about how many calories are in Totally Light To Go Cranberry \nPomegranate Antioxidant Stix, 4C? Get nutrition information and sign up for a \nfree online\u00a0...","shopping":false,"dirty_url":"/url?q=http://caloriecount.about.com/calories-4c-totally-light-go-cranberry-i147593&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CFgQFjAJOBQ&usg=AFQjCNF1xyiK98Dsm5Nd1EZIHBcrvpd6lA","news":false,"local_pack":{},"page":4,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://caloriecount.about.com/calories-4c-totally-light-go-cranberry-i147593","title":"Totally Light To Go Cranberry Pomegranate Antioxidant Stix","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"about.com","place_details":{},"place_pack":{}},"19":{"href":"http://en.wikipedia.org/wiki/Glow_stick","base_url":"en.wikipedia.org","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"The light cannot be turned off, and can be used only once. Glow sticks are often \nused for recreation, but may also be relied upon for light during military, police,\u00a0...","shopping":false,"dirty_url":"/url?q=http://en.wikipedia.org/wiki/Glow_stick&sa=U&ei=rOdMU-yFF-aNywOw_oKAAQ&ved=0CFAQFjAHOAo&usg=AFQjCNFJzaiDI6O51WCWBLl5ELBf2GZAyg","news":false,"local_pack":{},"page":2,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://en.wikipedia.org/wiki/Glow_stick","title":"Glow stick - Wikipedia, the free encyclopedia","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"wikipedia.org","place_details":{},"place_pack":{}},"25":{"href":"http://www.outdooroutlet.com/product/14122/Texsport-Glo-Light-Stix","base_url":"www.outdooroutlet.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Simple, yet so nice! Nothing lights up a dark night or makes the kids smile like a \nnice glow stick ! Put some smiles on some faces today!","shopping":false,"dirty_url":"/url?q=http://www.outdooroutlet.com/product/14122/Texsport-Glo-Light-Stix&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CDUQFjADOBQ&usg=AFQjCNGZbTytwq576hX2vvlpl4vVOcZyDw","news":false,"local_pack":{},"page":3,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.outdooroutlet.com/product/14122/Texsport-Glo-Light-Stix","title":"Outdoor Outlet - Texsport Glo Light Stix","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"outdooroutlet.com","place_details":{},"place_pack":{}},"16":{"href":"http://www.lightupandjuggle.com/store/index.php?main_page=product_info&products_id=30","base_url":"www.lightupandjuggle.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Juggle Sticks are known by a host of other names: Devil Sticks, Rhythm Sticks, \nand Spin Stix to name a few. Whatever you choose to call them though, Light Up\n\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.lightupandjuggle.com/store/index.php%3Fmain_page%3Dproduct_info%26products_id%3D30&sa=U&ei=rOdMU-yFF-aNywOw_oKAAQ&ved=0CD8QFjAEOAo&usg=AFQjCNEfttgDGixXOIhsXZpD23frlffUGw","news":false,"local_pack":{},"page":2,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.lightupandjuggle.com/store/index.php%3Fmain_page%3Dproduct_info%26products_id%3D30","title":"LED JUGGLE STICK/DEVIL STICK LED Devil Sticks [] - $36.00 ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"lightupandjuggle.com","place_details":{},"place_pack":{}},"107":{"href":"http://www.ahglow.com/","base_url":"www.ahglow.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"China factory supply: glow sticks, light sticks, Halloween glow sticks, safety light sticks, night fishing glow sticks, Halloween light sticks,lollipop glow stick, glow\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.ahglow.com/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CP8DEBYwVTgU&usg=AFQjCNE1Swpfklfr99NTAM0qLTq9MEV6Xg","news":false,"local_pack":{},"page":11,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.ahglow.com/","title":"light stick, glow sticks China factory, safety light sticks, Halloween ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"ahglow.com","place_details":{},"place_pack":{}},"22":{"href":"http://glowarehouse.com/sub_sections/night-fishing-glow-sticks","base_url":"glowarehouse.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"If you are an avid fisherman, or if you know one, you need to hear about the \nunbelievable results fishermen obtain at night using our glow sticks. It's almost \nnot\u00a0...","shopping":false,"dirty_url":"/url?q=http://glowarehouse.com/sub_sections/night-fishing-glow-sticks&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CCQQFjAAOBQ&usg=AFQjCNEwe8auNwvFQ8HDC4zXCk0M6GIORQ","news":false,"local_pack":{},"page":3,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://glowarehouse.com/sub_sections/night-fishing-glow-sticks","title":"Night Fishing Glow Sticks & Light Sticks - GloWarehouse.com","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"glowarehouse.com","place_details":{},"place_pack":{}},"113":{"href":"http://www.poipoi.info/","base_url":"www.poipoi.info","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Also, why not get illuminated with our fantastic selection of led, light up and glow \nPoi, light-sticks, fiber optic Poi and many more. For you pyromaniacs out there,\u00a0...","shopping":false,"dirty_url":"/url?q=http://www.poipoi.info/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CKEEEBYwWzgU&usg=AFQjCNGTNi8hEbYUgjFKW5tK5vmmx1zLfw","news":false,"local_pack":{},"page":12,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.poipoi.info/","title":"Welcome to Poi Poi","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"poipoi.info","place_details":{},"place_pack":{}},"13":{"href":"http://www.wayfair.com/Adesso-Stix-2-Light-Floor-Lantern-4046-01-AE1714.html","base_url":"www.wayfair.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Free Shipping when you buy Adesso Stix 2 Light Floor Lantern at Wayfair - Great \nDeals on all Decor products with the best selection to choose from!","shopping":false,"dirty_url":"/url?q=http://www.wayfair.com/Adesso-Stix-2-Light-Floor-Lantern-4046-01-AE1714.html&sa=U&ei=rOdMU-yFF-aNywOw_oKAAQ&ved=0CC0QFjABOAo&usg=AFQjCNGxeWvg6CpcvlWN9GwvqiabYI31dQ","news":false,"local_pack":{},"page":2,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.wayfair.com/Adesso-Stix-2-Light-Floor-Lantern-4046-01-AE1714.html","title":"Adesso Stix 2 Light Floor Lantern | Wayfair","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"wayfair.com","place_details":{},"place_pack":{}},"104":{"href":"http://snack-chips.findthebest.com/compare/43-106/LAY-S-vs-Pringles","base_url":"snack-chips.findthebest.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Compare Chips Nutrition: LAY'S Light Original Potato Chips vs Pringles Stix - \nHoney Butter Potato Chips. In this side by side comparison, find features which \nare\u00a0...","shopping":false,"dirty_url":"/url?q=http://snack-chips.findthebest.com/compare/43-106/LAY-S-vs-Pringles&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CO4DEBYwUjgU&usg=AFQjCNGaEp7mHFDooiYTHxWhXwhNgrl7pA","news":false,"local_pack":{},"page":11,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://snack-chips.findthebest.com/compare/43-106/LAY-S-vs-Pringles","title":"LAY'S Light Original Potato Chips vs Pringles Stix - Honey Butter ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"findthebest.com","place_details":{},"place_pack":{}},"98":{"href":"http://www.publistix.com/lightupstix.html","base_url":"www.publistix.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Similar to our GlowStix, these Light Up Stix have an LED bulb inside that \nilluminates the entire stick- watch 'em glow as you wave and bang them around!","shopping":false,"dirty_url":"/url?q=http://www.publistix.com/lightupstix.html&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CM0DEBYwTDgU&usg=AFQjCNG518qkGIMjEqk_xZH5cVttmwayJQ","news":false,"local_pack":{},"page":10,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://www.publistix.com/lightupstix.html","title":"Light Up Stix - PubliStix.com","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"publistix.com","place_details":{},"place_pack":{}},"110":{"href":"http://blushingdivasbookreviews.com/2013/09/15/author-spot-light-alluring-turmoil-bayou-stix-1-by-skye-turner-2/","base_url":"blushingdivasbookreviews.com","brand_pack_results":{},"video_pack_results":{},"image":false,"description":"Sep 15, 2013 ... Author Spot Light: Alluring Turmoil (Bayou Stix #1) by Skye Turner. Sep15. Alexia \nSloane is the twenty-six year old owner of the highly\u00a0...","shopping":false,"dirty_url":"/url?q=http://blushingdivasbookreviews.com/2013/09/15/author-spot-light-alluring-turmoil-bayou-stix-1-by-skye-turner-2/&sa=U&ei=wOdMU_OlC6iBywOEiYKAAQ&ved=0CJAEEBYwWDgU&usg=AFQjCNGKYK0-IvZByfzaL80T7Yz3g47m_w","news":false,"local_pack":{},"page":12,"micro_format":false,"real_time":false,"image_pack_results":[],"maps":false,"url":"http://blushingdivasbookreviews.com/2013/09/15/author-spot-light-alluring-turmoil-bayou-stix-1-by-skye-turner-2/","title":"Author Spot Light: Alluring Turmoil (Bayou Stix #1) by Skye Turner ...","blog_posts":false,"authorship":false,"indented":0,"video":false,"authorship_details":{},"base_domain":"blushingdivasbookreviews.com","place_details":{},"place_pack":{}}},"mobile":"false","autocorrect":"","engine":"google","locale":"en-us","maps":false,"micro_format":true,"rank_date":"2014-04-15","integrated_places":false,"ads":{"1":{"description":"Low Price Premium Glow Sticks:Variety of Colors + Imprints!","destination_url":"http://www.windycitynovelties.com/259c/glow-sticks.html","headline":"Wholesale Glow Sticks 3\u00a2","ad_location":"side_ad","display_url":"www.windycitynovelties.com/Glow-Sticks","dirty_destination_url":"/aclk?sa=l&ai=C0vZzm-dMU-a4NYLUywOp_IH4B9KTnSTWh8_cb9HwhadYEAEoCFDq-a_X______8BYMkGoAHB67b_A8gBAaoEJk_QxaxBN87nX2DGK_e9g5vsEipnUG2pQD2BmX6wbqP6wKeLxdKbgAenlEmQBwE&num=1&sig=AOD64_2qTFBexJ6y-4GCtCixNlMXvWcM9w&ved=0CIIBENEM&adurl=http://www.windycitynovelties.com/259c/glow-sticks.html"},"2":{"description":"Parse Error","destination_url":"Unable To Process","headline":"","ad_location":"side_ad","display_url":"Parse Error","dirty_destination_url":"/url?q=/settings/ads/preferences%3Fhl%3Den&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CIUBEIET&usg=AFQjCNEQOffXwyN_8dp1ld14d3C7nSg2GQ"},"3":{"description":"Free Shipping on Orders Over $30.Top Quality & Low Prices - Buy Now!","destination_url":"http://tracking.searchmarketing.com/click.asp%3FAID%3D124521560%26CAPCID%3D6325702088%26cadevice%3Dc","headline":"Glow Sticks In Bulk","ad_location":"side_ad","display_url":"www.glowuniverse.com/Glow-Sticks","dirty_destination_url":"/aclk?sa=l&ai=Cl7yJm-dMU-a4NYLUywOp_IH4B_iamIECkNz59ReK3qfxBBACKAhQma-3C2DJBqAB_NPC_gPIAQGqBCZP0NXcSzfN71_Yxao9vvU7srxlTLpX2FZhSJBMR7nHedCni_jz64AH7Ku9AZAHAQ&num=2&sig=AOD64_1cPHhYxbwBk4FygF4pJ273EQNAxw&ved=0CIcBENEM&adurl=http://tracking.searchmarketing.com/click.asp%3FAID%3D124521560%26CAPCID%3D6325702088%26cadevice%3Dc"},"4":{"description":"Parse Error","destination_url":"Unable To Process","headline":"","ad_location":"side_ad","display_url":"Parse Error","dirty_destination_url":"/url?q=/settings/ads/preferences%3Fhl%3Den&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CIoBEIET&usg=AFQjCNEQOffXwyN_8dp1ld14d3C7nSg2GQ"},"5":{"description":"Free Shipping on $35 and above!Buy Now - Stock Clearance Sale","destination_url":"http://www.amazon.com/Light-Sticks-Waterproof-Flashlights-Guarantee/dp/B00GZCYEWM/ie%3DUTF8/%3Fm%3DA2YFX9DK4NMQUG%26keywords%3Dglow%2Bsticks","headline":"Light Sticks","ad_location":"side_ad","display_url":"www.amazon.com/BestSeasonalDiscount","dirty_destination_url":"/aclk?sa=l&ai=CrQbFm-dMU-a4NYLUywOp_IH4B7-O95oFl4GBrrAB77j4CxADKAhQ3OPIoAJgyQagAe3w8ugDyAEBqgQnT9CF5DA3zOdfYMYr972y07QXKmdQbalAPYGZfrBuo_rAp4vHum0KgAf7jo0XkAcB&num=3&sig=AOD64_0D-wrbnCgQ2gN2EMypfAP48ac0RA&ved=0CIwBENEM&adurl=http://www.amazon.com/Light-Sticks-Waterproof-Flashlights-Guarantee/dp/B00GZCYEWM/ie%3DUTF8/%3Fm%3DA2YFX9DK4NMQUG%26keywords%3Dglow%2Bsticks"},"6":{"description":"Parse Error","destination_url":"Unable To Process","headline":"","ad_location":"side_ad","display_url":"Parse Error","dirty_destination_url":"/url?q=/settings/ads/preferences%3Fhl%3Den&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CI8BEIET&usg=AFQjCNEQOffXwyN_8dp1ld14d3C7nSg2GQ"},"7":{"description":"Huge Selection of LED Bar Supplies.Wholesale Prices. Buy Bulk & Save.","destination_url":"http://www.lightgod.com/categories/Lightgod/Light-Up-Bar-Items.aspx%3Futm_source%3Dgoogle%26utm_medium%3Dcpc%26utm_campaign%3Dbar%2Bsupplies%26utm_term%3Dled%2520light%2520sticks%26o%3Dsellingprice","headline":"LED Light Sticks","ad_location":"side_ad","display_url":"www.lightgod.com/BarSupplies","dirty_destination_url":"/aclk?sa=l&ai=C75YRm-dMU-a4NYLUywOp_IH4B9KjsqkD-uj7onvCsvPKYxAEKAhQiNDAzfj_____AWDJBqABpuP1_gPIAQGqBCRP0MWyKTfL519gxiv3vaezqRIqZ1BtqUA9gZl-sG42msSpDAaAB8KcigGQBwE&num=4&sig=AOD64_1dIZF5doKIQ3Kpo9l34CqN4qaBEw&ved=0CJMBENEM&adurl=http://www.lightgod.com/categories/Lightgod/Light-Up-Bar-Items.aspx%3Futm_source%3Dgoogle%26utm_medium%3Dcpc%26utm_campaign%3Dbar%2Bsupplies%26utm_term%3Dled%2520light%2520sticks%26o%3Dsellingprice"},"8":{"description":"Parse Error","destination_url":"Unable To Process","headline":"","ad_location":"side_ad","display_url":"Parse Error","dirty_destination_url":"/url?q=/settings/ads/preferences%3Fhl%3Den&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CJYBEIET&usg=AFQjCNEQOffXwyN_8dp1ld14d3C7nSg2GQ"},"9":{"description":"Reliable, Safe Premium Light SticksHuge Range at Discounted Prices...","destination_url":"http://www.glow-sticks.org/Glow-Sticks/%3Fsource%3DGoogle-Adwords%26kw%3DLight-Sticks-Z","headline":"Buy Light Sticks Online","ad_location":"side_ad","display_url":"www.glow-sticks.org/","dirty_destination_url":"/aclk?sa=l&ai=C4R3um-dMU-a4NYLUywOp_IH4B_S8v8AD_K6spVfMpfi-ARAFKAhQq8fywPn_____AWDJBqAB48T8_gPIAQGpAoRlkq8JC7w-qgQhT9C1uyE3yudfKMaejS4X4AELyiZMSg1494eNu7BrrrKHgAeFu4MBkAcB&num=5&sig=AOD64_1peIal1CNaaWBRWqbJKScJevFZlQ&ved=0CJgBENEM&adurl=http://www.glow-sticks.org/Glow-Sticks/%3Fsource%3DGoogle-Adwords%26kw%3DLight-Sticks-Z"},"10":{"description":"Parse Error","destination_url":"Unable To Process","headline":"","ad_location":"side_ad","display_url":"Parse Error","dirty_destination_url":"/url?q=/settings/ads/preferences%3Fhl%3Den&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CJsBEIET&usg=AFQjCNEQOffXwyN_8dp1ld14d3C7nSg2GQ"},"11":{"description":"Get Light Sticks Bulk Info.Access 4 Search Engines at Once.","destination_url":"http://www.info.com/light%2520sticks%2520bulk%3Fcb%3D1%26cmp%3D3489","headline":"Light Sticks Bulk","ad_location":"side_ad","display_url":"www.info.com/Light+Sticks+Bulk","dirty_destination_url":"/aclk?sa=L&ai=COShvm-dMU-a4NYLUywOp_IH4B_zOy7gEzKbjvp8BzKX4vgEQBigIUOKQoLD6_____wFgyQbIAQGqBCJP0MWyKTfJ518oxv7KIArgAQvKJkxKDXj3h427sGuzgSxvgAecrocokAcB&num=6&sig=AOD64_06gDqDt7S4DyXAo4HuzZxMqnYWsg&ved=0CJ0BENEM&adurl=http://www.info.com/light%2520sticks%2520bulk%3Fcb%3D1%26cmp%3D3489"},"12":{"description":"Parse Error","destination_url":"Unable To Process","headline":"","ad_location":"side_ad","display_url":"Parse Error","dirty_destination_url":"/url?q=/settings/ads/preferences%3Fhl%3Den&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CKABEIET&usg=AFQjCNEQOffXwyN_8dp1ld14d3C7nSg2GQ"},"13":{"description":"Free Products - Limited Offer!Low Price Guarantee, Free Shipment","destination_url":"http://coolglow.com/Glow-Sticks/","headline":"White Glow Sticks 3\u00a2","ad_location":"side_ad","display_url":"www.coolglow.com/GlowSticks","dirty_destination_url":"/aclk?sa=l&ai=CVJ-Dm-dMU-a4NYLUywOp_IH4B4iP5fkF0PfCzI0BnMeDvggQBygIUMfOlez9_____wFgyQagAdjQjd4DyAEBqgQnT9DV-R03yO9f2MWqPb71O9qvdiO6V9hWYUiQTEe5x3nQp4uS8kE9gAeQr_IhkAcB&num=7&sig=AOD64_0_xIzUVyHuwggJFC3NMZoOzKpK_Q&ved=0CKIBENEM&adurl=http://coolglow.com/Glow-Sticks/"},"14":{"description":"Parse Error","destination_url":"Unable To Process","headline":"","ad_location":"side_ad","display_url":"Parse Error","dirty_destination_url":"/url?q=/settings/ads/preferences%3Fhl%3Den&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CKUBEIET&usg=AFQjCNEQOffXwyN_8dp1ld14d3C7nSg2GQ"},"15":{"description":"Find Stick On Light Today.Shop Stick On Light at Target.com.","destination_url":"http://126.xg4ken.com/media/redir.php%3Fprof%3D4%26camp%3D12665%26affcode%3Dkw2702958%26cid%3D20112965088%26networkType%3Dsearch%26kdv%3Dc%26url%5B%5D%3Dhttp://www.target.com/c/-/Nao-30/Ntk-All/Ntt-stick%252Bon%252Bled%252Blights/Ntx-matchallpartial%253Fref%253Dtgt_adv_XS000000%2526AFID%253Dgoogle%2526CPNG%253Ddsa_whole%252Bsite%2526adgroup%253Ddsa%252Bwhole%252Bsite%2526LNM%253D_cat:target.com%2526MT%253Dexact%2526LID%253D4p2702958%2526KID%253D_kenshoo_clickid_","headline":"Stick on Led Lights at Target","ad_location":"side_ad","display_url":"www.target.com/","dirty_destination_url":"/aclk?sa=l&ai=C2FoBm-dMU-a4NYLUywOp_IH4B5Cnp9EC0IfflUv4tbCtShAIKAhQ-MK4__v_____AWDJBqAB9uvl_QPIAQGqBCVP0OWPNTfH519gxiv3vcX6pgMqZ1BtqUA9gZl-sG43msSzXdFOoAYsgAeo6IUNkAcB&num=8&sig=AOD64_2BferSku0ejiqMq9hA8nUgKqOU_Q&ved=0CKcBENEM&adurl=http://126.xg4ken.com/media/redir.php%3Fprof%3D4%26camp%3D12665%26affcode%3Dkw2702958%26cid%3D20112965088%26networkType%3Dsearch%26kdv%3Dc%26url%5B%5D%3Dhttp://www.target.com/c/-/Nao-30/Ntk-All/Ntt-stick%252Bon%252Bled%252Blights/Ntx-matchallpartial%253Fref%253Dtgt_adv_XS000000%2526AFID%253Dgoogle%2526CPNG%253Ddsa_whole%252Bsite%2526adgroup%253Ddsa%252Bwhole%252Bsite%2526LNM%253D_cat:target.com%2526MT%253Dexact%2526LID%253D4p2702958%2526KID%253D_kenshoo_clickid_"},"16":{"description":"Parse Error","destination_url":"Unable To Process","headline":"","ad_location":"side_ad","display_url":"Parse Error","dirty_destination_url":"/url?q=/settings/ads/preferences%3Fhl%3Den&sa=U&ei=m-dMU9n8M-z8yAPQ3IAg&ved=0CKoBEIET&usg=AFQjCNEQOffXwyN_8dp1ld14d3C7nSg2GQ"}},"suggested_search":true,"blog_posts":false,"total_number_of_results":3830000,"ppl_id_author":"","ppl_id":"","invalid_term":false,"video":true,"_id":"534ce7c1fbb0627d810002d5","image_packs":[4],"requested_at":"2014-04-15T05:00:12+00:00","pages_from":"false","ad_count":16}<?php
        $jsonCallback = ob_get_clean();

        $jsonDecoded = json_decode($jsonCallback);
        //pr($jsonDecoded);
        //exit;

        $url = 'http://www.amazon.com/Omega-Fish-Oil-Supplements-Satisfaction/dp/B00EY305CW';
        $jsonCallback = 'http://api.authoritylabs.com/keywords/get.json?keyword=light%20stix&rank_date=2014-04-11&locale=en-us&engine=google&pages_from=false&lang_only=false&geo=&autocorrect=&mobile=false&ppl_id=&papi_id=534ce7c1fbb0627d810002d5';
        $auth_token = "zpN0Q1ZzCDxty3p3nnVk";
        $myResult = $this->Authoritylab->parseRanks($url, $jsonCallback, $auth_token);

        pr($myResult);
        exit;
    }

    function checkAutoSites()
    {
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->loadModel('Nitro');

        $allAutoDomains = $this->Domain->find('all', array('conditions' => "networktype IN (5)"));

        $allDbInfo = $this->Nitro->nitroDbInfo();

        $allAutoDomainsDb = array();

        foreach ($allAutoDomains as $aAD) {
            foreach ($allDbInfo as $key => $value) {
                if ($aAD['Domain']['domain'] == $key) {
                    $allAutoDomainsDb[$key] = $value;
                }
            }
        }

        echo '<pre>';
        var_export($allAutoDomainsDb);
        exit;
    }

    function check_indexing($campId = null)
    {
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->loadModel('Linkindexing');
        $this->loadModel('Indexingurl');

        $campClause = '';
        if ($campId) {
            $campClause = " AND id = $campId";
        }

        $allCampaigns = $this->Linkindexing->find('all', array('conditions' => "status = 1 $campClause"));

        //pr($allCampaigns);

        $checkedIds = array();

        if ($allCampaigns) {
            $this->loadModel('Commonmodel');
            $apiKey = 'f64c1a7148092fde388f0decbcacef567faf8505';

            foreach ($allCampaigns as $key => $value) {
                $iCamp = $value['Linkindexing'];
                $jsonUrl = "https://app.indexemperor.com/api/status.json?apikey=$apiKey&batchid=" . $iCamp['batch_id'];
                $requestResult = $this->Commonmodel->_request($jsonUrl, 'GET');

                pr($requestResult);

                if (isset($requestResult['result']) && $requestResult['result']) {
                    $reString = $requestResult['result'];
                    $rep = array(':id', ':status', ':batch_urls', ':url', ':ping_count', ':crawl_count', '=>');
                    $with = array('"id"', '"status"', '"batch_urls"', '"url"', '"ping_count"', '"crawl_count"', ':');
                    $reString = str_replace($rep, $with, $reString);
                    $jsonResult = json_decode($reString);
                    //pr($jsonResult);
                    //exit;
                    if (isset($jsonResult->batch_urls) && $jsonResult->batch_urls) {
                        foreach ($jsonResult->batch_urls as $batchU) {
                            $updateData['ping_count'] = $batchU->ping_count;
                            $updateData['crawl_count'] = $batchU->crawl_count;
                            $updateIndex['url'] = $batchU->url;
                            $this->Indexingurl->updateAll($updateData, $updateIndex, false);
                        }

                        $datetime1 = new DateTime();
                        $datetime2 = new DateTime($iCamp['created']);
                        $interval = $datetime2->diff($datetime1);
                        $dayGap = (int)$interval->format('%a');

                        if ($dayGap >= 24) {
                            $this->Linkindexing->id = $iCamp['id'];
                            $this->Linkindexing->saveField('status', 2);
                        }
                        $checkedIds[] = $iCamp['id'];
                    }
                }
            }
        }
        pr($checkedIds);
        $this->cmail(TEST_EMAIL, 'CHECK INDEXING', print_r($checkedIds, true));
        exit;
    }

    function firstdata()
    {
        // attempt the charge (array $data, boolean $testing)

        $data[''] = '';
        $data[''] = '';
        $data[''] = '';
        $data[''] = '';

        $response = $this->Ggapi->ggProcess($data, true);

        pr($response);

        exit;

        // update the order table with the response
        if ($response) {

            if ($response['r_approved'] == 'APPROVED') {
                // merge the response data with the order data
                $this->data['Order'] = array_merge($this->data['Order'], $response);
            } else {
                // card was DECLINED
                $error = explode(':', $response['r_error']);
                $this->Session->setFlash(
                    'Your credit card was declined. The message was: ' . $error[1], 'modal', array('class' => 'modal error')
                );
                $this->redirect(array('controller' => 'orders', 'action' => 'checkout'));
            }
        } else {
            // no response from the gateway
            $this->Session->setFlash(
                'There was a problem connecting to our payment gateway, please try again.', 'modal', array('class' => 'modal error')
            );
            $this->redirect(array('controller' => 'orders', 'action' => 'checkout'));
        }
    }

    function test_ixr()
    {

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        App::import('Vendor', 'IXR_Library');
        $fullRpcDomain = 'http://www.25kw.info/nitro/nitroserver.php';
        //$fullRpcDomain = 'http://www.wemonster.com/nitroserver.php';
        $client = new IXR_ClientSSL($fullRpcDomain);

        $response = array();
        if (!$client->query('nitro.getTime')) {
            $response[] = 'Error: ' . $client->getErrorCode() . " : " . $client->getErrorMessage();
        } else {
            $response = $client->getResponse();
        }
        pr($response);
        exit;
    }

    function test_curl()
    {
        $this->loadModel('Commonmodel');

        $fullRpcDomain = 'http://www.wemonster.com/';
        $re = $this->Commonmodel->_request($fullRpcDomain, 'POST', array('test' => 'testme'));
        pr($re);
        exit;
    }

    function update_blogroll_loop()
    {

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->loadModel('Campaign');
        $this->loadModel('Blogroll');

        $cond = array(
            'fields' => 'id,id',
            'status' => 1
        );

        $userIdList = $this->User->find('list', $cond);

        if ($userIdList) {
            foreach ($userIdList as $userId) {
                $re = $this->update_blogroll($userId);
            }
        }
    }

    function update_blogroll($userId = 0)
    {

        if (!isset($_GET['direct'])) {
            ob_end_clean();
            ignore_user_abort();
            ob_start();
            header("Connection: close");
            header("Content-Length: " . ob_get_length());
            ob_end_flush();
            flush();
        }
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->loadModel('Campaign');
        $this->loadModel('Blogroll');

        $allUCampId = $this->Campaign->find('list', array('fields' => 'id,id', 'conditions' => "user_id = $userId"));

        $campIn = 'campaign_id IN(0)';
        if ($allUCampId) {
            $str = implode(',', $allUCampId);
            $campIn = "campaign_id IN($str)";
        }

        $allCampId = $this->Blogroll->find('list', array('fields' => 'campaign_id,campaign_id', 'conditions' => "$campIn"));

        if ($allCampId) {
            foreach ($allCampId as $campId) {
                pr(array($campId));
                $reTest = $this->nitro_generate_campaign_results($campId);
                pr($reTest);
            }
        }
        pr($allCampId);
        //exit;
    }

    // custom mail sender
    function cmail($to = '', $subject = '', $body = '', $attchfile = '', $from = '', $fromName = 'RankRatio', $templates = 'default')
    {
        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail();
        $email->helpers('Html');
        $email->viewVars(array('body' => $body));
        $email->from(array('noreply@rankratio.com' => 'RankRatio'));
        $email->to($to);
        $email->subject('[RankRatio] ' . $subject);
        $email->template('email', 'default');
        $email->emailFormat('html');
        $email->transport('Mail');
        if ($attchfile != '')
            $email->attachments($attchfile);
        $email->send();
    }

    function manualposting($postId = null, $userId = null)
    {


        $this->loadModel('Manualpost');
        $this->Manualpost->virtualFields = array(
            'domain' => "SELECT domains.domain FROM domains WHERE Manualpost.domain_id = domains.id"
        );
        $nowDate = date('Y-m-d');
        //$cond['Manualpost.post_date <='] = $nowDate;
        $cond['Manualpost.status'] = 1;
        $cond['OR'][]['Manualpost.post_id'] = null;
        $cond['OR'][]['Manualpost.post_id'] = 0;
        if ($userId) {
            $cond['Manualpost.user_id'] = $userId;
        }
        if ($postId) {
            $cond['Manualpost.id'] = $postId;
        }
        $postList = $this->Manualpost->find('all', array('conditions' => $cond));

        //pr($postList);
        //exit;

        $allIds = array();
        if ($postList) {
            ini_set('memory_limit', '512M');
            ini_set('max_execution_time', 7200);
            ini_set('max_input_time', 7200);


            $this->loadModel('Any');
            $this->Any->useTable = 'article_categories';
            $allCats = $this->Any->find('list', array('fields' => 'id,name'));

            App::import('Vendor', 'IXR_Library');
            $fullRpcDomain = 'http://www.25kw.info/nitro/nitroserver.php';
            $client = new IXR_ClientSSL($fullRpcDomain);

            $this->loadModel('Nitro');
            $this->loadModel('Nitrocamp');

            $recordedCidsAll = array();
            foreach ($postList as $post) {
                $arr = $post['Manualpost'];
                $domain = $arr['domain'];
                //$domain = 'buckminsterfullerlive.com';
                $categoryId = $arr['category_id'];
                //$dbInfo = $this->Nitro->nitroDbInfo(array($domain));
                $dbInfo = $this->Nitro->nitroDbInfoNew(array($domain));
                echo $domain;
                echo '<br/>';
                pr($dbInfo);
                if (isset($dbInfo[$domain]['TABLE_SCHEMA'])) {
                    $params = array();
                    $params['domain'] = $arr['domain'];
                    $params['siteUrl'] = $siteUrl = $dbInfo[$domain]['SITE_URL'];
                    $params['dbName'] = $dbInfo[$domain]['TABLE_SCHEMA'];
                    $params['tableName'] = $dbInfo[$domain]['TABLE_PREFIX'] . 'posts';
                    $params['tablePrefix'] = $dbInfo[$domain]['TABLE_PREFIX'];
                    $params['category'] = $allCats[$categoryId];

                    //exit(pr($params));

                    $post_title = $this->spinContent($arr['post_title']);
                    $post_content = str_replace(array('http:/', 'http:///', 'http://'), 'http://', $this->spinContent($arr['post_content']));

                    //$params['post_id'] = 170;
                    $params['post_title'] = $post_title;
                    $params['post_content'] = utf8_encode(html_entity_decode($post_content));
                    $params['post_date'] = $postDate = $arr['post_date'];

                    /**/
                    if (!$client->query('nitro.autoPostCat', $params)) {
                        $response['error'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
                    } else {
                        $response = $client->getResponse();
                        if (isset($response['post_id']) && intval($response['post_id'])) {
                            $post_name = $response['post_name'];
                            $update_data['id'] = $arr['id'];
                            $update_data['post_id'] = $post_id = $response['post_id'];
                            $update_data['post_url'] = $siteUrl . '?p=' . $post_id;
                            $update_data['post_link'] = $siteUrl . '/' . $post_id . '-' . $post_name;

                            $allIds[] = $arr['id'];
                            $this->Manualpost->id = $arr['id'];
                            if ($this->Manualpost->save($update_data, false)) {
                                $update_data['domain_id'] = $arr['domain_id'];
                                //$recordedCids1 = $this->Nitrocamp->updateCodeFound($update_data, $post_content);
                                $updateDStas['domain_id'] = $arr['domain_id'];
                                $updateDStas['post_id'] = $post_id;
                                //$recordedCids1 = $this->Nitrocamp->updateCodeFound($insert_data, $post_content);
                                $recordedCids1 = $this->updateNewDStats($updateDStas, $post_content);
                                if ($recordedCids1) {
                                    foreach ($recordedCids1 as $rCid) {
                                        $recordedCidsAll[] = $rCid;
                                    }
                                }
                            }
                            $response['id'] = $arr['id'];
                            $response['domain'] = $domain;
                        }
                    }
                    pr($response);
                    //exit;
                    /* */
                }
            }
            //$this->setCStats();
        }
        $this->cmail(TEST_EMAIL, 'Manual Posting!', print_r($allIds, true));
        //$this->render('sql');
    }

    function updateTR()
    {
        $this->loadModel('Manualpost');
        $this->loadModel('Trackeddomain');
        $this->loadModel('Nitrocamp');

        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 7200);
        ini_set('max_input_time', 7200);


        $allPost = $this->Manualpost->find('all', array('conditions' => 'post_url != ""'));

        //pr($allPost);
        //exit;

        if ($allPost) {
            $recordedCidsAll = array();
            foreach ($allPost as $value) {
                $arr = $value['Manualpost'];
                $insertData['domain_id'] = $arr['domain_id'];
                $insertData['post_url'] = $arr['post_url'];
                $insertData['post_id'] = $arr['post_id'];
                $post_content = $arr['post_content'];
                $recordedCids1 = $this->Nitrocamp->updateCodeFound2($insertData, $post_content);
                if ($recordedCids1) {
                    foreach ($recordedCids1 as $rCid) {
                        $recordedCidsAll[] = $rCid;
                    }
                }
            }
            if ($recordedCidsAll) {
                $recordedCidsAll = array_unique($recordedCidsAll);
                pr($recordedCidsAll);
                foreach ($recordedCidsAll as $rCidA) {
                    //$this->nitro_generate_campaign_results($rCidA);
                }
            }
        }
    }

    function post_previous($domain = null)
    {

        $this->loadModel('Submissionreport');

        $this->Submissionreport->virtualFields = array(
            'post_content' => 'SELECT submissions.postcontent FROM submissions WHERE Submissionreport.submission_id = submissions.id'
        );

        $cond = array(
            'post_link LIKE' => "%$domain%",
            'modified NOT LIKE' => "%2014-05-31 00:00:00%",
        );
        $itemList = $this->Submissionreport->find('all', array('conditions' => $cond));

        //pr($itemList);
        //exit;

        $this->loadModel('Nitro');

        App::import('Vendor', 'IXR_Library');
        $fullRpcDomain = 'http://www.25kw.info/nitro/nitroserver.php';
        $client = new IXR_ClientSSL($fullRpcDomain);

        if ($itemList) {
            foreach ($itemList as $key => $item) {
                $arr = $item['Submissionreport'];
                $itemList[$key]['Submissionreport']['post_title'] = $post_title = $this->deTitle($arr['post_link']);
                $itemList[$key]['Submissionreport']['post_content'] = $post_content = $this->spinContent($arr['post_content']);


                //$dbInfo = $this->Nitro->nitroDbInfo(array($domain));
                $dbInfo = $this->Nitro->nitroDbInfoNew(array($domain));
                //pr($dbInfo);
                if (isset($dbInfo[$domain]['TABLE_SCHEMA'])) {
                    $params = array();
                    $params['domain'] = $domain;
                    $params['siteUrl'] = $siteUrl = $dbInfo[$domain]['SITE_URL'];
                    $params['dbName'] = $dbInfo[$domain]['TABLE_SCHEMA'];
                    $params['tableName'] = $dbInfo[$domain]['TABLE_PREFIX'] . 'posts';

                    $post_title = $this->spinContent($post_title);
                    $post_content = str_replace(array('http:/', 'http:///', 'http://'), 'http://', $this->spinContent($post_content));

                    $params['post_id'] = $arr['post_id'];
                    $params['post_title'] = $post_title;
                    $params['post_content'] = utf8_encode(html_entity_decode($post_content));
                    $params['post_date'] = $postDate = $arr['created'];

                    /**/
                    if (!$client->query('nitro.autoPost', $params)) {
                        $response['error'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
                    } else {
                        $response = $client->getResponse();
                        if (isset($response['post_id']) && intval($response['post_id'])) {
                            $post_name = $response['post_name'];
                            $update_data['id'] = $arr['id'];
                            $update_data['post_id'] = $post_id = $response['post_id'];
                            $update_data['post_url'] = $siteUrl . '?p=' . $post_id;
                            $update_data['post_link'] = $siteUrl . '/' . $post_id . '-' . $post_name;

                            $this->Submissionreport->id = $arr['id'];
                            if ($this->Submissionreport->save(array('modified' => '2014-05-31 00:00:00'), false)) {

                            }
                            $response['id'] = $arr['id'];
                            $response['domain'] = $domain;
                        }
                    }
                    pr($response);
                    /* */
                }
            }
        }

        //pr($itemList);
        echo $this->render('sql');
        exit;
    }

    function deTitle($str)
    {
        $post_title_arr = explode('/', $str);
        $post_title = $post_title_arr[count($post_title_arr) - 1];
        $nArr = explode('-', $post_title);
        unset($nArr[0]);

        return $post_title = ucwords(implode(' ', $nArr));
    }

    function clear_comments($domain)
    {
        $this->loadModel('Nitro');
        App::import('Vendor', 'IXR_Library');
        $fullRpcDomain = 'http://www.25kw.info/nitro/nitroserver.php';
        $client = new IXR_ClientSSL($fullRpcDomain);

        $params['domain'] = $domain;
        if (!$client->query('nitro.clearComments', $params)) {
            $response['error'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
        } else {
            $response = $client->getResponse();
        }
        pr($response);
    }

    function delete_post($domain, $postId)
    {
        $this->loadModel('Nitro');
        App::import('Vendor', 'IXR_Library');
        $fullRpcDomain = 'http://www.25kw.info/nitro/nitroserver.php';
        $client = new IXR_ClientSSL($fullRpcDomain);

        $params['domain'] = $domain;
        $params['post_id'] = $postId;
        if (!$client->query('nitro.deletePost', $params)) {
            $response['error'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
        } else {
            $response = $client->getResponse();
        }
        pr($response);
    }

    function setiu()
    {
        $this->loadModel('Linkindexing');
        $this->loadModel('Indexingurl');

        $allItems = $this->Linkindexing->find('all');
        foreach ($allItems as $item) {
            $arr = $item['Linkindexing'];
            $urls = unserialize($arr['urls']);
            pr($urls);
            if ($urls) {
                foreach ($urls as $url) {
                    $this->Indexingurl->create();
                    $insertData['campaign_id'] = $arr['id'];
                    $insertData['url'] = $url['url'];
                    $insertData['ping_count'] = $url['ping'];
                    $insertData['crawl_count'] = $url['crawl'];
                    try {
                        $this->Indexingurl->save($insertData, false);
                    } catch (Exception $exc) {
                        echo $exc->getTraceAsString();
                    }
                }
            }
        }
        exit;
    }

    function abc()
    {
        $this->loadModel('Iwarticle');
        $this->loadModel('Iwreport');

        $IDS = $this->Iwarticle->find('list', array('fields' => 'id,id'));
        $IDS = array_values($IDS);
        pr($IDS);

        $res = $this->Iwreport->find('list', array('fields' => 'article_id,article_id', 'order' => 'article_id asc'));
        $res = array_values($res);
        pr($res);


        $new = array_diff($IDS, $res);

        pr($new);

        exit;
    }

    function testPost()
    {

        $this->loadModel('Nitro');
        App::import('Vendor', 'IXR_Library');
        $fullRpcDomain = 'http://www.25kw.info/nitro/nitroserver.php';
        $client = new IXR_ClientSSL($fullRpcDomain);

        $domain = 'michaeldoverlaw.com';

        //$dbInfo = $this->Nitro->nitroDbInfo(array($domain));
        $dbInfo = $this->Nitro->nitroDbInfoNew(array($domain));
        pr($dbInfo);
        //exit;
        if (isset($dbInfo[$domain]['TABLE_SCHEMA'])) {
            $params = array();
            $params['domain'] = $domain;
            $params['siteUrl'] = $siteUrl = $dbInfo[$domain]['SITE_URL'];
            $params['dbName'] = $dbInfo[$domain]['TABLE_SCHEMA'];
            $params['tableName'] = $dbInfo[$domain]['TABLE_PREFIX'] . 'posts';
            $params['tablePrefix'] = $dbInfo[$domain]['TABLE_PREFIX'];
            $params['category'] = 'Broadband';

            $post_title = 'Test Post Ignore';
            $post_content = 'Test Content Ignore';

            $params['post_title'] = $post_title;
            $params['post_content'] = utf8_encode(html_entity_decode($post_content));
            $params['post_date'] = date('Y-m-d');

            /**/
            if (!$client->query('nitro.autoPostCat', $params)) {
                $response['error'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
            } else {
                $response = $client->getResponse();
            }

            pr($response);
            exit;
        }
    }

    function refreshindex($type, $skip = null)
    {

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        set_time_limit(7200);
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 7200);
        ini_set('max_input_time', 7200);

        $this->loadModel('Domain');
        $this->loadModel('GoogePageRank');
        $skipStr = $skip ? " AND last_check IS NULL" : '';
        $cond = "Domain.owner_id = -1 AND Domain.networktype = $type $skipStr";
        $fields = 'id,domain,is_indexed,last_check';
        $itemList = $this->Domain->find('all', array('conditions' => $cond, 'fields' => $fields));

        //pr($itemList);
        //exit;

        if ($itemList) {
            foreach ($itemList as $key => $item) {
                $arr = $item['Domain'];
                $admin_ip = $this->admin_ip();
                $proxyIp = $admin_ip['ip'];
                $proxy_user_pass = $admin_ip['password'];
                $proxy_port = $admin_ip['port'];
                $indexed = $this->GoogePageRank->checkIndex($arr['domain'], $proxyIp, $proxy_user_pass, $proxy_port);
                $isIndexed = $indexed == 'Yes' ? 1 : 0;
                $updateData['id'] = $arr['id'];
                $updateData['is_indexed'] = $isIndexed;
                $updateData['iw_active'] = $isIndexed;
                $updateData['last_check'] = date('Y-m-d H:i:s');
                $this->Domain->save($updateData, false);
                $updateData['count'] = $key;
                pr($updateData);
                sleep(3);
            }
        }
    }

    /**
     * This function gets all anchor text info for each campaign and insert into dclcampaigns table.
     *
     * @author mainul@aan-nahl.com
     * @version 1.0.1 [19 August 2014]
     */
    function updateAnchors()
    {
        $this->loadModel('Campaign');
        $this->Campaign->belongsTo = array();
        $this->Campaign->hasMany = array(
            'Anchortext' => array(
                'className' => 'Anchortext',
                'foreignKey' => 'campaign_id',
                'fields' => 'targetDensity,anchortexts,internalPageUrl,nofollow',
            )
        );

        $allCamps = $this->Campaign->find('all', array('fields' => 'id'));

        $finalArr = array();
        if ($allCamps) {
            $this->loadModel('DclCampaign');
            foreach ($allCamps as $aCamp) {
                $item = $aCamp['Anchortext'];
                if ($item) {
                    foreach ($item as $key => $value) {
                        unset($item[$key]['campaign_id']);
                        ksort($item[$key]);
                        $item[$key] = array_values($item[$key]);
                    }
                }
                //pr($item);
                $updateDclCampaign['id'] = $aCamp['Campaign']['id'];
                $updateDclCampaign['anchors'] = json_encode($item);
                $this->DclCampaign->save($updateDclCampaign, false);
            }
        }

        echo $this->render('sql');
        exit;
    }

    /**
     * This function gets all dcl stats from old trackeddomains table and insert into dcldomains table.
     *
     * @author mainul@aan-nahl.com
     * @version 1.0.1 [19 August 2014]
     */
    function getOldCount()
    {

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->loadModel('Trackeddomain');
        $this->loadModel('DclDomain');

        $allTResults = $this->Trackeddomain->find('all', array('fields' => "campaign_id,domain_id,post_id,codefound", 'conditions' => "campaign_id !='-1'"));

        $dStatsArr = array();
        if ($allTResults) {
            foreach ($allTResults as $value) {
                $res = $value['Trackeddomain'];
                $cId = $res['campaign_id'];
                $dId = $res['domain_id'];
                $pId = $res['post_id'];

                $dStatsArr[$dId][$pId][$cId] = intval($res['codefound']);
            }
        }

        foreach ($dStatsArr as $dId => $pValue) {
            ksort($pValue);
            $updateData['id'] = $dId;
            $updateData['d_stats'] = json_encode($pValue);
            $this->DclDomain->save($updateData, false);
        }
        $this->render('sql');
        exit;
    }

    /**
     * This function counts the blogroll link for each domain and campaign.
     *
     * @author mainul@aan-nahl.com
     * @version 1.0.1 [19 August 2014]
     */
    function setBFound()
    {
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->loadModel('Blogroll');
        $this->Blogroll->virtualFields['c_found'] = 'COUNT(campaign_id)';
        $allBlogRolls = $this->Blogroll->find('all', array('fields' => 'domain_id,campaign_id,c_found', 'group' => 'domain_id,campaign_id'));

        $this->loadModel('DclDomain');
        $prevBFound = $this->DclDomain->find('list', array('fields' => 'id,b_found', 'conditions' => 'b_found IS NOT NULL', 'order' => 'id ASC'));
        //pr($prevBFound);

        $cBlogRollArr = array();
        if ($allBlogRolls) {
            foreach ($allBlogRolls as $bRoll) {
                $dId = $bRoll['Blogroll']['domain_id'];
                $cId = $bRoll['Blogroll']['campaign_id'];
                $cBlogRollArr[$dId][$cId] = intval($bRoll['Blogroll']['c_found']);
            }
        }

        if ($cBlogRollArr) {
            $domIdArr = array();
            foreach ($cBlogRollArr as $domId => $cFound) {
                $domIdArr[] = $domId;
                $bFound = json_encode($cFound);
                if (!isset($prevBFound[$domId]) || (mysql_escape_string($prevBFound[$domId]) != mysql_escape_string($bFound))) {
                    $updateData['id'] = $domId;
                    $updateData['b_found'] = $bFound;
                    $this->DclDomain->save($updateData, false);
                }
            }
            $allPrevDomIdArr = array_keys($prevBFound);
            if ($domIdArr) {
                $deletedDomIdArr = array_diff($allPrevDomIdArr, $domIdArr);
                $domIdStr = implode(',', $deletedDomIdArr);
                if ($domIdStr) {
                    $this->DclDomain->updateAll(array('b_found' => null), array("id IN($domIdStr)"), false);
                }
            }
        }
    }

    /**
     * This function gets all stats from dcldomains and insert into dclcampaigns for each campaign.
     *
     * @author mainul@aan-nahl.com
     * @version 1.0.1 [19 August 2014]
     */
    function setCStats()
    {
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->setBFound();

        $this->loadModel('DclDomain');
        $allDFound = $this->DclDomain->find('all', array('fields' => 'id,d_stats,b_found'));

        $campArr = array();
        $cCountArr = array();
        if ($allDFound) {
            foreach ($allDFound as $item) {
                $dId = $item['DclDomain']['id'];
                $dStats = json_decode($item['DclDomain']['d_stats'], true);
                $bFound = json_decode($item['DclDomain']['b_found'], true);

                if ($dStats) {
                    foreach ($dStats as $pId => $pValue) {
                        foreach ($pValue as $cId => $cCount) {
                            $campArr[$cId][$dId][$pId] = $cCount;
                            @$cCountArr[$cId] += $cCount;
                        }
                    }
                }
                if ($bFound) {
                    foreach ($bFound as $cId => $cCount) {
                        $campArr[$cId][$dId]['-1'] = $cCount;
                        @$cCountArr[$cId] += $cCount;
                    }
                }
            }
        }

        $this->loadModel('DclCampaign');
        $prevCStats = $this->DclCampaign->find('list', array('fields' => 'id,c_stats'));

        if ($campArr) {
            foreach ($campArr as $cId => $cFound) {
                $cFoundJson = json_encode($cFound);
                if (!isset($prevCStats[$cId]) || (mysql_escape_string($prevCStats[$cId]) != mysql_escape_string($cFoundJson))) {
                    $updateDclCampaign['id'] = $cId;
                    $updateDclCampaign['c_stats'] = $cFoundJson;
                    $updateDclCampaign['c_count'] = $cCountArr[$cId];
                    $this->DclCampaign->save($updateDclCampaign, false);
                }
            }
        }

        $this->generateDcl();

        //echo $this->render('sql');
        exit;
    }

    function getRemoteDStats($domain = null)
    {

        /**/
        @ob_end_clean();
        ignore_user_abort();
        ob_start();
        header("Connection: close");
        header("Content-Length: " . ob_get_length());
        ob_end_flush();
        flush();
        /* */

        echo 'downhere';

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->loadModel('Domain');
        $this->loadModel('DclDomain');
        $this->loadModel('Ready');

        $getKey = 'nitro_dcl_string_meta';


        $cond = "networktype IN(3,4,5)";
        $allAutoDomains = $this->Domain->find('all', array('conditions' => $cond));

        $allAutoDomains_ = array(
            array('Domain' => array('id' => 99997, 'domain' => 'aan-mainul/wpfresh')),
            array('Domain' => array('id' => 99999, 'domain' => 'localhost/wptest')),
        );

        if ($allAutoDomains) {

            //App::import('Vendor', 'IXR_Library');

            foreach ($allAutoDomains as $item) {
                $domInfo = $item['Domain'];
                $domain = $domInfo['domain'];
                $url = 'http://www.' . $domain . '/index.php?' . $this->_cMetaKey;
                //$client = new IXR_ClientSSL($url);
                $nitro_dcl_string_meta = $this->_getDclStats($url);
                $updateData = array();
                if (stripos($nitro_dcl_string_meta, 'httperror#') !== false) {
                    $updateData['httpstatus'] = str_replace('httperror#', '', $nitro_dcl_string_meta);
                    //$this->DclDomain->save($updateData, false);
                } elseif (trim($nitro_dcl_string_meta) == '0' || stripos($nitro_dcl_string_meta, 'd_stats') == true) {
                    $updateData['httpstatus'] = 'OK';
                } else {
                    $updateData['httpstatus'] = 'OK';
                }
                //pr($updateData);
                if ($updateData) {
                    $this->Domain->id = $domInfo['id'];
                    $this->Domain->save($updateData, false);
                }

                /**
                 * $updateData['id'] = $domInfo['id'];
                 * $updateData['errormsg'] = null;
                 * if (!$client->query('nitro.getNitroCMeta', array($getKey => true))) {
                 * $updateData['errormsg'] = $client->getErrorCode() . " : " . $client->getErrorMessage();
                 * //$this->DclDomain->save($updateData, false);
                 * } else {
                 * $nitro_dcl_string_meta = trim($client->getResponse());
                 * //exit;
                 * if (stripos($nitro_dcl_string_meta, 'd_stats') !== false) {
                 * //$dclStats = json_decode($nitro_dcl_string_meta, true);
                 * //$updateData['d_stats'] = isset($dclStats['d_stats']) ? json_encode($dclStats['d_stats']) : array();
                 * //$this->DclDomain->save($updateData, false);
                 * } elseif (stripos($nitro_dcl_string_meta, 'httperror#') !== false) {
                 * $updateData['errormsg'] = $nitro_dcl_string_meta;
                 * //$this->DclDomain->save($updateData, false);
                 * }
                 * }
                 * /* */
                unset($updateData);
            }
        }
    }

    function _getDclStats($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($curl);
        if (FALSE === ($result)) {
            return 'httperror# ' . curl_error($curl);
        } else {
            return $result;
        }
    }

    function nitroPingMe()
    {
        $this->setRemoteDResults();
        exit;
        $url = 'http://www.safemoveoregon.com/186-top-personal-injury-law-facts-you-need-to-know';
        $pinged = get_headers($url);
        pr($pinged);
        exit;
    }

    function setRemoteDResults($dom = null, $exit = null)
    {
        if (!$exit) {
            /**/
            @ob_end_clean();
            ignore_user_abort();
            ob_start();
            header("Connection: close");
            header("Content-Length: " . ob_get_length());
            ob_end_flush();
            flush();
            /* */
        }
        //echo 'downhere';

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $this->loadModel('Domain');
        $this->loadModel('DclDomain');
        $this->loadModel('Ready');

        $param1 = array(
            'conditions' => "networktype NOT IN(0,1)",
            'fields' => 'id,domain'
        );
        $allAutoDomains = $this->Domain->find('list', $param1);

        if ($dom) {
            $dom = trim($dom);
            $domainMe = array_search($dom, $allAutoDomains) ? array_search($dom, $allAutoDomains) : '9999999';
        }
        //echo '<br/>';
        $domId = $dom ? $domainMe : '';

        $param2 = array(
            'conditions' => $domId ? "id IN($domId)" : '',
            'fields' => 'id,d_results',
            //'limit' => 100,
            'order' => 'id ASC'
        );
        $allDclRerults = $this->DclDomain->find('list', $param2);

        /**
         * pr($allAutoDomains);
         * pr($allDclRerults);
         * exit;
         * /* */
        if ($allDclRerults) {
            $multiCurlData = array();
            foreach ($allDclRerults as $dId => $dResult) {
                $domain = isset($allAutoDomains[$dId]) ? $allAutoDomains[$dId] : false;
                if ($domain) {
                    $url = 'http://www.' . $domain . '/index.php?';
                    $params[$this->_dclResultsValueKey] = trim($dResult);
                    //$temp['url'] = $url;
                    //$temp['post'] = $params;
                    $curlyId = $dId . ' : ' . $domain;
                    $posted = $this->_setDclResults($url, $params);

                    $multiCurlData[$curlyId] = $posted;

                    if ($exit) {
                        pr($multiCurlData);
                        pr(strip_tags($dResult));
                    }
                    //sleep(1);
                }
            }
            if (!$exit) {
                if ($multiCurlData) {
                    //$results = $this->multiRequest($multiCurlData);
                    $body = $this->prettyEmailBody($multiCurlData);
                    $body .= '<br/>';
                    $body .= 'Total: ' . count($multiCurlData);
                    $body .= '<br/>';
                    $this->cmail(TEST_EMAIL, 'Setting Results New', $body);
                }
            }
        }
    }

    function customSetDResults()
    {

        $customDoms = array(
            '95zqw.com',
            'authenticmichaelkorsbags.com',
            'delshannoncarshow.com',
        );

        $customDoms_ = array(
            '95zqw.com',
            'authenticmichaelkorsbags.com',
            'casanoblemortgages.com',
            'cityberryclub.com',
            'colorblockheadphones.com',
            'colorblockheadphones.com',
            'cosmeticscheapmac.com',
            'covistawireless.com',
            'delshannoncarshow.com',
            'france-r.com',
            'giantsteamfans.com',
            'giantsteamfans.com',
            'giny.us',
            'greengenllc.com',
            'jerseysmall2012.us',
            'jerseysmall2012.us',
            'louisvuittonoutletbll.com',
            'marcparctransportation.com',
            'marcparctransportation.com',
            'officialcowboysfanstore.com',
            'ramthestrait.com',
            'resultspropertiestenerife.com',
            'sabq.us'
        );

        foreach ($customDoms as $dom) {
            $this->setRemoteDResults($dom, 'exit');
        }
    }

    function prettyEmailBody($arr)
    {
        $body = '<pre>';
        $body .= var_export($arr, true);
        $body .= '</pre>';
        return $body;
    }

    function deleteDD()
    {
        $this->loadModel('Domain');
        $allDomains = $this->Domain->find('list', array('fields' => 'id,id', 'conditions' => ''));
        if ($allDomains) {
            $domStr = implode(',', $allDomains);
            $this->loadModel('DclDomain');
            $delDD = $this->DclDomain->find('list', array('fields' => 'id,id', 'conditions' => "id NOT IN($domStr)"));
            pr($delDD);

            if ($delDD) {
                sort($delDD);
                foreach ($delDD as $dId) {
                    $this->DclDomain->delete($dId);
                }
            }
        }

        exit;
    }

    function setRemoteDResultsLocal($domains = null)
    {
        //$this->cmail(TEST_EMAIL, 'New Plugin Test', 'Success!');
        //exit;

        if ($domains) {

            $domainArr = explode(',', $domains);
            $domainStr = "'" . implode("','", $domainArr) . "'";

            $this->loadModel('Domain');

            //$domainIds = $this->Domain->find('list', array('conditions' => "domain IN($domainStr)", 'fields' => 'domain,id'));

            $domainIds = array(137);
            pr($domainIds);

            if ($domainIds) {
                $domainIdStr = implode(',', $domainIds);

                $this->loadModel('DclDomain');
                $this->loadModel('Ready');

                $cond2 = "id IN($domainIdStr)";
                //$allAutoDomains = $this->Domain->find('list', array('conditions' => $cond, 'fields' => 'id,domain'));
                $allDclRerults = $this->DclDomain->find('list', array('conditions' => $cond2, 'fields' => 'id,d_results'));

                pr(stripslashes(strip_tags(json_encode($allDclRerults))));

                //$demoResult = '';
                //$allDclRerults = array('9999' => $demoResult);
                //$allAutoDomains = array('9999' => 'aan-mainul/wpfresh');

                if ($allDclRerults) {
                    foreach ($allDclRerults as $dId => $dResult) {
                        $domain = array_search($dId, $domainIds);
                        echo $domain = 'aan-mainul/wpfresh';
                        //exit;
                        //$domain = 'it-wh.com';
                        if ($domain) {
                            $url = 'http://' . $domain . '/index.php?';
                            if ($dResult) {
                                $dResultArr = json_decode($dResult, true);
                                if (isset($dResultArr['-1'])) {
                                    $dResultArr['-1'][60] = array();
                                }
                                //$dResultArr[39][399] = array();
                                $dResultArr[39][402] = array();
                                ksort($dResultArr);
                                $dResult = json_encode($dResultArr);
                            }

                            $params[$this->_dclResultsValueKey] = trim($dResult);
                            $posted = $this->_setDclResults($url, $params);
                            pr(array($dId . ' ' . $domain => $posted));
                        }
                    }
                }
            }
        }
    }

    function _setDclResults($url, $postData)
    {

        /**
         * @ob_end_clean();
         * ignore_user_abort();
         * ob_start();
         * header("Connection: close");
         * header("Content-Length: " . ob_get_length());
         * ob_end_flush();
         * flush();
         * /* */
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        $result = curl_exec($curl);
        //curl_exec($curl);
        /**/
        if (FALSE === ($result)) {
            return curl_error($curl);
        } else {
            return $result;
            return trim($result) == '1' ? '1' : '0';
        }
        /* */
    }

    /**
     * Generate results to set back to live sites for each campaign.
     *
     * @author mainul@aan-nahl.com
     * @version 1.0.1 [19 August 2014]
     */
    function generateDcl($campaignId = null)
    {
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        $campaignIdClause = $campaignId ? "AND id = $campaignId" : '';
        $this->loadModel('DclCampaign');
        $allCamps = $this->DclCampaign->find('all', array('conditions' => "c_count != 0 $campaignIdClause", 'fields' => 'id,anchors,c_stats,c_count,c_results', 'limit' => 5000));

        if ($allCamps) {
            $this->loadModel('Blogroll');
            $bRolls = $this->Blogroll->find('all', array('fields' => 'id,domain_id,campaign_id,setting_option'));
            $bRollInfo = array();
            if ($bRolls) {
                foreach ($bRolls as $bRoll) {
                    $bDid = $bRoll['Blogroll']['domain_id'];
                    $bCid = $bRoll['Blogroll']['campaign_id'];
                    $bRollInfo[$bDid][$bCid][] = $bRoll['Blogroll']['setting_option'];
                }
            }

            //pr($bRollInfo); exit;

            $finallDResults = array();
            foreach ($allCamps as $akey => $aCamp) {
                $arr = $aCamp['DclCampaign'];
                $cId = $arr['id'];
                //pr(array($arr['id'], $arr['c_count']));
                $anchorList = $this->getAnchorListByCid($arr['anchors']);

                if ($campaignId) {
                    //pr($anchorList);
                }

                if ($anchorList) {
                    $allAnchors = $this->getContentAnchorArray($anchorList, $arr['c_count']);
                    if ($campaignId) {
                        //pr($allAnchors);
                    }

                    if ($allAnchors) {
                        $dStats = json_decode($arr['c_stats'], true);
                        //pr($dStats);
                        if ($dStats) {
                            $cResultArr = array();
                            $increment = 0;
                            foreach ($dStats as $dId => $pStat) {
                                foreach ($pStat as $pId => $codeCount) {
                                    for ($index = 0; $index < $codeCount; $index++) {
                                        $currentAnchor = $allAnchors[$increment];
                                        if ($pId == '-1') {
                                            $currentAnchor = '<li>' . $currentAnchor . '</li>';
                                            if (isset($bRollInfo[$dId][$cId][0])) {
                                                $sOption = $bRollInfo[$dId][$cId][0];
                                                unset($bRollInfo[$dId][$cId][0]);
                                                $bRollInfo[$dId][$cId] = array_values($bRollInfo[$dId][$cId]);
                                                if ($sOption == 2) {
                                                    $currentAnchor = str_replace('<li>', "<li class='dcl_footer'>", $currentAnchor);
                                                }
                                            }
                                        }
                                        $cResultArr[$dId][$pId][] = $currentAnchor;
                                        $increment++;
                                    }
                                }
                            }
                            if ($cResultArr) {
                                /* saving campaign specific results */
                                $cResultJson = json_encode($cResultArr);
                                if (mysql_escape_string($allCamps[$akey]['DclCampaign']['c_results']) != mysql_escape_string($cResultJson)) {
                                    $updateCResults['id'] = $cId = $arr['id'];
                                    $updateCResults['c_results'] = $cResultJson;
                                    $this->DclCampaign->save($updateCResults, false);
                                    unset($updateCResults);
                                }
                                /* */
                                foreach ($cResultArr as $dId => $pValue) {
                                    foreach ($pValue as $pId => $anchors) {
                                        $finallDResults[$dId][$pId][$cId] = $anchors;
                                    }
                                    ksort($finallDResults[$dId]);
                                }
                            }

                            if ($campaignId) {
                                //pr($cResultArr);
                            }
                        }
                    }
                }
            }

            if ($finallDResults) {
                ksort($finallDResults);

                /**
                 * Configure::write('Cache.disable', false);
                 * Cache::write('_dcl_domains_d_results', $finallDResults);
                 * Configure::write('Cache.disable', true);
                 * /* */
                $this->loadModel('DclDomain');
                $prevDResults = $this->DclDomain->find('list', array('fields' => 'id,d_results'));

                /**/
                foreach ($finallDResults as $dId => $dValue) {
                    $dValueJson = json_encode($dValue);
                    if (!isset($prevDResults[$dId]) || mysql_escape_string($prevDResults[$dId]) != mysql_escape_string($dValueJson)) {
                        $updateDResults['id'] = $dId;
                        $updateDResults['d_results'] = $dValueJson;
                        $this->DclDomain->save($updateDResults, false);
                    }
                }
                /**/
            }
            //pr($finallDResults);
        }
        //echo $this->render('sql');
        $this->setRemoteDResults();
        exit;
    }

    function getAnchorListByCid($anchorJson)
    {
        $anchorList = json_decode($anchorJson, true);
        $finalList = array();
        if (is_array($anchorList) && count($anchorList)) {
            foreach ($anchorList as $k => $data) {

                $aText = rtrim(trim($data[0]), ',');
                $aTextArr = (array)explode(",", $aText);
                $aTextArr = array_filter($aTextArr);
                $uText = rtrim(trim($data[1]), ',');
                $uTextArr = (array)explode(",", $uText);
                $uTextArr = array_filter($uTextArr);
                $aNoFollow = $data[2];
                $tDensity = intval(trim($data[3]));
                $tDensityEach = @ceil($tDensity / count($aTextArr));

                if (($tDensity == '') || ($aText == '')) {
                    continue;
                }

                $aCount = count($aTextArr) ? count($aTextArr) : 0;
                $aTextFullArr = array();
                if ($aCount) {
                    $uIndex = 0;
                    for ($i = 0; $i < $aCount; $i++) {
                        if (isset($uTextArr[$uIndex])) {
                            $aTextFullArr[] = trim($uTextArr[$uIndex]);
                        } else {
                            $uIndex = 0;
                            $aTextFullArr[] = trim($uTextArr[$uIndex]);
                        }
                        $uIndex++;
                    }
                }
                // all multiple anchor text with commas
                for ($ii = 0; $ii < count($aTextArr); $ii++) {
                    $temp = array();
                    $anchorText = isset($aTextArr[$ii]) ? $aTextArr[$ii] : '-';
                    $temp[0] = str_replace(array("\r\n", "\n\r", "\n", "\r"), '', trim($anchorText));
                    $interPageUrl = isset($aTextFullArr[$ii]) ? $aTextFullArr[$ii] : '#';
                    $temp[1] = str_replace(array(" ", "\r\n", "\n\r", "\n", "\r"), '', trim($interPageUrl));
                    $temp[2] = $aNoFollow;
                    $temp[3] = $tDensityEach;
                    $finalList[] = $temp;
                }
            }
        }
        return $finalList;
    }

    function getContentAnchorArray($anchorList, $count)
    {
        if ($anchorList) {
            $tempAnchorTexts = array();
            if ($anchorList) {
                foreach ($anchorList as $k => $aL) {
                    $times = @ceil($count * ($aL[3] / 100));
                    if ($times > 0) {
                        for ($i = 0; $i < $times; $i++) {
                            $hrefAnchor = trim($aL[0]);
                            $hrefUrl = preg_replace('/https?\:\/\//i', '', trim($aL[1]));
                            $hrefNoFollow = $aL[2] ? " rel='nofollow' " : ' ';
                            $finalLink = "<a" . $hrefNoFollow . "href='http://" . $hrefUrl . "'>" . $hrefAnchor . "</a>";
                            $tempAnchorTexts[] = $finalLink;
                        }
                    }
                }
                if ($tempAnchorTexts) {
                    $tempAnchorTexts = array_slice($tempAnchorTexts, 0, $count);
                    $tempAnchorTexts = $this->seoShuffleMtSrand($tempAnchorTexts);
                }
            }
            return $tempAnchorTexts;
        }
    }

    function seoShuffleMtSrand($items, $mtSrand = 1)
    {

        if ($items) {
            mt_srand($mtSrand);
            for ($i = count($items) - 1; $i > 0; $i--) {
                $j = @mt_rand(0, $i);
                $tmp = $items[$i];
                $items[$i] = $items[$j];
                $items[$j] = $tmp;
            }
        }
        return $items;
    }

    function st_login()
    {

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        set_time_limit(7200);
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 7200);
        ini_set('max_input_time', 7200);

        App::import('Vendor', 'simpletest/browser');

        $browser = &new SimpleBrowser();

        $this->loadModel('Domain');
        $cond = array(
            'fields' => 'id,domain',
            'conditions' => "hostname != 'SmartSEOHosting' AND networktype IN(5)",
            'limit' => 100,
            'offset' => 0,
        );

        $allSHDomains = $this->Domain->find('list', $cond);

        echo count($allSHDomains);
        //pr($allSHDomains);
        //exit;

        if ($allSHDomains) {
            $counter = 1;
            foreach ($allSHDomains as $dId => $domain) {
                $reArr = array();

                $reArr[$domain]['sl'] = $counter;
                $reArr[$domain]['id'] = $dId;

                //$browser->get('http://as-soudan.com/wp-login.php');
                $url = 'http://www.' . $domain . '/wp-login.php';
                $parameters['log'] = 'admin';
                $parameters['pwd'] = 'Buckaroo04';
                $browser->post($url, $parameters);
                //$browser->setField('log', 'admin');
                //$browser->setField('pwd', 'Buckaroo04');
                //$browser->clickSubmit('Log In');
                $browser->click('Installed Plugins');
                $browser->clickLink('Add New', 3);
                $browser->click('Upload');
                //$browser->setField('pluginzip', 'http://tracker.seonitro.com/pluginsupdates/dclwpnext.zip');
                $browser->setField('pluginzip', 'D:\repo\dclwpnext.zip');
                $actv = $browser->click('Install Now');
                if (strstr($actv, 'Plugin installed successfully')) {
                    $browser->click('Activate Plugin');
                    $reArr[$domain]['new'] = 'Installed and activated';
                } elseif (strstr($actv, 'Destination folder already exists')) {
                    $reArr[$domain]['new'] = 'Already Installed';
                } else {
                    $reArr[$domain]['new'] = 'Something went wrong!';
                }

                $plugins = array(
                    array(
                        'plugin' => 'dclwp/dclwp.php',
                        'action' => 'delete'
                    )
                );
                $params['_nitro_p_action'] = json_encode($plugins);

                $urlOld = 'http://www.' . $domain . '/index.php';

                $result = $this->_updateDclPlugin($urlOld, $params);
                $reArr[$domain]['old'] = stripslashes($result);
                pr($reArr);
                $counter++;
            }
            exit;
        }
    }

    function st_activate()
    {

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        set_time_limit(7200);
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 7200);
        ini_set('max_input_time', 7200);

        App::import('Vendor', 'simpletest/browser');

        $browser = &new SimpleBrowser();

        $this->loadModel('Domain');
        $cond = array(
            'fields' => 'id,domain',
            'conditions' => "hostname != 'SmartSEOHosting' AND networktype IN(5)",
            'limit' => 100,
            'offset' => 0,
        );

        $allSHDomains = $this->Domain->find('list', $cond);

        echo count($allSHDomains);
        //pr($allSHDomains);
        //exit;

        if ($allSHDomains) {
            $counter = 1;
            foreach ($allSHDomains as $dId => $domain) {
                $reArr = array();

                $reArr[$domain]['sl'] = $counter;
                $reArr[$domain]['id'] = $dId;

                //$browser->get('http://as-soudan.com/wp-login.php');
                $url = 'http://www.' . $domain . '/wp-login.php';
                $parameters['log'] = 'admin';
                $parameters['pwd'] = 'Buckaroo04';
                $browser->post($url, $parameters);
                //$browser->setField('log', 'admin');
                //$browser->setField('pwd', 'Buckaroo04');
                //$browser->clickSubmit('Log In');
                $cnt = $browser->click('Installed Plugins');

                //$links = $browser->getLink('Activate');
                //pr($links);
                //exit;
                $isInactiveStr = 'PHPEmbed</strong><div class="row-actions visible"><span class="activate">';
                $isInactiveStr = 'PHPEmbed';

                if (strstr($cnt, $isInactiveStr)) {
                    $actv = $browser->clickLink('Activate', 0);
                    if (strstr($actv, '<strong>activated</strong>.')) {
                        $reArr[$domain]['new'] = 'Activated';
                    } else {
                        $reArr[$domain]['new'] = 'Activation failed!';
                    }
                }


                pr($reArr);
                $counter++;
            }
            exit;
        }
    }

    function pingAction($actionName = null)
    {
        $this->$actionName();
    }

    function paction()
    {
        /**/
        @ob_end_clean();
        ignore_user_abort();
        ob_start();
        header("Connection: close");
        header("Content-Length: " . ob_get_length());
        ob_end_flush();
        flush();
        /* */
        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        set_time_limit(7200);
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 7200);
        ini_set('max_input_time', 7200);

        $this->loadModel('Domain');
        $cond = "hostname != 'SmartSEOHosting' AND networktype IN(2,3,4,5,6)";
        //$cond = "";
        $limit = '999999999';
        $allSHDomains = $this->Domain->find('list', array('conditions' => $cond, 'fields' => 'id,domain', 'limit' => $limit, 'offset' => $offset));

        //pr($allSHDomains);

        if ($allSHDomains) {
            $counter = 0;
            foreach ($allSHDomains as $dId => $domain) {
                $reArr = array();

                $reArr[$domain]['sl'] = $counter;
                $reArr[$domain]['id'] = $dId;

                $plugins_ = array(
                    array(
                        'plugin' => 'dclwpnext/dclwpnext.php',
                        'action' => 'update'
                    )
                );
                $plugins = array(
                    array(
                        'plugin' => 'dclwpnext/dclwpnext.php',
                        'update_url' => 'http://tracker.seonitro.com/pluginsupdates/dclwpnext.zip',
                        'latest_version' => '2.2.4',
                    )
                );
                //$params['_nitro_p_action'] = json_encode($plugins);
                $params['_nitro_p_update'] = json_encode($plugins);

                $urlOld = 'http://www.' . $domain . '/index.php';

                $result = $this->_updateDclPlugin($urlOld, $params);
                $reArr[$domain]['old'] = stripslashes($result);
                //pr($reArr);
                $counter++;
            }
            $this->cmail(TEST_EMAIL, 'P_Update', 'Total Update: ' . $counter);
            exit;
        }
    }

    function _updateDclPlugin($url, $postData)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        $result = curl_exec($curl);
        if (FALSE === ($result)) {
            $this->_globalArr['not_resolved'][] = $url;
            return 'httperror# ' . curl_error($curl);
        } else {
            if (strlen($result) > 200) {
                $this->_globalArr['phpembed_not_found'][] = $url;
                return 'PHPEmbed Next not found';
            } else {
                return $result;
            }
        }
    }

    function _vrp($arr)
    {
        echo '<pre>';
        echo preg_replace("/[0-9]+ \=\>/i", '', var_export($arr, true));
        echo '</pre>';
    }

    function listDErrors()
    {
        $this->loadModel('Domain');
        $allPrimes = $this->Domain->find('list', array('fields' => 'domain,httpstatus', 'conditions' => "httpstatus IN('0:')"));
        echo count($allPrimes);
        $string = '';
        if ($allPrimes) {
            foreach ($allPrimes as $dom => $error) {
                $string .= $dom . ',' . $error . '<br/>';
            }
        }
        echo $string;
        exit;
    }

    function listDomains($domain = null)
    {
        $this->loadModel('Domain');
        $params = array(
            'fields' => 'id,domain',
            'conditions' => $domain ? "domain='$domain' OR id = '$domain'" : ''
        );
        $dList = $this->Domain->find('list', $params);
        pr($dList);
        exit;
    }

    function multiRequest($data, $options = array())
    {
        //http://www.phpied.com/simultaneuos-http-requests-in-php-with-curl/
        // array of curl handles
        $curly = array();
        // data to be returned
        $result = array();

        // multi handle
        $mh = curl_multi_init();

        // loop through $data and create curl handles
        // then add them to the multi-handle
        foreach ($data as $id => $d) {

            $curly[$id] = curl_init();

            $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
            curl_setopt($curly[$id], CURLOPT_URL, $url);
            curl_setopt($curly[$id], CURLOPT_HEADER, 0);
            curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);

            // post?
            if (is_array($d)) {
                if (!empty($d['post'])) {
                    curl_setopt($curly[$id], CURLOPT_POST, 1);
                    curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
                }
            }

            // extra options?
            if (!empty($options)) {
                curl_setopt_array($curly[$id], $options);
            }

            curl_multi_add_handle($mh, $curly[$id]);
        }

        // execute the handles
        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running > 0);


        // get content and remove handles
        foreach ($curly as $id => $c) {
            curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 0);
            $result[$id] = trim(curl_multi_getcontent($c)) == '1' ? '1' : curl_error($c);
            //$result[$id] = trim(curl_multi_getcontent($c)) == '1' ? '1' : curl_multi_getcontent($c);
            curl_multi_remove_handle($mh, $c);
        }

        // all done
        curl_multi_close($mh);

        return $result;
    }

    function check_phpembed()
    {

        Configure::write('debug', 2);
        Configure::write('Cache.disable', true);

        set_time_limit(7200);
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 7200);
        ini_set('max_input_time', 7200);

        /**/
        @ob_end_clean();
        ignore_user_abort();
        ob_start();
        header("Connection: close");
        header("Content-Length: " . ob_get_length());
        ob_end_flush();
        flush();
        /* */
        echo 'downhere';

        $this->loadModel('Domain');
        //$allAutos = $this->Domain->find('list', array('fields' => 'id,domain', 'conditions' => 'networktype IN(3)', 'limit' => 10));
        $cond = array(
            'conditions' => "networktype IN(3,4,5)",
            'fields' => 'id,domain',
            //'limit' => 20
        );
        $allAutos = $this->Domain->find('list', $cond);
        if ($allAutos) {
            $allArr = array();
            foreach ($allAutos as $dId => $domain) {
                $updateData['httpstatus'] = $this->remoteFileCheck($domain);
                $allArr[$domain] = $updateData['httpstatus'];
                $this->Domain->id = $dId;
                $this->Domain->save($updateData, false);
            }
            $this->cmail(TEST_EMAIL, 'PHPEmbed Check', $this->prettyEmailBody($allArr));
        }
    }

    function remoteFileCheck($domain = null)
    {

        /**
         * $url = 'http://aan-mainul/wpfresh/wp-content/';
         * $url = 'http://www.iphone4sjailbreakhome.com/wp-content/index.php';
         * $headersLocal = @get_headers($url);
         * pr($headersLocal);
         * exit;
         * /* */
        $urlPlugin = 'http://www.' . $domain . '/wp-content/plugins/dclwpnext/index.html';
        $headersPlugin = @get_headers($urlPlugin);
        if (!$headersPlugin || ($headersPlugin[0] == 'HTTP/1.1 404 Not Found')) {
            $urlRoot = 'http://www.' . $domain . '/wp-content/index.php';
            $headersRoot = @get_headers($urlRoot);
            //pr($headersRoot);
            if (!$headersRoot || ($headersRoot[0] == 'HTTP/1.1 404 Not Found')) {
                return 'Site Error';
            } else {
                return 'PHPEmbed Error';
            }
            return 'Unknown';
        } else {
            return 'OK';
        }
    }

    function checkdb()
    {
        App::uses('ConnectionManager', 'Model');
        $db = ConnectionManager::getDataSource('default');

        $isconnected = $db->isConnected();  //is the connection open?
        $db->close();  //close the connection
        $db->reconnect();  //reconnect to the db
    }

    function dbprefix()
    {
        $this->loadModel('Nitro');
        $allDb = $this->Nitro->nitroDbInfo();
        $this->loadModel('Domain');

        $allDomains = $this->Domain->find('list', array(
            'conditions' => "owner_id = -1 AND httpcode = '200' AND status = 1",
            'fields' => 'domain,id'
        ));

        echo count($allDomains);
        echo '<br/>';
        //pr($allDomains);
        //exit;

        $counter = 0;
        if ($allDb) {
            foreach ($allDb as $dom => $value) {
                if ($value['DOMAIN']) {
                    $dom = trim($dom);
                    if (isset($allDomains[$dom])) {
                        $updateData['id'] = $allDomains[$dom];
                        $updateData['dbname'] = $value['TABLE_SCHEMA'];
                        $updateData['dbuser'] = $value['TABLE_SCHEMA'];
                        $updateData['dbtable_prefix'] = $value['TABLE_PREFIX'];
                        $this->Domain->save($updateData, false);
                        //pr($updateData);
                        $counter++;
                    }
                }
            }
        }

        echo $counter;
        //pr($allDb);
        exit;
    }

    function sendd()
    {
        $this->autoRender = false;
        $this->EmailSmtp = $this->Components->load('Smail');
        // You can use customised thmls or the default ones you setup at the start
        //$to[] = 'abirymail@gmail.com';
        $to[] = 'mainuljs@gmail.com';
        $subject = 'your new account';
        $msg = 'your new account';


        $result = $this->EmailSmtp->send($to, $subject, $msg);
        pr($result);
        exit;
    }

    function getvillo()
    {
        Configure::write('debug', 2);
        Configure::write('Cache.disable', false);
        $limit = 1000;

        $endpoint = 'http://villo.me/getd.php';

        $this->loadModel('Ready');

        /**/
        for ($i = 0; $i <= 4000; $i += $limit) {
            $offset = $i == 0 ? $i : $i + 1;
            $qr = '?offset=' . $offset . '&limit=' . $limit;
            $url = $endpoint . $qr;
            $jres = $this->Ready->curl_get_contents($url);
            Cache::write("dbinfo_{$offset}_{$limit}", $jres, 'year');
            unset($jres);
        }
        /* */
    }

    function myvillo()
    {
        Configure::write('debug', 2);
        Configure::write('Cache.disable', false);
        $limit = 1000;
        $arr = array();
        for ($i = 0; $i <= 4000; $i += $limit) {
            /**/
            $offset = $i == 0 ? $i : $i + 1;
            $key = "dbinfo_{$offset}_{$limit}";
            $jres = Cache::read($key, 'year');
            $arr = $arr + json_decode($jres, true);
            unset($jres);
            /**/
        }
        echo count($arr);
        vr($arr);
        exit;
    }

}
