<?php

App::uses('Component', 'Controller');
App::import('Vendor', 'phpmailer', array('file' => 'phpmailer' . DS . 'class.phpmailer.php'));

class EmailComponent extends Component {

    public function send($to, $subject, $message) {
        $sender = "from_you@you.com"; // this will be overwritten by GMail

        $header = "X-Mailer: PHP/" . phpversion() . "Return-Path: $sender";

        $mail = new PHPMailer();

        $mail->IsSMTP();
        //$mail->Timeout = 10;
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "ssl";
        //$mail->SMTPSecure = "tls";
        $mail->Port = 465;
        //$mail->Port = 587;
        $mail->SMTPDebug = 2; // turn it off in production
        $mail->Username = "itestgo@gmail.com";
        $mail->Password = "24842311";

        $mail->From = $sender;
        $mail->FromName = "From Me";

        if (is_array($to)) {
            foreach ($to as $toemail) {
                $mail->AddAddress($toemail);
            }
        } else {
            $mail->AddAddress($to);
        }

        $mail->IsHTML(true);
        $mail->CreateHeader($header);

        $mail->Subject = $subject;
        $mail->Body = nl2br($message);
        $mail->AltBody = nl2br($message);

        // return an array with two keys: error & message
        if (!$mail->Send()) {
            return array('error' => true, 'message' => 'Mailer Error: ' . $mail->ErrorInfo);
        } else {
            return array('error' => false, 'message' => "Message sent!");
        }
    }

}
