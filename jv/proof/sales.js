function ie6Filter() {
	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ //test for MSIE x.x;
		var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
	    if (ieversion<=6) {
			anchors=document.getElementsByTagName('a');
		 	for(i=0;i<anchors.length;i++) {
		  		if(anchors[i].href == "javascript:void(0);" || anchors[i].href == "javascript:void();") {
		           anchors[i].href = "#";
				}
		 	}
	    }
	}
}

function cleanText(id, text) {
  if (document.getElementById(id).value == text) {
    document.getElementById(id).value = '';
  }
}

function refillText(id, text) {
  if (document.getElementById(id).value == '') {
    document.getElementById(id).value = text;
  }
}